//
//  AppDelegate+PKPushRegistryDelegate.swift
//  de
//
//  Created by  on 31/08/17.
//  Copyright © 2017. All rights reserved.
//
import UserNotifications
import PushKit
import SwiftyJSON
import UIKit
import CallKit

// MARK: PKPushRegistryDelegate
extension AppDelegate: PKPushRegistryDelegate {
    
    func pushRegistry(_ registry: PKPushRegistry, didUpdate pushCredentials: PKPushCredentials, for type: PKPushType) {
        
        let characterSet: CharacterSet = CharacterSet( charactersIn: "<>" )
        
        var token = ""
        for i in 0..<pushCredentials.token.count {
            token = token + String(format: "%02.2hhx", arguments: [pushCredentials.token[i]])
        }
        
        token = token.trimmingCharacters(in: characterSet)
        token = token.replacingOccurrences(of: " ", with: "")
        
        if !token.isEmpty {
            NSLog("VoIP token: \(token)")
            print_debug("VoIP token: \(token)")
            DeviceDetail.deviceToken = token
            AppUserDefaults.save(value: token, forKey: .voIPToken)
        }
    }
    
    func pushRegistry(_ registry: PKPushRegistry, didReceiveIncomingPushWith payload: PKPushPayload, for type: PKPushType) {

        if !sharedAppDelegate.socket.isConnected{
            sharedAppDelegate.socket.connect()
        }
        
        guard type == .voIP, let aps = payload.dictionaryPayload["aps"] as? JSONDictionary else { return }
        
        NSLog(JSON(aps).rawString() ?? "")
        
        let callerJSON = JSON(aps)
        guard let caller = Caller(json: callerJSON["data"]) else {
            if (aps["type"] as? String) == "disconnect" {
                removeAllLocalNotifications()
                endAllCalls()
            }
            return
        }
        guard let apsData = aps["data"] as? JSONDictionary else{ return}
        print_debug(apsData["type"])

        let appState = UIApplication.shared.applicationState
        
        if appState == .active {
            let topVC = UIApplication.topViewController()
             if let notitype = apsData["type"] as? String, notitype == "calling"{
//
                if ( topVC is RTCVideoChatVC) {
                    let loginJson = ["type": "login", "name": User.getUserModel().user_id ?? ""]
                    CommonClass.writeToSocket(loginJson)
                    print_debug(apsData["type"])
                    CommonClass.delayWithSeconds(0.3, completion: {
                        let json: JSONDictionary = ["type": "leave", "name": User.getUserModel().user_id ?? "" , "otherName":caller.sender_id]
                        CommonClass.writeToSocket(json)
                    })

                    if let videoChatScene = UIApplication.topViewController() as? RTCVideoChatVC {
                        videoChatScene.disconnect()
                    }
                }else{
//                    self.displayIncomingCall(for: caller) { _ in
//
//                        // pre-heat the AVAudioSession
//
//                        configureAudioSession()
//
//                        // start time out timer
//                        self.setupTimeoutTimer()
//
////                        UIApplication.shared.endBackgroundTask(backgroundTaskIdentifier)
//                    }

                    sharedAppDelegate.moveToVideoChatScene(with: caller,fromPush : true, fromCallKit: false)
                }
             }else{
                removeAllLocalNotifications()
                endAllCalls()
                UIApplication.shared.endBackgroundTask(UIApplication.shared.beginBackgroundTask(expirationHandler: nil))
                self.navigationController?.dismiss(animated: true, completion: nil)
            }

        }
        else if #available(iOS 10.0, *) {
            let backgroundTaskIdentifier = UIApplication.shared.beginBackgroundTask(expirationHandler: nil)

            if let notitype = apsData["type"] as? String, notitype == "calling"{

                self.displayIncomingCall(for: caller) { _ in

                    // pre-heat the AVAudioSession

                    configureAudioSession()

                    // start time out timer
                    self.setupTimeoutTimer()

                    UIApplication.shared.endBackgroundTask(backgroundTaskIdentifier)
                }
            }else{
                UIApplication.shared.endBackgroundTask(backgroundTaskIdentifier)
                endAllCalls()
            }
        }
        else {

            let notificationContent = UNMutableNotificationContent()
            
            // Configure Notification Content
            notificationContent.title = ""
            if let notitype = apsData["type"] as? String, notitype == "calling"{
                
                notificationContent.subtitle = "\(caller.sender_name) is calling."
                
                let notificationTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 3.0, repeats: false)
                notificationContent.sound = UNNotificationSound(named: "ringtone.mp3")
                
                guard let userInfo = payload.dictionaryPayload["aps"] as? JSONDictionary else{return}
                notificationContent.userInfo = userInfo
                // Create Notification Request
                let notificationRequest = UNNotificationRequest(identifier: "ANSWER_CATEGORY", content: notificationContent, trigger: notificationTrigger)
                
                // Add Request to User Notification Center
                UNUserNotificationCenter.current().add(notificationRequest) { (error) in
                    if let error = error {
                        print("Unable to Add Notification Request (\(error), \(error.localizedDescription))")
                    }
                }
            }else{
                removeAllLocalNotifications()
                endAllCalls()

            }
        }
    }
    func sendBusy(to caller: Caller) {
        let json: JSONDictionary = ["type": "busy", "name": caller.loginId]
        CommonClass.writeToSocket(json)
        self.navigationController?.dismiss(animated: true, completion: nil)
    }
//    
//    func showCallScene(on controller: UIViewController?, with caller: Caller) {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        let callScene = storyboard.instantiateViewController(withIdentifier: "CallVC") as! CallVC
//        callScene.caller = caller
//        
//        if let navCont = controller?.navigationController {
//            navCont.pushViewController(callScene, animated: true)
//        } else {
//            let navCont = UINavigationController(rootViewController: callScene)
//            controller?.present(navCont, animated: true, completion: nil)
//        }
//    }
    
    func pushRegistry(_ registry: PKPushRegistry, didInvalidatePushTokenFor type: PKPushType) {
        //printlnDebug("token invalidated")
    }
    
    // Display the incoming call to the user
    @available(iOS 10.0, *)
    func displayIncomingCall(for caller: Caller, completion: ((NSError?) -> Void)? = nil) {
        if let providerDelegate = providerDelegate as? ProviderDelegate {
            
            providerDelegate.reportIncomingCall(for: caller, completion: completion)
        }
    }
    
    func endAllCalls() {
        
        if #available(iOS 10.0, *),
            let callMngr = callManager as? CallManager {
            callMngr.calls.forEach({ call in
                callMngr.end(call)
                
                if let providerDelegate = providerDelegate as? ProviderDelegate {
                    providerDelegate.provider.reportCall(with: call.uuid, endedAt: nil, reason: CXCallEndedReason.remoteEnded)
                }
                callMngr.remove(call)
            })
        }
//        sharedAppDelegate.socket.disconnect()
    }
    
    func addCalls() {
        if #available(iOS 10.0, *),
            let callMngr = callManager as? CallManager {
            callMngr.calls.forEach({ call in
                callMngr.add(call)
            })
        }
    }
}
