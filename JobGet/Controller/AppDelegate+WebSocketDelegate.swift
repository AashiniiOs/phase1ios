//
//  AppDelegate+PKPushRegistryDelegate.swift
//  de
//
//  Created by  on 31/08/17.
//  Copyright © 2017. All rights reserved.
//

import PushKit
import SwiftyJSON
import UIKit
import Starscream

// MARK: WebSocketDelegate
extension AppDelegate: WebSocketDelegate {
    func websocketDidConnect(socket: WebSocketClient) {
        
        print_debug(socket.isConnected)
//        let loginJson = ["type": "login", "name": User.getUserModel().user_id ?? ""]
//        CommonClass.writeToSocket(loginJson)
    }
    
    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
         print_debug("\(socket.isConnected) \(error?.localizedDescription)")
        socket.connect()
    }
    
    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
         print_debug("\(socket.isConnected) \(text)")
        sharedAppDelegate.endAllCalls()
    }
    
    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
        print_debug("\(socket.isConnected) \(data.base64EncodedData())")
    }
    
    
}
