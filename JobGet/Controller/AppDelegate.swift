 //
 //  AppDelegate.swift
 //  Onboarding
 //
 //  Created by Gurdeep Singh on 04/07/16.
 //  Copyright © 2016 Gurdeep Singh. All rights reserved.
 //
 
 import UIKit
 import SwiftyJSON
 import FBSDKCoreKit
 import FBSDKLoginKit
 import GooglePlaces
 import IQKeyboardManagerSwift
 import GoogleMaps
 import Firebase
 import UserNotifications
 import PushKit
 import Starscream
 import Fabric
 
 
 enum isFromDeepLinking {
    
    case fromDepplinking
    case firstTime
 }
 
 let sharedAppDelegate = UIApplication.shared.delegate as! AppDelegate
 
 // MARK: Enum
 //    ===========
 enum CallAction {
    case ring
    case accept
    case none
 }
 
 
 @UIApplicationMain
 class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var navigationController: UINavigationController?
    var chatMember : ChatMember?
    
    var callManager: Any?
    var providerDelegate: Any?
    var callTimeOutTimer: Timer?
    var deviceToken: String?
    var fcmToken: String?
    
    let voipRegistry = PKPushRegistry(queue: DispatchQueue.main)
    let socket = WebSocket(url: URL(string: "ws://184.73.96.84:3000")!)
//  let socket = WebSocket(url: URL(string: "ws://test.jobget.com")!)
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        if let launch = launchOptions ,
            let remoteNotifications =  launch[UIApplicationLaunchOptionsKey.remoteNotification] as? [NSObject: AnyObject] {
            DispatchQueue.main.async{
                
//                self.application(application, didReceiveRemoteNotification: remoteNotifications)
            }
        }
        registerUserNotifications()

        ///Reset location filter to nil
        AppUserDefaults.removeValue(forKey: .filterLat)
        AppUserDefaults.removeValue(forKey: .filterLong)
        
        if let dic = Bundle.main.infoDictionary, let version = dic["CFBundleShortVersionString"] as? String{
            let param = ["device_type" : 2,"version" : version] as [String : Any]
            WebServices.getVersionNumber(params : param,success: { (json) in
                let versionNum = json["data"]["versionNumber"].floatValue
                let fVersion = Float(version) ?? 0.0
                if fVersion < versionNum{
                    let alertController = UIAlertController(title: "New version available", message: "Please, upgrade app to new version for better experience.", preferredStyle: .alert)
                    if json["data"]["updateType"].stringValue == "NORMAL"{
                        let cancel = UIAlertAction(title: "No, Thanks", style: .cancel, handler: nil)
                        alertController.addAction(cancel)
                    }
                    let update = UIAlertAction(title: "Update", style: .default, handler: { action in
                        self.updateApplication()
                    })
                    alertController.addAction(update)
                    self.window?.rootViewController?.present(alertController, animated: true, completion: nil)
                }
            }) { (error) in
                CommonClass.showToast(msg: error.localizedDescription)
            }
        }
        self.didFinishLaunchingWithOptionsMethod(application, didFinishLaunchingWithOptions: launchOptions)
        return true
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        Messaging.messaging().apnsToken = deviceToken
        let deviceTokenString = deviceToken.base64EncodedString()
        self.deviceToken = deviceTokenString
        
        let characterSet: CharacterSet = CharacterSet( charactersIn: "<>" )
        
        var token = ""
        for i in 0..<deviceToken.count {
            token = token + String(format: "%02.2hhx", arguments: [deviceToken[i]])
        }
        token = token.trimmingCharacters(in: characterSet)
        token = token.replacingOccurrences(of: " ", with: "")
        if !token.isEmpty {
        }
        //         self.registerPushNotification(application)
    }
    
    func application(_ application: UIApplication, continue userActivity: NSUserActivity, restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        guard let handle = userActivity.startCallHandle else {
            //printlnDebug("Could not determine start call handle from user activity: \(userActivity)")
            return false
        }
        
        guard let video = userActivity.video else {
            //printlnDebug("Could not determine video from user activity: \(userActivity)")
            return false
        }
        
        if #available(iOS 10.0, *), let callManager = callManager as? CallManager {
            callManager.startCall(handle: handle, videoEnabled: video)
        }
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        //FirebaseHelper.setStatus(online: false)
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // FirebaseHelper.setStatus(online: false)
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        if let userId = User.getUserModel().user_id, !userId.isEmpty{
            ChatHelper.setOnline(userId: userId, status: false)
        }
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        //FirebaseHelper.setStatus(online: true)
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
        if let userId = User.getUserModel().user_id, !userId.isEmpty{
            ChatHelper.setOnline(userId: userId, status: true)
        }
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        //FirebaseHelper.setStatus(online: true)
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        self.socket.connect()
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // FirebaseHelper.setStatus(online: false)
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        if let userId = User.getUserModel().user_id, !userId.isEmpty{
            ChatHelper.setOnline(userId: userId, status: false)
        }
        if let videoChatScene = UIApplication.topViewController() as? RTCVideoChatVC {
            videoChatScene.isRejectedByCan = true
            let json: JSONDictionary = ["type": SocketDataType.decline.rawValue, "name": User.getUserModel().user_id ?? "" , "otherName":videoChatScene.caller.receiver_id]
            CommonClass.writeToSocket(json)
            videoChatScene.callRecord()
            videoChatScene.disconnect()
        }
        databaseReference.removeAllObservers()
        endAllCalls()
        
        
    }
    func application(_ app: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any] = [:]) -> Bool{
        
        if (url.scheme == StringConstants.FBiD) {
            return FBSDKApplicationDelegate.sharedInstance().application(app,
                                                                         open: url as URL?,
                                                                         sourceApplication: options[.sourceApplication] as? String,
                                                                         annotation: options[.annotation] as? String)
        } else if url.scheme?.lowercased() == "jobget" {
            
            let urlString = url.absoluteString
            print_debug(urlString)
            
            if urlString.contains(s: "share") {
                let userType = AppUserDefaults.value(forKey: .userType).stringValue
                
                if userType == UserType.candidate.rawValue {
                    
                    self.goToCandidateJobDetailSceen(jobId: urlString.components(separatedBy: "=").last ?? "")
                } else {
                }
            } else if urlString.contains(s: "candidate") {
                
                let userType = AppUserDefaults.value(forKey: .userType).stringValue
                print_debug(userType)
                if userType == UserType.recuriter.rawValue {
                    self.goToCandidateDetail(userId: urlString.components(separatedBy: "=").last ?? "")
                } else {
                    let selecteUserTpye = SelectUserTypeVC.instantiate(fromAppStoryboard: .Main)
                    CommonClass().gotoViewController(selecteUserTpye)
                }
            }else if urlString.contains(s: "reset"){
                if let emailAndType = urlString.components(separatedBy: "id").last  {
                    
                    guard let type = emailAndType.components(separatedBy: "=").last  else { return false}
                    guard let emailValue = emailAndType.components(separatedBy: "&").first  else { return false}
                    guard let email = emailValue.components(separatedBy: "=").last  else { return false}
                    
                    let sceen = ResetPasswordVC.instantiate(fromAppStoryboard: .Main)
                    sceen.userType = type
                    sceen.email = email
                    commingFrom = .resetPassword
                    CommonClass().gotoViewController(sceen)
                    
                    //                    guard let navigation = self.navigationController else { return false }
                    //
                    //                    if  navigation.viewControllers.count > 1   {
                    //                        CommonClass.delayWithSeconds(0.1, completion: {
                    //                            navigation.pushViewController(sceen, animated: true)
                    //                        })
                    //
                    ////                    let selecteUserTpye = SelectUserTypeVC.instantiate(fromAppStoryboard: .Main)
                    ////                    CommonClass().gotoViewController(selecteUserTpye)
                    //                }
                }

            }
                
            else if !urlString.contains(s: "invite"){
                if let emailAndType = urlString.components(separatedBy: "id").last  {
                    
                    guard let type = emailAndType.components(separatedBy: "=").last  else { return false}
                    guard let emailValue = emailAndType.components(separatedBy: "&").first  else { return false}
                    guard let email = emailValue.components(separatedBy: "=").last  else { return false}
                    let sceen = ResetPasswordVC.instantiate(fromAppStoryboard: .Main)
                    sceen.userType = type
                    sceen.email = email
                    guard let navigation = self.navigationController else { return false }
                    
                    if  navigation.viewControllers.count > 1   {
                        commingFrom = .resetPassword
                        CommonClass.delayWithSeconds(0.1, completion: {
                            navigation.pushViewController(sceen, animated: true)
                        })
                        //  self.navigationController?.setViewControllers([mainViewController, socialLogin,login, resetPasswordObj!], animated: true)
                    } else {
                        self.goSelectUser(from: .fromDepplinking, type: type, resetPasswordObj: sceen)
                    }
                }
            }
        }
        return true
    }
    
    
//    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
//        guard let aps = (userInfo as? [String: AnyObject]) else { return }
//        guard let info = aps["aps"] as? [String : AnyObject] else { return }
//        if let type = aps["gcm.notification.type"] as? String{
//            if let data = JSON(aps)["gcm.notification.sender"].stringValue.data(using: .utf8){
//                if let json = try? JSONSerialization.jsonObject(with: data, options: []){
//
//                    let roomid = JSON(aps)["gcm.notification.roomId"].stringValue
//                    let userId = JSON(json)["user_id"].stringValue
//                    if userId.isEmpty{
//                        return
//                    }
//                    ChatHelper.getUserDetails(userId: userId, completion: { [weak self](member) in
//                        guard let _self = self else { return }
//                        if let member = member {
//                            if #available(iOS 10.0, *) {
//                                let center = UNUserNotificationCenter.current()
//                                center.removeAllPendingNotificationRequests() // To remove all pending notifications which are not delivered yet but scheduled.
//                                center.removeAllDeliveredNotifications() // To remove all delivered notifications
//                            }
//                            let topVC = UIApplication.topViewController()
//                            if ( topVC is PersonalChatVC) {
//                                print_debug("Stop PushViewController")
//                            }else{
//                                let loggedInUser = User.getUserModel()
//                                if loggedInUser.user_Type == .candidate{
//                                    let sceen = CandidateTabBarVC.instantiate(fromAppStoryboard: .Candidate)
//                                    //sceen.fromPush = true
//                                    sceen.isChatPush = true
//                                     sceen.roomId = roomid
//                                    sceen.chatMember = member
//                                    if let navigation = _self.navigationController  {
//                                        navigation.pushViewController(sceen, animated: true)
//                                        _self.window?.makeKeyAndVisible()
//                                    }
//                                }else{
//                                    let sceen = RecruiterTabBarVC.instantiate(fromAppStoryboard: .Recruiter)
//                                    //sceen.fromPush = true
//                                    sceen.isChatPush = true
//                                    sceen.selectIndex = 1
//                                    sceen.roomId = roomid
//                                    sceen.chatMember = member
//                                    if let navigation = _self.navigationController  {
//                                        navigation.pushViewController(sceen, animated: true)
//                                        _self.window?.makeKeyAndVisible()
//                                    }
//                                }
//
//
////                                let chatVc = RecentChatVC.instantiate(fromAppStoryboard: .Candidate)
////                                chatVc.roomId = roomid
////                                chatVc.fromPush = true
////                                if let navigation = _self.navigationController  {
////                                    navigation.pushViewController(chatVc, animated: true)
////                                }
//                            }
//                        }
//                    })
//                }
//            }
//        }
//        guard let type = aps["type"] as? String else{return}
//        if let badgeCount = aps["badgeCount"] as? Int{
//            AppUserDefaults.save(value: badgeCount, forKey: .totalNotification)
//        }
//        print_debug("Payload: \(aps)\n")
//        print_debug(info)
//        switch type {
//        case "1","3":
//
//            let candidateDetail = CandidateTabBarVC.instantiate(fromAppStoryboard: .Candidate)
//            candidateDetail.fromPush = true
//            if type == "1"{
//                AppUserDefaults.save(value: 1, forKey: .shortlistNotification)
//            }
//
//            if let jobid = aps["job_id"] as? String{
//                candidateDetail.job_id = jobid
//            }
//            if let sender_id = aps["sender_id"] as? String{
//                candidateDetail.sender_id = sender_id
//            }
//            if let navigation = self.navigationController  {
//                navigation.pushViewController(candidateDetail, animated: true)
//                self.window?.makeKeyAndVisible()
//            }
//        case "2":
//            let sceen = RecruiterTabBarVC.instantiate(fromAppStoryboard: .Recruiter)
//            //sceen.fromPush = true
//            sceen.isJobPush = true
//            if let jobid = aps["job_id"] as? String{
//                sceen.jobId = jobid
//            }
////            if let sender_id = aps["sender_id"] as? String{
////                candidateDetail.sender_id = sender_id
////            }
//            if let navigation = self.navigationController  {
//                navigation.pushViewController(sceen, animated: true)
//                self.window?.makeKeyAndVisible()
//            }
//
//
//        default:
//            return
//        }
//    }
    
    func application(_ application: UIApplication, didReceive notification: UILocalNotification) {
        
        handleVoIPInfo((notification.userInfo as! JSONDictionary), fromCallKit: false, by: .ring)
    }
    
    func connectAndLogin(caller: Caller, fromCallKit:Bool, performing action: CallAction, completion: (() -> Void)? = nil) {
        
        if !socket.isConnected {
            socket.connect()
        }
        socket.onConnect = { [weak self] in
            
            guard let strongSelf = self else {
                return
            }
            
            let userId = AppUserDefaults.value(forKey: .userId).stringValue
            let loginJson = ["type": "login", "name": "\(userId)_\("firstname")"]
            CommonClass.writeToSocket(loginJson)
            
            switch action {
            case .ring: break
            //                strongSelf.moveToCallScene(with: caller)
            case .accept:
                strongSelf.moveToVideoChatScene(with: caller, fromPush: true, fromCallKit: fromCallKit)
            case .none:
                break
            }
            
            completion?()
            self?.socket.onConnect = nil
        }
    }
 }
 
 extension AppDelegate : MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
        self.fcmToken  = fcmToken
        
        AppUserDefaults.save(value: fcmToken, forKey: .fcmToken)
        
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
 }
 
 @available(iOS 10.0, *)
 extension AppDelegate: UNUserNotificationCenterDelegate {
    
    // This method will be called when app received push notifications in foreground
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
    {
        
        let appState = UIApplication.shared.applicationState
        if appState != .active{
            guard let userInfo =  notification.request.content.userInfo  as? JSONDictionary else{return}
            print_debug("\(userInfo)")
            if let type = userInfo["gcm.notification.type"] as? String, type == "chat"{
                print_debug(type)
                completionHandler([.alert, .badge, .sound])
            }else{
                var count = AppUserDefaults.value(forKey: .totalNotification).intValue
                count += 1
                if let type = userInfo["type"] as? String, type == "1"{
                    AppUserDefaults.save(value: 1, forKey: .shortlistNotification)
                }
                NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue: "Update_Badge_Count")))
                AppUserDefaults.save(value: count, forKey: .totalNotification)
                completionHandler([.alert, .badge, .sound])
                
            }
        }
    }
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {

        let appState = UIApplication.shared.applicationState
        if appState != .active{
            if let userInfo = response.notification.request.content.userInfo  as? JSONDictionary{
//                self.application(UIApplication.shared, didReceiveRemoteNotification: userInfo)
                if let _ = userInfo["hasVideo"] as? Int{
                    handleVoIPInfo(userInfo,fromCallKit: true, by: .accept)
                }
            }            
        }
        
        guard let aps = (response.notification.request.content.userInfo as? [String: AnyObject]) else { return }
        guard let info = aps["aps"] as? [String : AnyObject] else { return }
        if let type = aps["gcm.notification.type"] as? String{
            if let data = JSON(aps)["gcm.notification.sender"].stringValue.data(using: .utf8){
                if let json = try? JSONSerialization.jsonObject(with: data, options: []){
                    
                    let roomid = JSON(aps)["gcm.notification.roomId"].stringValue
                    let userId = JSON(json)["user_id"].stringValue
                    if userId.isEmpty{
                        return
                    }
                    ChatHelper.getUserDetails(userId: userId, completion: { [weak self](member) in
                        guard let _self = self else { return }
                        if let member = member {
                            if #available(iOS 10.0, *) {
                                let center = UNUserNotificationCenter.current()
                                center.removeAllPendingNotificationRequests() // To remove all pending notifications which are not delivered yet but scheduled.
                                center.removeAllDeliveredNotifications() // To remove all delivered notifications
                            }
                            let topVC = UIApplication.topViewController()
                            if ( topVC is PersonalChatVC) {
                                print_debug("Stop PushViewController")
                            }else{
                                let loggedInUser = User.getUserModel()
                                if loggedInUser.user_Type == .candidate{
                                    let sceen = CandidateTabBarVC.instantiate(fromAppStoryboard: .Candidate)
                                    //sceen.fromPush = true
                                    sceen.isChatPush = true
                                    sceen.roomId = roomid
                                    sceen.chatMember = member
                                    if let navigation = _self.navigationController  {
                                        navigation.pushViewController(sceen, animated: true)
                                        _self.window?.makeKeyAndVisible()
                                    }
                                }else{
                                    let sceen = RecruiterTabBarVC.instantiate(fromAppStoryboard: .Recruiter)
                                    //sceen.fromPush = true
                                    sceen.isChatPush = true
                                    sceen.selectIndex = 1
                                    sceen.roomId = roomid
                                    sceen.chatMember = member
                                    if let navigation = _self.navigationController  {
                                        navigation.pushViewController(sceen, animated: true)
                                        _self.window?.makeKeyAndVisible()
                                    }
                                }
                                
                                
                                //                                let chatVc = RecentChatVC.instantiate(fromAppStoryboard: .Candidate)
                                //                                chatVc.roomId = roomid
                                //                                chatVc.fromPush = true
                                //                                if let navigation = _self.navigationController  {
                                //                                    navigation.pushViewController(chatVc, animated: true)
                                //                                }
                            }
                        }
                    })
                }
            }
        }
        guard let type = aps["type"] as? String else{return}
        if let badgeCount = aps["badgeCount"] as? Int{
            AppUserDefaults.save(value: badgeCount, forKey: .totalNotification)
        }
        print_debug("Payload: \(aps)\n")
        print_debug(info)
        switch type {
        case "1","3":
            
            let candidateDetail = CandidateTabBarVC.instantiate(fromAppStoryboard: .Candidate)
            candidateDetail.fromPush = true
            if type == "1"{
                AppUserDefaults.save(value: 1, forKey: .shortlistNotification)
            }
            
            if let jobid = aps["job_id"] as? String{
                candidateDetail.job_id = jobid
            }
            if let sender_id = aps["sender_id"] as? String{
                candidateDetail.sender_id = sender_id
            }
            if let navigation = self.navigationController  {
                navigation.pushViewController(candidateDetail, animated: true)
                self.window?.makeKeyAndVisible()
            }
        case "2":
            let sceen = RecruiterTabBarVC.instantiate(fromAppStoryboard: .Recruiter)
            //sceen.fromPush = true
            sceen.isJobPush = true
            if let jobid = aps["job_id"] as? String{
                sceen.jobId = jobid
            }
            //            if let sender_id = aps["sender_id"] as? String{
            //                candidateDetail.sender_id = sender_id
            //            }
            if let navigation = self.navigationController  {
                navigation.pushViewController(sceen, animated: true)
                self.window?.makeKeyAndVisible()
            }
            
            
        default:
            return
        }
        
        completionHandler()
    }
 }
 
 extension AppDelegate{
    
    private func didFinishLaunchingWithOptionsMethod(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?){
        
        FirebaseApp.configure()
        Database.database().isPersistenceEnabled = true
        Database.database().reference().child(ChatEnum.User.root.rawValue).keepSynced(true)
        Fabric.with([Crashlytics.self])
        //         FirebasePushModule.activateForFirebase()
        
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        AWSController.setupAmazonS3(withPoolID: S3_PoolId)
        UIApplication.shared.statusBarStyle = .default
        IQKeyboardManager.shared.enable = true
        
        let accessToken = AppUserDefaults.value(forKey: .Accesstoken).stringValue
        
        if accessToken == "" {
            self.goSelectUser(from: .firstTime, type: nil, resetPasswordObj: nil)
            UIApplication.shared.applicationIconBadgeNumber =  0
        } else {
            self.checkUserType()
            
            if let userId = User.getUserModel().user_id, !userId.isEmpty{
                ChatHelper.setOnline(userId: userId, status: true)
                ChatHelper.updateUsrDetails()
            }
        }
        
        //For remote notification
        GMSServices.provideAPIKey(StringConstants.GoogleAPIKey)
        GMSPlacesClient.provideAPIKey(StringConstants.GoogleAPIKey)
        
        if #available(iOS 10.0, *) {
            callManager = CallManager()
            providerDelegate = ProviderDelegate(callManager: callManager as! CallManager)
        } else if let launchOptions = launchOptions {
            
            if let userInfo = launchOptions[.localNotification] as? JSONDictionary,
                let aps = userInfo["aps"] as? JSONDictionary {
                handleVoIPInfo(aps,fromCallKit: false, by: .accept)
            }
        } else if let aps = AppUserDefaults.value(forKey: .notificationUserInfo, fallBackValue: JSON.null).dictionaryObject {
            handleVoIPInfo(aps,fromCallKit: false, by: .accept)
            AppUserDefaults.removeValue(forKey: .notificationUserInfo)
        }
        
        
        socket.delegate = self
        //For remote notification
        //FirebaseHelper.setStatus(online: true)
        self.registerPushNotification(application)
        //        application.registerForRemoteNotifications()
        registerForPushNVoip()
        
    }
    
    
    private func registerUserNotifications() {
        
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options: [.badge, .sound, .alert], completionHandler: { (grant, error)  in
                
                if error == nil, grant {
                    DispatchQueue.main.async {[weak self] in
                        guard let strongSelf = self else{return}
                        strongSelf.registerUNNotificationCategory()
                        strongSelf.registerForPushNVoip()
                    }
                } else if error != nil {
                    //printlnDebug("error: \(unwrappedError.localizedDescription)")
                }
            })
        } else {
            registerUIUserNotificationCategory()
            self.registerForPushNVoip()
        }
    }
    
    
    fileprivate func handleVoIPInfo(_ userInfo: JSONDictionary?,fromCallKit:Bool, by action: CallAction) {
        
        //        guard let aps = userInfo?["aps"] as? JSONDictionary else {
        //            return
        //        }
        
        //        guard let timeInterval = aps["date"] as? Double,
        //            (timeInterval + 60) > Date().timeIntervalSince1970 else {
        //                return
        //        }
        let callerJSON = JSON(userInfo)
        
        guard let caller = Caller(json: callerJSON["data"]) else {
            
            if (userInfo?["type"] as? String) == "disconnect" {
                removeAllLocalNotifications()
                endAllCalls()
            }
            
            return
        }
        
        connectAndLogin(caller: caller, fromCallKit: fromCallKit, performing: action)
    }
    
    func moveToVideoChatScene(with caller: Caller,fromPush : Bool,fromCallKit: Bool) {
        let videoChatScene = RTCVideoChatVC.instantiate(fromAppStoryboard: .settingsAndChat)
        videoChatScene.caller = caller
        videoChatScene.isOpenFromCallKit = fromCallKit
        //videoChatScene.callType = .send
        videoChatScene.fromPush = fromPush
        let chatNavVC = UINavigationController(rootViewController: videoChatScene)
        chatNavVC.isNavigationBarHidden = false
        chatNavVC.modalPresentationStyle = .overCurrentContext
        
        chatNavVC.modalTransitionStyle = .flipHorizontal
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        CommonClass.delayWithSeconds(1.5) {
            UIApplication.shared.endIgnoringInteractionEvents()
            UIApplication.shared.keyWindow?.rootViewController?.present(chatNavVC, animated: true, completion: nil)
            
        }
    }
    
    func removeAllLocalNotifications() {
        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        } else {
            UIApplication.shared.cancelAllLocalNotifications()
        }
    }
    
    func goSelectUser(from: isFromDeepLinking, type: String?, resetPasswordObj: ResetPasswordVC?) {
        
        let mainViewController = SelectUserTypeVC.instantiate(fromAppStoryboard: .Main)
        let login = LoginVC.instantiate(fromAppStoryboard: .Main)
        let socialLogin = SocialLoginVC.instantiate(fromAppStoryboard: .Main)
        self.navigationController = UINavigationController(rootViewController: mainViewController)
        self.navigationController?.isNavigationBarHidden = true
        
        if from == .fromDepplinking {
            guard let usersType = type else { return }
            
            if usersType == UserType.candidate.rawValue {
                userType = .candidate
                CommonClass.delayWithSeconds(0.1, completion: {
                    commingFrom = .resetPassword
                    resetPasswordObj?.userType = type ?? ""
                    self.navigationController?.setViewControllers([mainViewController, socialLogin,login, resetPasswordObj!], animated: true)
                })
                
            } else {
                userType = .recuriter
                commingFrom = .resetPassword
                CommonClass.delayWithSeconds(0.1, completion: {
                    resetPasswordObj?.userType = type ?? ""
                    self.navigationController?.setViewControllers([mainViewController,login, resetPasswordObj!], animated: true)
                })
            }
        }
        
        sharedAppDelegate.window?.rootViewController = self.navigationController
        sharedAppDelegate.window?.makeKeyAndVisible()
        
    }
    
    func goToCandidateTabBarVC() {
        let mainViewController = CandidateTabBarVC.instantiate(fromAppStoryboard: .Candidate)
        self.navigationController?.pushViewController(mainViewController, animated: true)
        self.navigationController = UINavigationController(rootViewController: mainViewController)
        self.navigationController?.isNavigationBarHidden = true
        sharedAppDelegate.window?.rootViewController = self.navigationController
        sharedAppDelegate.window?.makeKeyAndVisible()
        
    }
    
    func goToRecruiterTabBarVC() {
        
        let mainViewController = RecruiterTabBarVC.instantiate(fromAppStoryboard: .Recruiter)
        //        self.navigationController?.pushViewController(mainViewController, animated: true)
        self.navigationController = UINavigationController(rootViewController: mainViewController)
        self.navigationController?.isNavigationBarHidden = true
        sharedAppDelegate.window?.rootViewController = self.navigationController
        sharedAppDelegate.window?.makeKeyAndVisible()
        
    }
    func checkUserType() {
        let userType = AppUserDefaults.value(forKey: .userType).stringValue
        
        if userType == UserType.candidate.rawValue {
            self.goToCandidateTabBarVC()
        } else if userType == UserType.recuriter.rawValue {
            
            let isOtpVerified = AppUserDefaults.value(forKey: .isOtpVerified)
            print_debug(isOtpVerified)
            if isOtpVerified == "1" {
                
                self.goToRecruiterTabBarVC()
            } else {
                
                let rctCompanyName = RecruiterCompanyNameVC.instantiate(fromAppStoryboard: .Main)
                CommonClass().gotoViewController(rctCompanyName)
            }
        }
    }
    
    func goToCandidateJobDetailSceen(jobId: String) {
        
        guard let navigation = self.navigationController else { return }
        
        let candidateDetail = CandidateJobDetailVC.instantiate(fromAppStoryboard: .Candidate)
        candidateDetail.commingFromScreen = .deepLink
        candidateDetail.jobId = jobId
        navigation.pushViewController(candidateDetail, animated: true)
    }
    
    func goToCandidateDetail(userId: String) {
        guard let navigation = self.navigationController else { return }
        let candidateDetailScene = RecruiterCandidateDetailVC.instantiate(fromAppStoryboard: .Recruiter)
        candidateDetailScene.userId = userId
        candidateDetailScene.fromProfile = false
        navigation.pushViewController(candidateDetailScene, animated: true)
    }
    private func registerForPushNVoip() {
        //register for voip notifications
        voipRegistry.delegate = self
        
        voipRegistry.desiredPushTypes = Set([PKPushType.voIP])
        registerPushNotification(UIApplication.shared)
        //        UIApplication.shared.registerForRemoteNotifications()
        
    }
    
    //Register push notification.
    private func registerPushNotification(_ application: UIApplication){
        
        // For iOS 10 display notification (sent via APNS)
        UNUserNotificationCenter.current().delegate = self
        
        // For iOS 10 data message (sent via FCM)
        Messaging.messaging().delegate = self
        UIApplication.shared.applicationIconBadgeNumber = AppUserDefaults.value(forKey: .totalNotification).intValue
        
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in
                DispatchQueue.main.async {
                    application.registerForRemoteNotifications()
                }
                
        })
    }
    
    @available(iOS 10.0, *)
    private func registerUNNotificationCategory() {
        
        let answerableCategory: UNNotificationCategory = {
            
            let acceptAction = UNNotificationAction(
                identifier: "ACCEPT_IDENTIFIER",
                title: "ACCEPT",
                options: [.foreground])
            
            let rejectAction = UNNotificationAction(
                identifier: "REJECT_IDENTIFIER",
                title: "REJECT",
                options: [.destructive])
            
            return UNNotificationCategory(identifier: "ANSWER_CATEGORY", actions: [acceptAction, rejectAction], intentIdentifiers: [], options: [.customDismissAction])
        }()
        
        UNUserNotificationCenter.current().setNotificationCategories([answerableCategory])
    }
    
    private func registerUIUserNotificationCategory() {
        let acceptAction = UIMutableUserNotificationAction()
        acceptAction.identifier = "ACCEPT_IDENTIFIER"
        acceptAction.title = "ACCEPT"
        acceptAction.isDestructive = false
        acceptAction.isAuthenticationRequired = false
        acceptAction.activationMode = .background
        
        let rejectAction = UIMutableUserNotificationAction()
        rejectAction.identifier = "REJECT_IDENTIFIER"
        rejectAction.title = "REJECT"
        rejectAction.isDestructive = true
        rejectAction.isAuthenticationRequired = false
        rejectAction.activationMode = .background
        
        let answerableCategory = UIMutableUserNotificationCategory()
        answerableCategory.identifier = "ANSWER_CATEGORY"
        answerableCategory.setActions([acceptAction, rejectAction], for: .default)
        
        let types: UIUserNotificationType = [.alert, .sound, .badge]
        let notificationSettings = UIUserNotificationSettings(types: types, categories: [answerableCategory])
        
        UIApplication.shared.registerUserNotificationSettings(notificationSettings)
    }
    
    func setupTimeoutTimer() {
        callTimeOutTimer = Timer.scheduledTimer(timeInterval: 180, target: self, selector: #selector(timeoutCall), userInfo: nil, repeats: false)
    }
    
    func invalidateTimeoutTimer() {
        callTimeOutTimer?.invalidate()
        callTimeOutTimer = nil
    }
    
    @objc private func timeoutCall() {
        endAllCalls()
    }
    
    private func updateApplication(){
        if let url = NSURL(string: "https://itunes.apple.com/in/app/jobget/id1436462309?mt=8") as URL?{
            if UIApplication.shared.canOpenURL(url) == true  {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
            
        }
    }
    
 }
 
 
