//
//  LocationController.swift
//  Onboarding
//
//  Created by Appinventiv on 22/08/16.
//  Copyright © 2016 Gurdeep Singh. All rights reserved.
//

import Foundation
import CoreLocation

let SharedLocationManager = LocationController.sharedLocationManager

class LocationController : NSObject, CLLocationManagerDelegate {
    
    static let sharedLocationManager = LocationController()
    
    fileprivate var locationUpdateCompletion : ((CLLocation)->Void)?
    
    fileprivate var deniedLocationPermissionCompletion : ((Error)->Void)?
    
    let locationManager = CLLocationManager()
    
    var currentLocation : CLLocation!
    
    var locationsEnabled : Bool {
        
        if( CLLocationManager.locationServicesEnabled() && CLLocationManager.authorizationStatus() != CLAuthorizationStatus.denied) {
                
                return true
        
        } else {

            return false
        }
    }
    
    fileprivate override init() {
        
        super.init()
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.activityType = CLActivityType.otherNavigation
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.distanceFilter = 500.0
    }
    
    func fetchCurrentLocation(_ completion : @escaping (CLLocation)->Void){
         
        self.locationUpdateCompletion = completion
        getCurrentLocation()
        
    }
    
    func deniedLocationPermission(_ completion : @escaping (Error)->Void) {
         self.deniedLocationPermissionCompletion = completion
    }
    
    fileprivate func getCurrentLocation() {
        
        if self.locationsEnabled {
            
            self.locationManager.startUpdatingLocation()
            
        } else {
            
            if CLLocationManager.locationServicesEnabled() || CLLocationManager.authorizationStatus() == CLAuthorizationStatus.denied {
                NotificationCenter.default.post(name: NSNotification.Name("DENY_CURRENT_LOCATION_PERMISSION"), object: nil)

            } else {
                self.locationManager.requestWhenInUseAuthorization()
            }
        }
    }
        
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
   
        if let _ = self.deniedLocationPermissionCompletion {
           self.deniedLocationPermissionCompletion?(error)
        }
        print_debug(error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        if let _ = self.locationUpdateCompletion {
            
            self.locationUpdateCompletion?(locations.last!)
        }
      
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch status {
        case .notDetermined:
            // If status has not yet been determied, ask for authorization
            manager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            // If authorized when in use
            locationManager.startUpdatingLocation()
            break
        case .authorizedAlways:
            // If always authorized
            locationManager.startUpdatingLocation()
            break
        case .restricted:
            // If restricted by e.g. parental controls. User can't enable Location Services
            break
        case .denied:
            // If user denied your app access to Location Services, but can grant access from Settings.app
            NotificationCenter.default.post(name: NSNotification.Name("DENY_CURRENT_LOCATION_PERMISSION"), object: nil)
            
            
        }
        //        guard status == .authorizedWhenInUse else {
        //            return
        //        }
        
        
        
    }
    
    func reverse(location: CLLocation) {
        
    }
}
