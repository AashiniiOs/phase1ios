//
//  AppColors.swift
//  DannApp
//
//  Created by Aakash Srivastav on 20/04/17.
//  Copyright © 2017 Appinventiv. All rights reserved.
//

import Foundation
import UIKit

struct AppColors {
    
    static let black26                  = #colorLiteral(red: 0.1019607843, green: 0.09019607843, blue: 0.09411764706, alpha: 1)
    static let black46                  = #colorLiteral(red: 0.1803921569, green: 0.1803921569, blue: 0.1803921569, alpha: 1)
    static let themeBlueColor           = #colorLiteral(red: 0.2745098039, green: 0.5294117647, blue: 0.8196078431, alpha: 1)
    static let buttonDisableBgColor     = #colorLiteral(red: 0.2745098039, green: 0.5294117647, blue: 0.8196078431, alpha: 0.3047945205)
    static let lightGray103             = #colorLiteral(red: 0.4039215686, green: 0.4039215686, blue: 0.4039215686, alpha: 0.7191245719)
    static let gray152                  = #colorLiteral(red: 0.5960784314, green: 0.5960784314, blue: 0.5960784314, alpha: 1)
    static let gray15                   = #colorLiteral(red: 0.5960784314, green: 0.5960784314, blue: 0.5960784314, alpha: 0.2) // 152 152 152 20%
    static let gray163                  = #colorLiteral(red: 0.6392156863, green: 0.6666666667, blue: 0.6941176471, alpha: 1)
    static let gray119                  = #colorLiteral(red: 0.4666666667, green: 0.4666666667, blue: 0.4666666667, alpha: 0.7) // 119 119 119 70%
    static let fbBlue                   = #colorLiteral(red: 0.231372549, green: 0.3490196078, blue: 0.5960784314, alpha: 1)
    static let gray_0_63_128_10         = #colorLiteral(red: 0, green: 0.2470588235, blue: 0.5019607843, alpha: 0.1)
    static let green_66_143_10          = #colorLiteral(red: 0.2470588235, green: 0.5607843137, blue: 0.03921568627, alpha: 1)
    static let gray227                  = #colorLiteral(red: 0.8901960784, green: 0.8901960784, blue: 0.8901960784, alpha: 1)
    static let lightGray                = UIColor.gray
    static let statusBarGray210         = #colorLiteral(red: 0.8235294118, green: 0.8235294118, blue: 0.8235294118, alpha: 1)
    static let white                    = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
    static let lightBlack               = #colorLiteral(red: 0.1803921569, green: 0.1803921569, blue: 0.1803921569, alpha: 0.75)
    static let cellCancel               = #colorLiteral(red: 0.8901960784, green: 0.8901960784, blue: 0.8901960784, alpha: 1)
    static let blakopacity15            = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.15)
    static let blakopacity50            = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5)
    static let darkGray                 = #colorLiteral(red: 0.3921568627, green: 0.3921568627, blue: 0.3921568627, alpha: 1)
    static let progressBarGray = UIColor(red: 211/255, green: 211/255, blue: 211/255, alpha: 1)
    static let appBlue = UIColor(red: 70/255, green: 135/255, blue: 209/255, alpha: 1)
    static let appGrey = UIColor(red: 152/255, green: 152/255, blue: 152/255, alpha: 1)
    static let appBlack = UIColor(red: 46/255, green: 46/255, blue: 46/255, alpha: 1)
    static let tableViewBgColor = UIColor(red: 248/255, green: 248/255, blue: 248/255, alpha: 1)
}
