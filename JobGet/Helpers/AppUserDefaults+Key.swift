//
//  AppUserDefaults+Key.swift
//  StarterProj
//
//  Created by MAC on 07/03/17.
//  Copyright © 2017 Gurdeep. All rights reserved.
//

import Foundation

extension AppUserDefaults {

    enum Key : String {
        
        case Accesstoken
        case tutorialDisplayed
        case userData
        case firstName
        case lastName
        case isOtpVerified
        case recruiterCreateProfile
        case userType
        case isProfileAdded
        case userId
        case userPhoneNoFormatWithCode
        case recruiterCompanyName
        case recruiterMobile
        case recruiterCountryCode
        case candidateSearchResult
        case recruiterCompanyAddress
        case voIPToken
        case notificationUserInfo
        case inviteUrl
        case totalNotification
        case shortlistNotification
        case facebookImage
        case completePrfileData
        case fcmToken
        case messageBadge
        case BrainTree_clientToken
        case stars
        case freeStars
        case userLat
        case userLong
        case isLogin
        case pauseMsg
        case isNotify
        case profileLat
        case profileLong
        case referralUrl
        case referralCode
        
        case filterLat
        case filterLong
    }
}
