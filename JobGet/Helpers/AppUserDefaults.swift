//
//  AppUserDefaults.swift
//  AppUserDefaults
//
//  Created by Gurdeep on 15/12/16.
//  Copyright © 2016 Gurdeep. All rights reserved.
//

import Foundation
import SwiftyJSON

enum AppUserDefaults { }

extension AppUserDefaults {
    
    static func value(forKey key: Key,
                      file : String = #file,
                      line : Int = #line,
                      function : String = #function) -> JSON {
        
        guard let value = UserDefaults.standard.object(forKey: key.rawValue) else {
            
            print("No Value Found in UserDefaults\nFile : \(file) \nLine Number : \(line) \nFunction : \(function)")
            
            return JSON.null
        }
        
        return JSON(value)
    }
    
    static func value<T>(forKey key: Key,
                      fallBackValue : T,
                      file : String = #file,
                      line : Int = #line,
                      function : String = #function) -> JSON {
        
        guard let value = UserDefaults.standard.object(forKey: key.rawValue) else {
            
            print("No Value Found in UserDefaults\nFile : \(file) \nFunction : \(function)")
            return JSON(fallBackValue)
        }
        
        return JSON(value)
    }
    
    static func getUserSearchResult(forKey key: Key,
                                    file : String = #file,
                                    line : Int = #line,
                                    function : String = #function) -> [String] {
        
        guard let value = UserDefaults.standard.object(forKey: key.rawValue) as? Array<String> else {
            
            print("No Value Found in UserDefaults\nFile : \(file) \nLine Number : \(line) \nFunction : \(function)")
            
            return []
        }
        return value //as? Array<String>
    }
    
    static func getData(forKey key: Key)-> String{
        guard let recovedUserJsonData = UserDefaults.standard.object(forKey: key.rawValue) as? Data else{return ""}
        guard let recovedUserJson = NSKeyedUnarchiver.unarchiveObject(with: recovedUserJsonData ) as? String else{return ""}
        
        return recovedUserJson
        
    }
    
    static func save(value : Any, forKey key : Key) {
        
        UserDefaults.standard.set(value, forKey: key.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    static func removeValue(forKey key : Key) {
        
        UserDefaults.standard.removeObject(forKey: key.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    static func removeAllValues() {
        
        let tutorial = AppUserDefaults.value(forKey: .tutorialDisplayed).stringValue
        
        let appDomain = Bundle.main.bundleIdentifier!
        UserDefaults.standard.removePersistentDomain(forName: appDomain)
        UserDefaults.standard.synchronize()
        UIApplication.shared.applicationIconBadgeNumber = 0
        AppUserDefaults.save(value: tutorial, forKey: .tutorialDisplayed)
    }
    
}





