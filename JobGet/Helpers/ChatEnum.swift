//
//  ChatEnum.swift
//  Shopoholic
//
//  Created by Appinventiv on 20/04/18.
//  Copyright © 2018 Gurdeep Singh. All rights reserved.
//

import Foundation


enum ChatEnum{
    
    enum Root: String{
        case inbox
        case roomInfo = "room_info"
        case lastMessage
        case chatLastMessage
        case block
        case product
        case starConsumed
        case unreadMessage
    }
    
    enum RoomType: String{
        case none
        case single
        case group
    }
    
    enum MessageType: String{
        
        case none
        case text
        case image
        case video
        case location
        case file
        case action
        case call
    }
    
    enum MessageStatus: String{
        
        case sent
        case read = "seen"
        case none
    }
    
    enum User: String{
        case root = "users"
        case firstName = "first_name"
        case lastName = "last_name"
        case email = "email"
        case userImage = "image"
        case countryCode = "country_code"
        case mobileNumber = "mobile"
        case deviceToken = "device_token"
        case userId = "user_id"
        case isOnline = "isOnline"
        case deviceType = "device_type"
        case isShortlisted = "isShortlisted"
    }
    
    enum Message: String{
        
        case root = "messages"
        case timestamp
        case type
        case message = "messageText"
        case messageId
        case latitude
        case longitude
        case mediaUrl
        case sender = "senderId"
        case roomId
        case isBlock
        case isDelete = "isDeleted"
        case status
        case thumbnail
        case receiverId
    }
    
    enum Room: String{
        case root = "rooms"
        case product
        case roomInfo = "room_info"
    }
    
    enum ChatType{
        
        case none
        case new
        case old
    }
    
}

enum RoomInfo: String{
    case chatLastUpdate = "chatLastUpdate"
    case chatRoomId = "chatRoomId"
    case chatRoomPic = "chatRoomPic"
    case chatRoomTitle = "chatRoomTitle"
    case chatRoomType = "chatRoomType"
    case memberLeave = "memberLeave"
    case memberDelete = "memberDelete"
    case memberJoin = "memberJoin"
    case chatRoomIsTyping = "chatRoomIsTyping"
    case chatLastUpdates = "chatLastUpdates"
    case chatRoomMembers = "chatRoomMembers"
    
}

enum ObserverKeys: String{
    
    case message
    case messageAdded
    case typing
    case roomInfo
    case leaveGroup
    case unreadCount
    case myUnreadCount
}

enum CameraButtonOption{
    
    case photos
    case videos
    case camera
}
