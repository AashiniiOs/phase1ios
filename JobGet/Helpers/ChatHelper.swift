//
//  ChatHelper.swift
//  Shopoholic
//
//  Created by Appinventiv on 20/04/18.
//  Copyright © 2018 Gurdeep Singh. All rights reserved.
//

import Foundation
import Firebase
import SwiftyJSON
import AVKit

let databaseReference = Database.database().reference()
let fireBasePassword = "Jobget12345"

enum ChatHelper{
    
    static func createProduct(roomId: String, details: [String:Any]){
        let roomRef = databaseReference.child(ChatEnum.Root.roomInfo.rawValue).child(roomId)
        let productRef = roomRef.child(ChatEnum.Room.product.rawValue)
        productRef.setValue(details)
    }
    
    /*static func getProductDetails(roomId: String, completion: @escaping (ProductDetails?)->()){
     let roomRef = databaseReference.child(ChatEnum.Root.roomInfo.rawValue).child(roomId)
     let productRef = roomRef.child(ChatEnum.Room.product.rawValue)
     
     productRef.observeSingleEvent(of: .value) { (snapshot) in
     if let value = snapshot.value{
     
     print_debug(value)
     if let data = try? JSON(value).rawData(){
     let productDetails = try? JSONDecoder().decode(ProductDetails.self, from: data)
     completion(productDetails)
     }else{
     completion(nil)
     }
     }
     }
     }*/
    
    static func getProductId(roomId: String)->String?{
        
        let idArr = roomId.split(separator: "_").map { (sub) -> String in
            return String(sub)
        }
        
        if idArr.count == 3{
            return idArr[1]
        }else{
            return nil
        }
        
    }
    
    /**
     Create user using the email and default password
     */
    
    static func updateUsrDetails(){
        
        let user  = User.getUserModel()
        guard let userId = user.user_id else { return }
        
        var userDetails: [String: Any] = [:]
        if let fcmTocken = Messaging.messaging().fcmToken{
            userDetails[ChatEnum.User.deviceToken.rawValue] = fcmTocken
        }
        //        userDetails[ChatEnum.User.countryCode.rawValue] = user.country_code
        //        userDetails[ChatEnum.User.deviceToken.rawValue] = AppUserDefaults.value(forKey: .fcmToken).stringValue
//        if let loginSession = user.user_login_session{
//            var deviceTokenCommaSeparated = ""
//            for currentUserSession in loginSession{
//                print_debug(currentUserSession.device_token)
//                if let  devicetoken = currentUserSession.fcm_token{
//                    if deviceTokenCommaSeparated.isEmpty{
//                        deviceTokenCommaSeparated = devicetoken
//                    }else{
//                        deviceTokenCommaSeparated = deviceTokenCommaSeparated + "," + devicetoken
//                    }
//                }
//            }
//            userDetails[ChatEnum.User.deviceToken.rawValue] = deviceTokenCommaSeparated
//        }

        userDetails[ChatEnum.User.email.rawValue] = user.email
        userDetails[ChatEnum.User.firstName.rawValue] = user.first_name
        userDetails[ChatEnum.User.lastName.rawValue] = user.last_name
        userDetails[ChatEnum.User.mobileNumber.rawValue] = user.phone
        userDetails[ChatEnum.User.userImage.rawValue] = user.imageUrl
        userDetails[ChatEnum.User.userId.rawValue] = userId
        userDetails[ChatEnum.User.isOnline.rawValue] = true  //false
        userDetails[ChatEnum.User.deviceType.rawValue] = Device_Type.iOS.rawValue
        ChatHelper.updateUserDetails(userId: userId, details: userDetails)
    }
    
    
    static func updateCandInfoCoresRec(candUserId: String, isShortListed: Bool, isReport: Bool, isStarDeduct:Bool){
        let user  = User.getUserModel()
        guard let userId = user.user_id else { return }
        var candInfo: JSONDictionary = [:]
        candInfo["isShortListed"] = isShortListed
        candInfo["isReport"] = isReport
        candInfo["isStarDeduct"] = isStarDeduct
        
        databaseReference.child("candidateInfo").child(userId).child(candUserId).updateChildValues(candInfo)
    }
    
    static func getCandInfoCoresRec(candUserId: String, completion: @escaping (JSONDictionary)->()){
        let user  = User.getUserModel()
        guard let userId = user.user_id else { return }
        databaseReference.child("candidateInfo").child(userId).child(candUserId).observeSingleEvent(of: .value) { (detail) in
            if let value = detail.value as? JSONDictionary{
                completion(value)
            }else{
                completion([:])
            }
        }
    }
    
    /**
     Create user using the email and default password
     */
    static func createUser(withEmail email: String, password: String = fireBasePassword, completion: @escaping (Bool)->()){
        
        print_debug(password)
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            
            if error == nil{
                print_debug(user?.user.uid)
                completion(true)
                ChatHelper.updateUsrDetails()
            }else{
                completion(false)
            }
        }
    }
    
    /**
     Update device token
     */
    
    static func updateDeviceToken(userId: String, deviceToken: String){
        
        let userRef = databaseReference.child(ChatEnum.User.root.rawValue).child(userId)
        userRef.child(ChatEnum.User.deviceToken.rawValue).setValue(deviceToken)
    }
    /**
     completion(device token and  device type)
     */
    static func getDeviceToken(userId: String, completion: @escaping (String,String)->()){
        
        self.getUserDetails(userId: userId) { (member) in
            
            if let member = member{
                completion(member.deviceToken ?? "",member.deviceType)
            }
        }
        
    }
    //    static func updateDeviceType(userId: String, deviceType: String){
    //
    //        let userRef = databaseReference.child(ChatEnum.User.root.rawValue).child(userId)
    //        userRef.child(ChatEnum.User.deviceType.rawValue).setValue(deviceType)
    //    }
    
    static func checkStatusAndSignIn(){
        
        if Auth.auth().currentUser == nil{
            
            self.signIn(withEmail: User.getUserModel().email, withUserId: User.getUserModel().user_id ?? "", completion: { (success) in
                if success{
                    print_debug("========================\n\n")
                    print_debug("User logged in firebase\n\n")
                    print_debug("========================")
                    if let userId = User.getUserModel().user_id, !userId.isEmpty{
                        self.setOnline(userId: userId, status: true)
                    }
                }else{
                    print_debug("========================\n\n")
                    print_debug("User cannot log in firebase\n\n")
                    print_debug("========================")
                }
            })
        }else if let userId = User.getUserModel().user_id, !userId.isEmpty{
            
            self.setOnline(userId: userId, status: true)
        }
    }
    
    /**
     SetOnline status
     */
    
    static func setOnline(userId: String, status: Bool){
        let userRef = databaseReference.child(ChatEnum.User.root.rawValue).child(userId)
        userRef.child(ChatEnum.User.isOnline.rawValue).setValue(status)
    }
    
    /**
     Login user to the firebase
     */
    static func signIn(withEmail email: String, withUserId userId: String, password: String = fireBasePassword, completion: @escaping (Bool)->()){
        
        var emaill = ""
        
        if email.isEmpty{
            emaill = userId + "@jobget.com"
        }else{
            emaill = email
        }
        
        Auth.auth().signIn(withEmail: emaill, password: password) { (user, error) in
            if error == nil{
                print_debug(user?.user.uid)
                completion(true)
            }else{
                ChatHelper.createUser(withEmail: emaill, completion: { (success) in
                    if success{
                        print_debug(user?.user.uid)
                        completion(true)

                        print_debug("========================\n\n")
                        print_debug("User created in firebase\n\n")
                        print_debug("========================")
                    }else{
                        print_debug(error?.localizedDescription)
                        completion(false)

                        print_debug("========================\n\n")
                        print_debug("User creation failed in firebase\n\n")
                        print_debug("========================")
                    }
                })
            }
        }
    }
    
    /**
     Logout user
     */
    @discardableResult
    static func logOutUser(userId: String)->Bool{
        CommonClass.removeAllNotifications()
        if self.logout(){
            if !userId.isEmpty{
                print_debug("Logout user_id : \(userId)")
                self.setOnline(userId: userId, status: false)
                self.updateDeviceToken(userId: userId, deviceToken: "")
            }
            return true
        }
        return false
    }
    
    static func facebookLogin(fbAccessToken token: String, completion: @escaping (Bool)->()){
        
        let cred = FacebookAuthProvider.credential(withAccessToken: token)
        
        Auth.auth().signIn(with: cred) { (user, error) in
            if error == nil{
                print_debug(user?.uid)
                completion(true)
                
            }else{
                print_debug(error?.localizedDescription)
                completion(false)
            }
        }
    }
    
    static func logout()->Bool{
        
        do{
            try Auth.auth().signOut()
            return true
        }catch let error{
            print_debug(error.localizedDescription)
            return false
        }
    }
    
    static func updateUserDetails(userId: String, details: [String: Any]){
        databaseReference.child(ChatEnum.User.root.rawValue).child(userId).setValue(details)
    }
    
    /**
     Get room id for a chatroom
     */
    
    static func getRoomId(forRoom roomId: String)->String{
        
        let roomRef = databaseReference.child(ChatEnum.Room.roomInfo.rawValue)
        return roomRef.childByAutoId().key
    }
    
    /**
     Get message id for a chatroom
     */
    
    static func getMessageId(forRoom roomId: String)->String{
        
        let messageRef = databaseReference.child(ChatEnum.Message.root.rawValue).child(roomId)
        return messageRef.childByAutoId().key
    }
    
    /**
     Get new room Id
     */
    static func getRoomId()->String{
        
        let roomId = databaseReference.child(ChatEnum.Room.root.rawValue).childByAutoId().key
        return roomId
    }
    
    static func getLocationUrl(_ latitude: Double, _ longitude: Double)->String{
        
        return "https://maps.googleapis.com/maps/api/staticmap?center=\(latitude),\(longitude)&zoom=12&size=200x180&markers=color:red%7Clabel:S%7C28.60558749999999,77.36282421874998&markers=size:tiny%7Ccolor:green%7CDelta+Junction,AK&markers=size:mid%7Ccolor:0xFFFF00%7Clabel:C%7CTok,AK&key=AIzaSyAZxKx6JwRrhBJFJy0Wp_V9ASrG-wGV_xo"
    }
}
// MARK:- User related details
extension ChatHelper{
    
    static func getUserDetails(userId: String, completion: @escaping (ChatMember?)->()){
        
        databaseReference.child(ChatEnum.User.root.rawValue).child(userId).observeSingleEvent(of: .value) { (snapshot) in
            
            if let value = snapshot.value{
                
                do{
                    let data = try JSON(value).rawData()
                    let user = try JSONDecoder().decode(ChatMember.self, from: data)
                    //print_debug(user)
                    completion(user)
                }catch let error{
                
                }
            }else{
                completion(nil)
                print_debug("Inbox message detail empty :2: \(userId)")
            }
        }
    }
    
    static func getChatRoomDetails(roomId: String, completion: @escaping (ChatRoom?)->()){
        
        databaseReference.child(ChatEnum.Root.roomInfo.rawValue).child(roomId).observeSingleEvent(of: .value) { (snapshot) in
            
            if let value = snapshot.value{
                
                print_debug(JSON(value))
                let room = ChatRoom(with: JSON(value))
                completion(room)
                
            }else{
                completion(nil)
            }
        }
    }
}

//    MARK:- Initial room setp
//    =================================
extension ChatHelper{
    
    static var timeStamp: [AnyHashable:Any]{
        return ServerValue.timestamp()
    }
    
    
    static func checkIfNewChat(productId: String, userId: String, completion: (Bool)->()){
        
        guard let curUserId = User.getUserModel().user_id else { return }
        
        let inboxRef = databaseReference.child(ChatEnum.Root.inbox.rawValue)
        inboxRef.child(curUserId).observeSingleEvent(of: .value) { (snapshot) in
            
        }
    }
    
    static func createInbox(forUserId userId: String, roomId: String, otherUserId: String){
        
        let inboxRef = databaseReference.child(ChatEnum.Root.inbox.rawValue)
        
        inboxRef.child(userId).updateChildValues([otherUserId:roomId])
        inboxRef.child(otherUserId).updateChildValues([userId:roomId])
    }
    
    /*
     create chat room for the user
     */
    static func createRoom(forUserId userId: String, curUserId: String, members: [ChatMember] )->ChatRoom{
        
        let roomId = ChatHelper.getRoomId()
        
        let chatroom = self.createRoom(roomId: roomId,
                                       curUserId: curUserId,
                                       members: members,
                                       chatRoomPic: "",
                                       chatRoomTitle: "",
                                       chatRoomType: .single)
        
        return chatroom
    }
    
    private static func createRoom(roomId: String, curUserId: String, members: [ChatMember], chatRoomPic: String, chatRoomTitle: String, chatRoomType: ChatEnum.RoomType)->ChatRoom{
        
        let roomRef = databaseReference.child(ChatEnum.Root.roomInfo.rawValue).child(roomId)
        
        var roomData: [String:Any] = [:]
        
        roomData[RoomInfo.chatLastUpdate.rawValue] = ServerValue.timestamp()
        roomData[RoomInfo.chatRoomId.rawValue] = roomId
        roomData[RoomInfo.chatRoomPic.rawValue] = chatRoomPic
        roomData[RoomInfo.chatRoomTitle.rawValue] = chatRoomTitle
        roomData[RoomInfo.chatRoomType.rawValue] = chatRoomType.rawValue
        
        var updates: [String: Any] = [:]
        var chatRoomMembers: [String:Any] = [:]
        var isTyping: [String:Any] = [:]
        
        for member in members{
            
            if member.userId == curUserId{
                updates[member.userId] = ChatHelper.timeStamp
                //chatRoomMembers.append(["":""])
            }else{
                updates[member.userId] = 0
            }
            
            chatRoomMembers[member.userId] = [RoomInfo.memberLeave.rawValue:0,
                                              RoomInfo.memberDelete.rawValue: ChatHelper.timeStamp,
                                              RoomInfo.memberJoin.rawValue: ChatHelper.timeStamp]
            
            isTyping[member.userId] = false
        }
        
        roomData[RoomInfo.chatRoomIsTyping.rawValue] = isTyping
        roomData[RoomInfo.chatLastUpdates.rawValue] = updates
        roomData[RoomInfo.chatRoomMembers.rawValue] = chatRoomMembers
        
        roomRef.setValue(roomData)
        let chatroom = ChatRoom(with: JSON(roomData))
        
        return chatroom
    }
    
    static func checkifNewChat(forUserId userId: String, curUserId: String, productId: String , completion: @escaping (String?)->()){
        
        let roomId = "\(curUserId)_\(productId)_\(userId)"
        let roomId2 = "\(userId)_\(productId)_\(curUserId)"
        let roomRef = databaseReference.child(ChatEnum.Root.roomInfo.rawValue)
        
        roomRef.observeSingleEvent(of: .value) { (snapshot) in
            
            if snapshot.hasChild(roomId){
                completion(roomId)
            } else if snapshot.hasChild(roomId2){
                completion(roomId2)
            }else{
                completion(nil)
            }
        }
    }
    
    static func checkifNewSingleChat(forUserId userId: String, curUserId: String , completion: @escaping (String?)->()){
        
        let inboxRef = databaseReference.child(ChatEnum.Root.inbox.rawValue)
        
        inboxRef.child(curUserId).observeSingleEvent(of: .value) { (snapshot) in
            if snapshot.hasChild(userId){
                let roomId = self.getExistingRoomId(key: userId, dict: snapshot.value as? [String:Any])
                completion(roomId)
            }else{
                
                inboxRef.child(userId).observeSingleEvent(of: .value) { (snapshot) in
                    if snapshot.hasChild(curUserId){
                        let roomId = self.getExistingRoomId(key: curUserId, dict: snapshot.value as? [String:Any])
                        completion(roomId)
                    }else{
                        completion(nil)
                    }
                }
            }
        }
    }
    
    private static func getExistingRoomId(key: String, dict: [String: Any]?)->String?{
        
        return dict?[key] as? String
        
    }
}
//MARK:- Sending messages
//======================================
extension ChatHelper{
    
    /**
     Create last message node
     */
    static func setLastMessage(_ message: ChatMessage){
        
        let lastMessageRef = databaseReference.child(ChatEnum.Root.lastMessage.rawValue)
        let lastMsgRoom = lastMessageRef.child(message.roomId).child(ChatEnum.Root.chatLastMessage.rawValue)
        lastMsgRoom.setValue(message.getMessageDictionary())
    }
    
    static func removeOtherUserLastMessage(_ message: ChatMessage){
        let lastMessageRef = databaseReference.child(ChatEnum.Root.lastMessage.rawValue)
        lastMessageRef.child(message.roomId).child(message.receiverId).removeValue()
    }
    
    /**
     Create message node
     */
    static func sendMessage(_ message: ChatMessage){
        
        let messageRef = databaseReference.child(ChatEnum.Message.root.rawValue).child(message.roomId).child(message.messageId)
        messageRef.setValue(message.getMessageDictionary())
    }
    
    /**
     Set last updates
     */
    
    static func setLastUpdates(forRoom roomId: String, userId: String){
        
        let roomRef = databaseReference.child(ChatEnum.Root.roomInfo.rawValue).child(roomId)
        
        // set chat last update
        roomRef.child("chatLastUpdate").setValue(self.timeStamp)
        
        // set sender time stamp
        self.setUserLastUpdate(forRoom: roomId, userId: userId)
    }
    
    static func setUserLastUpdate(forRoom roomId: String, userId: String){
        
        let roomRef = databaseReference.child(ChatEnum.Root.roomInfo.rawValue).child(roomId)
        // set sender time stamp
        roomRef.child("chatLastUpdates").updateChildValues([userId: self.timeStamp])
    }
    
    static func setUnreadMessageCount(userId: String, roomId: String, count: Int){
        
        let countRef = databaseReference.child(ChatEnum.Root.unreadMessage.rawValue).child(roomId)
        countRef.updateChildValues([userId:count])
    }
    
    static func getUnreadMessagesCount(userId: String, roomId: String, completion: @escaping (Int)->()){
        let countRef = databaseReference.child(ChatEnum.Root.unreadMessage.rawValue).child(roomId)
        countRef.child(userId).observeSingleEvent(of: .value) { (snapshot) in
            
            if let count = snapshot.value as? Int{
                completion(count)
            }else{
                completion(0)
            }
        }
    }
}

//MARK:- Compose action/text/video/image/location message
//========================================================
extension ChatHelper{
    
    // compose action message
    static func composeActionMessage(text message: String, sender: String, receiver: String, roomId: String, isBlock: Bool)->ChatMessage{
        
        let messageId: String = ChatHelper.getMessageId(forRoom: roomId)
        
        return ChatMessage(timestamp: 0,
                           type: .action,
                           message: message,
                           latitude: 0.0,
                           longitude: 0.0,
                           mediaUrl: "",
                           senderId: sender,
                           messageId: messageId,
                           status: .sent,
                           thumbnail: "",
                           isDeleted: false,
                           isBlock: isBlock,
                           roomId: roomId,
                           receiverId: receiver,
                           image: nil)
        
    }
    
    // compose text message
    
    static func composeTextMessage(text message: String, sender: String, receiver: String, roomId: String, isBlock: Bool, deviceToken: String, deviceType: String)->ChatMessage{
        
        let messageId: String = ChatHelper.getMessageId(forRoom: roomId)
        self.push(message: message, token: deviceToken,messageId: messageId, roomId: roomId, receiverId: receiver, deviceType: deviceType)
        return ChatMessage(timestamp: 0,
                           type: .text,
                           message: message,
                           latitude: 0.0,
                           longitude: 0.0,
                           mediaUrl: "",
                           senderId: sender,
                           messageId: messageId,
                           status: .sent,
                           thumbnail: "",
                           isDeleted: false,
                           isBlock: isBlock,
                           roomId: roomId,
                           receiverId: receiver,
                           image: nil)
        
    }
    // compose call message
    static func composeCallMessage(callTime message: String, sender: String, receiver: String, roomId: String, isBlock: Bool)->ChatMessage{
        
        let messageId: String = ChatHelper.getMessageId(forRoom: roomId)
        
        return ChatMessage(timestamp: 0,
                           type: .call,
                           message: message,
                           latitude: 0.0,
                           longitude: 0.0,
                           mediaUrl: "",
                           senderId: sender,
                           messageId: messageId,
                           status: .sent,
                           thumbnail: "",
                           isDeleted: false,
                           isBlock: isBlock,
                           roomId: roomId,
                           receiverId: receiver,
                           image: nil)
        
    }
    
    static func composeImageMessage(image: UIImage?, imageUrlString: String, sender: String, receiver: String, roomId: String, isBlock: Bool, timestamp: Double = Date().timeIntervalSince1970 * 1000)->ChatMessage{
        
        let messageId: String = ChatHelper.getMessageId(forRoom: roomId)
        
        return ChatMessage(timestamp: timestamp,
                           type: .image,
                           message: "Image",
                           latitude: 0.0,
                           longitude: 0.0,
                           mediaUrl: imageUrlString,
                           senderId: sender,
                           messageId: messageId,
                           status: .sent,
                           thumbnail: "",
                           isDeleted: false,
                           isBlock: isBlock,
                           roomId: roomId,
                           receiverId: receiver,
                           image: image)
    }
    
    static func composeImageMessage(with message: ChatMessage, imageUrlString: String)->ChatMessage{
        
        return ChatMessage(timestamp: Date().timeIntervalSince1970 * 1000,
                           type: .image,
                           message: "Image",
                           latitude: 0.0,
                           longitude: 0.0,
                           mediaUrl: imageUrlString,
                           senderId: message.senderId,
                           messageId: message.messageId,
                           status: .sent,
                           thumbnail: "",
                           isDeleted: false,
                           isBlock: message.isBlock,
                           roomId: message.roomId,
                           receiverId: message.receiverId,
                           image: nil)
    }
    
    static func composeVideoMessage(thumb: UIImage?, videoUrlString: String, sender: String, receiver: String, roomId: String, isBlock: Bool)->ChatMessage{
        
        let messageId: String = ChatHelper.getMessageId(forRoom: roomId)
        
        return ChatMessage(timestamp: Date().timeIntervalSince1970 * 1000,
                           type: .video,
                           message: "Video",
                           latitude: 0.0,
                           longitude: 0.0,
                           mediaUrl: "",
                           senderId: sender,
                           messageId: messageId,
                           status: .sent,
                           thumbnail: "",
                           isDeleted: false,
                           isBlock: isBlock,
                           roomId: roomId,
                           receiverId: receiver,
                           image: thumb)
    }
    
    static func composeVideoMessage(with message: ChatMessage, thumbnail: String, mediaUrl: String)->ChatMessage{
        
        return ChatMessage(timestamp: Date().timeIntervalSince1970 * 1000,
                           type: .video,
                           message: "Video",
                           latitude: 0.0,
                           longitude: 0.0,
                           mediaUrl: mediaUrl,
                           senderId: message.senderId,
                           messageId: message.messageId,
                           status: .sent,
                           thumbnail: thumbnail,
                           isDeleted: false,
                           isBlock: message.isBlock,
                           roomId: message.roomId,
                           receiverId: message.receiverId,
                           image: nil)
    }
    
    static func composeLocationMessage(latitude: Double, longitude: Double, sender: String, receiver: String, roomId: String, isBlock: Bool)->ChatMessage{
        
        let messageId: String = ChatHelper.getMessageId(forRoom: roomId)
        let locationUrl = self.getLocationUrl(latitude, longitude)
        
        return ChatMessage(timestamp: 0,
                           type: .location,
                           message: "Location",
                           latitude: latitude,
                           longitude: longitude,
                           mediaUrl: locationUrl,
                           senderId: sender,
                           messageId: messageId,
                           status: .sent,
                           thumbnail: "",
                           isDeleted: false,
                           isBlock: isBlock,
                           roomId: roomId,
                           receiverId: receiver,
                           image: nil)
    }
    
    static func composeDocumentMessage(docUrl: String, sender: String, receiver: String, roomId: String, isBlock: Bool)->ChatMessage{
        
        let messageId: String = ChatHelper.getMessageId(forRoom: roomId)
        
        return ChatMessage(timestamp: Date().timeIntervalSince1970 * 1000,
                           type: .file,
                           message: "File",
                           latitude: 0.0,
                           longitude: 0.0,
                           mediaUrl: docUrl,
                           senderId: sender,
                           messageId: messageId,
                           status: .sent,
                           thumbnail: "",
                           isDeleted: false,
                           isBlock: isBlock,
                           roomId: roomId,
                           receiverId: receiver,
                           image: nil)
    }
}

//MARK:- Leave group and block user
//===================================
extension ChatHelper{
    
    static func hasLeftGroup(chatRoom: ChatRoom, userId: String)->Bool{
        
        var hasleft: Bool = false
        
        let result = chatRoom.chatRoomMembers.filter({ (member) -> Bool in
            return member.userId == userId
        })
        
        for eachmem in result{
            if !eachmem.memberLeave.isZero{
                hasleft = true
            }
        }
        
        return hasleft
    }
    
    static func leaveGroup(roomId: String, userId: String, lastMessage text: String){
        
        let roomRef = databaseReference.child(ChatEnum.Root.roomInfo.rawValue).child(roomId)
        roomRef.child("chatRoomMembers").child(userId).updateChildValues(["memberLeave":ChatHelper.timeStamp])
        
        self.setLastMessageOnGroupLeave(roomId: roomId, userId: userId, lastMessage: text)
        DispatchQueue.delay(1) {
            self.setLastUpdates(forRoom: roomId, userId: userId)
        }
    }
    
    private static func setLastMessageOnGroupLeave(roomId: String, userId: String, lastMessage text: String){
        
        let message = self.composeActionMessage(text: text,
                                                sender: userId,
                                                receiver: roomId,
                                                roomId: roomId,
                                                isBlock: false)
        
        let lastMessageRef = databaseReference.child(ChatEnum.Root.lastMessage.rawValue)
        lastMessageRef.child(roomId).child(userId).setValue(message.getMessageDictionary())
        
        self.sendMessage(message)
        self.setLastMessage(message)
    }
    
    static func addNew(members: [ChatMember], roomId: String, userId: String, userName: String){
        
        let roomRef = databaseReference.child(ChatEnum.Root.roomInfo.rawValue).child(roomId)
        
        var chatLastUpdates: [String: Any] = [:]
        var chatRoomMembers: [String:Any] = [:]
        var isTyping: [String:Any] = [:]
        
        for member in members{
            
            chatLastUpdates[member.userId] = 0
            
            chatRoomMembers[member.userId] = ["memberLeave":0,
                                              "memberDelete": ChatHelper.timeStamp,
                                              "memberJoin": ChatHelper.timeStamp]
            
            isTyping[member.userId] = false
        }
        
        roomRef.child("chatLastUpdates").updateChildValues(chatLastUpdates)
        roomRef.child("chatRoomIsTyping").updateChildValues(isTyping)
        roomRef.child("chatRoomMembers").updateChildValues(chatRoomMembers)
        
        let lastMessageRef = databaseReference.child(ChatEnum.Root.lastMessage.rawValue).child(roomId)
        
        for member in members{
            
            let message = self.composeActionMessage(text: "\(userName) added \(member.firstName)",
                sender: userId,
                receiver: roomId,
                roomId: roomId,
                isBlock: false)
            
            self.sendMessage(message)
            self.setLastMessage(message)
            self.setLastUpdates(forRoom: roomId, userId: userId)
            
            lastMessageRef.child(member.userId).removeValue()
        }
        
    }
    
    static func blockUser(userToBlock userId: String, blockingUser b_userId: String, message: ChatMessage?){
        
        let blockRef = databaseReference.child(ChatEnum.Root.block.rawValue).child(b_userId)
        blockRef.updateChildValues([userId:ChatHelper.timeStamp])
        
        if let message = message{
            let lastMessageRef = databaseReference.child(ChatEnum.Root.lastMessage.rawValue)
            lastMessageRef.child(message.roomId).child(b_userId).child("chatLastMessage").setValue(message.getMessageDictionary())
        }
        
    }
    
    static func unBlockOtherUser(userToUnBlock userId: String, unBlockingUser b_userId: String, roomId: String){
        
        let blockRef = databaseReference.child(ChatEnum.Root.block.rawValue).child(b_userId).child(userId)
        
        blockRef.removeValue()
        
        //        let lastMessageRef = databaseReference.child(ChatEnum.Root.lastMessage.rawValue)
        //        lastMessageRef.child(roomId).child(b_userId).removeValue()
    }
}

//MARK:- GroupChat
//==========================
extension ChatHelper{
    
    static func createGroupChatRoom(members: [ChatMember], chatRoomPic: String, chatRoomTitle: String)->ChatRoom{
        
        let roomId = self.getRoomId()
        let curUserId = User.getUserModel().user_id!
        
        return self.createRoom(roomId: roomId,
                               curUserId: curUserId,
                               members: members,
                               chatRoomPic: chatRoomPic,
                               chatRoomTitle: chatRoomTitle,
                               chatRoomType: .group)
    }
    
    static func createGroupInbox(forMembers members: [ChatMember], roomId: String){
        
        let inboxRef = databaseReference.child(ChatEnum.Root.inbox.rawValue)
        
        for member in members{
            
            inboxRef.child(member.userId).updateChildValues([roomId:roomId])
        }
    }
}

//MARK:- Get Messages
//==========================
extension ChatHelper{
    
    static func getMessage(ofRoom chatRoom: ChatRoom, ofUser userId: String, completion: @escaping ([ChatMessage])->()){
        
        let memberUpdates = chatRoom.chatRoomMembers.filter { (member) -> Bool in
            return member.userId == userId
        }
        
        if let member = memberUpdates.first{
            
            let messageRef = databaseReference.child(ChatEnum.Message.root.rawValue).child(chatRoom.chatRoomId)
            messageRef.queryOrdered(byChild: "timestamp").queryStarting(atValue: member.memberDelete).queryEnding(atValue: member.memberLeave).queryLimited(toLast: 25).observeSingleEvent(of: .value, with: { (snapshot) in
                if let valueDict = snapshot.value as? [String:Any]{
                    
                    var messageArr: [ChatMessage] = []
                    
                    for (_,value) in valueDict{
                        
                        do{
                            
                            let data = try JSON(value).rawData()
                            let message = try JSONDecoder().decode(ChatMessage.self, from: data)
                            if !message.isDeleted{
                                messageArr.append(message)
                            }
                            
                        }catch let error{
                            print_debug(error)
                        }
                    }
                    let sortedMessage = messageArr.sorted(by: { (m1, m2) -> Bool in
                        return m1.timestamp < m2.timestamp
                    })
                    completion(sortedMessage)
                    
                }else{
                    completion([])
                }
            })
        }else{
            completion([])
        }
    }
    
    /**
     Get old chat messages in pagination
     
     - parameter chatRoomId: RoomId of chat room to get old messages
     - parameter lastKey: messageId of the first message in the array
     
     */
    static func getPaginatedData(_ roomId: String, _ lastKey: String, completion: @escaping ([ChatMessage])->()){
        
        let messageRef = databaseReference.child(ChatEnum.Message.root.rawValue).child(roomId)
        
        messageRef.queryOrderedByKey().queryEnding(atValue: lastKey).queryLimited(toLast: 25).observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let valueDict = snapshot.value as? [String: Any]{
                
                DispatchQueue.global(qos: .background).async {
                    
                    var messageArr: [ChatMessage] = []
                    
                    for (_,value) in valueDict{
                        
                        do{
                            
                            let data = try JSON(value).rawData()
                            let message = try JSONDecoder().decode(ChatMessage.self, from: data)
                            
                            if message.messageId != lastKey && !message.isDeleted{
                                messageArr.append(message)
                            }
                            
                        }catch let error{
                            print_debug(error)
                        }
                    }
                    let sortedMessage = messageArr.sorted(by: { (m1, m2) -> Bool in
                        return m1.timestamp < m2.timestamp
                    })
                    DispatchQueue.main.async {
                        completion(sortedMessage)
                    }
                }
                
            }else{
                completion([])
            }
        })
    }
}

extension ChatHelper{
    
    /**
     Compress video before uploading
     
     - parameter inputURL: Original video  url
     - parameter outputURL: Url after converting video
     - parameter handler: closure for handling export session
     
     */
    
    static func compressVideo(inputURL: URL, outputURL: URL, handler:@escaping (_ exportSession: AVAssetExportSession?)-> Void) {
        
        let urlAsset = AVURLAsset(url: inputURL, options: nil)
        
        guard let exportSession = AVAssetExportSession(asset: urlAsset, presetName: AVAssetExportPresetMediumQuality) else {
            handler(nil)
            
            return
        }
        
        exportSession.outputURL = outputURL
        exportSession.outputFileType = AVFileType.mp4
        exportSession.shouldOptimizeForNetworkUse = true
        exportSession.exportAsynchronously { () -> Void in
            handler(exportSession)
        }
    }
}

//MARK:- Get Images
//================================
extension ChatHelper{
    
    static func getImages(forRoom roomId: String, completion: @escaping ([ChatMessage])->()){
        let messageRef = databaseReference.child(ChatEnum.Message.root.rawValue).child(roomId)
        
        messageRef.queryOrdered(byChild: "type").queryEqual(toValue: "image").queryLimited(toLast: 25).observeSingleEvent(of: .value) { (snapshot) in
            
            if let valueDict = snapshot.value as? [String: Any]{
                
                DispatchQueue.global(qos: .background).async {
                    
                    var messageArr: [ChatMessage] = []
                    
                    for (_,value) in valueDict{
                        
                        do{
                            
                            let data = try JSON(value).rawData()
                            let message = try JSONDecoder().decode(ChatMessage.self, from: data)
                            
                            messageArr.append(message)
                            
                        }catch let error{
                            print_debug(error)
                        }
                    }
                    let sortedMessage = messageArr.sorted(by: { (m1, m2) -> Bool in
                        return m1.timestamp < m2.timestamp
                    })
                    DispatchQueue.main.async {
                        completion(sortedMessage)
                    }
                }
            }else{
                completion([])
            }
        }
    }
    
    /**
     Get old chat messages in pagination
     
     - parameter chatRoomId: RoomId of chat room to get old messages
     - parameter lastKey: messageId of the first message in the array
     
     */
    static func getPreviousImages(_ roomId: String, _ lastKey: String, completion: @escaping ([ChatMessage])->()){
        
        let messageRef = databaseReference.child(ChatEnum.Message.root.rawValue).child(roomId)
        
        messageRef.queryOrdered(byChild: "type").queryEqual(toValue: "image").queryEnding(atValue: lastKey).queryLimited(toLast: 25).observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let valueDict = snapshot.value as? [String: Any]{
                
                DispatchQueue.global(qos: .background).async {
                    
                    var messageArr: [ChatMessage] = []
                    
                    for (_,value) in valueDict{
                        
                        do{
                            
                            let data = try JSON(value).rawData()
                            let message = try JSONDecoder().decode(ChatMessage.self, from: data)
                            
                            if message.messageId != lastKey{
                                messageArr.append(message)
                            }
                            
                        }catch let error{
                            print_debug(error)
                        }
                    }
                    let sortedMessage = messageArr.sorted(by: { (m1, m2) -> Bool in
                        return m1.timestamp < m2.timestamp
                    })
                    DispatchQueue.main.async {
                        completion(sortedMessage)
                    }
                }
                
            }else{
                completion([])
            }
        })
    }
    
    /**
     Get old chat messages in pagination
     
     - parameter chatRoomId: RoomId of chat room to get old messages
     - parameter lastKey: messageId of the first message in the array
     
     */
    static func getNextImages(_ roomId: String, _ lastKey: String, completion: @escaping ([ChatMessage])->()){
        
        let messageRef = databaseReference.child(ChatEnum.Message.root.rawValue).child(roomId)
        
        messageRef.queryOrdered(byChild: "type").queryEqual(toValue: "image").queryLimited(toFirst: 25).observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let valueDict = snapshot.value as? [String: Any]{
                
                DispatchQueue.global(qos: .background).async {
                    
                    var messageArr: [ChatMessage] = []
                    
                    for (_,value) in valueDict{
                        
                        do{
                            
                            let data = try JSON(value).rawData()
                            let message = try JSONDecoder().decode(ChatMessage.self, from: data)
                            
                            if message.messageId != lastKey{
                                messageArr.append(message)
                            }
                            
                        }catch let error{
                            print_debug(error)
                        }
                    }
                    let sortedMessage = messageArr.sorted(by: { (m1, m2) -> Bool in
                        return m1.timestamp < m2.timestamp
                    })
                    DispatchQueue.main.async {
                        completion(sortedMessage)
                    }
                }
                
            }else{
                completion([])
            }
        })
    }
}

//MARK:- Set star consumed
//====================================
extension ChatHelper{
    
    static func consumeStar(forUser userId: String, currentUserId: String){
        
        let starRef = databaseReference.child(ChatEnum.Root.starConsumed.rawValue)
        starRef.child(currentUserId).child(userId).setValue(true)
        
    }
    
    static func getConsumedStarStatus(forUser userId: String, currentUserId: String, completion: @escaping (Bool)->()){
        
        let starRef = databaseReference.child(ChatEnum.Root.starConsumed.rawValue)
        starRef.child(currentUserId).child(userId).observe(.value) { (snapshot) in
            
            if let isConsumed = snapshot.value as? Bool{
                completion(isConsumed)
            }else{
                completion(false)
            }
        }
        
    }
}
//MARK:- Get price for the product
//====================================
extension ChatHelper{
    
    
    static func deleteMessage(messageId: String, roomId: String){
        
        databaseReference.child("Messages").child("roomId").child("messageId").updateChildValues(["isDeleted":true])
        
    }
    /* static func getPrice(forProduct product: ProductDetails)->String{
     
     var currency: String = ""
     
     if product.currency == "1"{
     currency = "$"
     }else if product.currency == "2"{
     currency = "₹"
     }else{
     currency = "S$"
     }
     
     return "\(currency) \(product.price)"
     }*/
}

//MARK:- Badge count maintenence
//====================================
extension ChatHelper{
    
    static func setBadgeCount(userId: String, count: Int){
        databaseReference.child("badgeCount").child(userId).setValue(count)
    }
    
    static func getBadgeCount(userId: String, completion: @escaping (Int)->()){
        databaseReference.child("badgeCount").child(userId).observeSingleEvent(of: .value) { (snapshot) in
            if let count = snapshot.value as? Int{
                completion(count)
            }
        }
    }
}
//MARK:- Call Messages
//====================================
extension ChatHelper{
    
    static func sendCallMessages(userId: String, timeString: String){
        
        let user = User.getUserModel()
        guard let currentUserId = user.user_id else { return }
        
        let currentMember: ChatMember = ChatMember(userId: currentUserId,
                                                   firstName: user.first_name,
                                                   lastName: user.last_name,
                                                   email: user.email,
                                                   userImage: user.imageUrl,
                                                   mobileNumber: user.phone,
                                                   deviceToken: "",
                                                   isOnline: true, deviceType: Device_Type.iOS.rawValue)
        
        ChatHelper.checkifNewSingleChat(forUserId: userId, curUserId: currentUserId) { (roomId) in
            
            if let roomId = roomId{
                // send message here as both users have chsatted earlier
                
                
                self.sendCallMessage(roomId: roomId,
                                     sender: currentUserId,
                                     receiver: userId,
                                     curMem: currentMember,
                                     timeString: timeString)
                
            }else{
                
                ChatHelper.getUserDetails(userId: userId, completion: { (member) in
                    if let member = member{
                        let room = ChatHelper.createRoom(forUserId: userId,
                                                         curUserId: currentUserId,
                                                         members: [currentMember,member])
                        
                        self.sendCallMessage(roomId: room.chatRoomId,
                                             sender: currentUserId,
                                             receiver: userId,
                                             curMem: currentMember,
                                             timeString: timeString)
                    }else{
                        
                    }
                })
                
            }
        }
    }
    
    private static func sendCallMessage(roomId: String, sender: String, receiver: String, curMem: ChatMember, timeString: String){
        
        let message = ChatHelper.composeCallMessage(callTime: timeString,
                                                    sender: sender,
                                                    receiver: receiver,
                                                    roomId: roomId,
                                                    isBlock: false)
        
        var unreadCount: Int?
        
        ChatHelper.setLastMessage(message)
        ChatHelper.sendMessage(message)
        ChatHelper.setLastUpdates(forRoom: message.roomId,
                                  userId: message.senderId)
        
        ChatHelper.getUnreadMessagesCount(userId: message.receiverId, roomId: message.roomId) { (count) in
            
            
            unreadCount = count
            //            ChatHelper.setUnreadMessageCount(userId: message.receiverId,
            //                                             roomId: message.roomId,
            //                                             count: count + 1)
        }
        
        ChatHelper.getBadgeCount(userId: message.receiverId) { (count) in
            //            ChatHelper.setBadgeCount(userId: message.receiverId,
            //                                     count: count + 1)
            let unreadRef = "/\(ChatEnum.Root.unreadMessage.rawValue)/\(message.roomId)/\(message.receiverId)"
            let badgeCount = "/badgeCount/\(message.receiverId)"
            let dict : JSONDictionary = ["\(unreadRef)":(unreadCount ?? 0)+1, "\(badgeCount)": count+1]
            databaseReference.updateChildValues(dict)
        }
    }
    
    
    static func push(message: String, token: String, messageId: String, roomId: String, receiverId: String, deviceType: String) {
        var  device_token = ""
        var device_Type = ""
        let userData = User(json: (AppUserDefaults.value(forKey: .userData)))
        
        let sender: JSONDictionary = ["user_id":userData.user_id ?? "","first_name":userData.first_name,"last_name":userData.last_name,"email":userData.email,"image":userData.imageUrl,"device_token":sharedAppDelegate.fcmToken ?? "","mobile":userData.phone,"device_type":Device_Type.iOS.rawValue,"isOnline":true]
        guard let dic = self.convertToJsonFormat(sender)?.components(separatedBy: .whitespacesAndNewlines) else{return}
//        if !token.isEmpty{
            var request = URLRequest(url: URL(string: "https://fcm.googleapis.com/fcm/send")!)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.setValue(StringConstants.FirebasePushKey, forHTTPHeaderField: "Authorization")
            request.setValue("Cache-Control", forHTTPHeaderField: "no-cache")
            
            var json = [String : Any] ()
            
            self.getDeviceToken(userId: receiverId) { (token, deviceType) in
                device_token = token
                device_Type = deviceType
                json["to"] = device_token
                json["priority"] = "high"
                    self.getBadgeCount(userId: receiverId, completion: { (count) in
                    json["data"] = [
                        "body" : [
                            "message":message,
                            "messageId":messageId,
                            "messageText":"\(userData.first_name) \(userData.last_name): \(message)",
                            "roomId":roomId,
                            "roomName":"",
                            "roomType":"single",
                            "type":"chat",
                            "sound" : "default",
                            "badge" : count + 1,
                            "device_type":Device_Type.iOS.rawValue,
                            "sender": sender]
                        ]
                        json["notification"] = [
                            "body" : "\(userData.first_name) \(userData.last_name): \(message)",
                            "messageId":messageId,
                            "messageText":message,
                            "roomId":roomId,
                            "roomName":"",
                            "roomType":"single",
                            "type":"chat",
                            "sound" : "default",
                            "badge" : count + 1,
                            "device_type":Device_Type.iOS.rawValue,
                            "sender": (dic.joined().replace(string: "\\", withString: "")) as Any
                        ]
                        do {
                            let jsonData = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
                            request.httpBody = jsonData
                            let task = URLSession.shared.dataTask(with: request) { data, response, error in
                                guard let data = data, error == nil else {
                                    return
                                }
                                if let httpStatus = response as? HTTPURLResponse, httpStatus.statusCode != 200 {
                                    print_debug("Status Code should be 200, but is \(httpStatus.statusCode)")
                                }
                                let responseString = String(data: data, encoding: .utf8)
                            }
                            task.resume()
                        }
                        catch {
                            print_debug(error)
                        }
                    })
                print_debug(device_token)
                print_debug(device_Type)
                print_debug(json)
            }
    }
    
    
    static func convertToJsonFormat(_ array : [String : Any]) -> String?{
        do {
            //Convert to Data
            let jsonData = try JSONSerialization.data(withJSONObject: array, options: JSONSerialization.WritingOptions.prettyPrinted)
            //Convert back to string. Usually only do this for debugging
            return String(data: jsonData, encoding: String.Encoding.utf8)
        } catch {
            print_debug(error.localizedDescription)
        }
        return nil
    }
}
