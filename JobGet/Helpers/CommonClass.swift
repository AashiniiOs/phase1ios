//
//  CommonClass.swift
//  Onboarding
//
//  Created by Anuj on 9/15/16.
//  Copyright © 2016 Gurdeep Singh. All rights reserved.
//

import UIKit
import Toaster
import SwiftyJSON
import AVFoundation
import UserNotifications

struct LocalCategoryList {
    
}

final class CommonClass {
    
    
    static func commonCategoryList(_ categoryName: String, isSelected: Bool) -> UIImage{
        
        switch categoryName {
        case "Food (Others)":
            if isSelected{
                 return #imageLiteral(resourceName: "ic_select_food_")
            }else{
                 return #imageLiteral(resourceName: "ic_deselect_food_")
            }
            
        case "Retail":
            if isSelected{
                return #imageLiteral(resourceName: "ic_select_retail_")
            }else{
                return #imageLiteral(resourceName: "ic_deselect_retail_")
            }
            
        case "Sales":
            if isSelected{
                return #imageLiteral(resourceName: "ic_select_sales_")
            }else{
                return #imageLiteral(resourceName: "ic_deselect_sales_")
            }
            
        case "Customer Service":
            if isSelected{
                return #imageLiteral(resourceName: "ic_select_customer_service_")
            }else{
                return #imageLiteral(resourceName: "ic_deselect_customer_service_")
            }
            
        case "Personal Care":
            if isSelected{
                return #imageLiteral(resourceName: "ic_select_beauty_")
            }else{
                return #imageLiteral(resourceName: "ic_deselect_beauty_")
            }
            
        case "Delivery & Transport":
            if isSelected{
                return #imageLiteral(resourceName: "ic_select_delivery _")
            }else{
                return #imageLiteral(resourceName: "ic_deselect_delivery _")
            }
            
        case "Chef & Cook":
            if isSelected{
                return #imageLiteral(resourceName: "ic_chef_&_cook_active")
            }else{
                return #imageLiteral(resourceName: "ic_chef_&_cook_deactive")
            }
            
        case "Cleaning & Maintenance":
            if isSelected{
                return #imageLiteral(resourceName: "ic_cleaning_and_maintenance_active")
            }else{
                return #imageLiteral(resourceName: "ic_cleaning_and_maintenance_deactive")
            }
            
        case "Hotel & Leisure":
            if isSelected{
                return #imageLiteral(resourceName: "ic_hotels_and_hospitality_active")
            }else{
                return #imageLiteral(resourceName: "ic_hotels_and_hospitality_deactive")
            }
            
        case "Host & Server":
            if isSelected{
                return #imageLiteral(resourceName: "ic_host_and_server_active")
            }else{
                return #imageLiteral(resourceName: "ic_host_and_server_deactive")
            }
            
        case "Barista & Bartender":
            if isSelected{
                return #imageLiteral(resourceName: "ic_barista_and_bartenders_active")
            }else{
                return #imageLiteral(resourceName: "ic_barista_and_bartenders_deactive")
            }
            
        default:
            return #imageLiteral(resourceName: "icHomeCategoryPlaceholder1")
            
        }
    
    }
    
    static func catagoryList(_ categoryName: String) -> String{
        
        switch categoryName {
            
        case "Food (Others)":
            return "Food (Others)"
            
        case "Retail":
            return "Retail"
            
        case "Sales":
            return "Sales"
            
        case "Customer Service":
            return "Customer Service"
            
        case "Personal Care":
            return "Personal Care"
            
        case "Delivery & Transport":
            return "Delivery & Transport"
            
        case "Chef & Cook":
            return "Chef & Cook"
            
        case "Cleaning & Maintenance":
            return "Cleaning & Maintenance"
            
        case "Hotel & Leisure":
            return  "Hotel & Leisure"
            
        case "Host & Server":
            return "Host & Server"
          
        case "Barista & Bartender":
            return "Barista & Bartender"
            
        default:
            return ""
            
            //        case "Cleaning & Maintenance":
            //            if isSelected{
            //                return maintana
            //            }else{
            //                return #imageLiteral(resourceName: "icHomeJob")
            //            }
            
        }
        
    }
    
   // static let categoryListIcon = []
    static var dateFormatter : DateFormatter {
        let dateFormatterr = DateFormatter()
        dateFormatterr.timeZone = TimeZone.current
        return dateFormatterr
    }
    
    static func showToast(msg: String, _ completion : (()->())? = nil) {
        
        UIApplication.shared.keyWindow?.makeToast(message: msg, duration: 2, position: "bottom" as AnyObject)
//        if let currentToast = ToastCenter.default.currentToast {
//            currentToast.cancel()
//        }
//        let toast = Toast(text: msg)
//
//        toast.show()
    }
    
    static func showWithDelay(msg: String, _ completion : (()->())? = nil){
        
        UIApplication.shared.keyWindow?.makeToast(message: msg, duration: 5.5, position: "bottom" as AnyObject)
    }
    
    static func showToastWithDelay(msg: String, _ completion : (()->())? = nil){
        if let currentToast = ToastCenter.default.currentToast {
            currentToast.cancel()
        }
        let toast = Toast(text: msg, delay: 0.0, duration: 5.0)
        
        toast.show()
    }
    
    //MARK:- dealey Block
    //=======================
    static func delayWithSeconds(_ seconds: Double, completion: @escaping () -> ()) {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + seconds) {
            completion()
        }
    }
    
    // et bands = ["Coldplay", "Nirv
    
    func gotoSelectUserVC() {
        let sceen = SelectUserTypeVC.instantiate(fromAppStoryboard: .Main)
        self.gotoViewController(sceen)
    }
    
    func goToLogin() {
        let sceen = SelectUserTypeVC.instantiate(fromAppStoryboard: .Main)
        self.gotoViewController(sceen)
    }
    
  func gotoViewController(_ vc : BaseVC) {
        let nvc = UINavigationController(rootViewController: vc)
        nvc.isNavigationBarHidden = true
        sharedAppDelegate.window?.rootViewController = nvc
        sharedAppDelegate.window?.makeKeyAndVisible()
        
    }
    
   static func setActiveLabelTextField(textField: FloatingTextField, placeholder: String, title: String) {
        
        textField.placeholder = placeholder
        textField.title =  title        // "         Email Address"//StringConstants.Email_Address.localized
        textField.lineHeight = 0.5 // bottom line height in points
        textField.selectedLineHeight = 1.5
        textField.lineColor = AppColors.blakopacity15
        textField.selectedTitleColor = AppColors.gray152
        textField.selectedLineColor = AppColors.themeBlueColor
        textField.placeholderFont = AppFonts.Poppins_Regular.withSize(16)
        textField.titleFont = AppFonts.Poppins_Regular.withSize(12)
        textField.font = AppFonts.Poppins_Medium.withSize(16)
        textField.textColor = AppColors.black46
        
    }
    
   static func setSkyFloatingLabelTextField(textField: SkyFloatingLabelTextField, placeholder: String, title: String) {
        
        textField.placeholder = placeholder
        textField.title =  title        // "         Email Address"//StringConstants.Email_Address.localized
        textField.lineHeight = 1.5 // bottom line height in points
        textField.selectedLineHeight = 1.5
        textField.lineColor = AppColors.blakopacity15
        textField.selectedTitleColor = AppColors.gray152
        textField.selectedLineColor = AppColors.themeBlueColor
        textField.placeholderFont = AppFonts.Poppins_Regular.withSize(14)
        textField.titleFont = AppFonts.Poppins_Regular.withSize(12)
        textField.font = AppFonts.Poppins_Medium.withSize(15)
        textField.textColor = AppColors.black46
    
    }
    
    static func setSkyFloatingLocationTextField(textField: SkyFloatingLocationTextField, placeholder: String, title: String) {
        
        textField.placeholder = placeholder
        textField.title =  title        // "         Email Address"//StringConstants.Email_Address.localized
        textField.lineHeight = 1.5 // bottom line height in points
        textField.selectedLineHeight = 1.5
        textField.lineColor = AppColors.blakopacity15
        textField.selectedTitleColor = AppColors.gray152
        textField.selectedLineColor = AppColors.themeBlueColor
        textField.placeholderFont = AppFonts.Poppins_Regular.withSize(14)
        textField.titleFont = AppFonts.Poppins_Regular.withSize(12)
        textField.font = AppFonts.Poppins_Medium.withSize(15)
        textField.textColor = AppColors.black46
        
    }
    
    static func setTLFoatTextView(textView: TLFloatLabelTextView, title: String) {
        textView.hint   = title

        textView.titleTextColour = AppColors.gray152

        textView.titleActiveTextColour = AppColors.gray152
        textView.titleBackgroundColor = AppColors.white
        textView.titleFont = AppFonts.Poppins_Regular.withSize(12) // change when clent review
        textView.titleActiveFont = AppFonts.Poppins_Medium.withSize(20) // when not select textView or place holder
        textView.bottomLineColour = AppColors.blakopacity15
     
         textView.textColor = AppColors.black46
        textView.bottomLineActiveColour = AppColors.themeBlueColor
    }
    
    static func setSkyFloatingTextFieldWithIcon(textField: SkyFloatingLabelTextFieldWithIcon, placeholder: String, title: String) {
        
        textField.placeholder = placeholder
        textField.title =  title        // "         Email Address"//StringConstants.Email_Address.localized
        textField.lineHeight = 1.5 // bottom line height in points
        textField.selectedLineHeight = 1.5
        textField.lineColor = AppColors.blakopacity15
        textField.selectedTitleColor = AppColors.gray152
        textField.selectedLineColor = AppColors.themeBlueColor
        textField.placeholderFont = AppFonts.Poppins_Regular.withSize(16)
        textField.titleFont = AppFonts.Poppins_Regular.withSize(12)
        textField.font = AppFonts.Poppins_Medium.withSize(16)
        textField.textColor = AppColors.black46
        
    }
    static func setButtonAndShadowView(button: UIButton, shadowView: UIView) {
    button.backgroundColor = AppColors.themeBlueColor
        button.setCorner(cornerRadius: 5, clip: true)
        
        
        shadowView.height = 1.0
        shadowView.dropShadow(color: AppColors.blakopacity50, opacity: 0.4, offSet: CGSize(width: 0, height: 1.5), radius: 1.0)
        shadowView.backgroundColor =  AppColors.blakopacity50
    }
    
    static func setNextButton(button: UIButton) {
        
        button.backgroundColor = AppColors.themeBlueColor
        button.setCorner(cornerRadius: 5, clip: true)
    }
    

    // job type will be FULL TIME, PART TIME, BOTH
    static func getJobType(jobType: Int) -> String {
        
        if jobType == 1 {
            return "Full Time"
            
        } else if jobType == 2 {
            return "Part Time"
            
        } else if jobType == 3 {
            return  "Part/Full Time"
        } else {
            return "Part/Full Time"
        }
    }
    
    //append dollar ($) in front of salary label
    static func appendDollarInSalaryLabel(fromSalary: String, toSalary: String) -> String {
        
        if fromSalary != "", toSalary != "" {
            
            let salaryText =  "$" + "\(fromSalary)" + " - " + "$" + "\(toSalary)"
            return salaryText
            
        } else if fromSalary != "" {
            
            let salaryText =  "$" + "\(fromSalary)"
            return salaryText
        } else if toSalary != "" {
            
            let salaryText =  "$" + "\(toSalary)"
            return salaryText
        } else {
            return "Salary not disclosed"
        }
    }
    func convertToApiFormat(_ array : [[String : Any]]) -> String?{
        
        do {
            //Convert to Data
            let jsonData = try JSONSerialization.data(withJSONObject: array, options: JSONSerialization.WritingOptions.prettyPrinted)
            
            //Convert back to string. Usually only do this for debugging
            return String(data: jsonData, encoding: String.Encoding.utf8)
            
        } catch {
            print(error.localizedDescription)
        }
        return nil
    }
    
//    static func cropperFrame() -> CGRect {
//    
//     return  CGRect(x: 50, y: self.view.center.y, width: self.view.frame.width - 100, height: 180)
//    }
    
    static func getExperience(pos: Float) -> Float {
        
        if(pos < 15) {
            return 0
        } else if(pos < 30) {
            return 1
            
        } else if(pos < 45) {
            return 2
            
        } else if(pos < 60) {
            return 3
            
        } else if(pos < 75) {
            return 4
            
        } else if(pos < 90) {
            return 5
            
        } else if(pos < 105) {
            return 6
            
        } else if(pos < 120) {
            return 7
            
        } else if(pos < 135) {
            return 8
            
        } else if(pos < 150) {
            return 9
            
        } else if(pos < 165) {
            return 10
            
        } else if(pos < 180) {
            return 11
            
        } else if(pos < 225) {
            return 1
            
        } else if(pos < 247.5) {
            return 1.5
            
        }
        else if(pos < 270) {
            return 2
            
        } else if(pos < 292.5) {
            return 2.5
            
        }
            //        else if(pos < 315) {
            //            return 3
            //
            //        }
        else if(pos < 337.5) {
            return 3
            
        }
        else if(pos < 355) {
            return 4
            
        }
            //        else if(pos < 360) {
            //            return 4.5
            //
            //        }
        else if(pos < 382.5) {
            return 5
            
        }
        else {
            return 6
            
        }
    }
    
    static func getExperienceAngle(experience: Float, experienceType: Int, fromTo: Int) -> Int {
        
        if(experienceType == 1) {   //------- 1 => month, 2=> year
            switch (experience) {
            case 0:
                if(fromTo == 0) {
                    return 0;
                } else {
                    return 10;
                }
                
            case 1:
                return 15;
                
            case 2:
                return 30;
                
            case 3:
                return 45;
                
            case 4:
                return 60;
                
            case 5:
                return 75;
                
            case 6:
                return 90;
                
            case 7:
                return 105;
                
            case 8:
                return 120;
                
            case 9:
                return 135;
                
            case 10:
                return 150;
                
            case 11:
                return 165;
            default:
                return 5
                
            }
        } else {
            switch (experience) {
            case 1:
                return 200
                
            case 1.5:
                return 225
            case 2:
                return 250;
                
            case 2.5:
                return 275
                
            case 3:
                return 305
                
                //            case 3.5:
                //                return 300;
                
                //            case 4:
                //                return 325;
                
            case 4:
                return 350;
                
            case 5:
                return 375;
            default:
                return 400;
            }
        }
    }
    
    private static func convertToData(_ json: JSONDictionary) -> (Data?, Error?) {
        do {
            let data = try JSONSerialization.data(withJSONObject: json, options: .prettyPrinted)
            return (data, nil)
        } catch {
            return (nil, error)
        }
    }
    
    static func writeToSocket(_ json: JSONDictionary) {
        DispatchQueue.main.async {
            let (data, error) = convertToData(json)
            if let unwrappedData = data {
                sharedAppDelegate.socket.write(data: unwrappedData)
            } else if let e = error {
                print_debug(e.localizedDescription)
            } else {
                
                print_debug("Not found.")
            }
        }
    }
    
    static func sortedInHeighestExperience(_ experienceData: [JSON])-> [JSON]{
        
        
        var convertInMonth = experienceData.map { (value) -> JSON in
            var duration = value
            if value["duration"].stringValue == "5+" {
                duration["duration"] = JSON(6*12)
                return duration
            } else if value["durationType"].intValue  == 2{
                duration["duration"] = JSON(value["duration"].floatValue*12)
                return duration
            }else{
                duration["duration"] = JSON(value["duration"].intValue)
                return duration
            }
        }
        print_debug(convertInMonth)
        
        convertInMonth = convertInMonth.sorted {
            //            print_debug("\($0["duration"].intValue), \($1["duration"].intValue)")
            //            return ($0["duration"].intValue) > ( $1["duration"].intValue)
            print_debug("\($0["duration"].floatValue), \($1["duration"].floatValue)")
            return ($0["duration"].floatValue) > ( $1["duration"].floatValue)
        }
        
        let convertInYear = convertInMonth.map { (value) -> JSON in
            var duration = value
            
            if duration["duration"] > 11 {
                duration["duration"] = JSON(duration["duration"].floatValue/12)
                return duration
            } else {
                return duration
            }
            
        }
        
        
        print_debug(convertInMonth)
        
        print_debug(convertInYear)
        return convertInYear
        
    }
    
   static func getAttributedText(wholeString : String, attributedString : String, attributedColor : UIColor, normalColor: UIColor, fontSize : CGFloat)-> NSAttributedString {
        
        let attributedText = NSMutableAttributedString(string: wholeString)
        
        attributedText.addAttributes([NSAttributedStringKey.foregroundColor:normalColor, NSAttributedStringKey.font: AppFonts.Poppins_Medium.withSize(fontSize)], range: (wholeString as NSString).range(of: wholeString))
        
        attributedText.addAttributes([NSAttributedStringKey.foregroundColor:attributedColor, NSAttributedStringKey.font: AppFonts.Poppins_Medium.withSize(fontSize)], range: (wholeString as NSString).range(of: attributedString))
        
        return attributedText
    }
    
    static func calculateTimeDifference(to dateTime2: String) -> Int {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        
//        let dateAsString = dateTime1
//        guard let date1 = dateFormatter.date(from: dateAsString) else{return 0}
        
        let dateAsString2 = dateTime2
        guard let date2 = dateFormatter.date(from: dateAsString2) else{return 0}
        
//        let components : NSCalendar.Unit = [.second, .minute, .hour, .day, .weekOfMonth, .month, .year]
//        let difference = (Calendar.current as NSCalendar).components(components, from: date1, to: date2, options: [])
        
        let diff = date2.secondsFrom(Date())
        
//        var dateTimeDifferenceString = ""
//
//        if difference.day != 0 {
//            dateTimeDifferenceString = "\(difference.day!)d \(difference.hour!)h \(difference.minute!)m \(difference.second!)s"
//        } else if  difference.day == 0 {
//            dateTimeDifferenceString = "\(difference.hour!)h \(difference.minute!)m \(difference.second!)s"
//        }
        
        return diff
        
    }

   static func getAbbrevitedState(state: String) -> String {
        var address = ""
        getAbbreatedState { (stateList) in
            
            //            print_debug(stateList)
            _ = stateList.map{ value in
                
                if state.contains(s: value["state_name"] as? String ?? ""){
                    if let abState = value["state_abbreviations"] {
                       address.append(String(describing: "\(abState)"))
                    }
                }
                
            }
        }
        return address
    }
    
    static func calculateDistanceBtw(from lat : Double,long : Double) -> String{
        let targetloc = CLLocation(latitude: lat, longitude: long)
        var sourceloc = CLLocation()
        if SharedLocationManager.locationsEnabled{
            sourceloc = CLLocation(latitude: AppUserDefaults.value(forKey: .userLat).doubleValue, longitude: AppUserDefaults.value(forKey: .userLong).doubleValue)
        }else{
            if let lat = AppUserDefaults.value(forKey: .profileLat).double{
                sourceloc = CLLocation(latitude: lat, longitude: AppUserDefaults.value(forKey: .profileLong).doubleValue)
            }else{
                sourceloc = CLLocation(latitude: 42.361145, longitude: -71.057083)
            }
        }
        let distInMeter = sourceloc.distance(from: targetloc)
        return String(Int((distInMeter/1609.344) + 0.5))
    }

    static func userNotExist() {
        
        self.removeAllNotifications()
        let popUpVc = AlertPopUpVC.instantiate(fromAppStoryboard: .Candidate)
      //  popUpVc.delegate = self
        popUpVc.alertType = .userNotExist
        popUpVc.modalPresentationStyle = .overCurrentContext
        popUpVc.modalTransitionStyle = .crossDissolve
       
        sharedAppDelegate.window?.rootViewController?.present(popUpVc, animated: true, completion: nil)
    }
    
    static func removeAllNotifications(){
        let center = UNUserNotificationCenter.current()
        center.removeAllPendingNotificationRequests() // To remove all pending notifications which are not delivered yet but scheduled.
        center.removeAllDeliveredNotifications()
    }
}


