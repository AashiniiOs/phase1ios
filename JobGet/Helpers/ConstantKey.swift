//
//  ConstantKey.swift
//  Onboarding
//
//  Created by macOS on 27/03/18.
//  Copyright © 2018 Gurdeep Singh. All rights reserved.
//

import Foundation

enum ApiKeys: String {
    
    case acess_token  = ""
    case user         = "user"
    case code         = "code"
    case userId       = "userId"
    case jobId        = "jobId"
    case data         = "data"
    case page         = "page"
    case latitude     = "latitude"
    case longitude    = "longitude"
    case categoryId   = "categoryId"
    case type         = "type"
    case search       = "search"
    case sort         = "sort"
    case jobs          = "jobs"
    case next         = "next"
    case message      = "message"
    case applyId      = "applyId"
    case fromExperience = "fromExperience"
    case toExperience = "toExperience"
    case recruiterId = "recruiterId"
    case rating         = "rating"
    case startMonthExp = "startMonthExp"
    case endMonthExp   = "endMonthExp"
    case startYearExp  = "startYearExp"
    case endYearExp     = "endYearExp"
    case reviews        = "reviews"
    case status         = "status"
    case oldPassword       = "oldPassword"
    case newPassword       = "newPassword"
    case confirmPassword   = "conPassword"
    case radius            = "radius"
    
    //    : Int?
    //    private var : Int?
    //    private var : Int?
    //    private var :
}
