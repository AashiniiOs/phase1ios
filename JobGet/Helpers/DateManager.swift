//
//  DateManager.swift
//  Shopoholic
//
//  Created by appinventiv on 26/04/18.
//  Copyright © 2018 Gurdeep Singh. All rights reserved.
//

import Foundation

let dateManager = DateManager.shared

class DateManager{
    
    let dateFormatter: DateFormatter
    static let shared = DateManager()
    
    init() {
        self.dateFormatter = DateFormatter()
        self.dateFormatter.timeZone = TimeZone.current
    }
    
    func string(from timestamp: Double, dateFormat: String = "h:mm a", inMilliSeconds: Bool = true)->String{
        
        self.dateFormatter.dateFormat = dateFormat
        
        if inMilliSeconds{
            return self.dateFormatter.string(from: Date(timeIntervalSince1970: timestamp/1000))
        }else{
            return self.dateFormatter.string(from: Date(timeIntervalSince1970: timestamp))
        }
    }
    
    func stringFrom(date: Date, dateFormat: String = "M/d/yyyy")->String{
        self.dateFormatter.dateFormat = dateFormat
        
        return self.dateFormatter.string(from: date)
    }
    
    func elapsedTime(from timestamp: Double, inMilliSeconds: Bool = true)->String{
        if inMilliSeconds{
            return Date(timeIntervalSince1970: timestamp/1000).elapsedTime
        }else{
            return Date(timeIntervalSince1970: timestamp).elapsedTime
        }
    }
    
    
    func elapsed(from timestamp: Double, inMilliSeconds: Bool = true, isChatTime: Bool = false)->String {
        if isChatTime{
//            return Date(timeIntervalSince1970: timestamp/1000).chatElapsed
            return Date(timeIntervalSince1970: timestamp/1000).chatElapsed
        }else{
//             return Date(timeIntervalSince1970: timestamp/1000).elapsed
           return Date(timeIntervalSince1970: timestamp/1000).elapsed
        }
        
    }
    

    /**
     returns today, yesterday and the date in string
     */
    func getNotationFor(timestamp: Double, inMilliSeconds: Bool = true)->String{
        return Date(timeIntervalSince1970: timestamp/1000).headerElapsed

//        var timeInterval: Double!
//
//        if inMilliSeconds{
//            timeInterval = timestamp/1000
//        }else{
//            timeInterval = timestamp
//        }
//
//        let date = Date(timeIntervalSince1970: timeInterval)
//
//        let numberOfdays = Date().daysFrom(date)
//
//        if numberOfdays == 0{
//            return "Today"
//        }else if numberOfdays == 1{
//            return "Yesterday"
//        }else{
//            return self.stringFrom(date: date)
//        }
        
//        let formatter = DateFormatter()
//        var component: Set<Calendar.Component> = [.year]
//        var interval = Calendar.current.dateComponents(component, from: self, to: Date()).year ?? 0
//        //  let date = Calendar.current.dateComponents(component, from: self, to: Date())
//
//        component = [.month]
//        interval = Calendar.current.dateComponents(component, from: self, to: Date()).month ?? 0
//        if interval > 0 {
//            formatter.locale = Locale(identifier: "en_US_POSIX")
//            formatter.dateFormat = "M/d/yyyy"
//            return formatter.string(from: self)
//        }
//        if let yesterday = Calendar.current.date(byAdding: Calendar.Component.day, value: -1, to: Date()){
//            formatter.locale = Locale(identifier: "en_US_POSIX")
//            formatter.dateFormat = "M/d/yyyy"
//            if formatter.string(from: self) == formatter.string(from: yesterday){
//                return "Yesterday"
//            }
//            component = [.hour]
//            interval = Calendar.current.dateComponents(component, from: self, to: Date()).hour ?? 0
//            if interval < 24 {
//
//                formatter.locale = Locale(identifier: "en_US_POSIX")
//                formatter.dateFormat = "h:mm a"
//                formatter.amSymbol = "AM"
//                formatter.pmSymbol = "PM"
//                return formatter.string(from: self)
//
//
//                // return interval == 1 ? "\(interval)" + " hour ago" :
//                //"\(interval)" + " hours ago"
//            }
//
//        }
//        component = [.day]
//        interval = Calendar.current.dateComponents(component, from: self, to: Date()).day ?? 0
//        if interval > 0 {
//            formatter.locale = Locale(identifier: "en_US_POSIX")
//            formatter.dateFormat = "M/d/yyyy"
//
//            return formatter.string(from: self)
//        }
//
//        component = [.hour]
//        interval = Calendar.current.dateComponents(component, from: self, to: Date()).hour ?? 0
//        if interval < 24 {
//
//            formatter.locale = Locale(identifier: "en_US_POSIX")
//            formatter.dateFormat = "h:mm a"
//            formatter.amSymbol = "AM"
//            formatter.pmSymbol = "PM"
//            return formatter.string(from: self)
//
//
//            // return interval == 1 ? "\(interval)" + " hour ago" :
//            //"\(interval)" + " hours ago"
//        }
//
//        //    component = [.minute]
//        //    interval = Calendar.current.dateComponents(component, from: self, to: Date()).minute ?? 0
//        //    if interval > 0 {
//        //        return interval == 1 ? "\(interval)" + " min ago" :
//        //            "\(interval)" + " mins ago"
//        //    }
//
//        return ""
//
//
//
    }
    
}


extension Date {
    
    // Convert local time to UTC (or GMT)
    func toGlobalTime() -> Date {
        let timezone = TimeZone.current
        let seconds = -TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
    // Convert UTC (or GMT) to local time
    func toLocalTime() -> Date {
       
        let timezone = TimeZone.current
        let seconds = TimeInterval(timezone.secondsFromGMT(for: self))
        return Date(timeInterval: seconds, since: self)
    }
    
}

