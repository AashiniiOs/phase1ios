//
//  DrawCircleInViewCentre.swift
//  JobGet
//
//  Created by Appinventiv on 19/06/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import Foundation
import MapKit

//class used to create the views and draw circles in them
class CircleView: UIView {
    
    
    let pi:CGFloat = CGFloat(Double.pi)
    let circle = CAShapeLayer()
    var secondLayerColor: UIColor = AppColors.white
    
    
    //custom initializer
    override init(frame: CGRect) {
        super.init(frame: frame)
        isUserInteractionEnabled = true
        //         setupShapeLayer()
        //        createOverlay()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        isUserInteractionEnabled = true
        //          setupShapeLayer()
        //        createOverlay()
    }
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let hitView = super.hitTest(point, with: event)
        self.backgroundColor = AppColors.themeBlueColor.withAlphaComponent(1)
        return hitView
    }
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        
        return false
    }
    
    
    //
    //    override func layoutSubviews() {
    //        super.layoutSubviews()
    //        setupShapeLayer()
    //    }
    
 
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.setupShapeLayer()
    }
    
    func setupShapeLayer() {
        
        self.backgroundColor = AppColors.themeBlueColor.withAlphaComponent(1)
        self.isUserInteractionEnabled = false
        let path = CGMutablePath()
        path.addArc(center: CGPoint(x: center.x , y: frame.height/2) ,
                    radius: 140,
                    startAngle: 0.0,
                    endAngle: 2.0 * .pi,
                    clockwise: false)
        path.addRect(CGRect(origin: .zero, size: self.frame.size))
        let maskLayer = CAShapeLayer()
        maskLayer.fillRule = kCAFillRuleEvenOdd
        
        maskLayer.path = path
        self.layer.mask = maskLayer
        self.clipsToBounds = true
        let shape = CAShapeLayer()
        shape.frame = self.bounds
        shape.path = path
        shape.lineWidth = 4.0
        shape.strokeColor = #colorLiteral(red: 0, green: 0.5898008943, blue: 1, alpha: 1).cgColor
        shape.fillColor = UIColor.clear.cgColor
        self.layer.insertSublayer(shape, at: 0)
        let shadowSubLayer = createShadowLayer()
        shadowSubLayer.insertSublayer(shape, at: 0)
        self.layer.addSublayer(shadowSubLayer)
    }
    
    func createShadowLayer() -> CALayer {
        let shadowLayer = CALayer()
        shadowLayer.shadowColor = AppColors.themeBlueColor.cgColor
        shadowLayer.shadowOffset = CGSize(width: 10.0, height: 40.0)
        shadowLayer.shadowRadius = 60.0
        shadowLayer.shadowOpacity = 0.5
        shadowLayer.backgroundColor = UIColor.clear.cgColor
        return shadowLayer
    }
}

class SmallCircleMarker: UIView{
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.roundCorners()
    }
}


