//
//  Enum.swift
//  Onboarding
//
//  Created by macOS on 27/03/18.
//  Copyright © 2018 Gurdeep Singh. All rights reserved.
//

import Foundation

enum Device_Type: String {
    
    case android = "1"
    case iOS     = "2"
}

enum UserType: String {
    case none
    case candidate = "1"
    case recuriter = "2"
}

