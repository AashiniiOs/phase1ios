//
//  DateExtention.swift
//  WoshApp
//
//  Created by Saurabh Shukla on 19/09/17.
//  Copyright © 2017 Appinventiv. All rights reserved.
//


import Foundation

extension Date {
    
    var isToday:Bool{
        return Calendar.current.isDateInToday(self)
    }
    var isYesterday:Bool{
        return Calendar.current.isDateInYesterday(self)
    }
    var isTomorrow:Bool{
        return Calendar.current.isDateInTomorrow(self)
    }
    var isWeekend:Bool{
        return Calendar.current.isDateInWeekend(self)
    }
    var year:Int{
        return (Calendar.current as NSCalendar).components(.year, from: self).year!
    }
    var month:Int{
        return (Calendar.current as NSCalendar).components(.month, from: self).month!
    }
    var weekOfYear:Int{
        return (Calendar.current as NSCalendar).components(.weekOfYear, from: self).weekOfYear!
    }
    var weekday:Int{
        return (Calendar.current as NSCalendar).components(.weekday, from: self).weekday!
    }
    var weekdayOrdinal:Int{
        return (Calendar.current as NSCalendar).components(.weekdayOrdinal, from: self).weekdayOrdinal!
    }
    var weekOfMonth:Int{
        return (Calendar.current as NSCalendar).components(.weekOfMonth, from: self).weekOfMonth!
    }
    var day:Int{
        return (Calendar.current as NSCalendar).components(.day, from: self).day!
    }
    var hour:Int{
        return (Calendar.current as NSCalendar).components(.hour, from: self).hour!
    }
    var minute:Int{
        return (Calendar.current as NSCalendar).components(.minute, from: self).minute!
    }
    var second:Int{
        return (Calendar.current as NSCalendar).components(.second, from: self).second!
    }
    var numberOfWeeks:Int{
        let weekRange = (Calendar.current as NSCalendar).range(of: .weekOfYear, in: .month, for: Date())
        return weekRange.length
    }
    var unixTimestamp:Double {
        
        return self.timeIntervalSince1970
    }
    
    func yearsFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(.year, from: date, to: self, options: []).year!
    }
    func monthsFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(.month, from: date, to: self, options: []).month!
    }
    func weeksFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(.weekOfYear, from: date, to: self, options: []).weekOfYear!
    }
    func weekdayFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(.weekday, from: date, to: self, options: []).weekday!
    }
    func weekdayOrdinalFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(.weekdayOrdinal, from: date, to: self, options: []).weekdayOrdinal!
    }
    func weekOfMonthFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(.weekOfMonth, from: date, to: self, options: []).weekOfMonth!
    }
    func daysFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(.day, from: date, to: self, options: []).day!
    }
    func hoursFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(.hour, from: date, to: self, options: []).hour!
    }
    func minutesFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(.minute, from: date, to: self, options: []).minute!
    }
    func secondsFrom(_ date:Date) -> Int{
        return (Calendar.current as NSCalendar).components(.second, from: date, to: self, options: []).second!
    }
    func offsetFrom(_ date:Date) -> String {
        if yearsFrom(date)   > 0 { return "\(yearsFrom(date))y"   }
        if monthsFrom(date)  > 0 { return "\(monthsFrom(date))M"  }
        if weeksFrom(date)   > 0 { return "\(weeksFrom(date))w"   }
        if daysFrom(date)    > 0 { return "\(daysFrom(date))d"    }
        if hoursFrom(date)   > 0 { return "\(hoursFrom(date))h"   }
        if minutesFrom(date) > 0 { return "\(minutesFrom(date))m" }
        if secondsFrom(date) > 0 { return "\(secondsFrom(date))s" }
        return ""
    }
    
    ///Converts a given Date into String based on the date format and timezone provided
    func toString(dateFormat:String,timeZone:TimeZone = TimeZone.current)->String{
        
        let frmtr = DateFormatter()
        frmtr.locale = Locale(identifier: "en_US_POSIX")
        frmtr.dateFormat = dateFormat
        frmtr.timeZone = timeZone
        return frmtr.string(from: self)
    }
    
    func calculateAge() -> Int {
        
        let calendar : Calendar = Calendar.current
        let unitFlags : NSCalendar.Unit = [NSCalendar.Unit.year , NSCalendar.Unit.month , NSCalendar.Unit.day]
        let dateComponentNow : DateComponents = (calendar as NSCalendar).components(unitFlags, from: Date())
        let dateComponentBirth : DateComponents = (calendar as NSCalendar).components(unitFlags, from: self)
        
        if ( (dateComponentNow.month! < dateComponentBirth.month!) ||
            ((dateComponentNow.month! == dateComponentBirth.month!) && (dateComponentNow.day! < dateComponentBirth.day!))
            )
        {
            return dateComponentNow.year! - dateComponentBirth.year! - 1
        }
        else {
            return dateComponentNow.year! - dateComponentBirth.year!
        }
    }
    
    public var elapsedTime: String {
        
        var component: Set<Calendar.Component> = [.year]
        var interval = Calendar.current.dateComponents(component, from: self, to: Date()).year ?? 0
        
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " year ago" :
                "\(interval)" + " years ago"
        }
        
        component = [.month]
        interval = Calendar.current.dateComponents(component, from: self, to: Date()).month ?? 0
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " month ago" :
                "\(interval)" + " months ago"
        }
        //        component = [.weekOfYear]
        //        interval = Calendar.current.dateComponents(component, from: self, to: Date()).weekOfYear ?? 0
        //        if interval > 0 {
        //            return interval == 1 ? "\(interval)" + " week ago" :
        //                "\(interval)" + " weeks ago"
        //        }
        
        component = [.day]
        interval = Calendar.current.dateComponents(component, from: self, to: Date()).day ?? 0
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " day ago" :
                "\(interval)" + " days ago"
        }
        
        component = [.hour]
        interval = Calendar.current.dateComponents(component, from: self, to: Date()).hour ?? 0
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " hour ago" :
                "\(interval)" + " hours ago"
        }
        
        component = [.minute]
        interval = Calendar.current.dateComponents(component, from: self, to: Date()).minute ?? 0
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " min ago" :
                "\(interval)" + " mins ago"
        }
        
        return "just now"
    }
    
    public var headerElapsed: String {
        let formatter = DateFormatter()
        var component: Set<Calendar.Component> = [.year]
        var interval = Calendar.current.dateComponents(component, from: self, to: Date()).year ?? 0
        //  let date = Calendar.current.dateComponents(component, from: self, to: Date())
        
        component = [.month]
        interval = Calendar.current.dateComponents(component, from: self, to: Date()).month ?? 0
        if interval > 0 {
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "M/d/yyyy"
            return formatter.string(from: self)
        }
        if let yesterday = Calendar.current.date(byAdding: Calendar.Component.day, value: -1, to: Date()){
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "M/d/yyyy"
            if formatter.string(from: self) == formatter.string(from: yesterday){
                return "Yesterday"
            }
            component = [.hour]
            interval = Calendar.current.dateComponents(component, from: self, to: Date()).hour ?? 0
            if interval < 24 {
                
//                formatter.locale = Locale(identifier: "en_US_POSIX")
//                formatter.dateFormat = "h:mm a"
//                formatter.amSymbol = "AM"
//                formatter.pmSymbol = "PM"
//                return formatter.string(from: self)
                return "Today"
                
                // return interval == 1 ? "\(interval)" + " hour ago" :
                //"\(interval)" + " hours ago"
            }
            
        }
        component = [.day]
        interval = Calendar.current.dateComponents(component, from: self, to: Date()).day ?? 0
        if interval > 0 {
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "M/d/yyyy"
            
            return formatter.string(from: self)
        }
        
        component = [.hour]
        interval = Calendar.current.dateComponents(component, from: self, to: Date()).hour ?? 0
        if interval < 24 {
            
//            formatter.locale = Locale(identifier: "en_US_POSIX")
//            formatter.dateFormat = "h:mm a"
//            formatter.amSymbol = "AM"
//            formatter.pmSymbol = "PM"
//            return formatter.string(from: self)
            return "Today"

            
            // return interval == 1 ? "\(interval)" + " hour ago" :
            //"\(interval)" + " hours ago"
        }
        
        //    component = [.minute]
        //    interval = Calendar.current.dateComponents(component, from: self, to: Date()).minute ?? 0
        //    if interval > 0 {
        //        return interval == 1 ? "\(interval)" + " min ago" :
        //            "\(interval)" + " mins ago"
        //    }
        
        return ""
    }

    // get time if less 24 hour otherwise get date in dd/MM/yyyy
    public var elapsed: String {
        let formatter = DateFormatter()
        var component: Set<Calendar.Component> = [.year]
        var interval = Calendar.current.dateComponents(component, from: self, to: Date()).year ?? 0
        //  let date = Calendar.current.dateComponents(component, from: self, to: Date())
        
        component = [.month]
        interval = Calendar.current.dateComponents(component, from: self, to: Date()).month ?? 0
        if interval > 0 {
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "M/d/yyyy"
            return formatter.string(from: self)
        }
        if let yesterday = Calendar.current.date(byAdding: Calendar.Component.day, value: -1, to: Date()){
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "M/d/yyyy"
            if formatter.string(from: self) == formatter.string(from: yesterday){
                 return "Yesterday"
            }
            component = [.hour]
            interval = Calendar.current.dateComponents(component, from: self, to: Date()).hour ?? 0
            if interval < 24 {
                
                formatter.locale = Locale(identifier: "en_US_POSIX")
                formatter.dateFormat = "h:mm a"
                formatter.amSymbol = "AM"
                formatter.pmSymbol = "PM"
                return formatter.string(from: self)
                
                
                // return interval == 1 ? "\(interval)" + " hour ago" :
                //"\(interval)" + " hours ago"
            }
            
        }
        component = [.day]
        interval = Calendar.current.dateComponents(component, from: self, to: Date()).day ?? 0
        if interval > 0 {
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "M/d/yyyy"
          
            return formatter.string(from: self)
        }
        
        component = [.hour]
        interval = Calendar.current.dateComponents(component, from: self, to: Date()).hour ?? 0
        if interval < 24 {
            
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "h:mm a"
            formatter.amSymbol = "AM"
            formatter.pmSymbol = "PM"
            return formatter.string(from: self)
            
            
            // return interval == 1 ? "\(interval)" + " hour ago" :
            //"\(interval)" + " hours ago"
        }
        
        //    component = [.minute]
        //    interval = Calendar.current.dateComponents(component, from: self, to: Date()).minute ?? 0
        //    if interval > 0 {
        //        return interval == 1 ? "\(interval)" + " min ago" :
        //            "\(interval)" + " mins ago"
        //    }
        
        return ""
    }
    
    public var chatElapsed: String {
        let formatter = DateFormatter()
        var component: Set<Calendar.Component> = [.year]
        var interval = Calendar.current.dateComponents(component, from: self, to: Date()).year ?? 0
        //  let date = Calendar.current.dateComponents(component, from: self, to: Date())
        
        component = [.month]
        interval = Calendar.current.dateComponents(component, from: self, to: Date()).month ?? 0
        if interval > 0 {
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "M/d/yyyyy"
            return formatter.string(from: self)
        }
        
        component = [.day]
        interval = Calendar.current.dateComponents(component, from: self, to: Date()).day ?? 0
        if interval > 0 {
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "h:mm a"
            formatter.amSymbol = "AM"
            formatter.pmSymbol = "PM"
            return formatter.string(from: self)
        }
        
        component = [.hour]
        interval = Calendar.current.dateComponents(component, from: self, to: Date()).hour ?? 0
        if interval < 24 {
            
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "h:mm a"
            formatter.amSymbol = "AM"
            formatter.pmSymbol = "PM"
            return formatter.string(from: self)
            
            
            // return interval == 1 ? "\(interval)" + " hour ago" :
            //"\(interval)" + " hours ago"
        }
        
        //    component = [.minute]
        //    interval = Calendar.current.dateComponents(component, from: self, to: Date()).minute ?? 0
        //    if interval > 0 {
        //        return interval == 1 ? "\(interval)" + " min ago" :
        //            "\(interval)" + " mins ago"
        //    }
        
        return ""
    }
    
    // get time if less 24 hour otherwise get date in dd/MM/yyyy
    public var transactionElapsed: String {
        let formatter = DateFormatter()
        var component: Set<Calendar.Component> = [.year]
        var interval = Calendar.current.dateComponents(component, from: self, to: Date()).year ?? 0
        //  let date = Calendar.current.dateComponents(component, from: self, to: Date())
        
        component = [.month]
        interval = Calendar.current.dateComponents(component, from: self, to: Date()).month ?? 0
        if interval > 0 {
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "M/d/yyyy, h:mm a"
            return formatter.string(from: self)
        }
        
        if let yesterday = Calendar.current.date(byAdding: Calendar.Component.day, value: -1, to: Date()){
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "M/d/yyyy"
            if formatter.string(from: self) == formatter.string(from: yesterday){
                formatter.dateFormat = "h:mm a"
                return "Yesterday, \(formatter.string(from: self))"
            }
            component = [.hour]
            interval = Calendar.current.dateComponents(component, from: self, to: Date()).hour ?? 0
            if interval < 24 {
                
                formatter.locale = Locale(identifier: "en_US_POSIX")
                formatter.dateFormat = "h:mm a"
                formatter.amSymbol = "AM"
                formatter.pmSymbol = "PM"
                return formatter.string(from: self)
                
                
                // return interval == 1 ? "\(interval)" + " hour ago" :
                //"\(interval)" + " hours ago"
            }
            
        }
        
        component = [.day]
        interval = Calendar.current.dateComponents(component, from: self, to: Date()).day ?? 0
        if interval > 0 {
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "M/d/yyyy, h:mm a"
            
            return formatter.string(from: self)
        }
        
        component = [.hour]
        interval = Calendar.current.dateComponents(component, from: self, to: Date()).hour ?? 0
        if interval < 24 {
            
            formatter.locale = Locale(identifier: "en_US_POSIX")
            formatter.dateFormat = "h:mm a"
            formatter.amSymbol = "AM"
            formatter.pmSymbol = "PM"
            return "Today, \(formatter.string(from: self))"
            
            
            // return interval == 1 ? "\(interval)" + " hour ago" :
            //"\(interval)" + " hours ago"
        }
        
        //    component = [.minute]
        //    interval = Calendar.current.dateComponents(component, from: self, to: Date()).minute ?? 0
        //    if interval > 0 {
        //        return interval == 1 ? "\(interval)" + " min ago" :
        //            "\(interval)" + " mins ago"
        //    }
        
        return ""
    }
}


extension Date {
    
    var timeAgoSinceNow: String {
        return getTimeAgoSinceNow()
    }
    
    private func getTimeAgoSinceNow() -> String {
        
        var interval = Calendar.current.dateComponents([.year], from: self, to: Date()).year!
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " year" : "\(interval)" + " years"
        }
        
        interval = Calendar.current.dateComponents([.month], from: self, to: Date()).month!
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " month" : "\(interval)" + " months"
        }
        
        interval = Calendar.current.dateComponents([.day], from: self, to: Date()).day!
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " day" : "\(interval)" + " days"
        }
        
        interval = Calendar.current.dateComponents([.hour], from: self, to: Date()).hour!
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " hour" : "\(interval)" + " hours"
        }
        
        interval = Calendar.current.dateComponents([.minute], from: self, to: Date()).minute!
        if interval > 0 {
            return interval == 1 ? "\(interval)" + " minute" : "\(interval)" + " minutes"
        }
        
        return "a moment ago"
    }
    
    
    private func getTime() -> String {
        
        //        var interval = Calendar.current.dateComponents([.year], from: self, to: Date()).year!
        //        if interval > 0 {
        //            return interval == 1 ? "\(interval)" + " year" : "\(interval)" + " years"
        //        }
        //
        //        interval = Calendar.current.dateComponents([.month], from: self, to: Date()).month!
        //        if interval > 0 {
        //            return interval == 1 ? "\(interval)" + " month" : "\(interval)" + " months"
        //        }
        //
        //        interval = Calendar.current.dateComponents([.day], from: self, to: Date()).day!
        //        if interval > 0 {
        //            return interval == 1 ? "yesterday" : "yesterday"
        //        }
        
        //        interval = Calendar.current.dateComponents([.hour], from: self, to: Date()).hour!
        //        if interval > 0 {
        //            return interval == 1 ? "\(interval)" + " hour" : "\(interval)" + " hours"
        //        }
        //
        //        interval = Calendar.current.dateComponents([.minute], from: self, to: Date()).minute!
        //        if interval > 0 {
        //            return interval == 1 ? "\(interval)" + " minute" : "\(interval)" + " minutes"
        //        }
        //
        
        let now = Date()
        
        let formatter = DateFormatter()
        
        formatter.timeZone = TimeZone.current
        
        formatter.dateFormat = "H:mm"
        
        let dateString = formatter.string(from: now)
        return dateString
    }
    
    
    
    static func zero() -> Date {
        let dateComp = DateComponents(calendar: Calendar.current, timeZone: TimeZone.current, era: 0, year: 0, month: 0, day: 0, hour: 0, minute: 0, second: 0, nanosecond: 0, weekday: 0, weekdayOrdinal: 0, quarter: 0, weekOfMonth: 0, weekOfYear: 0, yearForWeekOfYear: 0)
        return dateComp.date!
    }
}
