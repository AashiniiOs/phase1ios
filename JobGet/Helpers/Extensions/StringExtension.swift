//
//  StringExtention.swift
//  WoshApp
//
//  Created by Saurabh Shukla on 19/09/17.
//  Copyright © 2017 Appinventiv. All rights reserved.
//


import Foundation
import UIKit


// FOR PHONE NUMBER TEXTFIELD FORMAT
// uhg, worst Swift 3.0 decision ever. Stupid people don't grok optionals.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
    switch (lhs, rhs) {
    case let (l?, r?):
        return l < r
    case (nil, _?):
        return true
    default:
        return false
    }
}
extension String {
    
    ///Removes all spaces from the string
    var removeSpaces:String{
        return self.replacingOccurrences(of: " ", with: "")
    }
    
    ///Returns a localized string
    var localized:String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    
    ///Removes leading and trailing white spaces from the string
    var byRemovingLeadingTrailingWhiteSpaces:String {
        
        let spaceSet = CharacterSet.whitespaces
        return self.trimmingCharacters(in: spaceSet)
    }
    
    ///Returns 'true' if the string is any (file, directory or remote etc) url otherwise returns 'false'
    var isAnyUrl:Bool{
        return (URL(string:self) != nil)
    }
    
    ///Returns the json object if the string can be converted into it, otherwise returns 'nil'
    var jsonObject:Any? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: [])
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    ///Returns the base64Encoded string
    var base64Encoded:String {
        
        return Data(self.utf8).base64EncodedString()
    }
    
    ///Returns the string decoded from base64Encoded string
    var base64Decoded:String? {
        
        guard let data = Data(base64Encoded: self) else {
            return nil
        }
        return String(data: data, encoding: .utf8)
    }

    func heightOfText(_ width: CGFloat, font: UIFont) -> CGFloat {
        
        let constraintRect = CGSize(width: width, height: CGFloat.greatestFiniteMagnitude)
        
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return boundingBox.height
    }
    
    public func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        
        let mutableParagraphStyle = NSMutableParagraphStyle()
        mutableParagraphStyle.lineBreakMode = .byWordWrapping
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font, NSAttributedStringKey.paragraphStyle: mutableParagraphStyle], context: nil)
        
        return boundingBox.height
        
    }

    
    func widthOfText(_ height: CGFloat, font: UIFont) -> CGFloat {
        
        let constraintRect = CGSize(width: CGFloat.greatestFiniteMagnitude, height: height)
        
        let boundingBox = self.boundingRect(with: constraintRect, options: NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)
        
        return boundingBox.width
    }
    
    func localizedString(lang:String) ->String {
        
        let path = Bundle.main.path(forResource: lang, ofType: "lproj")
        print_debug(path)
        let bundle = Bundle(path: path!)
        print_debug(bundle)
        
        return NSLocalizedString(self, tableName: nil, bundle: bundle!, value: "", comment: "")
    }
    
    ///Returns 'true' if string contains the substring, otherwise returns 'false'
    func contains(s: String) -> Bool
    {
        return self.range(of: s) != nil ? true : false
    }
    
    ///Replaces occurances of a string with the given another string
    func replace(string: String, withString: String) -> String
    {
        return self.replacingOccurrences(of: string, with: withString, options: String.CompareOptions.literal, range: nil)
    }
    
    ///Converts the string into 'Date' if possible, based on the given date format and timezone. otherwise returns nil
    func toDate(dateFormat:String,timeZone:TimeZone = TimeZone.current)->Date?{
        
        let frmtr = DateFormatter()
        frmtr.locale = Locale(identifier: "en_US_POSIX")
        frmtr.dateFormat = dateFormat
        frmtr.timeZone = timeZone
        return frmtr.date(from: self)
    }
    
    func checkIfValid(_ validityExression : ValidityExression) -> Bool {
        
        let regEx = validityExression.rawValue
        
        let test = NSPredicate(format:"SELF MATCHES %@", regEx)
        
        return test.evaluate(with: self)
    }
    
    func checkIfInvalid(_ validityExression : ValidityExression) -> Bool {
        
        return !self.checkIfValid(validityExression)
    }
    
  
}

enum ValidityExression : String {
    
    case userName = "^[a-zA-z]{1,}+[a-zA-z0-9!@#$%&*]{2,15}"
    case email = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9-]+\\.[A-Za-z]{2,}(\\.[A-Za-z]{2,20})?"
    case mobileNumber = "^[0-9]{6,20}$"
    case password = "^[a-zA-Z0-9!@#$%&*+-_]{6,}"
    case name = "^[a-zA-Z]{2,15}"
    case webUrl = "((https|http)://)((\\w|-)+)(([.]|[/])((\\w|-)+))+"

    
    //    case Username = "[a-zA-z]+([ '-][a-zA-Z]+)*$"
    //    case Email = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
    //    case MobileNumber = "^[0-9]{8,10}$"
    //    case Password = "^[a-zA-Z]{6}"
    //    case Name = "[a-z A-Z]{20}$"
}

// FOR PHONE NUMBER TEXTFIELD FORMAT

extension String {
    /**
     Pads the left side of a string with the specified string up to the specified length.
     Does not clip the string if too long.
     
     - parameter padding:   The string to use to create the padding (if needed)
     - parameter length:    Integer target length for entire string
     - returns: The padded string
     */
    func lpad(_ padding: String, length: Int) -> (String) {
        if self.count > length {
            return self
        }
        return "".padding(toLength: length - self.characters.count, withPad:padding, startingAt:0) + self
    }
    /**
     Pads the right side of a string with the specified string up to the specified length.
     Does not clip the string if too long.
     
     - parameter padding:   The string to use to create the padding (if needed)
     - parameter length:    Integer target length for entire string
     - returns: The padded string
     */
    func rpad(_ padding: String, length: Int) -> (String) {
        if self.characters.count > length { return self }
        return self.padding(toLength: length, withPad:padding, startingAt:0)
    }
    /**
     Returns string with left and right spaces trimmed off.
     
     - returns: Trimmed String
     */
    func trim() -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespaces)
    }
    /**
     Shortcut for getting length (since Swift keeps cahnging this).
     
     - returns: Int length of string
     */
    var length: Int {
        return self.count
    }
    /**
     Returns character at a specific position from a string.
     
     - parameter index:               The position of the character
     - returns: Character
     */
    subscript (i: Int) -> Character {
        return self[self.index(self.startIndex, offsetBy: i)]
    }
    subscript (i: Int) -> String {
        return String(self[i] as Character)
    }
    /**
     Returns substring extracted from a string at start and end location.
     
     - parameter start:               Where to start (-1 acceptable)
     - parameter end:                 (Optional) Where to end (-1 acceptable) - default to end of string
     - returns: String
     */
    func stringFrom(_ start: Int, to end: Int? = nil) -> String {
        var maximum = self.count
        
        let i = start < 0 ? self.endIndex : self.startIndex
        let ioffset = min(maximum, max(-1 * maximum, start))
        let startIndex = self.index(i, offsetBy: ioffset)
        
        maximum -= start
        
        let j = end < 0 ? self.endIndex : self.startIndex
        let joffset = min(maximum, max(-1 * maximum, end ?? 0))
        let endIndex = end != nil && end! < self.characters.count ? self.index(j, offsetBy: joffset) : self.endIndex
        return self.substring(with: (startIndex ..< endIndex))
    }
    /**
     Returns substring composed of only the allowed characters.
     
     - parameter allowed:             String list of acceptable characters
     - returns: String
     */
    func onlyCharacters(_ allowed: String) -> String {
        let search = allowed.characters
        return characters.filter({ search.contains($0) }).reduce("", { $0 + String($1) })
    }
    /**
     Simple pattern matcher. Requires full match (ie, includes ^$ implicitly).
     
     - parameter pattern:             Regex pattern (includes ^$ implicitly)
     - returns: true if full match found
     */
    func matches(_ pattern: String) -> Bool {
        let test = NSPredicate(format:"SELF MATCHES %@", pattern)
        return test.evaluate(with: self)
    }
}

// get Attributes Text
//======================

extension String {
    
    func stringByAppendingPathComponent(path: String) -> String {
        
        let nsSt = self as NSString
        return nsSt.appendingPathComponent(path)
    }
    
    func getAttributedString(attributedFont: UIFont =  UIFont.systemFont(ofSize: 12), attributedColor: UIColor, normalFont: UIFont, normalColor: UIColor, attributedRange: NSRange) -> NSAttributedString  {
        
        let attributedString = NSMutableAttributedString(string: self, attributes: [
            .font: attributedFont,
            .foregroundColor: attributedColor,
           
            ])
        attributedString.addAttribute(.font, value: attributedFont, range: attributedRange )
        
        return attributedString
    }
    
    var trailingSpacesTrimmed: String {
        var newString = self
        
        while newString.hasSuffix(" ") || newString.hasSuffix("\n") {
            newString = String(newString.dropLast())
        }

        return newString
    }
    
    func strstr(needle: String, beforeNeedle: Bool = false) -> String? {
        guard let range = self.range(of: needle) else { return nil }
        
        if beforeNeedle {
            return String(self.prefix(upTo: range.lowerBound))
//                self.substring(to: range.lowerBound)
        }
        
        return String(self.suffix(from: range.upperBound))
//            self.substring(from: range.upperBound)
    }

}

extension NSMutableAttributedString {
    
    func setColorForText(textForAttribute: String, withColor color: UIColor, font: UIFont) {
        let range: NSRange = self.mutableString.range(of: textForAttribute, options: .caseInsensitive)
        self.addAttribute(NSAttributedStringKey.foregroundColor, value: color, range: range)
        self.addAttribute(NSAttributedStringKey.font, value: font, range: range )
    }
}
