//
//  UITextFieldExtension.swift
//  WoshApp
//
//  Created by Saurabh Shukla on 19/09/17.
//  Copyright © 2017 Appinventiv. All rights reserved.
//


import Foundation
import UIKit

extension UITextField{
    
    ///Sets and gets if the textfield has secure text entry
    var isSecureText:Bool{
        get{
            return self.isSecureTextEntry
        }
        set{
            let font = self.font
            self.isSecureTextEntry = newValue
            if let text = self.text{
                self.text = ""
                self.text = text
            }
            self.font = nil
            self.font = font
            self.contentVerticalAlignment = UIControlContentVerticalAlignment.center
        }
    }
    
    func setPlaceholderColor(color:UIColor){
        
        let str = NSAttributedString(string: self.placeholder ?? "", attributes: [NSAttributedStringKey.foregroundColor:color])
        self.attributedPlaceholder = str
    }
    
    func setButtonToRightView(btn : UIButton, selectedText: String?, normalText: String?, size: CGSize?)
    {
        self.rightViewMode = .always
        self.rightView = btn
        btn.isSelected = false
       
        if let selectedText = selectedText {
           // btn.setImage(selectedImg, for: .selected)
            btn.setTitle(selectedText, for: .selected)
        }
        if let normalText = normalText {
           // btn.setImage(unselectedImg, for: .normal)
             btn.setTitle(normalText, for: .normal)
        }
        if let btnSize = size {
            self.rightView?.frame.size = btnSize
            
        } else {
            self.rightView?.frame.size = CGSize(width: btn.intrinsicContentSize.width+10, height: self.frame.height )
            
        }
        
    }
    
    
    // SET BUTTON TO RIGHT VIEW    //=========================
    func setButtonRightView(btn : UIButton, selectedImage : UIImage?, normalImage : UIImage?, size: CGSize?)
    {
        self.rightViewMode = .always
        self.rightView = btn
        btn.isSelected = false
        if let selectedImg = selectedImage {
            btn.setImage(selectedImg, for: .selected)
            
        }
        if let unselectedImg = normalImage {
            btn.setImage(unselectedImg, for: .normal)
            
        }
        if let btnSize = size {
            self.rightView?.frame.size = btnSize
            
        } else {
            self.rightView?.frame.size = CGSize(width: btn.intrinsicContentSize.width+10, height: self.frame.height)
            
        }
        
    }
//    
//    func setButtonRightView(btn : UIButton, selectedText: String?, normalText: String?, size: CGRect?)
//    {
//        self.rightViewMode = .always
//        self.rightView = btn
//        btn.isSelected = false
//        
//        if let selectedText = selectedText {
//            // btn.setImage(selectedImg, for: .selected)
//            btn.setTitle(selectedText, for: .selected)
//        }
//        if let normalText = normalText {
//            // btn.setImage(unselectedImg, for: .normal)
//            btn.setTitle(normalText, for: .normal)
//        }
//        if let btnSize = size {
//            self.rightView?.frame = btnSize
//            
//        } else {
//            self.rightView?.frame.size = CGSize(width: btn.intrinsicContentSize.width+10, height: self.frame.height )
//            
//        }
//        
//    }
    func setLabelToRightView(label : UILabel, size: CGSize?)
    {
        self.rightViewMode = .always
        self.rightView = label
       // btn.isSelected = false
       
        if let labelSize = size {
            self.rightView?.frame.size = labelSize
            
        } else {
            self.rightView?.frame.size = CGSize(width: label.intrinsicContentSize.width+10, height: self.frame.height )
            
        }
        
    }
    
//    func addRightBtn(textField:UITextField) {
//        let rightView = UIButton()
//        
//        let frame = CGRect(x: 0, y: 0, width: 100, height: 30)
//        rightView.frame = frame
//        rightView.layer.borderColor = Global.getGradientImage_Color(size: rightView.size)?.cgColor
//        
//        let textSize = Global.textSizeCount("Change", font:AppFonts.bold.withSize(8), bundingSize: CGSize(width: 40, height: 1000))
//        rightView.setTitle("Change", for: .normal)
//        
//        rightView.setTitleColor(Global.getGradientImage_Color(size: textSize), for: .normal)
//        //rightView.backgroundColor = UIColor.red
//        rightView.addTarget(self, action: #selector(btnTapped), for: .touchUpInside)
//        textField.rightView = rightView
//        textField.rightViewMode = UITextFieldViewMode.always
//        textField.layoutIfNeeded()
//    }
    func adjustFrameWidthToFitText()
    {
        let size = intrinsicContentSize
        frame = CGRect(x:frame.origin.x, y:frame.origin.y, width:size.width, height:frame.height)
    }
}
