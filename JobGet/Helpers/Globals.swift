//
//  Globals.swift
//  Onboarding
//
//  Created by Appinventiv on 22/08/16.
//  Copyright © 2016 Gurdeep Singh. All rights reserved.
//

import UIKit

//let sharedAppDelegate = UIApplication.shared.delegate as! AppDelegate

//let sharedAppDelegate = UIApplication.shared.delegate as! AppDelegate

let SCREEN_WIDTH = UIScreen.main.bounds.width
let SCREEN_HEIGHT = UIScreen.main.bounds.height

let S3_PoolId = "us-east-1:fa10f84e-3377-40f4-92dd-1767358271ce"
func print_debug <T> (_ object: T) {
    
    // TODO: Comment Next Statement To Deactivate Logs
//    print(object)
    
}

var isSimulatorDevice:Bool {
    
    var isSimulator = false
    #if arch(i386) || arch(x86_64)
        //simulator
        isSimulator = true
    #endif
    return isSimulator
}

//MARK: Loader
//====================
var loader = __Loader(frame: CGRect(x:0, y:0, width:0, height:0))

class __Loader : UIView {
    
    var activityIndicator : UIActivityIndicatorView!
    
    var isLoading = false
    
    override init(frame: CGRect) {
        super.init(frame: UIScreen.main.bounds)
        
     
        
        self.backgroundColor = UIColor(white: 0.0, alpha: 0.25)
        self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.white)
        self.activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.whiteLarge
        let lbl = UILabel(frame: CGRect(x: 40, y: 50, width: 250, height: 50))
        lbl.textColor = AppColors.white
        lbl.text =  "Profile Image uploading..."
        lbl.font = AppFonts.Poppins_Regular.withSize(14.0)
        let innerView = UIView(frame: CGRect(x:0,y:0,width:100,height:50))
        innerView.addSubview(self.activityIndicator)
        self.activityIndicator.center = CGPoint(x:10, y:innerView.center.y+50)
        innerView.center = self.center
        innerView.addSubview(self.activityIndicator)
        innerView.addSubview(lbl)
        
        self.addSubview(innerView)

        
        
//        self.backgroundColor = UIColor(white: 0.0, alpha: 0.25)
//        self.activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.gray)
//
//        let lbl = UILabel(frame: CGRect(x:30, y:0, width:200, height:50))
//        lbl.textColor = AppColors.white
//        lbl.text = "Please wait till image gets uploaded..."
////        let innerView = UIView(frame: CGRect(x:0,y:0,width:130,height:50))
//        let innerView = UIView(frame: frame)
//        innerView.backgroundColor = UIColor.clear
//        innerView.addSubview(self.activityIndicator)
//        self.activityIndicator.center = CGPoint(x: innerView.center.x, y:innerView.center.y)
//        innerView.center = self.center
//
//         innerView.center = CGPoint(x: frame.midX  , y: frame.midY )
//        innerView.addSubview(self.activityIndicator)
//        innerView.addSubview(lbl)
//
//        self.addSubview(innerView)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func start() {
        
        if self.isLoading { return }
        
        sharedAppDelegate.window?.addSubview(self)
        self.activityIndicator.startAnimating()
        
        self.isLoading = true
    }
    
    func stop() {
        
        self.activityIndicator.stopAnimating()
        self.removeFromSuperview()
        self.isLoading = false
    }
}

func dateFromMilliseconds(timestamp: Int64) -> Date {
    return Date(timeIntervalSince1970: TimeInterval(timestamp / 1000)) ///1000
}


func getCountryList(completion : @escaping ([[String: AnyObject]]) -> Void){
    
    if let path = Bundle.main.path(forResource: "CountryCode", ofType: "json") {
        
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
            let jsonObject =  try JSONSerialization.jsonObject(with: data , options: JSONSerialization.ReadingOptions.mutableContainers)
            if let jsonDict = jsonObject as? [String: AnyObject] {                
                if let sendData = jsonDict["countryDetails"] as? [[String: AnyObject]]{
                    completion(sendData)
                }
                completion([])
            }else {
                completion([])
            }
        } catch let error {
            //            print(error.localizedDescription)
            completion([])
        }
    } else {
        //        print("Invalid filename/path.")
        completion([])
    }
    
}

func getAbbreatedState(completion : @escaping ([[String: AnyObject]]) -> Void){
    
    if let path = Bundle.main.path(forResource: "State", ofType: "json") {
        
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .alwaysMapped)
            let jsonObject =  try JSONSerialization.jsonObject(with: data , options: JSONSerialization.ReadingOptions.mutableContainers)
            if let jsonDict = jsonObject as? [String: AnyObject] {
                
                if let sendData = jsonDict["response_data"] as? [[String: AnyObject]]{
                    
                    completion(sendData)
                }
                completion([])
            }else {
                completion([])
            }
            
        } catch let error {
            //            print(error.localizedDescription)
            completion([])
        }
    } else {
        //        print("Invalid filename/path.")
        completion([])
    }
    
}
