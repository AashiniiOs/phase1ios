//
//  ViewController.swift
//  InAppPurchaseDemo
//
//  Created by Saurabh Shukla on 30/08/17.
//  Copyright © 2017 Appinventiv. All rights reserved.
//

import UIKit
import StoreKit

class IAPController: NSObject {
    
    fileprivate enum IAPRequestType {
        
        case fetchProduct
        case restoreProduct
        case purchaseProduct
        case fetchReceipt
        case none
    }
    
    // MARK: - Check if payment can be made.
    class var canMakePayment:Bool {  return SKPaymentQueue.canMakePayments()  }
    
    // MARK: - Shared instance
    static let shared : IAPController = {
        let instance = IAPController()
        return instance
    }()
    
    // MARK: - Completion block to notify reciver about fetched products from Appstore.
    fileprivate var fetchedProductsCompletionBlock:((_ product:[SKProduct])->Void)!
    
    // MARK: - Completion block to notify reciver about purchased, restored and failed products.
    fileprivate var purchaseCompletionBlock:((_ purchasedPID:Set<String>,_ restoredPID:Set<String>,_ failedPID:Set<String>)->Void)!
    
    // MARK: - Completion block to notify reciver about the error causing failure.
    fileprivate var recieptFailureBlock:((_ error:Error?)->Void)!
    fileprivate var productFetchFailureBlock:((_ error:Error?)->Void)!
    fileprivate var productRestoreFailureBlock:((_ error:Error?)->Void)!

    fileprivate var fetchReceiptBlock:((_ recipt:[AnyHashable:Any])->Void)!
    fileprivate var sharedSecrete:String?
    var shouldAddStorePaymentHandler:((_ payment: SKPayment, _ product: SKProduct) -> Bool)?
    
    // MARK: - URL for app receipt.
    fileprivate let appReceiptURL = Bundle.main.appStoreReceiptURL
    
    // MARK: - URL to validate app receipt
    #if DEBUG
    fileprivate let receiptValidationURLString = "https://sandbox.itunes.apple.com/verifyReceipt"
    #else
    fileprivate let receiptValidationURLString = "https://buy.itunes.apple.com/verifyReceipt"
    #endif
    
    fileprivate var iapRequestType:IAPRequestType = .none
    
    // MARK: - Fetch available In App Purchase products
    func fetchAvailableProducts(productIdentifiers:Set<String>, success:@escaping ((_ product:[SKProduct])->Void), failure:@escaping ((_ error:Error?)->Void))  {
        
        fetchedProductsCompletionBlock = success
        productFetchFailureBlock = failure
        iapRequestType = .fetchProduct
        
        let iapProductsRequest = SKProductsRequest(productIdentifiers: productIdentifiers)
        iapProductsRequest.delegate = self
        iapProductsRequest.start()
    }
    
    // MARK: - Purchase an In App Purchase product
    func purchaseProduct(product: SKProduct,
                         completion:@escaping ((_ purchasedPID:Set<String>,_ restoredPID:Set<String>,_ failedPID:Set<String>)->Void)) {
        
        iapRequestType = .purchaseProduct
        
        if IAPController.canMakePayment {
            purchaseCompletionBlock = completion
            let payment = SKPayment(product: product)
            SKPaymentQueue.default().add(self)
            SKPaymentQueue.default().add(payment)
        } else {
            let alertController = UIAlertController(title: PURCHASE_FAILED, message: PURCHASE_DISABLED, preferredStyle: .alert)
            let alertAction = UIAlertAction(title: OK, style: .cancel, handler: nil)
            alertController.addAction(alertAction)
            sharedAppDelegate.window?.rootViewController?.present(alertController, animated: true, completion: nil)
        }
    }
    
    // MARK: - Restore In App Purchase products
    func restoreIAPProducts(success:@escaping ((_ purchasedPID:Set<String>,_ restoredPID:Set<String>,_ failedPID:Set<String>)->Void), failure:@escaping ((_ error:Error?)->Void)){
        
        purchaseCompletionBlock = success
        productRestoreFailureBlock = failure
        iapRequestType = .restoreProduct
        
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
    
    // MARK: - Fetch latest app receipt
    func fetchIAPReceipt(forceRefresh:Bool = false, sharedSecrete:String?, success:@escaping ((_ receipt:[AnyHashable:Any])->Void),
                         failure:@escaping ((_ error:Error?)->Void)) {
         AppNetworking.showLoader()
        fetchReceiptBlock = success
        recieptFailureBlock = failure
        iapRequestType = .fetchReceipt
        self.sharedSecrete = sharedSecrete

        if forceRefresh {
            refreshReceipt()
        }
        else{
            guard let receiptURL = appReceiptURL else {
                 AppNetworking.hideLoader()
                /* receiptURL is nil, it would be very weird to end up here */  return }
            do {
                let receipt = try Data(contentsOf: receiptURL)
                validateReceipt(receipt)
            } catch {
                // there is no app receipt, don't panic, ask apple to refresh it
                refreshReceipt()
                 AppNetworking.hideLoader()
            }
        }
    }
    
    // MARK: - Refresh reciept by fetching latest one from Appstore
    fileprivate func refreshReceipt(){
        
        iapRequestType = .fetchReceipt
        let appReceiptRefreshRequest = SKReceiptRefreshRequest(receiptProperties: nil)
        appReceiptRefreshRequest.delegate = self
        appReceiptRefreshRequest.start()
        // If all goes well control will land in the requestDidFinish() delegate method.
        // If something bad happens control will land in didFailWithError.
    }
    
    // MARK: - validate reciept from Appstore
    fileprivate func validateReceipt(_ receipt: Data) {
        
        func errorOccured(reason:String){
            AppNetworking.hideLoader()
            sharedAppDelegate.window?.rootViewController?.showAlert(msg: reason)
            let error = NSError(domain: reason, code: 500, userInfo: [NSLocalizedDescriptionKey:reason])
            
            recieptFailureBlock?(error as Error)
        }
        
        let base64encodedReceipt = receipt.base64EncodedString()
        var requestDictionary = ["receipt-data":base64encodedReceipt]
        if let sharedSecrete = sharedSecrete{
            requestDictionary["password"] = sharedSecrete
        }
        
        guard JSONSerialization.isValidJSONObject(requestDictionary) else {
            
            errorOccured(reason: DICTIONARY_IS_INVALID)
            return
        }
        do {
            let requestData = try JSONSerialization.data(withJSONObject: requestDictionary)
            guard let validationURL = URL(string: receiptValidationURLString) else {
                errorOccured(reason: VALIDATION_URL_NOT_CREATED)
                return
            }
            let session = URLSession(configuration: URLSessionConfiguration.default)
            var request = URLRequest(url: validationURL)
            request.httpMethod = "POST"
            request.cachePolicy = URLRequest.CachePolicy.reloadIgnoringCacheData
            let task = session.uploadTask(with: request, from: requestData) { [weak self] (data, response, error) in
                if let data = data , error == nil {
                    do {
                        let appReceiptJSON = try JSONSerialization.jsonObject(with: data)
                        if let receipt = appReceiptJSON as? [AnyHashable:Any]{
                            self?.fetchReceiptBlock?(receipt)
                        }
                    } catch let error as NSError {
                        errorOccured(reason: error.localizedDescription)
                    }
                } else {
                    errorOccured(reason: error?.localizedDescription ?? UPLOAD_TASK_ERROR)
                }
            }
            task.resume()
        } catch let error as NSError {
            errorOccured(reason: error.localizedDescription)
        }
    }
    
    // MARK: - Verify if a product purchased is stil valid or not.
    func verifyProduct(
        type: SubscriptionType,
        productId: String, receipt: ReceiptInfo) -> VerifySubscriptionResult{
        
        let result = InAppReceipt.verifySubscription(type: type, productId: productId, inReceipt: receipt)
        NSLog("\(result)")
        return result
    }
}

// MARK: - SKProductsRequestDelegate delegate methods
extension IAPController:SKProductsRequestDelegate{
    
    //MARK: - Accepts the response from the App Store that contains the requested product information.
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        
        fetchedProductsCompletionBlock?(response.products)
    }
    
    // MARK: - Tells the delegate that the request has completed.
    func requestDidFinish(_ request: SKRequest) {
        
        if iapRequestType == .fetchReceipt{
            // a fresh receipt should now be present at the url
            do {
                let receipt = try Data(contentsOf: appReceiptURL!) //force unwrap is safe here, control can't land here if receiptURL is nil
                validateReceipt(receipt)
            } catch {
                // still no receipt, possible but unlikely to occur since this is the "success" delegate method
            }
        }
    }
    //MARK: - Tells the delegate that the request failed to execute.
    func request(_ request: SKRequest, didFailWithError error: Error) {
        
        switch iapRequestType {
        case .fetchReceipt:
            recieptFailureBlock?(error)
        case .fetchProduct:
            productFetchFailureBlock?(error)
        case .restoreProduct:
            productRestoreFailureBlock?(error)
        default:
            break
        }
    }
}

// MARK: - SKPaymentTransactionObserver delegate methods
extension IAPController:SKPaymentTransactionObserver{
    
    //MARK: - Tells the observer that the payment queue has finished sending restored transactions.
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        
        let alertController = UIAlertController(title: RESTORE_COMPLETED, message: SUCCESSFULLY_RESTORED, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: OK, style: .cancel, handler: nil)
        alertController.addAction(alertAction)
        sharedAppDelegate.window?.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    //MARK: - Tells the observer that an error occurred while restoring transactions.
    func paymentQueue(_ queue: SKPaymentQueue, restoreCompletedTransactionsFailedWithError error: Error) {
        
        switch iapRequestType {
        case .restoreProduct:
            productRestoreFailureBlock?(error)
        default:
            break
        }
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, shouldAddStorePayment payment: SKPayment, for product: SKProduct) -> Bool {
        
        return shouldAddStorePaymentHandler?(payment, product) ?? false
    }
    //MARK: - Tells an observer that one or more transactions have been updated.
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        
        var purchasedPID:Set<String> = []
        var restoredPID:Set<String> = []
        var failedPID:Set<String> = []
        
        var shouldFireCompletionHandler = false
        
        for transaction in transactions {
            switch transaction.transactionState{
            case .purchased:
                AppNetworking.showLoader()
                purchasedPID.insert(transaction.payment.productIdentifier)
                SKPaymentQueue.default().finishTransaction(transaction)
                shouldFireCompletionHandler = true
//                AppNetworking.hideLoader()
            case .restored:
                restoredPID.insert(transaction.payment.productIdentifier)
                SKPaymentQueue.default().finishTransaction(transaction)
                shouldFireCompletionHandler = true
                AppNetworking.hideLoader()
            case .failed:
                failedPID.insert(transaction.payment.productIdentifier)
                SKPaymentQueue.default().finishTransaction(transaction)
                shouldFireCompletionHandler = true
                AppNetworking.hideLoader()
            default:
                break
            }
        }
        if shouldFireCompletionHandler{
            purchaseCompletionBlock?(purchasedPID,restoredPID,failedPID)
        }
    }
}

