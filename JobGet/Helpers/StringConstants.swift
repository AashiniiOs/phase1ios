//
//  StringConstants.swift
//  Onboarding
//
//  Created by Appinventiv on 13/09/17.
//  Copyright © 2017 Gurdeep Singh. All rights reserved.
//

import Foundation


let RESTORE_COMPLETED = "Restore Completed!"
let SUCCESSFULLY_RESTORED = "You have successfully restored your products"
let OK = "OK"
let RESTORE_ERROR = "Restore Error!"
let RESTORE_FAILED = "Restore could not be completed"
let PURCHASE_FAILED = "Purchase failed!" 
let PURCHASE_DISABLED = "Purchases are disabled in your device" 
let FETCH_PRODUCT_ERROR = "Error in fetching products!" 
let SURE_TO_PURCHASE_PRODUCT = "Are you sure you want to purchase this product?" 
let YES = "Yes" 
let NO = "No" 
let DICTIONARY_IS_INVALID = "requested Dictionary is not valid JSON." 
let VALIDATION_URL_NOT_CREATED = "the validation url could not be created." 
let UPLOAD_TASK_ERROR = "the upload task returned an error." 
struct StringConstants {
    
    
    
    //MARK:- Common keys.
    
    
    //MARK:- 
    static let GoogleAPIKey = "AIzaSyAYCpLnWWhNKEkqw4jCViGUzjehocK0Ddo"
    static let FBiD = "fb2085172798444309"
    static let InAppPurchaseSecrateKey = "80d7c8e949e646a1a2d5518dc20a45da"
    
//    key=AAAA6sLErn4:APA91bGIxCvjJgA1UipeeMDuI9Zwxc2eXbjkh79uz6QdgGdhbWqk_I753mGtXH-C2Py6GpmsEUN6F2GS6CQek903ZICF8UastLW6tiRpdU62vv-RNqEEL3KVG2z-Qn7MWJ0dmWRiPARb
    //testing
//    static let FirebasePushKey = "key=AAAAZ_yren0:APA91bHECOwOtubiD7UhJUyTAQnUEjABzcbubj7Dxf3NCsULSc1iTdOj89ZZaW7EF282Dy-Fx_qjMHYwiLM2tTehRg21Iyk3v8xsa_VEx7oG68BR7uKpCWuMc2HSonAGhw37HDa1nG1D"
    
    //live application
    static let FirebasePushKey = "key=AAAAwoeAyd8:APA91bG4zHT3mTn4tUarpwRt4MDZQcXvn4FnnypYHgmctjhaaRYomfVre8IHxfE-z29ZWBntpEEaLawEKrxFtR5SCM2FS5kiqv1t1GjJdqOYcdptqQgdEaEx8mrwPF8ZgOz8SYQYBEuP"

    
    static let APP_TITLE = "ONBOARDING"
    
    static let Single_Plan_Key     = "com.jobget.singleplan"
    static let Preferred_Plan_Key  = "com.jobaget.preferredplan"
    static let Premium_Plan_Key    = "com.jobaget.premiumplan"
    static let Basic_Plan_Key      = "com.jobaget.monthlyplan"
    static let Boston_Lat          = "42.361145"
    static let Boston_Long         = "-71.057083"
    
    //MARK:- Validations
    //==================
    static let Enter_Email = "Enter_Email"
    static let Invalid_Email = "Invalid_Email"
    static let Invalid_First_Name = "Invalid_First_Name"
    static let Invalid_Middle_Name = "Invalid_Middle_Name"
    static let Invalid_Last_Name = "Invalid_Last_Name"
    static let First_Name_Invalid_Length = "First_Name_Invalid_Length"
    static let Invalid_Username = "Invalid_Username"
    static let Enter_First_Name = "Enter_First_Name"
    static let Enter_Middle_Name = "Enter_Middle_Name"
    static let Enter_Last_Name = "Enter_Last_Name"
    static let Last_Name_Invalid_Length = "Last_Name_Invalid_Length"
    
    static let User_Name_Invalid_Length = "User_Name_Invalid_Length"
    
    static let Invalid_Phone = "Invalid_Phone"
    static let Invalid_Password = "Invalid_Password"
    static let Minimum_Age_limit = "Minimum_Age_limit"
    static let Maximum_Age_limit = "Maximum_Age_limit"
    static let Enter_Mobile = "Enter_Mobile"
    static let Enter_Password = "Enter_Password"
    static let Enter_Confirm_Password = "Enter_Confirm_Password"
    static let Confirm_Password_Wrong = "Confirm_Password_Wrong"
    static let Invalid_City_Name = "Invalid_City_Name"
    static let City_Name_Invalid_Length = "City_Name_Invalid_Length"
    static let Enter_City_name = "Enter_City_name"
    
    static let Invalid_Region_Name = "Invalid_Region_Name"
    static let Region_Name_Invalid_Length = "Region_Name_Invalid_Length"
    static let Enter_Region_name = "Enter_Region_name"
    
    static let Invalid_Country_Code = "Invalid_Country_Code"
    static let Enter_Country_code = "Enter_Country_code"
    
    static let Biography_Invalid_Length = "Biography_Invalid_Length"
    static let Enter_Biography = "Enter_Biography"
    
    static let Something_Went_Wrong = "Something_Went_Wrong"
    //MARK:- SignupVC
    //===============
    static let Male = "Male"
    static let Female = "Female"
    static let Other = "Other"
    
    static let First_Name = "First_Name"
    static let Middle_Name = "Middle_Name"
    static let Last_Name = "Last_Name"
    static let User_Name = "User_Name"
    static let Gender = "Gender"
    static let Date_Of_Birth = "Date_Of_Birth"
    static let Phone_No = "Phone_No"
    static let City = "City"
    static let Country_Code = "Country_Code"
    static let Region = "Region"
    static let Longitude = "Longitude"
    static let Latitude = "Latitude"
    static let Postal_Code = "Postal_Code"
    static let Confirm_Password = "Confirm_Password"
    static let Done = "Done"
    
    
    //MARK:- LoginVC
    //==============
    static let Ok = "Ok"
    static let LOGIN = "LOGIN"
    static let Back = "Back"
    static let Email_Address = "Email_Address"
    static let Password = "Password"
    static let Referral_Code_Optional = "Referral Code (Optional)"
    static let Referral_Code = "Referral Code"
    static let FORGOT_PASSWORD = "FORGOT_PASSWORD"
    static let Logout = "Logout"
    static let Email_Title = "         Email Address"
    static let Password_Title = "         Password"
    static let Retry = "Retry"
    
    
    //MARK:- ForgotPasswordVC
    //=======================
    static let Send_OTP_to_email_address = "Send_OTP_to_email_address"
    static let CONTINUE = "CONTINUE"
    
    //MARK:- MyJob VC
    //=============================
    static let NoApplicants = "No Applicants"
    static let Applicants = "Applicants"
    static let Applicant = "Applicant"
    
    //MARK:- HomeViewController
    //=========================
    static let SIGNUP = "SIGN UP"
    static let full_name = "full_name"
    static let first_name = "first_name"
    static let last_name  = "last_name"
    static let email = "email"
    static let password = "password"
    static let device_type = "device_type"
    static let user_type = "user_type"
    static let device_token = "device_token"
    static let device_id = "device_id"
    
    
    //MARK:- ChangePasswordVC
    //=======================
    static let Old_Password = "Old_Password"
    static let New_Password = "New_Password"
    static let CHANGE_PASSWORD = "CHANGE_PASSWORD"
    static let Enter_Old_Password = "Enter_Old_Password"
    static let Invalid_Old_Password = "Invalid_Old_Password"
    static let Enter_New_Password = "Enter_New_Password"
    static let Password_Eight_Char_Long = "Password must be 8 characters long"
    static let Invalid_New_Password = "Invalid_New_Password"
    
    
    //MARK:- ChooseExpPopupVC
    //=======================
    static let ChooseExperience = "CHOOSE EXPERIENCE"
    static let ExperienceRequired = "Experience required ?"
    
    //MARK:- CountryCodeVC
    //=======================
    static let Select_Country = "Select_Country"
    static let Search_Country = "Search_Country"
    
    //MARK:- ResetPasswordVC
    //======================
    static let Enter_New_Password_For_Your_Account = "Enter_New_Password_For_Your_Account"
    static let New_Password_Title = "         New Password"
    
    static let Confirm_Password_Title = "         Confirm Password"
    
    //MARK:- UIViewController Extension
    //=================================
    static let Choose_Options = "Choose_Preferred_Source_Type"
    static let Camera = "Camera"
    static let Camera_not_available = "Camera_not_available"
    static let Choose_from_gallery = "Choose_from_gallery"
    static let Take_Photo = "Take_Photo"
    static let Cancel = "Cancel"
    static let Alert = "Alert"
    static let Account_Already_Exists = "This email already exists."
    static let Oops = "OOPS!"
    
    static let Settings = "Settings"
    static let You_have_been_restricted_from_using_the_camera_on_this_device_without_camera_access_this_feature_wont_work = "You_have_been_restricted_from_using_the_camera_on_this_device_without_camera_access_this_feature_wont_work"
    
    static let Please_change_your_privacy_setting_from_the_Settings_app_and_allow_access_to_camera_for_your_app = "Please_change_your_privacy_setting_from_the_Settings_app_and_allow_access_to_camera_for_your_app"
    static let Youve_been_restricted_from_using_the_library_on_this_device_Without_camera_access_this_feature_wont_work = "Youve_been_restricted_from_using_the_library_on_this_device_Without_camera_access_this_feature_wont_work"
    static let Please_change_your_privacy_setting_from_the_Settings_app_and_allow_access_to_library_for = "Please_change_your_privacy_setting_from_the_Settings_app_and_allow_access_to_library_for"
    
    
    //MARK:- POST JOB controllers
    //=============================
    static let AddImageNumber = "1/9"
    static let JobCategoyNumber = "2/9"
    static let JobDescriptionNumber = "3/9"
    static let JobLocationNumber = "4/9"
    static let JobTypeNumber = "5/9"
    static let SalaryNumber = "6/9"
    static let RequiredExperiencedNumber = "7/9"
    static let CompanyDescription = "8/9"
    static let FeaturedNumber = "9/9"
    
    //MARK:-Add Image VC
    //=============================
    static let AddImage = "Add Image (Optional)"
    static let SalaryOptional = "Salary (Optional)"
    static let RequiredExperianceText = "Does this role require experience? (Optional)"
    
    //MARK:- RecruiterTab Bar VC
    //=============================
    
    static let MyJob = "My Jobs"
    static let Chat = "Message"
    static let Search = "Search"
    static let Star = "Star"
    static let Profile = "Profile"
    
    //MARK: - JobDetailVC
    //=======================
    static let ReadMore = ".. Read more"
    static let ReadLess = "Read Less"
    
    //MARK: - CompanyDescriptionVC
    //=======================
    static let Company_Name                     = "Company Name"
    static let Company_Desc                     = "Company Description"
    static let Company_Web                      = "Company Website"
    static let Company_Address                  = "Company Address"
    static let CompanyLatitude                  = "CompanyLatitude"
    static let CompanyLongitude                 = "CompanyLongitude"
    
    
    //MARK: - JobDescriptionVC
    //=======================
    static let JobTitle = "Job Title"
    static let JobDescription = "Job Description"
    
    //MARK: - FeaturedPostVC
    //=======================
    
    static let MakePostFeatured = "Shine a Spotlight on your Job Posting (Optional)"
    
    //MARK:- CategoryVC
    //=================
    static let CategoryTitle = "Filter"
    static let Category_ResetButton_Title = "Reset"
    static let Category_ApplyButton_Title = "Apply"
    
    //MARK:- SettingsVC
    //=================
    static let pushNotification = "Push Notifications"
    static let changePassword = "Change Password"
    static let invite = "Invite"
    static let featuredPackage = "Featured Package"
    static let RestorePackage = "Restore Package"
    static let Star_Purchase_History = "Star Packages"
    static let rateReview = "Rate & Review App"
    static let termsConditions = "Terms & Conditions"
    static let privacyPloicy = "Privacy Policy"
    static let faq = "FAQ"
    static let  deactivate = "Deactivate Account"
    static let logout = "LOGOUT"
    static let Rate_Review_App = "Rate & Review App"
    static let ContactUS = "Contact Us"
    
    //MARK: CandidateJobDetailVC
    //============================
    static let EmployerName        = "Employer Name"
    static let Applied             = "Applied"
    static let Shortlisted             = "Shortlisted"
    static let Apply               = "Apply"
    static let Company_Description = "Company Description"
    static let Company_Website     = "CompanyWebsite"
    static let WantsFetchLocation  = "Jobget wants to fetch your current location. "
    static let LogoutConfirmation = "ARE YOU SURE YOU WANT TO LOGOUT?"
    static let RejectConfirmation = "ARE YOU SURE YOU WANT TO REJECT THIS SHORTLISTED CANDIDATE? \n\n Once Rejected, you will not be able to connect with user."
    static let DeleteExpConfirm = "ARE YOU SURE YOU WANT TO DELETE MESSAGE? \n Entire conversation will be deleted."
    static let AttributedDelete = "Entire conversation will be deleted."
    static let Deactivate_Account = "DEACTIVATE YOUR ACCOUNT! \n\n Are you sure you want to deactivate your account? You will no longer access to your Profile."
    static let Deactivate_Confirm = "Are you sure you want to deactivate your account? You will no longer access to your Profile."
    static let RejectConfimTwo = "Once you delete all information associated will be removed permanently."
    static let Congratulations     = "Congratulations!\n\n You've just applied to this job. Hear back in 24 hours."
    static let JobsInCalifornia    = "Jobs In California"
    static let California          = "California"
    
    //MARK:- UserDetailViewController
    //===============================
    static let Change_Password = "Change_Password"
    static let Old_Pass = "Old Password"
    static let New_Pass = "New Password"
    static let Confirm_New_Password = "Confirm New Password"
    static let Old_PassTitle = "         Old Password"
    static let New_PassTitle = "         New Password"
    static let Confirm_New_PasswordTitle = "         Confirm New Password"
    
    //MARK: SalaryVC
    //============================
    static let To_Salary_Greater_Then_From_Salary = "Ending salary should not be lower than initial salary"
    
    //MARK: DynamicPopupVC
    //============================
    static let FirstBtnTitle        = "Ok"
    static let SecondBtnTitle       = "Not Now"
    
    
    //MARK: JobListCell
    //============================
    static let JobsIn = "Jobs in"
    
    
    //MARK: JobListCell
    //============================
    static let Company = "Company:"
    static let Experience = "Experience:"
    static let Years       = "years"
    static let Months       = "months"
    static let experience = "Experience"

    static let NoJobsFound = "Oops, there are currently no jobs in your area yet"
    static let Please_try_different_location = "Please select a different location."
    static let Experienc = "Experience"
    
    //MARK:- SalaryRangeCell
    //========================
    static let Salary_Range = "Salary Range"
    
    //MARK:- SortListedStatusPopupVC
    //========================
    static let Status = "Status"
    static let Select_Job = "Select_Job"
    
    
    //MARK:- RecruiterEditProfileVC
    //========================
    static let FirstName = "First Name"
    static let LastName = "Last Name"
    static let Designation = "Designation"
    static let EmailAddress = "Email Address"
    static let Phone_Number = "Mobile Number"
    static let location = "Location"
    static let Education = "Education"
    static let About_Me = "About Me"
    
    
    //MARK:-  JobDetailCell
    //========================
    static let Job_Closed = "(Job Closed)"
    
    
    //MARK:- EditPostedJobVC
    //========================
    static let TipsCommessions = "Tips/Commission"
    
    //MARK:- JobPostContainerVC
    //=======================
    static let SelectSuitableExp = "Please select minimum experience"
    
    //MARK:- Required ExperianceVC
    //========================
    static let fivePlus = "5+"
    static let upTo = "Min"
    static let NoRelevantCategory = "No relevant category"
    static let NoRelevantExperience = "No relevant experience"
    static let NoExpRequired = "No experience required"
    static let YearsExperience = "Years Experience"
    static let MonthExperience = "Months Experience"
    static let NoExperiance = "     No Applicants"
    
    //MARK:- RateApVC
    //========================
    static let writeFeedback = "Write your feedback here"
    
    //MARK:- ChangePasswordVC
    //========================
    static let EnterOldPassword = "Please enter old password"
    static let EnterNewPassword = "Please enter new password"
    static let EnterConfirmPassword = "Please enter confirm password"
    static let Confirm_Password_must_match = "Confirm Password must match"
    
    //MARK:- ProfileUpdateMobileNoVC
    //==============================
    static let MobileCouldNotEmpty = "Phone number could not be empty"
    
    //MARK: - Alert Popup
    //=======================
    static let ReportJob = "ARE YOU SURE YOU WANT TO REPORT THIS JOB? \n\n This job will no longer be visible to you."
    static let VisibleJob = "This job will not be visible to you."
    static let DeleteNotification = "ARE YOU SURE YOU WANT TO DELETE ALL NOTIFICATION FROM THE LIST?"
    static let DeleteSingleNotification = "ARE YOU SURE WANT TO DELETE THIS NOTIFICATION FROM THE LIST?"
    static let ReportEmployer = "ARE YOU SURE YOU WANT TO REPORT THIS EMPLOYER? \n\n Jobs posted by this employer will no longer be visible to you."
    static let EmployerVisibleJob = "Job posted by this employer will not be visible to you"
    static let ReportCandidate = "ARE YOU SURE YOU WANT TO REPORT THIS CANDIDATE? \n\n This candidate will no longer be visible to you"
    static let VisibleReport = "This candidate will not be visible to you."
    static let DeleteJob  = "ARE YOU SURE WANT TO CLOSE THE JOB?"
    
    static let InformationLost = "YOUR PROFILE IS CURRENTLY INCOMPLETE. \n\n If you exit now, your profile information will be lost."
    static let LostInformation = "If you exit now, your profile information will be lost."
    static let LoginOtherDevice = "USER DOES LOGIN NOT EXISTS OR LOGGED IN TO OTHER DEVICE."
    static let LogoutFirst = "Please logout first."
    static let DeleteCardText = "ARE YOU SURE YOU WANT TO DELETE THIS CARD?"
    
    //MARK: - Web Service Toast
    //=======================
    static let UserNotAvailable = "User is currently unavailable to take call."
    
    //MARK: - Star
    //=======================
    static let SelectCardForPayment =  "Select any card for payment"
    static let creditCard =  "Credit Card"
    static let debitCard =  "Debit Card"
    
    static let CardNumber =  "Card Number"
    static let NameonCard =  "Name on Card"
    static let CVC =  "CVV"
    static let Month =  "Month"
    static let Year =  "Year"
    
    static let plz_enter_card_number =  "Please enter card number"
    static let plz_enter_valid_card =  "Please enter a valid card number"
    static let plz_enter_card_holder_name =  "Please enter card holder name"
    
    static let card_holder_name_limit =  "Name must be at least 2 characters long"
    static let please_select_expiry_month =  "Please select card expiry month"
    static let please_select_expiry_year =  "Please select card expiry year"
    static let please_enter_cvv =  "Please enter CVV"
    static let please_enter_valid_cvv =  "Please enter a valid CVV"
    
    static let Please_select_reason_first = "Please select reason first."
    static let Please_Enter_Job_Title = "Please Enter Job Title"
    static let Please_Enter_Job_Description = "Please Enter Job Description"
    static let Please_Enter_From_Salary = "Please enter initial salary"
    static let Please_Enter_To_Salary = "Please enter ending salary"
    static let Please_Enter_Company_Name = "Please Enter Company Name"
    
    static let Please_enter_review = "Please enter review."
    static let Profile_updated_successfully = "Profile updated successfully."
    static let Internet_connection_appears_offline = "The Internet connection appears to be offline."
    static let Please_check_your_internet_connection = "Please check your internet connection."
    static let Check_Camera_Permission = "To answer this video call, please allow JobGet access to your camera."
    static let Check_microphone_Permission = "To answer this video call, please allow JobGet access to your microphone."
    static let Rec_Check_Camera_Permission = "To make this video call, please allow JobGet access to your camera."
    static let Rec_Check_microphone_Permission = "To make this video call, please allow JobGet access to your microphone."
    
    static let Call_disconnected_by = "Call disconnected by"
    static let Busy_message = "is busy on another call. Please try again later."
    static let Connected = "Connected"
    static let Please_select_an_option = "Please select an option"
    static let Please_select_category = "Please select category"
    static let Please_add_company_name = "Please add company name"
    static let Please_enter_job_duration = "Please enter job duration"
    static let Please_select_duration = "Please select duration"
    static let Cannot_logout_user = "Cannot logout user"
    static let Please_wait_profile_image_uploading = "Please wait profile image is uploading."
    
    static let pendingJobTitle = "  Pending   "
    static let shortListedJobTitle = "  ShortListed    "
    static let rejectedJobTitle = "  Rejected    "
    static let expiredJobTitle = "  Expired     "
    static let emptyStringToManage = "   "
    static let Per_Hour = "Per Hour"
    static let Per_Week = "Per Week"
    static let Per_Month = "Per Month"
    static let Per_Year = "Per Year"
    static let from = "From"
    static let to = "To"
    static let PROFILE_PICTURE = "Profile Picture"
    static let SKIP = "SKIP"
    static let FIND_A_JOB_AND_APPLY_NOW = "FIND A JOB AND APPLY NOW"
    static let POST_A_JOB_AND_HIRE_NOW = "POST A JOB AND HIRE NOW"
    static let We_sent_you_link_at = "We've sent you a link at"
    static let to_reset_your_password = "to reset your password"
    static let Total_Job_Posted = "Total Jobs Posted"
    
    static let Recruiter_not_available = "Recruiter not available"
    static let Your_profile_registered = "Your profile registered in:"
    static let No_Jobs_Found = "No Jobs Found"
    static let miles = "miles"
    static let Searching_Address = "Searching Address"
    static let yes = "Yes"
    static let no = "No"
    static let Miles_Away = "Miles Away"
    static let Read_More = " ...Read More"
    static let Not_Selected = "Not Selected"
    static let Optional = "(Optional)"
    static let Min  = "Min"
    static let next = "NEXT"
    static let Save = "SAVE"
    static let required = "Required"
    static let Optionals = "Optional"
    static let COMPLETE_PROFILE = "COMPLETE PROFILE"
    static let CANDIDATES = "CANDIDATES"
    static let No_Candidate_Found = "Oops, no candidates found in this area."
    static let Search_for_candidate = "Please select a different location."
    static let Any_Experienc = "Any Experience"
    static let PENDING_ACTION = "PENDING ACTION"
    static let Shortlist = "Shortlist"
    static let Reject = "Reject"
    static let No_Expired_Requests = "No Expired Requests"
    static let No_Rejected_Candidates = "No Rejected Candidates"
    static let MESSAGE = "MESSAGE"
    static let No_Shortlisted_Candidates = "No Shortlisted Candidates"
    static let Action = "Action"
    static let No_Pending_Request = "No Pending Request"
    static let Postings = "Postings"
    static let Messages = "Messages"
    static let No_Jobs_Posted = "No Jobs Posted"
    static let There_are_many_candidates_looking_for_jobs = "There are so many candidates looking \nfor jobs!"
    static let Status_Normal = "Status: Standard"
    static let Status_Featured = "Status: Spotlight!"
    static let Part_Time = "Part Time"
    static let Full_Time = "Full Time"
    static let Part_Full_Time = "Part/Full Time"
    static let No_Experience = "No Experience"
    static let Are_you_sure_you_want_delete_message = "Are you sure you want to delete the message."
    static let Are_you_sure_want_delete_experience = "Are you sure you want to delete the experience."
    
    static let No_Applied_Jobs = "No Applied Jobs"
    static let Worked_at = "Worked at"
    static let month_of_total_experience = "month of total experience"
    static let year_of_total_experience = "year of total experience"
    static let months_of_total_experience = "months of total experience"
    static let years_of_total_experience = "years of total experience"
    
    static let Call = "Call"
    static let Missed_Call = "Missed Call"
    static let Cancelled_Call = "Cancelled Call"
    static let This_message_has_been_deleted = "This message has been deleted"
    static let month_of_experience = "month of experience"
    static let year_of_experience = "year of experience"
    
    static let months_of_experience = "months of experience"
    static let years_of_experience = "years of experience"
    
    static let SAVE_JOB = "SAVE JOB"
    static let Ago = "Ago"
    static let Hear_back_in = "Hear back in"
    static let Jobget_cannot_access_your_location_Please_allow_from_settings = "Jobget cannot access your location. Please allow from settings"
    static let Allow = "Allow"
    static let Deny = "Don't Allow"
    static let DEACTIVATES = "DEACTIVATE"
    static let NOT_NOW = "NOT NOW"
    
    static let EXIT = "EXIT"
    static let LOGOFF = "LOGOFF"
    static let COMPLETE = "COMPLETE"
    static let NO_COMPLETE = "NO, COMPLETE"
    static let Newest = "Newest"
    static let Nearby = "Nearby"
    static let You_have_successfully_changed_your_password = "You have successfully changed your password"
    static let You_successfully_verified_number = "You have successfully verified your number"
    static let You_just_applied_this_job_Hear_back_hours = "You've just applied to this job. Hear back in 24 hours."
    static let EDIT = "EDIT"
    static let Category_Name = "Category Name"
    static let Hide = "Hide"
    static let Show = "Show"
    static let RESEND_OTP = "RESEND OTP"
    static let Applications = "Applications"
    static let Home = "Home"
    static let Type_message = "Type a message"
    static let copy = "Copy"
    static let delete = "Delete"
    static let You_can_only_start_video_call_candidate_replied_message = "You can only start video call after the candidate replied to the message."
    static let No_Messages_Yet = "No Messages Yet"
    static let Candidate_not_available = "Candidate not available"
    static let No_Calls_Yet = "No Calls Yet"
    static let No_Notification_Found = "No Notification Found"
    static let All = "All"
    static let Candidate_Search_Placeholder_text = "Search(ex: waiter, bartender, starbucks...)"
    static let Recruiter_Search_Placeholder_text = "Search(ex: retail, food & beverage, starbucks..) "
    static let Location = "Location"
    static let Categories = "Categories"
    static let Sort_by = "Sort by"
    static let Recent_Search = "Recent Search"
    static let Month_experience = "Month experience"
    static let Year_experience = "Year experience"
    static let Months_experience = "Months experience"
    static let Years_experience = "Years experience"
    static let No_Shortlisted_Jobs = "You have not been shortlisted by an employer yet"
    static let You_saved_jobs_yet_Save_that_you_apply_later = "You haven\'t saved any jobs yet. \nSave now so that you can apply later."
    static let No_Saved_Jobs = "No Saved Jobs"
    static let Job_Category = "Job Category"
    static let Job_Location = "Job Location"
    static let Feature_this_post = "Spotlight this job"
    static let Post_featured = "Spotlighted Job"
    static let Job_spotlighted = "Job spotlighted"
    static let Total_No_Stars = "Total no of stars"
    static let Payment_Method = "Total no of stars"
    static let Stars = "Stars"
    static let Are_you_sure_you_want_to_purchase_this_plan = "Are you sure you want to purchase this plan."
    static let Would_You_Like_To_Purchase = "Would you like to purchase"
    static let For = "for"
    static let Update_The_Preferred_Location = "UPDATE THE PREFERRED LOCATION"
    static let Your_Preferred_Location = "Your preferred location is:"
    static let APPLICANTS = "APPLICANTS"
    static let JOB_DETAIL = "JOB DETAIL"
    static let Your_Call_Disconnected_In = "Call will be disconnected in"
    static let seconds = "seconds"
    static let No_Package_Purchased = "No Package Purchased"
    
    static let Purchase_Date = "Purchase Date:"
    static let Expire_Date = "Expire Date:"
    static let Plan_Price = "Plan Price: "
    static let Subscription_Type = "Subscription Type: "
    static let Monthly = "Monthly"
    static let Error = "Error"
    static let Shortlist_candidate_Before_message = "Inorder to message or video call, Candidate must be shortlisted for at least one active job."
    static let Consume_Star_Before_message = "Please consume the 1 star before initiating message. Star will be consume only once for a candidate."
    static let Consume_Star_Before_call = "Before using video call, please message the candidate first by clicking on 'Start chat for 1 star' button below."

    static let Shortlist_candidate_Before_call = "Inorder to message or video call, Candidate must be shortlisted for at least one active job."
    static let Card_payment_sucess_alert = "Congratulations! Your Spotlight job will now be seen by more candidates."
    static let You_reported_the_candidate_So_you_cannot_initiate_call = "You reported the candidate. So you cann't initiate call."
    static let You_reported_the_recruiter_So_you_cannot_initiate_message = "You reported the recruiter. So you cann't initiate message."
    static let You_reported_the_candidate_So_you_cannot_initiate_message = "You reported the candidate. So you cann't initiate message."
    static let PAUSE = "PAUSE"
    static let PAUSE_ACCOUNT = "Pause Account"
    static let DELETE_ACCOUNT = "Delete Account"
    static let CANDIDATE_PAUSE_ACCOUNT_DESCRIPTION = "If you have recently found a job, congratulations! You can pause your JobGet account until you are ready to job search again. When your account is paused, it will not be visible to anyone and notifications will be stopped, but your profile, job applications, and message history will be right here when you return."
    
    static let CANDIDATE_DELETE_ACCOUNT_DESCRIPTION = "If you delete your JobGet account, it will immediately be deactivated and permanently deleted after 30 days. When your account is deleted, you will permanently lose all your profile, job applications and message history."
    
    static let CANDIDATE_PAUSE_ACCOUNT_POPUP_TEXT = "Great! We will go ahead and pause your account for you. When you are ready to job search again, simply login to JobGet and we will unpause your account."
    static let CANDIDATE_DELETE_ACCOUNT_POPUP_TEXT = "Your account has been successfully deactivated and will be deleted after 30 days. Thanks for using JobGet and we hope to see you again soon."
    
    static let RECRUITER_PAUSE_ACCOUNT_DESCRIPTION = "If you have recently filled a job position, congratulations! You can pause your account until you're ready to hire again. An account on pause means that your profile and any existing job postings will not be visible to anyone, but your jobs, previous applicants, shortlisted candidates, stars, and message history will be right here when you return."
    
    static let RECRUITER_PAUSE_ACCOUNT_POPUP_TEXT = "Great! We will go ahead and pause your account for you. When you are ready to hire again, simply login to JobGet and we will unpause your account."
    
    static let RECRUITER_DELETE_ACCOUNT_DESCRIPTION = "If you delete your JobGet account, it will immediately be deactivated and permanently deleted after 30 days. When your account is deleted, you will permanently lose any job postings, candidates, shortlists, message history, stars, promotions, or credits linked to your account."
    
    static let DEACTIVATE_ACCOUNT_TITLE_TEXT = ", we're sorry to see you go. Mind sharing with us the reason why you are deleting your account?"
    
    
    static let I_have_recently_found_job_JobGet = "I have recently found a job on JobGet"
    static let I_have_recently_found_job_another_app_website = "I have recently found a job on another app or website"
    static let I_receive_too_many_notifications = "I receive too many notifications"
    static let There_are_not_enough_jobs = "There are not enough jobs"
    static let The_app_difficult_to_use = "The app is difficult to use"
    static let Other_followed_by_free_text_field = "Other"
    
    static let I_have_recently_hired_candidate_found_JobGet = "I have recently hired a candidate found on JobGet"
    static let I_have_recently_hired_candidate_found_another_app_website = "I have recently hired a candidate found on another app or website"
    static let The_position_not_needed_and_was_closed = "The position is not needed and was closed"
    static let There_are_not_enough_candidates = "There are not enough candidates"
    static let Write_Here = "Write here..."
    static let Please_Enter_Other_Text = "Please Write your comments."
    static let Front_camera = "Front camera"
    static let Back_camera = "Back camera"
    
    static let Video_enabled = "Video enabled"
    static let Video_disabled = "Video disabled"
    static let Audio_enabled = "Audio enabled"
    static let Audio_disabled = "Audio disabled"
    static let Speaker_enabled = "Speaker enabled"
    static let Speaker_disabled = "Speaker disabled"
    static let Please_Consume_Star = "PLEASE CONSUME STAR"
    static let REPORTED = "REPORTED!"
    static let Please_Shortlist_Candidate = "PLEASE SHORTLIST CANDIDATE"
    static let You_have_been_Shortlist = "You have been shortlisted by"
    static let send_them_Message = "send them a message!"
    static let has_applied_for = "has applied for"
    static let job_decription_text_limit = "Text exceeds the 2000 character limit, please keep your description short and concise."
    static let Please_Create_at_least_one_job = "Oops, this candidate had applied to your job but was not responded to within 24 hours. Please be sure to shortlist candidates you are interested in right away before they expire."
    
    static let Location_Allow_Title = "Allow jobget to access your location while you are using the app?"
    static let Fetch_Location_Reason_Text = "Nearby jobs posts will be filtered/sorted and search candidate based on location."
    static let Spotlight_Job_Toast_Text = "Spotlight this job and it will be one of the first jobs candidates see."
    static let Great = "GREAT!"
    static let Contact_Us_AlertText = "We loving hearing from you. Let us get back to you shortly."
}



