//
//  POIItem.swift
//  ClusteringDemo
//
//  Created by Appinventiv on 03/11/17.
//  Copyright © 2017 Appinventiv. All rights reserved.
//

import Foundation

/// Point of Interest Item which implements the GMUClusterItem protocol.
class POIItem: NSObject, GMUClusterItem {
    var position: CLLocationCoordinate2D
    var name: String
    var index: Int
    
    init(position: CLLocationCoordinate2D, name: String, index: Int) {
        self.position = position
        self.name = name
        self.index = index

    }
}
