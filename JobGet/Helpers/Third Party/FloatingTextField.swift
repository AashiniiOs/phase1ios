//
//  FloatingTextField.swift
//  BlackPrivilege
//
//  Created by Arpit Arora on 09/02/18.
//  Copyright © 2018 Appinventiv. All rights reserved.
//

import UIKit
//import SkyFloatingLabelTextField

public class FloatingTextField: SkyFloatingLabelTextFieldWithIcon {
    
//    var validity = true
//    {
//        didSet {
//            self.warning.isHidden = validity
//            self.lineColor = validity == false ? AppColors.errorColor : AppColors.textFieldBorderColor
//        }
//    }
    
    let warning = UILabel()
    
    let lightGreyColor: UIColor = UIColor(red: 197 / 255, green: 205 / 255, blue: 205 / 255, alpha: 1.0)
    let darkGreyColor: UIColor = UIColor(red: 52 / 255, green: 42 / 255, blue: 61 / 255, alpha: 1.0)
    let overcastBlueColor: UIColor = UIColor(red: 0, green: 187 / 255, blue: 204 / 255, alpha: 1.0)
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupTheme()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setupTheme()
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        self.setupFrames()
    }
    
    private func setupTheme() {
        setupSubviews()
    }
    
    private func setupSubviews(){
        self.addSubview(warning)

//        self.warning.font = AppFonts.Montserrat_SemiBold.withSize(10.0)
//        self.warning.textAlignment = .left
//        self.warning.textColor = AppColors.errorColor
//        self.warning.textAlignment = .left
//        self.warning.isHidden = validity
    }
    
    private func setupFrames() {
        self.warning.frame = CGRect(x: bounds.origin.x, y: bounds.height+2, width: bounds.width, height: 16)
    }
    
    func setIconImage(img : UIImage, size: CGSize?) {
        
        if let imgSize = size {
            self.iconWidth = imgSize.width
        } else {
            self.iconWidth = 20
        }
        
        self.iconType = .image
        self.iconImage = img
    }
    
//    // MARK: - Styling the text fields to the Skyscanner theme
//
//    func applySkyscannerThemeWithIcon(font: UIFont, hideIcon: Bool = false) {
//        self.applySkyscannerTheme(font: font)
//
//        self.iconColor = AppColors.textFieldBorderColor
//        self.selectedIconColor = AppColors.themeColor
//        self.iconType = .image
//
//        if hideIcon{
//            self.iconWidth = 0.0
//        }
//    }
//
//    func applySkyscannerTheme(font: UIFont) {
//
//        self.tintColor = AppColors.themeColor
//
//        self.textColor = AppColors.white
//        self.lineColor = AppColors.textFieldBorderColor
//        self.placeholderColor = AppColors.textFieldBorderColor
//
//        self.selectedTitleColor = AppColors.themeColor
//        self.selectedLineColor = AppColors.themeColor
//
//        self.lineHeight = 1.0
//        self.selectedLineHeight = 1.0
//
//        // Set custom fonts for the title, placeholder and textfield labels
//        self.titleFont = AppFonts.Montserrat_SemiBold.withSize(12.0)
//        self.placeholderFont = font
//        self.font = font
//    }
//
//    func applyFontAndColor(font: UIFont, hideIcon: Bool = false){
//
//        self.tintColor = AppColors.themeColor
//
//        self.textColor = AppColors.blackTextColor
//        self.lineColor = AppColors.textFieldBorderColor
//        self.placeholderColor = AppColors.textFieldBorderColor
//
//        self.selectedTitleColor = AppColors.themeColor
//        self.selectedLineColor  = AppColors.themeColor
//
//        self.lineHeight = 1.0
//        self.selectedLineHeight = 1.0
//
//        // Set custom fonts for the title, placeholder and textfield labels
//        self.titleFont = AppFonts.Montserrat_SemiBold.withSize(12.0)
//        self.placeholderFont = font
//        self.font = font
//
//        self.iconColor = AppColors.textFieldBorderColor
//        self.selectedIconColor = AppColors.themeColor
//        self.iconType = .image
//
//        if hideIcon{
//            self.iconWidth = 0.0
//        }
//    }
//    //this method is override just to change default placeholder which skyfloat change to lightgrey
//    public override func resignFirstResponder() -> Bool {
//        let result = super.resignFirstResponder()
//        self.placeholderColor = AppColors.textFieldBorderColor
//        return result
//    }
}
