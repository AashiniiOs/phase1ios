//
//  OTPTextField.swift
//  BlackPrivilege
//
//  Created by appinventiv on 13/11/17.
//  Copyright © 2017 Appinventiv. All rights reserved.
//

import UIKit

class OTPTextField: UITextField {
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        configureTextField()
    }
    required override init(frame: CGRect) {
        super.init(frame: frame)
        configureTextField()
    }
    
    private func configureTextField(){
        setCornerRadiusAndBorder()
        updateFont()
        normalBackgroundColor()
    }
    
    private func setCornerRadiusAndBorder(){
        //self.layer.borderColor = AppColors.textFieldBorderColor.cgColor
        //self.layer.borderWidth = 1.0
        self.layer.masksToBounds = true
        self.layer.cornerRadius = 5.0
        self.clipsToBounds = true
    }
    
    private func updateFont(){
        self.font = AppFonts.Poppins_Regular.withSize(20)
    }
    
    func normalBackgroundColor(){
        self.backgroundColor = #colorLiteral(red: 0.9215686275, green: 0.9215686275, blue: 0.9215686275, alpha: 1)
        self.textColor = AppColors.black46
    }
    
//    func filledBackgroundColor(){
//        self.backgroundColor = AppColors.themeColor
//        self.textColor = AppColors.white
//    }
    
    override func deleteBackward() {
        super.deleteBackward()
    }
}
