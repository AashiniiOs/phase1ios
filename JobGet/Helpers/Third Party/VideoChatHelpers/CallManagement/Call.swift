/*
	Copyright (C) 2016 Apple Inc. All Rights Reserved.
	See LICENSE.txt for this sample’s licensing information
	
	Abstract:
	Model class representing a single call
*/

import SwiftyJSON
import Foundation

final class Call {

    // MARK: Metadata Properties
    var caller = Caller(json: JSON())

    let uuid: UUID
    var sender_name: String = ""
    var receiver_name: String = ""
    var sender_id: String = ""
    var receiver_id: String = ""
    var sender_image: String = ""
    var receiver_image: String = ""
    var device_id: String = ""
    var device_token: String = ""
    var type: String = ""
    var senderTwilioToken: String = ""
    var twilioToken: String = ""
    var senderDeviceToken: String = ""
    var senderImage: String = ""
    
    let hasVideo: Bool = true
    var device_type: String = ""

    // MARK: Call State Properties

    var connectingDate: Date? {
        didSet {
            stateDidChange?()
            hasStartedConnectingDidChange?()
        }
    }
    var connectDate: Date? {
        didSet {
            stateDidChange?()
            hasConnectedDidChange?()
        }
    }
    var endDate: Date? {
        didSet {
            stateDidChange?()
            hasEndedDidChange?()
        }
    }
    var isOnHold = false {
        didSet {
            stateDidChange?()
        }
    }

    // MARK: State change callback blocks

    var stateDidChange: (() -> Void)?
    var hasStartedConnectingDidChange: (() -> Void)?
    var hasConnectedDidChange: (() -> Void)?
    var hasEndedDidChange: (() -> Void)?

    // MARK: Derived Properties

    var hasStartedConnecting: Bool {
        get {
            return connectingDate != nil
        }
        set {
            connectingDate = newValue ? Date() : nil
        }
    }
    var hasConnected: Bool {
        get {
            return connectDate != nil
        }
        set {
            connectDate = newValue ? Date() : nil
        }
    }
    var hasEnded: Bool {
        get {
            return endDate != nil
        }
        set {
            endDate = newValue ? Date() : nil
        }
    }
    var duration: TimeInterval {
        guard let connectDate = connectDate else {
            return 0
        }

        return Date().timeIntervalSince(connectDate)
    }
    
    var startCallCompletion: ((Bool) -> Void)?

    // MARK: Initialization

    init(uuid: UUID,device_type: String, device_id: String, device_token: String,senderToken: String,receiverToken:String,senderId:String,receiverId:String,senderName: String,receiverName:String) {
        self.caller?.uuid = uuid
        self.caller?.device_type = device_type
        self.caller?.device_id = device_id
        self.caller?.device_token = device_token
        self.caller?.senderTwilioToken = receiverToken
        self.caller?.twilioToken = senderToken
        self.caller?.sender_id = senderId
        self.caller?.receiver_id = receiverId
        self.caller?.receiver_name = receiverName
        self.caller?.sender_name = senderName

        self.uuid = uuid
        self.device_type = device_type
        self.device_id = device_id
        self.device_token = device_token
        self.senderTwilioToken = receiverToken
        self.twilioToken = senderToken
        self.sender_id = senderId
        self.receiver_id = receiverId
        self.receiver_name = receiverName
        self.sender_name = senderName

    }

    // MARK: Actions
    func start(completion: ((_ success: Bool) -> Void)?) {
        startCallCompletion = completion
    }

    func answer() {

        hasStartedConnecting = true
        sharedAppDelegate.invalidateTimeoutTimer()
//  sharedAppDelegate.moveToVideoChatScene(with: Caller(json: JSON(self))!, fromPush: false)
        if let callerDetail = self.caller{
            print_debug(callerDetail.twilioToken)
            print_debug(callerDetail.senderTwilioToken)
            
             sharedAppDelegate.moveToVideoChatScene(with: callerDetail, fromPush: true, fromCallKit: true)
        }
       
    }

    func end() {
        hasEnded = true
        sharedAppDelegate.invalidateTimeoutTimer()

        // Notify user busy

        //Disconnect socket
        CommonClass.delayWithSeconds(2.0) {
//            sharedAppDelegate.socket.disconnect()
        }
    }
    
    private func sendBusyToPeer(_ loginId: String) {
        let json = ["type": "busy", "name": loginId]
        CommonClass.writeToSocket(json)
    }
}
