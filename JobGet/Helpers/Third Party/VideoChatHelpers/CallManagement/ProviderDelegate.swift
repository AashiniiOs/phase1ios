/*
 Copyright (C) 2016 Apple Inc. All Rights Reserved.
 See LICENSE.txt for this sample’s licensing information

 Abstract:
 CallKit provider delegate class, which conforms to CXProviderDelegate protocol
 */

import UIKit
import CallKit

@available(iOS 10.0, *)
final class ProviderDelegate: NSObject {

    let callManager: CallManager
    let provider: CXProvider
    
    init(callManager: CallManager) {
        self.callManager = callManager
        
//        let providerConfiguration = CXProviderConfiguration(localizedName: "Video")

    /// The app's provider configuration, representing its CallKit capabilities
        var providerConfiguration: CXProviderConfiguration {

        let providerConfiguration = CXProviderConfiguration(localizedName: "Jobget")
            
        providerConfiguration.supportsVideo = false
        
        providerConfiguration.maximumCallsPerCallGroup = 1

        providerConfiguration.supportedHandleTypes = [.phoneNumber]

        if let iconMaskImage = UIImage(named: "icHomeDetailVideo1") {
            providerConfiguration.iconTemplateImageData = UIImagePNGRepresentation(iconMaskImage)
        }

        //providerConfiguration.ringtoneSound = "Ringtone.caf"

        return providerConfiguration
    }
        
        if #available(iOS 11.0, *) {
            providerConfiguration.includesCallsInRecents = false
        }
        
        provider = CXProvider(configuration: providerConfiguration)
        
        super.init()

        provider.setDelegate(self, queue: nil)
    }

    // MARK: Incoming Calls

    /// Use CXProvider to report the incoming call to the system
    func reportIncomingCall(for caller: Caller, completion: ((NSError?) -> Void)? = nil) {
        // Construct a CXCallUpdate describing the incoming call, including the caller.
        let update = CXCallUpdate()
        update.remoteHandle = CXHandle(type: .phoneNumber, value: caller.sender_id)
        update.localizedCallerName = caller.sender_name
        update.hasVideo = true
        //update.remoteHandle = nil

        // Report the incoming call to the system
        provider.reportNewIncomingCall(with: caller.uuid, update: update) { error in
            /*
             Only add incoming call to the app's list of calls if the call was allowed (i.e. there was no error)
             since calls may be "denied" for various legitimate reasons. See CXErrorCodeIncomingCallError.
             */
            if error == nil {
                
                let call = Call(uuid: caller.uuid, device_type: caller.device_type, device_id: caller.device_id, device_token: caller.device_token, senderToken: caller.twilioToken, receiverToken: caller.senderTwilioToken, senderId: caller.sender_id, receiverId: caller.receiver_id,senderName: caller.sender_name,receiverName: caller.receiver_name)
                self.callManager.removeAllCalls()
                self.callManager.add(call)
                configureAudioSession()
            }

            completion?(error as NSError?)
        }
    }
}
// MARK: - CXProviderDelegate

@available(iOS 10.0, *)
extension ProviderDelegate: CXProviderDelegate {

    func providerDidReset(_ provider: CXProvider) {

        stopAudio()

        /*
         End any ongoing calls if the provider resets, and remove them from the app's list of calls,
         since they are no longer valid.
         */
        for call in callManager.calls {
            call.end()
        }

        // Remove all calls from the app's list of calls.
        callManager.removeAllCalls()
    }

    func provider(_ provider: CXProvider, perform action: CXStartCallAction) {
        
        let userId = AppUserDefaults.value(forKey: .userId).stringValue
        let call = Call(uuid: action.callUUID, device_type: "2", device_id: (UIDevice.current.identifierForVendor?.uuidString)!, device_token: DeviceDetail.deviceToken, senderToken: "", receiverToken: "", senderId: userId, receiverId: "", senderName: User.getUserModel().first_name, receiverName: "")

        /*
         Configure the audio session, but do not start call audio here, since it must be done once
         the audio session has been activated by the system after having its priority elevated.
         */
        configureAudioSession()

        /*
         Set callback blocks for significant events in the call's lifecycle, so that the CXProvider may be updated
         to reflect the updated state.
         */
        call.hasStartedConnectingDidChange = { [weak self] in
            self?.provider.reportOutgoingCall(with: call.uuid, startedConnectingAt: call.connectingDate)
        }
        call.hasConnectedDidChange = { [weak self] in
            self?.provider.reportOutgoingCall(with: call.uuid, connectedAt: call.connectDate)
        }

        // Trigger the call to be started via the underlying network service.
        call.start { success in
            if success {
                // Signal to the system that the action has been successfully performed.
                action.fulfill()

                // Add the new outgoing call to the app's list of calls.
                self.callManager.add(call)
            } else {
                // Signal to the system that the action was unable to be performed.
                action.fail()
            }
        }
    }

    func provider(_ provider: CXProvider, perform action: CXAnswerCallAction) {
        // Retrieve the Call instance corresponding to the action's call UUID
        guard let call = callManager.callWithUUID(uuid: action.callUUID) else {
            action.fail()
            return
        }

        /*
         Configure the audio session, but do not start call audio here, since it must be done once
         the audio session has been activated by the system after having its priority elevated.
         */
        configureAudioSession()

        // Trigger the call to be answered via the underlying network service.
        call.answer()

        // Signal to the system that the action has been successfully performed.
        action.fulfill()
    }

    func provider(_ provider: CXProvider, perform action: CXEndCallAction) {
        // Retrieve the Call instance corresponding to the action's call UUID
        guard let call = callManager.callWithUUID(uuid: action.callUUID) else {
            action.fail()
            return
        }
        let json: JSONDictionary = ["type": SocketDataType.decline.rawValue, "name": User.getUserModel().user_id ?? "" , "otherName":call.sender_id]
        CommonClass.writeToSocket(json)
        // Stop call audio whenever ending the call.
        stopAudio()

        // Trigger the call to be ended via the underlying network service.
        callManager.calls.forEach { call in
            call.end()
        }
        callManager.removeAllCalls()

        // Signal to the system that the action has been successfully performed.
        action.fulfill()

    }

    func provider(_ provider: CXProvider, perform action: CXSetHeldCallAction) {
        // Retrieve the Call instance corresponding to the action's call UUID
        guard let call = callManager.callWithUUID(uuid: action.callUUID) else {
            action.fail()
            return
        }

        // Update the Call's underlying hold state.
        call.isOnHold = action.isOnHold

        // Stop or start audio in response to holding or unholding the call.
        if call.isOnHold {
            stopAudio()
        } else {
            startAudio()
        }

        // Signal to the system that the action has been successfully performed.
        action.fulfill()
    }

    func provider(_ provider: CXProvider, timedOutPerforming action: CXAction) {
        print_debug("Timed out \(#function)")

        // React to the action timeout if necessary, such as showing an error UI.
    }

    func provider(_ provider: CXProvider, didActivate audioSession: AVAudioSession) {
        print_debug("Received \(#function)")
//        RTCAudioSession.sharedInstance().audioSessionDidActivate(audioSession)

        // Start call audio media, now that the audio session has been activated after having its priority boosted.
//        startAudio()
    }

    func provider(_ provider: CXProvider, didDeactivate audioSession: AVAudioSession) {
        print_debug("Received \(#function)")
//        RTCAudioSession.sharedInstance().audioSessionDidDeactivate(audioSession)

        /*
         Restart any non-call related audio now that the app's audio session has been
         de-activated
         after having its priority restored to normal.
         */
        callManager.calls.forEach { call in
            call.end()
        }
        callManager.removeAllCalls()

        stopAudio()
    }

}

