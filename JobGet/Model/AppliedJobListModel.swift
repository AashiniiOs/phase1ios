//
//  AppliedJobListModel.swift
//  JobGet
//
//  Created by Appinventiv on 24/05/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import Foundation
import SwiftyJSON

struct AppliedJobListModel {
    
    var categoryName: String
    var duration: String
    var fromExperience: Int
    var toExperience : Int
    let totalApplyCount: Int
    var categoryId: String
    let totalViewCount: Int
    var created_at: String
    var updated_at: String
    var jobApply_Time: String
    let userId: String
    var first_name: String
    var last_name: String
    var userImage: String
    var education: String
    var state: String
    var city: String
    var about: String
    let shareUrl: String
    var experience : [ExperienceModel]
    var categoryTitle = [String]()
    var expType: Int // 1 for Full time and 2 for part time 3 for Both
    
    var applyTime: Date? {
        
        guard !self.jobApply_Time.isEmpty else{
            return nil
        }
        
        guard let time = Int64(self.jobApply_Time) else {
            return nil
        }
        
        let date = dateFromMilliseconds(timestamp: time)
        return date
    }
    
    var createdAt: Date? {
        
        guard !self.created_at.isEmpty else{
            return nil
        }
        
        guard let time = Int64(self.created_at) else {
            return nil
        }
        
        let date = dateFromMilliseconds(timestamp: time)
        return date
    }
    
    var updatedAt: Date? {
        
        guard !self.updated_at.isEmpty else{
            return nil
        }
        
        guard let time = Int64(self.updated_at) else {
            return nil
        }
        
        let date = dateFromMilliseconds(timestamp: time)
        return date
    }
    
    init(dict: JSON) {
        
        self.categoryName           = dict["categoryName"].stringValue
        
        self.fromExperience         = dict["fromExperience"].intValue
        self.toExperience              = dict["toExperience"].intValue
        self.duration           = dict["duration"].stringValue
        self.totalApplyCount           = dict["totalApplyCount"].intValue
        self.categoryId            = dict["categoryId"].stringValue
        self.totalViewCount           = dict["totalViewCount"].intValue
        self.userId           = dict["userId"].stringValue
        self.first_name  = dict["first_name"].stringValue
        self.last_name             = dict["last_name"].stringValue
        self.userImage        = dict["userImage"].stringValue
        self.education         = dict["education"].stringValue
        self.about             = dict["about"].stringValue
        self.expType         = dict["expType"].intValue
        self.updated_at   = dict["updated_at"].stringValue
        self.created_at   = dict["created_at"].stringValue
        self.jobApply_Time  = dict["applyTime"].stringValue
        self.shareUrl  = dict["shareUrl"].stringValue
        self.state   = dict["state"].stringValue
        self.city          = dict["city"].stringValue
        self.experience = dict["experience"].arrayValue.map({ExperienceModel(withJSON: $0)})
        
        let _ = dict["categoryTitle"].arrayValue.filter({ (element) -> Bool in
            self.categoryTitle.append(element.stringValue)
            return true
        })
    }
    
}

