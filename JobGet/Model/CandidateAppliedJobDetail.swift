//
//  CandidateAppliedJobDetail.swift
//  JobGet
//
//  Created by Appinventiv on 24/05/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import Foundation


import SwiftyJSON

struct CandidateAppliedJobDetail {
    
    var first_name: String
    var last_name: String
    var isExperience: Int
    var user_image: String
    var education: String
    var email: String
    let mobile: String
    var id : String
    var jobLocation: String
    var jobLongitude    : Double
    var jobLatitude    : Double
    var status: Int
    let user_id: Int
    var createdTimeStamp: String
    var updated_at: String
    var shareUrl : String
    var about: String
    var jobType: Int
    let user_type : Int
    var state: String
    var city: String
    var profileView: Int
     var isSearch: Int
    var experience : [ExperienceModel]
    var jobsInfo:[CandidateAppliedJobInfo]
    var user_login_session : [LoginSession]
    var visitorImageList =  [String]()
    
    var createdAt: Date? {
        
        guard !self.createdTimeStamp.isEmpty else{
            return nil
        }
        
        guard let time = Int64(self.createdTimeStamp) else {
            return nil
        }
        
        let date = dateFromMilliseconds(timestamp: time)
        return date
    }
    
    var updatedAt: Date? {
        
        guard !self.updated_at.isEmpty else{
            return nil
        }
        
        guard let time = Int64(self.updated_at) else {
            return nil
        }
        
        let date = dateFromMilliseconds(timestamp: time)
        return date
    }
    
    init(dict: JSON) {
        
        self.first_name           = dict["first_name"].stringValue
        self.last_name     = dict["last_name"].stringValue
        self.user_image         = dict["user_image"].stringValue
        self.isExperience              = dict["isExperience"].intValue
        self.education           = dict["education"].stringValue
        self.email           = dict["email"].stringValue
        self.mobile            = dict["mobile"].stringValue
        
        self.status         = dict["status"].intValue
        
        self.about           = dict["about"].stringValue
        self.user_type           = dict["user_type"].intValue
         self.isSearch          = dict["isSearch"].intValue
        self.status             = dict["status"].intValue
        self.profileView             = dict["profileView"].intValue
        self.user_id             = dict["_id"].intValue
        self.id                  = dict["_id"].stringValue
        
        self.updated_at   = dict["updated_at"].stringValue
        
        self.createdTimeStamp   = dict["created_at"].stringValue
        self.shareUrl          = dict["shareUrl"].stringValue
        self.jobLatitude          = dict["lat"].doubleValue
        self.jobLongitude          = dict["lng"].doubleValue
        self.jobLocation        = dict["address"].stringValue
        
        self.state   = dict["state"].stringValue
        self.city          = dict["city"].stringValue
        
        let sortedExperience = CommonClass.sortedInHeighestExperience(dict["experience"].arrayValue)
        self.experience = sortedExperience.map({ExperienceModel(withJSON: $0)})
        self.jobType        = dict["jobType"].intValue
        
        self.jobsInfo = dict["jobs"].arrayValue.map{
            
            return CandidateAppliedJobInfo(dict: $0)
            
        }
        self.user_login_session = dict["user_login_session"].arrayValue.map({LoginSession(withJSON: $0)})

        
        
        dict["visitors"].arrayValue.forEach { (image) in
            self.visitorImageList.append(String(describing: image))
        }
    }
}

struct CandidateAppliedJobInfo {
    
    let jobId: String
    let jobTitle: String
    let categoryId: String
    
    var dictionaryValue : [String:Any]{
        
        var details = [String:Any]()
        
        details["jobTitle"] = jobTitle
        details["_id"] = jobId
        
        return details
        
    }
    
    init(dict: JSON) {
        self.jobTitle            = dict["jobTitle"].stringValue
        self.jobId           = dict["_id"].stringValue
        self.categoryId   = dict["categoryId"].stringValue
    }
}

