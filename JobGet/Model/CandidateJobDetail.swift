//
//  CandidateJobDetail.swift
//  JobGet
//
//  Created by macOS on 02/05/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import Foundation
import SwiftyJSON

struct CandidateJobDetail {
    
    let recruiterDetail: JobListRecruiterDetail?
    let jobTitle: String
    let jobDescription: String
    var duration: String
    // let experience: Int
    let extraCompensation: String
    let isApplied : Int
    let isExp: Int
    let jobCity: String
    let jobImage: String
    let jobState: String
    let jobLongitude    : Double
    let jobLatitude    : Double
    let salaryFrom: String
    let salaryTo: String
    let jobShareUrl: String
    let status: String
    let appliedUserCount: Int
    let totalJobView: Int
    var companyName: String
    var companyDesc: String
    let planId: String
    let jobId: String
    var categoryId: String
    let companyWebsite: String
    let isSaved : Int
    let distance: Int
    let jobType : Int
    var createdTimeStamp: String
    var updatedTimeStamp: String
    var categoryName: String
    let jobLocation: String
    let companyLocation: String
    var totalExperience: String
    var experienceType: Int
    let isCommision : Int
    var isShortlisted: Bool
    
    var createdAt: Date? {
        
        guard !self.createdTimeStamp.isEmpty else{
            return nil
        }
        
        guard let time = Int64(self.createdTimeStamp) else {
            return nil
        }
        
        let date = dateFromMilliseconds(timestamp: time)
        return date
    }
    
    var updatedAt: Date? {
        
        guard !self.updatedTimeStamp.isEmpty else{
            return nil
        }
        
        guard let time = Int64(self.updatedTimeStamp) else {
            return nil
        }
        
        let date = dateFromMilliseconds(timestamp: time)
        return date
    }
    
    init(dict: JSON) {
        
        self.jobTitle           = dict["jobTitle"].stringValue
        self.jobDescription     = dict["jobDesc"].stringValue
        self.salaryFrom         = dict["salaryFrom"].stringValue
        self.isExp              = dict["isExp"].intValue
        self.duration           = dict["duration"].stringValue
        self.jobState           = dict["jobState"].stringValue
        self.jobCity            = dict["jobCity"].stringValue
        self.jobImage           = dict["jobImage"].stringValue
        self.salaryTo           = dict["salaryTo"].stringValue
        self.extraCompensation  = dict["extraCompensation"].stringValue
        self.planId             = dict["planId"].stringValue
        self.companyDesc        = dict["companyDesc"].stringValue
        // self.experience         = dict["experience"].intValue
        self.status             = dict["status"].stringValue
        self.jobId             = dict["_id"].stringValue
        self.categoryId         = dict["categoryId"].stringValue
        self.companyWebsite     = dict["companyWebsite"].stringValue
        self.distance           = dict["distance"].intValue
        self.companyName        = dict["companyName"].stringValue
        self.jobType            = dict["jobType"].intValue
        self.updatedTimeStamp   = dict["updated_at"].stringValue
        self.categoryName       = dict["categoryName"].stringValue
        self.appliedUserCount   = dict["totalApplyCount"].intValue
        self.totalJobView       = dict["totalViewCount"].intValue
        self.jobShareUrl        = dict["shareUrl"].stringValue
        self.createdTimeStamp   = dict["created_at"].stringValue
        self.isApplied          = dict["isApplied"].intValue
        self.jobLatitude          = dict["latitude"].doubleValue
        self.jobLongitude          = dict["longitude"].doubleValue
        self.jobLocation        = dict["address"].stringValue
        self.companyLocation        = dict["companyAddress"].stringValue
        self.isSaved           = dict["isSaved"].intValue
        self.experienceType      = dict["experienceType"].intValue
        self.totalExperience       = dict["totalExperience"].stringValue
        self.isCommision          = dict["isCommision"].intValue
        self.recruiterDetail   = JobListRecruiterDetail(dict: dict["recruiterId"])
        self.isShortlisted     = dict["isShortlisted"].boolValue
    }
    
}


