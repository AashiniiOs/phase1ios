//
//  CandidateJobList.swift
//  JobGet
//
//  Created by macOS on 01/05/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import Foundation
import SwiftyJSON

class CandidateJobList {
    
    let recruiterDetail: JobListRecruiterDetail?
    let jobTitle: String
    let jobDescription: String
    let jobImage: String
    let jobCity: String
    let jobState: String
    let salaryFrom: String
    let salaryTo: String
    let duration: String
    let extraCompensation: String
    let isExp: Int
 //   let experience: Int
    let companyName: String
    let companyDesc: String
    let planId: String
    let status: String
    let jobId: String
    let categoryId: String
    let companyWebsite: String
    var distance: Int
    let jobType : Int
    var totalJobView: Int
    let appliedUserCount: Int
    let jobShareUrl: String
    var createdTimeStamp: String
    var updatedTimeStamp: String
    let categoryName: String
    let longitude    : Double
    let latitude    : Double
    let jobAddress : String
    let companyAddress: String
    let isSaved: Int
    var jobExpiredTime: String
    var isApplied: Int
    var isShortlisted: Bool
    var totalExperience: String
    var experienceType: Int
    var applyStatus: Int
    var isPremium: Bool
    
    var designation: String
    
    var createdAt: Date? {
        
        guard !self.createdTimeStamp.isEmpty else {
            return nil
        }
        
        guard let time = Int64(self.createdTimeStamp) else {
            return nil
        }
        
        let date = dateFromMilliseconds(timestamp: time)
        return date
    }
    
    var updatedAt: Date? {
        
        guard !self.updatedTimeStamp.isEmpty else{
            return nil
        }
        
        guard let time = Int64(self.updatedTimeStamp) else {
            return nil
        }
        
        let date = dateFromMilliseconds(timestamp: time)
        return date
    }
    
    init(dict: JSON) {
        
        self.jobTitle           = dict["jobTitle"].stringValue
        self.jobDescription     = dict["jobDesc"].stringValue
        self.salaryFrom         = dict["salaryFrom"].stringValue
        self.isExp              = dict["isExp"].intValue
        self.duration           = dict["duration"].stringValue
        self.jobState           = dict["jobState"].stringValue
        self.jobCity            = dict["jobCity"].stringValue
        self.jobImage           = dict["jobImage"].stringValue
        self.salaryTo           = dict["salaryTo"].stringValue
        self.extraCompensation  = dict["extraCompensation"].stringValue
        self.planId             = dict["planId"].stringValue
        self.companyDesc        = dict["companyDesc"].stringValue
       // self.experience         = dict["experience"].intValue
        self.status             = dict["status"].stringValue
        self.jobId             = dict["_id"].stringValue
        self.categoryId         = dict["categoryId"].stringValue
        self.companyWebsite     = dict["companyWebsite"].stringValue
        self.distance           = dict["distance"].intValue
        self.companyName        = dict["companyName"].stringValue
        self.jobType            = dict["jobType"].intValue
        self.updatedTimeStamp   = dict["updated_at"].stringValue
        self.categoryName       = dict["categoryName"].stringValue
        self.appliedUserCount   = dict["totalApplyCount"].intValue
        self.totalJobView       = dict["totalViewCount"].intValue
        self.jobShareUrl        = dict["shareUrl"].stringValue
        self.createdTimeStamp   = dict["created_at"].stringValue
        self.latitude          = dict["latitude"].doubleValue
        self.longitude          = dict["longitude"].doubleValue
        self.recruiterDetail   = JobListRecruiterDetail(dict: dict["recruiterId"])
        self.jobAddress        = dict["address"].stringValue
        self.companyAddress        = dict["companyAddress"].stringValue
        self.isSaved           = dict["isSaved"].intValue
         self.jobExpiredTime   = dict["expireTime"].stringValue
         self.isApplied         = dict["isApplied"].intValue
        self.designation        = dict["designation"].stringValue
        self.experienceType      = dict["experienceType"].intValue
        self.totalExperience       = dict["totalExperience"].stringValue
        self.applyStatus         = dict["applyStatus"].intValue
        self.isShortlisted   =  dict["isShortlisted"].boolValue
        self.isPremium   =  dict["isPremium"].boolValue
        
    }

}



class JobListRecruiterDetail {
    
    let recruiterId: String
    let recruiterName: String
    let last_name: String
    let email: String
    let mobile: String
    let country_code: String
    let isExperience: String
    let user_image: String
    
    init(dict: JSON) {
        self.recruiterId    = dict["_id"].stringValue
        self.recruiterName  = dict["first_name"].stringValue
        
        self.last_name      = dict["last_name"].stringValue
        self.email          = dict["email"].stringValue
        self.mobile         = dict["mobile"].stringValue
        self.country_code   = dict["country_code"].stringValue
        self.isExperience   = dict["isExperience"].stringValue
        self.user_image     = dict["user_image"].stringValue
    }
}
