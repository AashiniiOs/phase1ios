//
//  CardListModel.swift
//  JobGet
//
//  Created by Admin on 19/09/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import Foundation
import SwiftyJSON

struct CardListModel {
    var cardholderName: String = ""
    var expirationMonth: String = ""
    var customerId: String = ""
    
    var uniqueNumberIdentifier: String = ""
    var cardType: String = ""
    var bin: String = ""
    
    var expirationYear: String = ""
    var token: String = ""
    var last4: String = ""
    
    var expirationDate: String = ""
    var maskedNumber: String = ""
    var imageUrl: String = ""
    
    init(withJson json: JSON) {
        
        self.cardholderName = json["cardholderName"].stringValue
        self.expirationMonth = json["expirationMonth"].stringValue
        self.customerId = json["customerId"].stringValue
        
        self.uniqueNumberIdentifier = json["uniqueNumberIdentifier"].stringValue
        self.cardType = json["cardType"].stringValue
        self.bin = json["bin"].stringValue
        
        self.expirationYear = json["expirationYear"].stringValue
        self.token = json["token"].stringValue
        self.maskedNumber = json["maskedNumber"].stringValue
        
        self.expirationDate = json["expirationDate"].stringValue
        self.maskedNumber = json["maskedNumber"].stringValue
        self.imageUrl = json["imageUrl"].stringValue
        
    }
}
