//
//  ChatLocalDBModel.swift
//  JobGet
//
//  Created by Admin on 05/10/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import Foundation
import SwiftyJSON
import RealmSwift



class ChatLocalDB: Object {
    
    @objc dynamic var chat_userId: String = ""
    
//    @objc dynamic var room_id: String = ""
   
    var userList = List<ChatUserListDB>()

    
//    override static func primaryKey() -> String? {
//        return "chat_userId"
//    }
    
   
}

class ChatUserListDB: Object {
    @objc dynamic var room_id = ""
     var messageList = List<MessageListDB>()
}

class MessageListDB: Object {
    
    @objc dynamic var messageId: String = ""
    @objc dynamic var message: String = ""
    @objc dynamic var timestamp: String = ""
    @objc dynamic var first_name: String = ""
    @objc dynamic var mediaUrl: String = ""
    @objc dynamic var roomId = ""
    @objc dynamic var senderId: String = ""
    @objc dynamic var last_name: String = ""
//    override static func primaryKey() -> String? {
//        return "messageId"
//    }
}


