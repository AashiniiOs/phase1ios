//
//  DBManager.swift
//  JobGet
//
//  Created by Admin on 08/10/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import RealmSwift

class DBManager {
    
    private var database:Realm
    static let sharedInstance = DBManager()
    
    private init() {
        
        database = try! Realm()
        
    }
    
    func getDataFromDB() -> Results<ChatLocalDB> {
        
        let results: Results<ChatLocalDB> = database.objects(ChatLocalDB.self)
        return results
    }
    
    func addData(object: ChatLocalDB) {
        
        try! database.write {
            
            database.add(object)
        }
    }
    
    func deleteAllDatabase()  {
        try! database.write {
            database.deleteAll()
        }
    }
    
    func deleteFromDb(object: ChatLocalDB) {
        
        try! database.write {
                database.delete(object)
        }
    }
    
    func deleteMessageFromDB(object: MessageListDB){
        
        try! database.write {
            let deletedNotifications = database.objects(MessageListDB.self).filter("messageId == %@", object.messageId)
            database.delete(deletedNotifications)
            
        }
    }
    
    func deleteUserFromDB(object: ChatUserListDB){
        try! database.write {
            let deletedNotifications = database.objects(ChatUserListDB.self).filter("room_id == %@", object.room_id)
            database.delete(deletedNotifications)
            
        }
    }
    
    func getChatLocalDbForId(id: String) -> ChatLocalDB? {
        
        let results: Results<ChatLocalDB> = database.objects(ChatLocalDB.self).filter("chat_userId == '\(id)'")
        return results.first
    }
    
   
    func getChatUserForRoomId(id: String, user_id: String) -> ChatUserListDB? {

        guard let item = self.getChatLocalDbForId(id: user_id) else{return nil}
        let results: Results<ChatUserListDB> = item.userList.filter("room_id == '\(id)'")
        return results.first
    }
    
    func getChatUserForMessageId(id: String, user_id: String, message_Id: String) -> MessageListDB? {

        guard let userList = self.getChatUserForRoomId(id: id, user_id: user_id) else{return nil}
        let results: Results<MessageListDB> = userList.messageList.filter("messageId == '\(message_Id)'")
        return results.first
    }
    
    func getMessgaeAfterSearch(text: String, messageList: List<MessageListDB>)->MessageListDB?{
        let result: Results<MessageListDB> = messageList.filter("message == '\(text)'")

        return result.first
    }
}
