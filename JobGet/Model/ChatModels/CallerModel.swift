//
//  CallerModel.swift
//  JobGet
//
//  Created by Appinventiv on 29/05/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import SwiftyJSON

class Caller {
    
    var uuid: UUID
    var sender_name: String = ""
    var receiver_name: String = ""
    var sender_id: String = ""
    var receiver_id: String = ""
    var sender_image: String = ""
    var receiver_image: String = ""
    var device_id: String = ""
    var device_token: String = ""
    var type: String = ""
    var senderTwilioToken: String = ""
    var twilioToken: String = ""
    var senderDeviceToken: String = ""
    var senderImage: String = ""

    let hasVideo: Bool
    var device_type: String = ""
    
    var loginId: String {
        return "\(sender_id)_\(receiver_id)"
    }
    
    required init?(json: JSON) {
        
        let uuidString = json["UUID"].stringValue
        let identifier = json["senderId"].stringValue
        let type = json["type"].stringValue
        
//        guard let uuid = UUID(uuidString: uuidString), !identifier.isEmpty, type == "calling" else {
//            return nil
//        }
        
        self.uuid                       = UUID(uuidString: uuidString) ?? UIDevice.current.identifierForVendor!
        self.type                       = type
        
        self.receiver_id                = json["receiverId"].stringValue
        self.sender_name                = json["senderName"].stringValue
        self.sender_image               = json["sender_image"].stringValue
        self.receiver_image             = json["receiver_image"].stringValue
        self.device_id                  = json["deviceId"].stringValue
        self.device_token               = json["deviceToken"].stringValue
        self.device_type                = json["deviceType"].stringValue
        
        self.sender_id                  = identifier
        self.hasVideo                   = json["hasVideo"].boolValue
        self.twilioToken                = json["twilioToken"].stringValue
        print_debug("twilioToken  == \(json["twilioToken"].stringValue)")
        
        self.senderTwilioToken                = json["senderTwilioToken"].stringValue

        print_debug("senderTwilioToken  == \(json["senderTwilioToken"].stringValue)")
        
        self.receiver_name              = json["receiverName"].stringValue
        self.senderDeviceToken              = json["senderDeviceToken"].stringValue
        self.senderImage              = json["senderImage"].stringValue

    }
    
    init(uuid: UUID, name: String, firstname: String, identifier: String, hasVideo: Bool, type: String) {
        self.uuid           = uuid
        self.sender_name           = name
        self.sender_id     = identifier
        self.hasVideo       = hasVideo
        self.type           = type
        self.receiver_name      = firstname
    }
    
    class func models(from jsonArray: [JSON]) -> [Caller] {
        var models: [Caller] = []
        for json in jsonArray {
            if let caller = Caller(json: json) {
                models.append(caller)
            }
        }
        return models
    }
    
    var dictionaryObject: JSONDictionary {
        var dictionary                  = JSONDictionary()
        dictionary["sender_id"]                = sender_id
        dictionary["sender_name"]              = sender_name
        dictionary["UUID"]              = uuid.uuidString
        dictionary["receiver_name"]    = receiver_name
        dictionary["type"]              = type
        dictionary["hasVideo"]          = hasVideo
        dictionary["receiver_id"]          = receiver_id
        dictionary["sender_image"]          = sender_image
        dictionary["receiver_image"]          = receiver_image
        dictionary["device_id"]          = device_id
        dictionary["device_token"]          = device_token
        dictionary["device_type"]          = device_type
        
        return dictionary
    }
}


