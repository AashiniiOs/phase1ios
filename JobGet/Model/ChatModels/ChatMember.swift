//
//  ChatMember.swift
//  Shopoholic
//
//  Created by Appinventiv on 20/04/18.
//  Copyright © 2018 Gurdeep Singh. All rights reserved.
//

import Foundation


struct ChatMember: Codable, Hashable{
    var hashValue: Int{
        return self.userId.hashValue
    }
    
    static func ==(lhs: ChatMember, rhs: ChatMember) -> Bool {
        return lhs.userId == rhs.userId
    }
    
    
    let userId: String
    let firstName:  String
    let lastName: String
    let email: String
    let userImage: String
    let mobileNumber: String
    let deviceToken: String?
    var isOnline: Bool
    let deviceType: String
    
    enum CodingKeys: String, CodingKey {
        
        case userId = "user_id"
        case firstName = "first_name"
        case lastName = "last_name"
        case userImage = "image"
        case mobileNumber = "mobile"
        case deviceToken = "device_token"
        case isOnline = "isOnline"
        case email = "email"
        case deviceType = "device_type"
    }

}
