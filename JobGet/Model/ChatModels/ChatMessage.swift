//
//  ChatMessage.swift
//  Shopoholic
//
//  Created by Appinventiv on 20/04/18.
//  Copyright © 2018 Gurdeep Singh. All rights reserved.
//

import Foundation
import UIKit

struct ChatMessage: Hashable {
    
    var hashValue: Int{
        return self.messageId.hashValue
    }
    
    static func ==(lhs: ChatMessage, rhs: ChatMessage) -> Bool {
        return lhs.messageId == rhs.messageId
    }
    
    
    let timestamp: Double
    let type : ChatEnum.MessageType
    let message : String
    let latitude : Double
    let longitude : Double
    var mediaUrl : String
    let senderId : String
    let messageId: String
    var status: ChatEnum.MessageStatus
    var thumbnail: String?
    var isDeleted: Bool
    let isBlock: Bool
    let roomId: String
    let receiverId: String
    var image: UIImage? = nil
    
    enum CodingKeys: String, CodingKey{
        
        case timestamp
        case type
        case message = "messageText"
        case latitude
        case longitude
        case mediaUrl
        case senderId
        case messageId
        case status
        case isBlock
        case isDeleted
        case thumbnail
        case roomId
        case receiverId
        case image
    }
    
    func getMessageDictionary()-> [String:Any]{
        
        var message: [String:Any] = [:]
        message[ChatEnum.Message.latitude.rawValue] = self.latitude
        message[ChatEnum.Message.longitude.rawValue] = self.longitude
        message[ChatEnum.Message.mediaUrl.rawValue] = self.mediaUrl
        message[ChatEnum.Message.message.rawValue] = self.message
        message[ChatEnum.Message.sender.rawValue] = self.senderId
        message[ChatEnum.Message.timestamp.rawValue] = ChatHelper.timeStamp
        message[ChatEnum.Message.type.rawValue] = self.type.rawValue
        message[ChatEnum.Message.messageId.rawValue] = self.messageId
        message[ChatEnum.Message.status.rawValue] = self.status.rawValue
        message[ChatEnum.Message.thumbnail.rawValue] = self.thumbnail
        message[ChatEnum.Message.isDelete.rawValue] = self.isDeleted
        message[ChatEnum.Message.isBlock.rawValue] = self.isBlock
        message[ChatEnum.Message.roomId.rawValue] = self.roomId
        message[ChatEnum.Message.receiverId.rawValue] = self.receiverId
        
        return message
    }
}

extension ChatMessage: Decodable{
    
    init(from decoder: Decoder) throws {
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        
        self.timestamp = try container.decode(Double.self, forKey: .timestamp)
        self.latitude = try container.decode(Double.self, forKey: .latitude)
        self.longitude = try container.decode(Double.self, forKey: .longitude)
        self.message = try container.decode(String.self, forKey: .message)
        self.senderId = try container.decode(String.self, forKey: .senderId)
        self.mediaUrl = try container.decode(String.self, forKey: .mediaUrl)
        self.messageId = try container.decode(String.self, forKey: .messageId)
        self.thumbnail = try container.decodeIfPresent(String.self, forKey: .thumbnail)
        self.isDeleted = try container.decode(Bool.self, forKey: .isDeleted)
        self.isBlock = try container.decode(Bool.self, forKey: .isBlock)
        self.roomId = try container.decode(String.self, forKey: .roomId)
        self.receiverId = try container.decode(String.self, forKey: .receiverId)
        
        let status = try container.decode(String.self, forKey: .status)
        
        if let status = ChatEnum.MessageStatus(rawValue: status){
            self.status = status
        }else{
            self.status = .none
        }
        
        let type = try container.decode(String.self, forKey: .type)
        if let type = ChatEnum.MessageType(rawValue: type){
            self.type = type
        }else{
            self.type = .none
        }
    }
}
