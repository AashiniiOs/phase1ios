//
//  ChatRoom.swift
//  Shopoholic
//
//  Created by Appinventiv on 20/04/18.
//  Copyright © 2018 Gurdeep Singh. All rights reserved.
//

import Foundation
import SwiftyJSON

struct ChatRoom{
    
    let chatRoomId: String
    let chatRoomTitle: String
    let chatRoomType: ChatEnum.RoomType
    let chatRoomPic: String
    let chatLastUpdate: Double
    let chatLastUpdates: [String:Double]
    var chatRoomIsTyping: [Typing]
    let chatRoomMembers: [MemberUpdates]
    
    init(with json: JSON) {
        self.chatRoomId = json["chatRoomId"].stringValue
        self.chatRoomTitle = json["chatRoomTitle"].stringValue
        self.chatRoomPic = json["chatRoomPic"].stringValue
        
        if let type = ChatEnum.RoomType(rawValue: json["chatRoomType"].stringValue){
            
            self.chatRoomType = type
        }else{
            self.chatRoomType = .none
        }
        
        self.chatLastUpdate = json["chatLastUpdate"].doubleValue
        
        if let lastUpdatesDict = json["chatLastUpdates"].dictionaryObject, let lastUpdates = lastUpdatesDict as? [String:Double]{
            
            self.chatLastUpdates = lastUpdates
        }else{
            self.chatLastUpdates = [:]
        }
        
        if let typingJson = json["chatRoomIsTyping"].dictionaryObject, let typing = typingJson as? [String:Bool]{
            
            var isTypingArr: [Typing] = []
            
            for (key,value) in typing{
                let isTyping = Typing(userId: key, isTyping: value)
                isTypingArr.append(isTyping)
            }
            
            self.chatRoomIsTyping = isTypingArr
        }else{
            self.chatRoomIsTyping = []
        }
        
        let jsonDict = json["chatRoomMembers"].dictionaryValue
        var chatMem: [MemberUpdates] = []
        for (key,value) in jsonDict{
            
            let memberJoin = value["memberJoin"].doubleValue
            let memberLeave = value["memberLeave"].doubleValue
            let memberDelete = value["memberDelete"].doubleValue
            
            let memberUp = MemberUpdates(userId: key,
                                         memberDelete: memberDelete,
                                         memberJoin: memberJoin,
                                         memberLeave: memberLeave)
            
            chatMem.append(memberUp)
        }
        
        self.chatRoomMembers = chatMem
    }
    
    var isMessageRead: Bool{
        
        guard let userId = User.getUserModel().user_id else  { return false}
        
        if let update = self.chatLastUpdates[userId]{
            return update >= self.chatLastUpdate
        }else{
            return false
        }
    }
    
    var currentUserUpdate: MemberUpdates?{
        
        guard let userId = User.getUserModel().user_id else { return nil }
        
        let res = self.chatRoomMembers.filter { (memberUpdate) -> Bool in
            return memberUpdate.userId == userId
        }
        return res.first
    }
    
    var isTyping: Bool{
        guard let userId = User.getUserModel().user_id else  { return false }
        var typeStat: Bool = false
        for typing in self.chatRoomIsTyping{
            
            if typing.userId == userId{
                typeStat = true
                break
            }else{
                continue
            }
        }
        return typeStat
    }
}

struct Typing{
    
    let userId: String
    let isTyping: Bool
}

struct MemberUpdates {
    let userId: String
    let memberDelete: Double
    let memberJoin : Double
    let memberLeave: Double
}
