//
//  Inbox.swift
//  Shopoholic
//
//  Created by Appinventiv on 23/04/18.
//  Copyright © 2018 Gurdeep Singh. All rights reserved.
//

import Foundation


struct Inbox:Equatable,Hashable {
    
    var hashValue: Int{
        return self.roomId.hashValue
    }
    
    static func ==(lhs: Inbox, rhs: Inbox) -> Bool {
        return lhs.roomId == rhs.roomId
    }
    
    let roomId: String
    var message: ChatMessage? = nil
    var roomType: ChatEnum.RoomType = .none
    var chatMember: ChatMember? = nil
    var unreadCount: Int = 0
    var chatRoom: ChatRoom?
    
    init(roomId: String) {
        self.roomId = roomId
    }
}
