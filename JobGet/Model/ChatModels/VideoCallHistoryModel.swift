//
//  VideoCallHistoryModel.swift
//  JobGet
//
//  Created by Admin on 18/07/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import Foundation
import SwiftyJSON

struct VideoCallHistoryModel {
    let senderImage: String
    var status: String
    var receiverImage:String
    var senderFirstName:String
    var _id : Int
    var receiverFirstName: String
    
    let callTime: Int
    var receiverId: String
    var senderId:String
    var senderLastName:String
    var timestamp : String
    var receiverLastName: String
    var user_login_session : [LoginSession]
    
    var createdAt: Date? {
        
        guard !self.timestamp.isEmpty else {
            return nil
        }
        
        guard let time = Int64(self.timestamp) else {
            return nil
        }
        
        let date = dateFromMilliseconds(timestamp: time)
        return date
    }
    
    init(withDict dict: JSON){
        self.senderImage = dict["senderImage"].stringValue
        self.status = dict["status"].stringValue
      
        self.receiverImage = dict["receiverImage"].stringValue
        self.senderFirstName = dict["senderFirstName"].stringValue
        self._id = dict["_id"].intValue
        self.receiverFirstName = dict["receiverFirstName"].stringValue
        self.callTime = dict["callTime"].intValue
        self.receiverId = dict["receiverId"].stringValue
        self.senderId = dict["senderId"].stringValue
        self.senderLastName = dict["senderLastName"].stringValue
        self.timestamp = dict["timestamp"].stringValue
        self.receiverLastName = dict["receiverLastName"].stringValue
        self.user_login_session = dict["user_login_session"].arrayValue.map({LoginSession(withJSON: $0)})
    }
}
