//
//  CountryModel.swift
//  ChekiOdds
//
//  Created by MAC on 09/11/17.
//  Copyright © 2017 Beta. All rights reserved.
//

import Foundation


class Country:NSObject{
    let countryName: String
    var countryCode: String
    var max_NSN_NO:Int
    var min_NSN_NO:Int
    var countryShortName : String
    var nameWithCode: String
    
    init(withDict dict: [String:AnyObject]){
        self.countryName = dict["name"] as? String ?? ""
        self.countryCode = dict["dial_code"] as? String ?? ""
        self.max_NSN_NO = 0
        self.min_NSN_NO = 0
        self.countryShortName = dict["code"] as? String ?? ""
        self.nameWithCode = dict["nameWithCode"] as? String ?? ""
    }
}
