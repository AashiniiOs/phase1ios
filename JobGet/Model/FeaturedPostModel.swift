//
//  FeaturedPostModel.swift
//  JobGet
//
//  Created by macOS on 18/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import Foundation
import SwiftyJSON

struct FeaturedPostModel {
    
    let planName: String
    let planId: String
    let createdAt: String
    var planAmount: String
    let description: String
    let validity: String
    let type: Int
    let stars: String
    
    init(dict: JSON ) {
        
        self.planName = dict["planName"].stringValue
        self.planId = dict["_id"].stringValue
        self.createdAt = dict["createdAt"].stringValue
        self.planAmount = dict["planAmount"].stringValue
        self.description = dict["description"].stringValue
        self.validity = dict["validity"].stringValue
        self.type = dict["type"].intValue
        self.stars = dict["stars"].stringValue
    }
}

struct MonthlySubscription {
    
    let planName: String
    let endTimestamp: String
    let planId: String
    let paymentType: String
    let amount: String
    let status: String
    let startTimestamp: String
    let userId: Int
    let stars: String
    
    var startDate: Date? {
        
        guard !self.startTimestamp.isEmpty else {
            return nil
        }
        
        guard let time = Int64(self.startTimestamp) else {
            return nil
        }
        
        let date = dateFromMilliseconds(timestamp: time)
        return date
    }
    
    var endDate: Date? {
        
        guard !self.endTimestamp.isEmpty else {
            return nil
        }
        
        guard let time = Int64(self.endTimestamp) else {
            return nil
        }
        
        let date = dateFromMilliseconds(timestamp: time)
        return date
    }
    
    init(dict: JSON ) {
        
        self.planName = dict["planName"].stringValue
        self.planId = dict["_id"].stringValue
        self.endTimestamp = dict["endTimestamp"].stringValue
        self.amount = dict["amount"].stringValue
        self.startTimestamp = dict["startTimestamp"].stringValue
        self.status = dict["status"].stringValue
        self.userId = dict["userId"].intValue
        self.stars = dict["stars"].stringValue
        self.paymentType = dict["paymentType"].stringValue
    }
}
