//
//  JobCategoryModel.swift
//  JobGet
//
//  Created by macOS on 05/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import Foundation
import SwiftyJSON

struct JobCategory {
    
    
    var categoryId: String
    let updatedAt: String
    let ceatedAt: String
    let categoryImage: String
    var categoryTitle: String
    let status: String
    let selectedCategoryImage: String
    let isLastCategory: Bool
    var selectedCategoryID = ""
    
    init(dict: JSON) {
        
        self.categoryTitle  = dict["categoryTitle"].stringValue
        self.categoryImage = dict["categoryImage"].stringValue
        self.categoryId  = dict["_id"].stringValue
        self.updatedAt = dict["updatedAt"].stringValue
        self.ceatedAt = dict["ceatedAt"].stringValue
        self.status = dict["status"].stringValue
        self.isLastCategory = dict["isLastCategory"].boolValue
        self.selectedCategoryImage = dict["selectedImage"].stringValue
    }
}


