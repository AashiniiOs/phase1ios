//
//  JobDetail.swift
//  JobGet
//
//  Created by macOS on 23/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import Foundation
import SwiftyJSON

struct JobDetail {
    
    var jobTitle                : String
    var jobDescription          : String
    var salaryFrom              : String
    var updatedAt               : String
    var planId                  : String
    var salaryTo                : String
    var jobImage                : String
    var companyDesc             : String
    var extraCompensation       : String
    var recruiterId             : String
    var jobState                : String
    var experience              : Int
    var latitude                : String
    var status                  : Int
    var jobId                   : String
    var categoryId              : String
    var jobCity                 : String
    var createdAt               : String
    var longitude               : String
    var companyName             : String
    var perHour                 : String
    var jobType                 : Int
    var isExp                   : Int
    
    init(dict: JSON) {
        
        self.jobTitle           = dict["jobTitle"].stringValue
        self.jobDescription     = dict["jobDesc"].stringValue
        self.jobImage           = dict["jobImage"].stringValue
        self.updatedAt          = dict["updated_at"].stringValue
        self.planId             = dict["planId"].stringValue
        self.salaryTo           = dict["salaryTo"].stringValue
        self.companyDesc        = dict["companyDesc"].stringValue
        self.extraCompensation  = dict["extraCompensation"].stringValue
        self.recruiterId        = dict["recruiterId"].stringValue
        self.jobState           = dict["jobState"].stringValue
        self.latitude           = dict["latitude"].stringValue
        self.experience         = dict["experience"].intValue
        self.status             = dict["status"].intValue
        self.jobId              = dict["_id"].stringValue
        self.categoryId         = dict["categoryId"].stringValue
        self.salaryFrom         = dict["salaryFrom"].stringValue
        self.jobCity            = dict["jobCity"].stringValue
        self.createdAt          = dict["created_at"].stringValue
        self.companyName        = dict["companyName"].stringValue
        self.perHour            = dict["perHour"].stringValue
        self.longitude          = dict["longitude"].stringValue
        self.jobType            = dict["jobType"].intValue
        self.isExp              = dict["isExp"].intValue
    }
}
