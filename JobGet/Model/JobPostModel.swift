//
//  JobPostModel.swift
//  JobGet
//
//  Created by macOS on 22/06/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import Foundation

var SharedJobPost = JobPostModel.sharedJobPost

struct JobPostModel {
    
    static var sharedJobPost = JobPostModel()
    
    var jobType: Int = -1 // Full Time, Part Time, Both
    var jobDesc : String = ""
    var salaryFrom : String = ""
    
    var jobTitle: String = ""
    var isExp : Int?
    var totalExperience : String = ""
    var rawExpValue : Float?
    var rawExp: String = ""
    var experienceType : Int?
    var categoryName : String = ""
    
    var jobImage : String = ""
    
    var address: String = ""
    var planId : String = ""
    var recruiterId : String = ""
    var jobCity: String = ""
    var jobState : String = ""
    var latitude: Double?
    var categoryId : String = ""
    var duration : String = ""
    var rawJobImage: UIImage?
    var longitude: Double?
    var companyName : String = ""
    var salaryTo : String = ""
    var isCommisionRequired: Int = 0 // 0 For no, 1 for yes
    
    init() {
        
    }
    
    func dictionary() -> [String:Any]{
        
        
        var details = [String:Any]()
        
        details["jobType"] = self.jobType
        details["jobDesc"] = self.jobDesc
        details["salaryFrom"] = self.salaryFrom
        details["jobTitle"] = self.jobTitle
        details["isExp"] = self.isExp
        details["totalExperience"] = self.totalExperience
        if self.experienceType != nil {
            details["experienceType"] = self.experienceType
        }
        details["categoryName"] = self.categoryName
        details["jobImage"]  = self.jobImage
        details["address"] = self.address
        details["planId"] = self.planId
        details["recruiterId"] = self.recruiterId
        details["latitude"] = self.latitude
        details["longitude"] = self.longitude
        details["companyName"] = self.companyName
        details["salaryTo"] = self.salaryTo
        details["jobCity"] = self.jobCity
        details["jobState"] = self.jobState
        details["categoryId"] = self.categoryId
        details["duration"] = self.duration
        details["isCommision"] = self.isCommisionRequired
        return details
        
    }
    
    mutating func removeAllValue() {
        self.jobType = -1
        self.jobDesc = ""
        self.salaryFrom = ""
        
        self.jobTitle = ""
        self.isExp = nil
        self.totalExperience = ""
        self.rawExpValue = nil
        self.rawExp = ""
        self.experienceType = nil
        self.categoryName = ""
        
        self.jobImage  = ""
        
        self.address = ""
        self.planId  = ""
        self.recruiterId  = ""
        self.rawJobImage = nil
        self.latitude = nil
        self.categoryId  = ""
        self.duration  = ""
        
        self.longitude = nil
        self.companyName  = ""
        self.salaryTo  = ""
        self.jobCity = ""
        self.jobState = ""
    }
}

