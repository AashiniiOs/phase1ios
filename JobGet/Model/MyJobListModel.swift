//
//  MyJobListModel.swift
//  JobGet
//
//  Created by macOS on 18/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import Foundation
import SwiftyJSON

class MyJobListModel {
    
    var applicantsImage = [String]()
    var jobTitle: String
    var jobDescription: String
    var jobImage: String
    var jobAddress: String
    var latitude: Double
    var longitude: Double
    var salaryFrom: String
    var salaryTo: String
    var perHour: String
    let extraCompensation: String
    let isExp: Int
    var companyName: String
    var companyDesc: String
    let planId: String
    var deleteStatus: Int  // if deleteStatus == 3 then job is closed
    let recruiterId: String
    let jobId: String
    var categoryId: String
    var jobType : Int
    let totalJobView: Int
    var jobCity: String
    var jobState : String
    let appliedUserCount: Int
    var createdTimeStamp: String
    var updatedTimeStamp: String
    var totalExperience: String
    var experienceType: Int
    var companyAddress : String
    var companyWebsite : String
    var companyLatitude: Double
    var isCommision: Int
    var companyLongitude: Double
    var shareUrl: String
    var categoryName: String
    var isPremium:Bool
    var premiumStart: String
    var premiumEnd: String

    
    var createdAt: Date? {
        
        guard !self.createdTimeStamp.isEmpty else{
            return nil
        }
        
        guard let time = Int64(self.createdTimeStamp) else {
            return nil
        }
        
        let date = dateFromMilliseconds(timestamp: time)
        return date
    }
    
    var updatedAt: Date? {
        
        guard !self.updatedTimeStamp.isEmpty else{
            return nil
        }
        
        guard let time = Int64(self.updatedTimeStamp) else {
            return nil
        }
        
        let date = dateFromMilliseconds(timestamp: time)
        return date
    }
    
    
    init(dict: JSON) {
        
        self.jobTitle           = dict["jobTitle"].stringValue
        self.jobDescription     = dict["jobDesc"].stringValue
        self.jobImage           = dict["jobImage"].stringValue
        self.updatedTimeStamp   =  dict["updated_at"].stringValue
        self.planId             = dict["planId"].stringValue
        self.salaryTo           = dict["salaryTo"].stringValue
        self.companyDesc        = dict["companyDesc"].stringValue
        self.extraCompensation  = dict["extraCompensation"].stringValue
        self.isCommision       = dict["isCommision"].intValue
        self.recruiterId        = dict["recruiterId"].stringValue
        self.jobAddress           = dict["address"].stringValue
        self.latitude           = dict["latitude"].doubleValue
        self.deleteStatus             = dict["status"].intValue
        self.jobId              = dict["_id"].stringValue
        self.categoryId         = dict["categoryId"].stringValue
        self.salaryFrom         = dict["salaryFrom"].stringValue
        self.createdTimeStamp     = dict["created_at"].stringValue
        self.companyName        = dict["companyName"].stringValue
        self.perHour            = dict["duration"].stringValue
        self.longitude          = dict["longitude"].doubleValue
        self.jobType            = dict["jobType"].intValue
        self.isExp              = dict["isExp"].intValue
        self.categoryName       = dict["categoryName"].stringValue
        self.appliedUserCount   = dict["totalApplyCount"].intValue
        self.totalJobView        = dict["totalViewCount"].intValue
        self.jobCity            = dict["jobCity"].stringValue
        self.jobState           = dict["jobState"].stringValue
        self.companyAddress     = dict["companyAddress"].stringValue
        self.companyWebsite     = dict["companyWebsite"].stringValue
        self.companyLatitude    = dict["companyLatitude"].doubleValue
        self.companyLongitude   = dict["companyLongitude"].doubleValue
        self.totalExperience      = dict["totalExperience"].stringValue
        self.experienceType      = dict["experienceType"].intValue
        self.shareUrl           = dict["shareUrl"].stringValue
        self.isPremium          = dict["isPremium"].boolValue
        self.premiumStart           = dict["premiumStart"].stringValue
        self.premiumEnd           = dict["premiumEnd"].stringValue

        for image in dict["appliedUsers"].arrayValue {
            applicantsImage.append(image.stringValue)
        }
    }
}


