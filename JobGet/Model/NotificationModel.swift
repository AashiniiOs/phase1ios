//
//  NotificationModel.swift
//  JobGet
//
//  Created by Admin on 04/07/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import Foundation
import SwiftyJSON


struct NotificationModel {
    let moduleId : String
    let id: String
    let senderName: String
    let type: String
    let userImage: String
    var message:String
    var senderId:String
    var createdTimeStamp: String
    var jobTitle:String
    var companyName: String
    
    var createdAt: Date? {
        
        guard !self.createdTimeStamp.isEmpty else {
            return nil
        }
        
        guard let time = Int64(self.createdTimeStamp) else {
            return nil
        }
        
        let date = dateFromMilliseconds(timestamp: time)
        return date
    }
    
    init(dict: JSON ) {
        
        self.id = dict["_id"].stringValue
        self.moduleId = dict["moduleId"].stringValue
        self.senderName = dict["senderName"].stringValue
        self.type = dict["type"].stringValue
        self.userImage = dict["senderImage"].stringValue
        self.message = dict["message"].stringValue
        self.senderId = dict["senderId"].stringValue
        self.createdTimeStamp   = dict["createdAt"].stringValue
        self.companyName = dict["companyName"].stringValue
        self.jobTitle   = dict["jobTitle"].stringValue

    }
}
