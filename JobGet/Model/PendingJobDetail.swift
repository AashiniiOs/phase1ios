//
//  PendingJobDetail.swift
//  JobGet
//
//  Created by macOS on 11/05/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import Foundation
import SwiftyJSON

class PendingJobDetail {
    
    var userData: UserData
    var jobData : PendingJobData
    var experience : [ExperienceModel]
    var categoryTitles = [String]()
    var jobAppliedTime: String
    var jobAppliedId: String
    var jobExpiredTime: String
    var shortlistedTime : String
    
    var jobType : Int // either pending rejected shortlisted or expired
    
    var jobAppliedAt: Date? {
        
        guard !self.jobAppliedTime.isEmpty else{
            return nil
        }
        
        guard let time = Int64(self.jobAppliedTime) else {
            return nil
        }
        
        let date = dateFromMilliseconds(timestamp: time)
        return date
    }
    
    var jobShortistedAt: Date?{
        
        guard !self.shortlistedTime.isEmpty else{
            return nil
        }
        
        guard let time = Int64(self.shortlistedTime) else {
            return nil
        }
        
        let date = dateFromMilliseconds(timestamp: time)
        return date
    }
    
    init(dict: JSON) {
        
        self.jobAppliedId           = dict["_id"].stringValue
        self.jobAppliedTime         = dict["applyTime"].stringValue
        self.jobType                = dict["applyStatus"].intValue
        self.jobExpiredTime         = dict["expireTime"].stringValue
        self.userData               = UserData(dict: dict["userId"].dictionaryValue)
        self.jobData                = PendingJobData(jobData: dict["jobId"].dictionaryValue)
        self.experience             = dict["userId"]["experience"].arrayValue.map({ExperienceModel(withJSON: $0)})
        self.shortlistedTime        = dict["shortlistTime"].stringValue
        
        let _ = dict["categoryTitle"].arrayValue.filter { (title) -> Bool in
            self.categoryTitles.append(title.stringValue)
            return true
        }
    }
}

extension PendingJobDetail : Equatable{
    static func ==(lhs: PendingJobDetail, rhs: PendingJobDetail) -> Bool {
        return lhs.jobAppliedId == rhs.jobAppliedId
    }
}


class UserData {
    
    var userID: String          = ""
    var firstName: String       = ""
    var secondName: String      = ""
    var userImage : String      = ""
    var isExperience: String      = ""
    var device_id: String      = ""
    var device_token: String      = ""
    var device_type: String      = ""
    
    init(dict: JSONDictionary) {
        
        self.firstName = dict["first_name"] as? String ?? ""
        if let firstName = dict["first_name"] {
            self.firstName = String(describing: firstName)
        }
        
        if let userId = dict["_id"] {
            self.userID = String(describing: userId)
        }
        
        if let userImage = dict["user_image"] {
            self.userImage = String(describing: userImage)
        }
        
        if let secondName = dict["last_name"] {
            self.secondName = String(describing: secondName)
        }
        if let isExperience = dict["isExperience"] as? Int {
            self.isExperience = String(describing: isExperience)
        }
        if let device_id = dict["device_id"] {
            self.device_id = String(describing: device_id)
        }
        if let device_token = dict["device_token"] {
            self.device_token = String(describing: device_token)
        }
        if let device_type = dict["device_type"] {
            self.device_type = String(describing: device_type)
        }
        
    }
}

class PendingJobData {
    
    var categoryId:      String = ""
    var createdTimeStamp:  String = ""
    var experiance:  String = ""
    var jobTitle  :  String = ""
    
    var createdAt: Date? {
        
        guard !self.createdTimeStamp.isEmpty else{
            return nil
        }
        
        guard let time = Int64(self.createdTimeStamp) else {
            return nil
        }
        
        let date = dateFromMilliseconds(timestamp: time)
        return date
    }
    
    init(jobData: JSONDictionary) {
        if let jobId = jobData["categoryId"] {
            self.categoryId = String(describing: jobId)
        }
        
        if let jobId = jobData["experience"] {
            self.experiance = String(describing: jobId)
        }
        
        if let jobTitle = jobData["jobTitle"] {
            self.jobTitle = String(describing: jobTitle)
        }
        
        if let timeStamp = jobData["created_at"] {
            self.createdTimeStamp = String(describing: timeStamp)
        }
       
        
    }
}

class HeaderJobCount {
    
    var type: Int
    var count : Int
    var title = ""
    
    init(dict: JSON) {
        self.type = dict["type"].intValue
        self.count = dict["count"].intValue
        self.title  = dict["title"].stringValue
    }
}

