//
//  ProfileVisitorModel.swift
//  JobGet
//
//  Created by Admin on 31/07/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import Foundation
import SwiftyJSON



struct ProfileVisitorModel {
    
    let userImage: String
    let last_name: String
    let _id: String
    let companyName: String
    let first_name: String
    let created_at: String
    let recruiterId : String
    var createdAt: Date? {
        
        guard !self.created_at.isEmpty else{
            return nil
        }
        
        guard let time = Int64(self.created_at) else {
            return nil
        }
        
        let date = dateFromMilliseconds(timestamp: time)
        return date
    }
    
    init(withJSON json : JSON) {
        
        self.userImage = json["userImage"].stringValue
        self.recruiterId = json["recruiterId"].stringValue
        self.last_name = json["last_name"].stringValue
        self._id = json["_id"].stringValue
        self.companyName = json["companyName"].stringValue
        self.first_name = json["first_name"].stringValue
        self.created_at = json["createdAt"].stringValue
    }
}
