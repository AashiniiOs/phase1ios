//
//  RecruiterProfile.swift
//  JobGet
//
//  Created by macOS on 29/05/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import Foundation
import SwiftyJSON


struct RecruiterProfile {
    
    var dashboardData = [RecruiterDashboardData]()
    var recruiterInfoData :RecruiterInfoData!
    
    init(dict: [String: JSON]) {
      
        
        let _ = dict["detail"].map{ value in
            
            let data = RecruiterInfoData.init(dict: value)
            self.recruiterInfoData = data
            
            
        }
        guard let dashboard = dict["dashboard"] else{return}
        
        _ = dashboard.arrayValue.map{ value in
            
            let data = RecruiterDashboardData.init(dict: value)
            self.dashboardData.append(data)
            
        }
    }
}

struct RecruiterDashboardData {
    let type: Int
    let count: Int
    let title : String
    
    init(dict: JSON) {
        
         self.type           = dict["type"].intValue
         self.count           = dict["count"].intValue
         self.title           = dict["title"].stringValue
    }
    
}

struct RecruiterInfoData {
    var firstName: String
    var lastName: String
    var email: String
    var countrycode: String
    var mobile : String
   
    var recruiterImage : String
    let userId : String
    var userType: Int
    let createdTimeStamp: String
    let updatedTimeStamp: String
    var recruiterCompanyData: RecruiterCompanyData!
    var user_image : String
    var createdAt: Date? {
        
        guard !self.createdTimeStamp.isEmpty else{
            return nil
        }
        
        guard let time = Int64(self.createdTimeStamp) else {
            return nil
        }
        
        let date = dateFromMilliseconds(timestamp: time)
        return date
    }
    
    var updatedAt: Date? {
        
        guard !self.updatedTimeStamp.isEmpty else{
            return nil
        }
        
        guard let time = Int64(self.updatedTimeStamp) else {
            return nil
        }
        
        let date = dateFromMilliseconds(timestamp: time)
        return date
    }
    
    init(dict: JSON) {
        
        self.firstName               = dict["first_name"].stringValue
        self.lastName                = dict["last_name"].stringValue
        self.email                      = dict["email"].stringValue
        self.countrycode                  = dict["country_code"].stringValue
        self.mobile                      = dict["mobile"].stringValue
        self.userId                     = dict["_id"].stringValue
        self.recruiterImage             = dict["user_image"].stringValue
        self.createdTimeStamp             = dict["created_at"].stringValue
        self.updatedTimeStamp            = dict["updated_at"].stringValue
        self.userType                    = dict["user_type"].intValue
        self.user_image                   = dict["user_image"].stringValue
        self.recruiterCompanyData = RecruiterCompanyData.init(dict: dict["company"])
    }
    
}



struct RecruiterCompanyData {
    var companyName: String
    var isOtpVerified: Int
    var otpExpireTime: String
    var countrycode: String
    var mobile : String
    var mobileNo : String
    var companyAddress: String
    var companyLattitude: String
    var companyLongitude: String
    
    var otp: String
    var companyDesc: String
    var designation: String
    
    init(dict: JSON) {
        
        self.companyName               = dict["companyName"].stringValue
        self.isOtpVerified                    = dict["isOtpVerified"].intValue
        self.otpExpireTime                = dict["otpExpireTime"].stringValue
        self.countrycode                  = dict["countryCode"].stringValue
        self.mobile                      = dict["mobileFormat"].stringValue
        self.mobileNo                     = dict["mobile"].stringValue
        self.companyAddress              = dict["companyAddress"].stringValue
        self.companyLattitude            = dict["companyLatitude"].stringValue
        self.companyLongitude           = dict["companyLongitude"].stringValue
        self.otp                     = dict["otp"].stringValue
        self.companyDesc             = dict["companyDesc"].stringValue
        self.designation             = dict["designation"].stringValue
        
        
    }
    
}
