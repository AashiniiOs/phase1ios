//
//  TransactionListModel.swift
//  JobGet
//
//  Created by Admin on 19/09/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import Foundation
import SwiftyJSON


struct TransactionList {
    
    var transectionId: String = ""
    var planName: String = ""
    var planId: String = ""
    var userId: String = ""
    var amount: String = ""
    var stars: String = ""
    var paymentMethod = ""
    var paymentType = ""
    var createdTimeStamp: String
    
    var createdAt: Date? {
        
        guard !self.createdTimeStamp.isEmpty else {
            return nil
        }
        
        guard let time = Int64(self.createdTimeStamp) else {
            return nil
        }
        
        let date = dateFromMilliseconds(timestamp: time)
        print_debug(date)
        return date
    }
    
    init(withJSON json: JSON) {
        self.transectionId = json["transectionId"].stringValue
        self.planName = json["planName"].stringValue
        self.planId = json["planId"].stringValue
        self.userId = json["userId"].stringValue
        self.amount = json["amount"].stringValue
        self.stars = json["stars"].stringValue
        self.createdTimeStamp = json["timestamp"].stringValue
        self.paymentType = json["paymentFor"].stringValue
        self.paymentMethod = json["paymentMethod"].stringValue
    }
}
