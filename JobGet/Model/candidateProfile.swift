//
//  candidateProfile.swift
//  JobGet
//
//  Created by Abhi on 22/05/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import Foundation
import SwiftyJSON

struct CandidateProfileModel {
    
    var experience : [ExperienceModel]
    var education : String
    var isSearch = 1
    var about : String
    var userImage : String
    var aboutMeTextViewheight: CGFloat = 100
    var educationTextViewHeight: CGFloat = 56.5
    var imageToUpload : UIImage?
    var isExperience : Int // set 1 for no experience and 2 for expeiance
    var durationType : Int // 1 for month 2 for type
    var address : String
    var state : String
    var city : String
    
    var latitude : String
    var longitude : String

    init(withJSON json : JSON){
        
        let sortedExperience = CommonClass.sortedInHeighestExperience(json["experience"].arrayValue)
        self.experience = sortedExperience.map({ExperienceModel(withJSON: $0)})
        self.education = json["education"].stringValue
        self.about = json["about"].stringValue
        self.userImage = json["userImage"].stringValue
        self.isExperience = json["isExperience"].intValue
        self.durationType = json["durationType"].intValue
        self.isSearch = json["isSearch"].int ?? 1
        self.state = json["state"].stringValue
        self.city = json["city"].stringValue
        self.address = json["address"].stringValue
        self.latitude = json["latitude"].stringValue
        self.longitude = json["longitude"].stringValue
    }
    func dictionary() -> [String:Any]{
        
        var details = [String:Any]()
        
        
        let dic = CommonClass().convertToApiFormat(experience.map({$0.dictionaryValue}))?.components(separatedBy: .newlines)
        
        details["experience"] = dic?.joined()
        details["education"] = education
        details["about"] = about
        details["userImage"] = userImage
        details["durationType"] = durationType
        details["isExperience"] = isExperience
        details["is_profile_added"] = 1
        details["isSearch"] = 1
        details["city"] = city
        details["state"] = state
        details["address"] = address
        details["latitude"] = latitude
        details["longitude"] = longitude
       
        
        return details
    }
}

struct ExperienceModel {
    
    var companyName : String
    var position : String
    var duration : String
    var categoryId : String
    var categoryTitle: String
    var expType : Int
    var durationType: Int // 1 for months and 2 for year
    var isCurrentCompany : Int
    var rawSliderExp: Float = 0 // use this value to set slider value locally
    var categoryName: String
    var id: String
    
    var dictionaryValue : [String:Any]{
        
        var details = [String:Any]()
        
        details["companyName"] = companyName
        if categoryName.isEmpty{
            details["categoryTitle"] = categoryTitle
            
        }else{
            details["categoryTitle"] = categoryName
            
        }
        
        if categoryId.isEmpty{
            details["categoryId"] = id
        }else{
            details["categoryId"] = categoryId
        }
        
        details["position"] = position
        details["duration"] = duration
        details["expType"] = expType
        details["isCurrentCompany"] = isCurrentCompany
        details["durationType"] = durationType
        
        return details
        
    }
    init(withJSON json : JSON){
        
        self.companyName  = json["companyName"].stringValue
        self.position     = json["position"].stringValue
        self.duration     = json["duration"].stringValue
        self.categoryId   = json["categoryId"].dictionaryValue["_id"]?.string ?? ""
        self.expType      = json["expType"].intValue
        self.isCurrentCompany = json["isCurrentCompany"].intValue
        self.durationType    = json["durationType"].intValue
        self.categoryTitle   = json["categoryId"].dictionaryValue["categoryTitle"]?.string ?? ""
        self.id   = json["categoryId"].stringValue
        self.categoryName = json["categoryTitle"].stringValue
    }
}

