//
//  AppNetworking+Loader.swift
//  StarterProj
//
//  Created by Gurdeep on 06/03/17.
//  Copyright © 2017 Gurdeep. All rights reserved.
//

import Foundation
import UIKit
import PKHUD

extension AppNetworking {
    
    static func showLoader() {
        HUD.show(.progress)
    }
    
    static func hideLoader() {
        HUD.hide()
    }
    
    static func showUploadImageLoader(){
        
        HUD.show(.label("Profile Image is uploading..."))
    }
}

