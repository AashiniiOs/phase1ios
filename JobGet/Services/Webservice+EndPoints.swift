//
//  Webservice+EndPoints.swift
//  StarterProj
//
//  Created by Gurdeep on 06/03/17.
//  Copyright © 2017 Gurdeep. All rights reserved.
//

import Foundation

//let BASE_URL = "http://jobsget.appinventive.com:7140/api/v0/"  // develoment server

let STATIC_PAGE_BASE_URL = "http://184.73.96.84:3000/"
//let STATIC_PAGE_BASE_URL = "http://test.jobget.com/"
//let BASE_URL = "http://jobsget.appinventive.com:7140/api/v0/"  // develoment server

//let BASE_URL = "http://jobsget.appinventive.com:7180/api/v0/" // staging server
//let BASE_URL = "http://jobsget.appinventive.com:7140/api/v0/" // // develoment server

//Live Url
let BASE_URL = "http://184.73.96.84:3000/api/v0/" // Beta Live server
//let BASE_URL = "http://test.jobget.com/api/v0/"
//let BASE_URL = "http://jobsget.appinventive.com:7180/api/v0/" // staging server

extension WebServices {
    
    enum EndPoint : String {
        
        case signup                 = "users/signup"
        case login                  = "users/login"
        case changepassword         = "users/change-password"
        case forgotpassword         = "users/forgot"
        case profiledata
        case resetpassword          = "users/resetPassword"
        case verifyOtp              = "users/verifyOtp"
        case resendOtp              = "users/resendOtp"
        case fbLogin                = "users/facebook-login"
        case managefriends
        case logout                 = "logout"
        case createRecruiterProfile = "users/createProfile"
        case postJob                = "job/add"
        case planList               = "plan-list"
        case recruiterJobList       = "job/list"
        case categoryList           = "category-list"
        case getJobPendingDetail    = "job/detail"
        case deletePostedJob        = "job/delete"
        case editJob                = "job/edit"
        case applyJob               = "users/apply-job"
        case candidateJobList       = "users/job-list"
        case candidateJobDetail     = "users/job-detail"
        case userSavedJob           = "users/my-jobs"
        case saveJob                = "users/save-job"
        case pendingJobStatus       = "job/change-status"
        case addProfile             = "users/add-profile"
        case userDetail             = "job/user-detail"
        case applyUsers             = "job/apply-users"
        case shortlistUser          = "job/shortlist-user"
        case recruiterProfile       = "recruiter-profile"
        case recruiterUpdateProfile = "recruiter-update-profile"
        case addJobCategory         = "add-category"
        case rateApp                = "rate-app"
        case contact_us             = "contact-us"
        case recruiterJobGetail     = "job/view-detail"
        case reportCandidate        = "report-candidate"
        case reportUser             = "report-user"
        case notification_list      = "notification-list"
        case reportJob              = "report-job"
        case notificationDelete     = "notification-delete"
        case notifyVideoCall        = "notify-call"
        case recruiterJobsList      = "recruiter-view"
        case deactivateAccount      = "deactivate-account"
        case record_call            = "record-call"
        case call_list              = "call-list"
        case visitor_list           = "visitor-list"
        case delete_one_notification           = "delete-one-notifiaction"
        case similar_jobs           = "similar-jobs"
        case card_list              = "card-list"
        case add_cards              = "add-cards"
        case client_token           = "client-token"
        case delete_card            = "delete-card"
        case create_transaction     = "create-transaction"
        case buy_premium            = "buy-premium"
        case transaction_list       = "transaction-list"
        case inapp_purchase         = "inapp-purchase"
        case buy_subscription       = "buy-subscription"
        case deduct_stars           = "deduct-stars"
        case active_jobs            = "active-jobs"
        case notify                 = "is-notify"
        case getVersion             = "get-version"

        var path : String {
            
            //            let url = BASE_URL + DEV_URL
            let url = BASE_URL
            return url + self.rawValue
        }
    }
}

