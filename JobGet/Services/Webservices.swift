//
//  Webservices.swift
//  StarterProj
//
//  Created by Gurdeep on 16/12/16.
//  Copyright © 2016 Gurdeep. All rights reserved.
//

import Foundation
import SwiftyJSON
import Firebase
import Alamofire

enum WebServices { }

extension NSError {
    
    convenience init(localizedDescription : String) {
        
        self.init(domain: "AppNetworkingError", code: 0, userInfo: [NSLocalizedDescriptionKey : localizedDescription])
    }
    
    convenience init(code : Int, localizedDescription : String) {
        
        self.init(domain: "AppNetworkingError", code: code, userInfo: [NSLocalizedDescriptionKey : localizedDescription])
    }
}

extension WebServices {
    
    static func loginAPI(parameters : JSONDictionary, success : @escaping UserControllerSuccess, failure : @escaping FailureResponse) {
        
        // Configure Parameters and Headers
        AppNetworking.showLoader()
        AppNetworking.POST(endPoint: .login, parameters: parameters, loader: false, success: { (json) -> () in
            print_debug(json)
            var isOtpVerified = ""
            
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                
                if let result = json["data"].dictionary {
                    
                    if let data = json["data"].dictionaryObject {
                        
                        AppUserDefaults.save(value: data, forKey: .userData)
                        if let userType = data["user_type"] {
                            AppUserDefaults.save(value: userType, forKey: .userType)
                        }
                        AppUserDefaults.save(value: true, forKey: .isLogin)
                        if let isProfileAdded = data["is_profile_added"] {
                            print_debug(isProfileAdded)
                            AppUserDefaults.save(value: isProfileAdded, forKey: .isProfileAdded)
                        } 
                        
                        if let userId = data["user_id"] {
                            AppUserDefaults.save(value: userId, forKey: .userId)

                        }
                        if let isNotify = data["isNotify"] {
                            AppUserDefaults.save(value: isNotify, forKey: .isNotify)
                            
                        }

                        if let isProfileAdded = data["stars"] {
                            print_debug(isProfileAdded)
                            AppUserDefaults.save(value: isProfileAdded, forKey: .stars)
                        }
                        
                        if let userId = data["freeStars"] {
                            AppUserDefaults.save(value: userId, forKey: .freeStars)
                            
                        }
                        if let userId = data["referralCode"] {
                            AppUserDefaults.save(value: userId, forKey: .referralCode)
                        }
                        
                        if let userId = data["referralUrl"] {
                            AppUserDefaults.save(value: userId, forKey: .referralUrl)
                        }
                        
                        if let userId = data["inviteUrl"] {
                            AppUserDefaults.save(value: userId, forKey: .inviteUrl)
                        }
                        
                        if let totalNotification = data["totalNotification"] {
                            AppUserDefaults.save(value: totalNotification, forKey: .totalNotification)
                        }
                        
                        if let company = data["company"] as? [String: Any] {
                            
                            isOtpVerified = company["isOtpVerified"] as? String ?? ""
                             print_debug(isOtpVerified)
                            AppUserDefaults.save(value: isOtpVerified, forKey: .isOtpVerified)
                            
                            let mobileFormat = company["mobileFormat"] as? String ?? ""
                            AppUserDefaults.save(value: mobileFormat, forKey: .userPhoneNoFormatWithCode)
                            
                            if let mobileNo = company["mobile"] {
                                AppUserDefaults.save(value: mobileNo, forKey: .recruiterMobile)
                            }
                            if let countryCode = company["countryCode"] {
                                AppUserDefaults.save(value: countryCode, forKey: .recruiterCountryCode)
                            }
                            if let companyName = company["companyName"] {
                                AppUserDefaults.save(value: companyName, forKey: .recruiterCompanyName)
                            }
                            
                            if let companyName = company["companyAddress"] {
                                AppUserDefaults.save(value: companyName, forKey: .recruiterCompanyAddress)
                            }
                        }
                    }
                    
                    if let rspMsg = json["message"].string {
                        
                        let userInfo = User(dictionary: result)
                        let _ = User(isOtpVerified: isOtpVerified)
                        userInfo.responseString = rspMsg
                        if let fcmTocken = Messaging.messaging().fcmToken{
                            ChatHelper.updateDeviceToken(userId: AppUserDefaults.value(forKey: .userId).stringValue, deviceToken: fcmTocken)
                        }
//                        ChatHelper.updateDeviceType(userId: AppUserDefaults.value(forKey: .userId).stringValue, deviceType: "2")
                        ChatHelper.signIn(withEmail: userInfo.email, withUserId: userInfo.user_id ?? "", completion: { (status) in
                            AppNetworking.hideLoader()
                            if status{
                                print_debug("========================\n\n")
                                print_debug("User logged in firebase\n\n")
                                print_debug("========================")
                                success(userInfo)
                            }else{
                                print_debug("========================\n\n")
                                print_debug("User cannot log in firebase\n\n")
                                print_debug("========================")
                                let error = NSError(code: 6994, localizedDescription: "Cannot login to firebase")
                                failure(error)
                            }
                        })
                        
                    }
                    if let msg = json["pauseMsg"].string {
                        
                        AppUserDefaults.save(value: msg, forKey: .pauseMsg)
                    }

                    if let access_token = result["authToken"]?.string {
                        AppUserDefaults.save(value: access_token, forKey: .Accesstoken)
                    }
                }
            } else {
                
                let errorCode = json["code"]
                let  errorMsg = json["message"]
                 AppNetworking.hideLoader()
                CommonClass.showToast(msg: errorMsg.stringValue)
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }, failure: { (e : Error) -> Void in
            AppNetworking.hideLoader()
            failure(e)
            
        })
    }
    
    static func signupApi(parameters : JSONDictionary, success : @escaping UserControllerSuccess, failure : @escaping FailureResponse) {
        
        // Configure Parameters and Headers
        AppNetworking.POST(endPoint: .signup, parameters: parameters, loader: true, success: { (json) -> () in
            
            print_debug(json)
            
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                
                if let result = json["data"].dictionary {
                    
                    if let data = json["data"].dictionaryObject {
                        
                        AppUserDefaults.save(value: data, forKey: .userData)
                        if let userType = data["user_type"] {
                            AppUserDefaults.save(value: userType, forKey: .userType)
                        }
                         AppUserDefaults.save(value: true, forKey: .isLogin)
                        if let userId = data["user_id"] {
                            AppUserDefaults.save(value: userId, forKey: .userId)
                        }
                        if let isNotify = data["isNotify"] {
                            AppUserDefaults.save(value: isNotify, forKey: .isNotify)
                        }
                        
                        if let totalNotification = data["totalNotification"] {
                            AppUserDefaults.save(value: totalNotification, forKey: .totalNotification)
                        }
                        if let isProfileAdded = data["stars"] {
                            print_debug(isProfileAdded)
                            AppUserDefaults.save(value: isProfileAdded, forKey: .stars)
                        }
                        
                        if let userId = data["freeStars"] {
                            AppUserDefaults.save(value: userId, forKey: .freeStars)
                            
                        }
                        if let userId = data["referralCode"] {
                            AppUserDefaults.save(value: userId, forKey: .referralCode)
                        }
                        
                        if let userId = data["referralUrl"] {
                            AppUserDefaults.save(value: userId, forKey: .referralUrl)
                        }
                        
                        if let isProfileAdded = data["is_profile_added"] {
                            AppUserDefaults.save(value: isProfileAdded, forKey: .isProfileAdded)
                        }
                        
                        if let company = data["company"] as? [String: Any] {
                            if let mobileNo = company["mobile"] {
                                AppUserDefaults.save(value: mobileNo, forKey: .recruiterMobile)
                            }
                            if let countryCode = company["countryCode"] {
                                AppUserDefaults.save(value: countryCode, forKey: .recruiterCountryCode)
                            }
                            if let companyName = company["companyName"] {
                                AppUserDefaults.save(value: companyName, forKey: .recruiterCompanyName)
                            }
                            
                            if let formattedPhoneNo = company["mobileFormat"] {
                                AppUserDefaults.save(value: formattedPhoneNo, forKey: .userPhoneNoFormatWithCode)
                            }
                        }
                    }
                    
                    if let rspMsg = json["message"].string {
                        
                        let userInfo = User(dictionary: result)
                        userInfo.responseString = rspMsg
                        success(userInfo)
                        
                    }
                    let user = User.getUserModel()
                    if let fcmTocken = Messaging.messaging().fcmToken{
                        ChatHelper.updateDeviceToken(userId: AppUserDefaults.value(forKey: .userId).stringValue, deviceToken: fcmTocken)
                    }
//                    ChatHelper.updateDeviceType(userId: AppUserDefaults.value(forKey: .userId).stringValue, deviceType: "2")
                    ChatHelper.signIn(withEmail: user.email, withUserId: user.user_id ?? "", completion: { (status) in
                        AppNetworking.hideLoader()
                        if status{
                            print_debug("========================\n\n")
                            print_debug("User logged in firebase\n\n")
                            print_debug("========================")
//                            success(user)
                        }else{
                            print_debug("========================\n\n")
                            print_debug("User cannot log in firebase\n\n")
                            print_debug("========================")
                            let error = NSError(code: 6994, localizedDescription: "Cannot login to firebase")
                            failure(error)
                        }
                    })

                    if let access_token = result["authToken"]?.string {
                        AppUserDefaults.save(value: access_token, forKey: .Accesstoken)
                    }
                }
            } else if let errorCode = json["code"].int, errorCode == error_codes.accountAlreadyExist {
                let sceen = ResetPasswordPopupVC.instantiate(fromAppStoryboard: .Main)
                commingFrom = .accountExists
                sceen.exitAccountText = json["message"].stringValue
                sceen.modalTransitionStyle = .crossDissolve
                sceen.modalPresentationStyle = .overFullScreen
                sharedAppDelegate.window?.rootViewController?.present(sceen, animated: true, completion: nil)
            }else{
                let errorCode = json["code"]
                let  errorMsg = json["message"]
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                failure(error)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func socialLogin(parameters : JSONDictionary, success : @escaping UserControllerSuccess, failure : @escaping (Error) -> Void) {
        
        AppNetworking.POST(endPoint: .fbLogin, parameters: parameters, loader: true, success: { (json : JSON) -> Void in
            print_debug(json)
            
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                
                if let result = json["data"].dictionary {
                    
                    if let data = json["data"].dictionaryObject {
                        AppUserDefaults.save(value: data, forKey: .userData)
                        if let accessToken = data["authToken"] {
                            AppUserDefaults.save(value: accessToken, forKey: .Accesstoken)
                        }
                        if let totalNotification = data["totalNotification"] {
                            AppUserDefaults.save(value: totalNotification, forKey: .totalNotification)
                        }
                         AppUserDefaults.save(value: true, forKey: .isLogin)
                        
                        if let isProfileAdded = data["stars"] {
                            print_debug(isProfileAdded)
                            AppUserDefaults.save(value: isProfileAdded, forKey: .stars)
                        }
                        
                        if let userId = data["freeStars"] {
                            AppUserDefaults.save(value: userId, forKey: .freeStars)
                            
                        }
                        if let isNotify = data["isNotify"] {
                            AppUserDefaults.save(value: isNotify, forKey: .isNotify)
                            
                        }

                        if let referralCode = data["referralCode"] {
                            AppUserDefaults.save(value: referralCode, forKey: .referralCode)
                        }
                        
                        if let referralUrl = data["referralUrl"] {
                            AppUserDefaults.save(value: referralUrl, forKey: .referralUrl)
                        }
                        if let isProfileAdded = data["is_profile_added"] {
                            AppUserDefaults.save(value: isProfileAdded, forKey: .isProfileAdded)
                        }
                        
                        if let userId = data["user_id"] {
                            AppUserDefaults.save(value: userId, forKey: .userId)
                        }
                        if let userType =  data["user_type"] {
                            AppUserDefaults.save(value: userType, forKey: .userType)
                        }
                        
                        if let company = data["company"] as? [String: Any] {
                            
                          let  isOtpVerified = company["isOtpVerified"] as? String ?? ""
                            AppUserDefaults.save(value: isOtpVerified, forKey: .isOtpVerified)
                            
                            let mobileFormat = company["mobileFormat"] as? String ?? ""
                            AppUserDefaults.save(value: mobileFormat, forKey: .userPhoneNoFormatWithCode)
                            
                            if let mobileNo = company["mobile"] {
                                AppUserDefaults.save(value: mobileNo, forKey: .recruiterMobile)
                            }
                            if let countryCode = company["countryCode"] {
                                AppUserDefaults.save(value: countryCode, forKey: .recruiterCountryCode)
                            }
                            if let companyName = company["companyName"] {
                                AppUserDefaults.save(value: companyName, forKey: .recruiterCompanyName)
                            }
                            
                            if let companyName = company["companyAddress"] {
                                AppUserDefaults.save(value: companyName, forKey: .recruiterCompanyAddress)
                            }
                        }
                    }
                    
                    
                    if let rspMsg = json["message"].string {
                        
                        let userInfo = User(dictionary: result)
                        userInfo.responseString = rspMsg
                        success(userInfo)
                        
                    }
                    if let msg = json["pauseMsg"].string {
                        
                        AppUserDefaults.save(value: msg, forKey: .pauseMsg)
                    }

                    let user = User.getUserModel()
                    if let fcmTocken = Messaging.messaging().fcmToken{
                         ChatHelper.updateDeviceToken(userId: AppUserDefaults.value(forKey: .userId).stringValue, deviceToken: fcmTocken)
                    }
                  
//                    ChatHelper.updateDeviceType(userId: AppUserDefaults.value(forKey: .userId).stringValue, deviceType: "2")
//                    ChatHelper.createUser(withEmail: user.email, completion: { (success) in
//                        if success{
//                            print_debug("========================\n\n")
//                            print_debug("User created in firebase\n\n")
//                            print_debug("========================")
//                        }else{
//                            print_debug("========================\n\n")
//                            print_debug("User creation failed in firebase\n\n")
//                            print_debug("========================")
//                        }
//                    })
                    
                    ChatHelper.signIn(withEmail: user.email, withUserId: user.user_id ?? "", completion: { (status) in
                        AppNetworking.hideLoader()
                        if status{
                            print_debug("========================\n\n")
                            print_debug("User logged in firebase\n\n")
                            print_debug("========================")
                            success(user)
                        }else{
                            print_debug("========================\n\n")
                            print_debug("User cannot log in firebase\n\n")
                            print_debug("========================")
                            let error = NSError(code: 6994, localizedDescription: "Cannot login to firebase")
                            failure(error)
                        }
                    })
                    if let access_token = result["authToken"]?.string {
                        AppUserDefaults.save(value: access_token, forKey: .Accesstoken)
                    }
                }
            } else if let errorCode = json["code"].int, errorCode == error_codes.invalidToken{
                CommonClass.showToast(msg: json["message"].stringValue)
            } else {
                
                let errorCode = json["code"]
                let  errorMsg = json["message"]
                
                let error = NSError(code: errorCode.intValue, localizedDescription: errorMsg.stringValue)
                CommonClass.showToast(msg: json["message"].stringValue)
                failure(error)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
        
        //            } else {
        //
        //            }
        //
        //        }, failure: { (e : Error) -> Void in
        //            failure(e)
        //        })
    }
    
    
    static func jobPost(parameters : JSONDictionary, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        print_debug(parameters)
        AppNetworking.POST(endPoint: .postJob, parameters: parameters, loader: true, success: { (json : JSON) -> Void in
            print_debug(json)
            
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
                
                
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
            
            
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func getVersionNumber(params : [String:Any],success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        AppNetworking.GET(endPoint: .getVersion, parameters: params, loader: false, success: { (json : JSON) -> Void in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }

    
    static func addJobCategory(parameters : JSONDictionary, loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        
        AppNetworking.POST(endPoint: .addJobCategory, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            print_debug(json)
            
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
                
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
            
            
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func rateApp(parameters : JSONDictionary, loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        AppNetworking.POST(endPoint: .rateApp, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
             print_debug(parameters)
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
                
                
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func recruiterJobList(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
      
        AppNetworking.GET(endPoint: .recruiterJobList, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
           
            print_debug(json)
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()

                
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func getSaveJob(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
      
        AppNetworking.GET(endPoint: .userSavedJob, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
                
                
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
            
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func getCandidateJobList(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        AppNetworking.GET(endPoint: .candidateJobList, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
                
                
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
            
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func recruiterJobDetail(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
       
        AppNetworking.GET(endPoint: .recruiterJobGetail, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
        } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
            
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    
    static func changePasswordAPI(parameters : JSONDictionary, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        // Configure Parameters and Headers
        AppNetworking.POST(endPoint: .changepassword, parameters: parameters, loader: true, success: { (json : JSON) -> Void in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
                
                
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    static func forgotPasswordAPI(email : String, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        // Configure Parameters and Headers
        
        let params = ["email" : email, "user_type" : userType.rawValue] as [String: Any]
        
        AppNetworking.POST(endPoint: .forgotpassword, parameters: params, loader: true, success: { (json : JSON) -> Void in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
                
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func recruiterSignup(parameter : JSONDictionary, loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        AppNetworking.POST(endPoint: .createRecruiterProfile, parameters: parameter, loader: true, success: { (json : JSON) -> Void in
            print_debug(json)
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                
                if let data = json["data"].dictionary {
                    
                    if let companyDict = data["company"]?.dictionaryObject {
                        print_debug(companyDict)
                        AppUserDefaults.save(value: companyDict, forKey: .recruiterCreateProfile)
                        if let isVerified = companyDict["isOtpVerified"] {
                            AppUserDefaults.save(value: isVerified, forKey: .isOtpVerified)
                        }
                    }
                }
                success(json)
            } else {
                let message = json["message"].stringValue
                CommonClass.showToast(msg: message)
            }
            
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    static func resetPasswordAPI(parameters : JSONDictionary, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        // Configure Parameters and Headers
        
        AppNetworking.POST(endPoint: .resetpassword, parameters: parameters, loader: true, success: { (json : JSON) -> Void in
            
            print_debug(json)
            guard let _ = json.dictionary else {
                
                let e = NSError(localizedDescription: "")
                
                // check error code and present respective error message from here only
                
                failure(e)
                
                return
            }
            
            success(json)
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    static func verifyOtp(parameters : JSONDictionary, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        // Configure Parameters and Headers
        
        AppNetworking.POST(endPoint: .verifyOtp, parameters: parameters, loader: true, success: { (json : JSON) -> Void in
            print_debug(json)
            
            success(json)
            
            //            if let errorCode = json["code"].int, errorCode == error_codes.success {
            //
            //
            //            } else {
            //                let msg = json["message"].stringValue
            //                CommonClass.showToast(msg: msg)
            //            }
            
        }, failure: { (e : Error) -> Void in
            failure(e)
            print_debug(e)
        })
    }
    
    static func resendOtp( success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        // Configure Parameters and Headers
        let parameters = ["" : ""]
        AppNetworking.POST(endPoint: .resendOtp, parameters: parameters, loader: true, success: { (json : JSON) -> Void in
            print_debug(json)
            
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
                
            } else {
                
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
            
        }, failure: { (e : Error) -> Void in
            failure(e)
            print_debug(e)
        })
    }
    
    static func editJobDetail(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        AppNetworking.POST(endPoint: .editJob, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
                
                
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
            
        }, failure: { (e : Error) -> Void in
            failure(e)
            print_debug(e)
        })
    }
    
    static func getCandidateJobDetail(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        AppNetworking.GET(endPoint: .candidateJobDetail, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
            
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    static func applyJob(parameters : JSONDictionary, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        AppNetworking.POST(endPoint: .applyJob, parameters: parameters, loader: true, success: { (json : JSON) -> Void in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
                
                
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
            
        }, failure: { (e : Error) -> Void in
            failure(e)
            print_debug(e)
        })
    }
    
    static func saveJob(parameters : JSONDictionary, loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        AppNetworking.POST(endPoint: .saveJob, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
                
                
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
            
        }, failure: { (e : Error) -> Void in
            failure(e)
            print_debug(e)
        })
    }
    
    static func profileAPI(success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        // Configure Parameters and Headers
        
        AppNetworking.GET(endPoint: .profiledata, parameters: [:], loader: true, success: { (json : JSON) -> Void in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
                
                
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func getUserList(parameters : JSONDictionary,success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        // Configure Parameters and Headers
        
        AppNetworking.GET(endPoint: .managefriends, parameters: parameters, loader: false, success: { (json : JSON) -> Void in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
                
                
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func getFeaturedPlan(parameters : JSONDictionary,loader: Bool,success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        // Configure Parameters and Headers
        
        AppNetworking.GET(endPoint: .planList, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            
            
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
                
                
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func getJobPendingDetail(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        // Configure Parameters and Headers
        
        AppNetworking.GET(endPoint: .getJobPendingDetail, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
                
                
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    static func notifyVideoCall(parameters : JSONDictionary,  loader: Bool,success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        print_debug(parameters)
        AppNetworking.POST(endPoint: .notifyVideoCall, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            print_debug(json)
            
            success(json)
            
            
            
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
        
    }
    
    
    static func deletePostedJob(parameters : JSONDictionary,success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        AppNetworking.POST(endPoint: .deletePostedJob, parameters: parameters, loader: true, success: { (json : JSON) -> Void in
            
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
                
                
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
            
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func changePendingJobStatus(parameters : JSONDictionary,loader: Bool,success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        AppNetworking.POST(endPoint: .pendingJobStatus, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
                
                
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
            
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func logoutAPI(success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        // Configure Parameters and Headers
        
       // let parameters : [String:Any] = ["device_token":DeviceDetail.deviceToken]
        
        //        if AppUserDefaults.value(forKey: .Accesstoken) != JSON.null {
        //
        //            parameters["accesstoken"] = AppUserDefaults.value(forKey: .Accesstoken).stringValue
        //
        //        }
        AppNetworking.PUT(endPoint: .logout, parameters: [:], loader: true, success: { (json : JSON) -> Void in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func notifyAPI(params: [String:Any], success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        // Configure Parameters and Headers
        AppNetworking.PUT(endPoint: .notify, parameters: params, loader: true, success: { (json : JSON) -> Void in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }

    
    static func deactivateAccount(parameters : JSONDictionary,success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        // Configure Parameters and Headers
        
        //        var parameters : [String:Any] = [:]
        //
        //        if AppUserDefaults.value(forKey: .Accesstoken) != JSON.null {
        //
        //            parameters["accesstoken"] = AppUserDefaults.value(forKey: .Accesstoken).stringValue
        //
        //        }
        
        AppNetworking.PUT(endPoint: .deactivateAccount, parameters: parameters, loader: true, success: { (json : JSON) -> Void in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func getRecruiterJobList(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        AppNetworking.GET(endPoint: .recruiterJobsList, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func getCategoryList(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        // Configure Parameters and Headers
        
        AppNetworking.GET(endPoint: .categoryList, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
            
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func addProfile(parameters : JSONDictionary,loader: Bool,success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        print_debug(parameters)
        AppNetworking.POSTWithRawJSON(endPoint: .addProfile, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            AppUserDefaults.removeValue(forKey: .userData)
            
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                if let data = json["data"].dictionaryObject {
                    
                    AppUserDefaults.save(value: data, forKey: .userData)
                    
                    success(json)
                    
                }
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func getCandidateList(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        // Configure Parameters and Headers
        print_debug(parameters)
        AppNetworking.GET(endPoint: .applyUsers, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            
            
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
                
                
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func recruiterProfileDetail(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        AppNetworking.GET(endPoint: .recruiterProfile, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    
    
    static func getCandidateDetail(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        // Configure Parameters and Headers
        print_debug(parameters)
        AppNetworking.GET(endPoint: .userDetail, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func candidateShortlisted(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        // Configure Parameters and Headers
        print_debug(parameters)
        AppNetworking.POST(endPoint: .shortlistUser, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func recruiterEditProfile(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
       
        AppNetworking.POST(endPoint: .recruiterUpdateProfile, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            
            
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                
                if let data = json["data"].dictionaryObject {
                    
//                    AppUserDefaults.save(value: data, forKey: .userData)
                    if let userType = data["user_type"] {
                        AppUserDefaults.save(value: userType, forKey: .userType)
                    }
                    if let userType = data["first_name"] {
                        AppUserDefaults.save(value: userType, forKey: .firstName)
                    }
                    if let userType = data["last_name"] {
                        AppUserDefaults.save(value: userType, forKey: .lastName)
                    }
                    AppUserDefaults.save(value: true, forKey: .isLogin)
                    if let isProfileAdded = data["is_profile_added"] {
                        print_debug(isProfileAdded)
                        AppUserDefaults.save(value: isProfileAdded, forKey: .isProfileAdded)
                    }
                    
                    
                    if let userId = data["user_id"] {
                        AppUserDefaults.save(value: userId, forKey: .userId)
                        
                    }
                    if let isProfileAdded = data["stars"] {
                        print_debug(isProfileAdded)
                        AppUserDefaults.save(value: isProfileAdded, forKey: .stars)
                    }
                    
                    if let userId = data["freeStars"] {
                        AppUserDefaults.save(value: userId, forKey: .freeStars)
                        
                    }
                    if let userId = data["referralCode"] {
                        AppUserDefaults.save(value: userId, forKey: .referralCode)
                    }
                    
                    if let userId = data["referralUrl"] {
                        AppUserDefaults.save(value: userId, forKey: .referralUrl)
                    }
                    
                    if let userId = data["inviteUrl"] {
                        AppUserDefaults.save(value: userId, forKey: .inviteUrl)
                    }
                    
                    if let totalNotification = data["totalNotification"] {
                        AppUserDefaults.save(value: totalNotification, forKey: .totalNotification)
                    }
                    
                    if let company = data["company"] as? [String: Any] {
                        
                      
                        let mobileFormat = company["mobileFormat"] as? String ?? ""
                        AppUserDefaults.save(value: mobileFormat, forKey: .userPhoneNoFormatWithCode)
                        
                        if let mobileNo = company["mobile"] {
                            AppUserDefaults.save(value: mobileNo, forKey: .recruiterMobile)
                        }
                        if let countryCode = company["countryCode"] {
                            AppUserDefaults.save(value: countryCode, forKey: .recruiterCountryCode)
                        }
                        if let companyName = company["companyName"] {
                            AppUserDefaults.save(value: companyName, forKey: .recruiterCompanyName)
                        }
                        
                        if let companyName = company["companyAddress"] {
                            AppUserDefaults.save(value: companyName, forKey: .recruiterCompanyAddress)
                        }
                    }
                }
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
                
                
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func reportUser(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        AppNetworking.POST(endPoint: .reportUser, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func reportJob(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        AppNetworking.POST(endPoint: .reportJob, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    
    static func reportCandidate(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        AppNetworking.POST(endPoint: .reportCandidate, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    static func deleteNotificationList(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        AppNetworking.DELETE(endPoint: .notificationDelete, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func getNotificationList(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        AppNetworking.GET(endPoint: .notification_list, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func getCallHistoryApi(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        AppNetworking.GET(endPoint: .call_list, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func callRecordApi(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        AppNetworking.POST(endPoint: .record_call, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    
    static func visitorListApi(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        AppNetworking.GET(endPoint: .visitor_list, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func deleteSingleNotificationApi(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        AppNetworking.POST(endPoint: .delete_one_notification, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }

    static func similarJobsApi(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        AppNetworking.GET(endPoint: .similar_jobs, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    
    static func contactUs(parameters : JSONDictionary, loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        AppNetworking.POST(endPoint: .contact_us, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    //Get Card List
    static func getCardList(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        AppNetworking.GET(endPoint: .card_list, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    //Get Card List
    static func addCard(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        AppNetworking.POST(endPoint: .add_cards, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func getBrainTreeClientToken(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        AppNetworking.GET(endPoint: .client_token, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func deleteCard(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        AppNetworking.POST(endPoint: .delete_card, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
                
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func createTransaction(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        // Configure Parameters and Headers
        print_debug(parameters)
        AppNetworking.POST(endPoint: .create_transaction, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            
            
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
                
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func buyPremium(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        // Configure Parameters and Headers
        print_debug(parameters)
        AppNetworking.POST(endPoint: .buy_premium, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            
            
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
                
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func transactionList(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        // Configure Parameters and Headers
        print_debug(parameters)
        AppNetworking.GET(endPoint: .transaction_list, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            
            
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
                
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func inappPurchase(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        // Configure Parameters and Headers
        print_debug(parameters)
        AppNetworking.POST(endPoint: .inapp_purchase, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
                
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func buySubscription(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        // Configure Parameters and Headers
        print_debug(parameters)
        AppNetworking.POST(endPoint: .buy_subscription, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            
            
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                success(json)
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
                
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func deductStars(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        // Configure Parameters and Headers
        print_debug(parameters)
        AppNetworking.POST(endPoint: .deduct_stars, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            
            if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
                
            }else{
                success(json)
            }
            
            
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func activeJobsApi(parameters : JSONDictionary,loader: Bool, success : @escaping (JSON) -> Void, failure : @escaping (Error) -> Void) {
        
        // Configure Parameters and Headers
        print_debug(parameters)
        AppNetworking.GET(endPoint: .active_jobs, parameters: parameters, loader: loader, success: { (json : JSON) -> Void in
            
            if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
                
            }else{
                success(json)
            }
            
        }, failure: { (e : Error) -> Void in
            failure(e)
        })
    }
    
    static func checkInternet(flag:Bool, completionHandler:@escaping (_ internet:Bool) -> Void)
    {
        DispatchQueue.main.async {

            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        let url = NSURL(string: "http://www.appleiphonecell.com/")
        var request = URLRequest(url: url! as URL)
        
        request.httpMethod = "HEAD"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData
        request.timeoutInterval = 10.0
        
        let task = URLSession.shared.dataTask(with: request) {data, response, err in
            print("Entered the completionHandler")
            
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
            }
            
            let rsp = response as! HTTPURLResponse?
            
            completionHandler(rsp?.statusCode == 200)

            }
        task.resume()
//        NSURLConnection.sendAsynchronousRequest(request as URLRequest, queue:OperationQueue.main, completionHandler:
//            {(response: URLResponse?, data: Data?, error: Error?) -> Void in
//
//                UIApplication.shared.isNetworkActivityIndicatorVisible = false
//
//                let rsp = response as! HTTPURLResponse?
//
//                completionHandler(rsp?.statusCode == 200)
//        })
    }
}

//MARK:- Error Codes
//==================
struct error_codes {
    static let success = 200
    static let invalidToken = 400
    static let accountAlreadyExist = 409
    static let loginOtherDevice = 501
    static let not_enough_star = 400
}

