//
//  AddExperienceCell.swift
//  JobGet
//
//  Created by Abhi on 22/05/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import CircularSlider

class AddExperienceCell: UITableViewCell {
    
    @IBOutlet weak var categoryTextfield: SkyFloatingLabelTextField!
    @IBOutlet weak var companyTextfield: SkyFloatingLabelTextField!
    @IBOutlet weak var positionTextfield: SkyFloatingLabelTextField!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var durationValue: UILabel!
    @IBOutlet weak var sliderView: CircularSlider!
    @IBOutlet weak var cellBackgroundView: UIView!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var jobTypeLabel: UILabel!
    @IBOutlet weak var fullTimeButton: UIButton!
    @IBOutlet weak var partTimeButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.partTimeButton.isHidden = true
        self.fullTimeButton.isHidden = true
        self.jobTypeLabel.isHidden = true
        
        self.positionTextfield.autocapitalizationType = .sentences
        self.companyTextfield.autocapitalizationType = .sentences
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.cellBackgroundView.layer.cornerRadius = 4
        self.cellBackgroundView.setShadow()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func populateData(experianceData: ExperienceModel) {
        
        self.companyTextfield.text = experianceData.companyName
        self.positionTextfield.text = experianceData.position
        self.categoryTextfield.text = experianceData.categoryName
        if experianceData.durationType == 1 {
            
            if experianceData.duration == "1" || experianceData.duration == "0" {
                self.durationValue.text = "\(experianceData.duration) \(StringConstants.Month)"
            } else {
                self.durationValue.text = "\(experianceData.duration) \(StringConstants.Months)"
            }
            
        } else {
            if experianceData.duration == "1"  {
                self.durationValue.text = "\(experianceData.duration) \(StringConstants.Year)"
            } else {
                self.durationValue.text = "\(experianceData.duration) \(StringConstants.Years)"
            }
        }
        
        if experianceData.duration == "5+" {
            self.sliderView.setValue(400, animated: false)
        } else  {
            
            let exp = Float(experianceData.duration)
            let sliderValue = CommonClass.getExperienceAngle(experience: exp ?? 0, experienceType:  experianceData.durationType, fromTo: 0)
            self.sliderView.setValue(Float(sliderValue), animated: false)
            
        }
        // self.sliderView.setValue(experianceData.rawSliderExp, animated: false)
        
        print_debug(experianceData.rawSliderExp)
        
    }
}

