//
//  AddExperienceView.swift
//  JobGet
//
//  Created by Appinventiv on 29/06/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import CircularSlider

class AddExperienceView: UIView {
    
    @IBOutlet weak var categoryExperienceLabel: UILabel!
    @IBOutlet weak var minusBtnContainerView: UIView!
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var sliderView: CircularSlider!
    @IBOutlet weak var cellBackgroundView: UIView!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var bottomSeperatorView: UIView!
    @IBOutlet weak var catgoryContainerView: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.minusBtnContainerView.backgroundColor = AppColors.themeBlueColor
        self.minusBtnContainerView.roundCorners()
        self.catgoryContainerView.roundCorners()
        self.catgoryContainerView.backgroundColor = AppColors.gray15
        
        
    }
    
    func populatedCell(data: CandidateAppliedJobDetail, index: Int){
        
        print_debug(data.experience)
        self.companyNameLabel.text = data.experience[index].companyName
        self.positionLabel.text = data.experience[index].position
        
        var year = ""
        _ = (data.experience[index].duration)
        if data.experience[index].durationType == 1{
            year = "\(StringConstants.Month.lowercased())"
        }else{
            year = "\(StringConstants.Year.lowercased())"
        }
        
        
        
        self.categoryExperienceLabel.text = data.experience[index].categoryName
        
        print_debug(data.experience[index].duration)
        if data.experience[index].duration == "5+" {
            self.sliderView.setValue(400, animated: false)
        } else if let experianceValue = Float(data.experience[index].duration) {
            
            let sliderValue = CommonClass.getExperienceAngle(experience: experianceValue, experienceType: data.experience[index].durationType, fromTo: 0)
            self.sliderView.setValue(Float(sliderValue), animated: false)
        }
        
        var duration = ""
        duration = data.experience[index].duration
        if duration == "6", data.experience[index].durationType == 2 {
            let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: "5+ \n\(year)")
            attributedString.setColorForText(textForAttribute: "5+", withColor: AppColors.black46, font: AppFonts.Poppins_SemiBold.withSize(22))
            print_debug(attributedString)
            self.durationLabel.attributedText = attributedString
        } else {
            let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: "\(duration) \n\(year)")
            attributedString.setColorForText(textForAttribute: "\(duration)", withColor: AppColors.black46, font: AppFonts.Poppins_SemiBold.withSize(22))
            print_debug(attributedString)
            self.durationLabel.attributedText = attributedString
        }
        
    }
}
