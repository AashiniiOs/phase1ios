//
//  CandidateAboutMeCell.swift
//  JobGet
//
//  Created by macOS on 29/05/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class CandidateAboutMeCell: UITableViewCell {
    
    @IBOutlet weak var aboutLabelContainerView: UIView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var aboutMeLabel: UILabel!
    @IBOutlet weak var aboutDescLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.aboutLabelContainerView.backgroundColor = AppColors.themeBlueColor
        self.containerView.setCorner(cornerRadius: 5, clip: false)
        self.containerView.setShadow()
        
        //aboutLabelContainerView.roundCornersFromOneSide([ .topRight], radius: 5)
        self.aboutDescLabel.textColor = AppColors.black46
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    override func draw(_ rect: CGRect) {
        
        super.draw(rect)
        
        aboutLabelContainerView.roundCornersFromOneSide([.topLeft, .topRight], radius: 5)
        
    }
    
}
