//
//  CandidateAbtMeCell.swift
//  JobGet
//
//  Created by macOS on 28/06/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class CandidateAbtMeCell: UITableViewCell {

    @IBOutlet weak var cornerRadiusView: UIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var abtMeLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var abtContainerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.cornerRadiusView.setCorner(cornerRadius: 6, clip: true)
        self.shadowView.setShadow()
        self.abtContainerView.backgroundColor = AppColors.themeBlueColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
