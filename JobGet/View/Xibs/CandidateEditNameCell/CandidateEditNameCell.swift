//
//  CandidateEditNameCell.swift
//  JobGet
//
//  Created by macOS on 04/07/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class CandidateEditNameCell: UITableViewCell {

    
    
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var editLocationButton: UIButton!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var cornerRadiusView: UIView!
    @IBOutlet weak var emailTxtField: SkyFloatingLabelTextField!

    @IBOutlet weak var locationTxtField: SkyFloatingLocationTextField!
    @IBOutlet weak var lastNameTxtField: SkyFloatingLabelTextField!
    @IBOutlet weak var firstNameTxtField: SkyFloatingLabelTextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        
         CommonClass.setSkyFloatingLabelTextField(textField: self.firstNameTxtField, placeholder: StringConstants.FirstName, title: StringConstants.FirstName)
         CommonClass.setSkyFloatingLabelTextField(textField: self.lastNameTxtField, placeholder: StringConstants.LastName, title: StringConstants.LastName)
          CommonClass.setSkyFloatingLabelTextField(textField: self.emailTxtField, placeholder: StringConstants.Email_Address.localized, title: StringConstants.Email_Address.localized)
         CommonClass.setSkyFloatingLocationTextField(textField: self.locationTxtField, placeholder: StringConstants.location, title: StringConstants.location)
        
        
        
        self.firstNameTxtField.autocapitalizationType = .sentences
        self.lastNameTxtField.autocapitalizationType = .sentences
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.shadowView.setCorner(cornerRadius: 5.0, clip: true)
        self.cornerRadiusView.setCorner(cornerRadius: 5.0, clip: true)
        self.shadowView.dropShadow(color: AppColors.gray119, opacity:0.2, offSet: CGSize.zero, radius: 0.8)
        self.shadowView.clipsToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
