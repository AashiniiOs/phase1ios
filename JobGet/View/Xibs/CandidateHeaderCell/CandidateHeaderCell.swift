//
//  CandidateHeaderCell.swift
//  JobGet
//
//  Created by macOS on 28/06/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class CandidateHeaderCell: UITableViewCell {

    //MARK: IBOUTLETS
    @IBOutlet weak var uperLabelContainerView: UIView!
    @IBOutlet weak var lowerLabel: UILabel!
    @IBOutlet weak var upperLabel: UILabel!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var cornerrRadiusView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.uperLabelContainerView.backgroundColor = AppColors.themeBlueColor
    }
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.uperLabelContainerView.roundCornersFromOneSide([.topLeft, .topRight], radius: 5.0)
        self.cornerrRadiusView.setShadow()
        self.shadowView.setCorner(cornerRadius: 5.0, clip: true)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
