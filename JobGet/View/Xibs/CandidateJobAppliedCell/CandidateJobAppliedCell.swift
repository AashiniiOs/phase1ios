//
//  CandidateJobAppliedCell.swift
//  JobGet
//
//  Created by macOS on 28/05/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class CandidateJobAppliedCell: UITableViewCell {
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var positionContainerView: UIView!
    @IBOutlet weak var companyNameLabel: UILabel!

    @IBOutlet weak var addViewWidth: NSLayoutConstraint!
    @IBOutlet weak var addViewTrailingConst: NSLayoutConstraint!
    @IBOutlet weak var adView: UIView!
    @IBOutlet weak var notSelectedLabel: UILabel!
    
    @IBOutlet weak var jobSeenCountButton: UIButton!
    @IBOutlet weak var jobPostTimeLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var jobImage: UIImageView!
    @IBOutlet weak var jobtitleLabel: UILabel!
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var experienceLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var distanceButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var jobAddressLabel: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    
    var timer = Timer()
    var timerIsOn = false
    var totalTimeInSec = 59
    var countdown = Timer()
    
    var isOpenFromApplied = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.adView.isHidden = true
        self.addViewWidth.constant = 0
        self.addViewTrailingConst.constant = 0
        self.descLabel.numberOfLines = 3
        self.descLabel.adjustsFontSizeToFitWidth = false;
        self.descLabel.lineBreakMode = NSLineBreakMode.byTruncatingTail
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.jobSeenCountButton.setTitleColor(AppColors.gray152, for: .normal)
        self.jobPostTimeLabel.textColor = AppColors.gray152
        self.containerView.setShadow(offset: CGSize(width: 0.0, height: 2.0))
        self.containerView.setCorner(cornerRadius: 5.0, clip: false)
        self.jobImage.setCorner(cornerRadius: 5.0, clip: true)
        self.distanceButton.setTitleColor(AppColors.black46, for: .normal)
        self.shareButton.setTitleColor(AppColors.black46, for: .normal)
        self.positionContainerView.roundCorners()
        jobSeenCountButton.isHidden = true
        //self.applyButton.roundCornersFromOneSide(.bottomRight, radius: 5.0)
    }
    
    
    func populateCandidateJobDetail(_ data: CandidateJobList) {
        
        self.jobtitleLabel.text         = data.jobTitle
        self.positionLabel.text         = data.categoryName
        self.companyNameLabel.text      = data.companyName
        self.descLabel.text             = ""
        var address = CommonClass.getAbbrevitedState(state: data.jobState)
        if address.isEmpty {
            address.append(data.jobState)
        }
        
        address.isEmpty ? self.jobAddressLabel.text = data.jobCity :  (self.jobAddressLabel.text = "\(data.jobCity), \(address)")
        
        if data.isPremium{
            self.adView.isHidden = false
            self.addViewWidth.constant = 26
            self.addViewTrailingConst.constant = 4
        }else{
            self.adView.isHidden = true
            self.addViewWidth.constant = 0
            self.addViewTrailingConst.constant = 0
        }
        if data.totalExperience.isEmpty || data.isExp == 0 || data.experienceType == 0{
            self.experienceLabel.text       = "\(StringConstants.NoExpRequired)"
        } else {
            if data.experienceType == 1 {
                if data.totalExperience == "1"{
                    self.experienceLabel.text       = "\(data.totalExperience) \(StringConstants.month_of_experience)"
                }else{
                    self.experienceLabel.text       = "\(data.totalExperience) \(StringConstants.months_of_experience)"
                }
                
            } else {
                if data.totalExperience == "1"{
                    self.experienceLabel.text       = "\(data.totalExperience) \(StringConstants.year_of_experience)"
                }else{
                    self.experienceLabel.text       = "\(data.totalExperience) \(StringConstants.years_of_experience)"
                }
            }
        }
        self.jobSeenCountButton.setImage(#imageLiteral(resourceName: "icHomeEye"), for: .normal)
        self.shareButton.setImage(#imageLiteral(resourceName: "icHomeShare"), for: .normal)
        self.jobSeenCountButton.setTitle("\(data.totalJobView)", for: .normal)
        self.distanceButton.setTitle(" \(CommonClass.calculateDistanceBtw(from: data.latitude, long: data.longitude)) \(StringConstants.Miles_Away)", for: .normal)
        
        
        self.jobImage.sd_setImage(with: URL(string: data.jobImage),  placeholderImage: #imageLiteral(resourceName: "icHomeCategoryPlaceholder1"), completed: nil)
        
        var time = ""
        if let timeDate = data.createdAt {
            time = timeDate.elapsedTime
        }
        
        self.jobPostTimeLabel.text = time
        
        
        if let expiresTime = Double(data.jobExpiredTime) {
            
            let timeInSec = expiresTime / 1000
            let expiryDate = Date(timeIntervalSince1970: timeInSec).secondsFrom(Date())
            self.totalTimeInSec = expiryDate
            
            if self.isOpenFromApplied {
                if data.applyStatus == 3 || data.applyStatus == 4 {
                    self.notSelectedLabel.isHidden = false
                    self.notSelectedLabel.text = StringConstants.Not_Selected
                    self.timerLabel.isHidden = true
                   // stopTimer()
                } else {
                    self.notSelectedLabel.isHidden = true
                    self.timerLabel.isHidden = true
                   // self.startTimer()
                }
            }
        }
    }
    
    func startTimer() {
        if !self.countdown.isValid{
            countdown = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        }
    }
    
    @objc func updateTime() {
        
        self.timerLabel.text = "\(StringConstants.Hear_back_in) \(timeFormatted(totalTimeInSec))"
        if totalTimeInSec != 0 {
            totalTimeInSec -= 1
        } else {
            stopTimer()
        }
    }
    
    func stopTimer() {
        countdown.invalidate()
        
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        
        //  let sec: Int = totalSeconds % 60
        let min: Int = (totalSeconds / 60) % 60
        let hours: Int = totalSeconds / 3600
        
        
        return String(format: "%02d:%02d", hours, min)
    }
}

