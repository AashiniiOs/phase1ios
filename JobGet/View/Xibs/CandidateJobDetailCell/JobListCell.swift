//
//  JobListCell.swift
//  JobGet
//
//  Created by Manish Nainwal on 11/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class JobListCell: UITableViewCell {
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var addViewWidth: NSLayoutConstraint!
    @IBOutlet weak var addViewTrailingConst: NSLayoutConstraint!
    @IBOutlet weak var adView: UIView!
    @IBOutlet weak var jobSeenCountButton: UIButton!
    @IBOutlet weak var jobPostTimeButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var jobImage: UIImageView!
    @IBOutlet weak var jobtitleLabel: UILabel!
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var experienceLabel: UILabel!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var distanceButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var jobAddressLabel: UILabel!
    @IBOutlet weak var positionContainerView: UIView!
    @IBOutlet weak var applyButton: UIButton!
    
    //MARK:- LifeCycle Methods
    //========================
    override func awakeFromNib() {
        super.awakeFromNib()
        self.adView.isHidden = true
        self.addViewWidth.constant = 0
         self.addViewTrailingConst.constant = 0
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        self.descLabel.numberOfLines = 3
        self.descLabel.adjustsFontSizeToFitWidth = false
        self.descLabel.lineBreakMode = NSLineBreakMode.byTruncatingTail
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.jobSeenCountButton.setTitleColor(AppColors.gray152, for: .normal)
        self.jobPostTimeButton.setTitleColor(AppColors.gray152, for: .normal)
        self.containerView.setShadow(offset: CGSize(width: 0.0, height: 2.0))
        self.containerView.setCorner(cornerRadius: 5.0, clip: false)
        self.jobImage.setCorner(cornerRadius: 5.0, clip: true)
        self.distanceButton.setTitleColor(AppColors.black46, for: .normal)
        self.shareButton.setTitleColor(AppColors.black46, for: .normal)
        self.applyButton.setCorner(cornerRadius: 5.0, clip: true)
        self.applyButton.borderColor = AppColors.themeBlueColor
        self.applyButton.borderWidth = 1.0
        self.positionLabel.textColor = AppColors.gray152
        self.positionContainerView.roundCorners()
        
    }
    
    func populateCandidateJobDetail(_ data: CandidateJobList) {
        
        
        self.jobtitleLabel.text         = data.jobTitle
        self.positionLabel.text         = data.categoryName
        
        self.companyNameLabel.text   = "\(data.companyName)"
        
        //        self.descLabel.numberOfLines = 3
        //        self.descLabel.lineBreakMode = .byWordWrapping
        //
        //        if data.jobDescription.count > 74{
        //            self.descLabel.attributedText = self.addSeeMore(str:  data.jobDescription, maxLength: 75)
        //        }else{
        
        //        }
        
        self.descLabel.text = data.jobDescription
        if data.isPremium{
            self.adView.isHidden = false
            self.addViewWidth.constant = 26
            self.addViewTrailingConst.constant = 4
        }else{
            self.adView.isHidden = true
            self.addViewWidth.constant = 0
            self.addViewTrailingConst.constant = 0
            
        }
        if data.isShortlisted{
            
            self.applyButton.setTitleColor(AppColors.themeBlueColor, for: .normal)
            self.applyButton.backgroundColor = AppColors.white
            self.applyButton.isEnabled = false
            self.applyButton.setTitle(StringConstants.Shortlisted.uppercased(), for: .normal)
        }else if !data.isShortlisted && data.isApplied == 1{

            self.applyButton.setTitleColor(AppColors.themeBlueColor, for: .normal)
            self.applyButton.backgroundColor = AppColors.white
            self.applyButton.isEnabled = false
            self.applyButton.setTitle(StringConstants.Applied.uppercased(), for: .normal)
        }
        else {
            self.applyButton.setTitle(StringConstants.Apply.uppercased(), for: .normal)
            self.applyButton.setTitleColor(AppColors.white, for: .normal)
            self.applyButton.isEnabled = true
            self.applyButton.backgroundColor = AppColors.themeBlueColor
            
        }
        
        var address = ""
        
        address.append(data.jobCity)
        
        getAbbreatedState { (stateList) in
            
            _ = stateList.map{ value in                
                if data.jobState.contains(s: value["state_name"] as? String ?? ""){
                    if let abState = value["state_abbreviations"] {
                        address.append(", ")
                        address.append(String(describing: abState))
                    }
                }
            }
        }
        if !address.contains(",") {
            address.append(", ")
            address.append(data.jobState)
        }
        self.jobAddressLabel.text = address
        if data.totalExperience.isEmpty || data.isExp == 0 || data.experienceType == 0{
            self.experienceLabel.text       = "\(StringConstants.NoExpRequired)"
        } else {
            if data.experienceType == 1 {
                if data.totalExperience == "1"{
                    self.experienceLabel.text       = "\(StringConstants.upTo) \(data.totalExperience) \(StringConstants.month_of_experience)"
                }else{
                    self.experienceLabel.text       = "\(StringConstants.upTo) \(data.totalExperience) \(StringConstants.months_of_experience)"
                }
            } else {
                if data.totalExperience == "1"{
                    self.experienceLabel.text       = "\(StringConstants.upTo) \(data.totalExperience) \(StringConstants.year_of_experience)"
                }else{
                    self.experienceLabel.text       = "\(StringConstants.upTo) \(data.totalExperience) \(StringConstants.years_of_experience)"
                }
            }
        }
//        self.jobSeenCountButton.setTitle("\(data.totalJobView)", for: .normal)
     self.jobSeenCountButton.isHidden = true
    self.distanceButton.setTitle(" \(CommonClass.calculateDistanceBtw(from: data.latitude, long: data.longitude)) \(StringConstants.Miles_Away)", for: .normal)
        self.jobImage.sd_setImage(with: URL(string: data.jobImage),  placeholderImage: #imageLiteral(resourceName: "icHomeCategoryPlaceholder1"), completed: nil)
        var time = ""
        if let timeDate = data.createdAt {
            time = timeDate.elapsedTime
        }
        self.jobPostTimeButton.setTitle("\(time)", for: .normal)
    }
    
    func addSeeMore(str: String, maxLength: Int) -> NSAttributedString {
        var attributedString = NSAttributedString()
        let index: String.Index = str.index(str.startIndex, offsetBy: maxLength)
        let editedText = String(str.prefix(upTo: index)) + "\(StringConstants.Read_More)"
        attributedString = NSAttributedString(string: editedText)
        return attributedString
    }
    
}

