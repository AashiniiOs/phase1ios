//
//  EduCell.swift
//  JobGet
//
//  Created by macOS on 03/08/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class EduCell: UITableViewCell {

    //MARK:- IBoutlets
    //================
    @IBOutlet weak var educationTextView: TLFloatLabelTextView!
    @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var cornerRadiusView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.shadowView.setShadow()
        self.cornerRadiusView.setCorner(cornerRadius: 5.0, clip: true)
        self.adjustTextViewHeight(textview: self.educationTextView)
    }
    
    func adjustTextViewHeight(textview : UITextView) {
        self.educationTextView.textContainer.maximumNumberOfLines = 5
        self.educationTextView.textContainer.lineBreakMode = .byWordWrapping
        educationTextView.isScrollEnabled = false
    }
}
