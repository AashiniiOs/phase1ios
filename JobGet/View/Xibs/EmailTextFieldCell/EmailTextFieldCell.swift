//
//  EmailTextFieldCell.swift
//  Onboarding
//
//  Created by macOS on 27/03/18.
//  Copyright © 2018 Gurdeep Singh. All rights reserved.
//

import UIKit

class EmailTextFieldCell: UITableViewCell {

    //MARK:- IB Outlets
    //===================
    @IBOutlet weak var emailTextField: FloatingTextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.emailTextField.lineHeight = 0.5 // bottom line height in points
        self.emailTextField.selectedLineHeight = 1.5
        self.emailTextField.lineColor = AppColors.blakopacity15
        self.emailTextField.selectedTitleColor = AppColors.gray152
        self.emailTextField.selectedLineColor = AppColors.themeBlueColor
        self.emailTextField.placeholderFont = AppFonts.Poppins_Regular.withSize(16)
        self.emailTextField.titleFont = AppFonts.Poppins_Regular.withSize(12)
        self.emailTextField.font = AppFonts.Poppins_Medium.withSize(16)
        self.emailTextField.textColor = AppColors.black46
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupTextField(placeholder: String, title: String) {
        
        self.emailTextField.placeholder = placeholder
        self.emailTextField.title = title
    }
    
}
