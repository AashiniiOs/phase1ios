//
//  ExpCell.swift
//  JobGet
//
//  Created by macOS on 28/06/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class ExpCell: UITableViewCell {

    @IBOutlet weak var uperLabelContainerView: UIView!
    @IBOutlet weak var upperLabel: UILabel!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var cornerrRadiusView: UIView!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        
         self.uperLabelContainerView.backgroundColor = AppColors.themeBlueColor
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
