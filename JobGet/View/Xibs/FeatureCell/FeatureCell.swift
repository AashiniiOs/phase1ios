//
//  FeatureCell.swift
//  JobGet
//
//  Created by macOS on 11/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class FeatureCell: UITableViewCell {

    
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var containerViewTopConst: NSLayoutConstraint!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var featureBtn: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        self.bgImageView.setCorner(cornerRadius: 5, clip: true)
       
    }
    
}
