//
//  JobDescCell.swift
//  JobGet
//
//  Created by macOS on 06/07/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class JobDescCell: UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var upperLabel: UILabel!
    @IBOutlet weak var lowerLabel: UILabel!
    @IBOutlet weak var seperatorView: UIView!
    
    //MARK:-
    //MARk:- Properties
    static var identifier : String {
        return String(describing: self)
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.upperLabel.textColor = AppColors.gray152
        self.lowerLabel.textColor = AppColors.black46
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    func populateJobDescrption(data: CandidateJobDetail){
        self.upperLabel.text  = StringConstants.JobDescription
        self.lowerLabel.text          = data.jobDescription
    }
    
    func populateDescripton(data: JobPostModel) {
        self.upperLabel.text  = StringConstants.JobDescription
        self.lowerLabel.text          = data.jobDesc
    }
    
    func populate(with data: CandidateJobDetail) {
        self.upperLabel.text = StringConstants.ExperienceRequired.replace(string: "?", withString: "")
        
        if data.totalExperience.isEmpty || data.totalExperience == "0" || data.isExp == 0 || data.experienceType == 0{
            self.lowerLabel.text       = StringConstants.NoExpRequired
        } else if data.experienceType == 1 {
            self.lowerLabel.text       = "\(StringConstants.upTo) \(data.totalExperience) \(StringConstants.Months)"
        } else if data.experienceType == 2 {
            self.lowerLabel.text     = "\(StringConstants.upTo) \(data.totalExperience) \(StringConstants.Years)"
        }
        self.seperatorView.isHidden = true
    }
    
}
