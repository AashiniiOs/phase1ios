//
//  JobDetailCell.swift
//  JobGet
//
//  Created by macOS on 10/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import GoogleMaps

class JobDetailCell: UITableViewCell {
    
    @IBOutlet weak var locationViewTrailing: NSLayoutConstraint!
    
    @IBOutlet weak var featuredStatusLabel: UILabel!
    @IBOutlet weak var chatBtnWidth: NSLayoutConstraint!
    @IBOutlet weak var locationBtnTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var locationBtn: UIButton!
    @IBOutlet weak var bookMarkBtn: UIButton!
    @IBOutlet weak var jobCategoryContainerView: UIView!
    @IBOutlet weak var jobSeenCountButton: UIButton!
    @IBOutlet weak var chatButton: UIButton!
    @IBOutlet weak var chatContainerView: UIView!
    @IBOutlet weak var jobDistanceLabel: UILabel!
    @IBOutlet weak var jobProImageContainerView: UIView!
    @IBOutlet weak var jobProImageView: UIImageView!
    @IBOutlet weak var jobNameLabel: UILabel!
    @IBOutlet weak var jobCategoryLabel: UILabel!
    @IBOutlet weak var sepeatorView: UIView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var timeButton: UIButton!
    @IBOutlet weak var fullTimeContainerView: UIView!
    @IBOutlet weak var fullTimeLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var yearContainerView: UIView!
    @IBOutlet weak var timeButtonTopConstrant: NSLayoutConstraint!
    @IBOutlet weak var locationImageView: UIImageView!
    @IBOutlet weak var spotLightView: UIView!
    @IBOutlet weak var spotLightViewLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.featuredStatusLabel.isHidden = true
        self.chatContainerView.isHidden = true
        self.chatButton.isHidden = true
        self.fullTimeContainerView.isHidden = true
        self.yearContainerView.isHidden = true
        self.locationLabel.text = ""
        self.chatButton.isHidden = true
        self.spotLightView.isHidden = true
        self.jobDistanceLabel.isHidden = true
        self.jobNameLabel.textColor = AppColors.black26
        self.chatButton.setTitleColor(AppColors.themeBlueColor, for: .normal)
        
        self.bookMarkBtn.setImage(#imageLiteral(resourceName: "icHomeBookmarkHeart"), for: .normal)
        self.bookMarkBtn.setImage(#imageLiteral(resourceName: "icHomeSaveBookmarkHeart"), for: .selected)
        self.yearContainerView.backgroundColor = #colorLiteral(red: 0.5960784314, green: 0.5960784314, blue: 0.5960784314, alpha: 0.2)
        self.yearLabel.textColor = AppColors.black46
        self.jobCategoryLabel.textColor = AppColors.gray152
        self.fullTimeLabel.textColor = AppColors.black46
        self.timeButton.setTitleColor(AppColors.black46, for: .normal)
        self.locationLabel.textColor = AppColors.black46
        self.jobSeenCountButton.setTitleColor(AppColors.black46, for: .normal)
        self.jobDistanceLabel.textColor = AppColors.themeBlueColor
        
        jobSeenCountButton.isHidden = true
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
    }
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.jobProImageView.roundCorners()
        self.jobProImageContainerView.roundCorners()
        self.chatContainerView.roundCorners()
        self.chatButton.roundCorners()
        self.chatContainerView.roundCorners()
        self.fullTimeContainerView.setCorner(cornerRadius: self.fullTimeContainerView.height/2, clip: true)
        self.yearContainerView.setCorner(cornerRadius: self.yearContainerView.height/2, clip: true)
        self.bookMarkBtn.roundCorners()
    }
    
    func populateData(detail: MyJobListModel) {
        self.fullTimeContainerView.backgroundColor = #colorLiteral(red: 0.5960784314, green: 0.5960784314, blue: 0.5960784314, alpha: 0.2)
        self.bookMarkBtn.isHidden = true
        
        self.fullTimeContainerView.isHidden = false
        if detail.deleteStatus == 3 {
            let font = self.jobNameLabel.font
            let fontSize = self.jobNameLabel.font.pointSize
            
            let _ = "\(detail.jobTitle) \(StringConstants.Job_Closed)"
            let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: "\(detail.jobTitle) \(StringConstants.Job_Closed)")
            attributedString.setColorForText(textForAttribute: StringConstants.Job_Closed, withColor: AppColors.gray152, font: font!)
            
            self.jobNameLabel.attributedText = attributedString
        } else {
            self.jobNameLabel.text = detail.jobTitle
        }
        
        self.jobCategoryLabel.text = detail.categoryName
        
        self.chatBtnWidth.constant = 0
        self.chatContainerView.isHidden = true
        self.chatButton.isHidden = true
        var time = ""
        if let timeDate = detail.createdAt {
            time = timeDate.elapsedTime
        }
        
        let times = time.replacingOccurrences(of: StringConstants.Ago.lowercased(), with: StringConstants.Ago)
        self.timeButton.setTitle(times, for: .normal)
        self.fullTimeContainerView.isHidden = false
        let jobType = CommonClass.getJobType(jobType: detail.jobType)
        self.fullTimeLabel.text = jobType
        
        if AppUserDefaults.value(forKey: .userType).stringValue == UserType.recuriter.rawValue{
          
            self.locationViewTrailing.constant = 40
            if detail.isPremium{
                  self.jobProImageView.image = #imageLiteral(resourceName: "icSpotlightEnable")
                self.featuredStatusLabel.text = StringConstants.Post_featured
                self.locationBtn.isUserInteractionEnabled = true

            }else{
                  self.jobProImageView.image = #imageLiteral(resourceName: "icSpotlightDisable")
                self.featuredStatusLabel.text = StringConstants.Feature_this_post
            }
            if detail.deleteStatus == 3{
                self.featuredStatusLabel.isHidden = true
                self.locationBtn.isUserInteractionEnabled = false
            }else{
                self.featuredStatusLabel.isHidden = false
                 self.locationBtn.isUserInteractionEnabled = true
            }
        }
        
        ////        if detail.planId.isEmpty {
        //            self.jobProImageView.isHidden = true
        //            self.jobProImageContainerView.isHidden = true
        //        } else {
        self.jobProImageView.isHidden = false
        self.jobProImageContainerView.isHidden = false
        //        }
        self.jobSeenCountButton.setTitle(String(detail.totalJobView), for: .normal)
        self.locationLabel.text = detail.jobAddress
        self.reverseGeoCode(lattitude: detail.latitude, longitude: detail.longitude)
        
        var experiance = ""
        
        if detail.totalExperience == StringConstants.NoExpRequired || detail.experienceType == 0 {
            experiance = StringConstants.NoExpRequired
        } else {
            if detail.totalExperience.isEmpty {
                experiance = StringConstants.NoExpRequired
            } else if detail.experienceType == 1 {
                experiance = "\(StringConstants.upTo) \(String(detail.totalExperience)) \(StringConstants.MonthExperience) "
                
            } else if detail.experienceType == 2 {
                experiance = "\(StringConstants.upTo) \(String(detail.totalExperience)) \(StringConstants.YearsExperience) " //String(detail.totalExperience) + " year "
            }
        }
        self.yearContainerView.isHidden = false
        
        self.yearLabel.text = experiance
    }
    
    func reverseGeoCode(lattitude: Double, longitude: Double) {
        let geocoder = GMSGeocoder()
        let coordinate = CLLocationCoordinate2DMake(Double(lattitude), Double(longitude))
        
        
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            if let location = response?.firstResult() {
                
                print_debug(response)
                print_debug(location)
                
            }
        }
    }
    
    func populateCandiateJobDetail(data: CandidateJobDetail) {
        self.fullTimeContainerView.backgroundColor = #colorLiteral(red: 0.5960784314, green: 0.5960784314, blue: 0.5960784314, alpha: 0.2)
        self.jobNameLabel.text = data.jobTitle
        self.jobCategoryLabel.text = data.categoryName
        
        let jobType = CommonClass.getJobType(jobType: data.jobType)
        self.fullTimeContainerView.isHidden = false
        self.fullTimeLabel.text = jobType
        var time = ""
        if let timeDate = data.createdAt {
            time = timeDate.elapsedTime
        }
        
        let times = time.replacingOccurrences(of: StringConstants.Ago.lowercased(), with: StringConstants.Ago)
        self.timeButton.setTitle(times, for: .normal)
        self.locationLabel.text = data.jobLocation
        self.jobSeenCountButton.setTitle(String(data.totalJobView), for: .normal)
        
        self.checkBookmarkButton(isSaved: data.isSaved)
        
    }
    
    func checkBookmarkButton(isSaved: Int) {
        self.fullTimeContainerView.backgroundColor = #colorLiteral(red: 0.5960784314, green: 0.5960784314, blue: 0.5960784314, alpha: 0.2)
        if isSaved == 0 {
            self.bookMarkBtn.isSelected = false
        } else {
            self.bookMarkBtn.isSelected = true
        }
    }
    
    func populatePostJobDetail(data: JobPostModel) {
        
        self.fullTimeContainerView.backgroundColor = #colorLiteral(red: 0.5960784314, green: 0.5960784314, blue: 0.5960784314, alpha: 0.2)
        self.jobNameLabel.text = data.jobTitle
        self.jobCategoryLabel.text = data.categoryName
        self.fullTimeContainerView.isHidden = false
        let jobType = CommonClass.getJobType(jobType: data.jobType)
        self.fullTimeLabel.text = jobType
        self.locationLabel.text = data.address
    }
}

