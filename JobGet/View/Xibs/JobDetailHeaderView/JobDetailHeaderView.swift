//
//  JobDetailHeaderView.swift
//  JobGet
//
//  Created by macOS on 10/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class JobDetailHeaderView: UIView {

  
    @IBOutlet weak var containerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var jobNameLabel: UILabel!
    @IBOutlet weak var jobImageView: UIImageView!
    @IBOutlet weak var jobNameContainerView: UIView!
    
    //MARK:- Life Cycle Methods
    //=========================
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        loadNib()
        setupViews()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        loadNib()
        setupViews()
    }
}
//MARK:- EXTENSION PRIVATE METHODS
//MARK:-
extension JobDetailHeaderView{
    
    private func loadNib() {
       
        let view = self.instanceFromNib()
        view.frame = bounds
        self.addSubview(view)
        view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    private func instanceFromNib() -> UIView {
        // remove class from identity inspector and set class in file owner
        return UINib(nibName: "JobDetailHeaderView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
    }
    
    //MARK:- Helper Methods
    //======================
    private func setupViews() {
 
    }
    
}
    

