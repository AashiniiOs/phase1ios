//
//  MapInfoWindowView.swift
//  JobGet
//
//  Created by macOS on 03/05/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
//import GSPO
class MapInfoWindowView: UIView {
   
   
    @IBOutlet weak var bgView: GSPopOverView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
    //MARK:- Life Cycle Methods
    //=========================
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
       // loadNib()
        setupViews()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
       // loadNib()
        setupViews()
    }
}
    
//MARK:- EXTENSION PRIVATE METHODS
//MARK:-
extension MapInfoWindowView{
    
//    private func loadNib() {
//
//      //  let view = self.instanceFromNib()
//        view.frame = bounds
//        self.addSubview(view)
//        view.autoresizingMask = [.flexibleHeight, .flexibleWidth]
//    }
//
//    private func instanceFromNib() -> UIView {
//        // remove class from identity inspector and set class in file owner
//        return UINib(nibName: "MapInfoWindowView", bundle: nil).instantiate(withOwner: self, options: nil)[0] as! UIView
//    }
    
//    class func instanceFromNib() -> UIView {
//        return UINib(nibName: "MapInfoWindowView", bundle: nil).instantiate(withOwner: self, options: nil).first as! UIView
//        
//    }
    
    //MARK:- Helper Methods
    //======================
    private func setupViews() {

        
    }
    
}

