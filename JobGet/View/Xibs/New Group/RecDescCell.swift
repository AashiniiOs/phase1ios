//
//  RecDescCell.swift
//  JobGet
//
//  Created by macOS on 13/06/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class RecDescCell: UITableViewCell {

   
    @IBOutlet weak var descriptionLeadingConstraints: NSLayoutConstraint!
    @IBOutlet weak var upperLabel: UILabel!
    @IBOutlet weak var lowerLabel: UILabel!
    @IBOutlet weak var seperatorView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.upperLabel.textColor = AppColors.gray152
        self.lowerLabel.textColor = AppColors.black46
        self.seperatorView.isHidden = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setJobDescData(jobDetail:MyJobListModel) {
       
        self.upperLabel.text  = StringConstants.JobDescription
        self.lowerLabel.text    = jobDetail.jobDescription
        self.seperatorView.isHidden = false
        
    }
    
}
