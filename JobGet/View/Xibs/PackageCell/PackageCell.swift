//
//  PackageCell.swift
//  JobGet
//
//  Created by macOS on 10/07/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class PackageCell: UITableViewCell {

    //MARK: - Properties
    //MARK:-
    static var identifier : String {
        return String(describing: self)
        
    }
   
   
    //MARK: - IBOutlets
    //====================
    @IBOutlet weak var validityLabel: UILabel!
    @IBOutlet weak var starCountLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var costLabel: UILabel!
    @IBOutlet weak var packageNameLabel: UILabel!
    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var labelArrow: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.containerView.dropShadow(color: AppColors.black26, opacity:0.2, offSet: CGSize(width: 1.0, height: 1.0), radius: 2.0)
    }
    
    func populateStar(data: FeaturedPostModel, index: Int) {
       
        switch index {
        case 0:
            self.costLabel.text = "$\(String(data.planAmount)) each star"
        case 1:
            self.costLabel.text = "$\(String(data.planAmount)) ($1.99 each star)"
        case 2:
            self.costLabel.text = "$\(String(data.planAmount)) ($1.49 each star)"
        default:
            self.costLabel.text = "$\(String(data.planAmount)) ($0.99 each star)"

        }
        
        self.detailLabel.text = data.description
        if data.stars == "1" {
            self.starCountLabel.text = "+\(data.stars) \(StringConstants.Star)"
        }else{
            self.starCountLabel.text = "+\(data.stars) \(StringConstants.Stars)"
        }
         self.packageNameLabel.text = data.planName
        if index == 3{
            self.validityLabel.text = "most popular"
            labelArrow.isHidden = false
        }else{
            self.validityLabel.text = ""
            labelArrow.isHidden = true
//             self.packageNameLabel.text = data.planName
        }
        
    }
}
