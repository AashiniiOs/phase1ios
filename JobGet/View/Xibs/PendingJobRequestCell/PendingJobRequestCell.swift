class PendingJobRequestCell: UITableViewCell {
    
    //MARK: - Properties
    //======================
    var timer = Timer()
    var timerIsOn = false
    var totalTimeInSec = 59
    var countdown =  Timer()
    var categoryId = ""
    var pulsatingLayer : CAShapeLayer!
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var chatWidthConstraint: NSLayoutConstraint!
    // @IBOutlet weak var videoContaineerView: UIView!
    @IBOutlet weak var shadowView: UIView!
    // @IBOutlet weak var buttonsSeperatorView: UIView!
    // @IBOutlet weak var messageContainerView: UIView!
    @IBOutlet weak var rejectButton: UIButton!
    @IBOutlet weak var shortlistedGreenTick: UIImageView!
    @IBOutlet weak var expiredImageView : UIImageView!
    @IBOutlet weak var containerView    : UIView!
    @IBOutlet weak var imageContaiinerView: UIView!
    @IBOutlet weak var appplicantsImageView: UIImageView!
    @IBOutlet weak var timerButton: UIButton!
    @IBOutlet weak var statusButton: UIButton!
    @IBOutlet weak var totalExperienceLabel: UILabel!
    @IBOutlet weak var pendingTimeLabel: UILabel!
    @IBOutlet weak var experianceLabel: UILabel!
    @IBOutlet weak var jobPostLabel: UILabel!
    @IBOutlet weak var applicantsNameLabel: UILabel!
    //  @IBOutlet weak var messageButton: UIButton!
    //  @IBOutlet weak var videoCallButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectionStyle = .none
        self.applicantsNameLabel.textColor = AppColors.black26
        self.jobPostLabel.textColor = AppColors.gray152
        self.experianceLabel.textColor = AppColors.black46
        self.pendingTimeLabel.textColor = AppColors.gray152
        self.statusLabel.textColor = AppColors.themeBlueColor
        self.timerButton.setTitleColor(AppColors.themeBlueColor, for: .normal)
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
    }
    
    func setHighlitCell() {
        self.pulsatingLayer = CAShapeLayer()
        pulsatingLayer.strokeColor = AppColors.gray163.cgColor
        pulsatingLayer.fillColor = UIColor.clear.cgColor
        pulsatingLayer.lineCap = kCALineCapRound
        pulsatingLayer.position = self.center
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.imageContaiinerView.borderColor = #colorLiteral(red: 0.2745098039, green: 0.5294117647, blue: 0.8196078431, alpha: 0.2725652825)
        self.imageContaiinerView.borderWidth = 2.0
        self.imageContaiinerView.setCircleShadow()
        self.statusButton.roundCorners()
        self.appplicantsImageView.roundCorners()
        self.expiredImageView.roundCorners()
        self.containerView.setCorner(cornerRadius: 5, clip: true)
        self.shadowView.setShadow()
        self.statusButton.borderColor = AppColors.themeBlueColor
        self.statusButton.setTitleColor(AppColors.themeBlueColor, for: .normal)
        self.statusButton.borderWidth = 2.0
        
        self.rejectButton.setCorner(cornerRadius: self.rejectButton.frame.height/2, clip: true)
        
        
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        
    }
    
    func populateData(data: PendingJobDetail,_ isShortlisted : Bool = false) {
        self.appplicantsImageView.sd_setImage(with: URL(string: data.userData.userImage),  placeholderImage: #imageLiteral(resourceName: "ic_user_"), completed: nil)
        if let firstChar = data.userData.secondName.first {
            self.applicantsNameLabel.text = "\(String(data.userData.firstName)) \(firstChar)."
        }
        
        
        var exp: Float = 0
        var totalMonth : Float = 0
        var categoryTitle = ""
        var experience = ""
        var experienceDuration = ""
        
        
        let _ =  data.experience.map { (experence)  in
            print_debug(experence.id)
            print_debug(self.categoryId)
            if experence.id == self.categoryId {
                if experence.duration == "5+" {
                    exp += 12 * 5
                    categoryTitle = experence.categoryName
                } else {
                    
                    if experence.durationType == 1 {
                        exp += Float(experence.duration) ?? 0
                        categoryTitle = experence.categoryName
                        
                    } else if experence.durationType == 2 {
                        if experence.duration.contains(s: ".5") {
                            exp += (Float(experence.duration.components(separatedBy: ".").first ?? "") ?? 0)*12
                            exp += 6 // add 6 month extra
                            categoryTitle = experence.categoryName
                        } else {
                            exp += (12 * (Float(experence.duration) ?? 0))
                            categoryTitle = experence.categoryName
                        }
                    }
                }
            }
        }
        if !data.experience.isEmpty {
            
            if exp == 0 {
                self.experianceLabel.text = "\(StringConstants.NoRelevantExperience)"
            } else if exp <= 11 {
                var month = String(format: "%.1f", exp)
                if month.contains(s: ".0"){
                    month = month.replace(string: ".0", withString: "")
                }
                experience = "\(month) \(StringConstants.Months) in \(categoryTitle)"
                experienceDuration = "\(month) \(StringConstants.Months)"
            } else if exp > 11 {
                
                var year = String(format: "%.1f", Float(exp)/12)
                if year.contains(s: ".0"){
                    year = year.replace(string: ".0", withString: "")
                }
                experience = "\(year) \(StringConstants.Years) in \(categoryTitle)"
                experienceDuration = "\(year) \(StringConstants.Years)"
            }
            
            let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: experience)
            attributedString.setColorForText(textForAttribute: experienceDuration, withColor: AppColors.themeBlueColor, font: AppFonts.Poppins_SemiBold.withSize(14))
            self.experianceLabel.attributedText = attributedString
            let _ = data.experience.map { (value) -> Void in
                if value.durationType == 1 {
                    totalMonth += Float(value.duration) ?? 0
                } else {
                    if value.duration == "5+" {
                        totalMonth += 12*5
                    } else {
                        totalMonth += ((Float(value.duration) ?? 0 ) * 12)
                    }
                }
            }
            if totalMonth <= 11 {
                var month = String(format: "%.1f", totalMonth)
                if month.contains(s: ".0"){
                    month = month.replace(string: ".0", withString: "")
                }
                self.totalExperienceLabel.text = "\(month) \(StringConstants.months_of_total_experience)"
            } else {
                var year = String(format: "%.1f", Float(totalMonth)/12)
                
                if year == "0.0" {
                    self.totalExperienceLabel.text = ""
                } else {
                    if year.contains(s: ".0"){
                        year = year.replace(string: ".0", withString: "")
                    }
                    self.totalExperienceLabel.text = "\(year) \(StringConstants.years_of_total_experience)"
                }
            }
            
        }else{
            
            self.totalExperienceLabel.text = "\(StringConstants.NoRelevantExperience)"
        }
        
        if let expiresTime = Double(data.jobExpiredTime) {
            let timeInSec = expiresTime / 1000
            let expiryDate = Date(timeIntervalSince1970: timeInSec).secondsFrom(Date())
            self.totalTimeInSec = expiryDate
            self.startTimer()
        }
        var time = ""
        if isShortlisted{
            if let fetchedTime = data.jobShortistedAt{
                time = fetchedTime.elapsedTime
            }
        }else{
            if let timeDate = data.jobAppliedAt {
                time = timeDate.elapsedTime
            }
        }
        self.pendingTimeLabel.text = time
    }
}


extension PendingJobRequestCell {
    
    func startTimer() {
        if !countdown.isValid{
            countdown = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTime), userInfo: nil, repeats: true)
        }
    }
    
    @objc func updateTime() {
        //  timerLabelOutlet.text = "\(timeFormatted(totalTimeInSec))"
        self.timerButton.setTitle("\(timeFormatted(totalTimeInSec))", for: .normal)
        if totalTimeInSec != 0 {
            totalTimeInSec -= 1
        } else {
            stopTimer()
        }
    }
    
    func stopTimer() {
        countdown.invalidate()
        
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        
        let sec: Int = totalSeconds % 60
        let min: Int = (totalSeconds / 60) % 60
        let hours: Int = totalSeconds / 3600
        
        return String(format: "%02d:%02d:%02d", hours, min, sec)
    }
    
}

