//
//  ProfileExperienceCell.swift
//  JobGet
//
//  Created by Appinventiv on 04/06/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import CircularSlider

class ProfileExperienceCell: UITableViewCell {
    
    @IBOutlet weak var categoryExperienceLabel: UILabel!
    @IBOutlet weak var categoryContainerView: UIView!
    @IBOutlet weak var minusBtnContainerView: UIView!
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var sliderView: CircularSlider!
    @IBOutlet weak var cellBackgroundView: UIView!
    @IBOutlet weak var minusButton: UIButton!
    // @IBOutlet weak var jobTypeLabel: UILabel!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        self.minusBtnContainerView.backgroundColor = AppColors.themeBlueColor
        self.minusBtnContainerView.roundCorners()
        self.sliderView.isUserInteractionEnabled = false
        categoryContainerView.roundCorners()
        categoryContainerView.backgroundColor = AppColors.gray15
        self.sliderView.pgHighlightedColor = AppColors.themeBlueColor
        
    }
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.cellBackgroundView.layer.cornerRadius = 4
        self.cellBackgroundView.setShadow()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func populatedCell(data: CandidateProfileModel, index: Int){
        
        self.companyNameLabel.text = data.experience[index].companyName
        self.positionLabel.text = data.experience[index].position
        
        var year = ""
        _ = (data.experience[index].duration)
        if data.experience[index].durationType == 1{
            if data.experience[index].duration == "0" || data.experience[index].duration == "1"{
                year = "\(StringConstants.Month)"
            } else {
                year = "\(StringConstants.Months)"
            }
            
        }else{
            if data.experience[index].duration == "1" {
                year = "\(StringConstants.Year)"
            } else {
                year = "\(StringConstants.Years)"
            }
        }
        
        // self.sliderView.maximumValue = 360
        
        
        self.categoryExperienceLabel.text = "\(data.experience[index].categoryName)"
        
        if data.experience[index].duration == "5+" {
            self.sliderView.setValue(400.0, animated: false)
        } else {
            if let experianceValue = Float(data.experience[index].duration.replacingOccurrences(of: "+", with: "")) {
                let sliderValue = CommonClass.getExperienceAngle(experience: experianceValue, experienceType: data.experience[index].durationType, fromTo: 0)
                self.sliderView.setValue(Float(sliderValue), animated: false)
            }
        }
        
        
        
        //  self.durationLabel.text = " \(data.experience[index].duration) \(year)"
        
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: "\(data.experience[index].duration) \n\(year)")
        attributedString.setColorForText(textForAttribute: "\(data.experience[index].duration)", withColor: AppColors.black46, font: AppFonts.Poppins_SemiBold.withSize(22))
        print_debug(attributedString)
        self.durationLabel.attributedText = attributedString
        
    }
}
