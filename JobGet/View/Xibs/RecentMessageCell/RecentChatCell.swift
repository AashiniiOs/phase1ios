//
//  RecentChatCell.swift
//  JobGet
//
//  Created by appinventiv on 16/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class RecentChatCell: UITableViewCell {
    
    //MARK:-
    //MARK:- IBOutlets
    
    @IBOutlet weak var dateTraillingConstr: NSLayoutConstraint!
    @IBOutlet weak var videoCallButton: UIButton!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var dateTimeLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var unreadCountLabel: UILabel!
    @IBOutlet weak var onlineOfflineDotView: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    //MARK:-
    //MARk:- Properties
    private let user = User.getUserModel()
    static var identifier : String {
        return String(describing: self)
        
    }
    
    static var nib : UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    var unreadCount : Int = 0 {
        didSet {
            if unreadCountLabel != nil {
                unreadCountLabel.text = "\(unreadCount)"
                unreadCountLabel.isHidden = (unreadCount <= 0)
//                self.tabBar.items?[1].badgeValue = nil
               
            }
        }
    }
    
    //MARK:-
    //MARK:- View Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        self.videoCallButton.isHidden = true
        setupFontsAndColors()
        unreadCountLabel.text = "2"
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        userImageView.image = nil
        nameLabel.text = ""
        messageLabel.text = ""
        dateTimeLabel.text = ""
        unreadCountLabel.text = ""
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
    
    private func setupFontsAndColors() {
        layoutIfNeeded()
        userImageView.roundCorners()
        // userImageView.borderWidth = 3
        //  userImageView.borderColor = AppColors.gray_0_63_128_10
        
        nameLabel.font = AppFonts.Poppins_Medium.withSize(15)
        nameLabel.textColor = AppColors.black26
        
        dateTimeLabel.font = AppFonts.Poppins_Light.withSize(11)
        dateTimeLabel.textColor = AppColors.gray163
        
        messageLabel.font = AppFonts.Poppins_Light.withSize(13)
        messageLabel.textColor = AppColors.gray163
        
        unreadCountLabel.roundCorners()
        unreadCountLabel.font = AppFonts.Poppins_Medium.withSize(11)
        unreadCountLabel.textColor = AppColors.white
        unreadCountLabel.backgroundColor = AppColors.themeBlueColor
        
        onlineOfflineDotView.roundCorners()
        onlineOfflineDotView.borderWidth = 3
        onlineOfflineDotView.borderColor = AppColors.gray_0_63_128_10
        onlineOfflineDotView.borderWidth = 1
        onlineOfflineDotView.borderColor = AppColors.white
        onlineOfflineDotView.backgroundColor = AppColors.green_66_143_10
    }
    
    func populateCellWith(inbox : Inbox, searchText: String) {
        
        if let imageStr = inbox.chatMember?.userImage {
            activityIndicator.startAnimating()
        userImageView.sd_setImage(with: URL(string: imageStr), placeholderImage: #imageLiteral(resourceName: "ic_user_")) { (img, err,cache, nil) in
            self.activityIndicator.stopAnimating()
          }
        }
        self.unreadCountLabel.isHidden = false
        switch self.user.user_Type {
        case .candidate:
            if let firstName = inbox.chatMember?.firstName, let lastName = inbox.chatMember?.lastName {
                  nameLabel.attributedText = CommonClass.getAttributedText(wholeString:  "\(firstName) \(lastName)", attributedString: searchText, attributedColor: AppColors.appBlue, normalColor: AppColors.black26, fontSize: 15)
//                nameLabel.text = "\(firstName) \(lastName)"
            }
            
        case .recuriter:
            if let firstName = inbox.chatMember?.firstName, let firstChar = inbox.chatMember?.lastName.first {
                 nameLabel.attributedText = CommonClass.getAttributedText(wholeString:  "\(firstName) \(firstChar)", attributedString: searchText, attributedColor: AppColors.appBlue, normalColor: AppColors.black26, fontSize: 15)
//                nameLabel.text = "\(firstName) \(firstChar)."
            }
        case .none:
            return
        }
        
        if let isDeleted = inbox.message?.isDeleted,isDeleted {
            messageLabel.text = StringConstants.This_message_has_been_deleted
        }else if let message = inbox.message, message.type == .call{
            if let _ = Double(message.message){
                messageLabel.text = StringConstants.Call// \(self.timeFormatted(Int(tInt)))"
            }else{
                messageLabel.text = StringConstants.Missed_Call
            }
        }else{
            messageLabel.text = inbox.message?.message
        }
//        self.unreadCountLabel.isHidden = false
        self.onlineOfflineDotView.isHidden = false
        unreadCount = inbox.unreadCount
        if self.unreadCountLabel.isHidden{
            nameLabel.font = AppFonts.Poppins_Medium.withSize(15)
        }else{
            nameLabel.font = AppFonts.Poppins_Bold.withSize(15)
        }
        if let timeStamp = inbox.message?.timestamp {
            dateTimeLabel.text = dateManager.elapsed(from: timeStamp)
        } else {
            dateTimeLabel.text = ""
        }
        
        if let isOnline = inbox.chatMember?.isOnline, isOnline {
            onlineOfflineDotView.backgroundColor = AppColors.green_66_143_10
        } else {
            onlineOfflineDotView.backgroundColor = AppColors.gray152
        }
    }
    
    func popultedCellLocalDB(messageInfo: MessageListDB, searchText: String){
        
        
        userImageView.sd_setImage(with: URL(string: messageInfo.mediaUrl),  placeholderImage: #imageLiteral(resourceName: "ic_user_"), completed: nil)
//        imageFromURl(messageInfo.mediaUrl, placeHolderImage: #imageLiteral(resourceName: "ic_user_"), loader: false, completion: nil)
        
        switch self.user.user_Type {
        case .candidate:
//            if let firstName = messageInfo.firstName, let lastName = inbox.chatMember?.lastName {
            
                nameLabel.text = "\(messageInfo.first_name) \(messageInfo.last_name)"
//            }
            
        case .recuriter:
//            if let firstName = inbox.chatMember?.firstName, let firstChar = inbox.chatMember?.lastName.first {
            
                nameLabel.text = "\(messageInfo.first_name) \(messageInfo.last_name)"
//            }
        case .none:
            return
        }
          messageLabel.attributedText = CommonClass.getAttributedText(wholeString: messageInfo.message, attributedString: searchText, attributedColor: AppColors.appBlue, normalColor: AppColors.gray163, fontSize: 13)
        
//            messageLabel.text = messageInfo.message
//        unreadCount = inbox.unreadCount
        self.unreadCountLabel.isHidden = true
        self.onlineOfflineDotView.isHidden = true
//        if self.unreadCountLabel.isHidden{
//            nameLabel.font = AppFonts.Poppins_Medium.withSize(15)
//        }else{
//            nameLabel.font = AppFonts.Poppins_Bold.withSize(15)
//        }
        if let timeStamp = Double(messageInfo.timestamp) {
            dateTimeLabel.text = dateManager.elapsed(from: timeStamp)
        } else {
            dateTimeLabel.text = ""
        }
//
//        if let isOnline = inbox.chatMember?.isOnline, isOnline {
//            onlineOfflineDotView.backgroundColor = AppColors.green_66_143_10
//        } else {
//            onlineOfflineDotView.backgroundColor = AppColors.gray152
//        }
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        
        let sec: Int = totalSeconds % 60
        let min: Int = (totalSeconds / 60) % 60
        
        return String(format: "%02d:%02d", min, sec)
    }
}

