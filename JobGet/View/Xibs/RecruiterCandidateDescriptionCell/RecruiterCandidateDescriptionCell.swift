//
//  RecruiterCandidateDescriptionCell.swift
//  JobGet
//
//  Created by Appinventiv on 22/05/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class RecruiterCandidateDescriptionCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var bgView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.bgView.setShadow(offset: CGSize(width: 0.0, height: 2.0))
    }
    
}
