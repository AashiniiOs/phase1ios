//
//  RecruiterSearchCell.swift
//  JobGet
//
//  Created by Abhi on 22/05/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class RecruiterSearchCell: UITableViewCell {
    
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var companyName: UILabel!
    @IBOutlet weak var numberOfexperience: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var totalexperience: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var userAddress: UILabel!
    @IBOutlet weak var dayAgoButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    
    var isMultipleCategorySelected = false
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.descriptionLabel.numberOfLines = 3
        self.companyName.text = StringConstants.Worked_at
        self.descriptionLabel.adjustsFontSizeToFitWidth = false;
        self.descriptionLabel.lineBreakMode = NSLineBreakMode.byTruncatingTail
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.profileImage.roundCorners()
        self.profileImage.borderWidth = 3.0
        self.profileImage.borderColor = #colorLiteral(red: 0.9019607843, green: 0.9019607843, blue: 0.9019607843, alpha: 1)
        
        self.bgView.setShadow(offset: CGSize(width: 0.0, height: 2.0))
        self.bgView.setCorner(cornerRadius: 5.0, clip: false)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        profileImage.image = nil
        userName.text = nil
        companyName.text = nil
        numberOfexperience.text = nil
        totalexperience.text = nil
        descriptionLabel.text = nil
        userAddress.text = nil
        dayAgoButton.setTitle(nil, for: .normal)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func populatedCell(index: Int, data: AppliedJobListModel, selectedCategoryId: String){
        
        var address = ""
        if !data.city.isEmpty {
            address = data.city
            
        }
        if (CommonClass.getAbbrevitedState(state: data.state).isEmpty){
            address.append(", \(data.state)")
        }else{
            address.append(", \(CommonClass.getAbbrevitedState(state: data.state))")
        }
        
        self.dayAgoButton.setTitle(address, for: .normal)
        self.profileImage.sd_setImage(with: URL(string: data.userImage), placeholderImage: #imageLiteral(resourceName: "ic_user_"), options: .init(rawValue: 0), completed: nil)
//        if let url = URL(string: data.userImage) {
//            self.profileImage.sd_setImage(with: url )
//
//        } else {
//            self.profileImage.image = #imageLiteral(resourceName: "ic_user_") // placeholder image
//        }
        if let firstChar = data.last_name.first {
            self.userName.text = "\(data.first_name) \(firstChar)."
        }
        
        self.userAddress.text = ""
        self.descriptionLabel.text = data.about
        var time = ""
        if let timeDate = data.applyTime {
            time = timeDate.elapsedTime
        }
        
        var experiance = ""
        var exeperianceDuration = ""
        
        var candidateData : ExperienceModel?
        var sameCategoryExp = [ExperienceModel]()
        var selctedCategoryIDArray = [String]()
        var sameCategoryTotalExperince:Float = 0
        var isSelectedSameCategory = false
        
        //============
        let splitID = selectedCategoryId.split(separator: ",")
        
        //idArray is used to multiple selected category id string.
        let idArray = splitID.map{ value -> String in
            
            return String(value)
        }
        
        if splitID.count > 1{
            self.isMultipleCategorySelected = true
        }else{
            self.isMultipleCategorySelected = false
        }
        
        //newArray is used to getting multiple selected category id data from main data model.
        let newArray  = data.experience.filter { (value) -> Bool in
            return idArray.contains(value.id)
        }
        
        _ = data.experience.map{ value in
            
            if value.id == selectedCategoryId {
                candidateData = value
                
                selctedCategoryIDArray.append(value.id)
                
                sameCategoryExp.append(value)
            }
        }
        //
        if !newArray.isEmpty{
            //Sort multiple selected category id related data.
            let sortedExperience = self.sortExperience(experience: newArray)
            candidateData = sortedExperience.first
        }
        
        if let experianceData = data.experience.first {  //?.durationType == 1 {
            var durationType = 0
            
            if selctedCategoryIDArray.count > 1 {
                
                
                isSelectedSameCategory = true
                _ = sameCategoryExp.map{ value in
                    
                    if value.durationType == 2{
                        sameCategoryTotalExperince += (Float(value.duration) ?? 0)*12
                    }else{
                        sameCategoryTotalExperince += (Float(value.duration) ?? 0)
                    }
                    
                }
                let sortedExperience = self.sortExperience(experience: sameCategoryExp)
                
                candidateData = sortedExperience.first
                
                print_debug("toatal experince== \(sameCategoryTotalExperince) \((sameCategoryTotalExperince/12))")
                selctedCategoryIDArray.removeAll()
            }
            
            if let selectedExperince = candidateData {
                durationType = selectedExperince.durationType
            } else {
                durationType = experianceData.durationType
            }
            if durationType == 1 {
                if let selectedExperince = candidateData{
                    experiance = "\(String(describing: selectedExperince.duration)) \(StringConstants.Months.lowercased()) "
                    exeperianceDuration = experiance
                    self.companyName.text = "\(StringConstants.Worked_at) \(String(describing: selectedExperince.companyName))"
                    experiance.append(selectedExperince.categoryName)
                }else{
                    experiance = "\(String(describing: experianceData.duration)) \(StringConstants.Months.lowercased()) "
                }
            } else {
                
                if let selectedExperince = candidateData {
                    var year = ""
                    
                    if !isSelectedSameCategory{
                        if (Float(selectedExperince.duration) ?? 0) > 11 {
                            year =  String(format: "%.1f", (Float(selectedExperince.duration) ?? 0)/12)
                            print_debug(year)
                        } else {
                            
                            //                        if let convertedYear = Float(selectedExperince.duration){
                            //                            year = "\(convertedYear)"
                            //                        }else{
                            year = selectedExperince.duration
                            //                        }
                            
                            print_debug(year)
                        }
                    }else{
                        year =  String(format: "%.1f", (sameCategoryTotalExperince)/12)
                        
                    }
                    print_debug(year)
                    year = self.convertMonthInYear(year)
                    print_debug(year)
                    experiance = "\(String(describing: year)) \(StringConstants.Years.lowercased()) "
                    exeperianceDuration = experiance
                    self.companyName.text = "\(StringConstants.Worked_at) \(String(describing: selectedExperince.companyName))"
                    experiance.append(selectedExperince.categoryName)
                } else {
                    var year = ""
                    if (Int(experianceData.duration) ?? 0) > 11 {
                        year =  String(format: "%.1f", (Float(experianceData.duration) ?? 0)/12)
                        print_debug(year)
                        
                    }else {
                        year = "\(String(describing: experianceData.duration))"
                        print_debug(year)
                        
                    }
                    year = self.convertMonthInYear(year)
                    print_debug(year)
                    
                    experiance = "\(year) \(StringConstants.Years.lowercased()) "
                }
            }
            
            
            if let _ = candidateData{
            }else{
                self.companyName.text = "\(StringConstants.Worked_at) \(String(describing: experianceData.companyName))"
                exeperianceDuration = experiance
                experiance.append("in \(experianceData.categoryName)")
            }
            
            let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: experiance)
            attributedString.setColorForText(textForAttribute: exeperianceDuration, withColor: AppColors.themeBlueColor, font: AppFonts.Poppins_SemiBold.withSize(14))
            if self.isMultipleCategorySelected {
                self.numberOfexperience.text = ""
            }else{
                self.numberOfexperience.attributedText = attributedString
                
            }
            
        } else {
            self.numberOfexperience.text = StringConstants.NoRelevantExperience
            self.companyName.text = ""
        }
        
        
        var totalMonth : Float = 0
        
        let _ = data.experience.map { (value) -> Void in
            if value.durationType == 1 {
                totalMonth += Float(value.duration) ?? 0
            } else {
                if value.duration == "5+" {
                    totalMonth += 12*5
                } else {
                    totalMonth += ((Float(value.duration) ?? 0 ) * 12)
                }
            }
        }
        
        
        if totalMonth <= 11 {
            
            var monts = "\(totalMonth)"
            if totalMonth == 0 {
                self.totalexperience.text = ""
            } else if monts.contains(".0"){
                monts = monts.replace(string: ".0", withString: "")
                self.totalexperience.text = "\(monts) \(StringConstants.month_of_total_experience)"
            }else {
                self.totalexperience.text = "\(totalMonth) \(StringConstants.months_of_total_experience)"
            }
            
        } else {
            var year = String(format: "%.1f", Float(totalMonth)/12)
            
            // let year = totalMonth/12
            if year == "0.0" {
                self.totalexperience.text = ""
            } else if year.contains(".0"){
                year = year.replace(string: ".0", withString: "")
                self.totalexperience.text = "\(year) \(StringConstants.year_of_total_experience)"
            }
            else {
                self.totalexperience.text = "\(year) \(StringConstants.years_of_total_experience)"
            }
        }
    }
    
    // Replace 0 from the string after dot(.) number and form last.
    func convertMonthInYear(_ durationText: String) -> String{
        var year = durationText
        print_debug(year)
        if year.contains(".0"){
            year = year.replace(string: ".0", withString: "")
        }else if year.last == "0"{
            year.removeLast()
        }
        return year
    }
    
    // Sort experince in highest order.
    func sortExperience(experience: [ExperienceModel]) -> [ExperienceModel]{
        
        
        let  convertInMonth = experience.sorted {
            if let duration1 = Int($0.duration), let duration2 = Int($1.duration){
                return duration1<duration2
            }
            return false
            
        }
        
        return convertInMonth
        
    }
}

