//
//  SubmitButtonCell.swift
//  Onboarding
//
//  Created by macOS on 27/03/18.
//  Copyright © 2018 Gurdeep Singh. All rights reserved.
//

import UIKit

class SubmitButtonCell: UITableViewCell {
    
    //MARK:- IB Outlets
    //===================
    @IBOutlet weak var submitButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.submitButton.backgroundColor = AppColors.themeBlueColor
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.submitButton.setCorner(cornerRadius: 5, clip: true)
    }
    
    func setBtnStyle() {
        self.submitButton.setTitle( StringConstants.Reject, for: .normal)
        self.submitButton.borderColor = AppColors.themeBlueColor
        self.submitButton.borderWidth = 2.0
        self.submitButton.backgroundColor = AppColors.white
        self.submitButton.setTitleColor(AppColors.themeBlueColor, for: .normal)
    }
    
    func setButton(status: Int) {
        self.submitButton.setTitle(StringConstants.SAVE_JOB, for: .normal)
        
        if status == 3 {
            self.submitButton.backgroundColor = AppColors.buttonDisableBgColor
            self.submitButton.isEnabled = false
        } else {
            self.submitButton.backgroundColor = AppColors.themeBlueColor
            self.submitButton.isEnabled = true
        }
    }
}
