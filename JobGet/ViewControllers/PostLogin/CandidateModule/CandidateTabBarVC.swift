//
//  CandidateTabBarVC.swift
//  JobGet
//
//  Created by appinventiv on 16/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class CandidateTabBarVC: UITabBarController {
    
    private var badgeObserverHandle: UInt?
    var myJobs : MyJobsVC!
    var myBadgeCount = 0
    var fromPush = false
    var isChatPush = false
    var job_id = ""
    var sender_id = ""
    var isFirstTime = false
    var roomId = ""
    var chatMember : ChatMember?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if !AppUserDefaults.value(forKey: .pauseMsg).stringValue.isEmpty{
            AppUserDefaults.save(value: "", forKey: .pauseMsg)
            let obj = AlertPopUpVC.instantiate(fromAppStoryboard: .Candidate)
            obj.alertType = .pauseImage
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .crossDissolve
            let rootViewController = UIApplication.shared.keyWindow?.rootViewController
            rootViewController?.present(obj, animated: true, completion: nil)
        }
        self.initialSetup()
        self.observeBadgeCount()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isChatPush{
            isChatPush = false
            selectedIndex = 1
        }
        if self.isFirstTime{
            self.myJobs.isOpenFromNotification = true
            self.selectedIndex = 2
            self.isFirstTime = false
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        self.removeBadgeObserver()
    }
    
    private func initialSetup() {
        
        let search = JobsListVC.instantiate(fromAppStoryboard: .Candidate)
        search.fromTabbar = true
        search.fromPush = self.fromPush
        search.job_id = self.job_id
        search.sender_id = self.sender_id
        myJobs = MyJobsVC.instantiate(fromAppStoryboard: .Candidate)
        let chat = ChatVC.instantiate(fromAppStoryboard: .Candidate)
        chat.fromPush = self.isChatPush
        chat.roomId = self.roomId
        if let member = self.chatMember{
            chat.chatMember = member
        }
        let profile = YourProfileStep1VC.instantiate(fromAppStoryboard: .Recruiter)
        let candidateDetail = RecruiterCandidateDetailVC.instantiate(fromAppStoryboard: .Recruiter)
        
        // check weather experiance added or not if added go to candidate detail  else go to profile
        let value = AppUserDefaults.value(forKey: .userData)
        
        // 1 for no experiance and 2 for experaince
        if value["isExperience"].intValue == 1 || value["isExperience"].intValue == 2 {
            candidateDetail.userId = value["user_id"].stringValue
            candidateDetail.fromProfile = true
            
            self.viewControllers = [search,chat,myJobs,candidateDetail]
        } else {
            self.viewControllers = [search,chat,myJobs,profile]
        }
        
        if let tabs = self.tabBar.items {
            
            for count in 0..<tabs.count{
                
                switch count{
                case 0:
                    tabs[count].title = StringConstants.Home
                    tabs[count].image = #imageLiteral(resourceName: "ic_deselect_home_")
                    tabs[count].selectedImage = #imageLiteral(resourceName: "ic_select_home_")
                    
                    tabs[0].selectedImage = tabs[0].selectedImage?.withRenderingMode(.alwaysOriginal)
                    tabs[0].image = tabs[0].image?.withRenderingMode(.alwaysOriginal)
                case 2:
                    tabs[count].title =  StringConstants.Applications
                    tabs[count].image = #imageLiteral(resourceName: "icHomeDeselectJob")
                    tabs[count].selectedImage = #imageLiteral(resourceName: "icHomeSelectJob")
                    
                    tabs[1].selectedImage = tabs[1].selectedImage?.withRenderingMode(.alwaysOriginal)
                    tabs[1].image = tabs[1].image?.withRenderingMode(.alwaysOriginal)
                    
                case 1:
                    tabs[count].title =  StringConstants.Messages
                    tabs[count].image = #imageLiteral(resourceName: "icHomeDeselectChat")
                    tabs[count].selectedImage = #imageLiteral(resourceName: "icHomeSelectChat")
                    
                    tabs[2].selectedImage = tabs[2].selectedImage?.withRenderingMode(.alwaysOriginal)
                    tabs[2].image = tabs[2].image?.withRenderingMode(.alwaysOriginal)
                case 3:
                    tabs[count].title = StringConstants.Profile
                    tabs[count].image = #imageLiteral(resourceName: "ic_home_profile_deselect")
                    tabs[count].selectedImage = #imageLiteral(resourceName: "ic_home_profile_select1")
                    
                    tabs[3].selectedImage = tabs[3].selectedImage?.withRenderingMode(.alwaysOriginal)
                    tabs[3].image = tabs[3].image?.withRenderingMode(.alwaysOriginal)
                    
                default:
                    return
                }
            }
            self.selectedIndex = 0
        }
        
    }
}

//    MARK:- BadgeCount observer
//    ===========================================
extension CandidateTabBarVC{
    
    private func observeBadgeCount(){
        guard let userId = User.getUserModel().user_id else{
            return
        }
        badgeObserverHandle = databaseReference.child("badgeCount").child(userId).observe(.value) { [weak self](snapshot) in
            
            guard let _self = self else { return }
            if let badgeCount = snapshot.value as? Int{
                print_debug("Badge \(badgeCount)")
                
                if badgeCount > 0{
                    _self.tabBar.items?[1].badgeValue = "\(badgeCount)"
                    UIApplication.shared.applicationIconBadgeNumber = badgeCount
                    _self.myBadgeCount = badgeCount
                    
                }else{
                    _self.tabBar.items?[1].badgeValue = nil
                    UIApplication.shared.applicationIconBadgeNumber = badgeCount
                    _self.myBadgeCount = 0
                    
                }
                AppUserDefaults.save(value: _self.myBadgeCount, forKey: .messageBadge)
            }else{
                print_debug("Badge not found")
            }
        }
    }
    
    private func removeBadgeObserver(){
        if let handle = self.badgeObserverHandle,let userId = User.getUserModel().user_id{
            databaseReference.child("badgeCount").child(userId).removeObserver(withHandle: handle)
        }
    }
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        for controllers in self.viewControllers!{
            if controllers == self.viewControllers?.first{
                (controllers as! JobsListVC).fromTabbar = true
            }
        }
    }

}

