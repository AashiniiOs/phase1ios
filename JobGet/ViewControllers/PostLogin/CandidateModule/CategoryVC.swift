//
//  CategoryVC.swift
//  JobGet
//
//  Created by Manish Nainwal on 11/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class CategoryVC: UIViewController {
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var categoryTableView: UITableView!
    @IBOutlet weak var applyButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var resetButton: UIButton!
    
    //MARK:- Variables and Constants
    //==============================
    var categoryList: [String] = []
    
    //MARK:- LifeCycle Methods
    //========================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    //MARK:- Private Methods
    //======================
    
    private func initialSetup(){
        
        self.categoryTableView.delegate = self
        self.categoryTableView.dataSource = self
        self.titleLabel.text = StringConstants.CategoryTitle.localized
        self.applyButton.setTitle(StringConstants.Category_ApplyButton_Title.localized , for: .normal)
        self.resetButton.setTitle(StringConstants.Category_ResetButton_Title.localized, for: .normal)
    }
    
    //MARK:- Target Actions
    //=====================
    
    
    //MARK:- IBActions
    //===================
    @IBAction func onTappingBackButton(_ sender: UIButton) {
    }
    
    @IBAction func onTappingApplyButton(_ sender: UIButton) {
    }
    
    @IBAction func onTappingResetButton(_ sender: UIButton) {
    }
}

//MARK:- TableViewDataSource, TableViewDelegate
//=================================================
extension CategoryVC: UITableViewDataSource, UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.categoryList.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 0.088 * UIScreen.main.bounds.height
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell") as! CategoryCell
        return cell
    }
}

//MARK:- Webservice Methods
//=========================
extension CategoryVC{
    
    func getCategoryList(){
        let param = ["": ""]
        WebServices.getCategoryList(parameters: param, loader: true, success: { (data) in
            print_debug(data)
        }) { (error) in
            print_debug(error)
        }
    }
}

class CategoryCell: UITableViewCell{
    
    @IBOutlet weak var categoryIcon: UIImageView!
    @IBOutlet weak var categoryTitle: UILabel!
    @IBOutlet weak var checkedUnchecked: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
