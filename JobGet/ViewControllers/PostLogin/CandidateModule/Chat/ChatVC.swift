//
//  ChatVC.swift
//  JobGet
//
//  Created by appinventiv on 16/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import Firebase

class CustomTriangleLayer : CAShapeLayer {}

class ChatVC: UIViewController {
    
    //    MARK:- Properties
    //    ===========================================
    private var recentChatVc: RecentChatVC!
    private var videoCallHistoryVc : VideoCallHistoryVC!
    private var badgeObserverHandle: UInt?
    
    var myBadgeCount = 0
    var fromPush = false
    var roomId = ""
    var chatMember : ChatMember?

    //    MARK:- IBOutlets
    //    ===========================================
    
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var screenTitle: UILabel!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var underLineView: UIView!
    @IBOutlet weak var underLineViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var recentChatButton: UIButton!
    @IBOutlet weak var videoChatButton: UIButton!
    @IBOutlet weak var badgeLabel: UILabel!
    
    //    MARK:- View life cycle
    //    ===========================================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetUp()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if AppUserDefaults.value(forKey: .totalNotification).intValue > 0 {
            self.badgeLabel.isHidden = false
        }else{
            self.badgeLabel.isHidden = true
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.view.endEditing(true)
    }
    
    //    MARK:- functions
    //    ===========================================
    private func initialSetUp(){
        
        self.shadowView.setShadow()
        self.badgeLabel.roundCorners()
        self.scrollView.isScrollEnabled = false
        self.recentChatVc = RecentChatVC.instantiate(fromAppStoryboard: .Candidate)
        self.recentChatVc.view.frame.origin = CGPoint(x: 0, y: 0)
        if fromPush {
            recentChatVc.fromPush = self.fromPush
            recentChatVc.roomId = self.roomId
            if let member = self.chatMember{
                recentChatVc.chatMember = member
            }
            self.fromPush = false
        }

        self.scrollView.frame = self.recentChatVc.view.frame
        self.scrollView.addSubview(self.recentChatVc.view)
        self.addChildViewController(self.recentChatVc)
        self.willMove(toParentViewController: self.recentChatVc)
        
        self.videoCallHistoryVc = VideoCallHistoryVC.instantiate(fromAppStoryboard: .Candidate)
       
        self.videoCallHistoryVc.view.frame.origin = CGPoint(x: SCREEN_WIDTH, y: 0)
        self.scrollView.frame = self.videoCallHistoryVc.view.frame
         self.videoCallHistoryVc.isFirstTime = true
        self.scrollView.addSubview(self.videoCallHistoryVc.view)
        self.addChildViewController(self.videoCallHistoryVc)
        self.willMove(toParentViewController: self.videoCallHistoryVc)
        
        self.scrollView.contentSize.width = 2 * SCREEN_WIDTH
        self.scrollView.frame.size.width = 2 * SCREEN_WIDTH
        self.scrollView.isPagingEnabled = true
        self.scrollView.delegate = self
        
        self.recentChatButton.isSelected = true
        self.myBadgeCount = AppUserDefaults.value(forKey: .messageBadge).intValue
    }
    
    private func setButtonState(_ state: Bool){
        
        self.recentChatButton.isSelected = state
        self.videoChatButton.isSelected = !state
    }
    
    
    //    MARK:- IBactions
    //    ===========================================
    @IBAction func recentChatButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        self.scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
    }
    
    @IBAction func videoChatBttonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        self.scrollView.setContentOffset(CGPoint(x: SCREEN_WIDTH, y: 0), animated: true)
    }
    
    @IBAction func notificationBtnTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        let notificationVC = NotificationsVC.instantiate(fromAppStoryboard: .settingsAndChat)
        self.navigationController?.pushViewController(notificationVC, animated: true)
        
    }
}

//    MARK:- ScrollView delegate
//    ===========================================
extension ChatVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        guard scrollView === self.scrollView else {
            return
        }
        
        self.underLineViewLeadingConstraint.constant = self.scrollView.contentOffset.x / 2
        
        if scrollView.contentOffset.x < (SCREEN_WIDTH * 0.5){
            self.setButtonState(true)
        }else{
            self.setButtonState(false)
        }
    }
}
