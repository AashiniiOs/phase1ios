//
//  PersonalChatVC+FirebaseMethods.swift
//  JobGet
//
//  Created by Admin on 18/10/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import SwiftyJSON
import DotsLoading
import RealmSwift

//    MARK:- Compose Message and send
//    ========================================
extension PersonalChatVC {
    
    /**
     Send text message
     */
    func sendTextMessage(_ text: String){
        
        
        self.sendMessage { () -> (ChatMessage) in
            return self.composeTextMessage(text)
        }
    }
    
    /**
     Set message at the firebase node
     */
    func sendMessage(_ getMessage: ()->(ChatMessage)){
        
        switch self.chatType{
            
        case .new:
            self.createRoomInfo()
            
        case .old:
            print_debug("Room already exist")
        case .none:
            fatalError("Chat type cannot be none")
            
        }
        let message = getMessage()
        
        ChatHelper.setLastMessage(message)
        
        if !message.isBlock{
            ChatHelper.removeOtherUserLastMessage(message)
        }
        
        ChatHelper.sendMessage(message)
        ChatHelper.setLastUpdates(forRoom: message.roomId,
                                  userId: message.senderId)
        self.createInbox(roomId: message.roomId)

        let unreadRef = "/\(ChatEnum.Root.unreadMessage.rawValue)/\(message.roomId)/\(self.chatMember.userId)"
         let badgeCount = "/badgeCount/\(self.chatMember.userId)"
         let dict1: JSONDictionary = ["\(unreadRef)":self.otherUserUnreadMessageCount + 1, "\(badgeCount)": self.otherUserbadgeCount + 1]
         databaseReference.updateChildValues(dict1)
        if chatType == .new{
            self.addFirebaseObservers(roomId: message.roomId)
            self.chatType = .old
        }
    }
    
    /**
     Compose text message
     */
    func composeTextMessage(_ text: String)->ChatMessage{
        
        return ChatHelper.composeTextMessage(text: text,
                                             sender: self.user.user_id!,
                                             receiver: self.chatMember.userId,
                                             roomId: self.chatRoom.chatRoomId,
                                             isBlock: self.blockStatus.someoneBlockedMe,
                                             deviceToken:self.chatMember.deviceToken ?? "", deviceType: self.chatMember.deviceType)
    }
    /**
     Create room info
     */
    func createRoomInfo(){
        
        guard let currentUserId = self.user.user_id else { return }
        
        self.chatRoom = ChatHelper.createRoom(forUserId: self.chatMember.userId,
                                              curUserId: currentUserId,
                                              members: self.getChatMembers())
        
    }
    
    func getChatMembers()->[ChatMember]{
        
        
        guard let currentUserId = self.user.user_id else { return []}
        
        let member2: ChatMember = ChatMember(userId: currentUserId,
                                             firstName: self.user.first_name,
                                             lastName: self.user.last_name,
                                             email: self.user.email,
                                             userImage: self.user.imageUrl,
                                             mobileNumber: self.user.phone,
                                             deviceToken: "",
                                             isOnline: true, deviceType: Device_Type.iOS.rawValue)
        
        
        return [self.chatMember,member2]
    }
}


//    MARK:- Firebase observers
//    ========================================
extension PersonalChatVC {
    
      func addFirebaseObservers(roomId: String){
        
        self.observeSelfUnreadMessageCount(roomId: roomId)
        self.observeMessages(forRoom: roomId)
        self.observeTypingStatus(roomId: roomId)
        self.getTypingStatus(roomId: roomId)
        self.observeUnreadMessageCountOfOtherUser(roomId: roomId)
        ChatHelper.setUserLastUpdate(forRoom: roomId,
                                     userId: self.user.user_id!)
    }
    
    
    func hideLoder(){
        if AppUserDefaults.value(forKey: .userType).stringValue == UserType.candidate.rawValue{
            AppNetworking.hideLoader()
        }
    }
    //    MARK:- Observe new messages
    //    =============================================
    func observeMessages(forRoom roomId: String){
        if AppUserDefaults.value(forKey: .userType).stringValue == UserType.candidate.rawValue{
            AppNetworking.showLoader()
        }
        let currentMember = self.chatRoom.currentUserUpdate
        
        let messageRef = databaseReference.child(ChatEnum.Message.root.rawValue).child(roomId)
        
        self.databaseHandle[ObserverKeys.message.rawValue] = messageRef.queryOrdered(byChild: "timestamp").observe(.childAdded) { [weak self](snapshot) in
            
            guard let _self = self else {
                self?.hideLoder()
                return }
            if let value = snapshot.value{
                
                do {
                    let data = try JSON(value).rawData()
                    let message = try JSONDecoder().decode(ChatMessage.self, from: data)
                    
                    if message.status != .read && message.senderId == _self.user.user_id! {
                        _self.observeMessageSeenStatus(message: message,
                                                       roomId: message.roomId)
                    }
                    _self.refreshMessageData(message)
                    
                    if !DBManager.sharedInstance.getDataFromDB().isEmpty , let messageList = DBManager.sharedInstance.getChatUserForMessageId(id: message.roomId, user_id: _self.user.user_id ?? "", message_Id: message.messageId), !messageList.messageId.isEmpty{
                    }else{
                        if _self.isSendMessage{
                            let messageList:JSONDictionary = [
                                "messageId": message.messageId,
                                "message":message.message,
                                "timestamp": "\(message.timestamp)",
                                "roomId":message.roomId,
                                "first_name":_self.chatMember.firstName,
                                "mediaUrl": message.mediaUrl,
                                "senderId":  message.senderId,
                                "last_name": _self.chatMember.lastName
                            ]
                            let uiRealm = try! Realm()
                            do {
                                try uiRealm.write {
                                    _self.userListDB.messageList.append(MessageListDB(value: messageList))
                                }
                            }catch let error {
                            }
                            
                            _self.upadteLocalDB(room_id: message.roomId)
                        }
                        
                    }
                }
                catch let error{
                     self?.hideLoder()
                }
               
            }
        }
    }
    
    //  MARK:- Observe message seen status
    //  ==========================================
    
      func observeMessageSeenStatus(message msg: ChatMessage, roomId: String){
        
        let messageRef = databaseReference.child(ChatEnum.Message.root.rawValue).child(roomId)
        let seenRef = messageRef.child(msg.messageId).child("status")
        
        self.seenObserverHandle[msg.messageId] = seenRef.observe(.value) { [weak self] (snapshot) in
            
            guard let _self = self else { return }
            
            if let value = snapshot.value as? String{
                
                let key = dateManager.getNotationFor(timestamp: msg.timestamp)
                
                if let status = ChatEnum.MessageStatus(rawValue: value),let msgArr = _self.organisedData[key], status == .read{
                    if let index = msgArr.index(where: { (message) -> Bool in
                        return msg.messageId == message.messageId
                    }){
                        _self.organisedData[key]?[index].status = status
                        if let message = _self.organisedData[key]?[index]{
                            _self.removeMessageSeenObserver(messageId: message.messageId, roomId:message.roomId )
                        }
                        _self.mainTableView.reloadData()
                    }
                }
            }
        }
    }
    
    //  MARK:- Observe other user typing status
    //  ========================================
      func observeTypingStatus(roomId: String){
        
        let typingRef = databaseReference.child(ChatEnum.Root.roomInfo.rawValue).child(roomId).child("chatRoomIsTyping")
        
        self.databaseHandle[ObserverKeys.typing.rawValue] = typingRef.observe(.childChanged) { [weak self](snapshot) in
            
            guard let _self = self, let curUserId = _self.user.user_id else { return }
            if let value = snapshot.value as? Bool{
                
                if snapshot.key != curUserId {
                    _self.typingContainerView.isHidden = !value
                    let url = URL(string: _self.chatMember.userImage)
                    _self.recieverImage.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "ic_placeholder_22"), options: [], completed: nil)
                    _self.recieverImage.isHidden = !value
                    
                    _self.bubbleView.isHidden = !value
                    _self.dotViewAnimation(isShow: value)
                }
                
            }
        }
    }
    
    
      func dotViewAnimation(isShow: Bool){
        
        //        let dotColors = [AppColors.gray152, AppColors.gray152, AppColors.gray152]
        //        let loadingView = DotsLoadingView(colors: dotColors)
        //        self.bubbleView.addSubview(loadingView)
        //
        //
        //        if !isShow{
        //
        //            loadingView.show(view: self.bubbleView)
        //        }else{
        //            loadingView.stop()
        //        }
    }
    
    //  MARK:- Observe other user unread message count
    //  ================================================
    
      func observeUnreadMessageCountOfOtherUser(roomId: String){
        
        let unreadRef = databaseReference.child(ChatEnum.Root.unreadMessage.rawValue).child(roomId)
        
        self.databaseHandle[ObserverKeys.unreadCount.rawValue] = unreadRef.child(self.chatMember.userId).observe(.value) { [weak self](snapshot) in
            
            guard let _self = self else { return }
            
            if let count = snapshot.value as? Int{
                _self.otherUserUnreadMessageCount = count
            }
        }
    }
    
    //  MARK:- Observe SELF unread message count
    //  ================================================
    
      func observeSelfUnreadMessageCount(roomId: String){
        guard let userId = User.getUserModel().user_id else {return}
        let unreadRef = databaseReference.child(ChatEnum.Root.unreadMessage.rawValue).child(roomId)
        
        self.databaseHandle[ObserverKeys.myUnreadCount.rawValue] = unreadRef.child(userId).observe(.value) { [weak self](snapshot) in
            
            guard let _self = self else { return }
            
            if let count = snapshot.value as? Int{
                _self.myUnreadMessageCount = count
            }
        }
    }
}

//MARK:- Firebase single event
//========================================
extension PersonalChatVC{
    
      func createInbox(roomId: String){
        
        guard let currentUserId = self.user.user_id else { return }
        
        ChatHelper.createInbox(forUserId: currentUserId,
                               roomId: roomId,
                               otherUserId: self.chatMember.userId)
    }
    
      func getTypingStatus(roomId: String){
        let typingRef = databaseReference.child(ChatEnum.Root.roomInfo.rawValue).child(roomId).child("chatRoomIsTyping").child(self.chatMember.userId)
        
        typingRef.observeSingleEvent(of: .value) { [weak self](snapshot) in
            
            guard let _self = self, let curUserId = _self.user.user_id else { return }
            if let value = snapshot.value as? Bool{
                
                if snapshot.key != curUserId{
                    _self.typingContainerView.isHidden = !value
                    
                    _self.recieverImage.isHidden = !value
                    let url = URL(string: _self.chatMember.userImage)
                    _self.recieverImage.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "ic_placeholder_22"), options: [], completed: nil)
                    _self.bubbleView.isHidden = !value
                    _self.dotViewAnimation(isShow: value)
                }
            }
        }
    }
    
      func updateSelfTypingStatus(_ isTyping: Bool, _ roomId: String){
        
        guard let curUserId = self.user.user_id,!self.blockStatus.hasBlockedSomeOne else { return }
        let typingRef = databaseReference.child(ChatEnum.Root.roomInfo.rawValue).child(roomId).child("chatRoomIsTyping")
        
        typingRef.updateChildValues([curUserId: isTyping])
    }
    
    //  MARK:- Check whether the chat is old or new
    //  ============================================
      func checkChatType(){
        
        guard let currentUserId = self.user.user_id else { return }
        
        ChatHelper.checkifNewSingleChat(forUserId: self.chatMember.userId, curUserId: currentUserId) { [weak self](roomId) in
            
            guard let _self = self else { return }
            
            if let roomId = roomId{
                
                _self.chatType = .old
                _self.getRoomDetail(roomId: roomId)
                
            }else{
                
                _self.chatType = .new
            }
        }
    }
    
    //  MARK:- GetRoomDetails
    //  ============================================
      func getRoomDetail(roomId: String){
        
        ChatHelper.getChatRoomDetails(roomId: roomId) { [weak self](chatroom) in
            
            guard let _self = self else { return }
            
            if let room = chatroom{
                
                _self.chatRoom = room
                _self.addFirebaseObservers(roomId: roomId)
            }
        }
    }
    
    //  MARK:- Read other user messages
    //  ==========================================
      func readOtherUserMessage(messageId: String, roomId: String){
        let messageRef = databaseReference.child(ChatEnum.Message.root.rawValue).child(roomId)
        let seenRef = messageRef.child(messageId).child("status")
        seenRef.setValue(ChatEnum.MessageStatus.read.rawValue)
    }
    
    //  MARK:- Get previous messages
    //  ==========================================
    @objc func getPreviousMessages(){
        
        guard !self.organisedData.isEmpty else { return }
        if let key = self.organisedKeys.first, let lastKey = self.organisedData[key]?.first?.messageId{
            
            self.refeshControl.beginRefreshing()
            ChatHelper.getPaginatedData(self.chatRoom.chatRoomId,
                                        lastKey) { [weak self](messages) in
                                            
                                            guard let _self = self else { return}
                                            _self.refeshControl.endRefreshing()
                                            _self.organisePrevious(messages: messages)
            }
        }
    }
    
    //  MARK:- Get star consumed status
    //  ==========================================
    
      func checkStarConsumedStatus(){
        
        guard let currentUserId = self.user.user_id else { return }
        
        ChatHelper.getConsumedStarStatus(forUser: self.chatMember.userId, currentUserId: currentUserId) { [weak self](starConsumed) in
            
            guard let _self = self else {return}
            
            _self.isStarConsumed = starConsumed
            _self.textFieldContainerView.isHidden = !starConsumed
        }
    }
    
      func setUnreadCountZero(){
        
        if self.chatRoom != nil{
            guard let userId = self.user.user_id else  {
                return
            }
            let count = self.myBadgeCount - self.myUnreadMessageCount
            let unreadRef = "/\(ChatEnum.Root.unreadMessage.rawValue)/\(self.chatRoom.chatRoomId)/\(userId)"
            let badgeCount = "/badgeCount/\(userId)"
            let dict1: JSONDictionary = ["\(unreadRef)":0, "\(badgeCount)": count >= 0 ? count : 0]
           databaseReference.updateChildValues(dict1)
        }
    }
    
      func resetBadgeCount(){
        if self.chatRoom != nil{
            guard let userId = User.getUserModel().user_id else { return }
            let count = self.myBadgeCount - self.myUnreadMessageCount
            
            if count >= 0{
                ChatHelper.setBadgeCount(userId: userId, count: count)
            }else{
                ChatHelper.setBadgeCount(userId: userId, count: 0)
            }
        }
    }
}
//MARK:- Remove firebse observers
//======================================

extension PersonalChatVC{
    
      func removeFirebaseObservers(roomId: String){
        
        ChatHelper.setUserLastUpdate(forRoom: roomId,
                                     userId: self.user.user_id!)
        
        if let handle = self.databaseHandle[ObserverKeys.message.rawValue]{
            
            let messageRef = databaseReference.child(ChatEnum.Message.root.rawValue).child(roomId)
            messageRef.queryLimited(toLast: 20).queryOrdered(byChild: "isBlock").queryEqual(toValue: false).removeObserver(withHandle: handle)
            
        }
        
        if let handle = self.databaseHandle[ObserverKeys.typing.rawValue]{
            databaseReference.child(ChatEnum.Root.roomInfo.rawValue).child(roomId).child("chatRoomIsTyping").removeObserver(withHandle: handle)
        }
        
        if let handle = self.databaseHandle[self.chatMember.userId]{
            
            let blockRef = databaseReference.child(ChatEnum.Root.block.rawValue).child(self.user.user_id!)
            blockRef.removeObserver(withHandle: handle)
        }
        
        
        if let handle = self.databaseHandle[self.user.user_id!]{
            databaseReference.child(ChatEnum.Root.block.rawValue).child(self.chatMember.userId).removeObserver(withHandle: handle)
        }
        
        if let handle = self.databaseHandle[ObserverKeys.unreadCount.rawValue]{
            let unreadRef = databaseReference.child(ChatEnum.Root.unreadMessage.rawValue).child(roomId)
            
            unreadRef.child(self.chatMember.userId).removeObserver(withHandle: handle)
        }
        
        if let handle = self.databaseHandle[ObserverKeys.myUnreadCount.rawValue], let userId = User.getUserModel().user_id{
            let unreadRef = databaseReference.child(ChatEnum.Root.unreadMessage.rawValue).child(roomId)
            unreadRef.child(userId).removeObserver(withHandle: handle)
        }
    }
    
      func removeMessageSeenObserver(messageId: String, roomId: String){
        
        if let handle = self.seenObserverHandle[messageId]{
            
            let messageRef = databaseReference.child(ChatEnum.Message.root.rawValue).child(roomId)
            let seenRef = messageRef.child(messageId).child("status")
            seenRef.removeObserver(withHandle: handle)
            
        }
    }
    
    func deleteChat(){
        let popUpVc = AlertPopUpVC.instantiate(fromAppStoryboard: .Candidate)
        popUpVc.delegate = self
        popUpVc.fromOpenChat = true
        popUpVc.modalPresentationStyle = .overCurrentContext
        popUpVc.modalTransitionStyle = .crossDissolve
        self.present(popUpVc, animated: true, completion: nil)
        
    }
    
    func upadteLocalDB(room_id: String){
        var isGetId = false
        var isUserId = false
        if self.isSendMessage{
            if let item = DBManager.sharedInstance.getChatLocalDbForId(id: User.getUserModel().user_id ?? "") {
                let uiRealm = try! Realm()
                //                if item.chat_userId == (User.getUserModel().user_id ?? ""){
                isUserId = true
                if let userList = DBManager.sharedInstance.getChatUserForRoomId(id: room_id, user_id: User.getUserModel().user_id ?? ""){
                    isGetId = true
                    for message in self.userListDB.messageList{
                        if !message.isInvalidated{
                            
                            
                            do {
                                try uiRealm.write {
                                    if !message.messageId.isEmpty{
                                        userList.messageList.append(message)
                                    }else{
                                        return
                                    }
                                }
                            }catch let error {
                                print(error)
                            }
                        }
                    }
                    DBManager.sharedInstance.addData(object: item)
                    
                }else{
                    if !isGetId{
                        do {
                            try uiRealm.write {
                                if !isGetId{
                                    self.userListDB.room_id = room_id
                                }
                                item.userList.append(self.userListDB)
                            }
                        }catch let error {
                            print(error)
                        }
                        //                            self.chatLocalDB.userList.append(self.userListDB)
                        DBManager.sharedInstance.addData(object: item)
                    }
                }
            }else{
                if !isUserId{
                    self.chatLocalDB.chat_userId = (User.getUserModel().user_id ?? "")
                }
                if !isGetId{
                    self.userListDB.room_id = room_id
                }
                self.chatLocalDB.userList.append(self.userListDB)
                DBManager.sharedInstance.addData(object: self.chatLocalDB)
            }
            if DBManager.sharedInstance.getDataFromDB().isEmpty{
                if !isUserId{
                    self.chatLocalDB.chat_userId = (User.getUserModel().user_id ?? "")
                }
                if !isGetId{
                    self.userListDB.room_id = room_id
                }
                self.chatLocalDB.userList.append(self.userListDB)
                DBManager.sharedInstance.addData(object: self.chatLocalDB)
            }
        }
        
    }
}

//    MARK:- BadgeCount observer
//    ===========================================
extension PersonalChatVC{
    
      func observeBadgeCount(){
        
        self.databaseHandle["badgeCount"] = databaseReference.child("badgeCount").child(self.chatMember.userId).observe(.value) { [weak self](snapshot) in
            guard let _self = self else { return }
            if let badgeCount = snapshot.value as? Int{
                _self.otherUserbadgeCount = badgeCount
            }else{
            }
        }
    }
    
      func observeMyBadgeCount(){
        guard let userId = User.getUserModel().user_id else { return }
        
        self.databaseHandle["MybadgeCount"] = databaseReference.child("badgeCount").child(userId).observe(.value) { [weak self](snapshot) in
            guard let _self = self else { return }
            if let badgeCount = snapshot.value as? Int{
                AppUserDefaults.save(value: badgeCount, forKey: .messageBadge)
            }else{
            }
        }
    }
    
      func removeBadgeObserver(){
        if let handle = self.databaseHandle["badgeCount"]{
            databaseReference.child("badgeCount").child(self.chatMember.userId).removeObserver(withHandle: handle)
        }
        
        if let handle = self.databaseHandle["MybadgeCount"], let userId = User.getUserModel().user_id{
            databaseReference.child("badgeCount").child(userId).removeObserver(withHandle: handle)
        }
    }
}

class CopyableLabel: UILabel {
    
    override public var canBecomeFirstResponder: Bool {
        get {
            return true
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        sharedInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        sharedInit()
    }
    
    func sharedInit() {
        isUserInteractionEnabled = true
        addGestureRecognizer(UILongPressGestureRecognizer(
            target: self,
            action: #selector(showMenu(sender:))
        ))
    }
    
    override func copy(_ sender: Any?) {
        UIPasteboard.general.string = text
        UIMenuController.shared.setMenuVisible(false, animated: true)
    }
    
    @objc func showMenu(sender: Any?) {
        becomeFirstResponder()
        let menu = UIMenuController.shared
        if !menu.isMenuVisible {
            menu.setTargetRect(bounds, in: self)
            menu.setMenuVisible(true, animated: true)
        }
    }
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        if action == #selector(copy(_:)) {
            return true
        }
        
        return false
    }
}

