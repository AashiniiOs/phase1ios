//
//  PersonalChatVC.swift
//  JobGet
//
//  Created by appinventiv on 08/05/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import Firebase
import SwiftyJSON
import DotsLoading
import RealmSwift

protocol PersonalChatVCDelegate : NSObjectProtocol {
    func chatDeleted(chatRoomId : String)
}

class PersonalChatVC : BaseVC {
    
    struct BlockStatus{
        var hasBlockedSomeOne: Bool = false
        var someoneBlockedMe: Bool = false
    }
    
    enum AlertType{
        case report
        case shortlist
        case starDeduct
        case none
    }
    
    //    MARK:- IBOutlets
    //    ========================================
    
    @IBOutlet weak var shortListRoundView: UIView!
    @IBOutlet weak var shortlistView: UIView!
    @IBOutlet weak var consumeStarButton: UIButton!
    @IBOutlet weak var totalStarLabel: UILabel!
    @IBOutlet weak var consumeStarAlertView: UIView!
    @IBOutlet weak var startChatRoundView: UIView!
    @IBOutlet weak var recieverImage: UIImageView!
    @IBOutlet weak var bubbleView: UIView!
    @IBOutlet weak var typingContainerView: UIView!
    @IBOutlet weak var navigationBarView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var videoCallButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var textFieldContainerView: UIView!
    @IBOutlet weak var textFieldContainerViewBottom: NSLayoutConstraint!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var mainTableView: UITableView!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var containerViewHeight: NSLayoutConstraint!
    
    
    //    MARK:- Properties
    //    ========================================
    var chatLocalDB = ChatLocalDB()
    var chatListDB = [ChatLocalDB]()
    var userListDB = ChatUserListDB()
    var isLoadingMessages = false
    var lastFetchedMessageId = ""
    var fromPendingJobVC = false
    var isOpenFromProfile = false
    var chatMember : ChatMember!
    var chatRoom: ChatRoom!
    var chatType: ChatEnum.ChatType = .none
    var selectedIndex : IndexPath?
    weak var delegate : PersonalChatVCDelegate?
    var blockStatus = BlockStatus()
    var activeJobList = [CandidateAppliedJobInfo]()
    var databaseHandle: [String: UInt] = [:]
    var organisedData: [String: [ChatMessage]] = [:]
    var organisedKeys: [String] = []
    
    let user = User.getUserModel()
    var seenObserverHandle: [String: UInt] = [:]
    
    var refeshControl = UIRefreshControl()
    var isStarConsumed: Bool = false
    
    var otherUserUnreadMessageCount: Int = 0
    var otherUserbadgeCount: Int = 0
    var myUnreadMessageCount: Int = 0
    var myBadgeCount: Int{
        return AppUserDefaults.value(forKey: .messageBadge).intValue
    }
    var totalMessage = 0
    var isVideoCallInitiate = false
    var isExpired = false
    var isSendMessage = false
    var alertType = AlertType.none
    var isShortlisted = false
    var isReported = false
    var isStarDeducted = false
    var fromShortlisted = false
   
    override var canBecomeFirstResponder: Bool { return true }
    
    //    MARK:- View Life Cycle
    //    ========================================
    override func loadView() {
        super.loadView()
        if AppUserDefaults.value(forKey: .userType).stringValue == UserType.recuriter.rawValue{
            self.getActiveJobListApi()
        }
        UIApplication.shared.shortcutItems?.removeAll()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialSetup()
        addAppObserver()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        IQKeyboardManager.shared.enable = false
        IQKeyboardManager.shared.enableAutoToolbar = false
        
        if self.chatRoom != nil{
            self.addFirebaseObservers(roomId: self.chatRoom.chatRoomId)
        }
        
        //  self.typingContainerView.backgroundColor = AppColors.gray152
        self.typingContainerView.layer.cornerRadius =  self.typingContainerView.frame.size.height / 2
        self.typingContainerView.borderColor = UIColor.lightGray
        self.typingContainerView.borderWidth = 1.5
        self.observeBadgeCount()
        self.observeMyBadgeCount()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = true
        
        if self.chatRoom != nil{
            self.removeFirebaseObservers(roomId: self.chatRoom.chatRoomId)
        }
        self.setUnreadCountZero()
        self.removeAppObserver()
        self.removeBadgeObserver()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        self.typingContainerView.layer.cornerRadius =  self.typingContainerView.frame.size.height / 2
        
        self.recieverImage.layer.cornerRadius =  self.recieverImage.frame.size.height / 2
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        self.startChatRoundView.dropShadow(color: AppColors.appBlack, opacity:0.2, offSet: CGSize(width: 1.0, height: 1.0), radius: 1.0)
        self.startChatRoundView.roundCorners()
        self.consumeStarButton.roundCorners()
        
        self.shortListRoundView.roundCorners()
        //         self.consumeStarButton.dropShadow(color: AppColors.appBlack, opacity:0.2, offSet: CGSize(width: 2.0, height: 2.0), radius: 2.0)
    }
    
    deinit {
        removeAppObserver()
    }
    
    private func addAppObserver(){
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleApplicationTermination), name: .UIApplicationWillTerminate, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.handleApplicationInBackground), name: .UIApplicationWillResignActive, object: nil)
    }
    
    private func removeAppObserver(){
        NotificationCenter.default.removeObserver(self, name: .UIApplicationWillTerminate, object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIApplicationWillResignActive, object: nil)
    }
    
    /// Reset badge count when the application is terminated by user
    @objc private func handleApplicationTermination(){
        self.setUnreadCountZero()
        let count = self.myBadgeCount - self.myUnreadMessageCount
        UIApplication.shared.applicationIconBadgeNumber = count >= 0 ? count : 0
        print("Called")
    }
    
    /// Reset badge count when the application enters background
    @objc private func handleApplicationInBackground(){
        self.setUnreadCountZero()
    }
    
    //    MARK:- IBActions
    
    @IBAction func profileDetailButtonTapp(_ sender: UIButton) {
        self.openProfileDetail()
    }
    
    @IBAction func shortlistButtonTapp(_ sender: UIButton) {
        if !self.activeJobList.isEmpty && !self.isShortlisted && !self.isReported{
            let shortListedPopup = SortListedStatusPopupVC.instantiate(fromAppStoryboard: .Recruiter)
            shortListedPopup.jobInfo = self.activeJobList
            shortListedPopup.delegate = self
            shortListedPopup.sortListedPopupType = .fromChat
            self.view.addSubview(shortListedPopup.view)
            self.add(childViewController: shortListedPopup)
            return
        }
        
        if self.activeJobList.isEmpty{
            self.showAlert(title: "", msg: "Either you don't have any active job or the candidate is expired/rejected")
            return
        }
        
        if !self.isShortlisted{
            self.showAlert(title: "", msg: StringConstants.Please_Create_at_least_one_job)
            return
        }
        
        if self.isReported{
            self.showPopup(desText: StringConstants.You_reported_the_candidate_So_you_cannot_initiate_message)
            return
        }
    }
    
    func openProfileDetail(){
        if self.isOpenFromProfile{
            if fromPendingJobVC {
                self.dismiss(animated: true, completion: nil)
            } else {
                self.navigationController?.popViewController(animated: true)
            }
            self.setUnreadCountZero()
        }else{
            if AppUserDefaults.value(forKey: .userType).stringValue == "2"{
                let profileDetail = RecruiterCandidateDetailVC.instantiate(fromAppStoryboard: .Recruiter)
                profileDetail.userId = self.chatMember.userId
                profileDetail.fromProfile = false
                profileDetail.fromRecruiterSearchVC = true
                self.navigationController?.pushViewController(profileDetail, animated: true)
            }else{
                let profileDetail = EmployerDetailVC.instantiate(fromAppStoryboard: .settingsAndChat)
                profileDetail.isOpenFromChat = true
                profileDetail.userId = self.chatMember.userId
                profileDetail.name = "\(self.chatMember.firstName ) \(self.chatMember.lastName )"
                if let navigationcontroller = self.navigationController{
                    navigationcontroller.pushViewController(profileDetail, animated: true)
                }else{
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    @objc func profileDetailBtnTap(_ sender: UIButton) {
        if self.isOpenFromProfile{
            if fromPendingJobVC {
                self.dismiss(animated: true, completion: nil)
            } else {
                self.navigationController?.popViewController(animated: true)
            }
            self.setUnreadCountZero()
        }else{
            if AppUserDefaults.value(forKey: .userType).stringValue == "2"{
                let profileDetail = RecruiterCandidateDetailVC.instantiate(fromAppStoryboard: .Recruiter)
                profileDetail.userId = self.chatMember.userId
                profileDetail.fromProfile = false
                self.navigationController?.pushViewController(profileDetail, animated: true)
            }else{
                let profileDetail = EmployerDetailVC.instantiate(fromAppStoryboard: .settingsAndChat)
                profileDetail.isOpenFromChat = true
                profileDetail.userId = self.chatMember.userId
                profileDetail.name = "\(self.chatMember.firstName ) \(self.chatMember.lastName )"
                if let navigationcontroller = self.navigationController{
                    navigationcontroller.pushViewController(profileDetail, animated: true)
                }else{
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func consumeStarButtonTapp(_ sender: UIButton) {
        
        let totalStar = AppUserDefaults.value(forKey: .freeStars).intValue + AppUserDefaults.value(forKey: .stars).intValue
        if totalStar == 0{
            let package = StarPackage.instantiate(fromAppStoryboard: .Star)
            package.isFromChat = true
            package.delegate = self
            self.navigationController?.pushViewController(package, animated: true)
        }else{
            self.starDeductApi()
        }
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
//        if fromPendingJobVC {
//            self.dismiss(animated: true, completion: nil)
//        } else {
            self.navigationController?.popViewController(animated: true)
//        }
//        self.resetBadgeCount()
    }
    
    @IBAction func videoCallButtonTapped(_ sender: UIButton) {
        
        if !self.activeJobList.isEmpty && !self.isShortlisted && !self.isReported{
            self.showPopup(desText: StringConstants.Shortlist_candidate_Before_call)
            return
        }
        if !self.isShortlisted {
            self.showAlert(title: "", msg: StringConstants.Please_Create_at_least_one_job)
            return
        }
        
        if !self.isStarDeducted{
            self.showAlert(title: "", msg: StringConstants.Consume_Star_Before_call)
            return
        }
        
        if self.isReported{
            self.showPopup(desText: StringConstants.You_reported_the_candidate_So_you_cannot_initiate_message)
            return
        }
        if !self.isVideoCallInitiate{
            self.showAlert(msg: StringConstants.You_can_only_start_video_call_candidate_replied_message)
            return
        }
        if self.checkCameraAccess() && self.checkAudioPermission(){
            self.videoCallButton.isUserInteractionEnabled = false
            //                self.notifyVideoCallApi(callType: "calling")
        }
    }
    
    @IBAction func deleteButtonTapped(_ sender: UIButton) {
        self.deleteChat()
    }
    
    @IBAction func sendButtonTapped(_ sender: UIButton) {
        //        self.observeIsShortList()
//        self.view.endEditing(true)
        sender.isSelected = false
//        ChatHelper.getCandInfoCoresRec(candUserId: self.chatMember.userId) { (detail) in
//            if let isReport = detail["isReport"] as? Bool, isReport{
//                //                    self.showAlert(msg: StringConstants.You_reported_the_recruiter_So_you_cannot_initiate_message)
//                commingFrom = .report
//                self.showPopup(desText: StringConstants.You_reported_the_recruiter_So_you_cannot_initiate_message)
//                return
//            }
//        }
        if self.alertType == .report{
            return
        }
        guard self.chatType != .none else {
            return
        }
        let text = self.messageTextView.text!.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        //                self.chatListData.append(ChatLocalDB(value: chatDBDetail))
        //                for item in self.chatLocalDB{
        //                }
        self.isSendMessage = true
        guard !text.isEmpty else { return }
        if text == StringConstants.Type_message{
            return
        }
        self.sendTextMessage(text)
        if !self.organisedData.isEmpty{
            self.scrollToLastMessageAfterSending()
        }
        if self.chatRoom != nil {
            self.updateSelfTypingStatus(false, self.chatRoom.chatRoomId)
        }
        self.messageTextView.text = ""
        
    }
}

//    MARK:-   Functions
//    ========================================
extension PersonalChatVC {
    
    
    func initialSetup() {
        backButton.setImage(#imageLiteral(resourceName: "icForgotPasswordBack"), for: .normal)
        userImageView.roundCorners()
        setFontsAndColors()
        
        if !DBManager.sharedInstance.getDataFromDB().isEmpty{
            for item in DBManager.sharedInstance.getDataFromDB(){
                self.chatListDB.append(item)
                _ = self.chatListDB.map{ value in
                    _ = value.userList.map{ user in
                        self.userListDB.messageList = user.messageList
                    }
                }
            }
        }
        messageTextView.autocapitalizationType = .sentences
        messageTextView.delegate = self
        self.videoCallButton.roundCorners()
        
        mainTableView.dataSource = self
        mainTableView.delegate = self
        self.navigationBarView.setShadow()
//        self.refeshControl.tintColor = UIColor.blue
//        self.refeshControl.attributedTitle = NSAttributedString(string: "Pull_to_load_previous_messages", attributes: [NSAttributedStringKey.font : AppFonts.Poppins_Medium.withSize(13), NSAttributedStringKey.foregroundColor: AppColors.white])
//        self.mainTableView.refreshControl = self.refeshControl
//        self.refeshControl.addTarget(self, action: #selector(self.getPreviousMessages), for: .valueChanged)
        keyBoardAppearClosure = { (height) in
            self.textFieldContainerViewBottom.constant = height
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
                if self.totalMessage > 0{
                    self.scrollToLastMessage()
                    //self.mainTableView.scrollToRow(at: [self.mainTableView.numberOfSections - 1,self.totalMessage - 1], at: .bottom, animated: true)
                }
            })
        }
        
        keyBoardDisappearClosure = {
            self.textFieldContainerViewBottom.constant = 0
            UIView.animate(withDuration: 0.3, animations: {
                self.view.layoutIfNeeded()
            })
        }
        self.mainTableView.estimatedRowHeight = 60
        self.mainTableView.rowHeight = UITableViewAutomaticDimension
        self.containerViewHeight.constant = 35
        self.mainTableView.keyboardDismissMode = .onDrag
        
        if self.chatType == .none{
            self.checkChatType()
        }
        
        self.messageTextView.text = ""
        
        switch self.user.user_Type {
        case .candidate:
            self.isStarConsumed = true
            self.videoCallButton.isHidden = true
            self.nameLabel.text = "\(self.chatMember.firstName) \(self.chatMember.lastName)"
            
        case .recuriter:
            self.isStarConsumed = true
            if let firstChar = self.chatMember.lastName.first {
                self.nameLabel.text = "\(self.chatMember.firstName) \(firstChar)."
            }
        case .none:
            return
        }
        
        let url = URL(string: self.chatMember.userImage)
        self.userImageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "ic_placeholder_22"), options: [], completed: nil)
        
        let longPressGR = UILongPressGestureRecognizer(target: self, action: #selector(longPressHandler))
        longPressGR.minimumPressDuration = 0.3 // how long before menu pops up
        longPressGR.cancelsTouchesInView = true
        mainTableView.addGestureRecognizer(longPressGR)
        
        self.typingContainerView.isHidden = true
        self.deleteButton.isEnabled = false
        
        self.recieverImage.isHidden = true
        
        bubbleView.isHidden = true
        self.setupTextAndColor()
        
        let totalStar = AppUserDefaults.value(forKey: .freeStars).intValue + AppUserDefaults.value(forKey: .stars).intValue
        self.totalStarLabel.text = "\(totalStar)"
        self.consumeStarAlertView.isHidden = true
        self.shortlistView.isHidden = true
        self.checkStarAndShortListForCandidate()
        /**/
    }
    
    func getActiveJobListApi(){
//        self.mainTableView.isHidden = true
        self.consumeStarAlertView.isHidden = true
        self.shortlistView.isHidden = true
        self.recieverImage.isHidden = true
        self.textFieldContainerView.isHidden = true
        self.typingContainerView.isHidden = true
        
        WebServices.activeJobsApi(parameters: ["candidateId": self.chatMember.userId], loader: true, success: { (json) in
            var data = [CandidateAppliedJobInfo]()
            let jobs = json[ApiKeys.data.rawValue].arrayValue
            for job in jobs {
                data.append(CandidateAppliedJobInfo(dict: job))
            }
            self.activeJobList = data
            self.mainTableView.isHidden = false
            
            self.consumeStarAlertView.isHidden = false
            AppUserDefaults.save(value: json["freeStars"].intValue, forKey: .freeStars)
            AppUserDefaults.save(value: json["stars"].intValue, forKey: .stars)
            self.totalStarLabel.text = "\(AppUserDefaults.value(forKey: .freeStars).intValue + AppUserDefaults.value(forKey: .stars).intValue)"
            self.textFieldContainerView.isHidden = false
            
            let isShortList = json["isShortlisted"].intValue
            if isShortList == 1{
                self.isShortlisted = true
            }else{
                self.isShortlisted = false
            }
            self.isStarDeducted = json["isStarDeducted"].boolValue
            if json["isReported"].boolValue{
                 commingFrom = .report
                self.isReported = true
               
            }
            if json["isStarDeducted"].boolValue && json["isShortlisted"].boolValue{
                self.consumeStarAlertView.isHidden = true
                self.shortlistView.isHidden = true
            }
            
            if !self.isShortlisted{
                self.shortlistView.isHidden = false
            }
            
        }) { (error) in
            CommonClass.showToast(msg: error.localizedDescription)
        }
    }
    
    func candidateShortlisted(loader: Bool, jobId: String, categoryId: String ) {
        
        let param = [ ApiKeys.userId.rawValue : self.chatMember.userId, ApiKeys.jobId.rawValue :jobId, "categoryId": categoryId ] as [String : Any]
        
        WebServices.candidateShortlisted(parameters: param, loader: loader, success: { (json) in
            if json["code"].intValue == error_codes.success{

                let msg = json["message"].stringValue
                self.isShortlisted = true
                self.shortlistView.isHidden = true
                if self.isStarDeducted{
                    self.consumeStarAlertView.isHidden = true
                }
                CommonClass.showToast(msg: msg)
            }else{
                
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
            
        }) { (error: Error) in
            CommonClass.showToast(msg: error.localizedDescription )
        }
    }
    
    
    func checkStarAndShortListForCandidate(){
        
        ChatHelper.getCandInfoCoresRec(candUserId: self.chatMember.userId) { (detail) in
            if AppUserDefaults.value(forKey: .userType).stringValue == UserType.recuriter.rawValue{
                if !detail.isEmpty{
                    
                    if let isShortlisted = detail["isShortListed"] as? Bool, !isShortlisted{
                        commingFrom = .shortlist
                        self.shortlistView.isHidden = true
                        //                            self.showPopup(desText: StringConstants.Shortlist_candidate_Before_message)
//                        self.consumeStarAlertView.isHidden = false
                    }
                    else if detail["isStarDeducted"] != nil{
                        
                        if let isStarDeduct = detail["isStarDeducted"] as? Bool, !isStarDeduct{
                            commingFrom = .starDeduct
                            
                            //                                self.showPopup(desText: StringConstants.Consume_Star_Before_message)
                            
//                            self.consumeStarAlertView.isHidden = false
                        }else{
                            self.consumeStarAlertView.isHidden = true
                            
                        }
                    }else{
                        commingFrom = .starDeduct
//                        self.consumeStarAlertView.isHidden = false
                        //                            self.showPopup(desText: StringConstants.Consume_Star_Before_message)
                    }
                }else{
                    commingFrom = .shortlist
                    //                    self.showPopup(desText: StringConstants.Shortlist_candidate_Before_message)
//                    self.consumeStarAlertView.isHidden = false
                    
                    return
                }
            }else{
                if let isReport = detail["isReport"] as? Bool, isReport{
//                    self.showAlert(msg: StringConstants.You_reported_the_recruiter_So_you_cannot_initiate_message)
                    commingFrom = .report
                    self.showPopup(desText: StringConstants.You_reported_the_recruiter_So_you_cannot_initiate_message)
                    
                    return
                }
            }
        }
    }
    
    //      func observeIsShortList(){
    //
    //        databaseReference.child("user").child(self.chatMember.userId).observe(.value) { [weak self](snapshot) in
    //
    ////            guard let _self = self else { return }
    //            if let badgeCount = snapshot.value as? Int{
    //            }
    //        }
    //    }
    func setupTextAndColor() {
        self.messageTextView.text = StringConstants.Type_message
        self.messageTextView.textColor = AppColors.gray152
    }
    
    func showPopup(desText: String){
        let sceen = ResetPasswordPopupVC.instantiate(fromAppStoryboard: .Main)
        sceen.desText = desText
        sceen.delegate = self
        self.view.addSubview(sceen.view)
        self.add(childViewController: sceen)
    }
    
    func starDeductApi(){
        
        WebServices.deductStars(parameters: ["userId":self.chatMember.userId], loader: true, success: { (json) in
            
            if let errorCode = json["code"].int, errorCode == error_codes.success {

                self.consumeStarAlertView.isHidden = true
                self.shortlistView.isHidden = true
                self.isStarDeducted = true
                AppUserDefaults.save(value: json["totalStars"].intValue, forKey: .stars)
                if let freeStars = json["freeStars"].int{
                    AppUserDefaults.save(value: freeStars, forKey: .freeStars)
                }
                let msg = json["message"].stringValue
                UIApplication.shared.keyWindow?.makeToast(message: msg, duration: 2, position: "center" as AnyObject)
            } else if let errorCode = json["code"].int, errorCode == error_codes.not_enough_star  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
                let package = StarPackage.instantiate(fromAppStoryboard: .Star)
                package.isFromChat = true
                package.delegate = self
                self.navigationController?.pushViewController(package, animated: true)
                
            } else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }) { (error) in
            self.showAlert(msg: error.localizedDescription)
            return
        }
    }
    
    func notifyVideoCallApi(callType : String){
        
        let userData = User(json: (AppUserDefaults.value(forKey: .userData)))
        
        
        var params = JSONDictionary()
        params["senderName"] = userData.first_name
        params["receiverName"] = self.chatMember.firstName
        params["senderId"] = userData.user_id ?? ""
        params["receiverId"] = self.chatMember.userId
        params["deviceId"] = self.chatMember.deviceToken
        params["deviceToken"] = self.chatMember.deviceToken
        params["deviceType"] = Device_Type.android.rawValue
        params["senderDeviceToken"] = DeviceDetail.deviceToken
        params["type"] = callType
        params["senderImage"] = userData.imageUrl
        
        WebServices.notifyVideoCall(parameters: params, loader: true, success: { (json) in
            //            let json: JSONDictionary = ["type": "leave", "otherName": User.getUserModel().user_id ?? "" , "name":self.chatMember.userId]
            //                        CommonClass.writeToSocket(json)
           
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                
                if AppUserDefaults.value(forKey: .userType).stringValue == UserType.recuriter.rawValue{
                    
                    DispatchQueue.main.async {
                        let videoChatScene = RTCVideoChatVC.instantiate(fromAppStoryboard: .settingsAndChat)
                        videoChatScene.caller = Caller(json: json["data"])!
                        videoChatScene.isOpenFromCallKit = false
                        //videoChatScene.callType = .send
                        videoChatScene.fromPush = false
                        let chatNavVC = UINavigationController(rootViewController: videoChatScene)
                        chatNavVC.isNavigationBarHidden = false
                        chatNavVC.modalPresentationStyle = .overCurrentContext
                        
                        chatNavVC.modalTransitionStyle = .flipHorizontal
                        UIApplication.shared.beginIgnoringInteractionEvents()
                        UIApplication.shared.endIgnoringInteractionEvents()
                        UIApplication.shared.keyWindow?.rootViewController?.present(chatNavVC, animated: true, completion: nil)

                    }
                }
            } else if let errorCode = json["code"].int, errorCode == error_codes.loginOtherDevice  {
                CommonClass.userNotExist()
                
            } else if let errorCode = json["code"].int, errorCode == error_codes.invalidToken  {
                CommonClass.showToast(msg: json["message"].stringValue)
            }
            else  {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
           
             self.videoCallButton.isUserInteractionEnabled = true
//            sharedAppDelegate.moveToVideoChatScene(with: Caller(json: data["data"])!,fromPush : false, fromCallKit: false)
        }) { (error) in
            self.videoCallButton.isUserInteractionEnabled = true
            self.showAlert(msg: error.localizedDescription)
            return
        }
    }
    
    
    func setFontsAndColors() {
        
        textFieldContainerView.borderColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 0.7508294092)
        textFieldContainerView.borderWidth = 0.5
        
        nameLabel.font = AppFonts.Poppins_Medium.withSize(15)
        nameLabel.textColor = AppColors.black26
        
        messageTextView.font = AppFonts.Poppins_Light.withSize(15)
        messageTextView.textColor = AppColors.black46
    }
    
    func refreshMessageData(_ message: ChatMessage){
        
        guard let currentUserId = self.user.user_id else { return }
        if (currentUserId != message.senderId){
            self.isVideoCallInitiate = true
        }else{
            self.isExpired = true
        }
        if (currentUserId != message.senderId) && message.isBlock{
            return
        }else if message.isDeleted{
            return
        }
        
        self.organise(message: message)
        self.scrollToLastMessage()
    }
    
    @objc func longPressHandler(sender: UILongPressGestureRecognizer) {
//        sender.view?.becomeFirstResponder()
        
        if sender.state == .began {
            let touchPoint = sender.location(in: mainTableView)
            if let indexPath = mainTableView.indexPathForRow(at: touchPoint) {
                showOptions(forIndex: indexPath)
            }
        }
    }
    
    func showOptions(forIndex index : IndexPath) {
        func getFrameFromCell(cell : UITableViewCell) -> (frame : CGRect, shouldShowDelete : Bool)? {
            if let cell = cell as? PersonalChatUserCell {
                cell.messageTextView.backgroundColor = .clear
                return (cell.messageBackgroundView.frame, true)
            }
            if let cell = cell as? PersonalChatOtherCell {
                cell.messageTextView.backgroundColor = .clear
                return (cell.messageBackgroundView.frame, false)
            }
            return nil
        }
        
        if let chatMessage = self.organisedData[self.organisedKeys[index.section]]{
            let message =  chatMessage[index.row]
            if message.isDeleted {
                return
            }
        }
        
        guard let cell = mainTableView.cellForRow(at: index) else { return }
        guard let result = getFrameFromCell(cell: cell) else { return }

        selectedIndex = index
        // Set up the shared UIMenuController
        let saveMenuItem = UIMenuItem(title: StringConstants.copy, action: #selector(copyTapped))
        let deleteMenuItem = UIMenuItem(title: StringConstants.delete, action: #selector(deleteTapped))
        if result.shouldShowDelete {
            UIMenuController.shared.menuItems = [saveMenuItem, deleteMenuItem]
        } else {
            UIMenuController.shared.menuItems = [saveMenuItem]
        }
        UIMenuController.shared.setTargetRect(result.frame, in: cell)
        UIMenuController.shared.setMenuVisible(true, animated: true)
    }
    
    @objc func copyTapped() {
        mainTableView.resignFirstResponder()
        if let selected = selectedIndex {
            if let chatMessage = self.organisedData[self.organisedKeys[selected.section]]{
                let message =  chatMessage[selected.row]
                UIPasteboard.general.string = message.message
            }
        }
    }
    
    @objc func deleteTapped() {
        mainTableView.resignFirstResponder()
        deleteSingleMessage()
    }
    
    func deleteSingleMessage(){
        guard let selectedIndex = selectedIndex else{
            return
        }
        
        if let chatMessage = self.organisedData[self.organisedKeys[selectedIndex.section]]{
            let message =  chatMessage[selectedIndex.row]
            var unreadCount = 0
            databaseReference.child(ChatEnum.Message.root.rawValue).child(message.roomId).child(message.messageId).child(ChatEnum.Message.isDelete.rawValue).setValue(true)
            
            if selectedIndex.section == self.organisedKeys.count - 1 && selectedIndex.row == chatMessage.count - 1 {
                
                if selectedIndex.section == 0 && selectedIndex.row == 0{
                    deleteChat(otherUserId: chatMember.userId, roomId: chatRoom.chatRoomId)
                }else if selectedIndex.section != 0 && selectedIndex.row == 0{
                    
                    if let lastMessage = self.organisedData[self.organisedKeys[selectedIndex.section - 1]]?.last{
                        ChatHelper.setLastMessage(lastMessage)
                    }
                    
                } else if selectedIndex.section == 0 && selectedIndex.row != 0{
                    if let lastMessage = self.organisedData[self.organisedKeys[selectedIndex.section]]?[selectedIndex.row - 1]{
                        ChatHelper.setLastMessage(lastMessage)
                    }
                } else if selectedIndex.section != 0 && selectedIndex.row != 0{
                    if let lastMessage = self.organisedData[self.organisedKeys[selectedIndex.section]]?[selectedIndex.row - 1]{
                        ChatHelper.setLastMessage(lastMessage)
                    }
                } else{
                    print_debug("Not found")
                }
            }
            ChatHelper.getUnreadMessagesCount(userId: chatMember.userId, roomId: chatRoom.chatRoomId) { (count) in
                
                unreadCount = count
            }
            
            ChatHelper.getBadgeCount(userId: chatMember.userId) { (count) in
                
                let unreadRef = "/\(ChatEnum.Root.unreadMessage.rawValue)/\(self.chatRoom.chatRoomId)/\(self.chatMember.userId)"
                let badgeCount = "/badgeCount/\(self.chatMember.userId)"
                let dict : JSONDictionary = ["\(unreadRef)":unreadCount == 0 ? 0 : unreadCount - 1, "\(badgeCount)": count-1]
                databaseReference.updateChildValues(dict)
            }
            
            self.organisedData[self.organisedKeys[selectedIndex.section]]?[selectedIndex.row].isDeleted = true
            self.organisedData[self.organisedKeys[selectedIndex.section]]?.remove(at: selectedIndex.row)
            if !DBManager.sharedInstance.getDataFromDB().isEmpty , let messageList = DBManager.sharedInstance.getChatUserForMessageId(id: message.roomId, user_id: User.getUserModel().user_id ?? "", message_Id: message.messageId), !messageList.messageId.isEmpty{
                //                let uiRealm = try! Realm()
                //                do {
                //                    try uiRealm.wr {
                DBManager.sharedInstance.deleteMessageFromDB(object: messageList)
                //                    }
                //                }catch let error {
                //                    print(error)
                //                }
            }
            self.mainTableView.reloadData()//Rows(at: [selectedIndex], with: .none)
        }
    }
    
    func organisePrevious(messages: [ChatMessage]){
        
        for message in messages{
            let key = dateManager.getNotationFor(timestamp: message.timestamp)
            
            if !self.organisedKeys.contains(key){
                if self.organisedKeys.isEmpty{
                    self.organisedKeys.append(key)
                }else{
                    self.organisedKeys.insert(key, at: 0)
                }
            }
            
            if var arr = self.organisedData[key]{
                
                if let index = arr.index(of: message){
                    arr.remove(at: index)
                }
                
                var newArr: [ChatMessage] = [message]
                newArr.append(contentsOf: arr)
                self.organisedData[key] = newArr
            }else{
                self.organisedData[key] = [message]
            }
        }
        self.mainTableView.reloadData()
    }
    
    func organise(message: ChatMessage){
        
        let key = dateManager.getNotationFor(timestamp: message.timestamp)
        
        if !self.organisedKeys.contains(key){
            self.organisedKeys.append(key)
        }
        
        if var arr = self.organisedData[key]{
            
            if let index = arr.index(of: message){
                arr.remove(at: index)
            }
            
            let index = arr.insertionIndexOf(elem: message, isOrderedBefore: { (m1, m2) -> Bool in
                return m1.timestamp < m2.timestamp
            })
            
            arr.insert(message, at: index)
            
            self.organisedData[key] = arr
        }else{
            self.organisedData[key] = [message]
        }
        
        self.deleteButton.isEnabled = true
        self.mainTableView.reloadData()
    }
    
    func scrollToLastMessage(){
        
        if let visibleIndexPath = self.mainTableView.indexPathsForVisibleRows{
            
            let section = self.organisedKeys.count
            if let count = self.organisedData[self.organisedKeys[section - 1]]?.count{
                
                for index in visibleIndexPath{
                    if index.row >= count - 4 && index.row <= count - 1{
                        self.mainTableView.scrollToRow(at: IndexPath(row: count - 1, section: section - 1), at: .bottom, animated: false)
                        self.hideLoder()
                        break
                    }
                }
            }
        }
    }
    
    func scrollToLastMessageAfterSending(){
        
        let section = self.organisedKeys.count
        if let row = self.organisedData["Today"]?.count{
            self.mainTableView.scrollToRow(at: IndexPath(row: row - 1, section: section - 1), at: .none, animated: false)
            self.containerViewHeight.constant = 35
            
        }
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        
        let sec: Int = totalSeconds % 60
        let min: Int = (totalSeconds / 60) % 60
        
        return String(format: "%02d:%02d", min, sec)
    }
}

//    MARK:- UITableViewDataSource
//    ========================================
extension PersonalChatVC : UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return self.organisedKeys.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let arr = self.organisedData[self.organisedKeys[section]]{
            self.totalMessage = arr.count
            return arr.count
        }else{
            fatalError("Error \(#line) \(#function)")
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let userId = self.user.user_id else {
            
            fatalError("User id is not available")
        }
        
        if let chatMessage = self.organisedData[self.organisedKeys[indexPath.section]]{
            
            let message =  chatMessage[indexPath.row]
            
            if message.type == .call{
                let cell = tableView.dequeueReusableCell(withIdentifier: "CallAlertCell", for: indexPath) as! CallAlertCell
                
                if let tInt = Double(message.message){
                    
                    var secs = Int(tInt/1000)
                    if tInt < 0{
                        secs = Int(tInt)
                    }
                    
                    if secs == 0{
                        cell.messageAlert.text = StringConstants.Missed_Call
                    }else if secs == -1{//When Recruiter decline the call.
                        cell.messageAlert.text = StringConstants.Cancelled_Call
                    }else{
                        cell.messageAlert.text = "\(StringConstants.Call) (\(self.timeFormatted(secs)))"
                    }
                    
                }else{
                    cell.messageAlert.text = StringConstants.Missed_Call
                }
                cell.timeDuration.text = dateManager.elapsed(from: message.timestamp,  isChatTime: true)
                return cell
            }else if message.senderId == userId{
                let cell = mainTableView.dequeueReusableCell(withIdentifier: PersonalChatUserCell.identifier, for: indexPath) as! PersonalChatUserCell
                let attrStrng = NSAttributedString(string: message.message)
                cell.messageTextView.linkTextAttributes = [NSAttributedStringKey.underlineStyle.rawValue : NSUnderlineStyle.styleSingle.rawValue]
                cell.messageTextView.attributedText = attrStrng
                cell.messageTextView.delegate = self
                cell.messageTextView.backgroundColor = .clear

                cell.timeLabel.text = dateManager.elapsed(from: message.timestamp,  isChatTime: true)
                
                cell.timeLabel.isHidden = !self.shouldShowTime(forRow: indexPath.row, messages: chatMessage)
                
                if message.status == .sent{
                    
                    cell.tickImageView.image = #imageLiteral(resourceName: "icChatUnread")
                }else{
                    
                    cell.tickImageView.image = #imageLiteral(resourceName: "icChatRead")
                }
                
                cell.tickImageView.isHidden = !self.shouldShowTime(forRow: indexPath.row, messages: chatMessage)
                if self.shouldShowTime(forRow: indexPath.row, messages: chatMessage) == false{
                    
                    cell.tickStackViewHight.constant = 0
                }else{
                    cell.tickStackViewHight.constant = 12
                }
                if message.isDeleted {
                    cell.messageTextView.text = StringConstants.This_message_has_been_deleted
                }
                return cell
                
            }else{
                let cell = mainTableView.dequeueReusableCell(withIdentifier: PersonalChatOtherCell.identifier, for: indexPath) as! PersonalChatOtherCell
                let attrStrng = NSAttributedString(string: message.message)
                cell.messageTextView.linkTextAttributes = [NSAttributedStringKey.underlineStyle.rawValue : NSUnderlineStyle.styleSingle.rawValue]
                cell.messageTextView.attributedText = attrStrng
                cell.messageTextView.delegate = self
                cell.timeLabel.text = dateManager.elapsed(from: message.timestamp,  isChatTime: true)
                let url = URL(string: self.chatMember.userImage)
                cell.userImageView.sd_setImage(with: url, placeholderImage: #imageLiteral(resourceName: "ic_placeholder_22"), options: [], completed: nil)
                cell.profileDetailBtn.addTarget(self, action: #selector(self.profileDetailBtnTap(_:)), for: .touchUpInside)
                
                cell.setNeedsDisplay()
                if message.isDeleted {
                    cell.messageTextView.text = StringConstants.This_message_has_been_deleted
                }
                
                cell.timeLabel.isHidden = !self.shouldShowTime(forRow: indexPath.row, messages: chatMessage)
                return cell
            }
            
            
        }else{
            return UITableViewCell()
        }

    }
    
    func shouldShowTime(forRow row: Int, messages: [ChatMessage])->Bool{
        let count = messages.count
        
        if count == 1 || count == 0 || (row + 1) == count{
            return true
        }else{
            if messages[row].senderId == messages[row + 1].senderId{
                let time1 = Date(timeIntervalSince1970: messages[row].timestamp/1000)
                let time2 = Date(timeIntervalSince1970: messages[row + 1].timestamp/1000)
                
                return !(abs(time1.minutesFrom(time2)) <= 5)
                
            }else{
                return true
            }
        }
    }
}

//    MARK:- TableViewDelegate
//    ========================================
extension PersonalChatVC: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: 45)
        let view = UIView(frame: frame)
        
        let label = UILabel(frame: frame)
        
        view.backgroundColor = AppColors.white
        
        view.addSubview(label)
        label.font = AppFonts.Poppins_SemiBold.withSize(16)
        label.textAlignment = .center
        label.textColor = UIColor.black
        label.text = self.organisedKeys[section]
        
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 45
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let curUserStatus = self.user.user_id else { return }
        let row = indexPath.row
        
        let key = self.organisedKeys[indexPath.section]
        guard let message = self.organisedData[key]?[row] else { return }
        
        if message.status == .sent && curUserStatus != message.senderId{
            
            self.organisedData[key]?[row].status = .read
            self.readOtherUserMessage(messageId: message.messageId, roomId: message.roomId)
        }
    }
}


//    MARK:- TextView delegate
//    ========================================
extension PersonalChatVC: UITextViewDelegate{
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        UIApplication.shared.open(URL)
        return true
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        if !self.organisedData.isEmpty{
            
            self.scrollToLastMessageAfterSending()
        }
        return self.isStarConsumed
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if messageTextView.textColor == AppColors.gray152 {
            messageTextView.text = nil
            messageTextView.textColor = AppColors.black26
        }
//        if self.chatRoom != nil{
//            self.updateSelfTypingStatus(true, self.chatRoom.chatRoomId)
//        }
        self.sendButton.isSelected = true
        textView.scrollToBottom()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        guard chatRoom != nil else { return true }
        self.sendButton.isSelected = true

        if !text.isEmpty{
            self.updateSelfTypingStatus(true, self.chatRoom.chatRoomId)
        }else{
             self.updateSelfTypingStatus(false, self.chatRoom.chatRoomId)
        }
        return true
    }
    
    
    func textViewDidChange(_ textView: UITextView) {
        
        let topBottomPadding: CGFloat = 20.0
        let threeLineTextHeight: CGFloat = 55.0
        
        let minimumTextHeight: CGFloat = 35
        let maximumTextHeight: CGFloat = threeLineTextHeight + topBottomPadding
        
        let textHeight = textView.text.heightWithConstrainedWidth(width: textView.width-10.0, font: textView.font!)
        
        if textHeight > threeLineTextHeight {
            textView.isScrollEnabled = true
            self.containerViewHeight.constant = maximumTextHeight
            
        } else {
            textView.isScrollEnabled = false
            self.containerViewHeight.constant = max(minimumTextHeight, textHeight + topBottomPadding)
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        
        if messageTextView.text.isEmpty {
            messageTextView.text = StringConstants.Type_message
            messageTextView.textColor = AppColors.gray152
        }
        
        if self.chatRoom != nil{
            self.updateSelfTypingStatus(false, self.chatRoom.chatRoomId)
        }
    }
}


//    MARK:- Check camera permission
//    ========================================
extension PersonalChatVC{
    
    func checkCameraAccess() -> Bool{
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .denied:
            if AppUserDefaults.value(forKey: .userType).stringValue == UserType.recuriter.rawValue{
                self.presentCameraSettings(StringConstants.Rec_Check_Camera_Permission)
            }else{
                self.presentCameraSettings(StringConstants.Check_Camera_Permission)
            }
            
            return false
        case .restricted:
            
            return false
        case .authorized:
            
            return true
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { success in
                if success {
                    self.notifyVideoCallApi(callType: "calling")
                } else {
                    let json = ["type": SocketDataType.decline.rawValue, "name": User.getUserModel().user_id ?? "" , "otherName": self.chatMember.userId]
                    CommonClass.writeToSocket(json)
                    self.notifyVideoCallApi(callType: "decline")
                }
            }
            return false
        }
    }
    
    func checkAudioPermission()-> Bool{
        
        switch AVAudioSession.sharedInstance().recordPermission() {
        case AVAudioSessionRecordPermission.granted:
            self.notifyVideoCallApi(callType: "calling")

            return true
        case AVAudioSessionRecordPermission.denied:
            
             if AppUserDefaults.value(forKey: .userType).stringValue == UserType.recuriter.rawValue{
                 self.presentCameraSettings(StringConstants.Rec_Check_microphone_Permission)
             }else{
                 self.presentCameraSettings(StringConstants.Check_microphone_Permission)
            }
           
            return false
        case AVAudioSessionRecordPermission.undetermined:

            AVAudioSession.sharedInstance().requestRecordPermission({ (granted) in
                // Handle granted
                if granted{

                    if AppUserDefaults.value(forKey: .userType).stringValue == UserType.recuriter.rawValue{
                        self.presentCameraSettings(StringConstants.Rec_Check_microphone_Permission)
                    }else{
                        self.presentCameraSettings(StringConstants.Check_microphone_Permission)
                    }
                    
                }else{

                    let json = ["type": SocketDataType.decline.rawValue, "name": User.getUserModel().user_id ?? "" , "otherName": self.chatMember.userId]
                    CommonClass.writeToSocket(json)
                    self.notifyVideoCallApi(callType: "decline")
                }
                
            })
            return false
        }
    }
    
    func presentCameraSettings(_ message: String) {
        let alertController = UIAlertController(title: "",
                                                message: message,
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: StringConstants.Cancel, style: .default){ _ in
            if AppUserDefaults.value(forKey: .userType).stringValue == UserType.candidate.rawValue{
                self.notifyVideoCallApi(callType: "decline")
                let json = ["type": SocketDataType.decline.rawValue, "name": User.getUserModel().user_id ?? "" , "otherName": self.chatMember.userId]
                CommonClass.writeToSocket(json)
            }
        })
        alertController.addAction(UIAlertAction(title: StringConstants.Settings, style: .cancel) { _ in
            if let url = URL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: { _ in
                    // Handle
                })
            }
        })
        
        self.present(alertController, animated: true, completion: nil)
    }
}


extension PersonalChatVC : AlertPopUpDelegate, ShouldOpenStatusPopup,UpdateTransactionListDelegate,ShortListedApplicantsPopup {
    
    func didTapAffirmativeButton() {
        deleteChat(otherUserId: chatMember.userId, roomId: chatRoom.chatRoomId)
    }
    func didTapNegativeButton(){
        
    }
    
    func updateTransactionListDelegate(){
        
        self.starDeductApi()
    }
    
    func deleteChat(otherUserId : String,roomId: String) {
        
        guard !otherUserId.isEmpty,!roomId.isEmpty,!ChatEnum.Root.roomInfo.rawValue.isEmpty else { return }
        
        guard let userID = User.getUserModel().user_id else { return  }
        databaseReference.child(ChatEnum.Root.roomInfo.rawValue).child(roomId).child(RoomInfo.chatRoomMembers.rawValue).child(userID).child(RoomInfo.memberDelete.rawValue).setValue(ServerValue.timestamp())
        
        databaseReference.child(ChatEnum.Root.inbox.rawValue).child(userID).child(otherUserId).removeValue()
       
        self.delegate?.chatDeleted(chatRoomId: roomId)
        self.navigationController?.popViewController(animated: true)
    }
    
    func presentShortlistedPopup(jobApplyId: String, categoryId: String) {
        
        if commingFrom == .shortlist{
            //self.openProfileDetail()
        }else if commingFrom == .starDeduct{
            self.starDeductApi()
        }else if commingFrom == .report{
//            self.pop()
        }
    }
    
    func onTapAffermation(index: Int, applyJobId: String,  categoryId: String) {
        //        self.categoryId = categoryId
        self.candidateShortlisted(loader: true, jobId: applyJobId, categoryId: categoryId)
        //        self.pop()
    }
    
    func onTapCancel() {
        self.setUnreadCountZero()
//        self.pop()
    }
}
