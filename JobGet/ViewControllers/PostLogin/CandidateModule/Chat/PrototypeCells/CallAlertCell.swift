//
//  CallDetailCell.swift
//  JobGet
//
//  Created by Admin on 08/08/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class CallAlertCell: UITableViewCell {
    
    @IBOutlet weak var messageAlert: UILabel!
    @IBOutlet weak var timeDuration: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
    }
    
}
