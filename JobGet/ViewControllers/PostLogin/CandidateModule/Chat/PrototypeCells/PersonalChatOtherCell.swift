//
//  PersonalChatOtherCell.swift
//  JobGet
//
//  Created by appinventiv on 08/05/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import Foundation
import UIKit

class PersonalChatOtherCell : UITableViewCell {
    
    //MARK:-
    //MARK:- IBOutlets
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var messageBackgroundView: UIView!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var tickImageView: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var profileDetailBtn: UIButton!
    
    //MARK:-
    //MARK:- Properties
    static var identifier : String {
        return String(describing: self)
    }
    
    //MARK:-
    //MARK:- View Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        
        messageTextView.isEditable = false
        
        layoutIfNeeded()
        userImageView.roundCorners()
        setupFontsAndColors()
        messageBackgroundView.setCorner(cornerRadius: 10, clip: true)
        tickImageView.isHidden = true
    }
    
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        addTriangleOnLeftSide()
    }
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    
    private func setupFontsAndColors() {
        
        
        messageBackgroundView.backgroundColor = AppColors.gray227
        
        messageTextView.font = AppFonts.Poppins_Regular.withSize(14)
        messageTextView.textColor  = AppColors.black46
        
        timeLabel.font = AppFonts.Poppins_Regular.withSize(11.5)
        timeLabel.textColor = AppColors.gray152
    }
    
    private func addTriangleOnLeftSide() {
        let frame = messageBackgroundView.frame
        let point1 = CGPoint(x: frame.minX + 10, y: frame.minY)
        let point2 = CGPoint(x: frame.minX - 9, y: frame.minY)
        let point3 = CGPoint(x: frame.minX + 10, y: frame.minY + 20)
        
        let trianglePath = UIBezierPath()
        trianglePath.move(to: point1)
        trianglePath.addLine(to: point2)
        trianglePath.addLine(to: point3)
        trianglePath.close()
        
        let triangleLayer = CustomTriangleLayer()
        triangleLayer.path = trianglePath.cgPath
        triangleLayer.fillColor = AppColors.gray227.cgColor
        triangleLayer.strokeColor = nil
        self.layer.addSublayer(triangleLayer)
        
    }
}
