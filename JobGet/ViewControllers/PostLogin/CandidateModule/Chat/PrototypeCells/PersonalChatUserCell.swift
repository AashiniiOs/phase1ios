//
//  PersonalChatUserCell.swift
//  JobGet
//
//  Created by appinventiv on 08/05/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import Foundation
import UIKit

class PersonalChatUserCell : UITableViewCell {
    
    //MARK:-
    //MARK:- IBOutlets
    @IBOutlet weak var messageBackgroundView: UIView!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var tickImageView: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var tickStackViewHight: NSLayoutConstraint!
    
    //MARK:-
    //MARK:- Properties
    static var identifier : String {
        return String(describing: self)
    }
    
    //MARK:-
    //MARK:- View Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
//        messageTextView.isSelectable = true
//        messageTextView.isEditable = false
        layoutIfNeeded()
        setupFontsAndColors()
        messageBackgroundView.setCorner(cornerRadius: 10, clip: true)
//        messageTextView.dataDetectorTypes = .init(rawValue: 0)
        //  addTriangleOnRightSide()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    private func setupFontsAndColors() {
        
        messageBackgroundView.backgroundColor = AppColors.themeBlueColor
        
        messageTextView.font = AppFonts.Poppins_Regular.withSize(14)
        messageTextView.textColor  = AppColors.white
        
        timeLabel.font = AppFonts.Poppins_Regular.withSize(11.5)
        timeLabel.textColor = AppColors.gray152
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        addTriangleOnRightSide()
    }
    
    private func addTriangleOnRightSide() {
        let frame = messageBackgroundView.frame
        let point1 = CGPoint(x: frame.maxX - 10 , y: frame.minY)
        let point2 = CGPoint(x: frame.maxX + 5, y: frame.minY)
        let point3 = CGPoint(x: frame.maxX - 10, y: frame.minY + 20)
        
        let trianglePath = UIBezierPath()
        trianglePath.move(to: point1)
        trianglePath.addLine(to: point2)
        trianglePath.addLine(to: point3)
        trianglePath.close()
        
        let triangleLayer = CAShapeLayer()
        triangleLayer.path = trianglePath.cgPath
        triangleLayer.fillColor = AppColors.themeBlueColor.cgColor
        triangleLayer.strokeColor = nil
        
        self.layer.addSublayer(triangleLayer)
        
    }
}
