//
//  RecentChatVC.swift
//  JobGet
//
//  Created by appinventiv on 16/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import SwiftyJSON
import Firebase
import DZNEmptyDataSet
import RealmSwift

class RecentChatVC: UIViewController {
    
    //    MARK:- IBOutlets
    //    ============================================
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var chatTableView: UITableView!
    
    
    //    MARK:- Properties
    //    ============================================
    private var unreadCountHandle: [String:UInt] = [:]
    private var databaseHandle: [String:UInt] = [:]
    private var inboxList: [Inbox] = []
    private var userDetails: [String:Any] = [:]
    private var groupDetailHandle: [String:UInt] = [:]
    private var groupDetails: [String:Any] = [:]
    private var onlineStatusHandle: [String:UInt] = [:]
    private var selectedIndex : Int?
    var roomId : String?
    var fromPush = false
    var userListDB = ChatLocalDB()
    var searchUserList = ChatUserListDB()
    var chatMember : ChatMember?
    var isLoadingFirstTime = true
    //    MARK:- View Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.fromPush{
            if let roomID = self.roomId,!roomID.isEmpty{
                self.fromPush = false
                let chatVc = PersonalChatVC.instantiate(fromAppStoryboard: .Candidate)
                if let member = self.chatMember{
                    chatVc.chatMember = member
                    chatVc.chatType = .old
                    chatVc.chatRoom = ChatRoom(with: JSON(["chatRoomId":roomID]))
                }
                chatVc.isOpenFromProfile = true
                self.navigationController?.pushViewController(chatVc, animated: false)
            }
        }
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: Notification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: Notification.Name.UIKeyboardWillShow, object: nil)
        self.chatTableView.alwaysBounceVertical = true
        self.chatTableView.isScrollEnabled = true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.view.endEditing(true)
        self.chatTableView.endEditing(true)
        self.chatTableView.alwaysBounceVertical = true
        self.chatTableView.isScrollEnabled = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //self.removeFirebaseObservers()
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        print_debug("Memory warning from \(self)")
    }
    
    //    MARK:- IBActions
    
}

//    MARK:- Functions
//    ===========================================
extension RecentChatVC {
    
    private func initialSetup(){
        
        chatTableView.register(RecentChatCell.nib, forCellReuseIdentifier: RecentChatCell.identifier)
        chatTableView.dataSource = self
        chatTableView.delegate = self
        self.chatTableView.emptyDataSetSource = self
        self.chatTableView.emptyDataSetDelegate = self
        chatTableView.separatorStyle = .none
        chatTableView.keyboardDismissMode = .onDrag
        self.searchBar.delegate = self
        if !DBManager.sharedInstance.getDataFromDB().isEmpty, let userList = DBManager.sharedInstance.getChatLocalDbForId(id: User.getUserModel().user_id ?? ""){
            self.userListDB = userList
        }
        self.customiseSearchBar()
        self.observeInbox()
    }
    
    @objc func keyboardWillAppear() {
        //Do something here
        print_debug("keyboardWillAppear")
        let tap = KeyboardTapGesture(target: self, action: #selector(self.removeKeyboard))
        self.chatTableView.addGestureRecognizer(tap)
        self.chatTableView.isScrollEnabled = true
        self.chatTableView.alwaysBounceVertical = true
    }
    
    @objc func keyboardWillDisappear() {
        //Do something here
        print_debug("keyboardWillDisappear")
        if let gesture = chatTableView.gestureRecognizers {
            for recognizer in gesture {
                if recognizer is KeyboardTapGesture {
                    chatTableView.removeGestureRecognizer(recognizer)
                }
            }
        }
        self.chatTableView.isScrollEnabled = true
        self.chatTableView.alwaysBounceVertical = true
    }
    
    @objc func removeKeyboard(){
        
        self.view.endEditing(true)
        self.chatTableView.isScrollEnabled = true
        self.chatTableView.alwaysBounceVertical = true
    }
    
    private func handleLastMessages(inbox: Inbox){
        
        if self.inboxList.isEmpty{
            self.inboxList.append(inbox)
        }else if let index = self.inboxList.index(of: inbox){
            self.inboxList[index] = inbox
            self.inboxList = self.sortInboxItem()
        }else{
            self.inboxList.append(inbox)
            self.inboxList = self.sortInboxItem()
        }
        UIView.setAnimationsEnabled(false)
//        self.chatTableView.reloadData()
        if inbox.roomType == .single{
            self.getUserDetails(inbox: inbox)
        }
        self.observeUnread(forRoom: inbox.roomId)
    }
    
    func upadteLocalDB(room_id: String, chatUserListDB : ChatUserListDB){
        let chatLocalDB  = ChatLocalDB()
        var isGetId = false
        var isUserId = false
//        if self.isSendMessage{
            if let item = DBManager.sharedInstance.getChatLocalDbForId(id: User.getUserModel().user_id ?? "") {
                let uiRealm = try! Realm()
                //                if item.chat_userId == (User.getUserModel().user_id ?? ""){
                isUserId = true
                if let userList = DBManager.sharedInstance.getChatUserForRoomId(id: room_id, user_id: User.getUserModel().user_id ?? ""){
                    isGetId = true
                    for message in chatUserListDB.messageList{
                        do {
                            try uiRealm.write {
                                userList.messageList.append(message)
                            }
                        }catch let error {
                            print(error)
                        }
                    }
                    DBManager.sharedInstance.addData(object: item)
                }else{
                    if !isGetId{
                        do {
                            try uiRealm.write {
                                if !isGetId{
                                    chatUserListDB.room_id = room_id
                                }
                                item.userList.append(chatUserListDB)
                            }
                        }catch let error {
                            print(error)
                        }
                        //                            self.chatLocalDB.userList.append(self.userListDB)
                        DBManager.sharedInstance.addData(object: item)
                    }
                }
            }else{
                if !isUserId{
                    chatLocalDB.chat_userId = (User.getUserModel().user_id ?? "")
                }
                if !isGetId{
                    chatUserListDB.room_id = room_id
                }
                chatLocalDB.userList.append(chatUserListDB)
                DBManager.sharedInstance.addData(object: chatLocalDB)
            }
            if DBManager.sharedInstance.getDataFromDB().isEmpty{
                if !isUserId{
                    chatLocalDB.chat_userId = (User.getUserModel().user_id ?? "")
                }
                if !isGetId{
                    chatUserListDB.room_id = room_id
                }
                chatLocalDB.userList.append(chatUserListDB)
                DBManager.sharedInstance.addData(object: chatLocalDB)
            }
//        }
        AppNetworking.hideLoader()
    }
    
    private func sortInboxItem()->[Inbox]{
        
        let sortedList = self.inboxList.sorted { (item1, item2) -> Bool in
            
            if let time1 = item1.message?.timestamp, let time2 = item2.message?.timestamp, time1 > time2{
                return true
            }else{
                return false
            }
        }
        return sortedList
    }
    
    private func customiseSearchBar(){
        
        self.searchBar.placeholder = StringConstants.Search
        self.searchBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        
        // TextField Color Customization
        let textFieldInsideSearchBar = self.searchBar.value(forKey: "searchField") as? UITextField
        
        textFieldInsideSearchBar?.borderStyle = .roundedRect
        textFieldInsideSearchBar?.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1.0)
        
        textFieldInsideSearchBar?.textAlignment = .center
        
        // Placeholder Customization
        if let textFieldInsideSearchBarLabel = textFieldInsideSearchBar?.value(forKey: "placeholderLabel") as? UILabel{
            textFieldInsideSearchBarLabel.textColor = UIColor(red: 152/255, green: 152/255, blue: 152/255, alpha: 1.0)
            textFieldInsideSearchBarLabel.textAlignment = .center
            textFieldInsideSearchBarLabel.font = AppFonts.Poppins_Light.withSize(15)
        }
        
        // Glass Icon Customization
        let glassIconView = textFieldInsideSearchBar?.leftView as? UIImageView
        glassIconView?.image = glassIconView?.image?.withRenderingMode(.alwaysTemplate)
        glassIconView?.tintColor = UIColor(red: 152/255, green: 152/255, blue: 152/255, alpha: 1.0)
        
    }
}

//MARK:-
//MARK:- UITableViewDataSource, UITableViewDelegate
extension RecentChatVC: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let searchText = self.searchBar.text, !searchText.isEmpty{
            return 2
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 30))
        let titleLable = UILabel(frame: CGRect(x: 15, y: 0, width: SCREEN_WIDTH-30, height: 30))
         if let searchText = self.searchBar.text, !searchText.isEmpty , section == 1, !self.searchUserList.messageList.isEmpty{
            titleLable.text = StringConstants.Messages
         }else{
            titleLable.text = ""
        }
        titleLable.font = AppFonts.Poppins_Medium.withSize(14.0)
        titleLable.textColor = AppColors.gray152
        headerView.backgroundColor = AppColors.gray227
        headerView.addSubview(titleLable)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if let searchText = self.searchBar.text, !searchText.isEmpty, !self.searchUserList.messageList.isEmpty{
            if section == 1{
                return 30
            }else{
                return .leastNormalMagnitude
            }
        }else{
            return .leastNormalMagnitude
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let searchText = self.searchBar.text, !searchText.isEmpty{
            if section == 1{
                return self.searchUserList.messageList.count
            }else{
                return self.inboxList.count
            }
        }else{
             return self.inboxList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: RecentChatCell.identifier, for: indexPath) as? RecentChatCell else { fatalError("RecentChatCell not found")}
        var search = ""
        if let searchText = self.searchBar.text, !searchText.isEmpty{
            if indexPath.section == 0{
                cell.populateCellWith(inbox: self.inboxList[indexPath.row], searchText: searchText )
            }else{
                cell.popultedCellLocalDB(messageInfo: self.searchUserList.messageList[indexPath.row], searchText: searchText)
            }
        }else{
            if let text = searchBar.text  {
                search = text
            }
            cell.populateCellWith(inbox: self.inboxList[indexPath.row], searchText: search)
        }
        //        if cell.unreadCount == 0{
        //            UIApplication.shared.applicationIconBadgeNumber = 0
        //            self.tabBarController?.tabBar.items?[1].badgeValue = nil
        //        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            deleteChat(atIndex: indexPath.row)
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func deleteChat(atIndex index : Int){
        let alertVc = AlertPopUpVC.instantiate(fromAppStoryboard: .Candidate)
        alertVc.delegate = self
        alertVc.modalPresentationStyle = .overCurrentContext
        alertVc.modalTransitionStyle = .crossDissolve
        alertVc.fromOpenChat = true
        self.present(alertVc,animated: true)
        selectedIndex = index
    }
}

extension RecentChatVC: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1{
            if !self.searchUserList.messageList.isEmpty{
                if let member = self.userDetails[self.searchUserList.messageList[indexPath.row].roomId] as? ChatMember, let room = self.groupDetails[self.searchUserList.messageList[indexPath.row].roomId] as? ChatRoom{
                    let chatVc = PersonalChatVC.instantiate(fromAppStoryboard: .Candidate)
                    chatVc.chatMember = member
                    chatVc.chatRoom = room
                    chatVc.chatType = .old
                    chatVc.delegate = self
                    self.navigationController?.pushViewController(chatVc, animated: true)
//                    if let _ = self.parent as? ChatVC, let userId = User.getUserModel().user_id{
//
//                        let updatedCount = AppUserDefaults.value(forKey: .messageBadge).intValue - inboxItem.unreadCount
//                        print_debug(updatedCount)
//                        if updatedCount >= 0{
//                            ChatHelper.setBadgeCount(userId: userId, count: updatedCount)
//                        }
//                    }
                }else{
                    print_debug("Something is not right")
                }
            }
        }
        else{
            let inboxItem = self.inboxList[indexPath.row]
            
            switch inboxItem.roomType{
                
            case .single:
                
//                print_debug(self.groupDetails[inboxItem.roomId])
                if let member = self.userDetails[inboxItem.roomId] as? ChatMember, let room = self.groupDetails[inboxItem.roomId] as? ChatRoom{
                    let chatVc = PersonalChatVC.instantiate(fromAppStoryboard: .Candidate)
                    
                    chatVc.chatMember = member
                    chatVc.chatRoom = room
                    chatVc.chatType = .old
                    chatVc.delegate = self
                    self.navigationController?.pushViewController(chatVc, animated: true)
                    if let _ = self.parent as? ChatVC, let userId = User.getUserModel().user_id{
                        let updatedCount = AppUserDefaults.value(forKey: .messageBadge).intValue - inboxItem.unreadCount
//                        if updatedCount >= 0{
//                            ChatHelper.setBadgeCount(userId: userId, count: updatedCount)
//                        }
                    }
                }else{
                    print_debug("Something is not right")
                }
            default :
                print_debug("Chat Type not found")
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
//    MARK:- Firebase Observer Methods
//    ========================================
extension RecentChatVC{
    
    private func observeInbox(){
        self.observeNewChatAdded()
    }
    
    //    MARK:- Observe newly added chat message
    private func observeNewChatAdded(){
       
        guard let currentUserId = User.getUserModel().user_id else { return  }
        let inboxRef = databaseReference.child(ChatEnum.Root.inbox.rawValue).child(currentUserId)
            if !fromPush{
                AppNetworking.showLoader()
            }
        databaseReference.child("inbox").child(currentUserId).observeSingleEvent(of: .value) { (detail) in
            if let _ = detail.value as? JSONDictionary{
//                print_debug(value)
            }else{
                 AppNetworking.hideLoader()
            }
        }
        self.databaseHandle[ObserverKeys.messageAdded.rawValue] = inboxRef.observe(.childAdded) { [weak self](snapshot) in
//             AppNetworking.showLoader()
            guard let _self = self else  { AppNetworking.hideLoader()
                return }
           
            if let value = snapshot.value as? String{
                print_debug(value)
                var inbox = Inbox(roomId: value)
                if let index = _self.inboxList.index(of: inbox){
                    inbox = _self.inboxList[index]
                }
                if snapshot.key == value{
                    inbox.roomType = .group
                }else {
                    inbox.roomType = .single
                }
                _self.observeRoomDetails(inbox: inbox)
                _self.getBlockLeaveLastMessage(roomId: inbox.roomId, completion: { (lastMessage) in
                    
                    if let lastMessage = lastMessage{
                        inbox.message = lastMessage
                        _self.handleLastMessages(inbox: inbox)
                    }else{
                        _self.setLastMessageObserver(inbox: inbox)
                    }
                })
            }else{
                AppNetworking.hideLoader()
            }
        }
    }
    
    //    MARK:- Observe Chat room for latest data change
    private func observeRoomDetails(inbox: Inbox){
        
        let roomRef = databaseReference.child(ChatEnum.Root.roomInfo.rawValue).child(inbox.roomId)
        self.groupDetailHandle[inbox.roomId] = roomRef.observe(.value, with: { [weak self](snapshot) in
            
            guard let _self = self else { return }
            
            if let value = snapshot.value {
                
                let chatRoom = ChatRoom(with: JSON(value))

                _self.groupDetails[inbox.roomId] = chatRoom
                
                if let index = _self.inboxList.index(of: inbox){
                    _self.inboxList[index].chatRoom = chatRoom
                    UIView.setAnimationsEnabled(false)
                   // _self.chatTableView.reloadData()
                }
            }
        })
    }
    
    //    MARK:- Observe last message change
    private func setLastMessageObserver(inbox: Inbox){
        
        let lastMessageRef = databaseReference.child(ChatEnum.Root.lastMessage.rawValue).child(inbox.roomId).child(ChatEnum.Root.chatLastMessage.rawValue)
        
        self.databaseHandle[inbox.roomId] = lastMessageRef.observe(.value) { [weak self](snapshot) in
            
            guard let _self = self else  { AppNetworking.hideLoader()
                return }
            
            if let value = snapshot.value{
                
                do{
                    let data = try JSON(value).rawData()
                    let message = try JSONDecoder().decode(ChatMessage.self, from: data)
                    var updatedInbox = inbox
                    updatedInbox.message = message
                    _self.handleLastMessages(inbox: updatedInbox)
                }catch let error{
                    AppNetworking.hideLoader()
                }
            }else{
                AppNetworking.hideLoader()
            }
        }
    }
    
    //    MARK:- Observe last message change
    private func observeUnread(forRoom roomId: String){
        
        guard let userId = User.getUserModel().user_id else { return }
        
        let unreadRef = databaseReference.child(ChatEnum.Root.unreadMessage.rawValue).child(roomId)
        self.unreadCountHandle[roomId] = unreadRef.child(userId).observe(.value) { [weak self](snapshot) in
            
            guard let _self = self else { return }
            
            if let unreadCount = snapshot.value as? Int{
                
                if let index = _self.inboxList.index(where: { (inbox) -> Bool in
                    return inbox.roomId == roomId
                }){
                    _self.inboxList[index].unreadCount = unreadCount
                    DispatchQueue.main.async {[weak self] in
                        guard let strongSelf = self else{return}
                        UIView.setAnimationsEnabled(false)
                        strongSelf.chatTableView.reloadData()
                        AppNetworking.hideLoader()
                        strongSelf.isLoadingFirstTime = false
                    }
                }
            }
        }
    }
    
    //    MARK:- Observe user online status
    private func observeUserOnlineStatus(userId: String, roomId: String){
        
        let isOnline = ChatEnum.User.isOnline.rawValue
        
        let ref = databaseReference.child(ChatEnum.User.root.rawValue).child(userId)
        
        self.onlineStatusHandle[userId] = ref.child(isOnline).observe(.value) { [weak self](snapshot) in
            
            guard let _self = self else { return }
            
            if let isOnline = snapshot.value as? Bool{
                
                if let index = _self.inboxList.index(where: { (inbox) -> Bool in
                    return inbox.roomId == roomId
                }){
                    _self.inboxList[index].chatMember?.isOnline = isOnline
                    UIView.setAnimationsEnabled(false)
                    if !_self.isLoadingFirstTime{
                        _self.chatTableView.reloadData()
                    }
                }
            }
        }
    }
}

//    MARK:- Firebase single event methods
extension RecentChatVC{
    
    private func getBlockLeaveLastMessage(roomId: String, completion: @escaping (ChatMessage?)->()){
        
        guard let userId = User.getUserModel().user_id else { return }
        
        let lastMessageRef = databaseReference.child(ChatEnum.Root.lastMessage.rawValue).child(roomId)
        
        lastMessageRef.child(userId).child("chatLastMessage").observeSingleEvent(of: .value) { (snapshot) in
            
            if let value = snapshot.value{
                
                if let data = try? JSON(value).rawData(), let message = try? JSONDecoder().decode(ChatMessage.self, from: data){
                    completion(message)
                }else{
                    completion(nil)
                }
                
            }else{
                completion(nil)
            }
        }
    }
    
    //    MARK:- get user details
    private func getUserDetails(inbox: Inbox){
        
        guard let currentUser = User.getUserModel().user_id else { return }
        guard let receiverId = inbox.message?.receiverId else { return }
        guard let senderId = inbox.message?.senderId else { return }
        
        var userId: String = ""
        
        if currentUser == receiverId{
            userId = senderId
        }else{
            userId = receiverId
        }
        guard !userId.isEmpty else {AppNetworking.hideLoader()
            return }
        
        ChatHelper.getUserDetails(userId: userId, completion: { [weak self](chatMember) in
            guard let _self = self, let chatMember = chatMember else { AppNetworking.hideLoader()
                return }
            
            _self.userDetails[inbox.roomId] = chatMember
            if let index = _self.inboxList.index(of: inbox){
                _self.inboxList[index].chatMember = chatMember
                _self.observeUserOnlineStatus(userId: chatMember.userId, roomId: inbox.roomId)
                if let messageList = inbox.message{
                      let userList = ChatUserListDB()
                    if let item = DBManager.sharedInstance.getChatLocalDbForId(id: User.getUserModel().user_id ?? "") {
                        if !DBManager.sharedInstance.getDataFromDB().isEmpty , let messageList = DBManager.sharedInstance.getChatUserForMessageId(id: messageList.roomId, user_id: User.getUserModel().user_id ?? "", message_Id: messageList.messageId), !messageList.messageId.isEmpty{
                        }else{
                            //                let userList = ChatLocalDB()
                          
                            let messagesList:JSONDictionary = [
                                "messageId": messageList.messageId,
                                "message":messageList.message,
                                "timestamp": "\(messageList.timestamp)",
                                "roomId":messageList.roomId,
                                "first_name":chatMember.firstName,
                                "mediaUrl": messageList.mediaUrl,
                                "senderId":  messageList.senderId,
                                "last_name": chatMember.lastName
                            ]
                            let uiRealm = try! Realm()
                            do {
                                try uiRealm.write {
                                    userList.messageList.append(MessageListDB(value: messagesList))
                                }
                            }catch let error {
                                print_debug(error)
                            }
                            DispatchQueue.backgroundQueueSync {
                                _self.upadteLocalDB(room_id:inbox.roomId,chatUserListDB: userList)
                            }
                        }
                    }else{
                        DispatchQueue.backgroundQueueSync {
                            _self.upadteLocalDB(room_id:inbox.roomId,chatUserListDB: userList)
                        }
                    }
                }
                
            //    _self.chatTableView.reloadRows(at: [IndexPath(row: Int(index), section: 0)], with: .none)
               // _self.chatTableView.reloadData()
//                AppNetworking.hideLoader()
            }else{
                AppNetworking.hideLoader()
            }
        })
    }
}

//    MARK:- RemoveObservers
//    ========================================
extension RecentChatVC{
    
    private func removeFirebaseObservers(){
        
        if let handle = self.databaseHandle[ObserverKeys.messageAdded.rawValue]{
            guard let currentUserId = User.getUserModel().user_id else { return  }
            let inboxRef = databaseReference.child(ChatEnum.Root.inbox.rawValue).child(currentUserId)
            inboxRef.removeObserver(withHandle: handle)
        }
        
        for item in self.inboxList{
            // remove last message observers
            if let handle = databaseHandle[item.roomId]{
                databaseReference.child(ChatEnum.Root.lastMessage.rawValue).child(item.roomId).child(ChatEnum.Root.chatLastMessage.rawValue).removeObserver(withHandle: handle)
            }
            // remove group details observers
            if let handle = self.groupDetailHandle[item.roomId]{
                
                let roomRef = databaseReference.child(ChatEnum.Root.roomInfo.rawValue).child(item.roomId)
                roomRef.removeObserver(withHandle: handle)
            }
        }
        
        for (roomId,handle) in self.unreadCountHandle{
            
            let unreadRef = databaseReference.child(ChatEnum.Root.unreadMessage.rawValue).child(roomId)
            
            if let userId = User.getUserModel().user_id, !userId.isEmpty{
                unreadRef.child(userId).removeObserver(withHandle: handle)
            }
        }
        
        for (userId,handle) in self.onlineStatusHandle{
            
            let isOnline = ChatEnum.User.isOnline.rawValue
            let ref = databaseReference.child(ChatEnum.User.root.rawValue).child(userId)
            ref.child(isOnline).removeObserver(withHandle: handle)
            
        }
    }
}

extension RecentChatVC : AlertPopUpDelegate {
    func didTapAffirmativeButton() {
        
        guard let selectedIndex = selectedIndex else{
            return
        }
        
        let inboxItem = self.inboxList[selectedIndex]
        
        if let member = self.userDetails[inboxItem.roomId] as? ChatMember, let room = self.groupDetails[inboxItem.roomId] as? ChatRoom{
            deleteChat(otherUserId: member.userId, roomId: room.chatRoomId)
        }
    }
    func didTapNegativeButton(){
        selectedIndex = nil
    }
    
    func deleteChat(otherUserId : String,roomId: String) {
        guard let selectedIndex = selectedIndex else{
            return
        }
        var unreadCount = 0
        guard !otherUserId.isEmpty,!roomId.isEmpty,!ChatEnum.Root.roomInfo.rawValue.isEmpty else { return }
        guard let userID = User.getUserModel().user_id else { return  }
    databaseReference.child(ChatEnum.Root.roomInfo.rawValue).child(roomId).child(RoomInfo.chatRoomMembers.rawValue).child(userID).child(RoomInfo.memberDelete.rawValue).setValue(ServerValue.timestamp())
        
        databaseReference.child(ChatEnum.Root.inbox.rawValue).child(userID).child(otherUserId).removeValue()
        ChatHelper.getUnreadMessagesCount(userId: userID, roomId: roomId) { (count) in
            unreadCount = count
        }
        
        ChatHelper.getBadgeCount(userId: userID) { (count) in
            //            ChatHelper.setBadgeCount(userId: message.receiverId,
            //                                     count: count + 1)
            let unreadRef = "/\(ChatEnum.Root.unreadMessage.rawValue)/\(roomId)/\(userID)"
            let badgeCount = "/badgeCount/\(userID)"
            let dict : JSONDictionary = ["\(unreadRef)": 0 , "\(badgeCount)": count - unreadCount]
            databaseReference.updateChildValues(dict)
        }
        self.inboxList.remove(at: selectedIndex)
//        if !DBManager.sharedInstance.getDataFromDB().isEmpty , let userInfo = DBManager.sharedInstance.getChatUserForRoomId(id: roomId, user_id: userID), !userInfo.room_id.isEmpty{
//            DBManager.sharedInstance.deleteUserFromDB(object: userInfo)
//        }
        chatTableView.deleteRows(at: [[0,selectedIndex]], with: .fade)
    }
}

extension RecentChatVC : PersonalChatVCDelegate {
    
    func chatDeleted(chatRoomId: String) {
        guard let index = inboxList.index(where: {$0.roomId == chatRoomId}) else{
            return
        }
        self.inboxList.remove(at: index)
        chatTableView.deleteRows(at: [[0,index]], with: .fade)
    }
}


//MARK:- DZNEmptyDataSource and Delegate
//==========================================
extension RecentChatVC: DZNEmptyDataSetDelegate, DZNEmptyDataSetSource {
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return #imageLiteral(resourceName: "ic_nomessage_")
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: StringConstants.No_Messages_Yet,                                                                           attributes: [NSAttributedStringKey.foregroundColor: AppColors.black46,
                                                                                                                                                    NSAttributedStringKey.font: AppFonts.Poppins_Medium.withSize(14)
            ])
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        return NSAttributedString(string: "",                                                                           attributes: [NSAttributedStringKey.foregroundColor: AppColors.gray152,
                                                                                                                                     NSAttributedStringKey.font: AppFonts.Poppins_Regular.withSize(12)                                                                                                                                                                                                            ])
    }
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}

//    MARK:- Search Delegate
//    ===========================================
extension RecentChatVC: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
    }
    func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool {
        
        self.chatTableView.isScrollEnabled = true
        self.chatTableView.alwaysBounceVertical = true
        self.chatTableView.bounces = true
        return true
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let searchText = searchBar.text else { return }
        if searchText.isEmpty {
            self.observeInbox()
            return
        }
        if !DBManager.sharedInstance.getDataFromDB().isEmpty, let userList = DBManager.sharedInstance.getChatLocalDbForId(id: User.getUserModel().user_id ?? ""){
            self.userListDB = userList
        }
        
        var matchList: [Inbox] = []
         let newUser = ChatUserListDB()
//        let searchMesssage = List<MessageListDB>()
        var count = 0
        matchList =  self.inboxList.filter {
            if let member =  $0.chatMember {
                return member.firstName.range(of: searchText, options: .caseInsensitive) != nil
            }
            return true
            
        }
        
//        DispatchQueue.main.async {[weak self] in
            let uiRealm = try! Realm()
            for user in self.userListDB.userList{
                
                let message = user.messageList.filter("message CONTAINS[c] %@", searchText).first
                // Iterate through all the Canteens
                //            let message = user.messageList.filter{ $0.message == searchText }
                if let newMessage = message{
                    //                searchMessage.append(newMessage)
                    do {
                        try uiRealm.write {
                            //                        searchMesssage.append(newMessage)
                            newUser.room_id = user.room_id
                            newUser.messageList.append(newMessage)
                            //                        searchMessage[count].messageList.append(newMessage)
                        }
                    }catch let error {
                        print_debug(error)
                    }
                    
                    count += 1
                }
            }
            
//        }
       
        print_debug(newUser)
        
        self.inboxList = matchList
        self.searchUserList = newUser
        print_debug(self.inboxList)
        print_debug(self.inboxList.count)
        UIView.setAnimationsEnabled(false)
        self.chatTableView.isScrollEnabled = true
        self.chatTableView.alwaysBounceVertical = true
        self.chatTableView.reloadData()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.chatTableView.alwaysBounceVertical = true
        self.chatTableView.isScrollEnabled = true
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        self.chatTableView.alwaysBounceVertical = true
        self.chatTableView.isScrollEnabled = true
    }
}

class KeyboardTapGesture: UITapGestureRecognizer {
    
}
