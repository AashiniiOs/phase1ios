//
//  VideoCallHistoryVC.swift
//  JobGet
//
//  Created by appinventiv on 16/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class VideoCallHistoryVC: UIViewController {
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var recentCallTableview: UITableView!
    
    
    var pageNo = 0
    var shouldLoadMoreData = false
    var isPaginationEnable = false
    var callListDetail = [VideoCallHistoryModel]()
    var isFirstTime = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getCallHistory(loader: true, page: 0, searchText: "")
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: Notification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: Notification.Name.UIKeyboardWillShow, object: nil)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.view.endEditing(true)
        self.recentCallTableview.endEditing(true)
    }
    
    private func initialSetup(){
        
        self.recentCallTableview.register(UINib(nibName: "RecentChatCell", bundle: nil), forCellReuseIdentifier: "RecentChatCell")
        self.recentCallTableview.dataSource = self
        self.recentCallTableview.delegate = self
        
        self.recentCallTableview.rowHeight = 50
        self.recentCallTableview.rowHeight = UITableViewAutomaticDimension
        self.recentCallTableview.emptyDataSetSource = self
        self.recentCallTableview.emptyDataSetDelegate = self
        //  recentCallTableview.separatorStyle = .none
        recentCallTableview.keyboardDismissMode = .onDrag
        self.searchBar.delegate = self
        self.customiseSearchBar()
        //  recentCallTableview.separatorStyle = .none
        recentCallTableview.keyboardDismissMode = .onDrag
        self.searchBar.delegate = self
        self.customiseSearchBar()
        self.getCallHistory(loader: true, page: self.pageNo, searchText: "")
        self.recentCallTableview.register(UINib(nibName: "LoadMoreTableViewCell", bundle: nil), forCellReuseIdentifier: "LoadMoreTableViewCell")
        self.recentCallTableview.enablePullToRefresh(tintColor: AppColors.themeBlueColor ,target: self, selector: #selector(refreshWhenPull(_:)))
    }
    
    
    @objc func keyboardWillAppear() {
        //Do something here
        print_debug("keyboardWillAppear")
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.removeKeyboard))
        
        self.recentCallTableview.addGestureRecognizer(tap)
    }
    
    @objc func keyboardWillDisappear() {
        //Do something here
        print_debug("keyboardWillDisappear")
        
        if let gesture = recentCallTableview.gestureRecognizers {
            for recognizer in gesture {
                if recognizer is KeyboardTapGesture {
                    recentCallTableview.removeGestureRecognizer(recognizer)
                }
            }
        }
    }
    
    @objc func removeKeyboard(){
        
        self.view.endEditing(true)
    }
    
    @objc func refreshWhenPull(_ sender: UIRefreshControl) {
        sender.endRefreshing()
        self.pageNo = 0
        self.shouldLoadMoreData = false
        self.isPaginationEnable = false
        
        self.getCallHistory(loader: false, page: 0, searchText: "")
        
    }
    
    private func isLoadingIndexPath(_ indexPath: IndexPath) -> Bool {
        guard shouldLoadMoreData else { return false}
        
        return indexPath.row == self.callListDetail.count
    }
    
    func getCallHistory( loader: Bool, page: Int, searchText: String) {
        
        let param = ["page": "\(page)", "search": searchText] as [String: Any]
        WebServices.getCallHistoryApi(parameters: param, loader: false, success: { (json) in
            var data = [VideoCallHistoryModel]()
            let jobs = json[ApiKeys.data.rawValue]["calls"].arrayValue
            for job in jobs {
                data.append(VideoCallHistoryModel(withDict: job))
            }
            if page == 0 {
                self.callListDetail.removeAll(keepingCapacity: false)
            }
            if self.shouldLoadMoreData {
                self.callListDetail.append(contentsOf: data)
            } else {
                self.callListDetail = data
            }
//            if self.isFirstTime{
//                AppNetworking.showLoader()
//                self.isFirstTime = false
//            }
            self.pageNo = json["page"].intValue
            self.shouldLoadMoreData = json["next"].boolValue
            self.recentCallTableview.reloadData()
        }) { (error: Error) in
            CommonClass.showToast(msg: error.localizedDescription )
        }
    }
    
    private func customiseSearchBar(){
        
        self.searchBar.placeholder = StringConstants.Search
        self.searchBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        
        // TextField Color Customization
        let textFieldInsideSearchBar = self.searchBar.value(forKey: "searchField") as? UITextField
        
        textFieldInsideSearchBar?.borderStyle = .roundedRect
        textFieldInsideSearchBar?.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1.0)
        
        textFieldInsideSearchBar?.textAlignment = .center
        
        // Placeholder Customization
        if let textFieldInsideSearchBarLabel = textFieldInsideSearchBar?.value(forKey: "placeholderLabel") as? UILabel{
            textFieldInsideSearchBarLabel.textColor = UIColor(red: 152/255, green: 152/255, blue: 152/255, alpha: 1.0)
            textFieldInsideSearchBarLabel.textAlignment = .center
            textFieldInsideSearchBarLabel.font = AppFonts.Poppins_Light.withSize(15)
        }
        
        // Glass Icon Customization
        let glassIconView = textFieldInsideSearchBar?.leftView as? UIImageView
        glassIconView?.image = glassIconView?.image?.withRenderingMode(.alwaysTemplate)
        glassIconView?.tintColor = UIColor(red: 152/255, green: 152/255, blue: 152/255, alpha: 1.0)
        
    }
}

extension VideoCallHistoryVC: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.callListDetail.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isLoadingIndexPath(indexPath){
            guard let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.defaultReuseIdentifier, for: indexPath) as? LoadMoreTableViewCell else {
                fatalError("Failed to initialize LoadMoreTableViewCell")
            }
            self.isPaginationEnable = true
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecentChatCell", for: indexPath) as! RecentChatCell
        
        
        let userData = User(json: (AppUserDefaults.value(forKey: .userData)))
        if userData.user_Type == .recuriter{
            cell.videoCallButton.isHidden = false
            cell.dateTraillingConstr.constant = 45
            cell.videoCallButton.addTarget(self, action: #selector(self.videoCallButtonTapp(_:)), for: .touchUpInside)
            cell.nameLabel.text = "\(self.callListDetail[indexPath.row].receiverFirstName ) \(self.callListDetail[indexPath.row].receiverLastName )"
            cell.userImageView.sd_setImage(with: URL(string: self.callListDetail[indexPath.row].receiverImage), placeholderImage: #imageLiteral(resourceName: "ic_user_"), completed: nil)
        }else{
            cell.dateTraillingConstr.constant = 15
            cell.videoCallButton.isHidden = true
            cell.nameLabel.text = "\(self.callListDetail[indexPath.row].senderFirstName ) \(self.callListDetail[indexPath.row].senderLastName )"
            cell.userImageView.sd_setImage(with: URL(string: self.callListDetail[indexPath.row].senderImage), placeholderImage: #imageLiteral(resourceName: "ic_user_"), completed: nil)
        }
        
        var time = ""
        cell.unreadCountLabel.isHidden = true
        if let timeDate = self.callListDetail[indexPath.row].createdAt {
            time = timeDate.elapsedTime
        }
        cell.messageLabel.text = time
        cell.dateTimeLabel.text = self.callListDetail[indexPath.row].callTime.msToSeconds.minuteSecondMS
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard self.shouldLoadMoreData else { return }
        
        if indexPath.row == self.callListDetail.count - 1 && self.shouldLoadMoreData   && isPaginationEnable {  //for 2nd last cell
            self.getCallHistory(loader: false, page: self.pageNo, searchText: "")
        }
    }
    
    @objc func videoCallButtonTapp(_ sender: UIButton){
        guard let indexPath = sender.tableViewIndexPath(self.recentCallTableview) else{return}
        let userData = User(json: (AppUserDefaults.value(forKey: .userData)))
        if userData.user_Type == .recuriter{
            self.notifyVideoCallApi(index: indexPath.row)
        }
    }
    
}


//MARK:- DZNEmptyDataSource and Delegate
//==========================================
extension VideoCallHistoryVC: DZNEmptyDataSetDelegate, DZNEmptyDataSetSource {
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return #imageLiteral(resourceName: "ic_novideo_")
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: StringConstants.No_Calls_Yet,                                                                           attributes: [NSAttributedStringKey.foregroundColor: AppColors.black46,
                                                                                                                                                 NSAttributedStringKey.font: AppFonts.Poppins_Medium.withSize(14)
            ])
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        return NSAttributedString(string: "",                                                                           attributes: [NSAttributedStringKey.foregroundColor: AppColors.gray152,
                                                                                                                                     NSAttributedStringKey.font: AppFonts.Poppins_Regular.withSize(12)                                                                                                                                                                                                            ])
    }
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}



extension VideoCallHistoryVC: UITableViewDelegate{
    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
////        let userData = User(json: (AppUserDefaults.value(forKey: .userData)))
////        if userData.user_Type == .recuriter{
////            self.notifyVideoCallApi(index: indexPath.row)
////        }
//    }
    
    func notifyVideoCallApi(index: Int){
        
        let userData = User(json: (AppUserDefaults.value(forKey: .userData)))
        
        let candidateDetail = self.callListDetail[index]
        if candidateDetail.user_login_session.isEmpty{
            self.showAlert(msg: StringConstants.Candidate_not_available)
            return
        }
        
        var params = JSONDictionary()
        params["senderName"] = userData.first_name
        params["receiverName"] = candidateDetail.senderLastName
        params["senderId"] = userData.user_id ?? ""
        params["receiverId"] = candidateDetail.receiverId
        params["deviceId"] = candidateDetail.user_login_session[0].device_id
        params["deviceToken"] = candidateDetail.user_login_session[0].device_token
        params["deviceType"] = candidateDetail.user_login_session[0].device_type
        params["senderDeviceToken"] = DeviceDetail.deviceToken
        params["type"] = "calling"
        params["senderImage"] = userData.imageUrl
        params["roomId"] = "\(userData.user_id ?? "")_\(candidateDetail.receiverId)"
        
        WebServices.notifyVideoCall(parameters: params, loader: true, success: { (data) in
            print_debug(data)
            sharedAppDelegate.moveToVideoChatScene(with: Caller(json: data["data"])!,fromPush : false, fromCallKit: false)
        }) { (error) in
            self.showAlert(msg: error.localizedDescription)
            return
        }
    }
}

//    MARK:- Search Delegate
//    ===========================================
extension VideoCallHistoryVC: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.getCallHistory(loader: false, page: 0, searchText: searchText)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
}


