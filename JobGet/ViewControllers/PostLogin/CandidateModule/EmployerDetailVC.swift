//
//  EmployerDetailVC.swift
//  JobGet
//
//  Created by Appinventiv on 06/06/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import MXParallaxHeader


class EmployerDetailVC: UIViewController {
    
    // MARK: - Outlets.
    // MARK: -
    
    @IBOutlet weak var navigationBGView: UIView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var reportView: UIView!
    @IBOutlet weak var blurView: UIView!
    @IBOutlet var navigationView: UIView!
    // @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var navigationTitle: UILabel!
    @IBOutlet weak var employerTableView: UITableView!
    
    // MARK: - Properties.
    // MARK: -
    var candidateJobDetail: CandidateJobList?
    var firstTimeLoad = true
    var userId = ""
    let parallexHeaderView   = JobDetailHeaderView()
    var isOpenFromChat = false
    var name = ""
    var recruiterProfileData: RecruiterInfoData?
    var isTableViewScrollEnable = false
    
    // MARK: - View life cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions and Target.
    // MARK: -
    
    @IBAction func moreButtonTapp(_ sender: UIButton) {
        self.blurView.alpha = 0.0
        self.reportView.alpha = 0.0
        UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            self.blurView.alpha = 0.55
            self.reportView.alpha = 1.0
            self.blurView.isHidden = false
            self.reportView.isHidden = false
        }, completion: nil)
    }
    
    @IBAction func reportButtonTapp(_ sender: UIButton) {
        
        let popUpVc = AlertPopUpVC.instantiate(fromAppStoryboard: .Candidate)
        popUpVc.delegate = self
        popUpVc.alertType = .reportUser
        popUpVc.modalPresentationStyle = .overCurrentContext
        popUpVc.modalTransitionStyle = .crossDissolve
        let rootViewController = UIApplication.shared.keyWindow?.rootViewController
        rootViewController?.present(popUpVc, animated: true, completion: nil)
        
    }
    
    
    @IBAction func navigationButtonTapp(_ sender: UIButton) {
        
        if !self.employerTableView.isScrollEnabled {
            
            UIView.animate(withDuration: 0.3, animations: {
                
                self.isTableViewScrollEnable = false
                self.employerTableView.contentOffset.y = 10
                
            })
            
        }
    }
    
    @IBAction func backButtonTapp(_ sender: UIButton) {
        if self.employerTableView.isScrollEnabled {
            self.navigationController?.popViewController(animated: true)
        } else {
            UIView.animate(withDuration: 0.3, animations: {
                
                self.isTableViewScrollEnable = false
                self.employerTableView.contentOffset.y = 10
                
            })
            
        }
    }
    
    @objc func removePopup(gesture: UITapGestureRecognizer) {
        self.blurView.alpha = 0.65
        self.reportView.alpha = 1.0
        UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            self.blurView.alpha = 0.0
            self.reportView.alpha = 0.0
            self.blurView.isHidden = true
            self.reportView.isHidden = true
        }, completion: nil)
    }
}

// MARK: - Private methods.
// MARK: -
private extension EmployerDetailVC{
    
    func initialSetup(){
        
        self.blurView.isHidden = true
        self.reportView.isHidden = true
        self.employerTableView.delegate = self
        self.employerTableView.dataSource = self
        self.employerTableView.estimatedRowHeight = 80
        self.employerTableView.rowHeight = UITableViewAutomaticDimension
        self.parallexHeaderView.containerViewHeight.constant = 0.0
        self.parallexHeaderView.jobNameLabel.isHidden = true
        self.parallexHeaderView.jobNameContainerView.isHidden = true
        if  let companyData = self.candidateJobDetail, let recruiterData = companyData.recruiterDetail{
            
            print_debug(recruiterData.user_image)
            self.parallexHeaderView.jobImageView.sd_setImage(with: URL(string:recruiterData.user_image ), placeholderImage: #imageLiteral(resourceName: "icHomeCategoryPlaceholder"), completed: nil)
        }
        self.reportView.setCorner(cornerRadius: 3.0, clip: true)
        
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.removePopup(gesture:)))
        self.blurView.addGestureRecognizer(tap)
        
    }
    
    func reportUserService(){
        
        var recruiterID = ""
        
        if self.userId.isEmpty{
            recruiterID = self.candidateJobDetail?.recruiterDetail?.recruiterId ?? ""
        }else{
            recruiterID = self.userId
        }
        
        WebServices.reportUser(parameters: ["recruiterId" :  recruiterID], loader: true, success: { (json) in
            print_debug(json)
            self.blurView.isHidden = true
            self.reportView.isHidden = true
            
            guard let vcArray = self.navigationController?.viewControllers else{return}
            for vc in vcArray{
                
                if let viewcontroller = vc as? CandidateTabBarVC {
                    let msg = json["message"].stringValue
                    CommonClass.showToast(msg: msg)
                    self.navigationController?.popToViewController(viewcontroller, animated: true)
                    NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "Refresh_After_Report_User"), object: nil)
                }
                
            }
            
            print_debug(self.navigationController?.viewControllers)
        }) { (error) in
            
            print_debug(error.localizedDescription)
        }
        
    }
}

// MARK: - Table view delegate and datasource methods.
// MARK: -
extension EmployerDetailVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            if let infoData = recruiterProfileData{
                if infoData.recruiterCompanyData.companyDesc.isEmpty{
                    return 1
                }else{
                    return 2
                }
            }else{
                return 1
            }
            
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "EmployerPersonalInfoCell", for: indexPath) as? EmployerPersonalInfoCell else{fatalError("EmployerPersonalInfoCell not found.")}
                if  let companyData = self.candidateJobDetail, let recruiterData = companyData.recruiterDetail{
                    
                    cell.employerNameLabel.text = "\(recruiterData.recruiterName) \(recruiterData.last_name)"
                    cell.chatButton.isHidden = false
                    cell.companyNameLabel.text = companyData.companyName
                }else{
                    if let infoData = recruiterProfileData{
                        cell.employerNameLabel.text = "\(infoData.firstName ) \(infoData.lastName )"
                        cell.companyNameLabel.text = infoData.recruiterCompanyData.companyName
                        cell.chatButton.isHidden = false
                    }
                }
                cell.chatButton.addTarget(self, action: #selector(self.chatButtonTapped(_:)), for: .touchUpInside)
                return cell
            }else{
                
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "EmployerCompanyInfoCell", for: indexPath) as? EmployerCompanyInfoCell else{fatalError("EmployerCompanyInfoCell not found.")}
                if let infoData = recruiterProfileData{
                    cell.descriptionDetailLabel.text = infoData.recruiterCompanyData.companyDesc
                }else{
                    if  let companyData = self.candidateJobDetail{
                        
                        cell.descriptionDetailLabel.text = companyData.companyDesc
                    }
                }
                return cell
            }
        } else  {
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "EmployerTotalJob", for: indexPath) as? EmployerTotalJob else{fatalError("EmployerTotalJob not found.")}
            if firstTimeLoad {
                self.firstTimeLoad = false
                if isOpenFromChat{
                    cell.recruiterId = self.userId
                }else{
                    cell.recruiterId = self.candidateJobDetail?.recruiterDetail?.recruiterId ?? ""
                    cell.jobId = self.candidateJobDetail?.jobId ?? ""
                }
                
                cell.setupView()
                cell.recruiterTotalJobVC.delegate = self
                cell.recruiterTotalJobVC.updateEmployerDetailsDelegate = self
                
            }
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return UITableViewAutomaticDimension
        } else {
            if SCREEN_HEIGHT == 812{
                return SCREEN_HEIGHT - 120
            }else{
                return SCREEN_HEIGHT - 60
            }
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            return self.parallexHeaderView
        } else {
            return UIView()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        
        return 0.00001
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 200
        } else {
            return 0.00001
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print_debug(scrollView.contentOffset.y)
       
        var isScrollEnable = Bool()
        if scrollView.contentOffset.y > 200 {

            isScrollEnable = false
            self.backBtn.setImage(#imageLiteral(resourceName: "ic_down_arrow_"), for: .normal)
            self.employerTableView.isScrollEnabled = false
            self.employerTableView.scrollToRow(at: IndexPath(row: 0, section: 1 ), at: .top, animated: false)
            self.navigationView.backgroundColor = AppColors.themeBlueColor
            self.navigationBGView.backgroundColor = AppColors.themeBlueColor
            self.isTableViewScrollEnable = true
            
        } else {
            if self.isTableViewScrollEnable{
                return
            }

             isScrollEnable = true
            self.employerTableView.isScrollEnabled = true
            self.backBtn.setImage(#imageLiteral(resourceName: "icHomeDetailBack"), for: .normal)
            self.navigationView.backgroundColor = UIColor.clear
            self.navigationBGView.backgroundColor = UIColor.clear
            
        }
        
        NotificationCenter.default.post(name: NSNotification.Name.init("manageTableViewScroll"), object: nil, userInfo: ["isScrollEnable" : isScrollEnable])
    }
    
    
    @objc private func chatButtonTapped(_ sender: UIButton){
        //        TODO:- Show loader if required
        
        
        if let companyData = self.candidateJobDetail, let recruiterData = companyData.recruiterDetail{
            print_debug(recruiterData.recruiterId)
            ChatHelper.getUserDetails(userId: recruiterData.recruiterId, completion: { [weak self](member) in
                
                guard let _self = self else { return }
                
                if let member = member{
                    
                    let chatVc = PersonalChatVC.instantiate(fromAppStoryboard: .Candidate)
                    chatVc.chatMember = member
                    chatVc.isOpenFromProfile = true
                    _self.navigationController?.pushViewController(chatVc, animated: false)
                }else{
                    _self.showAlert(msg: StringConstants.Recruiter_not_available)
                }
            })
        }else if !self.userId.isEmpty{
            ChatHelper.getUserDetails(userId: self.userId, completion: { [weak self](member) in
                
                guard let _self = self else { return }
                
                if let member = member{
                    
                    let chatVc = PersonalChatVC.instantiate(fromAppStoryboard: .Candidate)
                    chatVc.chatMember = member
                    chatVc.isOpenFromProfile = true
                    _self.navigationController?.pushViewController(chatVc, animated: false)
                }else{
                    _self.showAlert(msg: StringConstants.Recruiter_not_available)
                }
            })
            
        }else{
            self.showAlert(msg: StringConstants.Recruiter_not_available)
        }
    }
}

extension EmployerDetailVC: OpenProfileSetup , UpdateEmployerDetailsDelegate{
    func updateEmployerDetails(recruiterProfileData: RecruiterInfoData) {
        self.recruiterProfileData = recruiterProfileData
        
        if let infoData = self.recruiterProfileData{
            self.parallexHeaderView.jobImageView.sd_setImage(with: URL(string:infoData.recruiterImage ), placeholderImage: #imageLiteral(resourceName: "icHomeCategoryPlaceholder"), completed: nil)
        }
        
        self.employerTableView.reloadData()
    }
    
    func goToProfileSetup() {
        let obj = YourProfileStep1VC.instantiate(fromAppStoryboard: .Recruiter)
        
        obj.job_id = self.candidateJobDetail?.jobId ?? ""
        obj.recruiterId = self.candidateJobDetail?.recruiterDetail?.recruiterId ?? ""
        obj.fromTabbar = false
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
}

extension EmployerDetailVC: AlertPopUpDelegate {
    
    func didTapAffirmativeButton() {
        self.reportUserService()
    }
    
    func didTapNegativeButton() {
        self.blurView.alpha = 0.0
        self.reportView.alpha = 0.0
        self.blurView.isHidden = true
        self.reportView.isHidden = true
    }
    
    
}

// MARK: - EmployerPersonalInfoCell
// MARK: -
class EmployerPersonalInfoCell: UITableViewCell{
    
    @IBOutlet weak var companyTitleView: UIView!
    @IBOutlet weak var companyView: UIView!
    @IBOutlet weak var employerNameLabel: UILabel!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var positionLabel: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var chatButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.companyTitleView.backgroundColor = AppColors.themeBlueColor
        self.chatButton.isHidden = true
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.chatButton.roundCorners()
        self.bgView.setShadow(offset: CGSize(width: 0.0, height: 2.0))
    }
}

// MARK: - EmployerCompanyInfoCell
// MARK: -
class EmployerCompanyInfoCell: UITableViewCell{
    
    @IBOutlet weak var descriptionTitleLabel: UILabel!
    @IBOutlet weak var bgViewBottomConstraints: NSLayoutConstraint!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var descriptionDetailLabel: UILabel!
    
    @IBOutlet weak var descTitleContainerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.descTitleContainerView.backgroundColor = AppColors.themeBlueColor
        descriptionDetailLabel.textColor = AppColors.black46
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.bgView.setShadow(offset: CGSize(width: 0.0, height: 2.0))
        self.bgView.setCorner(cornerRadius: 5.0, clip: false)
        self.descTitleContainerView.roundCornersFromOneSide([.topLeft, .topRight], radius: 5.0)
        
    }
}

class EmployerTotalJob: UITableViewCell {
    
    //MARK:- Properties
    //==================
    var recruiterTotalJobVC = RecruiterTotalJobVC()
    var jobId: String?
    var recruiterId: String?
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var totalPostedJobLabel: UILabel!
    @IBOutlet weak var totalJobContainerView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.totalPostedJobLabel.text = StringConstants.Total_Job_Posted
        self.recruiterTotalJobVC = RecruiterTotalJobVC.instantiate(fromAppStoryboard: .settingsAndChat)
    }
    
    func setupView() {
        if let job_id = self.jobId{
            self.recruiterTotalJobVC.jobId = job_id
        }
        
        if let recruiter_Id = self.recruiterId{
            self.recruiterTotalJobVC.recruiterId = recruiter_Id
        }
        
        self.recruiterTotalJobVC.view.frame = self.totalJobContainerView.bounds
        self.totalJobContainerView.addSubview(recruiterTotalJobVC.view)
        
    }
}



