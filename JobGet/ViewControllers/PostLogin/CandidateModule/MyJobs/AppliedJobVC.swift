//
//  AppliedJobVC.swift
//  JobGet
//
//  Created by macOS on 27/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import CoreLocation

class AppliedJobVC: BaseVC {
    
    //MARK:- Properties
    //==================
    let jobType = UserJobType.applied
    var pageNo = 0
    var shouldLoadMoreData = false
    var appliedJobList = [CandidateJobList]()
    var isPaginationEnable = false
    var currentLocation: CLLocation?
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var appliedJobTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.getAppliedJob(loader: true, pageNo: 0, search: "")

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: Notification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: Notification.Name.UIKeyboardWillShow, object: nil)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        self.appliedJobTableView.endEditing(true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
//MARK:- Private Methods
//=======================
private extension AppliedJobVC {
    
    func initialSetup() {
        self.appliedJobTableView.keyboardDismissMode = .onDrag
        self.appliedJobTableView.delegate = self
        self.appliedJobTableView.dataSource = self
        self.appliedJobTableView.emptyDataSetSource = self
        self.appliedJobTableView.emptyDataSetDelegate = self
        self.searchBar.delegate = self
        self.appliedJobTableView.enablePullToRefresh(tintColor: AppColors.themeBlueColor ,target: self, selector: #selector(refreshWhenPull(_:)))
        self.registerNib()
        customiseSearchBar()
    }
    
    @objc func keyboardWillAppear() {
        //Do something here
        print_debug("keyboardWillAppear")
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.removeKeyboard))
        
        self.appliedJobTableView.addGestureRecognizer(tap)
    }
    
    @objc func keyboardWillDisappear() {
        //Do something here
        print_debug("keyboardWillDisappear")
        if let gesture = appliedJobTableView.gestureRecognizers {
            for recognizer in gesture {
                if recognizer is KeyboardTapGesture {
                    appliedJobTableView.removeGestureRecognizer(recognizer)
                }
            }
        }
       
    }
    
    @objc func removeKeyboard(){
        
        self.view.endEditing(true)
    }
    
    private func customiseSearchBar(){
        
        self.searchBar.placeholder = StringConstants.Search
        self.searchBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        // TextField Color Customization
        let textFieldInsideSearchBar = self.searchBar.value(forKey: "searchField") as? UITextField
        
        textFieldInsideSearchBar?.borderStyle = .roundedRect
        textFieldInsideSearchBar?.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1.0)
        textFieldInsideSearchBar?.textColor = AppColors.gray152
        textFieldInsideSearchBar?.textAlignment = .left
        
        // Placeholder Customization
        if let textFieldInsideSearchBarLabel = textFieldInsideSearchBar?.value(forKey: "placeholderLabel") as? UILabel{
            
            textFieldInsideSearchBarLabel.textAlignment = .center
            textFieldInsideSearchBarLabel.font = AppFonts.Poppins_Light.withSize(15)
        }
        
        // Glass Icon Customization
        let glassIconView = textFieldInsideSearchBar?.leftView as? UIImageView
        glassIconView?.image = glassIconView?.image?.withRenderingMode(.alwaysTemplate)
        glassIconView?.tintColor = UIColor(red: 152/255, green: 152/255, blue: 152/255, alpha: 1.0)
        
    }
    
    func getLocation() {
        
        self.isPaginationEnable = false
        
        
        DispatchQueue.main.async {
            
            if CLLocationManager.authorizationStatus() == .denied {
                
                CommonClass.delayWithSeconds(1.0, completion: {
                    let popUpVc = AlertPopUpVC.instantiate(fromAppStoryboard: .Candidate)
                    popUpVc.delegate = self
                    popUpVc.modalPresentationStyle = .overCurrentContext
                    popUpVc.modalTransitionStyle = .crossDissolve
                    self.present(popUpVc, animated: true, completion: nil)
                })
            } else {
                SharedLocationManager.fetchCurrentLocation { (location) in
                    //   SharedLocationManager.locationManager.stopMonitoringSignificantLocationChanges()
                    SharedLocationManager.locationManager.stopUpdatingLocation()
                    self.currentLocation = location
                    self.getAppliedJob(loader: false, pageNo: 0, search:  "")
                }
            }
        }
    }
    
    @objc func refreshWhenPull(_ sender: UIRefreshControl) {
        sender.endRefreshing()
        self.pageNo = 0
        self.shouldLoadMoreData = false
        self.isPaginationEnable = false
        self.getAppliedJob(loader: false, pageNo: 0, search: "")
        
    }
    private func isLoadingIndexPath(_ indexPath: IndexPath) -> Bool {
        guard shouldLoadMoreData else { return false}
        
        return indexPath.row == self.appliedJobList.count
    }
    
    func registerNib() {
        self.appliedJobTableView.register(UINib(nibName: "CandidateJobAppliedCell", bundle: nil), forCellReuseIdentifier: "CandidateJobAppliedCell")
        self.appliedJobTableView.register(UINib(nibName: "LoadMoreTableViewCell", bundle: nil), forCellReuseIdentifier: "LoadMoreTableViewCell")
    }
}


//MARK: - IB Action and Target
//===============================
extension AppliedJobVC {
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: - Web services
//======================
extension AppliedJobVC {
    
    func getAppliedJob(loader: Bool, pageNo: Int, search: String) {
        
        var params = [ApiKeys.type.rawValue: self.jobType.rawValue, ApiKeys.page.rawValue: pageNo, "search": search] as [String : Any]
        
//        params[ApiKeys.latitude.rawValue]  = AppUserDefaults.value(forKey: .userLat).doubleValue
//        params[ApiKeys.longitude.rawValue] = AppUserDefaults.value(forKey: .userLong).doubleValue
//     
        
        print_debug(params)
        WebServices.getSaveJob(parameters: params, loader: loader, success: { (json) in
            var data = [CandidateJobList]()
            
            if pageNo == 0 {
                self.appliedJobList.removeAll(keepingCapacity: false)
            }
            
            let jobs = json["data"]["jobs"].arrayValue
            
            for job in jobs {
                data.append(CandidateJobList(dict: job))
            }
            
            if self.shouldLoadMoreData {
                self.appliedJobList.append(contentsOf: data)
            } else {
                self.appliedJobList = data
            }
            
            
            self.pageNo = json["page"].intValue
            self.shouldLoadMoreData = json["next"].boolValue
            self.appliedJobTableView.reloadData()
            
        }) { (error) in
            print_debug(error)
            CommonClass.showToast(msg: error.localizedDescription)
            
        }
    }
}

//MARK: - Table view Delegate and DataSource
//============================================
extension AppliedJobVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = self.appliedJobList.count
        return shouldLoadMoreData ? count + 1 : count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isLoadingIndexPath(indexPath) {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.defaultReuseIdentifier, for: indexPath) as? LoadMoreTableViewCell else {
                fatalError("Failed to initialize LoadMoreTableViewCell")
            }
            isPaginationEnable = true
            return cell
        }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CandidateJobAppliedCell") as? CandidateJobAppliedCell else { fatalError("invalid cell \(self)")
        }
        
        cell.isOpenFromApplied = true
        cell.populateCandidateJobDetail(self.appliedJobList[indexPath.row])
        cell.shareButton.addTarget(self, action: #selector(self.shareJobButtonTapped(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard self.shouldLoadMoreData else { return }
        
        if indexPath.row == self.self.appliedJobList.count - 1 && self.shouldLoadMoreData && self.isPaginationEnable {  //for 2nd last cell
            getAppliedJob(loader: false, pageNo: self.pageNo, search: "")
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
        let sceen = CandidateJobDetailVC.instantiate(fromAppStoryboard: .Candidate)
        sceen.jobId = self.appliedJobList[indexPath.row].jobId
        sceen.commingFromScreen = .jobListCell
        sceen.jobDetail = self.appliedJobList[indexPath.row]
         sceen.distance = self.appliedJobList[indexPath.row].distance
        if let detail = self.appliedJobList[indexPath.row].recruiterDetail {
            sceen.recruiterDetail = detail
            
        }
        self.navigationController?.pushViewController(sceen, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.appliedJobTableView.superview?.endEditing(true)
        self.view.endEditing(true)
    }
    
    
    //MARK:- Target Actions
    //=====================
    
    @objc func shareJobButtonTapped(_ sender: UIButton) {
        
        guard let idx = sender.tableViewIndexPath(self.appliedJobTableView) else { return }
        self.displayShareSheet(shareContent: self.appliedJobList[idx.row].jobShareUrl)
    }
}

//    MARK:- Search Delegate
//    ===========================================

extension AppliedJobVC: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        print_debug("searchBar")
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        guard let text = searchBar.text else { return }
        
        if text.isEmpty {
            searchBar.textField?.resignFirstResponder()
        } else {
            self.getAppliedJob(loader: true, pageNo: 0, search: text)
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty{
            self.view.endEditing(true)
            self.getAppliedJob(loader: true, pageNo: 0, search: "")
        }
    }
}

//MARK:- DZNEmptyDataSource and Delegate
//==========================================
extension AppliedJobVC: DZNEmptyDataSetDelegate, DZNEmptyDataSetSource {
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return #imageLiteral(resourceName: "icHomeNoJobs")
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: StringConstants.No_Applied_Jobs,                                                                           attributes: [NSAttributedStringKey.foregroundColor: AppColors.black46,
                                                                                                                                                    NSAttributedStringKey.font: AppFonts.Poppins_Bold.withSize(16)
            ])
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        return NSAttributedString(string: "",                                                                           attributes: [NSAttributedStringKey.foregroundColor: AppColors.gray152,
                                                                                                                                     NSAttributedStringKey.font: AppFonts.Poppins_Regular.withSize(12)                                                                                                                                                                                                            ])
    }
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}

//MARK:- Location alert popup Delegate
//===========================================
extension AppliedJobVC: AlertPopUpDelegate {
    
    func didTapAffirmativeButton() {
        //        TODO: Add settings
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: { (success) in
                
            })
        } else {
            // Fallback on earlier versions
        }
    }
    
    func didTapNegativeButton() {
        print_debug("location access denied")
        self.getAppliedJob(loader: false, pageNo: 0, search: "")
    }
    
}

