//
//  EmployerProfileVC.swift
//  JobGet
//
//  Created by macOS on 26/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import MXParallaxHeader

class EmployerProfileVC: BaseVC {
    
    //MARK:- Properties
    //==================
    let parallexHeaderView = JobDetailHeaderView()
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var employerProfileTableView: UITableView!
    
    //MARK:- view Life cycle
    //=======================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
}
//MARK:- Private Methods
//=======================
private extension EmployerProfileVC {
    
    func initialSetup() {
        
        self.employerProfileTableView.delegate   = self
        self.employerProfileTableView.dataSource = self
        self.setupParallaxHeader()
    }
    
    private func setupParallaxHeader(){
        
        let parallexHeaderHeight = CGFloat(250.0)  //CGFloat(350.0)
        let parallexHeaderMinHeight = self.navigationController?.navigationBar.bounds.height ?? 64
        let statusBarHeight = CGFloat(25) // SwifterSwift.statusBarHeight
        
        
        // self.headerButtonTargetMethods()
        self.parallexHeaderView.frame = CGRect(x: self.view.frame.origin.x, y: self.view.frame.origin.y, width: self.view.frame.size.width, height: parallexHeaderHeight)
        
        self.employerProfileTableView.parallaxHeader.view = parallexHeaderView
        self.employerProfileTableView.parallaxHeader.minimumHeight = parallexHeaderMinHeight + statusBarHeight + 20   
        self.employerProfileTableView.parallaxHeader.height = parallexHeaderHeight
        self.employerProfileTableView.parallaxHeader.mode = MXParallaxHeaderMode.topFill
    }
}

//MARK: - IB Action and Target
//===============================
extension EmployerProfileVC {
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
//MARK: - Table view Delegate and DataSource
//============================================
extension EmployerProfileVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "EmployerDetailCell") as? EmployerDetailCell else { fatalError("invalid cell \(self)")
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 100
        case 1:
            return 150
        case 2:
            return 200
        default:
            return 0.0
        }
    }
}
//MARK:- Prototype Cell
//=========================
class EmployerDetailCell: UITableViewCell {
    
    //MARK:- Properties
    //==================
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var employerNameLabel: UILabel!
    @IBOutlet weak var jobNameLabel: UILabel!
    @IBOutlet weak var employerPositionLabel: UILabel!
    @IBOutlet weak var chatImageView: UIImageView!
    @IBOutlet weak var chatContainerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
    }
    
    override func draw(_ rect: CGRect) {
        self.employerNameLabel.font = AppFonts.Poppins_SemiBold.withSize(16)
        self.jobNameLabel.font = AppFonts.Poppins_Regular.withSize(14)
        self.jobNameLabel.font = AppFonts.Poppins_SemiBold.withSize(16)
        self.employerNameLabel.textColor = AppColors.black46
        self.employerPositionLabel.textColor = AppColors.black46
        self.jobNameLabel.textColor = AppColors.black46
        
        self.chatContainerView.roundCorners()
        self.chatImageView.roundCorners()
    }
}
