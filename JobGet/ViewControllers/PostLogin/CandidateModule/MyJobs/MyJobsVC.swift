//
//  MyJobsVC.swift
//  JobGet
//
//  Created by appinventiv on 16/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import CoreLocation

protocol UpdateAppliedJobListDelegate: class {
    func updateAppliedJobList()
}

class MyJobsVC: BaseVC {
    
    //    MARK:- Properties
    //    ====================
    enum SelectedVC {
        case savedVC
        case appliedVC
        case sortlistedVC
    }
    
    //    MARK:- Properties
    //    ====================
    var selectedVC: SelectedVC = .appliedVC
    var savedJobVC: SavedJobsVC!
    var appliedJobVC: AppliedJobVC!
    var shortlistedJobVC: ShortlistedJobVC!
    var savedSearch = ""
    var appliedSearch = ""
    var shortlistedSearch = ""
    var currentLocation: CLLocation?
    var isOpenFromNotification = false
    var isScrollToShortList = false

    //    MARK:- IBOutlets
    //    ===========================================
    
    @IBOutlet weak var shortlistBadgeLabel: UILabel!
    @IBOutlet weak var badgeLabel: UILabel!
    @IBOutlet weak var screenTitle: UILabel!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var savedJobButton: UIButton!
    @IBOutlet weak var appliedJobButton: UIButton!
    @IBOutlet weak var shortListedJobButton: UIButton!
    @IBOutlet weak var underLineView: UIView!
    @IBOutlet weak var underLineViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var scrollView: UIScrollView!
    //  @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var buttonContainerView: UIView!
    
    
    //    MARK:- View life cycle
    //    ===========================================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        shortlistedJobVC.delegate = self
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if AppUserDefaults.value(forKey: .totalNotification).intValue > 0{
            self.badgeLabel.isHidden = false
        }else{
            self.badgeLabel.isHidden = true
        }
        
        
        if self.isOpenFromNotification{
            self.scrollToShortList()
            self.isOpenFromNotification = false
            self.isScrollToShortList = true
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    //    MARK:- functions
    //    ===========================================
    
    private func initialSetup() {
        
        self.getLocation()
        self.badgeLabel.roundCorners()
        self.shortlistBadgeLabel.roundCorners()
        
        self.appliedJobButton.isSelected = true
        // self.instantiateViewController()
        self.buttonContainerView.setShadow()
//        NotificationCenter.default.addObserver(self, selector: #selector(self.updateShortlistBadge), name: NSNotification.Name(rawValue:"UPDATE_SHORTLIST_BADGE"), object: nil)
    }
    
    @objc func updateShortlistBadge(){
        self.shortlistBadgeLabel.isHidden = false
    }
    
    private func getLocation() {
        self.instantiateViewController()
//        DispatchQueue.main.async {
//            if CLLocationManager.authorizationStatus() == .denied {
////                CommonClass.delayWithSeconds(1.0, completion: {
////                    let popUpVc = AlertPopUpVC.instantiate(fromAppStoryboard: .Candidate)
////                    popUpVc.delegate = self
////                    popUpVc.modalPresentationStyle = .overCurrentContext
////                    popUpVc.modalTransitionStyle = .crossDissolve
////                    self.present(popUpVc, animated: true, completion: nil)
////                })
//            } else {
//                SharedLocationManager.fetchCurrentLocation { (location) in
//                    //   SharedLocationManager.locationManager.stopMonitoringSignificantLocationChanges()
//                    SharedLocationManager.locationManager.stopUpdatingLocation()
//                    self.currentLocation = location
//                    self.instantiateViewController()
//                }
//            }
//        }
    }
    
    private func instantiateViewController() {
        
        self.scrollView.delegate = self
        self.configureScrollView()
        
        //instanciate the AppliedJobVC
        self.appliedJobVC = AppliedJobVC.instantiate(fromAppStoryboard: .Candidate)
        self.appliedJobVC.view.frame.origin = CGPoint.zero
        self.appliedJobVC.currentLocation = self.currentLocation
        self.scrollView.frame = self.appliedJobVC.view.frame
        self.scrollView.addSubview(self.appliedJobVC.view)
        self.addChildViewController(self.appliedJobVC)
        
        //instanciate the SavedJobsVC
        self.savedJobVC = SavedJobsVC.instantiate(fromAppStoryboard: .Candidate)
        self.savedJobVC.view.frame.origin = CGPoint(x: SCREEN_WIDTH, y: 0)
        self.savedJobVC.currentLocation = self.currentLocation
        self.scrollView.frame = self.savedJobVC.view.frame
        self.scrollView.addSubview(self.savedJobVC.view)
        self.addChildViewController(self.savedJobVC)
        
        //instanciate the ShortlistedJobVC
        self.shortlistedJobVC = ShortlistedJobVC.instantiate(fromAppStoryboard: .Candidate)
        self.shortlistedJobVC.view.frame.origin = CGPoint(x: 2 * SCREEN_WIDTH, y: 0)
        self.shortlistedJobVC.currentLocation = self.currentLocation
        self.scrollView.frame = self.shortlistedJobVC.view.frame
        self.scrollView.addSubview(self.shortlistedJobVC.view)
        self.addChildViewController(self.shortlistedJobVC)
        if self.isScrollToShortList{
            self.scrollToShortList()
            self.isScrollToShortList = false
        }
        
    }
    
    func configureScrollView(){
        
        self.scrollView.contentSize = CGSize(width: 3 * SCREEN_WIDTH, height: 1)
        self.scrollView.isPagingEnabled = true
    }
    
    
    //    MARK:- IBActions
    //    ===========================================
    
    @IBAction func saveJobButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        self.savedJobButton.isSelected = true
        self.appliedJobButton.isSelected = false
        self.shortListedJobButton.isSelected = false
        self.savedJobVC.delegate = self
        self.selectedVC = .savedVC
        self.scrollView.setContentOffset(CGPoint(x: SCREEN_WIDTH, y: 0), animated: true)
        self.view.layoutIfNeeded()
    }
    
    @IBAction func notificationButtonTapp(_ sender: UIButton) {
        let notificationVC = NotificationsVC.instantiate(fromAppStoryboard: .settingsAndChat)
        self.navigationController?.pushViewController(notificationVC, animated: true)
    }
    
    @IBAction func appliedJobButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        self.savedJobButton.isSelected = false
        self.appliedJobButton.isSelected = true
        self.shortListedJobButton.isSelected = false
        self.selectedVC = .appliedVC
//        self.shortlistedJobVC.getShortlistedJob(loader: false, page: 0, search: "")
        self.scrollView.setContentOffset(CGPoint.zero, animated: true)
        self.view.layoutIfNeeded()
    }
    
    @IBAction func shortlistedJobButtonTapped(_ sender: UIButton) {
        
        self.view.endEditing(true)
        AppUserDefaults.save(value: 0, forKey: .shortlistNotification)
//        self.shortlistBadgeLabel.isHidden = true
        self.savedJobButton.isSelected = false
        self.appliedJobButton.isSelected = false
        self.shortListedJobButton.isSelected = true
        self.selectedVC = .sortlistedVC
        self.scrollView.setContentOffset(CGPoint(x: 2 * SCREEN_WIDTH, y: 0), animated: true)
        self.view.layoutIfNeeded()
    }
    
    func scrollToShortList(){
        AppUserDefaults.save(value: 0, forKey: .shortlistNotification)
//        self.shortlistBadgeLabel.isHidden = true
        self.savedJobButton.isSelected = false
        self.appliedJobButton.isSelected = false
        self.shortListedJobButton.isSelected = true
        self.selectedVC = .sortlistedVC
        self.scrollView.setContentOffset(CGPoint(x: 2 * SCREEN_WIDTH, y: 0), animated: true)
        self.view.layoutIfNeeded()
    }
}

//    MARK:- UpdateAppliedJobListDelegate method for update applied job list.
extension MyJobsVC: UpdateAppliedJobListDelegate{
    
    func updateAppliedJobList() {
        self.appliedJobVC.getAppliedJob(loader: false, pageNo: 0, search: "")
    }
}

//    MARK:- ScrollView delegate
//    ===========================================
extension MyJobsVC: UIScrollViewDelegate{
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        self.underLineView.frame.origin = CGPoint(x: scrollView.contentOffset.x/3, y: self.underLineView.frame.origin.y)
        self.setButtonColor()
    }
    
    func setButtonColor(){
        
        if self.scrollView.contentOffset.x <= SCREEN_WIDTH/2 {
            
            self.savedJobButton.isSelected = false
            self.appliedJobButton.isSelected = true
            self.shortListedJobButton.isSelected = false
            self.selectedVC = .appliedVC
            self.shortlistedJobVC.getShortlistedJob(loader: false, page: 0, search: "")
            
        } else if self.scrollView.contentOffset.x <= 3*SCREEN_WIDTH/2{
            
            self.savedJobButton.isSelected = true
            self.appliedJobButton.isSelected = false
            self.shortListedJobButton.isSelected = false
            
        } else {
            self.savedJobButton.isSelected = false
            self.appliedJobButton.isSelected = false
            self.shortListedJobButton.isSelected = true
        }
    }
}

//    MARK:- Search Delegate
//    ===========================================

extension MyJobsVC: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        switch self.selectedVC {
        case .savedVC:
            self.savedSearch = ""
        case .appliedVC:
            self.appliedSearch = ""
        case .sortlistedVC:
            self.shortlistedSearch = ""
        }
        self.view.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        
        guard let text = searchBar.text else { return }
        
        if text.isEmpty {
            searchBar.textField?.resignFirstResponder()
        } else {
            
            switch self.selectedVC {
            case .savedVC:
                self.savedSearch = text
                self.savedJobVC.getSavedJob(loader: true, pageNo: 0, searchText: text)
            case .appliedVC:
                self.appliedSearch = text
                self.appliedJobVC.getAppliedJob(loader: true, pageNo: 0, search: text)
            case .sortlistedVC:
                self.shortlistedSearch = text
                self.shortlistedJobVC.getShortlistedJob(loader: true, page: 0, search: text)
            }
        }
    }
}

//MARK:- Location alert popup Delegate
//===========================================
extension MyJobsVC: AlertPopUpDelegate {
    
    func didTapAffirmativeButton() {
        //        TODO: Add settings
        instantiateViewController()
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: { (success) in
                
            })
        } else {
            // Fallback on earlier versions
        }
    }
    
    func didTapNegativeButton() {
        print_debug("location access denied")
        instantiateViewController()
    }
    
}
extension MyJobsVC : ShowShortlistBadge{
    func showBadge(show: Bool) {
        if show {
            self.shortlistBadgeLabel.isHidden = false
        }else{
            self.shortlistBadgeLabel.isHidden = true

        }
    }
}
