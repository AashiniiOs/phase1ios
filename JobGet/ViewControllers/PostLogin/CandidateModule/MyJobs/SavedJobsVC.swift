//
//  SavedJobsVC.swift
//  JobGet
//
//  Created by macOS on 27/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//
enum UserJobType: Int {
    
    case applied         = 1
    case shortlisted     = 2
    case saved           = 5
}


import UIKit
import CoreLocation
import DZNEmptyDataSet

class SavedJobsVC: BaseVC {
    
    //MARK:- Properties
    //==================
    let jobType = UserJobType.saved
    var pageNo = 0
    var shouldLoadMoreData = false
    var savedJobList = [CandidateJobList]()
    var isPaginationEnable = false
    var job_id = ""
    var recruiterId = ""
    var categoryId = ""
    var fromProfile = false
    weak var delegate: UpdateAppliedJobListDelegate?
    var currentLocation: CLLocation?
    
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var savedJobTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()

    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.getSavedJob(loader: false, pageNo: 0, searchText: "")

        if fromProfile{
            self.candidateAppliedJob(job_id: self.job_id)
        }
        fromProfile = false
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: Notification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: Notification.Name.UIKeyboardWillShow, object: nil)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
//MARK:- Private Methods
//=======================
private extension SavedJobsVC {
    
    func initialSetup() {
        self.savedJobTableView.keyboardDismissMode = .onDrag
        self.savedJobTableView.delegate = self
        self.savedJobTableView.dataSource = self
        self.savedJobTableView.emptyDataSetSource = self
        self.savedJobTableView.emptyDataSetDelegate = self
        self.searchBar.delegate = self
        self.savedJobTableView.enablePullToRefresh(tintColor: AppColors.themeBlueColor ,target: self, selector: #selector(refreshWhenPull(_:)))
        self.registerNib()
        customiseSearchBar()
        self.getSavedJob(loader: false, pageNo: 0, searchText: "")
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshApi), name: NSNotification.Name(rawValue: "Refresh_Saved_Job"), object: nil)
    }
    
    @objc func refreshApi(){
        self.getSavedJob(loader: false, pageNo: 0, searchText: "")
        
    }
    
    @objc func keyboardWillAppear() {
        //Do something here
        print_debug("keyboardWillAppear")
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.removeKeyboard))
        self.savedJobTableView.addGestureRecognizer(tap)
    }
    
    @objc func keyboardWillDisappear() {
        //Do something here
        print_debug("keyboardWillDisappear")
        if let gesture = savedJobTableView.gestureRecognizers {
            for recognizer in gesture {
                if recognizer is KeyboardTapGesture {
                    savedJobTableView.removeGestureRecognizer(recognizer)
                }
            }
        }
    }
    
    @objc func removeKeyboard(){
        
        self.view.endEditing(true)
    }
    
    
    private func customiseSearchBar(){
        
        self.searchBar.placeholder = StringConstants.Search
        self.searchBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        // TextField Color Customization
        let textFieldInsideSearchBar = self.searchBar.value(forKey: "searchField") as? UITextField
        
        textFieldInsideSearchBar?.borderStyle = .roundedRect
        textFieldInsideSearchBar?.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1.0)
        textFieldInsideSearchBar?.textColor = AppColors.gray152
        textFieldInsideSearchBar?.textAlignment = .left
        
        // Placeholder Customization
        if let textFieldInsideSearchBarLabel = textFieldInsideSearchBar?.value(forKey: "placeholderLabel") as? UILabel{
            textFieldInsideSearchBarLabel.textAlignment = .center
            textFieldInsideSearchBarLabel.font = AppFonts.Poppins_Light.withSize(15)
        }
        
        // Glass Icon Customization
        let glassIconView = textFieldInsideSearchBar?.leftView as? UIImageView
        glassIconView?.image = glassIconView?.image?.withRenderingMode(.alwaysTemplate)
        glassIconView?.tintColor = UIColor(red: 152/255, green: 152/255, blue: 152/255, alpha: 1.0)
        
    }
//    func getLocation() {
//
//        self.isPaginationEnable = false
//
//        DispatchQueue.main.async {
//
//            if CLLocationManager.authorizationStatus() == .denied {
//
//                CommonClass.delayWithSeconds(1.0, completion: {
//                    let popUpVc = AlertPopUpVC.instantiate(fromAppStoryboard: .Candidate)
//                    popUpVc.delegate = self
//                    popUpVc.modalPresentationStyle = .overCurrentContext
//                    popUpVc.modalTransitionStyle = .crossDissolve
//                    self.present(popUpVc, animated: true, completion: nil)
//                })
//            } else {
//                SharedLocationManager.fetchCurrentLocation { (location) in
//                    SharedLocationManager.locationManager.stopUpdatingLocation()
//                    self.currentLocation = location
//                    self.getSavedJob(loader: false, pageNo: 0, searchText: "")
//                }
//            }
//        }
//    }
    
    @objc func refreshWhenPull(_ sender: UIRefreshControl) {
        sender.endRefreshing()
        self.pageNo = 0
        self.shouldLoadMoreData = false
        self.isPaginationEnable = false
        self.getSavedJob(loader: false, pageNo: 0, searchText: "")
        
    }
    
    func registerNib() {
        self.savedJobTableView.register(UINib(nibName: "JobListCell", bundle: nil), forCellReuseIdentifier: "JobListCell")
        self.savedJobTableView.register(UINib(nibName: "LoadMoreTableViewCell", bundle: nil), forCellReuseIdentifier: "LoadMoreTableViewCell")
    }
    
    private func isLoadingIndexPath(_ indexPath: IndexPath) -> Bool {
        guard shouldLoadMoreData else { return false}
        
        return indexPath.row == self.savedJobList.count
    }
}


//MARK: - IB Action and Target
//===============================
extension SavedJobsVC {
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: - Web services
//======================
extension SavedJobsVC {
    
    func getSavedJob(loader: Bool, pageNo: Int, searchText: String) {
        
        var params = [ApiKeys.type.rawValue: self.jobType.rawValue, ApiKeys.page.rawValue: pageNo, "search": searchText] as [String : Any]
//        params[ApiKeys.latitude.rawValue]  = AppUserDefaults.value(forKey: .userLat).doubleValue
//        params[ApiKeys.longitude.rawValue] = AppUserDefaults.value(forKey: .userLong).doubleValue
//        
        print_debug(params)
        WebServices.getSaveJob(parameters: params, loader: loader, success: { (json) in
            var data = [CandidateJobList]()
            
            if pageNo == 0 {
                self.savedJobList.removeAll(keepingCapacity: false)
            }
            
            let jobs = json["data"]["jobs"].arrayValue
            
            for job in jobs {
                data.append(CandidateJobList(dict: job))
            }
            
            if self.shouldLoadMoreData {
                self.savedJobList.append(contentsOf: data)
            } else {
                self.savedJobList = data
            }
            self.pageNo = json["page"].intValue
            self.shouldLoadMoreData = json["next"].boolValue
            
            self.savedJobTableView.reloadData()
            
        }) { (error) in
            print_debug(error)
            CommonClass.showToast(msg: error.localizedDescription)
            
        }
    }
    func candidateAppliedJob(job_id: String) {
        
        let params = [ApiKeys.userId.rawValue: AppUserDefaults.value(forKey: .userId), ApiKeys.jobId.rawValue: job_id, ApiKeys.recruiterId.rawValue: recruiterId, "categoryId": self.categoryId] as [String : Any]
        
        WebServices.applyJob(parameters: params, success: { (json) in
            if let errorCode = json[ApiKeys.code.rawValue].int, errorCode == error_codes.success {
                let sceen = ResetPasswordPopupVC.instantiate(fromAppStoryboard: .Main)
                commingFrom = .jobList
                sceen.jobId = job_id
                sceen.delegate = self
                sceen.isFromSavedJob = true
                sceen.modalTransitionStyle = .crossDissolve
                sceen.modalPresentationStyle = .overFullScreen
                self.present(sceen, animated: true, completion: nil)
                self.delegate?.updateAppliedJobList()
                NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "Refresh_After_Report_User"), object: nil)
            } else {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
            
        }) { (error) in
            print_debug(error)
            CommonClass.showToast(msg: error.localizedDescription)
            
        }
    }
    
}

//MARK: - Table view Delegate and DataSource
//============================================
extension SavedJobsVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = self.savedJobList.count
        return shouldLoadMoreData ? count + 1 : count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isLoadingIndexPath(indexPath){
            guard let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.defaultReuseIdentifier, for: indexPath) as? LoadMoreTableViewCell else {
                fatalError("Failed to initialize LoadMoreTableViewCell")
                
            }
            self.isPaginationEnable = true
            return cell
        }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "JobListCell") as? JobListCell else { fatalError("invalid cell \(self)")
        }
        
       
        cell.descLabel.text = ""
        cell.shareButton.addTarget(self, action: #selector(self.shareJobButtonTapped(_:)), for: .touchUpInside)
        cell.applyButton.addTarget(self, action: #selector(applyButtonTapped), for: .touchUpInside)
        cell.populateCandidateJobDetail(self.savedJobList[indexPath.row])
        cell.descLabel.text = ""
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard self.shouldLoadMoreData else { return }
        
        if indexPath.row == self.savedJobList.count - 1 && self.shouldLoadMoreData && self.isPaginationEnable{  //for 2nd last cell
            getSavedJob(loader: false, pageNo: self.pageNo, searchText: "")
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
        let sceen = CandidateJobDetailVC.instantiate(fromAppStoryboard: .Candidate)
        sceen.jobId = self.savedJobList[indexPath.row].jobId
        sceen.delegate = self
        
        
        sceen.isSaved = self.savedJobList[indexPath.row].isSaved
        sceen.commingFromScreen = .jobListCell
        sceen.distance = self.savedJobList[indexPath.row].distance
        sceen.jobDetail  =  self.savedJobList[indexPath.row]
        sceen.commingFromScreen = .jobListCell
        if let detail = self.savedJobList[indexPath.row].recruiterDetail {
            sceen.recruiterDetail = detail
            sceen.recruiterId = detail.recruiterId
        }
        self.navigationController?.pushViewController(sceen, animated: true)
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    @objc func applyButtonTapped(_ sender:UIButton){
        guard let index = sender.tableViewIndexPath(self.savedJobTableView) else { return }
        self.job_id = self.savedJobList[index.row].jobId
        self.recruiterId = self.savedJobList[index.row].recruiterDetail?.recruiterId ?? ""
        self.getCandidateJobDetail(loader: true, job_id: job_id)
        
    }
    func getCandidateJobDetail(loader: Bool, job_id: String) {
        
        let value = AppUserDefaults.value(forKey: .userData)
        
        if value["isExperience"] == 0{
            let sceen = ProfileErrorPopUpVC.instantiate(fromAppStoryboard: .Main)
            sceen.delegate = self
            sceen.fromSavedJob = true
            sceen.modalTransitionStyle = .crossDissolve
            sceen.modalPresentationStyle = .overFullScreen
            self.present(sceen, animated: true, completion: nil)
            
        }else{
            
            candidateAppliedJob(job_id: job_id)
        }
    }
    
    //MARK:- Target Actions
    //=====================
    
    @objc func shareJobButtonTapped(_ sender: UIButton) {
        
        guard let idx = sender.tableViewIndexPath(self.savedJobTableView) else { return }
        self.displayShareSheet(shareContent: self.savedJobList[idx.row].jobShareUrl)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.view.endEditing(true)
    }
}


extension SavedJobsVC:  OnTapYesButton {
    
    func yesBtnTap() {
        let obj = YourProfileStep1VC.instantiate(fromAppStoryboard: .Recruiter)
        obj.job_id = self.job_id
        obj.recruiterId = self.recruiterId
        obj.fromTabbar = false
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    func editButtonTapped() {
    }
}
extension SavedJobsVC: ShouldOpenStatusPopup{
    
    
    func presentShortlistedPopup(jobApplyId: String, categoryId: String) {
        
        guard let index = self.savedJobList.index(where: { (model) -> Bool in
            return model.jobId == jobApplyId
        })else{return}
        
        self.savedJobList.remove(at: index)
        self.savedJobTableView.deleteRows(at: [[0,index]], with: .automatic)
        
    }
}

//    MARK:- Search Delegate
//    ===========================================

extension SavedJobsVC: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        guard let text = searchBar.text else { return }
        
        if text.isEmpty {
            searchBar.textField?.resignFirstResponder()
        } else {
            self.getSavedJob(loader: true, pageNo: 0, searchText: text)
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty{
            self.view.endEditing(true)
            self.getSavedJob(loader: true, pageNo: 0, searchText: "")
        }
    }
}


//MARK:- DZNEmptyDataSource and Delegate
//==========================================
extension SavedJobsVC: DZNEmptyDataSetDelegate, DZNEmptyDataSetSource {
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return #imageLiteral(resourceName: "icHomeNoJobs")
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: StringConstants.No_Saved_Jobs,                                                                           attributes: [NSAttributedStringKey.foregroundColor: AppColors.black46,
                                                                                                                                                  NSAttributedStringKey.font: AppFonts.Poppins_Bold.withSize(16)
            ])
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        return NSAttributedString(string: StringConstants.You_saved_jobs_yet_Save_that_you_apply_later,                                                                           attributes: [NSAttributedStringKey.foregroundColor: AppColors.gray152,
                                                                                                                                                                                                             NSAttributedStringKey.font: AppFonts.Poppins_Regular.withSize(12)                                                                                                                                                                                                            ])
    }
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}

//MARK:- Location alert popup Delegate
//===========================================
extension SavedJobsVC: AlertPopUpDelegate {
    
    func didTapAffirmativeButton() {
        //        TODO: Add settings
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: { (success) in
                
            })
        } else {
            // Fallback on earlier versions
        }
    }
    
    func didTapNegativeButton() {
        print_debug("location access denied")
        self.getSavedJob(loader: false, pageNo: 0, searchText: "")
    }
    
}
