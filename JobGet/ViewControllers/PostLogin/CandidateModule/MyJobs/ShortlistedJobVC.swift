//
//  ShortlistedJobVC.swift
//  JobGet
//
//  Created by macOS on 27/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import CoreLocation

protocol ShowShortlistBadge{
    func showBadge(show: Bool)
}

class ShortlistedJobVC: BaseVC {
    
    //MARK:- Properties
    //==================
    let jobType = UserJobType.shortlisted
    var pageNo = 0
    var shouldLoadMoreData = false
    var shortlistCandidateList = [CandidateJobList]()
    var isPaginationEnable = false
    var currentLocation: CLLocation?
    var delegate : ShowShortlistBadge?
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var shortListedTableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.getShortlistedJob(loader: false, page: 0, search: "")

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: Notification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: Notification.Name.UIKeyboardWillShow, object: nil)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
//MARK:- Private Methods
//=======================
private extension ShortlistedJobVC {
    
    func initialSetup() {
        self.shortListedTableView.keyboardDismissMode = .onDrag
        self.shortListedTableView.delegate = self
        self.shortListedTableView.dataSource = self
        self.shortListedTableView.emptyDataSetSource = self
        self.shortListedTableView.emptyDataSetDelegate = self
        self.searchBar.delegate = self
        self.shortListedTableView.enablePullToRefresh(tintColor: AppColors.themeBlueColor ,target: self, selector: #selector(refreshWhenPull(_:)))
        self.getShortlistedJob(loader: false, page: 0, search: "")
        self.registerNib()
        customiseSearchBar()
    }
    
    
    @objc func keyboardWillAppear() {
        //Do something here
        print_debug("keyboardWillAppear")
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.removeKeyboard))
        
        self.shortListedTableView.addGestureRecognizer(tap)
    }
    
    @objc func keyboardWillDisappear() {
        //Do something here
        print_debug("keyboardWillDisappear")
        
        if let gesture = shortListedTableView.gestureRecognizers {
            for recognizer in gesture {
                if recognizer is KeyboardTapGesture {
                    shortListedTableView.removeGestureRecognizer(recognizer)
                }
            }
        }
    }
    
    @objc func removeKeyboard(){
        
        self.view.endEditing(true)
    }
    private func customiseSearchBar(){
        
        self.searchBar.placeholder = StringConstants.Search
        self.searchBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        
        // TextField Color Customization
        let textFieldInsideSearchBar = self.searchBar.value(forKey: "searchField") as? UITextField
        
        textFieldInsideSearchBar?.borderStyle = .roundedRect
        textFieldInsideSearchBar?.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1.0)
        textFieldInsideSearchBar?.textColor = AppColors.gray152
        textFieldInsideSearchBar?.textAlignment = .left
        
        // Placeholder Customization
        if let textFieldInsideSearchBarLabel = textFieldInsideSearchBar?.value(forKey: "placeholderLabel") as? UILabel{
            
            textFieldInsideSearchBarLabel.textAlignment = .center
            textFieldInsideSearchBarLabel.font = AppFonts.Poppins_Light.withSize(15)
        }
        
        // Glass Icon Customization
        let glassIconView = textFieldInsideSearchBar?.leftView as? UIImageView
        glassIconView?.image = glassIconView?.image?.withRenderingMode(.alwaysTemplate)
        glassIconView?.tintColor = UIColor(red: 152/255, green: 152/255, blue: 152/255, alpha: 1.0)
        
    }
//    func getLocation() {
//
//        self.isPaginationEnable = false
//
//
//        DispatchQueue.main.async {
//
//            if CLLocationManager.authorizationStatus() == .denied {
//
//                CommonClass.delayWithSeconds(1.0, completion: {
//                    let popUpVc = AlertPopUpVC.instantiate(fromAppStoryboard: .Candidate)
//                    popUpVc.delegate = self
//                    popUpVc.modalPresentationStyle = .overCurrentContext
//                    popUpVc.modalTransitionStyle = .crossDissolve
//                    self.present(popUpVc, animated: true, completion: nil)
//                })
//            } else {
//                SharedLocationManager.fetchCurrentLocation { (location) in
//
//                    SharedLocationManager.locationManager.stopUpdatingLocation()
//                    self.currentLocation = location
//                    self.getShortlistedJob(loader: false, page: 0, search: "")
//                }
//            }
//        }
//    }
    @objc func refreshWhenPull(_ sender: UIRefreshControl) {
        sender.endRefreshing()
        self.pageNo = 0
        self.shouldLoadMoreData = false
        self.isPaginationEnable = false
        self.getShortlistedJob(loader: false, page: 0, search: "")
    }
    
    func registerNib() {
        self.shortListedTableView.register(UINib(nibName: "CandidateJobAppliedCell", bundle: nil), forCellReuseIdentifier: "CandidateJobAppliedCell")
        self.shortListedTableView.register(UINib(nibName: "LoadMoreTableViewCell", bundle: nil), forCellReuseIdentifier: "LoadMoreTableViewCell")
    }
    
    private func isLoadingIndexPath(_ indexPath: IndexPath) -> Bool {
        guard shouldLoadMoreData else { return false}        
        return indexPath.row == self.shortlistCandidateList.count
    }
}


//MARK: - IB Action and Target
//===============================
extension ShortlistedJobVC {
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK: - Web services
//======================
extension ShortlistedJobVC {
    
    func getShortlistedJob(loader: Bool, page: Int, search: String) {
        
        let params = [ApiKeys.type.rawValue: self.jobType.rawValue, ApiKeys.page.rawValue: page, "search": search] as [String : Any]
        
//        params[ApiKeys.latitude.rawValue]  = AppUserDefaults.value(forKey: .userLat).doubleValue
//        params[ApiKeys.longitude.rawValue] = AppUserDefaults.value(forKey: .userLong).doubleValue
//        
        print_debug(params)
        WebServices.getSaveJob(parameters: params, loader: loader, success: { (json) in
            var data = [CandidateJobList]()
            
            let jobs = json["data"]["jobs"].arrayValue
            
            if page == 0 {
                self.shortlistCandidateList.removeAll(keepingCapacity: false)
            }
            
            if jobs.count > 0 {
                self.delegate?.showBadge(show: true)
            }else{
                self.delegate?.showBadge(show: false)
            }
            
            for job in jobs {
                data.append(CandidateJobList(dict: job))
            }
            
            if self.shouldLoadMoreData {
                self.shortlistCandidateList.append(contentsOf: data)
            } else {
                self.shortlistCandidateList = data
            }
            
            self.pageNo = json["page"].intValue
            self.shouldLoadMoreData = json["next"].boolValue
            self.shortListedTableView.reloadData()
            
        }) { (error) in
            print_debug(error)
            CommonClass.showToast(msg: error.localizedDescription)
            
        }
    }
}

//MARK: - Table view Delegate and DataSource
//============================================
extension ShortlistedJobVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = self.shortlistCandidateList.count
        return shouldLoadMoreData ? count + 1 : count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isLoadingIndexPath(indexPath){
            guard let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.defaultReuseIdentifier, for: indexPath) as? LoadMoreTableViewCell else {
                fatalError("Failed to initialize LoadMoreTableViewCell")
            }
            self.isPaginationEnable = true
            return cell
        }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CandidateJobAppliedCell") as? CandidateJobAppliedCell else { fatalError("invalid cell \(self)")
        }
        
        
        cell.shareButton.addTarget(self, action: #selector(self.shareJobButtonTapped(_:)), for: .touchUpInside)
        cell.populateCandidateJobDetail(self.shortlistCandidateList[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard self.shouldLoadMoreData else { return }
        
        if indexPath.row == self.shortlistCandidateList.count - 1 && self.shouldLoadMoreData && isPaginationEnable{
            getShortlistedJob(loader: false, page: self.pageNo, search: "")
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
        let sceen = CandidateJobDetailVC.instantiate(fromAppStoryboard: .Candidate)
        sceen.jobId = self.shortlistCandidateList[indexPath.row].jobId
        
        sceen.isSaved = self.shortlistCandidateList[indexPath.row].isSaved
        sceen.commingFromScreen = .jobListCell
        sceen.distance = self.shortlistCandidateList[indexPath.row].distance
        sceen.jobDetail  =  self.shortlistCandidateList[indexPath.row]
        if let detail = self.shortlistCandidateList[indexPath.row].recruiterDetail {
            sceen.recruiterDetail = detail
        }
        self.navigationController?.pushViewController(sceen, animated: true)
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    //MARK:- Target Actions
    //=====================
    
    @objc func shareJobButtonTapped(_ sender: UIButton) {
        
        guard let idx = sender.tableViewIndexPath(self.shortListedTableView) else { return }
        self.displayShareSheet(shareContent: self.shortlistCandidateList[idx.row].jobShareUrl)
    }
}

//    MARK:- Search Delegate
//    ===========================================

extension ShortlistedJobVC: UISearchBarDelegate {
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
        guard let text = searchBar.text else { return }
        
        if text.isEmpty {
            searchBar.textField?.resignFirstResponder()
        } else {
            self.getShortlistedJob(loader: true, page: 0, search: text)
        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty{
            self.view.endEditing(true)
            self.getShortlistedJob(loader: true, page: 0, search: "")
        }
    }
}

//MARK:- DZNEmptyDataSource and Delegate
//==========================================
extension ShortlistedJobVC: DZNEmptyDataSetDelegate, DZNEmptyDataSetSource {
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return #imageLiteral(resourceName: "icHomeNoJobs")
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: StringConstants.No_Shortlisted_Jobs,                                                                           attributes: [NSAttributedStringKey.foregroundColor: AppColors.black46,
                                                                                                                                                        NSAttributedStringKey.font: AppFonts.Poppins_Bold.withSize(16)
            ])
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        return NSAttributedString(string: "",                                                                           attributes: [NSAttributedStringKey.foregroundColor: AppColors.gray152,
                                                                                                                                     NSAttributedStringKey.font: AppFonts.Poppins_Regular.withSize(12)                                                                                                                                                                                                            ])
    }
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}

//MARK:- Location alert popup Delegate
//===========================================
extension ShortlistedJobVC: AlertPopUpDelegate {
    
    func didTapAffirmativeButton() {
        //        TODO: Add settings
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: { (success) in
                
            })
        } else {
            // Fallback on earlier versions
        }
    }
    
    func didTapNegativeButton() {
        print_debug("location access denied")
        self.getShortlistedJob(loader: false, page: 0, search: "")
    }
    
}

