//
//  SortPopUpVC.swift
//  JobGet
//
//  Created by appinventiv on 18/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

protocol SortPopUpDelegate:class {
    func didApplyFilter(selecteIndex: Int)
}

class SortPopUpVC: BaseVC {
    
    //    MARK:- properties
    //    ==================================
    weak var delegate: SortPopUpDelegate?
    var selectedIndex = 0
    private let sortByArray: [String] = [StringConstants.Newest, StringConstants.Nearby]
    
    //    MARK:- IBoutlets
    //    ==================================
    @IBOutlet weak var sorLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var okButton: UIButton!
    @IBOutlet weak var popupTable: UITableView!
    
    //    MARK:- Life cycle methods
    //    ==================================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        
    }
    
    //    MARK:- functions
    //    ==================================
    private func initialSetup(){
        self.popupTable.dataSource = self
        self.popupTable.delegate = self
        
        let image = #imageLiteral(resourceName: "ic_clear_").withRenderingMode(.alwaysTemplate)
        self.closeButton.setImage(image, for: .normal)
        self.closeButton.imageView?.tintColor = AppColors.appGrey
        
        self.okButton.setCorner(cornerRadius: 5, clip: true)
    }
    
    //    MARK:- IBActions
    //    ==================================
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    @IBAction func okButtonTapped(_ sender: UIButton) {
        
        self.delegate?.didApplyFilter(selecteIndex: self.selectedIndex)
        self.dismiss(animated: true, completion: nil)
    }
    
}

//    MARK:- Tableview datasource
//    ==================================
extension SortPopUpVC: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.sortByArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SorttPopUpCell", for: indexPath) as! SorttPopUpCell
        cell.sortTypeLabel.text = self.sortByArray[indexPath.row]
        
        if indexPath.row == self.selectedIndex {
            cell.radioImage.image = #imageLiteral(resourceName: "ic_sort_by_selected1")
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        } else if indexPath.row == 0 {
            cell.seperatorView.isHidden = false
        }
        return cell
    }
    
}

//    MARK:- Tableview delegate
//    ==================================
extension SortPopUpVC: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return tableView.bounds.height / 2
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.row
        guard let cell = tableView.cellForRow(at: indexPath) as? SorttPopUpCell else { return }
        cell.radioImage.image = #imageLiteral(resourceName: "ic_sort_by_selected1")
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        self.selectedIndex = 0
        guard let cell = tableView.cellForRow(at: indexPath) as? SorttPopUpCell else { return }
        cell.radioImage.image = #imageLiteral(resourceName: "ic_sort_by_deselected1")
        
    }
}

//    MARK:- SortPopUpCell
//    ==================================

class SorttPopUpCell: UITableViewCell{
    
    //MARK: - Properties
    //==================
    // var selectedIndex: Int = -1
    
    @IBOutlet weak var sortTypeLabel: UILabel!
    @IBOutlet weak var radioImage: UIImageView!
    
    @IBOutlet weak var seperatorView: UIView!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if selected{
            self.radioImage.image = #imageLiteral(resourceName: "ic_sort_by_selected1")
        }else{
            self.radioImage.image = #imageLiteral(resourceName: "ic_sort_by_deselected1")
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.radioImage.image = #imageLiteral(resourceName: "ic_sort_by_deselected1")
        self.seperatorView.isHidden = true
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
    }
}
