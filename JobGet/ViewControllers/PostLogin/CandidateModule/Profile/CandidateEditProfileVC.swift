//
//  CandidateEditProfileVC.swift
//  JobGet
//
//  Created by Appinventiv on 08/06/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import SwiftyJSON
import GooglePlaces
import GoogleMaps
import GooglePlacePicker
import CircularSlider
import Firebase

protocol RefreshCandidateProfileDelegate: class {
    
    func refreshCandidateProfileDelegate(candidateData: CandidateAppliedJobDetail)
}

class CandidateEditProfileVC: UIViewController {
    
    // MARK: - Outlets.
    // MARK: -
    
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var editProfileTableView: UITableView!
    @IBOutlet weak var navigationLabel: UILabel!
    
    // MARK: - Proerties.
    // MARK: -
    var candidateData: CandidateAppliedJobDetail?
    weak var delegate:RefreshCandidateProfileDelegate?
    var index: Int?
    var isUploadPic = false
    
    var isFirstTimeTVIncreaseHeight = false
    var isStateTxtFldEnableEditing = false
    
    // MARK: - View life cycle.
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions.
    // MARK: -
    
    @IBAction func cameraButtonTapp(_ sender: UIButton) {
        if self.isUploadPic{
            CommonClass.showToast(msg: StringConstants.Please_wait_profile_image_uploading)
            return
        }
        self.captureImage(on: self)
        
    }
    
    @IBAction func backButtonTapp(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveButtonTapp(_ sender: UIButton) {
        self.view.endEditing(true)
        if self.checkValidity() {
            self.editProfileService()
        }
        
    }
    
    
    // MARK: - Target.
    // MARK: -
    @objc func touchTapped(_ sender: UITapGestureRecognizer) {
        let obj = AddExperienceStepVC.instantiate(fromAppStoryboard: .Recruiter)
        
        obj.fromDidSelectRowAt = true
        obj.changeExpDelegate = self
        if let cellBackgroundView = sender.view {
            obj.row = cellBackgroundView.tag
        }
        
        var candidateExp = CandidateProfileModel(withJSON: JSON([:]))
        if let candidateData = self.candidateData {
            candidateExp.experience = candidateData.experience
        }
        
        obj.candidateDetail = candidateExp
        self.navigationController?.pushViewController(obj, animated: true)
    }
}

// MARK: - Change Experiance Protocol
// MARK: -
extension CandidateEditProfileVC : ChangeExpProtocol {
    
    func chageExp(experiance: ExperienceModel, arayIndex: Int) {
        
        if var candidateDetail = self.candidateData {
            
            print_debug(candidateDetail.experience)
            candidateDetail.experience.remove(at: arayIndex)
            candidateDetail.experience.insert(experiance, at: arayIndex)
            self.candidateData = candidateDetail
            print_debug(candidateDetail.experience)
            editProfileTableView.reloadData()
        }
    }
}

// MARK: - Private methods.
// MARK: -
private extension CandidateEditProfileVC{
    
    func initialSetup(){
        
        self.editProfileTableView.delegate = self
        self.editProfileTableView.dataSource = self
        self.editProfileTableView.tableHeaderView = self.headerView
        guard let userData = self.candidateData else{ return }
        self.profileImageView.backgroundColor = UIColor.lightGray
        self.profileImageView.sd_setImage(with: URL(string: userData.user_image), placeholderImage: nil, options: [], completed: nil)
        self.registerNib()
    }
    
    func registerNib() {
        
        let nib = UINib(nibName: "CandidateEditNameCell", bundle: nil)
        self.editProfileTableView.register(nib, forCellReuseIdentifier: "CandidateEditNameCell") //
        
        let eduNib = UINib(nibName: "EduCell", bundle: nil)
        self.editProfileTableView.register(eduNib, forCellReuseIdentifier: "EduCell")
    }
    
    func editProfileService(){
        
        guard let userData = self.candidateData else{return}
        var params = [String:Any]()
        
        let dic = CommonClass().convertToApiFormat(userData.experience.map({$0.dictionaryValue}))?.components(separatedBy: .newlines)
        params["firstName"] = userData.first_name
        params["lastName"] = userData.last_name
        params["email"] = userData.email
        params["address"] = userData.jobLocation
        params["isSearch"] = userData.isSearch
        params["jobType"] = userData.jobType
        
        params["state"] = userData.state
        
        params["city"] = userData.city
        params["experience"] = dic?.joined()
        params["education"] = userData.education
        params["about"] = userData.about
        params["userImage"] = userData.user_image
        
        params["isExperience"] = userData.isExperience
        
//        params["userImage"] = userData.user_image
        
        params["isExperience"] = userData.isExperience
        params["longitude"] = userData.jobLongitude
        
        params["latitude"] = userData.jobLatitude
        
        print_debug(params)
        WebServices.addProfile(parameters:params, loader: true, success: { (dict) in
            AppNetworking.hideLoader()
            if dict["code"].intValue == error_codes.success{
                print_debug(dict)
                let jobs = dict["data"]
                self.candidateData = CandidateAppliedJobDetail(dict: jobs)
                self.updateFirebaseDetail()
                if let userData = self.candidateData{
                    self.delegate?.refreshCandidateProfileDelegate(candidateData:userData )
                }
//                AppUserDefaults.save(value: userData.jobLatitude, forKey: .userLat)
//                AppUserDefaults.save(value: userData.jobLongitude, forKey: .userLong)

                CommonClass.showToast(msg: StringConstants.Profile_updated_successfully)
                self.navigationController?.popViewController(animated: true)
                
            }else{
                
                let msg = dict["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
            
        }) { (error) in
            AppNetworking.hideLoader()
            self.showAlert(msg: error.localizedDescription)
        }
    }
    
    func updateFirebaseDetail() {
        
        if let candidateDetail = self.candidateData {
            print_debug("\(candidateDetail.user_id)" )
            var userDetails: [String: Any] = [:]
            if let fcmTocken = Messaging.messaging().fcmToken{
                 userDetails[ChatEnum.User.deviceToken.rawValue] = fcmTocken
            }
           
            userDetails[ChatEnum.User.email.rawValue] =  candidateDetail.email
            userDetails[ChatEnum.User.firstName.rawValue] =  candidateDetail.first_name
            userDetails[ChatEnum.User.lastName.rawValue] =   candidateDetail.last_name
            userDetails[ChatEnum.User.mobileNumber.rawValue] =  candidateDetail.mobile
            userDetails[ChatEnum.User.userImage.rawValue] =  candidateDetail.user_image
            userDetails[ChatEnum.User.userId.rawValue] =  User.getUserModel().user_id ?? ""
            userDetails[ChatEnum.User.isOnline.rawValue] = true  //false
            userDetails[ChatEnum.User.deviceType.rawValue] = Device_Type.iOS.rawValue
            
            ChatHelper.updateUserDetails(userId:  User.getUserModel().user_id ?? "", details: userDetails)
        }
    }
    
    func checkValidity() -> Bool {
        
        
        guard let userData = self.candidateData else{return false}
        if userData.first_name.isEmpty {
            CommonClass.showToast(msg: StringConstants.Enter_First_Name.localized)
            return false
        }else{
            if userData.first_name.count < 2 {
                CommonClass.showToast(msg: StringConstants.First_Name_Invalid_Length.localized)
                return false
            }
        }
        
        if userData.last_name.isEmpty {
            CommonClass.showToast(msg: StringConstants.Enter_Last_Name.localized)
            return false
        }else{
            if userData.last_name.count < 1 {
                CommonClass.showToast(msg: StringConstants.Last_Name_Invalid_Length.localized)
                return false
            }
        }
        
        if !userData.email.isEmpty{
            if userData.email.checkIfInvalid(.email) {
                CommonClass.showToast(msg:  StringConstants.Enter_Email.localized)
                return false
            }
        } else {
            CommonClass.showToast(msg: StringConstants.Enter_Email.localized)
            return false
        }
        
        return true
    }
}

//MARK: PRIVATE FUNCTIONS
extension CandidateEditProfileVC{
    
    private func uploadImage(image : UIImage) {
        
       AppNetworking.showUploadImageLoader()
        image.uploadImageToS3( success: { (uploaded : Bool, imageUrl : String) in
            if uploaded {
                self.candidateData?.user_image = imageUrl
                self.isUploadPic = false
                
              AppNetworking.hideLoader()
                
            }
        }, progress: { (uploadPercentage : CGFloat) in
            print_debug(uploadPercentage)
            self.isUploadPic = true
        }, failure: { (err : Error) in
            
            if (err.localizedDescription  == StringConstants.Internet_connection_appears_offline) {
                CommonClass.showToast(msg: StringConstants.Please_check_your_internet_connection)
            }
            
            AppNetworking.hideLoader()
        })
    }
    
}
//MARK:- ImagePickerController & NavigationController Delegate
//============================================================
extension CandidateEditProfileVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let imagePicked = info[UIImagePickerControllerOriginalImage] as? UIImage{
            
            let frameRect = CGRect(x: 50, y: self.view.center.y, width: self.view.frame.width - 100, height: 180)
            Cropper.shared.openCropper(withImage: imagePicked, mode: .custom(frameRect), on: self)
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        
    }
}


//MARK: Extension AppInventivCropperDelegate
//MARK:
extension CandidateEditProfileVC: CropperDelegate {
    
    
    func imageCropperDidCancelCrop() {
        
        print_debug("Crop cancelled")
        
    }
    func imageCropper(didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect) {
        
        
        self.profileImageView.contentMode = .scaleAspectFill
        self.profileImageView.clipsToBounds = true
        self.profileImageView.image = croppedImage
        self.uploadImage(image: croppedImage)
    }
}
// MARK: - Google Place Picker
//======================================
extension CandidateEditProfileVC: GMSPlacePickerViewControllerDelegate {
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        self.candidateData?.jobLatitude = place.coordinate.latitude
        self.candidateData?.jobLongitude = place.coordinate.longitude
        
        CLLocation(latitude: place.coordinate.latitude, longitude: place.coordinate.longitude).convertToPlaceMark { (place) in
            self.candidateData?.city = place.locality ?? ""
            self.candidateData?.state = place.administrativeArea ?? ""

        }
//        guard let componentAddress = place.addressComponents else { return }
//        for component in componentAddress {
//            if component.type == "locality" {
//                self.candidateData?.city = component.name
//            }else if component.type == "administrative_area_level_1" {
//                self.candidateData?.state = component.name
//            }
//        }
        if let address = place.formattedAddress {
            self.candidateData?.jobLocation = String(describing: address)
        }
        self.editProfileTableView.reloadData()
        viewController.dismiss(animated: true, completion: nil)
        
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        
    }
    
}


// MARK: - Table view delegate and datasource methods.
// MARK: -
extension CandidateEditProfileVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            return 3
        }else if section == 1  {
            return 1
        } else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section{
        case 0:
            switch indexPath.row{
            case 0:
                
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "CandidateEditNameCell", for: indexPath) as? CandidateEditNameCell else{fatalError("CandidateEditNameCell not found.")}
                cell.firstNameTxtField.delegate = self
                cell.lastNameTxtField.delegate = self
                cell.emailTxtField.delegate = self
                cell.locationTxtField.delegate = self
                
                if let data = self.candidateData {
                    cell.firstNameTxtField.text = data.first_name
                    cell.lastNameTxtField.text = data.last_name
                    cell.emailTxtField.text = data.email
                    cell.locationTxtField.text = data.jobLocation
                }
                cell.editLocationButton.addTarget(self, action: #selector(self.toggleMapBtnTapp(_:)), for: .touchUpInside)
                cell.locationButton.addTarget(self, action: #selector(self.locationButtonTapp(_:)), for: .touchUpInside)
                return cell
            case 1:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "CandidateAboutCell", for: indexPath) as? CandidateAboutCell else{fatalError("CandidateAboutCell not found.")}
                
                
                CommonClass.setTLFoatTextView(textView: cell.aboutTextView, title: StringConstants.About_Me)
                if let data = self.candidateData{
                    cell.aboutTextView.text = data.about.trailingSpacesTrimmed
                }
                
                cell.aboutTextView.delegate = self
                self.textViewHeightChange(cell.textViewHeight,textView: cell.aboutTextView)
                
                return cell
                
            default:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "CandidateJobTypeCell", for: indexPath) as? CandidateJobTypeCell else{fatalError("CandidateJobTypeCell nit found.")}
                
                
                if let data = self.candidateData {
                    
                    if data.jobType == 1 {
                        cell.fullTimeButton.isSelected = false
                        cell.bothButton.isSelected = false
                        cell.partTimeButton.isSelected = true
                    } else if data.jobType == 2 {
                        cell.fullTimeButton.isSelected = true
                        cell.bothButton.isSelected = false
                        cell.partTimeButton.isSelected = false
                    } else if data.jobType == 3 {
                        
                        cell.fullTimeButton.isSelected = false
                        cell.bothButton.isSelected = true
                        cell.partTimeButton.isSelected = false
                    }
                    
                    if data.isSearch == 1{
                        cell.noButton.isSelected = false
                        cell.alertSearchLabel.text = ""
                        cell.yesButton.isSelected = true
                    }else if data.isSearch == 0{
                        cell.noButton.isSelected = true
                        cell.yesButton.isSelected = false
                    }
                }
                cell.yesButton.addTarget(self, action: #selector(self.yesButtonTapp(_:)), for: .touchUpInside)
                
                cell.noButton.addTarget(self, action: #selector(self.noButtonTapp(_:)), for: .touchUpInside)
                
                cell.bothButton.addTarget(self, action: #selector(self.bothButtonTapped(_:)), for: .touchUpInside)
                cell.fullTimeButton.addTarget(self, action: #selector(self.fullTimeButtonTapped(_:)), for: .touchUpInside)
                cell.partTimeButton.addTarget(self, action: #selector(self.partTimeButtonTapped(_:)), for: .touchUpInside)
                
                return cell
            }
        case 1:
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CandidateProfileExperienceCell", for: indexPath) as? CandidateProfileExperienceCell else{fatalError("CandidateProfileExperienceCell nit found.")}
            
            for view in cell.experienceStackView.arrangedSubviews {
                view.removeFromSuperview()
            }
            
            if let data = self.candidateData{
                var index = 0
                let _ = data.experience.map{value in
                    
                    guard let rowView = UIView.loadFromNibNamed("AddExperienceView") as? AddExperienceView else{fatalError("AddExperienceView not found.")}
                    
                    rowView.populatedCell(data: data, index: index)
                    rowView.minusButton.tag = index
                    rowView.cellBackgroundView.tag = index
                    index += 1
                    rowView.minusButton.addTarget(self, action: #selector(self.removeExperienceButtonTapped(_:)), for: .touchUpInside)
                    
                    let tap = UITapGestureRecognizer(target: self, action: #selector(self.touchTapped(_:)))
                    print_debug(index)
                    print_debug(data.experience.count)
                    if index == data.experience.count  {
                        rowView.bottomSeperatorView.isHidden = true
                    }
                    rowView.layoutIfNeeded()
                    rowView.cellBackgroundView.addGestureRecognizer(tap)
                    
                    cell.experienceStackView.addArrangedSubview(rowView)
                }
            }
            
            
            cell.addMoreExperienceButton.addTarget(self, action: #selector(self.addExperienceButtonTapped(_:)), for: .touchUpInside)
            return cell
            
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CandidateAboutCell", for: indexPath) as? CandidateAboutCell else{fatalError("CandidateAboutCell nit found.")}
            
            CommonClass.setTLFoatTextView(textView: cell.aboutTextView, title: StringConstants.Education)
            
            if let data = self.candidateData{
                cell.aboutTextView.text = data.education.trailingSpacesTrimmed
            }
            cell.aboutTextView.delegate = self
            self.isFirstTimeTVIncreaseHeight = false
            self.textViewHeightChange(cell.textViewHeight,textView: cell.aboutTextView)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    @objc func locationButtonTapp(_ sender: UIButton) {
        guard let cell = sender.tableViewCell as? CandidateEditNameCell else{return}
        if cell.editLocationButton.isSelected{
            return
        }
        //        if CLLocationManager.authorizationStatus() == .denied {
        //            CommonClass.delayWithSeconds(1.0, completion: {
        //                let popUpVc = AlertPopUpVC.instantiate(fromAppStoryboard: .Candidate)
        //                popUpVc.delegate = self
        //                popUpVc.modalPresentationStyle = .overCurrentContext
        //                popUpVc.modalTransitionStyle = .crossDissolve
        //                self.present(popUpVc, animated: true, completion: nil)
        //            })
        //        }else{
        let northEast = CLLocationCoordinate2DMake(AppUserDefaults.value(forKey: .userLat).doubleValue + 0.001, AppUserDefaults.value(forKey: .userLong).doubleValue + 0.001)
        let southWest = CLLocationCoordinate2DMake(AppUserDefaults.value(forKey: .userLat).doubleValue - 0.001, AppUserDefaults.value(forKey: .userLong).doubleValue - 0.001)
        let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
        let config = GMSPlacePickerConfig(viewport: viewport)
        
        let placePicker = GMSPlacePickerViewController(config: config)
        placePicker.delegate = self
        present(placePicker, animated: true, completion: nil)
        //        }
    }
    
    @objc func toggleMapBtnTapp(_ sender: UIButton) {
        guard let cell = sender.tableViewCell as? CandidateEditNameCell else{return}
        if cell.locationTxtField.hasText {
            sender.isSelected = !sender.isSelected
            self.isStateTxtFldEnableEditing = !self.isStateTxtFldEnableEditing
            self.view.endEditing(true)
        }
    }
    @objc func fullTimeButtonTapped(_ sender : UIButton){
        sender.isSelected = !sender.isSelected
        guard let cell = sender.tableViewCell as? CandidateJobTypeCell else{return}
        cell.partTimeButton.isSelected = false
        self.candidateData?.jobType = 2
        cell.bothButton.isSelected = false
    }
    
    @objc func partTimeButtonTapped(_ sender : UIButton){
        sender.isSelected = !sender.isSelected
        guard let cell = sender.tableViewCell as? CandidateJobTypeCell else{return}
        cell.fullTimeButton.isSelected = false
        cell.bothButton.isSelected = false
        self.candidateData?.jobType = 1
    }
    
    @objc func bothButtonTapped(_ sender : UIButton){
        sender.isSelected = !sender.isSelected
        guard let cell = sender.tableViewCell as? CandidateJobTypeCell else{return}
        cell.fullTimeButton.isSelected = false
        cell.partTimeButton.isSelected = false
        self.candidateData?.jobType = 3
        
    }
    
    @objc func addExperienceButtonTapped(_ sender : UIButton){
        let addExperienceScene = AddExperienceStepVC.instantiate(fromAppStoryboard: .Recruiter)
        addExperienceScene.isOpenEditProfile = true
        addExperienceScene.delegate = self
        self.present(addExperienceScene, animated: true, completion: nil)
    }
    
    @objc func removeExperienceButtonTapped(_ sender : UIButton){
        self.index = sender.tag
        let popUpVc = RemoveExperiancePopupVC.instantiate(fromAppStoryboard: .RecruiterProfile)
        popUpVc.delegate = self
        popUpVc.modalPresentationStyle = .overCurrentContext
        popUpVc.modalTransitionStyle = .crossDissolve
        self.present(popUpVc, animated: true, completion: nil)
    }
    
    @objc func yesButtonTapp(_ sender: UIButton){
        UIView.setAnimationsEnabled(false)
        sender.isSelected = true
        guard let cell = sender.tableViewCell as? CandidateJobTypeCell else{return}
        cell.noButton.isSelected = false
        self.candidateData?.isSearch = 1
    }
    
    @objc func noButtonTapp(_ sender: UIButton){
        UIView.setAnimationsEnabled(false)
        sender.isSelected = true
        guard let cell = sender.tableViewCell as? CandidateJobTypeCell else{return}
        cell.yesButton.isSelected = false
        self.candidateData?.isSearch = 0
    }
}

//MARK:- UITextFieldDelegate method
//=========================
extension CandidateEditProfileVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let indexPath = textField.tableViewIndexPath(self.editProfileTableView) else{return false}
        
        if indexPath.section == 0, indexPath.row == 0 {
            
            guard let cell = textField.tableViewCell as? CandidateEditNameCell else { return false }
            if textField === cell.firstNameTxtField ||  textField === cell.lastNameTxtField {
                if (textField.text ?? "").count == 0, string == " " {
                    return false
                } else if (textField.text ?? "").count - range.length < 200 {
                    
                    let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
                    let filtered = string.components(separatedBy: cs).joined(separator: "")
                    
                    return (string == filtered)
                    
                } else {
                    return false
                }
            } else if textField === cell.emailTxtField {
                if string == " " {
                    return false
                } else {
                    return (textField.text ?? "").count - range.length < 50
                }
            }else if textField === cell.locationTxtField {
                return true
            } else {
                return false
            }
        } else {
            return true
        }
        
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        guard let indexPath = textField.tableViewIndexPath(self.editProfileTableView) else{return false}
        if indexPath.section == 0 {
            guard let cell = textField.tableViewCell as? CandidateEditNameCell else { return false }
            if textField === cell.locationTxtField, !isStateTxtFldEnableEditing {
                //                if CLLocationManager.authorizationStatus() == .denied {
                //                    CommonClass.delayWithSeconds(1.0, completion: {
                //                        let popUpVc = AlertPopUpVC.instantiate(fromAppStoryboard: .Candidate)
                //                        popUpVc.delegate = self
                //                        popUpVc.modalPresentationStyle = .overCurrentContext
                //                        popUpVc.modalTransitionStyle = .crossDissolve
                //                        self.present(popUpVc, animated: true, completion: nil)
                //                    })
                //                }else{
                let northEast = CLLocationCoordinate2DMake(AppUserDefaults.value(forKey: .userLat).doubleValue + 0.001, AppUserDefaults.value(forKey: .userLong).doubleValue + 0.001)
                let southWest = CLLocationCoordinate2DMake(AppUserDefaults.value(forKey: .userLat).doubleValue - 0.001, AppUserDefaults.value(forKey: .userLong).doubleValue - 0.001)
                let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
                let config = GMSPlacePickerConfig(viewport: viewport)
                
                let placePicker = GMSPlacePickerViewController(config: config)
                placePicker.delegate = self
                present(placePicker, animated: true, completion: nil)
                //                }
                return false
            }else if textField === cell.emailTxtField{
                
                textField.autocorrectionType = .no
                return true
            } else {
                return true
            }
        }
        else{
            return true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let indexPath = textField.tableViewIndexPath(self.editProfileTableView) else{return}
        
        if indexPath.section == 0, indexPath.row == 0 {
            
            guard let cell = textField.tableViewCell as? CandidateEditNameCell else { return }
            if textField === cell.firstNameTxtField {
                self.candidateData?.first_name = textField.text ?? ""
            } else if textField === cell.lastNameTxtField {
                self.candidateData?.last_name = textField.text ?? ""
            } else if textField === cell.emailTxtField {
                self.candidateData?.email = textField.text ?? ""
            } else if textField === cell.locationTxtField {
                self.candidateData?.jobLocation = textField.text ?? ""
            } else {
                return
            }
        }else if indexPath.section == 2 {
            self.candidateData?.education = textField.text ?? ""
            
        }
    }
}

//MARK:- UITextViewDelegate method
//=========================
extension CandidateEditProfileVC: UITextViewDelegate{
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        self.isFirstTimeTVIncreaseHeight = false
        return true
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        guard let index = textView.tableViewIndexPath(self.editProfileTableView) else {return}
        
        if index.section == 0, index.row == 1 {
            self.candidateData?.about = textView.text ?? "".trailingSpacesTrimmed
        } else if index.section == 2, index.row == 0 {
            self.candidateData?.education = textView.text ?? "".trailingSpacesTrimmed
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        UIView.setAnimationsEnabled(false)
        self.editProfileTableView.beginUpdates()
        guard let cell = textView.tableViewCell as? CandidateAboutCell else{return}
        self.textViewHeightChange(cell.textViewHeight,textView: textView)
        self.editProfileTableView.endUpdates()
        UIView.setAnimationsEnabled(true)
    }
    
    
    func textViewHeightChange(_ textViewHeight: NSLayoutConstraint, textView: UITextView){
        
        self.editProfileTableView.beginUpdates()
        
        let topBottomPadding: CGFloat = 20.0
        let threeLineTextHeight: CGFloat = 95.0
        let fixedWidth = textView.frame.size.width
        let fixedHeight = textView.frame.size.height
        
        let maximumTextHeight: CGFloat = threeLineTextHeight + topBottomPadding
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: fixedHeight))
        let textHeight = textView.text.heightWithConstrainedWidth(width: textView.width-10.0, font: textView.font!)
        
        if textHeight > threeLineTextHeight {
            
            textView.isScrollEnabled = true
            textViewHeight.constant = maximumTextHeight
            
        } else {
            textView.isScrollEnabled = false
            
            if textView.text.isEmpty{
                textViewHeight.constant = 50
                textView.frame.size = CGSize(width: max(newSize.width, fixedWidth), height:50)
            }else{
                textView.frame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
                if self.isFirstTimeTVIncreaseHeight{
                    textViewHeight.constant = newSize.height + 20
                }else{
                    textViewHeight.constant = newSize.height
                }
            }
        }
        
        self.view.layoutIfNeeded()
        self.editProfileTableView.endUpdates()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        print_debug(newText.count)
        
        if text == ""{
            return true
        }
        if newText.count > 280{
            showAlert(msg:  StringConstants.job_decription_text_limit)
        }
        return newText.count < 280
    }
}

extension CandidateEditProfileVC:  AlertPopUpDelegate {
    
    func didTapAffirmativeButton() {
        
        if let idx = self.index {
            self.candidateData?.experience.remove(at: idx)
            self.editProfileTableView.reloadData()
        }
    }
    
    func didTapNegativeButton() {
        // TO DO TASK
    }
}

//MARK:- AddExperienceDelegate method for add experience.
extension CandidateEditProfileVC: AddExperienceDelegate{
    func addExperience(experienceData: CandidateProfileModel) {
        
        let _ = experienceData.experience.map{ value in
            self.candidateData?.experience.append(value)
        }
        
        self.editProfileTableView.reloadData()
    }
}

//MARK:- Prototype Cell
//=========================
class CandidateFullNameCell: UITableViewCell {
    
    //MARK:- Properties
    //==================
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var nameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var cornerRadiusView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //        self.shadowView.setShadow()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.shadowView.setCorner(cornerRadius: 5, clip: false)
        self.shadowView.dropShadow(color: UIColor.gray, opacity:0.2, offSet: CGSize.zero, radius: 1.0)
    }
    
}

class CandidateAboutCell: UITableViewCell {
    
    //MARK:- Properties
    //==================
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var aboutTextView: TLFloatLabelTextView!
    @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var cornerRadiusView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.cornerRadiusView.setCorner(cornerRadius: 5.0, clip: true)
        self.adjustTextViewHeight(textview: self.aboutTextView)
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.shadowView.setCorner(cornerRadius: 5, clip: false)
        self.shadowView.dropShadow(color: UIColor.gray, opacity:0.2, offSet: CGSize(width: 1.0, height: 1.0), radius: 1.0)
        self.shadowView.clipsToBounds = true
    }
    
    func adjustTextViewHeight(textview : UITextView) {
        
        self.aboutTextView.textContainer.lineBreakMode = .byWordWrapping
        self.aboutTextView.isScrollEnabled = false
    }
}

class CandidateJobTypeCell: UITableViewCell {
    
    //MARK:- Properties
    //==================
    
    //MARK:- IBoutlets
    //================
    
    @IBOutlet weak var alertSearchLabel: UILabel!
    @IBOutlet weak var fullTimeButton: UIButton!
    @IBOutlet weak var partTimeButton: UIButton!
    @IBOutlet weak var bothButton: UIButton!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var cornerRadiusView: UIView!
    @IBOutlet weak var noButton: UIButton!
    
    @IBOutlet weak var yesButton: UIButton!
    
    @IBOutlet weak var jobTypeTitleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.cornerRadiusView.setCorner(cornerRadius: 5.0, clip: true)
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.shadowView.setCorner(cornerRadius: 5, clip: false)
        self.shadowView.dropShadow(color: UIColor.gray, opacity:0.2, offSet: CGSize(width: 1.0, height: 1.0), radius: 1.0)
        self.shadowView.clipsToBounds = true
    }
}

class CandidateProfileExperienceCell: UITableViewCell {
    
    //MARK:- Properties
    //==================
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var experienceTitle: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var experienceStackView: UIStackView!
    @IBOutlet weak var addMoreExperienceButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.bgView.setCorner(cornerRadius: 5.0, clip: true)
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.containerView.setCorner(  cornerRadius: 5, clip: false)
        self.containerView.dropShadow(color: UIColor.gray, opacity:0.2, offSet: CGSize(width: 1.0, height: 1.0), radius: 1.0)
        self.containerView.clipsToBounds = true
    }
}

public extension UITextView {
    public var visibleRange: NSRange? {
        guard let start = closestPosition(to: contentOffset), let end = characterRange(at: CGPoint(x: contentOffset.x + bounds.maxX,
                                                                                                   y: contentOffset.y + bounds.maxY))?.end
            else { return nil }
        return NSMakeRange(offset(from: beginningOfDocument, to: start), offset(from: start, to: end))
    }
}

