//
//  DeactivateAccountVC.swift
//  JobGet
//
//  Created by Admin on 04/10/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class DeactivateAccountVC: UIViewController {

    // MARK: - Outlets.
    // MARK: -
    
    @IBOutlet weak var navigationTitleLabel: UILabel!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var deactivateTableView: UITableView!
    
    // MARK: - Properties.
    // MARK: -
    
    
    // MARK: - Virew life cycle.
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.initialSetup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Actions.
    // MARK: -
    
    @IBAction func backButtonTapp(_ sender: UIButton) {
        self.pop()
    }
    
}

// MARK: - Private methods.
// MARK: -
extension DeactivateAccountVC{
    
    func initialSetup(){
        self.deactivateTableView.delegate = self
        self.deactivateTableView.dataSource = self
        self.navigationView.setShadow()
    }
    
    func deactivateUserAcunt(loader: Bool, status: Int) {
        
        let params = [ApiKeys.status.rawValue: status]
        
        WebServices.deactivateAccount(parameters: params, success: { (json) in
            if json["code"].intValue == error_codes.success {
                //            TODO:- Logout user here
                if let userId = User.getUserModel().user_id {
                    
                    CommonClass().goToLogin()
                    UIApplication.shared.applicationIconBadgeNumber = 0
                    print_debug("Logout user_id : \(userId)")
                    ChatHelper.logOutUser(userId: userId)
                    AppUserDefaults.removeAllValues()
                    
                }else{
                    self.showAlert(msg: StringConstants.Cannot_logout_user)
                }
                CommonClass.showToast(msg: json["message"].stringValue)
            } else {
                CommonClass.showToast(msg: json["message"].stringValue)
            }
            
        }) { (error) in
            print_debug(error)
            CommonClass.showToast(msg: error.localizedDescription)
        }
    }
}

// MARK: - Table view delegate and datasource methods.
// MARK: -
extension DeactivateAccountVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "DeactivateAccountCell", for: indexPath) as? DeactivateAccountCell else{fatalError("DeactivateAccountCell not found.")}
        if AppUserDefaults.value(forKey: .userType).stringValue == UserType.recuriter.rawValue{
            switch indexPath.row{
                
            case 0:
                cell.titleLabel.text = StringConstants.PAUSE_ACCOUNT
                cell.descriptionLabel.text = StringConstants.RECRUITER_PAUSE_ACCOUNT_DESCRIPTION
            default:
                cell.titleLabel.text = StringConstants.DELETE_ACCOUNT
                cell.descriptionLabel.text = StringConstants.RECRUITER_DELETE_ACCOUNT_DESCRIPTION
                
            }
            
        }else{
            switch indexPath.row{
                
            case 0:
                cell.titleLabel.text = StringConstants.PAUSE_ACCOUNT
                cell.descriptionLabel.text = StringConstants.CANDIDATE_PAUSE_ACCOUNT_DESCRIPTION
            default:
                cell.titleLabel.text = StringConstants.DELETE_ACCOUNT
                cell.descriptionLabel.text = StringConstants.CANDIDATE_DELETE_ACCOUNT_DESCRIPTION
                
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0{
            let popUpVc = AlertPopUpVC.instantiate(fromAppStoryboard: .Candidate)
            popUpVc.delegate = self
            popUpVc.FromDeactivateAccount = true
            popUpVc.modalPresentationStyle = .overCurrentContext
            popUpVc.modalTransitionStyle = .crossDissolve
            self.present(popUpVc, animated: true, completion: nil)
            
        }else{
            let popUpVc = DeleteAccountPopupVC.instantiate(fromAppStoryboard: .settingsAndChat)
            popUpVc.delegate = self
            popUpVc.modalPresentationStyle = .overCurrentContext
            popUpVc.modalTransitionStyle = .crossDissolve
            self.present(popUpVc, animated: true, completion: nil)
        }
    }
}

// MARK: - AlertPopUpDelegate methods.
// MARK: -
extension DeactivateAccountVC:AlertPopUpDelegate, DeleteAccountDelegate{
    
    func didTapAffirmativeButton() {
        //         self.deactivateUserAcunt(loader: true)
    }
    
    func didTapNegativeButton() {
        
    }
    
    func didTapDeativateButton(){
        self.deactivateUserAcunt(loader: true, status: 5)
    }
    
    func deleteAccountDelegate(otherText: String) {
        self.deactivateUserAcunt(loader: true, status: 5)
    }
}

// MARK: - Table view cell class.
// MARK: -
class DeactivateAccountCell: UITableViewCell{
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleBgView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.bgView.dropShadow(color: AppColors.black46, opacity:0.2, offSet: CGSize(width: 1.0, height: 1.0), radius: 2.0)
          self.titleBgView.roundCornersFromOneSide([.topLeft, .topRight], radius: 5)
    }
}
