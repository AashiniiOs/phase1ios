//
//  DeleteAccountPopupVC.swift
//  JobGet
//
//  Created by Admin on 04/10/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit


protocol DeleteAccountDelegate: class {
    func deleteAccountDelegate(otherText: String)
}

class DeleteAccountPopupVC: UIViewController {

    // MARK: - Outlets
    // MARK: -
    
    @IBOutlet weak var titleImageView: UIImageView!
    @IBOutlet weak var notNowButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var deleteTableView: UITableView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var popupView: UIView!
    
    
    // MARK: - Properties.
    // MARK: -
    var isTextViewEnable = false
    var recruiterReasionArray = [StringConstants.I_have_recently_hired_candidate_found_JobGet,StringConstants.I_have_recently_hired_candidate_found_another_app_website,StringConstants.I_receive_too_many_notifications,StringConstants.The_position_not_needed_and_was_closed,StringConstants.There_are_not_enough_candidates,StringConstants.The_app_difficult_to_use,StringConstants.Other_followed_by_free_text_field ]
    
     var candidateReasionArray = [StringConstants.I_have_recently_found_job_JobGet,StringConstants.I_have_recently_found_job_another_app_website,StringConstants.I_receive_too_many_notifications,StringConstants.There_are_not_enough_jobs, StringConstants.The_app_difficult_to_use,StringConstants.Other_followed_by_free_text_field ]
    var count = 0
    var selectedIndex: Int?
    var otherComment: String?
    weak var delegate: DeleteAccountDelegate?
    
    // MARK: - View life cycle.
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.initialSetup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.view.endEditing(true)
    }
    
    // MARK: - Actions
    // MARK: -
    @IBAction func notNowButtonTapp(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func deleteButtonTapp(_ sender: UIButton) {
        if self.isTextViewEnable{
            if let _ = self.otherComment{}
            else{
                CommonClass.showToast(msg: StringConstants.Please_Enter_Other_Text)
                return
            }
        }
        self.delegate?.deleteAccountDelegate(otherText: self.otherComment ?? "")
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK: - Private methods.
// MARK: -
extension DeleteAccountPopupVC{
    
    func initialSetup(){
        
        self.deleteTableView.delegate = self
        self.deleteTableView.dataSource = self
        self.deleteTableView.keyboardDismissMode = .onDrag
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string:"\(User.getUserModel().first_name)\(StringConstants.DEACTIVATE_ACCOUNT_TITLE_TEXT)")
        attributedString.setColorForText(textForAttribute: "\(User.getUserModel().first_name)"  , withColor: AppColors.black46, font: AppFonts.Poppins_Regular.withSize(13))
        self.descriptionLabel.attributedText = attributedString
        if AppUserDefaults.value(forKey: .userType).stringValue == UserType.recuriter.rawValue{
            self.count = self.recruiterReasionArray.count
        }else{
            self.count = self.candidateReasionArray.count
        }
        self.titleImageView.roundCorners()
    }
}

// MARK: - Table view delegate and datasource methods.
// MARK: -
extension DeleteAccountPopupVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if AppUserDefaults.value(forKey: .userType).stringValue == UserType.recuriter.rawValue{
            if isTextViewEnable{
                return self.recruiterReasionArray.count+1
            }else{
                return self.recruiterReasionArray.count
            }
        }else{
            if isTextViewEnable{
                return self.candidateReasionArray.count+1
            }else{
                return self.candidateReasionArray.count
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        if indexPath.row == self.count{
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DeleteAccountPopupTextViewCell", for: indexPath) as? DeleteAccountPopupTextViewCell else{fatalError("DeleteAccountPopupTextViewCell not found.")}
            cell.deleteTextView.delegate = self
            return cell
            
        }else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DeleteAccountPopupCell", for: indexPath) as? DeleteAccountPopupCell else{fatalError("DeleteAccountPopupCell not found.")}
            if AppUserDefaults.value(forKey: .userType).stringValue == UserType.recuriter.rawValue{
                cell.reasonLabel.text = self.recruiterReasionArray[indexPath.row]
            }else{
                cell.reasonLabel.text = self.candidateReasionArray[indexPath.row]
            }
            if let index = self.selectedIndex, indexPath.row == index{
                if index == self.count - 1{
                    cell.bottomSeperatorView.isHidden = true
                }
                cell.selectedImageView.image = #imageLiteral(resourceName: "ic_radio_button_checked1")
            }else{
                cell.selectedImageView.image = #imageLiteral(resourceName: "ic_radio_button_unchecked1")
                cell.bottomSeperatorView.isHidden = false
            }
            
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.selectedIndex = indexPath.row
        if let index = self.selectedIndex, index == self.count - 1{
            print_debug(indexPath.row)
            self.isTextViewEnable = true
            self.deleteTableView.scrollToRow(at: indexPath, at: .top, animated: true)
        }else{
            self.isTextViewEnable = false
        }
        self.deleteTableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}

// MARK: - UITextViewDelegate methods.
// MARK: -
extension DeleteAccountPopupVC: UITextViewDelegate{
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        
        if textView.textColor == AppColors.gray152 {
            textView.text = nil
            textView.textColor = AppColors.black26
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text.isEmpty {
            textView.text = StringConstants.Write_Here
            textView.textColor = AppColors.gray152
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == " " {
            return false
        } else {
            return (textView.text ?? "").count - range.length < 55
        }
    }
}

// MARK: - Table view cell class
// MARK: -

class DeleteAccountPopupCell: UITableViewCell{
    
    @IBOutlet weak var reasonLabel: UILabel!
    @IBOutlet weak var selectedImageView: UIImageView!
    @IBOutlet weak var bottomSeperatorView: UIView!
   
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}


class DeleteAccountPopupTextViewCell: UITableViewCell{
    
    @IBOutlet weak var textViewhight: NSLayoutConstraint!
    @IBOutlet weak var deleteTextView: UITextView!
    @IBOutlet weak var bottomSeperatorView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.deleteTextView.autocapitalizationType = .sentences
        self.deleteTextView.text = StringConstants.Write_Here
        self.deleteTextView.textColor = AppColors.gray152
    }
}
