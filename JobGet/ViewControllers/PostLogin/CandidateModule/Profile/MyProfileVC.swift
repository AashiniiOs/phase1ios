//
//  MyProfileVC.swift
//  JobGet
//
//  Created by appinventiv on 16/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class MyProfileVC: UIViewController {
    
    //    MARK:- IBOutlets
    //    ==========================================
    @IBOutlet weak var settingsButton: UIButton!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var screenHeading: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var profileImageContainer: UIView!
    @IBOutlet weak var profileTable: UITableView!
    
    //    MARK:- Life cycle
    //    ==========================================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetUp()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    //    MARK:- functions
    //    ==========================================
    
    private func initialSetUp(){
        
        self.editButton.roundCorners()
        self.profileImageContainer.roundCorners()
        self.profileImage.roundCorners()
        
        self.profileTable.dataSource = self
        self.profileTable.delegate = self
        
        let image = #imageLiteral(resourceName: "ic_mode_edit_white_").withRenderingMode(.alwaysTemplate)
        self.editButton.setImage(image, for: .normal)
        self.editButton.imageView?.tintColor = AppColors.appBlue
        
        let n_image = #imageLiteral(resourceName: "ic_notifications").withRenderingMode(.alwaysTemplate)
        self.notificationButton.setImage(n_image, for: .normal)
        self.notificationButton.imageView?.tintColor = .white
    }
    
    //    MARK:- IBActions
    //    ==========================================
    
    
    @IBAction func notificationButtonTapped(_ sender: UIButton) {
        let notificationScene = NotificationsVC.instantiate(fromAppStoryboard: .settingsAndChat)
        self.navigationController?.pushViewController(notificationScene, animated: true)
    }
    
    @IBAction func settingsButtonTapped(_ sender: UIButton) {
        let settings = SettingsVC.instantiate(fromAppStoryboard: .settingsAndChat)
        self.navigationController?.pushViewController(settings, animated: true)
    }
    
    @IBAction func editButtonTapped(_ sender: UIButton) {
        
        
    }
    
    @IBAction func onTappedLogoutButton(_ sender: UIButton) {
        WebServices.logoutAPI(success: { (json) in
            if let userId = User.getUserModel().user_id {
                UIApplication.shared.applicationIconBadgeNumber = 0
                ChatHelper.logOutUser(userId:userId)
                AppUserDefaults.removeAllValues()
                CommonClass().goToLogin()
            }else{
                self.showAlert(msg: "Cannot logout user")
            }
        }) { (error) in
            print_debug(error)
            CommonClass.showToast(msg: error.localizedDescription)
            
        }
    }
}

//    MARK:- Tableview datasource
//    ==========================================
extension MyProfileVC: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileCell", for: indexPath) as! ProfileCell
        
        if indexPath.row == 0{
            cell.sliderContainer.isHidden = false
        }else{
            cell.userDetailContainer.isHidden = false
        }
        return cell
    }
    
}

//    MARK:- TableView delegate
//    ==========================================
extension MyProfileVC: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0{
            return 170
        }else{
            return 210
        }
    }
}

//    MARK:- ProfileCell
//    ==========================================
class ProfileCell: UITableViewCell{
    
    @IBOutlet weak var profileStatusContainer: UIView!
    @IBOutlet weak var profileCompletionStatus: UILabel!
    @IBOutlet weak var sliderContainer: UIView!
    @IBOutlet weak var completeProfileLabel: UILabel!
    
    @IBOutlet weak var emailPlaceHolderLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBOutlet weak var locationPlaceHolder: UILabel!
    @IBOutlet weak var locationName: UILabel!
    
    @IBOutlet weak var profileViews: UILabel!
    @IBOutlet weak var userDetailContainer: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.resetCell()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.resetCell()
    }
    
    private func resetCell(){
        self.sliderContainer.isHidden = true
        self.userDetailContainer.isHidden = true
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)        
        self.userDetailContainer.roundCorner(.allCorners, radius: 5)
        self.profileStatusContainer.roundCorner(.allCorners, radius: 5)
        self.sliderContainer.roundCorner(.allCorners, radius: self.sliderContainer.frame.height / 2)
    }
}
