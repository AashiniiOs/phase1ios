//
//  NotificationsVC.swift
//  JobGet
//
//  Created by appinventiv on 17/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class NotificationsVC: UIViewController {
    
    enum RemoveType{
        
        case all
        case single
        case none
    }
    
    //    MARK:- IBOutlets
    //    ======================================
    
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var screenHeading: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var notificationTable: UITableView!
    
    var pageNum = 0
    var isPaginationEnable = false
    var shouldLoadMoreData = false
    var notificationList = [NotificationModel]()
    var removeType = RemoveType.none
    var selectedIndex = 0
    //    MARK:- view life cycle
    //    ======================================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    //    MARK:- IBActions
    //    ======================================
    
    @IBAction func backButtonTapped(_ sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func deleteButtonTapped(_ sender: UIButton){
        
        if self.notificationList.isEmpty{
            return
        }
        self.removeType = .all
        let popUpVc = AlertPopUpVC.instantiate(fromAppStoryboard: .Candidate)
        popUpVc.delegate = self
        popUpVc.alertType = .deleteNotification
        popUpVc.modalPresentationStyle = .overCurrentContext
        popUpVc.modalTransitionStyle = .crossDissolve
        let rootViewController = UIApplication.shared.keyWindow?.rootViewController
        rootViewController?.present(popUpVc, animated: true, completion: nil)
    }
}

extension NotificationsVC{
    
    //    MARK:- functions
    //    ======================================
    
    private func initialSetup(){
        self.notificationTable.delegate = self
        self.notificationTable.dataSource = self
        self.notificationTable.emptyDataSetSource = self
        self.notificationTable.emptyDataSetDelegate = self
        
        self.notificationTable.estimatedRowHeight = 50
        self.notificationTable.rowHeight = UITableViewAutomaticDimension
        self.notificationTable.enablePullToRefresh(tintColor: AppColors.themeBlueColor ,target: self, selector: #selector(refreshWhenPull(_:)))
        self.getNotificationList(self.pageNum, loader: true)
        self.notificationTable.register(UINib(nibName: "LoadMoreTableViewCell", bundle: nil), forCellReuseIdentifier: "LoadMoreTableViewCell")
    }
    
    @objc func refreshWhenPull(_ sender: UIRefreshControl) {
        sender.endRefreshing()
        self.pageNum = 0
        self.getNotificationList(self.pageNum, loader: true)
    }
    private func isLoadingIndexPath(_ indexPath: IndexPath) -> Bool {
        guard shouldLoadMoreData else { return false}
        
        return indexPath.row == self.notificationList.count
    }
    
    
    func getNotificationList(_ page: Int, loader: Bool){
        
        WebServices.getNotificationList(parameters: ["page": "\(page)"], loader: loader, success: { (json) in
            var data = [NotificationModel]()
            let jobs = json[ApiKeys.data.rawValue]["notifications"].arrayValue
            for job in jobs {
                data.append(NotificationModel(dict: job))
            }
            if page == 0 {
                self.notificationList.removeAll(keepingCapacity: false)
            }
            if self.shouldLoadMoreData {
                self.notificationList.append(contentsOf: data)
            } else {
                self.notificationList = data
            }
            AppUserDefaults.save(value: 0, forKey: .totalNotification) 
            self.pageNum = json[ApiKeys.page.rawValue].intValue
            self.shouldLoadMoreData = json[ApiKeys.next.rawValue].boolValue
            self.notificationTable.reloadData()
        }) { (error) in
            CommonClass.showToast(msg: error.localizedDescription )
        }
    }
    
    func deleteNotificationList(){
        
        WebServices.deleteNotificationList(parameters: ["":""], loader: true, success: { (json) in
            self.notificationList.removeAll(keepingCapacity: false)
            self.notificationTable.reloadData()
        }) { (error) in
            CommonClass.showToast(msg: error.localizedDescription )
        }
    }
    
    func deleteSingleNotificationApi(){
        
        WebServices.deleteSingleNotificationApi(parameters: ["notificationId":self.notificationList[self.selectedIndex].id], loader: true, success: { (json) in
            self.notificationList.remove(at: self.selectedIndex)
            self.notificationTable.reloadData()
        }) { (error) in
            CommonClass.showToast(msg: error.localizedDescription )
        }
    }
}

//    MARK:- Tableview delegate and datasorce methods.
//    ======================================
extension NotificationsVC:UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = self.notificationList.count
        return shouldLoadMoreData ? count + 1 : count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isLoadingIndexPath(indexPath){
            guard let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.defaultReuseIdentifier, for: indexPath) as? LoadMoreTableViewCell else {
                fatalError("Failed to initialize LoadMoreTableViewCell")
            }
            self.isPaginationEnable = true
            return cell
        }
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationsCell", for: indexPath) as? NotificationsCell else{fatalError("")}
        if let timeDate = self.notificationList[indexPath.row].createdAt {
            cell.timeLabel.text = "\(timeDate.elapsedTime)"
        }
        cell.userImage.sd_setImage(with: URL(string: self.notificationList[indexPath.row].userImage),  placeholderImage: #imageLiteral(resourceName: "ic_user_"), completed: nil)
        
        var senderName = ""
        if self.notificationList[indexPath.row].message.contains(s: self.notificationList[indexPath.row].senderName){
            senderName = self.notificationList[indexPath.row].senderName
            self.notificationList[indexPath.row].message = self.notificationList[indexPath.row].message.replace(string: senderName, withString: "")
        }else{
            senderName = self.notificationList[indexPath.row].senderName
        }
        
        if AppUserDefaults.value(forKey: .userType).stringValue == UserType.candidate.rawValue{
            let notificationText = "\(StringConstants.You_have_been_Shortlist) \(self.notificationList[indexPath.row].companyName) \(StringConstants.send_them_Message)"
            cell.notificationMessage.attributedText = CommonClass.getAttributedText(wholeString:notificationText, attributedString: self.notificationList[indexPath.row].companyName, attributedColor: AppColors.black46, normalColor: AppColors.gray152, fontSize: 15)
//             cell.notificationMessage.text = self.notificationList[indexPath.row].message
        }else{
//            cell.notificationMessage.text = self.notificationList[indexPath.row].message
            if let lastName = self.notificationList[indexPath.row].senderName.strstr(needle: " "), let firstChar = lastName.first{
                senderName =  senderName.replace(string: lastName, withString: "\(firstChar).")
            }
             print_debug("senderNamesenderNamesenderNamesenderName=== \(senderName)")
           
            let notificationText = "\(senderName) \(StringConstants.has_applied_for) \(self.notificationList[indexPath.row].jobTitle)"
             cell.notificationMessage.attributedText = CommonClass.getAttributedText(wholeString:notificationText, attributedString: StringConstants.has_applied_for, attributedColor: AppColors.gray152, normalColor: AppColors.black46, fontSize: 15)
        }

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard self.shouldLoadMoreData else { return }
        
        if indexPath.row == self.notificationList.count - 1 && self.shouldLoadMoreData   && isPaginationEnable {  //for 2nd last cell
            self.getNotificationList(self.pageNum, loader: false)
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch AppUserDefaults.value(forKey: .userType).stringValue {
        case UserType.candidate.rawValue:
            
            guard let navigatinArray = self.navigationController?.viewControllers else{return}
            
            for vc in navigatinArray{
                
                if let viewController = vc as? CandidateTabBarVC {
                    viewController.isFirstTime = true
                    self.navigationController?.popToViewController(viewController, animated: true)
                }
            }
   
        case UserType.recuriter.rawValue:
            let sceen = JobDetailVC.instantiate(fromAppStoryboard: .PostJob)
            sceen.page = 0
            sceen.jobID = self.notificationList[indexPath.row].moduleId
            sceen.isOpenFromPush = true
            sceen.isForApplicantsBtnTap = true
            sceen.senderId = self.notificationList[indexPath.row].senderId
            self.navigationController?.pushViewController(sceen, animated: true)
            
        default:
            return
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        self.selectedIndex = indexPath.row
        if editingStyle == .delete {
            self.removeType = .single
            self.deleteSingleNotification()
            
        }
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func deleteSingleNotification(){
        
        let popUpVc = AlertPopUpVC.instantiate(fromAppStoryboard: .Candidate)
        popUpVc.delegate = self
        popUpVc.alertType = .deleteSingleNotification
        popUpVc.modalPresentationStyle = .overCurrentContext
        popUpVc.modalTransitionStyle = .crossDissolve
        let rootViewController = UIApplication.shared.keyWindow?.rootViewController
        rootViewController?.present(popUpVc, animated: true, completion: nil)
    }
    
    func getAttributedText(wholeString : String, attributedString : String, attributedColor : UIColor, normalColor: UIColor, fontSize : CGFloat)-> NSAttributedString {
        
        let attributedText = NSMutableAttributedString(string: wholeString)
        
        attributedText.addAttributes([NSAttributedStringKey.foregroundColor:normalColor, NSAttributedStringKey.font: AppFonts.Poppins_Medium.withSize(fontSize)], range: (wholeString as NSString).range(of: wholeString))
        
        attributedText.addAttributes([NSAttributedStringKey.foregroundColor:attributedColor, NSAttributedStringKey.font: AppFonts.Poppins_Medium.withSize(fontSize)], range: (wholeString as NSString).range(of: attributedString))
        
        return attributedText
    }
}

//MARK:- DZNEmptyDataSource and Delegate
//==========================================
extension NotificationsVC: DZNEmptyDataSetDelegate, DZNEmptyDataSetSource {
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return #imageLiteral(resourceName: "icHomeNoJobs")
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: StringConstants.No_Notification_Found,                                                                           attributes: [NSAttributedStringKey.foregroundColor: AppColors.black46,
                                                                                                                                                          NSAttributedStringKey.font: AppFonts.Poppins_Medium.withSize(14)
            ])
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        return NSAttributedString(string: "",                                                                           attributes: [NSAttributedStringKey.foregroundColor: AppColors.gray152,
                                                                                                                                     NSAttributedStringKey.font: AppFonts.Poppins_SemiBold.withSize(14)                                                                                                                                                                                                            ])
    }
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}


extension NotificationsVC: AlertPopUpDelegate {
    
    func didTapAffirmativeButton() {
        if self.removeType == .single{
            self.deleteSingleNotificationApi()
        }else if self.removeType == .all{
            self.deleteNotificationList()
        }
    }
    
    func didTapNegativeButton() {
        // To do Task
    }
}

//    MARK:- SettingsCell
//    ======================================
class NotificationsCell: UITableViewCell{
    
    @IBOutlet weak var imageContainer: UIView!
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var notificationMessage: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.initialSetup()
    }
    
    private func initialSetup(){
        
        self.userImage.roundCorners()
        self.imageContainer.roundCorners()
    }
}
