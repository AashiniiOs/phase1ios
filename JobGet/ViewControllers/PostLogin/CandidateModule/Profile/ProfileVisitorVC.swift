//
//  ProfileVisitorVC.swift
//  JobGet
//
//  Created by Admin on 30/07/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class ProfileVisitorVC: UIViewController {
    
    // MARK: - Outlet.
    // MARK: -
    
    @IBOutlet weak var visitorTableView: UITableView!
    
    @IBOutlet weak var crossButton: UIButton!
    
    // MARK: - Properties
    // MARK: -
    var pageNum = 0
    var isPaginationEnable = false
    var shouldLoadMoreData = false
    var profileVisitorData = [ProfileVisitorModel]()
    
    // MARK: - view life cycle.
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Action
    // MARK: -
    
    @IBAction func cancelButtonTapp(_ sender: UIButton) {
        self.view.alpha = 1.0
        UIView.animate(withDuration: 0.3) {
            self.view.alpha = 0.0
            self.view.removeFromSuperview()
            self.removeFromParentViewController()
        }
    }
    
    // MARK: - Private method.
    // MARK: -
    private func initialSetup(){
        
        self.visitorTableView.delegate = self
        self.visitorTableView.dataSource = self
        self.visitorTableView.enablePullToRefresh(tintColor: AppColors.themeBlueColor ,target: self, selector: #selector(refreshWhenPull(_:)))
        self.crossButton.roundCorners()
        self.visitorList(page: 0, loader: true)
        self.visitorTableView.register(UINib(nibName: "LoadMoreTableViewCell", bundle: nil), forCellReuseIdentifier: "LoadMoreTableViewCell")
    }
    
    @objc func refreshWhenPull(_ sender: UIRefreshControl) {
        sender.endRefreshing()
        self.pageNum = 0
        
        self.visitorList(page: self.pageNum, loader: false)
    }
    
    private func isLoadingIndexPath(_ indexPath: IndexPath) -> Bool {
        guard shouldLoadMoreData else { return false}
        
        return indexPath.row == self.profileVisitorData.count
    }
    
    func visitorList(page: Int, loader: Bool){
        
        WebServices.visitorListApi(parameters: ["page": "\(page)"], loader: loader, success: { (json) in
            var data = [ProfileVisitorModel]()
            let users = json[ApiKeys.data.rawValue]["users"].arrayValue
            for user in users {
                data.append(ProfileVisitorModel(withJSON: user))
            }
            if page == 0 {
                self.profileVisitorData.removeAll(keepingCapacity: false)
            }
            if self.shouldLoadMoreData {
                self.profileVisitorData.append(contentsOf: data)
            } else {
                self.profileVisitorData = data
            }
            self.pageNum = json[ApiKeys.page.rawValue].intValue
            self.shouldLoadMoreData = json[ApiKeys.next.rawValue].boolValue
            self.visitorTableView.reloadData()
            
        }) { (error) in
            CommonClass.showToast(msg: error.localizedDescription)
        }
    }
}

// MARK: - Table view delegate and datasource methods..
// MARK: -
extension ProfileVisitorVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.profileVisitorData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isLoadingIndexPath(indexPath){
            guard let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.defaultReuseIdentifier, for: indexPath) as? LoadMoreTableViewCell else {
                fatalError("Failed to initialize LoadMoreTableViewCell")
            }
            self.isPaginationEnable = true
            return cell
        }
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileVisitorCell", for: indexPath) as? ProfileVisitorCell else{
            fatalError("ProfileVisitorCell not found")
        }
        cell.nameLabel.text = "\(self.profileVisitorData[indexPath.row].first_name ) \(self.profileVisitorData[indexPath.row].last_name )"
        cell.companyNameLabel.text = self.profileVisitorData[indexPath.row].companyName
        cell.userImage.sd_setImage(with: URL(string: self.profileVisitorData[indexPath.row].userImage), placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"),  completed: nil)
        
        if let timeDate = self.profileVisitorData[indexPath.row].createdAt {
            cell.timeLabel.text = timeDate.elapsedTime
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard self.shouldLoadMoreData else { return }
        
        if indexPath.row == self.profileVisitorData.count - 1 && self.shouldLoadMoreData   && isPaginationEnable {  //for 2nd last cell
            self.visitorList(page: self.pageNum, loader: false)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let employerSceen = EmployerDetailVC.instantiate(fromAppStoryboard: .settingsAndChat)
        
        employerSceen.userId = self.profileVisitorData[indexPath.row].recruiterId
        employerSceen.isOpenFromChat = true
        
        self.navigationController?.pushViewController(employerSceen, animated: true)
    }
}

// MARK: - Table view cell class
// MARK: -
class ProfileVisitorCell: UITableViewCell{
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var userImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.userImage.roundCorners()
    }
}
