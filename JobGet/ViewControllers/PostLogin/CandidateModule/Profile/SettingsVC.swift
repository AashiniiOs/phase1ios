//
//  SettingsVC.swift
//  JobGet
//
//  Created by appinventiv on 17/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class SettingsVC: BaseVC {
    
    //    MARK:- IBOutlets
    //    ========================================
    @IBOutlet weak var settingsTable: UITableView!
    @IBOutlet weak var screenTitle: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var navigationView: UIView!
    
    //    MARK:- Properties
    //    ========================================
    
    private var settings = [StringConstants.pushNotification,
                            StringConstants.changePassword,
                            StringConstants.invite,
                            StringConstants.rateReview,
                            StringConstants.ContactUS,
                            StringConstants.termsConditions,
                            StringConstants.privacyPloicy,
                            StringConstants.faq,
                            StringConstants.deactivate,
                            StringConstants.logout ]
    
    var userId = ""
    var data = User.getUserModel()

    
    //    MARK:- life cycle
    //    ========================================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    //    MARK:- functions
    //    ========================================
    private func initialSetup() {
        
        
        self.settingsTable.dataSource = self
        self.settingsTable.delegate = self
        self.screenTitle.text = StringConstants.Settings.uppercased()
        self.navigationView.setShadow()
    }
    
    //    MARK:- selctor methods
    //    ========================================
    
    
    //    MARK:- IBActions
    //    ========================================
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

//MARK: - Web services
//======================
extension SettingsVC {
    
    func deactivateUserAcunt(loader: Bool) {
        
        let params = [ApiKeys.status.rawValue: 4]
        
        WebServices.deactivateAccount(parameters: params, success: { (json) in
            if json["code"].intValue == error_codes.success {
                //            TODO:- Logout user here
                if let userId = User.getUserModel().user_id {
                    CommonClass().goToLogin()
                    UIApplication.shared.applicationIconBadgeNumber = 0
                    print_debug("Logout user_id : \(userId)")
                    ChatHelper.logOutUser(userId: userId)
                    AppUserDefaults.removeAllValues()
                    
                }else{
                    self.showAlert(msg: StringConstants.Cannot_logout_user)
                }
                CommonClass.showToast(msg: json["message"].stringValue)
            } else {
                CommonClass.showToast(msg: json["message"].stringValue)
            }
            
        }) { (error) in
            print_debug(error)
            CommonClass.showToast(msg: error.localizedDescription)
        }
    }
    
    
    func logout(loader: Bool) {
        
        WebServices.logoutAPI(success: { (json) in
            print_debug("Logout user_id : \( User.getUserModel().user_id ?? "")")
            if let userId = User.getUserModel().user_id {
                
                UIApplication.shared.applicationIconBadgeNumber = 0
                
                CommonClass().goToLogin()
                ChatHelper.logOutUser(userId:userId)
                AppUserDefaults.removeAllValues()                
            }else{
                self.showAlert(msg: "Cannot logout user")
            }
            
        }) { (error) in
            print_debug(error)
            CommonClass.showToast(msg: error.localizedDescription)
            
        }
    }
}

//    MARK:- tableview datasource
//    ========================================
extension SettingsVC: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsCell", for: indexPath) as! SettingsCell
        
        if indexPath.row == 0{
            let data = AppUserDefaults.value(forKey: .isNotify)
            if data != 1{
                cell.toggle.isOn = false
            }else{
                cell.toggle.isOn = true
            }
            cell.toggle.isHidden = false
            cell.disclosureImage.isHidden = true
            cell.toggle.addTarget(self, action: #selector(notificationSwitchAction), for: .valueChanged)
        }else if indexPath.row == self.settings.count - 1{
            cell.disclosureImage.image = #imageLiteral(resourceName: "ic_power_settings_new_").rotateBy(angleDegree: 90)
        }
        
        cell.heading.text = self.settings[indexPath.row]
        
        return cell
    }
    
    @objc func notificationSwitchAction(_ sender: UISwitch){
        
        var type = 1
        if sender.isOn{
            type = 1
        }else{
            type = 0
        }
        
        WebServices.notifyAPI(params: ["isNotify": type], success: { (data) in
            AppUserDefaults.save(value: "\(type)", forKey: .isNotify) 
        }) { (error) in
            CommonClass.showToast(msg: error.localizedDescription)
        }
        
    }
}
//    MARK:- tabeleview delegate
//    ========================================
extension SettingsVC: UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 2{
             return 0
        }else{
             return 60
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 1:
            //   print_debug("Go to change Password")
            let changePassVc = ChangePasswordVC.instantiate(fromAppStoryboard: .RecruiterProfile)
            self.navigationController?.pushViewController(changePassVc, animated: true)
            
        case 2:
            print_debug(AppUserDefaults.value(forKey: .referralUrl).stringValue)
            let inviteText = "Get JobGet Application by using my refrral code: \(AppUserDefaults.value(forKey: .referralCode).stringValue) \n\(AppUserDefaults.value(forKey: .referralUrl).stringValue)"
            self.displayShareSheet(shareContent: inviteText)
        case 3:
            //
            let rateVC = RateAppVC.instantiate(fromAppStoryboard: .RecruiterProfile)
            self.navigationController?.pushViewController(rateVC, animated: true)
        case 4:
            let contactusVC = ContactusVC.instantiate(fromAppStoryboard: .RecruiterProfile)
            self.navigationController?.pushViewController(contactusVC, animated: true)
        case 5,6,7:
            let staticPage = StaticPageVC.instantiate(fromAppStoryboard: .settingsAndChat)
            if indexPath.row == 5{
                staticPage.webViewType = .terms
            }else if indexPath.row == 6{
                staticPage.webViewType = .privacy
            }else if indexPath.row == 7{
                staticPage.webViewType = .faq
            }
            self.navigationController?.pushViewController(staticPage, animated: true)
        case 8:
            let deactivateAccountVC = DeactivateAccountVC.instantiate(fromAppStoryboard: .settingsAndChat)
            self.navigationController?.pushViewController(deactivateAccountVC, animated: true)
        case  9:
            let popUpVc = AlertPopUpVC.instantiate(fromAppStoryboard: .Candidate)
            popUpVc.delegate = self
            popUpVc.commongFromSetting = true
            popUpVc.modalPresentationStyle = .overCurrentContext
            popUpVc.modalTransitionStyle = .crossDissolve
            self.present(popUpVc, animated: true, completion: nil)
            
        default:
            break
        }
    }
}

extension SettingsVC: AlertPopUpDelegate {
    
    func didTapAffirmativeButton() {
        print_debug("Logout")
        //            TODO:- Logout user here
        
        
        self.logout(loader: true)
    }
    
    func didTapNegativeButton() {
        // To do Task
    }
    
    func didTapDeativateButton() {
        print_debug("deactivate Button Tapped")
        self.deactivateUserAcunt(loader: true)
    }
}

//    MARK:- Settings cell
//    ========================================

class SettingsCell: UITableViewCell{
    
    @IBOutlet weak var heading: UILabel!
    @IBOutlet weak var disclosureImage: UIImageView!
    @IBOutlet weak var toggle: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.intialSetUp()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.intialSetUp()
    }
    
    private func intialSetUp(){
        self.toggle.isHidden = true
        self.disclosureImage.isHidden = false
        self.disclosureImage.image = #imageLiteral(resourceName: "ic_chevron_right_")
    }
}

