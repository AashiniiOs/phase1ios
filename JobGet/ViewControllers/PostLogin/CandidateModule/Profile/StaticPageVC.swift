//
//  StaticPageVC.swift
//  JobGet
//
//  Created by appinventiv on 17/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

enum WebViewType{
    
    case privacy
    case terms
    case faq
    case none
}


class StaticPageVC: UIViewController {
    
        
    //      MARK:-   IBOutlets
    //      =============================
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var heading: UILabel!
    @IBOutlet weak var webView: UIWebView!
    
    var webViewType = WebViewType.none
    var urlString = ""
    
    //      MARK:-   Life cycle
    //      =============================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    //      MARK:-   Fuctions
    //      =============================
    private func initialSetup(){
        
        
        if self.webViewType == .privacy {
            self.urlString = "\(STATIC_PAGE_BASE_URL)privacy-policy"
            self.heading.text = StringConstants.privacyPloicy.uppercased()
            
        }else if self.webViewType == .terms {
            self.urlString = "\(STATIC_PAGE_BASE_URL)term-conditions"
            self.heading.text = StringConstants.termsConditions.uppercased()
        }else if self.webViewType == .faq {
            self.urlString = "\(STATIC_PAGE_BASE_URL)faq"
            self.heading.text = StringConstants.faq.uppercased()
        }
        self.webView.delegate = self
        if let url =  URL(string: self.urlString){
            let request = URLRequest(url:url)
            
            self.webView.loadRequest(request)
        }
        
        
        self.navigationView.setShadow()
    }
    
    //      MARK:-   IBActions
    //      =============================
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
}

// MARK: - UIWebViewDelegate methods.
// MARK: -
extension StaticPageVC : UIWebViewDelegate {
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        AppNetworking.showLoader()
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        AppNetworking.hideLoader()
    }
}


