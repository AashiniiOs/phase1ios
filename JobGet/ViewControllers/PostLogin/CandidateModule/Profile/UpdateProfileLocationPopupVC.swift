//
//  UpdateProfileLocationPopupVC.swift
//  JobGet
//
//  Created by Admin on 07/08/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import GoogleMaps

protocol CompleteProfileWithLocationDelegate: class{
    func completeProfileWithLocationDelegate()
    
}

class UpdateProfileLocationPopupVC: UIViewController {
    
    enum OpenLocationPopupFrom{
        
        case jobListCand
        case none
    }
    
    // MARK: - Outlets.
    // MARK: -
    
    @IBOutlet weak var blurView: UIView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var popupView: UIView!
    
    @IBOutlet weak var itsFineButton: UIButton!
    @IBOutlet weak var updateButton: UIButton!
    @IBOutlet weak var addresslabel: UILabel!
    
    // MARK: - Properties
    // MARK: -
    weak var delegate: CompleteProfileWithLocationDelegate?
    var address: String?
    var openLocationPopupFrom = OpenLocationPopupFrom.none
    var currentLocation: CLLocation?
    // MARK: - View life cycle.
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions.
    // MARK: -
    
    @IBAction func updateButtonTapp(_ sender: UIButton) {
        self.removePopupView()
        let placePicker = SearchByMapVC.instantiate(fromAppStoryboard: .settingsAndChat)
        placePicker.isOpenFromAddressPopup = true
        if self.openLocationPopupFrom == .jobListCand{
            placePicker.mapOpenFrom = .candJobList
        }
        placePicker.currentLocation = self.currentLocation
        present(placePicker, animated: true, completion: nil)
    }
    
    @IBAction func itsFineButtonTapp(_ sender: UIButton) {
        self.removePopupView()
        self.delegate?.completeProfileWithLocationDelegate()
    }
}

// MARK: - Private methods.
// MARK: -
private extension UpdateProfileLocationPopupVC{
    
    func initialSetup(){
        self.animatePopupView()
//        let tap = UITapGestureRecognizer(target: self, action: #selector(self.removePopupView))
//        tap.numberOfTapsRequired = 1
//        self.blurView.addGestureRecognizer(tap)
        
        
        if self.openLocationPopupFrom == .jobListCand{
            self.titleLabel.text = StringConstants.Update_The_Preferred_Location
            
            self.addresslabel.attributedText = CommonClass.getAttributedText(wholeString: "\(StringConstants.Your_Preferred_Location) \(address ?? "")", attributedString: "\(StringConstants.Your_Preferred_Location)", attributedColor: AppColors.appBlue, normalColor: AppColors.black46, fontSize: 14.0)
        }else{
            self.addresslabel.attributedText = CommonClass.getAttributedText(wholeString: "\(StringConstants.Your_profile_registered) \(address ?? "")", attributedString: "\(StringConstants.Your_profile_registered)", attributedColor: AppColors.appBlue, normalColor: AppColors.black46, fontSize: 14.0)
        }
    }
    
    
    func animatePopupView() {
        self.popupView.transform = CGAffineTransform(scaleX: 0.000001, y: 0.000001)
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.3, options: .curveEaseInOut, animations: {
            self.popupView.transform = CGAffineTransform.identity
        }, completion: nil)
    }
    
    @objc func removePopupView() {
        
        UIView.animate(withDuration: 0.5, animations: {
            self.popupView.transform = CGAffineTransform.init(scaleX: 0.000001, y: 0.000001)
        }, completion: { (true) in
            
            self.view.removeFromSuperview()
            self.removeFromParentViewController()
        })
    }
    
}


