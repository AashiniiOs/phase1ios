//
//  RecruiterTotalJobVC.swift
//  JobGet
//
//  Created by macOS on 20/06/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import CoreLocation

protocol OpenProfileSetup:class {
    func goToProfileSetup()
    
}

protocol UpdateEmployerDetailsDelegate: class {
    func updateEmployerDetails(recruiterProfileData: RecruiterInfoData)
}


class RecruiterTotalJobVC: BaseVC {

    //MARK:- Properties
    //==================
    var isPaginationEnable = false
    var shouldLoadMoreData = false
    var latitude = "42.361145"
    var longitude = "-71.057083"
    var pageNo = 0
    var jobId = ""
    weak var delegate: OpenProfileSetup?
    // weak var profileDelegate: OpenProfileSetup?
    var recruiterId = ""
    var jobList = [CandidateJobList]()
    var recruiterProfileData: RecruiterInfoData?
    var updateEmployerDetailsDelegate: UpdateEmployerDetailsDelegate?

    private var currentLocation: CLLocation? {
        
        didSet {
            print_debug(currentLocation)
            
            latitude = "\(currentLocation?.coordinate.latitude ?? 0.0)"
            longitude = "\(currentLocation?.coordinate.longitude ?? 0.0)"
            self.getJobList(pageNo: self.pageNo, loader: true)
        }
    }
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var totalJobTableView: UITableView!
    
    @IBOutlet weak var topShadowView: UIView!
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
}

//MARK:- Private Methods
//=======================
private extension RecruiterTotalJobVC {
    
    func initialSetup() {
        self.totalJobTableView.isScrollEnabled = false
        self.topShadowView.setShadow()
        self.totalJobTableView.delegate = self
        self.totalJobTableView.dataSource = self
        self.registerNib()
        self.totalJobTableView.enablePullToRefresh(tintColor: AppColors.themeBlueColor ,target: self, selector: #selector(refreshWhenPull(_:)))
        NotificationCenter.default.addObserver(self, selector: #selector(self.tableViewScrollEnableDisable(_:)), name: NSNotification.Name.init("manageTableViewScroll"), object: nil)
         self.getJobList(pageNo: self.pageNo, loader: true)
//        self.getLocation()
        
    }
   
    @objc func tableViewScrollEnableDisable(_ notifi: Notification) {
        
        if let data = notifi.userInfo as? JSONDictionary{
            
            if let isScrollEnable = data["isScrollEnable"] as? Bool ,isScrollEnable && !self.jobList.isEmpty{
                self.totalJobTableView.isScrollEnabled = false
                self.totalJobTableView.scrollToRow(at: IndexPath(row: 0, section: 0), at: .top, animated: false)
            }else{
                self.totalJobTableView.isScrollEnabled = true
            }
        }
    }
    
    func registerNib() {
        
        self.totalJobTableView.register(UINib(nibName: "JobListCell", bundle: nil), forCellReuseIdentifier: "JobListCell")
        self.totalJobTableView.register(UINib(nibName: "LoadMoreTableViewCell", bundle: nil), forCellReuseIdentifier: "LoadMoreTableViewCell")
    }
    
    func isLoadingIndexPath(_ indexPath: IndexPath) -> Bool {
        guard shouldLoadMoreData else { return false}
        
        return indexPath.row == self.jobList.count
    }
    
    func getLocation() {
        
        self.isPaginationEnable = false
        
        DispatchQueue.main.async {[weak self] in
            guard let strongSelf = self else{return}
            
            if CLLocationManager.authorizationStatus() == .denied {
                
                CommonClass.delayWithSeconds(1.0, completion: {
                    let popUpVc = AlertPopUpVC.instantiate(fromAppStoryboard: .Candidate)
                    popUpVc.delegate = self
                    popUpVc.modalPresentationStyle = .overCurrentContext
                    popUpVc.modalTransitionStyle = .crossDissolve
                    sharedAppDelegate.window?.rootViewController?.present(popUpVc, animated: true, completion: nil)
                })
            } else {
                SharedLocationManager.fetchCurrentLocation { (location) in
                    SharedLocationManager.locationManager.stopMonitoringSignificantLocationChanges()
                    SharedLocationManager.locationManager.stopUpdatingLocation()
                    strongSelf.currentLocation = location
                }
            }
        }
        
        SharedLocationManager.deniedLocationPermission { (error) in
            print_debug(error)
            
            self.getJobList(pageNo: self.pageNo, loader: true)
        }
    }
    
    func getCandidateJobDetail(loader: Bool, job_id: String, recruiterId: String,categotyId: String) {
        
        let value = AppUserDefaults.value(forKey: .userData)
        print_debug(value["is_profile_added"])
        print_debug(value["isExperience"])
        if value["isExperience"] == 0 {
            let sceen = ProfileErrorPopUpVC.instantiate(fromAppStoryboard: .Main)
            sceen.delegate = self
            sceen.fromSavedJob = true
            sceen.modalTransitionStyle = .crossDissolve
            sceen.modalPresentationStyle = .overFullScreen
            self.present(sceen, animated: true, completion: nil)
        } else {
            
            candidateAppliedJob(job_id: job_id, recruiterId: recruiterId, categotyId: categotyId)
        }
    }
}

//MARK: - Ontap Yes Button
//=========================
extension RecruiterTotalJobVC:  OnTapYesButton {
    
    func yesBtnTap() {
        
        self.delegate?.goToProfileSetup()
        
    }
    
    func editButtonTapped() {
        //        self.pop()
    }
}

//MARK: - IB Action and Target
//===============================
extension RecruiterTotalJobVC {
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func shareJobButtonTapped(_ sender: UIButton) {
        
        guard let idx = sender.tableViewIndexPath(self.totalJobTableView) else { return }
        self.displayShareSheet(shareContent: self.jobList[idx.row].jobShareUrl)
    }
    
    
    @objc func applyButtonTapped(_ sender: UIButton) {
        
        guard let index = sender.tableViewIndexPath(self.totalJobTableView) else { return }
        
        if let recruiterDetail = self.jobList[index.row].recruiterDetail {
            
            self.getCandidateJobDetail(loader: true, job_id: self.jobList[index.row].jobId, recruiterId: recruiterDetail.recruiterId, categotyId: self.jobList[index.row].categoryId)
            
            self.jobList[index.row].isApplied = 1
        }
    }
    
    @objc func refreshWhenPull(_ sender: UIRefreshControl) {
        sender.endRefreshing()
        self.pageNo = 0
        self.shouldLoadMoreData = false
        self.isPaginationEnable = false
        self.getJobList(pageNo: 0, loader: false)
    }
}

//MARK:- Web Services
//====================
extension RecruiterTotalJobVC {
    
    func getJobList(pageNo: Int, loader: Bool) {
        
        var param =  [String : Any]()
        
        param = [ApiKeys.page.rawValue      : pageNo,
                 ApiKeys.jobId.rawValue      : self.jobId,
                 ApiKeys.recruiterId.rawValue : self.recruiterId,
                 ApiKeys.latitude.rawValue  : AppUserDefaults.value(forKey: .userLat).doubleValue,
                 ApiKeys.longitude.rawValue : AppUserDefaults.value(forKey: .userLong).doubleValue
        ]
        
        print_debug(param)
        
        WebServices.getRecruiterJobList(parameters: param, loader: loader, success: { (json) in
            
            print_debug(json)
            if let errorCode = json[ApiKeys.code.rawValue].int, errorCode == error_codes.success {
                
                var data = [CandidateJobList]()
                self.recruiterProfileData = RecruiterInfoData(dict: json["data"]["detail"])
                
                if let infoData = self.recruiterProfileData{
                    self.updateEmployerDetailsDelegate?.updateEmployerDetails(recruiterProfileData: infoData)
                }
                
                let jobs = json[ApiKeys.data.rawValue][ApiKeys.jobs.rawValue].arrayValue
                
                for job in jobs {
                    data.append(CandidateJobList(dict: job))
                }
                
                if pageNo == 0 {
                    self.jobList.removeAll(keepingCapacity: false)
                }
                
                if self.shouldLoadMoreData {
                    self.jobList.append(contentsOf: data)
                } else {
                    self.jobList = data
                }
                
                self.pageNo = json[ApiKeys.page.rawValue].intValue
                self.shouldLoadMoreData = json[ApiKeys.next.rawValue].boolValue
                
                self.totalJobTableView.reloadData()
                
            } else {
                
                let msg = json[ApiKeys.message.rawValue].stringValue
                CommonClass.showToast(msg: msg)
            }
            
        }) { (error: Error) in
            CommonClass.showToast(msg: error.localizedDescription )
        }
    }
    
    func candidateAppliedJob(job_id: String, recruiterId: String, categotyId: String) {
        
        let params = [ApiKeys.userId.rawValue: AppUserDefaults.value(forKey: .userId), ApiKeys.jobId.rawValue: job_id, ApiKeys.recruiterId.rawValue: recruiterId, ApiKeys.categoryId.rawValue: categotyId] as [String : Any]
        print_debug(params)
        WebServices.applyJob(parameters: params, success: { (json) in
            
            print_debug(json)
            
            if let errorCode = json[ApiKeys.code.rawValue].int, errorCode == error_codes.success {
                NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "Refresh_After_Report_User"), object: nil)
                let sceen = ResetPasswordPopupVC.instantiate(fromAppStoryboard: .Main)
                commingFrom = .jobList
                sceen.jobId = job_id
                sceen.isFromSavedJob = true
                sceen.delegate = self
                sceen.modalTransitionStyle = .crossDissolve
                sceen.modalPresentationStyle = .overFullScreen
                self.present(sceen, animated: true, completion: nil)
                
            } else {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
            
        }) { (error) in
            print_debug(error)
            CommonClass.showToast(msg: error.localizedDescription)
            
        }
    }
}
//MARK: - Table view Delegate and DataSource
//============================================
extension RecruiterTotalJobVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = self.jobList.count
        
        return shouldLoadMoreData ? count + 1 : count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if isLoadingIndexPath(indexPath){
            guard let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.defaultReuseIdentifier, for: indexPath) as? LoadMoreTableViewCell else {
                fatalError("Failed to initialize LoadMoreTableViewCell")
            }
            self.isPaginationEnable = true
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "JobListCell") as! JobListCell
        cell.contentView.backgroundColor = AppColors.white
        
        let jobLocation = CLLocation(latitude: self.jobList[indexPath.row].latitude, longitude: self.jobList[indexPath.row].longitude)
        if let data = AppUserDefaults.value(forKey: .userData).dictionary, let lat = data["lat"]?.double,let lng = data["lng"]?.double{
            if lat != 0.0{
                let loc = CLLocation(latitude: lat, longitude: lng)
                var distance = (loc.distance(from: jobLocation)/1000).rounded()
                distance = (distance*0.62137).rounded()
                print_debug(distance)
                self.jobList[indexPath.row].distance = Int(distance)
                
            }else{
                let cllocation = CLLocation(latitude: 42.361145, longitude: -71.057083)
                var distance = (cllocation.distance(from: jobLocation)/1000).rounded()
                distance = (distance*0.62137).rounded()
                print_debug(distance)
                self.jobList[indexPath.row].distance = Int(distance)
            }
        }else{
            let cllocation = CLLocation(latitude: 42.361145, longitude: -71.057083)
            var distance = (cllocation.distance(from: jobLocation)/1000).rounded()
            distance = (distance*0.62137).rounded()
            print_debug(distance)
            self.jobList[indexPath.row].distance = Int(distance)
            
        }
        cell.populateCandidateJobDetail(self.jobList[indexPath.row])
        cell.descLabel.text = self.jobList[indexPath.row].jobDescription
        
        cell.shareButton.addTarget(self, action: #selector(self.shareJobButtonTapped(_:)), for: .touchUpInside)
        
        cell.applyButton.addTarget(self, action: #selector(self.applyButtonTapped(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard self.shouldLoadMoreData else { return }
        
        if indexPath.row == self.jobList.count - 1 && self.shouldLoadMoreData   && isPaginationEnable {  //for 2nd last cell
            getJobList(pageNo: self.pageNo, loader: false)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let sceen = CandidateJobDetailVC.instantiate(fromAppStoryboard: .Candidate)
        sceen.jobId = self.jobList[indexPath.row].jobId
        
        sceen.isSaved   =  self.jobList[indexPath.row].isSaved
        sceen.distance = self.jobList[indexPath.row].distance
        sceen.delegate = self
        sceen.profileSetupdelegate = self
        sceen.recruiterId = self.jobList[indexPath.row].recruiterDetail?.recruiterId ?? ""
        sceen.jobDetail = self.jobList[indexPath.row]
        sceen.commingFromScreen = .employerDetail
        
        if let detail = self.jobList[indexPath.row].recruiterDetail {
            sceen.recruiterDetail = detail
        }
        let navigation = UINavigationController(rootViewController: sceen)
        navigation.isNavigationBarHidden = true
        
        
        sharedAppDelegate.navigationController?.present(navigation, animated: true, completion: nil)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        print_debug(scrollView.contentOffset.y)
    }
}

extension RecruiterTotalJobVC: OpenProfileSetup {
    func goToProfileSetup() {
        self.delegate?.goToProfileSetup()
    }
}

//MARK:- Location alert popup Delegate
//===========================================
extension RecruiterTotalJobVC: AlertPopUpDelegate {
    
    func didTapAffirmativeButton() {
        //        TODO: Add settings
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: { (success) in
                
            })
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    func didTapNegativeButton() {
        print_debug("location access denied")
        self.getJobList(pageNo: self.pageNo, loader: true)
    }
    
}

extension RecruiterTotalJobVC: ShouldOpenStatusPopup {
    
    func presentShortlistedPopup(jobApplyId: String, categoryId: String) {
        
        guard let index = self.jobList.index(where: { (model) -> Bool in
            
            return model.jobId == jobApplyId
        })else{return}
        
        let indexRow = self.jobList.startIndex.distance(to: index)
        
        self.jobList[indexRow].isApplied = 1
        self.totalJobTableView.reloadData()
    }
}

