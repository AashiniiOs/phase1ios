//
//  AlertPopUpVC.swift
//  JobGet
//
//  Created by appinventiv on 19/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

@objc protocol AlertPopUpDelegate: class{
    
    func didTapAffirmativeButton()
    @objc optional func didTapDeativateButton()
    @objc optional func didTabRejectBtn()
    @objc optional func didTabCancelBtn()
    func didTapNegativeButton()
}

enum AlertType{
    
    case reportUser
    case reportJob
    case deleteNotification
    case deleteSingleNotification
    case reportCandidate
    case cancelJob
    case completeProfile
    case logoutIncomplete
    case userNotExist
    case cardDelete
    case pauseImage
    case none
    
}

class AlertPopUpVC: UIViewController {
    
    weak var delegate: AlertPopUpDelegate?
    @IBOutlet weak var alertIcon: UIImageView!
    @IBOutlet weak var messageOne: UILabel!
    @IBOutlet weak var allowButton: UIButton!
    @IBOutlet weak var denyButton: UIButton!
    @IBOutlet weak var imageBgView: UIView!
    
    var alertImage : UIImage = #imageLiteral(resourceName: "icHomeLocation_1")
    var messageOneText = StringConstants.WantsFetchLocation
    var messageTwoText = StringConstants.Jobget_cannot_access_your_location_Please_allow_from_settings
    var allowButtonText  = StringConstants.Allow
    var denyButtonText  = StringConstants.Deny
    var commongFromSetting = false
    var FromDeactivateAccount = false
    var commingFromReject = false
    var fromOpenChat = false
    var alertType = AlertType.none
    var isOpenFromJobApplied = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.messageOne.text = messageOneText
        //  self.messageTwo.text = messageTwoText
        self.alertIcon.image = alertImage
        self.allowButton.setTitle(allowButtonText, for: .normal)
        self.denyButton.setTitle(denyButtonText, for: .normal)
        self.imageBgView.setCorner(cornerRadius: imageBgView.height/2, clip: true)

        if commongFromSetting {
            self.messageOne.text = StringConstants.LogoutConfirmation
            self.allowButton.setTitle(StringConstants.yes.uppercased(), for: .normal)
            self.denyButton.setTitle(StringConstants.no.uppercased(), for: .normal)
            self.alertIcon.image = #imageLiteral(resourceName: "icForgotPasswordSend")
        } else if commingFromReject {
            
            self.messageOne.text = StringConstants.RejectConfirmation
            self.allowButton.setTitle(StringConstants.yes.uppercased(), for: .normal)
            self.denyButton.setTitle(StringConstants.no.uppercased(), for: .normal)
            self.alertIcon.image = #imageLiteral(resourceName: "icForgotPasswordSend")
        } else if FromDeactivateAccount {
            var WholeText = ""
            var attributedText = ""
            self.alertIcon.image = #imageLiteral(resourceName: "ic_pause")
            self.alertIcon.contentMode = .scaleAspectFit
            if AppUserDefaults.value(forKey: .userType).stringValue == UserType.recuriter.rawValue{
                WholeText = "\(StringConstants.PAUSE_ACCOUNT.uppercased()) \n\n\(StringConstants.RECRUITER_PAUSE_ACCOUNT_POPUP_TEXT)"
                attributedText = StringConstants.RECRUITER_PAUSE_ACCOUNT_POPUP_TEXT
            }else{
                WholeText = "\(StringConstants.PAUSE_ACCOUNT.uppercased()) \n\n\(StringConstants.CANDIDATE_PAUSE_ACCOUNT_POPUP_TEXT)"
                attributedText = StringConstants.CANDIDATE_PAUSE_ACCOUNT_POPUP_TEXT
            }
            let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: WholeText )
            attributedString.setColorForText(textForAttribute: attributedText, withColor: AppColors.gray152, font: AppFonts.Poppins_Light.withSize(15))
            self.messageOne.attributedText = attributedString
            self.allowButton.setTitle(StringConstants.PAUSE, for: .normal)
//            let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: StringConstants.Deactivate_Account )
//            attributedString.setColorForText(textForAttribute: StringConstants.Deactivate_Confirm, withColor: AppColors.gray152, font: AppFonts.Poppins_Light.withSize(15))
//            self.messageOne.attributedText = attributedString
//            self.allowButton.setTitle(StringConstants.DEACTIVATES, for: .normal)
            self.denyButton.setTitle(StringConstants.NOT_NOW, for: .normal)
//            self.alertIcon.image = #imageLiteral(resourceName: "icForgotPasswordSend")
        } else if fromOpenChat {
            let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: StringConstants.DeleteExpConfirm )
            attributedString.setColorForText(textForAttribute: StringConstants.AttributedDelete, withColor: AppColors.gray152, font: AppFonts.Poppins_Light.withSize(15))
            self.messageOne.attributedText = attributedString
            self.allowButton.setTitle(StringConstants.yes.uppercased(), for: .normal)
            self.denyButton.setTitle(StringConstants.no.uppercased(), for: .normal)
            self.alertIcon.image = #imageLiteral(resourceName: "icForgotPasswordSend")
        } else if self.alertType == AlertType.reportJob{
            
            // self.messageOne.text = "Are you sure want to report this job?"
            
            let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: StringConstants.ReportJob )
            attributedString.setColorForText(textForAttribute: StringConstants.VisibleJob, withColor: AppColors.gray152, font: AppFonts.Poppins_Light.withSize(15))
            self.messageOne.attributedText = attributedString
            
            self.allowButton.setTitle(StringConstants.yes.uppercased(), for: .normal)
            self.denyButton.setTitle(StringConstants.no.uppercased(), for: .normal)
            self.alertIcon.image = #imageLiteral(resourceName: "icForgotPasswordSend")
        }else if self.alertType == AlertType.reportUser{
            
            let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: StringConstants.ReportEmployer )
            attributedString.setColorForText(textForAttribute: StringConstants.EmployerVisibleJob, withColor: AppColors.gray152, font: AppFonts.Poppins_Light.withSize(15))
            self.messageOne.attributedText = attributedString
            self.allowButton.setTitle(StringConstants.yes.uppercased(), for: .normal)
            self.denyButton.setTitle(StringConstants.no.uppercased(), for: .normal)
            self.alertIcon.image = #imageLiteral(resourceName: "icForgotPasswordSend")
        }else if self.alertType == AlertType.deleteNotification{
            
            self.messageOne.text = StringConstants.DeleteNotification
            
            self.allowButton.setTitle(StringConstants.yes.uppercased(), for: .normal)
            self.denyButton.setTitle(StringConstants.no.uppercased(), for: .normal)
            self.alertIcon.image = #imageLiteral(resourceName: "icForgotPasswordSend")
        }else if self.alertType == AlertType.deleteSingleNotification{
            
            self.messageOne.text = StringConstants.DeleteSingleNotification
            
            self.allowButton.setTitle(StringConstants.yes.uppercased(), for: .normal)
            self.denyButton.setTitle(StringConstants.no.uppercased(), for: .normal)
            self.alertIcon.image = #imageLiteral(resourceName: "icForgotPasswordSend")
        } else if self.alertType == AlertType.reportCandidate {
            
            let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: StringConstants.ReportCandidate )
            attributedString.setColorForText(textForAttribute: StringConstants.VisibleReport, withColor: AppColors.gray152, font: AppFonts.Poppins_Light.withSize(15))
            self.messageOne.attributedText = attributedString
            self.allowButton.setTitle(StringConstants.yes.uppercased(), for: .normal)
            self.denyButton.setTitle(StringConstants.no.uppercased(), for: .normal)
            self.alertIcon.image = #imageLiteral(resourceName: "icForgotPasswordSend")
        } else if self.alertType == AlertType.cancelJob {
            
            self.messageOne.text = StringConstants.DeleteJob
            self.allowButton.setTitle(StringConstants.yes.uppercased(), for: .normal)
            self.denyButton.setTitle(StringConstants.no.uppercased(), for: .normal)
            self.alertIcon.image = #imageLiteral(resourceName: "icForgotPasswordSend")
            
        } else if self.alertType == AlertType.logoutIncomplete{
            
            let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: StringConstants.InformationLost )
            attributedString.setColorForText(textForAttribute: StringConstants.LostInformation, withColor: AppColors.gray152, font: AppFonts.Poppins_Light.withSize(15))
            self.messageOne.attributedText = attributedString
            if isOpenFromJobApplied{
                self.allowButton.setTitle(StringConstants.EXIT, for: .normal)
                self.denyButton.setTitle(StringConstants.NO_COMPLETE, for: .normal)
            }else{
                self.allowButton.setTitle(StringConstants.LOGOFF, for: .normal)
                self.denyButton.setTitle(StringConstants.COMPLETE, for: .normal)
            }
            
            self.alertIcon.image = #imageLiteral(resourceName: "icForgotPasswordSend")
        } else if self.alertType == AlertType.userNotExist {
            let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: StringConstants.LoginOtherDevice )
            attributedString.setColorForText(textForAttribute:
                "", withColor: AppColors.gray152, font: AppFonts.Poppins_Light.withSize(15))
            self.messageOne.attributedText = attributedString
            self.allowButton.setTitle(StringConstants.Logout.uppercased(), for: .normal)
            self.denyButton.isHidden = true
            self.alertIcon.image = #imageLiteral(resourceName: "icHomeError")
        }else if self.alertType == AlertType.cardDelete {
            
            self.messageOne.text = StringConstants.DeleteCardText
            self.allowButton.setTitle(StringConstants.yes.uppercased(), for: .normal)
            self.denyButton.setTitle(StringConstants.no.uppercased(), for: .normal)
            self.alertIcon.image = #imageLiteral(resourceName: "icForgotPasswordSend")
            
        }else if self.alertType == AlertType.pauseImage{
            self.messageOne.text = "WELCOME BACK! WE HAVE MISSED YOU."
            self.allowButton.setTitle(StringConstants.Ok.uppercased(), for: .normal)
            self.denyButton.isHidden = true
            self.alertIcon.sd_setImage(with: URL(string: User.getUserModel().imageUrl), placeholderImage: #imageLiteral(resourceName: "ic_user_"), options: .init(rawValue: 0), completed: nil)

        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    @IBAction func allowButtonTapped(_ sender: UIButton) {
        
        if self.FromDeactivateAccount {
            self.delegate?.didTapDeativateButton?()
            
        } else if self.commingFromReject {
            self.delegate?.didTabRejectBtn?()
            
        } else if self.alertType == AlertType.userNotExist {
            self.logoutUser()
        }  else {
            self.delegate?.didTapAffirmativeButton()
        }
        
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func denyButtonTapped(_ sender: UIButton) {
        self.delegate?.didTapNegativeButton()
        self.dismiss(animated: true, completion: nil)
    }
    
    func logoutUser() {
        if let userId = User.getUserModel().user_id {
            UIApplication.shared.applicationIconBadgeNumber = 0
            AppUserDefaults.removeAllValues()
            CommonClass().goToLogin()
        }else{
            self.showAlert(msg: "Cannot logout user")
        }
    }
    
}

