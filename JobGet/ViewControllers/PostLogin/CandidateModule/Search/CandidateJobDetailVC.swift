////
//  CandidateJobDetailVC.swift
//  JobGet
//
//  Created by appinventiv on 18/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import MXParallaxHeader
import GoogleMaps


class CandidateJobDetailVC: BaseVC {
    
    //MARK:- Enum
    //=============
    enum CommingFrom {
        case mapView
        case jobListCell
        case employerDetail
        case deepLink
    }
    
    //MARK:- Properties
    //==================
    var commingFromScreen: CommingFrom = .jobListCell
    let parallexHeaderView    = JobDetailHeaderView()
    var jobId                   = ""
    var recruiterId            = ""
    var categoryId           = ""
    var isSaved               = 0
    var recruiterDetail: JobListRecruiterDetail?
    var candidateJobDetail: CandidateJobDetail?
    var jobDetail: CandidateJobList?
    var fromProfile = false
    var distance: Int = 0
    var delegate : ShouldOpenStatusPopup?
    weak var profileSetupdelegate: OpenProfileSetup?
    var jobList = [CandidateJobList]()
    var pageNo = 0
    var isPaginationEnable = false
    var shouldLoadMoreData = false
    var isSimilarJobShow = false
    var pushCount = 0
    var currentLocation:CLLocation?
    var selectedIndex : IndexPath?
    var isHideHeartIcon = false
    
    //    MARK:- IBOutlets
    //====================================
   
    @IBOutlet weak var headerImageTopConstr: NSLayoutConstraint!
    @IBOutlet var headerView: UIView!
    
    @IBOutlet weak var gradientView: UIView!
    //    @IBOutlet weak var containerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var jobNameLabel: UILabel!
    @IBOutlet weak var jobImageView: UIImageView!
    @IBOutlet weak var jobNameContainerView: UIView!
    
    @IBOutlet weak var navigationBgView: UIView!
    @IBOutlet weak var screenHeading: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var jobDetailTable: UITableView!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var applyButton: UIButton!
    @IBOutlet weak var applyViewHeightConstraint: NSLayoutConstraint!
    //  @IBOutlet weak var bookmarkButton: UIButton!
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var sharePopupView: UIView!
    
    
    //    MARK:- View life cycle
    //    =====================================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if self.fromProfile {
            self.fromProfile = false
            self.candidateAppliedJob()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
}

private extension CandidateJobDetailVC {
    
    private func initialSetup(){
        self.jobDetailTable.delegate = self
        self.jobDetailTable.dataSource = self
        self.jobDetailTable.tableHeaderView = self.headerView
        self.setupCornerRadiusAndFonts()
        self.registerCell()
        
        self.blurView.isHidden = true
        self.sharePopupView.isHidden = true
        
        if SCREEN_HEIGHT < 800{
            self.headerImageTopConstr.constant = 0
        }else{
            self.headerImageTopConstr.constant = -25
        }
        self.sharePopupView.setCorner(cornerRadius: 3.0, clip: true)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.removePopup(gesture:)))
        self.blurView.addGestureRecognizer(tap)
        
        self.getCandidateJobDetail(loader: true)
        let panGest = UIPanGestureRecognizer(target: self, action: #selector(self.swipeDown(_:)))
        self.navigationView.addGestureRecognizer(panGest)
        
    }
    
    private func isLoadingIndexPath(_ indexPath: IndexPath) -> Bool {
        guard shouldLoadMoreData else { return false}
        return indexPath.row == self.jobList.count
    }
    
    func setupCornerRadiusAndFonts() {
        CommonClass.setNextButton(button: self.applyButton)
        self.jobDetailTable.setShadow(offset: CGSize(width: 0.0, height: 2.0))
    }
    
    func hideShowBookmarkBtn(status: Bool) {
        guard let cell = self.jobDetailTable.cellForRow(at: IndexPath(row: 0, section: 0)) as? JobDetailCell else {return }
        cell.bookMarkBtn.isHidden = status
    }
    
    func registerCell() {
        let nib = UINib(nibName: "JobDetailCell", bundle: nil)
        self.jobDetailTable.register(nib, forCellReuseIdentifier: "JobDetailCell")
        self.jobDetailTable.register(UINib(nibName: "JobDescCell", bundle: nil), forCellReuseIdentifier: JobDescCell.identifier)
    }
    
    func populateDataInToParallexHeader() {
        if let details = candidateJobDetail {
            let image = details.jobImage.replacingOccurrences(of: "\"", with: "")
            self.jobImageView.sd_setImage(with: URL(string: image), placeholderImage: #imageLiteral(resourceName: "icHomeCategoryPlaceholder") , completed: nil)
            self.jobNameLabel.text = details.companyName
        }
    }
    
    @objc func swipeDown(_ sender: UIPanGestureRecognizer){
        
    }
    
    
}

//MARK: - IB Action and Target
//===============================
extension CandidateJobDetailVC {
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        if commingFromScreen == .jobListCell || commingFromScreen == .deepLink || commingFromScreen == .mapView {
            self.navigationController?.popViewController(animated: true)
        } else {
            
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func applyButtonTapped(_ sender: UIButton) {
        let value = AppUserDefaults.value(forKey: .userData)
        pushCount = 0
        if value["isExperience"] == 0{
            let sceen = ProfileErrorPopUpVC.instantiate(fromAppStoryboard: .Main)
            sceen.delegate = self
            self.view.addSubview(sceen.view)
            self.add(childViewController: sceen)
            
        }else{
            self.candidateAppliedJob()
        }
    }
    
    
    @IBAction func moreButtonTapped(_ sender: UIButton) {
        
        self.blurView.alpha = 0.0
        self.sharePopupView.alpha = 0.0
        self.sharePopupView.transform = CGAffineTransform(scaleX: 0.000001, y: 0.000001)
        UIView.animate(withDuration: 0.3, delay: 0, options: [.curveEaseInOut], animations: {
            self.blurView.alpha = 0.65
            self.sharePopupView.alpha = 1.0
            self.blurView.isHidden = false
            self.sharePopupView.transform = .identity
            
            self.sharePopupView.isHidden = false
        }, completion: nil)
    }
    
    
    @IBAction func shareBtnTapped(_ sender: UIButton) {
        
        self.displayShareSheet(shareContent: self.candidateJobDetail?.jobShareUrl ?? "")
        self.blurView.isHidden = true
        self.sharePopupView.isHidden = true
    }
    
    @IBAction func reportBtnTapped(_ sender: UIButton) {
        let popUpVc = AlertPopUpVC.instantiate(fromAppStoryboard: .Candidate)
        popUpVc.delegate = self
        popUpVc.alertType = .reportJob
        popUpVc.modalPresentationStyle = .overCurrentContext
        popUpVc.modalTransitionStyle = .crossDissolve
        self.present(popUpVc, animated: true, completion: nil)
        
    }
    
    @objc func didTappedEmployerName(_ sender: UIButton) {
        if self.commingFromScreen != .employerDetail {
            let employerSceen = EmployerDetailVC.instantiate(fromAppStoryboard: .settingsAndChat)
            
            employerSceen.candidateJobDetail = self.jobDetail 
            
            self.navigationController?.pushViewController(employerSceen, animated: true)
        }else{
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    @objc private func chatButtonTapped(_ sender: UIButton) {
        //        TODO:- Show loader if required
        if let jobDetails = self.candidateJobDetail, let recruiterId = jobDetails.recruiterDetail?.recruiterId {
            print_debug(recruiterId)
            ChatHelper.getUserDetails(userId: recruiterId, completion: { [weak self](member) in
                
                guard let _self = self else { return }
                
                if let member = member {
                    let chatVc = PersonalChatVC.instantiate(fromAppStoryboard: .Candidate)
                    chatVc.chatMember = member
                    if _self.commingFromScreen == .jobListCell || _self.commingFromScreen == .deepLink {
                        _self.navigationController?.pushViewController(chatVc, animated: false)
                    } else {
                        chatVc.fromPendingJobVC = true
                        _self.navigationController?.pushViewController(chatVc, animated: false)
                    }
                    
                }else{
                    _self.showAlert(msg: StringConstants.Recruiter_not_available)
                }
            })
        }else{
            self.showAlert(msg: StringConstants.Recruiter_not_available)
        }
    }
    
    @objc private func bookMarkBtnTapped(_ sender: UIButton){
        sender.isSelected = !sender.isSelected
        if sender.isSelected {
            self.saveJob(loader: false, type: 1)
        } else {
            self.saveJob(loader: false, type: 2)
        }
    }
    
    @objc private func locationBtnTapped(_ sender: UIButton){
        //let url = "http://maps.apple.com/maps?saddr=\(from.latitude),\(from.longitude)&daddr=\(to.latitude),\(to.longitude)"
       // UIApplication.shared.openURL(URL(string:url)!)
//        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string:"http://maps.apple.com/maps?saddr=\(AppUserDefaults.value(forKey: .userLat).stringValue),\(AppUserDefaults.value(forKey: .userLong).stringValue)&&daddr=\(self.candidateJobDetail?.jobLatitude ?? 0.0),\(self.candidateJobDetail?.jobLongitude ?? 0.0)")!, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
            }
//        } else {
//            print("Can't use comgooglemaps://")
//        }
    }
    
    
    @objc func removePopup(gesture: UITapGestureRecognizer) {
        self.blurView.alpha = 0.65
        self.sharePopupView.alpha = 1.0
        UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            self.blurView.alpha = 0.0
            self.sharePopupView.alpha = 0.0
            self.blurView.isHidden = true
            self.sharePopupView.isHidden = true
        }, completion: nil)
    }
}

//MARK: - Web services
//======================
extension CandidateJobDetailVC {
    
    func similarJobsApi(loader: Bool, pageNo: Int){
        
        let param : JSONDictionary = [
            ApiKeys.jobId.rawValue: self.jobId, "categoryId": self.candidateJobDetail?.categoryId ?? "", ApiKeys.latitude.rawValue: self.currentLocation?.coordinate.latitude ?? 42.361145, ApiKeys.longitude.rawValue: self.currentLocation?.coordinate.longitude ?? -71.057083, "radius": "20"]
        
        WebServices.similarJobsApi(parameters: param, loader: loader, success: { (json) in
            var data = [CandidateJobList]()
            let jobs = json[ApiKeys.data.rawValue][ApiKeys.jobs.rawValue].arrayValue
            for job in jobs {
                data.append(CandidateJobList(dict: job))
            }
            if pageNo == 0 {
                self.jobList.removeAll(keepingCapacity: false)
            }
            if self.shouldLoadMoreData {
                self.jobList.append(contentsOf: data)
            } else {
                self.jobList = data
            }
            self.pageNo = json[ApiKeys.page.rawValue].intValue
            self.shouldLoadMoreData = json[ApiKeys.next.rawValue].boolValue
            self.jobDetailTable.reloadData()
        }) { (error) in
            CommonClass.showToast(msg: error.localizedDescription)
        }
    }
    
    func getCandidateJobDetail(loader: Bool) {
        
        let params = [ApiKeys.jobId.rawValue: self.jobId]
        WebServices.getCandidateJobDetail(parameters: params, loader: loader, success: { (json) in
            if let errorCode = json[ApiKeys.code.rawValue].int, errorCode == error_codes.success {
                let values = json[ApiKeys.data.rawValue]
                
                self.candidateJobDetail = CandidateJobDetail(dict: values)
                self.jobDetail  = CandidateJobList(dict: values)
                if let categoryDetail = self.candidateJobDetail {
                    self.categoryId = categoryDetail.categoryId
                }
                self.populateDataInToParallexHeader()
                // self.setupAppliedButton()
                guard let data = self.candidateJobDetail else{return}
                self.applyButton.isHidden = false
                if data.isApplied == 2{
                    self.applyButton.backgroundColor = AppColors.white
                    self.applyButton.borderColor = AppColors.appBlue
                    self.applyButton.borderWidth = 1.0
                    self.applyButton.isEnabled = false
                    self.hideShowBookmarkBtn(status: true)
                    self.isHideHeartIcon = true
                    self.applyButton.setTitleColor(AppColors.appBlue, for: .normal)
                  self.applyButton.setTitle(StringConstants.Shortlisted.uppercased(), for: .normal)
                } else if data.isApplied == 1 {
                    self.applyButton.backgroundColor = AppColors.white
                    self.applyButton.borderColor = AppColors.appBlue
                    self.applyButton.borderWidth = 1.0
                    self.applyButton.isEnabled = false
                    self.isHideHeartIcon = true
                    self.hideShowBookmarkBtn(status: true)
                    self.applyButton.setTitleColor(AppColors.appBlue, for: .normal)
                    self.applyButton.setTitle(StringConstants.Applied.uppercased(), for: .normal)
                } else {
                    self.applyButton.setTitle(StringConstants.Apply.uppercased(), for: .normal)
                    self.hideShowBookmarkBtn(status: false)
                    self.isHideHeartIcon = false
                    self.applyButton.isEnabled = true
                    self.applyButton.setTitleColor(AppColors.white, for: .normal)
                    self.applyButton.backgroundColor = AppColors.appBlue
                }
                self.populateDataInToParallexHeader()
                self.similarJobsApi(loader: true, pageNo: 0)
                self.jobDetailTable.reloadData()
            } else {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
            
        }) { (error) in
            print_debug(error)
             self.applyButton.isHidden = true
            CommonClass.showToast(msg: error.localizedDescription)
            
        }
    }
    
    func saveJob(loader: Bool, type: Int) {
        guard let recruiter = self.recruiterDetail else{return}
        let params = [ApiKeys.jobId.rawValue: self.jobId, ApiKeys.userId.rawValue: AppUserDefaults.value(forKey: .userId), "type": type, ApiKeys.recruiterId.rawValue: recruiter.recruiterId, "categoryId": self.candidateJobDetail?.categoryId ?? ""] as [String: Any]
        
        WebServices.saveJob(parameters: params, loader: loader, success: { (json) in
            if let errorCode = json[ApiKeys.code.rawValue].int, errorCode == error_codes.success {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            } else {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
            
        }) { (error) in
            print_debug(error)
            CommonClass.showToast(msg: error.localizedDescription)
            
        }
    }
    
    func candidateAppliedJob() {
        
        let params = [ApiKeys.userId.rawValue: AppUserDefaults.value(forKey: .userId), ApiKeys.jobId.rawValue: self.jobId, ApiKeys.recruiterId.rawValue: self.recruiterId, "categoryId": self.categoryId] as [String : Any]
        
        print_debug(params)
        WebServices.applyJob(parameters: params, success: { (json) in
            if let errorCode = json[ApiKeys.code.rawValue].int, errorCode == error_codes.success {
                
                let sceen = ResetPasswordPopupVC.instantiate(fromAppStoryboard: .Main)
                commingFrom = .jobList
                sceen.jobId = self.jobId
                sceen.delegate = self
               
                self.view.addSubview(sceen.view)
                self.add(childViewController: sceen)
//                NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "Refresh_After_Report_User"), object: nil)
            } else {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
            
        }) { (error) in
            print_debug(error)
            CommonClass.showToast(msg: error.localizedDescription)
            
        }
    }
    
    func reportJobService(){
        
        WebServices.reportJob(parameters: [ ApiKeys.jobId.rawValue: self.jobId], loader: true, success: { (json) in
            self.blurView.isHidden = true
            self.sharePopupView.isHidden = true
            
            guard let vcArray = self.navigationController?.viewControllers else{return}
            for vc in vcArray{
                
                
                if let viewcontroller = vc as? CandidateTabBarVC {
                    let msg = json["message"].stringValue
                    CommonClass.showToast(msg: msg)
                    self.navigationController?.popToViewController(viewcontroller, animated: true)
                    NotificationCenter.default.post(name: NSNotification.Name.init(rawValue: "Refresh_After_Report_User"), object: nil)
                }
            }
            
            print_debug(self.navigationController?.viewControllers)
        }) { (error) in
            
            print_debug(error.localizedDescription)
        }
    }
}

//    MARK:- Tableview datasource
//    =====================================
extension CandidateJobDetailVC: UITableViewDataSource, UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let _ = self.candidateJobDetail {
            if  self.isSimilarJobShow || self.jobList.isEmpty{
                return 6
            }else{
                return 7
            }
            
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "JobDetailCell") as? JobDetailCell else { fatalError("invalid cell \(self)")
            }
            cell.chatContainerView.isHidden = false
            cell.chatButton.isHidden = false
            cell.jobDistanceLabel.isHidden = false
            cell.yearContainerView.isHidden = true
            cell.chatButton.addTarget(self, action: #selector(self.chatButtonTapped(_:)), for: .touchUpInside)
            cell.bookMarkBtn.addTarget(self, action: #selector(self.bookMarkBtnTapped(_:)), for: .touchUpInside)
            cell.locationBtn.addTarget(self, action: #selector(self.locationBtnTapped(_:)), for: .touchUpInside)
            
            cell.jobDistanceLabel.text = "\(CommonClass.calculateDistanceBtw(from: self.candidateJobDetail?.jobLatitude ?? 0.0, long: self.candidateJobDetail?.jobLongitude ?? 0.0)) \(StringConstants.Miles_Away)"
            cell.bookMarkBtn.isHidden = self.isHideHeartIcon
          
            
            if let jobDetail = self.candidateJobDetail {
                cell.populateCandiateJobDetail(data: jobDetail)
            }
            return cell
            
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "EmployerNameCell") as? EmployerNameCell else { fatalError("invalid cell \(self)")
            }
            cell.employeeNameLabel.text = StringConstants.EmployerName
            cell.nameLabel.textColor    = AppColors.themeBlueColor
            
            
            cell.nameButton.addTarget(self, action: #selector(didTappedEmployerName(_ :)), for: .touchUpInside)
            
            if let detail = self.candidateJobDetail?.recruiterDetail{
                cell.populateEmployerName(detail)
            }
            return cell
            
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: JobDescCell.identifier) as? JobDescCell else { fatalError("invalid cell \(self)")
            }
            
            if let detail = self.candidateJobDetail {
                cell.populateJobDescrption(data: detail)
            }
            
            return cell
        case 3:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "JobSalarycell") as? JobSalarycell else { fatalError("invalid cell \(self)")
            }
            if let detail = self.candidateJobDetail {
                cell.populateSalary(data: detail)
            }
            return cell
        case 4:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: JobDescCell.identifier) as? JobDescCell else { fatalError("invalid cell \(self)") }
            
            if let detail = self.candidateJobDetail {
                cell.populate(with: detail)
            }
            return cell
            
        case 5:
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CandidateJobLocationCell") as? CandidateJobLocationCell else { fatalError("invalid cell \(self)")
            }
            
            if let latitude = self.candidateJobDetail?.jobLatitude, let longitude = self.candidateJobDetail?.jobLongitude {
                
                let marker = GMSMarker()
                marker.position = CLLocationCoordinate2DMake(latitude, longitude)
                
                marker.icon = #imageLiteral(resourceName: "icRedMapPin")
                marker.map = cell.mapView
                cell.mapView.camera = GMSCameraPosition.camera(withTarget: marker.position, zoom: 13)
                cell.mapView.delegate = self
            }
            
            return cell
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SimilarJobCell") as? SimilarJobCell else { fatalError("invalid cell \(self)")
            }
            cell.similarJobCollectionView.delegate = self
            cell.similarJobCollectionView.dataSource = self
            
            if !self.jobList.isEmpty {
                cell.similarJobCollectionView.reloadData()
            }
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 5 {
            return 200
        } else if indexPath.row == 6 {
            return 185
        }else {
            return UITableViewAutomaticDimension
        }
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        //let parallexHeaderView    = JobDetailHeaderView()
//        return self.parallexHeaderView
//    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 230.0
//    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.00001
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        print_debug(scrollView.contentOffset)
        
        if scrollView.contentOffset.y > 100{
            
            self.navigationBgView.backgroundColor = AppColors.themeBlueColor
            self.navigationView.backgroundColor = AppColors.themeBlueColor
            self.gradientView.isHidden = true
        }
        else{
            self.gradientView.isHidden = false
            self.navigationView.backgroundColor = UIColor.clear
            self.navigationBgView.backgroundColor = UIColor.clear
        }
    }
}


//    MARK:- CollectionView daelegate and datasource
//    =====================================
extension CandidateJobDetailVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let count = self.jobList.count
        return shouldLoadMoreData ? count + 1 : count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SimilarJobCVCell", for: indexPath) as? SimilarJobCVCell else { fatalError("invalid cell \(self)")
        }
        if isLoadingIndexPath(indexPath){
            self.isPaginationEnable = true
            cell.activityIndicator.startAnimating()
            cell.activityIndiCatView.isHidden = false
            cell.bgView.isHidden = true
            
        }else{
            cell.activityIndicator.startAnimating()
            cell.activityIndiCatView.isHidden = true
            cell.bgView.isHidden = false
            cell.jobNameLabel.text = self.jobList[indexPath.item].jobTitle
            cell.jobImage.sd_setImage(with: URL(string: self.jobList[indexPath.item].jobImage),  placeholderImage: #imageLiteral(resourceName: "icHomeCategoryPlaceholder1"), completed: nil)
            
            cell.addressLabel.text = "\(self.jobList[indexPath.item].jobCity ), \(self.jobList[indexPath.item].jobState )"
            
        }
        
        if !self.isPaginationEnable{
            cell.activityIndicator.stopAnimating()
        }
       return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard self.shouldLoadMoreData else { return }
        
        if indexPath.row == self.jobList.count - 1 && self.shouldLoadMoreData   && isPaginationEnable {  //for 2nd last cell
            self.similarJobsApi(loader: false, pageNo: self.pageNo)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let sceen = CandidateJobDetailVC.instantiate(fromAppStoryboard: .Candidate)
        sceen.jobId = self.jobList[indexPath.row].jobId
        sceen.isSaved   =  self.jobList[indexPath.row].isSaved
        sceen.distance = self.jobList[indexPath.row].distance
        self.jobList[indexPath.row].totalJobView = self.jobList[indexPath.row].totalJobView + 1
        
        sceen.categoryId = self.categoryId
        
        self.pushCount = 1
        sceen.delegate = self
        sceen.jobDetail = self.jobList[indexPath.row]
        sceen.isSimilarJobShow = true
        sceen.commingFromScreen = .jobListCell
        if let detail = self.jobList[indexPath.row].recruiterDetail {
            sceen.recruiterDetail = detail
            sceen.recruiterId = detail.recruiterId
        }
        self.navigationController?.pushViewController(sceen, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 115, height: 130)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsetsMake(0, 15, 0, 15)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 15.0
    }
}

extension CandidateJobDetailVC: AlertPopUpDelegate {
    
    func didTapAffirmativeButton() {
        self.reportJobService()
    }
    
    func didTapNegativeButton() {
        self.blurView.alpha = 0.0
        self.sharePopupView.alpha = 0.0
    }
}

//MARK: - GMSMAP Delegate
//===============================
extension CandidateJobDetailVC: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        
        let customInfoWindow = Bundle.main.loadNibNamed("MapInfoWindowView", owner: self, options: nil)![0] as! MapInfoWindowView
        
        customInfoWindow.frame.size = CGSize(width: 150, height: 80)
        customInfoWindow.addressLabel.textColor = AppColors.black46
        customInfoWindow.distanceLabel.textColor = AppColors.gray152
        
        customInfoWindow.addressLabel.text = self.candidateJobDetail?.companyName
        customInfoWindow.distanceLabel.text = "\(CommonClass.calculateDistanceBtw(from: self.candidateJobDetail?.jobLatitude ?? 0.0, long: self.candidateJobDetail?.jobLongitude ?? 0.0)) \(StringConstants.Miles_Away)"
        marker.infoWindowAnchor = CGPoint(x: 1.0, y: -0.1)
        return customInfoWindow
    }
    
}


extension CandidateJobDetailVC:  OnTapYesButton {
    
    func yesBtnTap() {
        
        let obj = YourProfileStep1VC.instantiate(fromAppStoryboard: .Recruiter)
        obj.job_id = self.jobId
        obj.recruiterId = self.recruiterId
        obj.categoryId = self.categoryId
        obj.fromTabbar = false
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    func editButtonTapped() {
        //        self.pop()
    }
}

extension CandidateJobDetailVC: ShouldOpenStatusPopup {
    
    func presentShortlistedPopup(jobApplyId: String, categoryId: String) {
        
        self.delegate?.presentShortlistedPopup(jobApplyId: jobApplyId, categoryId: categoryId)
        self.applyButton.backgroundColor = AppColors.white
        self.applyButton.borderColor = AppColors.appBlue
        self.applyButton.borderWidth = 1.0
        self.applyButton.isEnabled = false
        self.hideShowBookmarkBtn(status: true)
        self.applyButton.setTitleColor(AppColors.appBlue, for: .normal)
        self.applyButton.setTitle(StringConstants.Applied.uppercased(), for: .normal)
        if self.commingFromScreen == .employerDetail {
//            self.dismiss(animated: true, completion: nil)
            
        }else{
            
            if pushCount == 0{
//                self.pushCount -= 1
//                self.popToRootViewController()
//                self.navigationController?.popViewController(animated: true)
            }
          //  else{
//
//                guard let navVC = self.navigationController?.viewControllers else {return}
//
//                for vc in navVC{
//
//                    if vc is CandidateJobDetailVC{
//                        self.navigationController?.popViewController(animated: true)
//                    }
//                }
//            }
        }
    }
}

//MARK: -
//MARK: - Prototype Cell
class CandidateJobLocationCell: UITableViewCell {
    
    @IBOutlet weak var mapView: GMSMapView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
    }
}


class EmployerNameCell: UITableViewCell {
    
    @IBOutlet weak var employeeNameLabel: UILabel!
    @IBOutlet weak var nameButton: UIButton!
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.employeeNameLabel.textColor = AppColors.gray152
        self.nameLabel.textColor = AppColors.black46
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        
    }
    
    func populateEmployerName(_ data: JobListRecruiterDetail) {
        self.nameLabel.text = "\(data.recruiterName ) \(data.last_name)"
    }
}

class JobSalarycell: UITableViewCell {
    
    
    @IBOutlet weak var experianceLabel: UILabel!
    @IBOutlet weak var experianceRequiredLabel: UILabel!
    @IBOutlet weak var salaryRangeLabel: UILabel!
    @IBOutlet weak var salaryLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.salaryRangeLabel.textColor = AppColors.gray152
        self.experianceRequiredLabel.textColor = AppColors.gray152
        self.experianceLabel.textAlignment = .left
        self.experianceRequiredLabel.textAlignment = .left
        
        self.salaryRangeLabel.text = StringConstants.Salary_Range
        
    }
    
    func populateSalary(data: CandidateJobDetail) {
        
        if !data.duration.isEmpty {
            self.salaryLabel.text = "\(CommonClass.appendDollarInSalaryLabel(fromSalary: data.salaryFrom, toSalary: data.salaryTo)) (\(data.duration))"
        } else {
            self.salaryLabel.text = "\(CommonClass.appendDollarInSalaryLabel(fromSalary: data.salaryFrom, toSalary: data.salaryTo))"
            
        }
        
        self.experianceRequiredLabel.text = StringConstants.TipsCommessions
        if data.isCommision == 0 {
            self.experianceLabel.text = StringConstants.no.capitalized
        } else {
            self.experianceLabel.text = StringConstants.yes.capitalized
        }
    }
}

class SimilarJobCell: UITableViewCell {
    
    
    @IBOutlet weak var similarJobLabel: UILabel!
    @IBOutlet weak var similarJobCollectionView: UICollectionView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

class SimilarJobCVCell: UICollectionViewCell {
    
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var activityIndiCatView: UIView!
    @IBOutlet weak var jobNameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var jobImage: UIImageView!
    @IBOutlet weak var bgView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        activityIndicator.color = AppColors.appBlue
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.bgView.dropShadow(color: UIColor.black, opacity:0.2, offSet: CGSize(width: 1.0, height: 1.0), radius: 2.0)
    }
}

