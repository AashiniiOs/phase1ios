//
//  CandidateSearchFilterVC.swift
//  JobGet
//
//  Created by Admin on 27/08/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import CoreLocation
import GooglePlaces
import GoogleMaps
import GooglePlacePicker


protocol RecruiterFilterDelegate: class {
    
    func recruiterFilter(text: String, lat: String, long: String,categoryId: String, selectedCategory: [IndexPath], experienceType: Int?, totalExperience: String?,rawExp: Float?, sliderLabelText: String?, categoryName: String, radius: String)
}


protocol SearchCompelete: class {
    func getSearchedText(text: String, lat: String, long: String, radius: String, sort: Int, categoryID: String, selectedCatFilterIndex: [IndexPath], categoryName: String,isClear:Bool)
    
}

class CandidateSearchFilterVC: UIViewController {
    
    // MARK: - Outlets.
    // MARK: -
    
    @IBOutlet weak var searchTable: UITableView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    // MARK: - Properties.
    // MARK: -
    var filterArray = [StringConstants.Location, StringConstants.Categories, StringConstants.Sort_by, ""]
    var searchLocation: CLLocation?
    var address: String?
    weak var delegate: SearchCompelete?
    var selectedFilterIndex = [IndexPath]()
    var sortByFilter: SortByFilter = .newest
    var filterPopupSelectedIndex = 0
    var totalSelectedCategory = StringConstants.All
    var filteredCategoryId = ""
    var searchText = ""
    var experienceType: Int?
    var totalExperience: String?
    var rawExp: Float?
    var sliderLabelText: String?
    weak var recDelegate:RecruiterFilterDelegate?
    var currentAddress: String?
    var filterDic = [String: Any]()
    var city: String?
    var isOpenFromMap = false
    var radius = "20"
    var isClear = false
    // MARK: - View life cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if AppUserDefaults.value(forKey: .userType).stringValue == UserType.recuriter.rawValue{
            DispatchQueue.mainQueueAsync{
                self.searchLocation?.convertToPlaceMark({ (place) in
                    self.city = place.locality
                    self.address = "\(place.locality ?? ""), \(place.administrativeArea ?? "")"
                    self.searchTable.reloadData()
                })
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: Notification.Name.UIKeyboardWillHide, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: Notification.Name.UIKeyboardWillShow, object: nil)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.view.endEditing(true)
        self.searchTable.endEditing(true)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self)
    }
    
    
    // MARK: - Actions.
    // MARK: -
    
    @IBAction func applyFilterButtonTapp(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        if AppUserDefaults.value(forKey: .userType).stringValue == UserType.candidate.rawValue{
            self.candidateFilterData()
        }else{
            self.recruiterFilterData()
        }
        self.pop()
    }
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        
        self.pop()
        
    }
}


// MARK: - Private methods.
// MARK: -
extension CandidateSearchFilterVC{
    
    private func initialSetup(){
        let textFieldInsideUISearchBar = searchBar.value(forKey: "searchField") as? UITextField
        let placeholderLabel       = textFieldInsideUISearchBar?.value(forKey: "placeholderLabel") as? UILabel
        placeholderLabel?.font     = AppFonts.Poppins_Regular.withSize(12.0)
        textFieldInsideUISearchBar?.font = AppFonts.Poppins_Regular.withSize(12.0)
        textFieldInsideUISearchBar?.textColor = AppColors.black46
        if AppUserDefaults.value(forKey: .userType).stringValue == UserType.candidate.rawValue{
            
            placeholderLabel?.text = StringConstants.Candidate_Search_Placeholder_text
        }else{
            placeholderLabel?.text = StringConstants.Recruiter_Search_Placeholder_text
        }
        self.backBtn.isUserInteractionEnabled = true
        self.searchBar.delegate = self
        self.searchTable.delegate = self
        self.searchTable.dataSource = self
        self.searchBar.text = self.searchText
        self.navigationView.setShadow()
        
    }
    
    func candidateFilterData(){
        var sort = 0
        var radius = ""
        
        if self.sortByFilter == .nearBy{
            sort = 1
            radius = ""
        }else{
            sort = 2
            radius = self.radius
        }
        
        self.delegate?.getSearchedText(text: self.searchText, lat: "\(self.searchLocation?.coordinate.latitude ?? 42.36025580)", long: "\(self.searchLocation?.coordinate.longitude  ?? -71.05727910)", radius: radius, sort: sort, categoryID: self.filteredCategoryId, selectedCatFilterIndex: self.selectedFilterIndex, categoryName: self.totalSelectedCategory, isClear: isClear)
    }
    
    func recruiterFilterData(){
        
        if let tExperience = self.totalExperience, tExperience == "0"{
            self.totalExperience = ""
            self.experienceType = nil
        }
        self.recDelegate?.recruiterFilter(text: self.searchText, lat: "\(self.searchLocation?.coordinate.latitude ?? 0.0)", long: "\(self.searchLocation?.coordinate.longitude  ?? 0.0)", categoryId: self.filteredCategoryId, selectedCategory: self.selectedFilterIndex, experienceType: self.experienceType, totalExperience: self.totalExperience, rawExp: self.rawExp, sliderLabelText: self.sliderLabelText, categoryName: self.totalSelectedCategory, radius: self.radius )
    }
    
    @objc func keyboardWillAppear() {
        //Do something here
        print_debug("keyboardWillAppear")
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.removeKeyboard))
        self.searchTable.addGestureRecognizer(tap)
    }
    
    @objc func keyboardWillDisappear() {
        //Do something here
        print_debug("keyboardWillDisappear")
        if let gesture = searchTable.gestureRecognizers{
            for recognizer: UIGestureRecognizer in gesture {
                searchTable.removeGestureRecognizer(recognizer)
            }
        }
    }
    
    @objc func removeKeyboard(){
        
        self.view.endEditing(true)
    }
    
    @objc func clearButtonTapp(_ sender: UIButton){
        isClear = true
        if AppUserDefaults.value(forKey: .userType).stringValue == UserType.candidate.rawValue{
            self.sortByFilter = .newest
            self.totalSelectedCategory = StringConstants.All
        }else{
            self.totalSelectedCategory = StringConstants.All
            self.experienceType = nil
            self.totalExperience = nil
            self.rawExp = nil
            self.sliderLabelText = nil
        }
        self.searchBar.text = ""
        self.searchText = ""
        self.radius = "20"
        if !self.isOpenFromMap{
            if let data = AppUserDefaults.value(forKey: .userData).dictionary, let lat = data["lat"]?.double,let lng = data["lng"]?.double, let city = data["city"]?.string, let state = data["state"]?.string{
                if lat != 0.0{
                    self.searchLocation = CLLocation(latitude: lat, longitude: lng)
                    self.address = city + ", " + state
                }
            
            }
            else{
//                if let currentLocation = self.filterDic["currentLocation"]  as? CLLocation{
//                    self.searchLocation = currentLocation
//                }
                 let currentLoc = CLLocation(latitude:AppUserDefaults.value(forKey: .userLat).doubleValue , longitude: AppUserDefaults.value(forKey: .userLong).doubleValue)
                    self.searchLocation = currentLoc
                self.address = self.currentAddress
            }
        }
        self.selectedFilterIndex.removeAll(keepingCapacity: false)
        self.filteredCategoryId = ""
        AppUserDefaults.removeValue(forKey: .filterLong)
        AppUserDefaults.removeValue(forKey: .filterLat)

        self.searchTable.reloadData()
    }
}

// MARK: - Table view delegate and datasource methods.
// MARK: -
extension CandidateSearchFilterVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.filterArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 3{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CandidateClearFilterBtnCell", for: indexPath) as? CandidateClearFilterBtnCell else {
                fatalError("not found.")
            }
            cell.clearButton.addTarget(self, action: #selector(self.clearButtonTapp(_:)), for: .touchUpInside)
            return cell
        }else{
            if AppUserDefaults.value(forKey: .userType).stringValue == UserType.candidate.rawValue{          
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "CandidateSearchFilterCell", for: indexPath) as? CandidateSearchFilterCell else {
                    fatalError("not found.")
                }
                cell.titleLabel.text = self.filterArray[indexPath.row]
                switch indexPath.row {
                case 0:
                    if let _ = self.address{}else{
                        
                        self.address = "Boston, MA"
                    }
                    cell.subTitleLabel.text = self.address ?? self.currentAddress
                    
                case 1:
                    if  self.totalSelectedCategory.isEmpty{
                        cell.subTitleLabel.text =  StringConstants.All
                    }else{
                        cell.subTitleLabel.text = self.totalSelectedCategory
                    }
                    
                default:
                    if self.sortByFilter == .newest{
                        cell.subTitleLabel.text =  StringConstants.Newest
                    }else{
                        cell.subTitleLabel.text =  StringConstants.Nearby
                    }
                }
                return cell
                
            }else{
                if indexPath.row == 2{
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: "FilterExperienceCell", for: indexPath) as? FilterExperienceCell else{fatalError("FilterExperienceCell not found.")}
                    cell.delegate = self
                    
                    if let currentVal = self.rawExp {
                        
                        cell.sliderContainerView.setValue(currentVal, animated: false)
                    }else{
                        cell.sliderContainerView.setValue(0, animated: false)
                    }
                    
                    if let sliderText = self.sliderLabelText {
                        cell.sliderLabel.text = sliderText
                    }else{
                        cell.sliderLabel.text = StringConstants.Any_Experienc
                    }
                    
                    return cell
                }else{
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: "CandidateSearchFilterCell", for: indexPath) as? CandidateSearchFilterCell else {
                        fatalError("not found.")
                    }
                    cell.titleLabel.text = self.filterArray[indexPath.row]
                    switch indexPath.row {
                    case 0:
                        if  let _ = self.address{}else{
                            
                            self.address = "Boston, MA"
                        }
                        cell.subTitleLabel.text = self.address ?? self.currentAddress
                        
                    default:
                        if  self.totalSelectedCategory.isEmpty{
                            cell.subTitleLabel.text = StringConstants.All
                        }else{
                            cell.subTitleLabel.text = self.totalSelectedCategory
                        }
                    }
                    return cell
                }
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if AppUserDefaults.value(forKey: .userType).stringValue == UserType.candidate.rawValue{
            
            if self.isOpenFromMap{
                if indexPath.row == 0 || indexPath.row == 2{
                    return 0
                }else{
                    return 50
                }
            }else{
                if indexPath.row == 2 {
                    return 0
                }else{
                    return 50
                }
            }
        }else{
            if indexPath.row == 2{
                return 185
            }else{
                return 50
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let placePicker = SearchByMapVC.instantiate(fromAppStoryboard: .settingsAndChat)
            
            placePicker.delegate = self
            placePicker.city = self.city ?? ""
            placePicker.currentLocation = self.searchLocation
            present(placePicker, animated: true, completion: nil)
        case 1:
            
            let filterScene = FilterVC.instantiate(fromAppStoryboard: .Candidate)
            filterScene.delegate = self
            filterScene.selectedFilter = selectedFilterIndex
            
            self.present(filterScene, animated: true, completion: nil)
            
            //        case 2:
            //            break
            //            if AppUserDefaults.value(forKey: .userType).stringValue == UserType.candidate.rawValue{
            //                let popUpVc = SortPopUpVC.instantiate(fromAppStoryboard: .Candidate)
            //                popUpVc.delegate = self
            //                popUpVc.modalPresentationStyle = .overCurrentContext
            //                popUpVc.modalTransitionStyle = .crossDissolve
            //                if self.sortByFilter == .newest{
            //                    self.filterPopupSelectedIndex = 0
            //                }else{
            //                    self.filterPopupSelectedIndex = 1
            //                }
            //                popUpVc.selectedIndex = self.filterPopupSelectedIndex
            //                self.present(popUpVc, animated: true, completion: nil)
        //            }
        default:
            break
        }
    }
}

extension CandidateSearchFilterVC: FilterSliderVauleDelegate {
    
    func filterSliderValue(experienceType: Int?, totalExperience: String?,rawExp: Float?, sliderLabelText: String?) {
        self.experienceType = experienceType
        self.totalExperience = totalExperience
        self.rawExp = rawExp
        self.sliderLabelText = sliderLabelText
    }
}


//MARK:- Search bar delgate methods.
//===========================================
extension CandidateSearchFilterVC: UISearchBarDelegate{
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        self.searchText = searchBar.text ?? ""
    }
}

//MARK:- Sortpoupvc delgate
//===========================================
extension CandidateSearchFilterVC: SortPopUpDelegate {
    
    func didApplyFilter(selecteIndex: Int) {
        self.filterPopupSelectedIndex = selecteIndex
        
        if selecteIndex == 0 {
            self.sortByFilter = .newest
        } else if selecteIndex == 1 {
            self.sortByFilter = .nearBy
        }
        self.searchTable.reloadRows(at: [IndexPath(row: 2, section:0)], with: .none)
    }
}

//MARK: - Get Filtered Category ID
//===================================

// get selected filter category Id for filteration
extension CandidateSearchFilterVC: GetCategoryID {
    func onTapAppliedButton(selectedCategoryId: Array<String>, selectedCategoryName: String, selectedIndex: Array<IndexPath>, isAllSelected: Bool) {
        
        if isAllSelected{
            self.totalSelectedCategory = StringConstants.All
        }else{
            self.totalSelectedCategory = selectedCategoryName
        }
        
        self.filteredCategoryId = selectedCategoryId.joined(separator: ",")
        self.selectedFilterIndex = selectedIndex
        self.searchTable.reloadRows(at: [IndexPath(row: 1, section:0)], with: .none)
        
    }
}

//MARK: - Job Search by location delegate method.
//MARK: -
extension CandidateSearchFilterVC: CandidateSeacrhByLocationDelegate{
    
    func candidateSeacrhByLocationDelegate(coordinates: CLLocation?, radius: String?, address: String?) {
        self.address = address
        self.searchLocation = coordinates
        print_debug(address)
        self.radius = radius ?? "20"
        self.searchTable.reloadRows(at: [IndexPath(row: 0, section:0)], with: .none)
    }
}


// MARK: - Table view cell class.
// MARK: -
class CandidateSearchFilterCell: UITableViewCell{
    
    
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

class CandidateClearFilterBtnCell: UITableViewCell{
    
    
    @IBOutlet weak var clearButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
