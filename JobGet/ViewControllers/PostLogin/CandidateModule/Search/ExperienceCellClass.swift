//
//  RecruiterFilterVC+CellClass.swift
//  JobGet
//
//  Created by Appinventiv on 23/05/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import CircularSlider

protocol FilterSliderVauleDelegate: class{
    
    func filterSliderValue(experienceType: Int?, totalExperience: String?, rawExp: Float?, sliderLabelText: String?)
}

protocol SetSliderInitialValue:class {
    
    func initialSlider(currentValue: Int, upperCurrentValue: Int)
}

class FilterCategoryCell: UITableViewCell{
    
    @IBOutlet weak var selectAllBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.selectAllBtn.borderColor = AppColors.themeBlueColor
        self.selectAllBtn.borderWidth = 1.0
        self.selectAllBtn.setTitleColor(AppColors.themeBlueColor, for: .normal)
        self.selectAllBtn.setCorner(cornerRadius: 5.0, clip: true)
    }
}

class FilterExperienceCell: UITableViewCell{
    
    //var circularSlider: DoubleHandleCircularSlider!
    var experienceType : Int?
    var totalExperience: String?
    
    //MARK:- IB Outlets
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var sliderContainerView: CircularSlider!
    @IBOutlet weak var sliderLabel: UILabel!
    weak var delegate: FilterSliderVauleDelegate?
    
    var isResetExperience = false
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setUpSlider()
        
        self.sliderLabel.text = StringConstants.Any_Experienc
    }
    
    
    func setUpSlider(){
        sliderContainerView.delegate = self
    }
    
    func resetExperience() {
        
        sliderContainerView.setValue(0, animated: false)
        
    }
    
}


// MARK: - CircularSliderDelegate
extension FilterExperienceCell: CircularSliderDelegate {
    func circularSlider(_ circularSlider: CircularSlider, valueForValue value: Float) -> Float {
        
        if value < 180 {
            
            let lowerValue = Int(CommonClass.getExperience(pos: value))
            if lowerValue < 1 {
                self.sliderLabel.text = StringConstants.Any_Experienc
            } else {
                self.sliderLabel.text = "\(StringConstants.Min) \(lowerValue) \(StringConstants.Months)"
            }
            self.experienceType = 1
            self.totalExperience = "\(lowerValue)"
        } else {
            
            let val = "\(CommonClass.getExperience(pos: value))".replacingOccurrences(of: ".0", with: "")
            self.experienceType = 2
            if CommonClass.getExperience(pos: value) > 5 {
                self.sliderLabel.text  = "\(StringConstants.Min) 5+ \(StringConstants.Years)"
                self.totalExperience = "\(val)+"
                
                
            } else if CommonClass.getExperience(pos: value) == 1 {
                self.sliderLabel.text = "\(StringConstants.Min) \(val) \(StringConstants.Year)"
                self.totalExperience = "\(val)"
            }  else {
                self.sliderLabel.text = "\(StringConstants.Min) \(val) \(StringConstants.Years)"
                self.totalExperience = "\(val)"
            }
            
        }
        self.delegate?.filterSliderValue(experienceType: self.experienceType, totalExperience: self.totalExperience, rawExp: value, sliderLabelText: self.sliderLabel.text)
        
        print(value)
        return floorf(value)
    }
}


class FilterCategoryCollectionViewCell: UICollectionViewCell{
    
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var categoryImage: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.borderColor = AppColors.gray152
        self.layer.cornerRadius = frame.size.height / 2
        clipsToBounds = true
        self.borderWidth = 1.0
    }
}

