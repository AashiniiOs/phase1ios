//
//  FilterVC.swift
//  JobGet
//
//  Created by appinventiv on 17/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

protocol GetCategoryID: class {
    func onTapAppliedButton(selectedCategoryId: Array<String>,selectedCategoryName: String, selectedIndex: Array<IndexPath>, isAllSelected: Bool)
}

protocol  GetSingleCategory: class {
    func getCategoryForProfile(selectedCategoryID: String, selectedCategoryName: String, selectedIndex: IndexPath)
}

import UIKit
import ViewAnimator

class FilterVC: BaseVC {
    
    //    MARK:- Properties
    //    ==========================================
    
    var selectedFilter: [IndexPath] = []
    var jobCategoryData = [JobCategory]()
    weak var delegate: GetCategoryID?
    weak var singleCategoryDelegate: GetSingleCategory?
    var isfromAddExperiance = false
    var selectedCategoryID = ""
    var selectedCategoryName = ""
    var selectedIndex: IndexPath?
    var selectedID = ""
    
    
    //    MARK:- IBOutlets
    //    ==========================================
    @IBOutlet weak var bgShadowView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var screenTitle: UILabel!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var applyButton: UIButton!
    @IBOutlet weak var filterTable: UITableView!
    @IBOutlet weak var navigationView: UIView!
    
    //    MARK:- View life cycle
    //    ==========================================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        
    }
    
    //    MARK:- Functions
    //    ==========================================
    
    private func initialSetup(){
        
        self.filterTable.dataSource = self
        self.filterTable.delegate = self
        self.getCategoryList(loader: true)
        self.filterTable.setCorner(cornerRadius: 5, clip: true)
        self.bgShadowView.setShadow()
        self.navigationView.setShadow()
        if isfromAddExperiance {
            self.filterTable.allowsMultipleSelection = false
        }
        self.applyButton.setTitle(StringConstants.Apply.uppercased(), for: .normal)
        CommonClass.setNextButton(button: self.applyButton)
        
        _ = self.jobCategoryData.flatMap { (category) -> String in
            category.selectedCategoryImage
        }
    }
    
    //    MARK:- IBAction
    //    ==========================================
    
    @IBAction func applyButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        if isfromAddExperiance {
            
            if let selectedIndex = self.selectedIndex {
                
                self.singleCategoryDelegate?.getCategoryForProfile(selectedCategoryID: self.selectedCategoryID, selectedCategoryName: self.selectedCategoryName, selectedIndex: selectedIndex)
            }
        } else {
            var selectedCategoryId = [String]()
            var selectedCategoryName = [String]()
            let _ = self.selectedFilter.filter { (index) -> Bool in
                
                selectedCategoryId.append(self.jobCategoryData[index.row].categoryId)
                selectedCategoryName.append(self.jobCategoryData[index.row].categoryTitle)
                return true
            }
            let catagoryName = selectedCategoryName.joined(separator: ",")
            
            var isAllselected = false
            if self.selectedFilter.count == self.jobCategoryData.count{
                isAllselected = true
            }
            delegate?.onTapAppliedButton(selectedCategoryId: selectedCategoryId, selectedCategoryName: catagoryName, selectedIndex: self.selectedFilter, isAllSelected: isAllselected)
            
        }
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func resetFilterButtonTapped(_ sender: UIButton) {
        self.selectedFilter.removeAll()
        self.filterTable.reloadData()
    }
    
    @objc func selectAllBtnTapped(_ sender: UIButton) {
        
        self.selectedFilter.removeAll(keepingCapacity: false)
        
        if self.jobCategoryData.isEmpty{
            return
        }
        for i in  (1...self.jobCategoryData.count) {
            self.filterTable.selectRow(at: IndexPath(row: i, section: 1 ), animated: true, scrollPosition: .none)
            self.selectedFilter.append(IndexPath(row: i-1, section: 1))
            
        }
        
        self.filterTable.reloadData()
    }
}

//MARK:- Webservice Methods
//=========================
extension FilterVC {
    
    func getCategoryList(loader: Bool){
        let param = ["": ""]
        WebServices.getCategoryList(parameters: param, loader: loader, success: { (json) in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                
                let data = json["data"].arrayValue
                
                self.jobCategoryData.removeAll(keepingCapacity: false)
                for categoryData in data {
                    self.jobCategoryData.append(JobCategory(dict: categoryData))
                }
                
                self.filterTable.reloadData()
                
                UIView.animate(views: self.filterTable.visibleCells, animations: [AnimationType.from(direction: .top, offset: 30.0)], completion: nil)
                
            }
            
        }) { (error) in
            print_debug(error)
        }
    }
}

//    MARK:- Tableview datasource
//    ==========================================
extension FilterVC: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        } else {
            return self.jobCategoryData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "FilterCell", for: indexPath) as! FilterCell
        
        if indexPath.section == 0 {
            if isfromAddExperiance {
                cell.categoryLabelContainer.isHidden = false
                cell.filteTextSeperatorView.isHidden = true
                cell.selectAllButton.isHidden = true
            } else {
                cell.categoryLabelContainer.isHidden = false
                cell.selectAllButton.addTarget(self, action: #selector(self.selectAllBtnTapped(_:)), for: .touchUpInside)
                cell.filteTextSeperatorView.isHidden = true
            }
            
        } else {
            
            cell.filterText.text = self.jobCategoryData[indexPath.row].categoryTitle
            if self.selectedID == self.jobCategoryData[indexPath.row].categoryId{
                tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
                if CommonClass.catagoryList(self.jobCategoryData[indexPath.row].categoryTitle).isEmpty{
                    cell.filterItemIcon.sd_setImage(with: URL(string: self.jobCategoryData[indexPath.row].selectedCategoryImage), placeholderImage:#imageLiteral(resourceName: "icHomeCategoryPlaceholder1"))
                }else{
                    cell.filterItemIcon.image = CommonClass.commonCategoryList(self.jobCategoryData[indexPath.row].categoryTitle, isSelected: true)
                }
            }
            if CommonClass.catagoryList(self.jobCategoryData[indexPath.row].categoryTitle).isEmpty{
                cell.filterItemIcon.sd_setImage(with: URL(string: self.jobCategoryData[indexPath.row].categoryImage),  placeholderImage: #imageLiteral(resourceName: "icHomeCategoryPlaceholder1"), completed: nil)
                
            }else{
                cell.filterItemIcon.image = CommonClass.commonCategoryList(self.jobCategoryData[indexPath.row].categoryTitle, isSelected: false)
            }
            
            
            cell.filteTextSeperatorView.isHidden = false
        }
        
        if selectedFilter.contains(indexPath) {
            self.selectedID = ""
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
            if CommonClass.catagoryList(self.jobCategoryData[indexPath.row].categoryTitle).isEmpty{
                cell.filterItemIcon.sd_setImage(with: URL(string: self.jobCategoryData[indexPath.row].selectedCategoryImage), placeholderImage:#imageLiteral(resourceName: "icHomeCategoryPlaceholder1"))
            }else{
                cell.filterItemIcon.image = CommonClass.commonCategoryList(self.jobCategoryData[indexPath.row].categoryTitle, isSelected: true)
            }
        }
        
        return cell
    }
}

//    MARK:- Tableview delegate
//    ==========================================
extension FilterVC: UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let cell = tableView.cellForRow(at: indexPath) as? FilterCell else { return }
        
        if CommonClass.catagoryList(self.jobCategoryData[indexPath.row].categoryTitle).isEmpty{
            cell.filterItemIcon.sd_setImage(with: URL(string: self.jobCategoryData[indexPath.row].selectedCategoryImage),  placeholderImage:  #imageLiteral(resourceName: "icHomeCategoryPlaceholder1"), completed: nil)
            
        }else{
            cell.filterItemIcon.image = CommonClass.commonCategoryList(self.jobCategoryData[indexPath.row].categoryTitle, isSelected: true)
        }
        
        
        if isfromAddExperiance {
            self.selectedCategoryID = self.jobCategoryData[indexPath.row].categoryId
            self.selectedCategoryName = self.jobCategoryData[indexPath.row].categoryTitle
            self.selectedIndex = indexPath
            
        } else {
            self.selectedFilter.append(indexPath)
        }
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        guard let cell = tableView.cellForRow(at: indexPath) as? FilterCell else { return }
        if CommonClass.catagoryList(self.jobCategoryData[indexPath.row].categoryTitle).isEmpty{
            cell.filterItemIcon.sd_setImage(with: URL(string: self.jobCategoryData[indexPath.row].categoryImage),  placeholderImage: #imageLiteral(resourceName: "icHomeCategoryPlaceholder1"), completed: nil)
            
        }else{
            cell.filterItemIcon.image = CommonClass.commonCategoryList(self.jobCategoryData[indexPath.row].categoryTitle, isSelected: false)
        }
        
        self.selectedFilter.removeObject(indexPath)
        
    }
}

extension FilterVC: IsCategoryAdded {
    func reloadData() {
        self.getCategoryList(loader: false)
    }
}

//    MARK:- FilterCell
//    ==========================================
class FilterCell: UITableViewCell{
    
    //    MARK:- IBOutlets
    //    ==========================================
    @IBOutlet weak var filterItemIcon: UIImageView!
    @IBOutlet weak var filterText: UILabel!
    @IBOutlet weak var checkBox: UIImageView!
    @IBOutlet weak var categoryLabelContainer: UIView!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var selectAllButton: UIButton!
    @IBOutlet weak var filteTextSeperatorView: UIView!
    
    //    MARK:- life cycle
    //    ==========================================
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.categoryLabel.textColor = AppColors.black46
        self.resetCell()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        
        self.filterItemIcon.setCorner(cornerRadius: self.filterItemIcon.height/2, clip: true)
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.resetCell()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if selected {
            
            self.checkBox.image = #imageLiteral(resourceName: "ic_check_box_").withRenderingMode(.alwaysTemplate)
            self.checkBox.tintColor = AppColors.themeBlueColor
            self.filterText.textColor = AppColors.themeBlueColor
            self.filterText.font = AppFonts.Poppins_Medium.withSize(16)
            
        }else {
            
            self.checkBox.image = #imageLiteral(resourceName: "ic_check_box_outline_blank_").withRenderingMode(.alwaysTemplate)
            self.checkBox.tintColor = AppColors.appGrey
            self.filterText.textColor = AppColors.black46
            self.filterText.font = AppFonts.Poppins_Light.withSize(16)
        }
        
    }
    
    private func resetCell(){
        self.categoryLabelContainer.isHidden = true
    }
}
