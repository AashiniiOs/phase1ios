//
//  JobListMapClusterVC.swift
//  JobGet
//
//  Created by Manish Nainwal on 11/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

import ViewAnimator
import DZNEmptyDataSet




class JobListMapClusterVC: BaseVC {
    
    //MARK:- Enum
    //==================
    
    
    //MARK:- Properties
    //==================
    
    var jobList = [CandidateJobList]()
    var filteredCategoryId: String?
    var job_id: String?
    var recruiterId: String?
    var companyLatitude: String?
    var companyLongtitude: String?
    var categoryId: String?
    var currentLocation:CLLocation?
    
    //MARK:- IBoutlets
    //================
    
    
    @IBOutlet weak var jobListTableView: UITableView!
    
    //MARK:- Variables and Constants
    //==============================
    
    
    
    //MARK:- LifeCycle Methods
    //========================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        print_debug("memory issue")
    }
    
    //MARK:- Private Methods
    //======================
    
    private func initialSetup() {
        
        self.jobListTableView.delegate = self
        self.jobListTableView.dataSource = self
        self.jobListTableView.emptyDataSetSource = self
        self.jobListTableView.emptyDataSetDelegate = self
        
        self.registerCells()
        
    }
    
    private func registerCells() {
        self.jobListTableView.register(UINib(nibName: "JobListCell", bundle: nil), forCellReuseIdentifier: "JobListCell")
        
    }
    
    //MARK:- Target Actions
    //=====================
    @objc func shareJobButtonTapped(_ sender: UIButton) {
        
        guard let idx = sender.tableViewIndexPath(self.jobListTableView) else { return }
        self.displayShareSheet(shareContent: self.jobList[idx.row].jobShareUrl)
    }
    
    @objc func applyButtonTapped(_ sender: UIButton) {
        
        guard let index = sender.tableViewIndexPath(self.jobListTableView) else { return }
        self.job_id = self.jobList[index.row].jobId
        self.categoryId = self.jobList[index.row].categoryId
        if let recruiterDetail = self.jobList[index.row].recruiterDetail {
            self.recruiterId = recruiterDetail.recruiterId
            print_debug(recruiterDetail.recruiterId)
            if let jobid = job_id{
                self.getCandidateJobDetail(loader: true, job_id: jobid, recruiterId: recruiterDetail.recruiterId)
            }
            
            
            self.jobList[index.row].isApplied = 1
        }
        
    }
    
    //MARK:- IBActions
    //================
    
}




//MARK:- Web Services
//====================
extension JobListMapClusterVC {
    
    
    func getCandidateJobDetail(loader: Bool, job_id: String, recruiterId: String) {
        
        let value = AppUserDefaults.value(forKey: .userData)
        print_debug(value["is_profile_added"])
        print_debug(value["isExperience"])
        if value["isExperience"] == 0 {
            let sceen = ProfileErrorPopUpVC.instantiate(fromAppStoryboard: .Main)
            sceen.delegate = self
            self.view.addSubview(sceen.view)
            self.add(childViewController: sceen)
            
        }else{
            
            candidateAppliedJob(job_id: job_id, recruiterId: recruiterId)
        }
    }
    
    func candidateAppliedJob(job_id: String, recruiterId: String) {
        
        let params = [ApiKeys.userId.rawValue: AppUserDefaults.value(forKey: .userId), ApiKeys.jobId.rawValue: job_id, ApiKeys.recruiterId.rawValue: recruiterId, "categoryId": self.categoryId ] as [String : Any]
        print_debug(params)
        WebServices.applyJob(parameters: params, success: { (json) in
            if let errorCode = json[ApiKeys.code.rawValue].int, errorCode == error_codes.success {
                let sceen = ResetPasswordPopupVC.instantiate(fromAppStoryboard: .Main)
                commingFrom = .jobList
                sceen.jobId = job_id
                sceen.delegate = self
                self.view.addSubview(sceen.view)
                self.add(childViewController: sceen)
                NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue: "Refresh_Saved_Job")))
            } else {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
            
        }) { (error) in
            print_debug(error)
            CommonClass.showToast(msg: error.localizedDescription)
            
        }
    }
}

//MARK:- Tableview delegate and datasource
//===========================================
extension JobListMapClusterVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.jobList.count
        // return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "JobListCell") as! JobListCell
        cell.populateCandidateJobDetail(self.jobList[indexPath.row])
        
        cell.shareButton.addTarget(self, action: #selector(self.shareJobButtonTapped(_:)), for: .touchUpInside)
        
        cell.applyButton.addTarget(self, action: #selector(self.applyButtonTapped(_:)), for: .touchUpInside)
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let sceen = CandidateJobDetailVC.instantiate(fromAppStoryboard: .Candidate)
        sceen.jobId = self.jobList[indexPath.row].jobId
        sceen.isSaved   =  self.jobList[indexPath.row].isSaved
        sceen.distance = self.jobList[indexPath.row].distance
        self.jobList[indexPath.row].totalJobView = self.jobList[indexPath.row].totalJobView + 1
        tableView.reloadRows(at: [indexPath], with: .none)
        if let category_id = self.categoryId{
            sceen.categoryId = category_id
        }
        sceen.currentLocation = self.currentLocation
        sceen.delegate = self
        sceen.jobDetail = self.jobList[indexPath.row]
        
        sceen.commingFromScreen = .jobListCell
        if let detail = self.jobList[indexPath.row].recruiterDetail {
            sceen.recruiterDetail = detail
            sceen.recruiterId = detail.recruiterId
        }
        self.navigationController?.pushViewController(sceen, animated: true)
    }
}

//MARK: - Ontap Yes Button
//=========================
extension JobListMapClusterVC:  OnTapYesButton {
    
    func yesBtnTap() {
        let obj = YourProfileStep1VC.instantiate(fromAppStoryboard: .Recruiter)
        
        if let category_id = self.categoryId, let jobid = self.job_id,let recruiter_Id = self.recruiterId{
            obj.job_id = jobid
            obj.recruiterId = recruiter_Id
            obj.categoryId = category_id
        }
        
        obj.fromTabbar = false
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    func editButtonTapped() {
        //        self.pop()
    }
}


extension JobListMapClusterVC: ShouldOpenStatusPopup {
    
    func presentShortlistedPopup(jobApplyId: String,  categoryId: String) {
        
        guard let index = self.jobList.index(where: { (model) -> Bool in
            
            return model.jobId == jobApplyId
        })else{return}
        
        let indexRow = self.jobList.startIndex.distance(to: index)
        
        self.jobList[indexRow].isApplied = 1
        
        self.jobListTableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
    }
}

//MARK:- DZNEmptyDataSource and Delegate
//==========================================
extension JobListMapClusterVC: DZNEmptyDataSetDelegate, DZNEmptyDataSetSource {
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return #imageLiteral(resourceName: "icHomeNoJobs")
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: StringConstants.No_Jobs_Found,                                                                           attributes: [NSAttributedStringKey.foregroundColor: AppColors.black46,
                                                                                                                                                                NSAttributedStringKey.font: AppFonts.Poppins_Medium.withSize(14)
            ])
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        return NSAttributedString(string: "",                                                                           attributes: [NSAttributedStringKey.foregroundColor: AppColors.gray152,
                                                                                                                                     NSAttributedStringKey.font: AppFonts.Poppins_Regular.withSize(12)                                                                                                                                                                                                            ])
    }
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}

