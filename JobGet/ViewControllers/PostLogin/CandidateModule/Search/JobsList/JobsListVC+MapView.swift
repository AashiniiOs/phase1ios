//
//  JobsListVC+MapView.swift
//  JobGet
//
//  Created by Admin on 03/10/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import GoogleMaps

extension JobsListVC{
    
    func clustering(){
        // Set up the cluster manager with the supplied icon generator and
        // renderer.
        let iconGenerator = GMUDefaultClusterIconGenerator()
        let algorithm = GMUNonHierarchicalDistanceBasedAlgorithm()
        let renderer = GMUDefaultClusterRenderer(mapView: mapView,
                                                 clusterIconGenerator: iconGenerator)
        renderer.delegate = self
        clusterManager = GMUClusterManager(map: mapView, algorithm: algorithm,
                                           renderer: renderer)
        
    }
    
    /// Randomly generates cluster items within some extent of the camera and
    /// adds them to the cluster manager.
     func generateClusterItems(kClusterItem: [CandidateJobList]) {
        if kClusterItem.isEmpty{
            return
        }
        self.mapView.clear()
        for index in 1...kClusterItem.count {
            let lat = kClusterItem[index-1].latitude
            let lng = kClusterItem[index-1].longitude
            let name = "Item \(index)"
            let item = POIItem(position: CLLocationCoordinate2DMake(lat, lng), name: name, index: index)
            clusterManager.add(item)
        }
    }
    
    
    /// Returns a random value between -1.0 and 1.0.
    private func randomScale() -> Double {
        return Double(arc4random()) / Double(UINT32_MAX) * 2.0 - 1.0
    }
    
    @objc func refreshAfterReportUser(){
        self.isSameIndex = true
        
        self.getJobList(filterID: self.filteredCategoryId, searchText: "", sortByFilter: self.sortByFilter.rawValue, pageNo: 0, primpage: primpage, loader: false, lat: self.companyLatitude, long: companyLongtitude)
        
    }
    func setupMapInfoWindow() {
        self.adView.isHidden = true
        self.addViewWidth.constant = 0
        self.addViewTrailingConst.constant = 0
        self.infoWindowCompanyNameLabel.textColor = #colorLiteral(red: 0.1803921569, green: 0.1803921569, blue: 0.1803921569, alpha: 0.75)
        self.infoWindowSalaryLabel.textColor = AppColors.gray152
        self.infoWindowExperianceLabel.textColor = AppColors.black46
        self.inFoWindowJobNameLabel.textColor = AppColors.black46
        self.infoWindowApplyButton.setCorner(cornerRadius: 5.0, clip: true)
        self.infoWindowApplyButton.backgroundColor = AppColors.themeBlueColor
        let cllocation = CLLocationCoordinate2D(latitude: Double(StringConstants.Boston_Lat)!, longitude: Double(StringConstants.Boston_Long)!)
        self.mapView.camera = GMSCameraPosition.camera(withTarget: cllocation, zoom: currentZoom)
    }
    
    func populateDataOnMapInfoWindow(data: CandidateJobList) {
        
        self.infoWindowHeaderImage.sd_setImage(with: URL(string: data.jobImage),  placeholderImage: #imageLiteral(resourceName: "icHomeCategoryPlaceholder"), completed: nil)
        self.inFoWindowJobNameLabel.text = data.jobTitle
        self.infoWindowJobCategoryLabel.text = data.categoryName
        if data.isPremium{
            self.adView.isHidden = false
            self.addViewWidth.constant = 26
            self.addViewTrailingConst.constant = 4
        }else{
            self.adView.isHidden = true
            self.addViewWidth.constant = 0
            self.addViewTrailingConst.constant = 0
            
        }
        if data.totalExperience.isEmpty || data.isExp == 0 || data.experienceType == 0{
            self.infoWindowExperianceLabel.text       = StringConstants.NoExpRequired
        } else {
            if data.experienceType == 1 {
                if data.totalExperience == "1" {
                    self.infoWindowExperianceLabel.text       = "\(StringConstants.Min) \(data.totalExperience) \(StringConstants.Month_experience)"
                } else {
                    self.infoWindowExperianceLabel.text       = "\(StringConstants.Min) \(data.totalExperience) \(StringConstants.Months_experience)"
                }
                
            } else {
                if data.totalExperience == "1" {
                    self.infoWindowExperianceLabel.text       = "\(StringConstants.Min) \(data.totalExperience) \(StringConstants.Year_experience)"
                }else {
                    self.infoWindowExperianceLabel.text       = "\(StringConstants.Min) \(data.totalExperience) \(StringConstants.Years_experience)"
                }
                
            }
        }
        var time = ""
        if let timeDate = data.createdAt {
            time = timeDate.elapsedTime
        }
        self.infoWindowJobTimeLabel.text = time
        if !data.duration.isEmpty {
            
            self.infoWindowSalaryLabel.text       = "\(CommonClass.appendDollarInSalaryLabel(fromSalary: data.salaryFrom, toSalary: data.salaryTo)) (\(data.duration))"
        }else{
            self.infoWindowSalaryLabel.text = CommonClass.appendDollarInSalaryLabel(fromSalary: data.salaryFrom, toSalary: data.salaryTo)
        }
        
        self.infoWindowCompanyNameLabel.text = "\(String(data.companyName))"
        
        if data.isShortlisted{
            
            self.infoWindowApplyButton.setTitleColor(AppColors.white, for: .normal)
            self.infoWindowApplyButton.backgroundColor = AppColors.white
            self.infoWindowApplyButton.borderColor = AppColors.themeBlueColor
            self.infoWindowApplyButton.setTitleColor(AppColors.themeBlueColor, for: .normal)
            self.infoWindowApplyButton.borderWidth = 1.0
            self.infoWindowApplyButton.isEnabled = false
            self.infoWindowApplyButton.setTitle(StringConstants.Shortlisted.uppercased(), for: .normal)
        }else if !data.isShortlisted && data.isApplied == 1{
            
            self.infoWindowApplyButton.setTitleColor(AppColors.white, for: .normal)
            self.infoWindowApplyButton.backgroundColor = AppColors.white
            self.infoWindowApplyButton.borderColor = AppColors.themeBlueColor
            self.infoWindowApplyButton.setTitleColor(AppColors.themeBlueColor, for: .normal)
            self.infoWindowApplyButton.borderWidth = 1.0
            self.infoWindowApplyButton.isEnabled = false
            self.infoWindowApplyButton.setTitle(StringConstants.Applied.uppercased(), for: .normal)
        } else {
            self.infoWindowApplyButton.setTitle(StringConstants.Apply.uppercased(), for: .normal)
            self.infoWindowApplyButton.backgroundColor = AppColors.themeBlueColor
            self.infoWindowApplyButton.borderColor = AppColors.themeBlueColor
            self.infoWindowApplyButton.isEnabled = true
            self.infoWindowApplyButton.borderWidth = 1.0
            self.infoWindowApplyButton.setTitleColor(AppColors.white, for: .normal)
        }
        self.infoWindowJobSeenBtn.setTitle(String(data.totalJobView), for: .normal)
    }
    
     func setMarkerToMapView(camera: GMSCameraPosition?, zoom: Float, selectedLocation: CLLocation?) {
        
        if let location = selectedLocation {
            let coordinate = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            print_debug(coordinate)
            self.mapView.camera = GMSCameraPosition.camera(withTarget: coordinate, zoom: zoom)
            self.marker.position = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            self.mapView.animate(toLocation: coordinate)
        } else {
            if let firstEle = self.jobList.first {
                let coordinate = CLLocationCoordinate2D(latitude:firstEle.latitude, longitude: firstEle.longitude)
                self.mapView.camera = GMSCameraPosition.camera(withTarget: coordinate, zoom: zoom)
                self.mapView.animate(toLocation: coordinate)
            }
        }
    }
    
     func addMarker(with location: CLLocationCoordinate2D, data: CandidateJobList) {
        
        self.marker = GMSMarker(position: location)
        marker.userData = data
        marker.icon = nil
        self.markers.append(marker)
        marker.icon = #imageLiteral(resourceName: "icBlueMapPinWhite")
        marker.map = self.mapView
    }
    
     func showCustomInfoWindow(_ status: Bool){
        
        if status {
            
            var heightOfTableView: CGFloat = 0.0
            // Get visible cells and sum up their heights
            let cells = self.jobListTableView.visibleCells
            
            for cell in cells {
                heightOfTableView += cell.frame.height
            }
            if self.isSingle{
                self.mapInfoViewWidth.constant = SCREEN_WIDTH-40
                
                if SCREEN_HEIGHT < 800{
                    self.customMapInfoWindow.frame = CGRect(x: self.customMapInfoWindow.frame.minX, y: self.mapView.centerY , width: self.customMapInfoWindow.frame.width, height:280)
                }else{
                     self.customMapInfoWindow.frame = CGRect(x: self.customMapInfoWindow.frame.minX, y: self.mapView.centerY + 50, width: self.customMapInfoWindow.frame.width, height:280)
                }
               
                self.mapInfoWindowViewHight.constant = 280
                self.infoViewBottomConstraints.constant = -8
            }else{
                self.mapBlurView.isHidden = false
                customMapJobListVC.jobList = self.clusterData
                self.customMapJobListVC.jobListTableView.reloadData()
                self.mapInfoViewWidth.constant = SCREEN_WIDTH
                if heightOfTableView < self.view.frame.height - 100{
                    self.mapInfoWindowViewHight.constant = self.mapView.height - 50
                }else{
                    self.mapInfoWindowViewHight.constant = self.mapView.height - 50
                }
                
                self.customMapInfoWindow.frame = CGRect(x: self.customMapInfoWindow.frame.minX, y: self.mapView.frame.maxY , width: self.customMapInfoWindow.frame.width, height:self.mapView.height)
                self.infoViewBottomConstraints.constant = -8
                self.customMapInfoWindow.origin.y = 50
                
            }
            
            self.jobListTableView.reloadData()
            
            UIView.animate(withDuration: 0.3, animations: {
                
                self.customMapInfoWindow.alpha = 1
                self.mapBlurView.alpha = 1
            }, completion: { (_) in
                self.customMapInfoWindow.isHidden = false
                self.mapBlurView.isHidden = false
            })
        } else {
            
            UIView.animate(withDuration: 0.3, animations: {
                self.mapBlurView.alpha = 0
                self.customMapInfoWindow.alpha = 0
            }, completion: { (_) in
                self.mapBlurView.isHidden = true
                self.customMapInfoWindow.isHidden = true
            })
        }
    }
}

//MARK: - GMUClusterManagerDelegate
//===================================

extension JobsListVC: GMUClusterManagerDelegate ,GMUClusterRendererDelegate {
    
    func renderer(_ renderer: GMUClusterRenderer, willRenderMarker marker: GMSMarker) {
        if marker.userData is GMUCluster{
           print_debug("marker.userData is GMUCluster ==========")
        }else{
            marker.icon = #imageLiteral(resourceName: "icBlueMapPinWhite")
        }
    }
    
    //    func clusterManager(_ clusterManager: GMUClusterManager, didTap clusterItem: GMUClusterItem) -> Bool {
    //
    //        print_debug(clusterItem)
    //        return true
    //    }
    private func clusterManager(clusterManager: GMUClusterManager, didTapCluster cluster: GMUCluster) {
        
        let newCamera = GMSCameraPosition.camera(withTarget: cluster.position,
                                                 zoom: mapView.camera.zoom + 1)
        
        
        let update = GMSCameraUpdate.setCamera(newCamera)
        mapView.moveCamera(update)
    }
    
    func clusterManager(_ clusterManager: GMUClusterManager, didTap cluster: GMUCluster) -> Bool {
        //        let newCamera = GMSCameraPosition.camera(withTarget: cluster.position,
        //                                                 zoom: mapView.camera.zoom + 1)
        //        let update = GMSCameraUpdate.setCamera(newCamera)
        //        mapView.moveCamera(update)
        //        cluster.items[1] as? POIItem)?.jobList[0].jobAddress
        self.clusterData.removeAll()
        for item in cluster.items{
            
            if let poitem = item as? POIItem{
                
                self.clusterData.append(self.jobList[poitem.index-1])
            }
        }
        print_debug( self.jobList)
        self.isSingle = false
        self.mapInfoSubView.isHidden = false
        
        self.showCustomInfoWindow(true)
        return true
    }
}

//MARK:- Mapview delegate
//===========================================
extension JobsListVC: GMSMapViewDelegate{
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        
        if let selectedMarker = mapView.selectedMarker {
            selectedMarker.icon = #imageLiteral(resourceName: "icBlueMapPinWhite")
        }
        self.mapInfoSubView.isHidden = true
//        mapView.clear()
        self.marker.icon = #imageLiteral(resourceName: "icBlueMapPinWhite")
        self.showCustomInfoWindow(false)
        // DispatchQueue.main.async {
        self.setMarkerToMapView(camera: mapView.camera, zoom: mapView.camera.zoom, selectedLocation: self.mapcurrentLocation)
        self.mapView.camera = GMSCameraPosition.camera(withTarget: coordinate, zoom: self.currentZoom )
        
        //        print_debug(self.mapView.camera.zoom)
        //
        //        print_debug(mapView.camera.zoom)
        
        
    }
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        if isMap != 0 {
            
            self.mapContainerView.isHidden = false
            self.isMap = 1
            let loc = CLLocation(latitude: position.target.latitude, longitude: position.target.longitude)
            self.mapcurrentLocation = loc
            loc.convertToPlaceMark({ [weak self](place) in
                guard let strongSelf = self else{return}

                let stateName = strongSelf.getUSAFullStateName(state: place.administrativeArea ?? "")
                let title = "\(StringConstants.JobsIn) \(stateName)"
                let attributedString =  NSMutableAttributedString(string: title)
                attributedString.setColorForText(textForAttribute: stateName, withColor: AppColors.black46, font: AppFonts.Poppins_Bold.withSize(17))
                strongSelf.titleLabel.attributedText = attributedString
                
            })
            let coordinate₀ = CLLocation(latitude: position.target.latitude, longitude: position.target.longitude)
            var coordinate₁ = CLLocation()
            if let lat = self.currentLocation?.coordinate.latitude, let long = self.currentLocation?.coordinate.longitude{
                coordinate₁ = CLLocation(latitude: lat, longitude: long)
            }
            
            pendingItem?.cancel()
            let newRequest = DispatchWorkItem(block: {
                let distanceInMeters = coordinate₀.distance(from: coordinate₁) // result is in meters
                self.getJobListForMap(distance: distanceInMeters, position: position)
            })
            
            pendingItem = newRequest
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: newRequest)
        }
    }
    
    private func getJobListForMap(distance : CLLocationDistance, position : GMSCameraPosition){
        if distance > 1000{
            self.getJobList(filterID: self.filteredCategoryId, searchText: self.searchText, sortByFilter: sortByFilter.rawValue, pageNo: 0, primpage: 0, loader: true, lat: "\(position.target.latitude)", long: "\(position.target.longitude)")
        }
    }
    
    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {
        self.currentZoom = mapView.camera.zoom
        
      //  customMapJobListVC.view.removeFromSuperview()
        print_debug("mapView.camera.zoom \(mapView.camera.zoom)")
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        if let poiItem = marker.userData as? POIItem {
            
            self.markerData = self.jobList[poiItem.index-1]
            self.populateDataOnMapInfoWindow(data: self.jobList[poiItem.index-1])
            self.mapInfoSubView.isHidden = true
            self.isSingle = true
            
        } else {
            
            self.isSingle = false
            self.mapInfoSubView.isHidden = false
        }
        
        self.showCustomInfoWindow(true)
        return true
    }
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        return false
    }
}
