
//  JobsListVC.swift
//  JobGet
//
//  Created by Manish Nainwal on 11/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import GoogleMaps
import ViewAnimator
import DZNEmptyDataSet

enum SortByFilter: Int {
    case nearBy = 1
    case newest = 2
    case none   = 3
}

class JobsListVC: BaseVC, UIGestureRecognizerDelegate {
    
    //MARK:- Enum
    //==================
    
    
    //MARK:- Properties
    //==================
    var customMapJobListVC: JobListMapClusterVC!
    var pageNo = 0
    var primpage = 0
    var filterPopupSelectedIndex = 0
    var jobList = [CandidateJobList]()
    var markerData: CandidateJobList?
    var selectedFilterIndex = [IndexPath]()
    var isPaginationEnable = false
    var selectedIndex: IndexPath?
    var shouldLoadMoreData = false
    var filteredCategoryId = ""
    var job_id = ""
    var recruiterId = ""
    var companyLatitude = StringConstants.Boston_Lat
    var companyLongtitude = StringConstants.Boston_Long
    var categoryId = ""
    var currentZoom: Float = 12.5
    var markers = [GMSMarker]()
    var fromProfile = false
    var marker = GMSMarker()
    var iOpenController = false
    var sortByFilter: SortByFilter = .newest
    var appliedJobIndexArray = [IndexPath]()
    var customWindowYPosition :CGFloat?
    var customWindowCenter: CGPoint?
    var radius = "20"
    var isLocationGet = false
    var clusterData = [CandidateJobList]()
    var filterDataDic = [String: AnyObject]()
    var clusterManager: GMUClusterManager!
    var isSingle = false
    var isMap = 0
    var currentAddress: String?
    var indexpath: IndexPath?
    var isSameIndex = false
    var searchText = ""
    var fromTabbar = true
    var mapcurrentLocation: CLLocation?
    var fromPush = false
    var sender_id = ""
    var pendingItem : DispatchWorkItem?
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var addViewWidth: NSLayoutConstraint!
    @IBOutlet weak var addViewTrailingConst: NSLayoutConstraint!
    @IBOutlet weak var adView: UIView!
    @IBOutlet weak var categoryBgView: UIView!
    @IBOutlet weak var infoViewBottomConstraints: NSLayoutConstraint!
    @IBOutlet weak var mapBlurView: UIView!
    @IBOutlet weak var mapInfoViewWidth: NSLayoutConstraint!
    @IBOutlet weak var searchBubbleView: UIView!
    @IBOutlet weak var mapInfoSubView: UIView!
    @IBOutlet weak var mapInfoWindowViewHight: NSLayoutConstraint!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var badgeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var jobListTableView: UITableView!
    @IBOutlet weak var filterSortContainerView: UIView!
    @IBOutlet weak var mapContainerView: UIView!
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var listButton: UIButton!
    @IBOutlet weak var infoWindowHeaderImage: UIImageView!
    @IBOutlet weak var inFoWindowJobNameLabel: UILabel!
    @IBOutlet weak var infoWindowJobCategoryLabel: UILabel!
    @IBOutlet weak var infoWindowJobTimeLabel: UILabel!
    @IBOutlet weak var infoWindowJobSeenBtn: UIButton!
    @IBOutlet weak var infoWindowExperianceLabel: UILabel!
    @IBOutlet weak var infoWindowApplyButton: UIButton!
    @IBOutlet weak var infoWindowSalaryLabel: UILabel!
    @IBOutlet weak var infoWindowCompanyNameLabel: UILabel!
    @IBOutlet weak var customMapInfoWindow: UIView!
    
    //MARK:- Variables and Constants
    //==============================
    
    var currentLocation: CLLocation? {
        
        didSet {
            print_debug(currentLocation)
            
            AppUserDefaults.save(value: "\(currentLocation?.coordinate.latitude ?? 42.361145)", forKey: .userLat)
            AppUserDefaults.save(value: "\(currentLocation?.coordinate.longitude ?? -71.057083)", forKey: .userLong)
            companyLatitude = AppUserDefaults.value(forKey: .userLat).stringValue
            companyLongtitude = AppUserDefaults.value(forKey: .userLong).stringValue
            
            self.getJobList(filterID: self.filteredCategoryId, searchText: self.searchText, sortByFilter: self.sortByFilter.rawValue, pageNo: self.pageNo, primpage: primpage, loader: true, lat: companyLatitude, long: companyLongtitude)
            self.isLocationGet = true
            print_debug(currentLocation)
            
            DispatchQueue.main.async { [weak self] in
                
                guard let strongSelf = self else{ return }
                guard AppUserDefaults.value(forKey: .filterLat).stringValue.isEmpty else {
                    strongSelf.getCurrentCity(lat: "", long: "", isFilter: true)
                    return
                }
                strongSelf.currentLocation?.convertToPlaceMark({ (place) in
                    strongSelf.filterDataDic["coordinates"] = strongSelf.currentLocation
                    strongSelf.filterDataDic["city"] = place.locality as AnyObject
                    strongSelf.currentAddress = "\(place.locality ?? ""), \(place.administrativeArea ?? "")"
                    strongSelf.filterDataDic["address"] = "\(place.locality ?? ""), \(place.administrativeArea ?? "")" as AnyObject
                    let stateName = strongSelf.getUSAFullStateName(state: place.administrativeArea ?? "")
                    let title = "\(StringConstants.JobsIn) \(stateName)"
                    let attributedString =  NSMutableAttributedString(string: title)
                    attributedString.setColorForText(textForAttribute: stateName, withColor: AppColors.black46, font: AppFonts.Poppins_Bold.withSize(17))
                    strongSelf.titleLabel.attributedText = attributedString
                    
                })
            }
        }
    }
    
    //MARK:- LifeCycle Methods
    //========================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        
        if !SharedLocationManager.locationsEnabled{
            getLocation()
        }else{
            SharedLocationManager.fetchCurrentLocation { (location) in
                SharedLocationManager.locationManager.stopUpdatingLocation()
                self.currentLocation = location
                self.companyLatitude = "\(location.coordinate.latitude)"
                self.companyLongtitude = "\(location.coordinate.longitude)"
                
            }
            NotificationCenter.default.addObserver(self, selector: #selector(self.manageDenyLocation), name: NSNotification.Name.init(rawValue: "DENY_CURRENT_LOCATION_PERMISSION"), object: nil)
//            SharedLocationManager.deniedLocationPermission { (error) in
//            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if AppUserDefaults.value(forKey: .totalNotification).intValue > 0{
            self.badgeLabel.isHidden = false
        }else{
            self.badgeLabel.isHidden = true
        }
//        if CLLocationManager.authorizationStatus() == .denied {
//            getLocation()
//        }
//        if fromTabbar{
////            searchBubbleView.isHidden = true
//            getLocation()
//            self.pageNo = 0
//            self.primpage = 0
//            self.shouldLoadMoreData = false
//            self.isPaginationEnable = false
//        }
        if fromProfile {
            print_debug(job_id)
            print_debug(self.recruiterId)
            candidateAppliedJob(job_id: self.job_id, recruiterId: self.recruiterId)
        }
//        fromProfile = false
        if fromPush{
            fromPush = false
            let candidateDetail = CandidateJobDetailVC.instantiate(fromAppStoryboard: .Candidate)
            candidateDetail.commingFromScreen = .deepLink
            candidateDetail.jobId = job_id
            candidateDetail.recruiterId = sender_id
            sharedAppDelegate.navigationController?.pushViewController(candidateDetail, animated: true)
        }
        if let markerData = self.markerData {
            self.populateDataOnMapInfoWindow(data: markerData)
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        if let touch:UITouch = touches.first{
            if touch.view == self.mapBlurView {
                UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                    self.mapBlurView.isHidden = true
                    self.customMapInfoWindow.frame.origin.y =  self.view.frame.height + 160
                }, completion: nil)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.customWindowYPosition = self.customMapInfoWindow.frame.minY
        self.customWindowCenter  = self.customMapInfoWindow.center
    }
    deinit {

        NotificationCenter.default.removeObserver(self)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.fromTabbar = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        print_debug("memory issue")
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    //MARK:- Private Methods
    //======================
    
    private func initialSetup() {
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.jobListTableView.delegate = self
        self.jobListTableView.dataSource = self
        self.jobListTableView.emptyDataSetSource = self
        self.jobListTableView.emptyDataSetDelegate = self
        self.navigationView.setShadow()
        self.searchBubbleView.roundCorners()
        self.searchBubbleView.isHidden = true
        self.mapView.delegate = self
        self.mapView.isMyLocationEnabled = true
        self.listButton.setImage(#imageLiteral(resourceName: "ic_format_list_bulleted"), for: .selected)
        self.listButton.setImage(#imageLiteral(resourceName: "ic_map"), for: .normal)
        self.listButton.isSelected = false
        self.mapContainerView.isHidden = true
        self.mapContainerView.alpha = 0.0
        self.badgeLabel.roundCorners()
        self.customMapInfoWindow.isHidden = true
        self.mapBlurView.isHidden = true
        print_debug(self.customMapInfoWindow.frame)
        if AppUserDefaults.value(forKey: .totalNotification).intValue > 0{
            self.badgeLabel.isHidden = false
        }else{
            self.badgeLabel.isHidden = true
        }
        self.categoryBgView.roundCorners()
//        self.getLocation()

        self.registerCells()
        
        self.setupMapInfoWindow()
        self.jobListTableView.estimatedRowHeight = 350
        self.jobListTableView.rowHeight = UITableViewAutomaticDimension
        self.jobListTableView.enablePullToRefresh(tintColor: AppColors.themeBlueColor ,target: self, selector: #selector(refreshWhenPull(_:)))
        self.mapView.isMyLocationEnabled = true
        self.mapView.settings.myLocationButton = true
        NotificationCenter.default.addObserver(self, selector: #selector(self.refreshAfterReportUser), name: NSNotification.Name.init(rawValue: "Refresh_After_Report_User"), object: nil)

        NotificationCenter.default.addObserver(self, selector: #selector(self.updateBadge), name: NSNotification.Name.init(rawValue: "Update_Badge_Count"), object: nil)
        
    }
    
    func getUSAFullStateName(state: String)-> String{
        
        var stateName = state
        getAbbreatedState { (stateList) in
            
            _ = stateList.map{ value  in
                if state.contains(s: value["state_abbreviations"] as? String ?? ""){
                    
                    if let state_name = value["state_name"] as? String{
                        stateName = state_name
                    }
                }
            }
        }
        
        return stateName
    }
    
    @objc func updateBadge(){
        self.badgeLabel.isHidden = false
    }
    
    @objc func refreshWhenPull(_ sender: UIRefreshControl) {
        sender.endRefreshing()
        self.getCurrentCity(lat: self.companyLatitude, long: self.companyLongtitude, isFilter: false)
        self.pageNo = 0
        self.primpage = 0
        self.shouldLoadMoreData = false
        self.isPaginationEnable = false
       // self.getLocation()
        self.getJobList(filterID: self.filteredCategoryId, searchText: self.searchText, sortByFilter: sortByFilter.rawValue, pageNo: 0, primpage: primpage, loader: false, lat: companyLatitude, long: companyLongtitude)
    }
    
    private func registerCells() {
        self.jobListTableView.register(UINib(nibName: "JobListCell", bundle: nil), forCellReuseIdentifier: "JobListCell")
        
        self.jobListTableView.register(UINib(nibName: "LoadMoreTableViewCell", bundle: nil), forCellReuseIdentifier: "LoadMoreTableViewCell")
        
    }
    
    private func isLoadingIndexPath(_ indexPath: IndexPath) -> Bool {
        guard shouldLoadMoreData else { return false}
        
        return indexPath.row == self.jobList.count
    }
    
    func getLocation() {
        
        self.isPaginationEnable = false
        
        DispatchQueue.main.async {[weak self] in
            guard let _self = self else{return}
            if CLLocationManager.authorizationStatus() == .denied {
                
                if AppUserDefaults.value(forKey: .isLogin).boolValue{
                    if AppUserDefaults.value(forKey: .filterLat).stringValue.isEmpty{
                        if let data = AppUserDefaults.value(forKey: .userData).dictionary, let lat = data["lat"]?.double,let lng = data["lng"]?.double{
                            if lat != 0.0{
                                _self.currentLocation = CLLocation(latitude: lat, longitude: lng)
                                _self.companyLatitude = "\(lat)"
                                _self.companyLongtitude = "\(lng)"
                            }else{
                                _self.currentLocation = CLLocation(latitude: 42.361145, longitude: -71.057083)
                            }
                        }
                    }else if let lat = AppUserDefaults.value(forKey: .filterLat).string,let lng = AppUserDefaults.value(forKey: .filterLong).string{
                        if lat != "0.0"{
                            _self.currentLocation = CLLocation(latitude: AppUserDefaults.value(forKey: .filterLat).doubleValue, longitude: AppUserDefaults.value(forKey: .filterLong).doubleValue)
                            _self.companyLatitude = "\(lat)"
                            _self.companyLongtitude = "\(lng)"
                            
                        }
                    }else {
                        _self.currentLocation = CLLocation(latitude: 42.361145, longitude: -71.057083)
                    }

                }else{
                    CommonClass.delayWithSeconds(1.0, completion: {
                        let alertViewController = UIAlertController(title: StringConstants.Location_Allow_Title, message: StringConstants.Fetch_Location_Reason_Text, preferredStyle: UIAlertControllerStyle.alert)
                        let okAction = UIAlertAction(title: StringConstants.Allow, style: UIAlertActionStyle.default, handler: { (action : UIAlertAction) in
                            //        TODO: Add settings
                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: { (success) in
                                    
                                })
                            } else {
                                // Fallback on earlier versions
                            }
                        })
                        
                        let deny = UIAlertAction(title: StringConstants.Deny, style: UIAlertActionStyle.default, handler: { (action : UIAlertAction) in
                            NotificationCenter.default.addObserver(_self, selector: #selector(_self.updateLocation(noti:)), name: NSNotification.Name(rawValue: "UPDATE_LOCATION"), object: nil)
//                            AppUserDefaults.save(value: false, forKey: .isLogin)
                            let updateProfileLocationPopupScene = UpdateProfileLocationPopupVC.instantiate(fromAppStoryboard: .settingsAndChat)
                            updateProfileLocationPopupScene.delegate = self
                            updateProfileLocationPopupScene.openLocationPopupFrom = .jobListCand
                            updateProfileLocationPopupScene.address = "Boston, MA"
                            sharedAppDelegate.window?.addSubview(updateProfileLocationPopupScene.view)
                            _self.addChildViewController(updateProfileLocationPopupScene)
                        })
                        alertViewController.addAction(deny)
                        alertViewController.addAction(okAction)
                        
                        _self.present(alertViewController, animated: true, completion: nil)
                    })
                }
                
            } else {
                if AppUserDefaults.value(forKey: .filterLat).stringValue.isEmpty{
                    if let data = AppUserDefaults.value(forKey: .userData).dictionary, let lat = data["lat"]?.double,let lng = data["lng"]?.double{
                        if lat != 0.0{
                            _self.currentLocation = CLLocation(latitude: lat, longitude: lng)
                            _self.companyLatitude = "\(lat)"
                            _self.companyLongtitude = "\(lng)"
                        }else{
                            SharedLocationManager.fetchCurrentLocation { (location) in
                                SharedLocationManager.locationManager.stopUpdatingLocation()
                                _self.currentLocation = location
                                _self.companyLatitude = "\(location.coordinate.latitude)"
                                _self.companyLongtitude = "\(location.coordinate.longitude)"
                            }
                        }
                    }
                }else if let lat = AppUserDefaults.value(forKey: .filterLat).string,let lng = AppUserDefaults.value(forKey: .filterLong).string{
                    if lat != "0.0"{
                        _self.currentLocation = CLLocation(latitude: AppUserDefaults.value(forKey: .filterLat).doubleValue, longitude: AppUserDefaults.value(forKey: .filterLong).doubleValue)
                        _self.companyLatitude = "\(lat)"
                        _self.companyLongtitude = "\(lng)"
                    }
                }else {
                    SharedLocationManager.fetchCurrentLocation { (location) in
                        SharedLocationManager.locationManager.stopUpdatingLocation()
                        _self.currentLocation = location
                        _self.companyLatitude = "\(location.coordinate.latitude)"
                        _self.companyLongtitude = "\(location.coordinate.longitude)"
                    }
                }
//                if AppUserDefaults.value(forKey: .userLat).isEmpty{
//                    SharedLocationManager.fetchCurrentLocation { (location) in
//                        SharedLocationManager.locationManager.stopUpdatingLocation()
//                        _self.currentLocation = location
//                    }
//
//                }else{
//                    _self.currentLocation = CLLocation(latitude: AppUserDefaults.value(forKey: .userLat).doubleValue, longitude: AppUserDefaults.value(forKey: .userLong).doubleValue)
//
//            }
        }
    }
}


private func flipViews(show: Bool){
    customMapJobListVC = JobListMapClusterVC.instantiate(fromAppStoryboard: .Candidate)
    if show {
        if let lat = Double(companyLatitude), let long = Double(companyLongtitude){
            customMapJobListVC.currentLocation = CLLocation(latitude: lat, longitude: long)
        }
        self.mapView.clear()
        self.mapContainerView.isHidden = false
        self.isMap = 1
        customMapJobListVC.currentLocation?.convertToPlaceMark({ (place) in
            let stateName = self.getUSAFullStateName(state: place.administrativeArea ?? "")
            let title = "\(StringConstants.JobsIn) \(stateName)"
            let attributedString =  NSMutableAttributedString(string: title)
            attributedString.setColorForText(textForAttribute: stateName, withColor: AppColors.black46, font: AppFonts.Poppins_Bold.withSize(17))
            self.titleLabel.attributedText = attributedString
        })

        self.getJobList(filterID: self.filteredCategoryId, searchText: self.searchText, sortByFilter: 1, pageNo: 0, primpage: 0, loader: true, lat: "\(customMapJobListVC.currentLocation?.coordinate.latitude ?? 42.361145)", long: "\(customMapJobListVC.currentLocation?.coordinate.longitude ?? -71.057083)")
        customMapJobListVC.view.frame = self.mapInfoSubView.frame
        self.mapInfoSubView.addSubview(customMapJobListVC.view)
        self.addChildViewController(customMapJobListVC)
        customMapJobListVC.didMove(toParentViewController: self)
        UIView.animate(withDuration: 0.5, animations: {
            self.mapContainerView.alpha = 1.0
        })
        self.customMapInfoWindow.isHidden = true
        self.customMapInfoWindow.alpha = 0
        //            self.setMarkerToMapView(camera: nil, zoom: 10.0, selectedLocation: self.currentLocation)
    } else {
        self.isMap = 0
        self.jobList.removeAll(keepingCapacity: false)
        self.jobListTableView.reloadData()
        if let lat = Double(companyLatitude), let long = Double(companyLongtitude){
            let loc = CLLocation(latitude: lat, longitude: long)
            loc.convertToPlaceMark({ (place) in
                let stateName = self.getUSAFullStateName(state: place.administrativeArea ?? "")
                let title = "\(StringConstants.JobsIn) \(stateName)"
                let attributedString =  NSMutableAttributedString(string: title)
                attributedString.setColorForText(textForAttribute: stateName, withColor: AppColors.black46, font: AppFonts.Poppins_Bold.withSize(17))
                self.titleLabel.attributedText = attributedString
            })
        }

        self.getJobList(filterID: self.filteredCategoryId, searchText: self.searchText, sortByFilter: sortByFilter.rawValue, pageNo: 0, primpage: 0, loader: true, lat: self.companyLatitude, long: self.companyLongtitude)
        self.showCustomInfoWindow(false)
        
        customMapJobListVC.view.removeFromSuperview()
        //  self.mapContainerView.alpha = 0.0
        
        UIView.animate(withDuration: 0.5, animations: {
            self.mapContainerView.alpha = 0.0
            self.mapContainerView.isHidden = true
        })
    }
}

//MARK:- Target Actions
//=====================
@objc func shareJobButtonTapped(_ sender: UIButton) {
    
    guard let idx = sender.tableViewIndexPath(self.jobListTableView) else { return }
    self.displayShareSheet(shareContent: self.jobList[idx.row].jobShareUrl)
}

@objc func applyButtonTapped(_ sender: UIButton) {
    
    guard let index = sender.tableViewIndexPath(self.jobListTableView) else { return }
    self.job_id = self.jobList[index.row].jobId
    self.categoryId = self.jobList[index.row].categoryId
    if let recruiterDetail = self.jobList[index.row].recruiterDetail {
        self.recruiterId = recruiterDetail.recruiterId
        print_debug(recruiterDetail.recruiterId)
        self.getCandidateJobDetail(loader: true, job_id: job_id, recruiterId: recruiterDetail.recruiterId)
        self.indexpath = index
        
    }
}

//MARK:- IBActionsx
//================
@IBAction func infoWindowApplyButtonTapped(_ sender: UIButton) {
    
    if let job_id = self.markerData?.jobId, let recruiterDetail = self.markerData?.recruiterDetail{
        self.job_id = job_id
        self.recruiterId = recruiterDetail.recruiterId
        self.getCandidateJobDetail(loader: true, job_id: self.job_id, recruiterId: self.recruiterId)
    }
}

@IBAction func onTappingSearchButton(_ sender: UIButton) {
    
    let searchScene = CandidateSearchFilterVC.instantiate(fromAppStoryboard: .settingsAndChat)
    searchScene.delegate = self
    
    if self.listButton.isSelected{
        searchScene.isOpenFromMap = true
    }
    searchScene.radius = radius
    searchScene.city = self.filterDataDic["city"] as? String ?? ""
    if isMap == 0{
        searchScene.filterDic["currentLocation"] = self.filterDataDic["coordinates"]
        if let lat = Double(companyLatitude),let long = Double(companyLongtitude){
            searchScene.searchLocation = CLLocation(latitude: lat, longitude: long)

        }
        searchScene.address = self.filterDataDic["address"] as? String ?? ""
        searchScene.currentAddress = self.currentAddress

    }else{
        searchScene.filterDic["currentLocation"] = self.mapcurrentLocation
        searchScene.searchLocation = mapcurrentLocation

    }
//    searchScene.address = self.filterDataDic["address"] as? String ?? ""
    searchScene.sortByFilter = self.sortByFilter
    searchScene.selectedFilterIndex = self.selectedFilterIndex
    searchScene.filteredCategoryId = self.filteredCategoryId
    searchScene.searchText = self.filterDataDic["searchText"] as? String ?? ""
    searchScene.totalSelectedCategory = self.filterDataDic["categoryName"] as? String ?? ""
    self.navigationController?.pushViewController(searchScene, animated: true)
    //        let rootViewController = UIApplication.shared.keyWindow?.rootViewController
    //        rootViewController?.present(searchScene, animated: true, completion: nil)
}

@IBAction func onTappingNotificationButton(_ sender: UIButton) {
    let notificationVC = NotificationsVC.instantiate(fromAppStoryboard: .settingsAndChat)
    self.navigationController?.pushViewController(notificationVC, animated: true)
    
}

@IBAction func onTappingListButton(_ sender: UIButton) {
    
    sender.isSelected = !sender.isSelected
    
    DispatchQueue.main.async {
        self.flipViews(show: sender.isSelected)
    }
}

@IBAction func onTappingSortButton(_ sender: UIButton) {
    
}

@IBAction func onTappingFilterButton(_ sender: UIButton) {
    
}

@IBAction func showControllerButtonTapped(_ sender: UIButton) {
    
    self.openCandidateDetail()
}

func openCandidateDetail() {
    let candidateDetailSceen = CandidateJobDetailVC.instantiate(fromAppStoryboard: .Candidate)
    candidateDetailSceen.commingFromScreen = .jobListCell
    //        increment 1 cause when its back on this screen its show updated view
    self.markerData?.totalJobView = (self.markerData?.totalJobView ?? 0) + 1
    candidateDetailSceen.jobId = self.markerData?.jobId ?? ""
    candidateDetailSceen.categoryId = self.markerData?.categoryId ?? ""
    candidateDetailSceen.delegate = self
    candidateDetailSceen.recruiterId = self.markerData?.recruiterDetail?.recruiterId ?? ""
    candidateDetailSceen.currentLocation = self.currentLocation
    if let detail = self.markerData?.recruiterDetail {
        candidateDetailSceen.recruiterDetail = detail
    }
    
    candidateDetailSceen.isSaved   = self.markerData?.isSaved ?? 0
    candidateDetailSceen.commingFromScreen = .mapView
    self.navigationController?.pushViewController(candidateDetailSceen, animated: true)
}

@IBAction func handlePan(_ sender: UIPanGestureRecognizer) {
    
    if sender.velocity(in: self.customMapInfoWindow).y < 0{
        return
    }
    
    guard let windowCenter = self.customWindowCenter else { return }
    let translation = sender.translation(in: self.view)
    if let view = sender.view {
        let yPositon = view.frame.minY + translation.y
        
        view.frame.origin.y = yPositon
        
        print_debug(view.center)
    }
    sender.setTranslation(CGPoint.zero, in: self.view)
    
    if sender.state == UIGestureRecognizerState.changed {
        
        if sender.view!.center.y < windowCenter.y - 100.0{
            
            UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                sender.view!.frame.origin.y =  self.customMapInfoWindow.frame.height + 160
                self.mapBlurView.alpha = 0
            }, completion: { (true) in
                self.mapBlurView.isHidden = true
                if let selectedMarker = self.mapView.selectedMarker {
                    selectedMarker.icon = #imageLiteral(resourceName: "icBlueMapPinWhite")
                }
            })
        }
    }
    
    if sender.state == UIGestureRecognizerState.ended {
        let velocity = sender.velocity(in: self.view)
        let magnitude = sqrt((velocity.x * velocity.x) + (velocity.y * velocity.y))
        let slideMultiplier = magnitude / 25
        
        if sender.view!.center.y < sender.view!.center.y+300{
            
            UIView.animate(withDuration: 1.0, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                sender.view!.frame.origin.y =  self.view.frame.height + 160
                self.mapBlurView.alpha = 0
            }, completion: { (true) in
                self.mapBlurView.isHidden = true
                if let selectedMarker = self.mapView.selectedMarker {
                    selectedMarker.icon = #imageLiteral(resourceName: "icBlueMapPinWhite")
                }
            })
        }else{
            UIView.animate(withDuration: 0.5, delay: 0, options: UIViewAnimationOptions.curveEaseOut, animations: {
                self.mapBlurView.isHidden = true
                sender.view!.frame.origin.y =  sender.view!.frame.origin.y
            }, completion: nil)
        }
    }
}
}

//MARK:- Location alert popup Delegate
//===========================================
extension JobsListVC: AlertPopUpDelegate , CompleteProfileWithLocationDelegate{
    
    func didTapAffirmativeButton() {
        //        TODO: Add settings
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: { (success) in
                
            })
        } else {
            // Fallback on earlier versions
        }
    }
    
    @objc func manageDenyLocation(){
        
        print_debug("location access denied")
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateLocation(noti:)), name: NSNotification.Name(rawValue: "UPDATE_LOCATION"), object: nil)
        
        let updateProfileLocationPopupScene = UpdateProfileLocationPopupVC.instantiate(fromAppStoryboard: .settingsAndChat)
        updateProfileLocationPopupScene.delegate = self
        updateProfileLocationPopupScene.openLocationPopupFrom = .jobListCand
        updateProfileLocationPopupScene.address = "Boston, MA"
        sharedAppDelegate.window?.addSubview(updateProfileLocationPopupScene.view)
        self.addChildViewController(updateProfileLocationPopupScene)
    }
    
    func didTapNegativeButton() {
        self.manageDenyLocation()
        print_debug("location access denied")
        //        NotificationCenter.default.addObserver(self, selector: #selector(self.updateLocation(noti:)), name: NSNotification.Name(rawValue: "UPDATE_LOCATION"), object: nil)
        //
        //        let updateProfileLocationPopupScene = UpdateProfileLocationPopupVC.instantiate(fromAppStoryboard: .settingsAndChat)
        //        updateProfileLocationPopupScene.delegate = self
        //        updateProfileLocationPopupScene.openLocationPopupFrom = .jobListCand
        //        updateProfileLocationPopupScene.address = "Boston, MA"
        //        sharedAppDelegate.window?.addSubview(updateProfileLocationPopupScene.view)
        //        self.addChildViewController(updateProfileLocationPopupScene)
        
    }
    
    
    func completeProfileWithLocationDelegate() {
        
        self.filterDataDic["currentLocation"] = CLLocation(latitude: 42.361145, longitude: -71.057083)
        self.filterDataDic["coordinates"] = CLLocation(latitude: 42.361145, longitude: -71.057083)
        AppUserDefaults.save(value: 42.361145, forKey: .userLat)
        AppUserDefaults.save(value: -71.057083, forKey: .userLong)
        self.filterDataDic["city"] = "Boston" as AnyObject
        self.currentAddress = "Boston, MA"
        self.filterDataDic["address"] = "Boston, MA"  as AnyObject
        let title = "\(StringConstants.JobsIn) Massachusetts"
        let attributedString =  NSMutableAttributedString(string: title)
        attributedString.setColorForText(textForAttribute: "Massachusetts", withColor: AppColors.black46, font: AppFonts.Poppins_Bold.withSize(17))
        self.titleLabel.attributedText = attributedString
        self.getJobList(filterID: self.filteredCategoryId, searchText: self.searchText, sortByFilter: self.sortByFilter.rawValue, pageNo: self.pageNo, primpage: primpage, loader: true, lat: "42.361145", long: "-71.057083")
    }
    
    @objc func updateLocation(noti: Notification){
        
        if let data = noti.userInfo as? [String:Any]{
            
            if let lat = Double(data["latitude"] as? String ?? ""), let long = Double(data["longitude"] as? String ?? ""){
                AppUserDefaults.save(value:lat, forKey: .userLat)
                AppUserDefaults.save(value: long, forKey: .userLong)
                self.filterDataDic["currentLocation"] = CLLocation(latitude: lat, longitude: long)
                self.filterDataDic["coordinates"] = CLLocation(latitude: lat, longitude: long)
            }
            
            self.filterDataDic["city"] = data["city"] as AnyObject
            self.currentAddress = "\(data["city"] as? String ?? ""), \(data["state"] as? String ?? "")"
            self.filterDataDic["address"] = "\(data["city"] as? String ?? ""), \(data["state"] as? String ?? "")" as AnyObject
            
            let stateName = self.getUSAFullStateName(state: data["state"] as? String ?? "")
            
            let title = "\(StringConstants.JobsIn) \(stateName)"
            let attributedString =  NSMutableAttributedString(string: title)
            attributedString.setColorForText(textForAttribute: stateName, withColor: AppColors.black46, font: AppFonts.Poppins_Bold.withSize(17))
            self.titleLabel.attributedText = attributedString
            self.getJobList(filterID: self.filteredCategoryId, searchText: self.searchText, sortByFilter: self.sortByFilter.rawValue, pageNo: self.pageNo, primpage: primpage, loader: true, lat: data["latitude"] as? String ?? "", long: data["longitude"] as? String ?? "")
        }
    }
}

//MARK:- Get Search Text
//===========================
extension JobsListVC: SearchCompelete {
    
    func getSearchedText(text: String, lat: String, long: String, radius: String, sort: Int, categoryID: String, selectedCatFilterIndex: [IndexPath], categoryName: String, isClear: Bool) {
        
        self.filteredCategoryId = categoryID
        self.selectedFilterIndex = selectedCatFilterIndex
        self.filterDataDic["searchText"] = text as AnyObject
        self.searchText = text
        print_debug(lat)
        print_debug(long)
        var lati = lat
        var longi = long
        self.radius = radius
        self.filterDataDic["categoryName"] = categoryName as AnyObject
        if !self.filteredCategoryId.isEmpty || !text.isEmpty ||  lat != "\(AppUserDefaults.value(forKey: .userLat).doubleValue)" || longi != "\(AppUserDefaults.value(forKey: .userLong).doubleValue)"{
            if isClear{
                self.searchBubbleView.isHidden = true
                AppUserDefaults.removeValue(forKey: .filterLong)
                AppUserDefaults.removeValue(forKey: .filterLat)
            }else{
                self.searchBubbleView.isHidden = false
                AppUserDefaults.save(value: lat, forKey: .filterLat)
                AppUserDefaults.save(value: longi, forKey: .filterLong)
            }
        }else{
            self.searchBubbleView.isHidden = true
//            AppUserDefaults.removeValue(forKey: .filterLong)
//            AppUserDefaults.removeValue(forKey: .filterLat)
        }
        
        if lat == "0.0" && long == "0.0"{
            lati = "\(self.companyLatitude)"
            longi = "\(self.companyLongtitude)"
        }else{
            self.companyLatitude = lat
            self.companyLongtitude = long
        }
        
        if !lat.isEmpty && isMap == 0{
            self.getCurrentCity(lat: lati, long: longi, isFilter: true)
        }
        
        if !self.jobList.isEmpty{
            self.jobListTableView.scrollToRow(at: IndexPath(row:0, section: 0), at: .top, animated: false)
        }
        
        self.getJobList(filterID: self.filteredCategoryId, searchText: text, sortByFilter: sort, pageNo: 0, primpage: 0, loader: true, lat: lati, long: longi)
    }
    
    func getCurrentCity(lat: String, long: String, isFilter: Bool){
        
        if let lati = AppUserDefaults.value(forKey : .filterLat).stringValue.isEmpty ? Double(lat) : AppUserDefaults.value(forKey : .filterLat).doubleValue, let longi = AppUserDefaults.value(forKey : .filterLong).stringValue.isEmpty ? Double(long) : AppUserDefaults.value(forKey : .filterLong).doubleValue{
            
            let fetchLocation = CLLocation(latitude: lati, longitude: longi)
            self.filterDataDic["coordinates"] = fetchLocation

//            self.currentLocation = fetchLocation
            DispatchQueue.mainQueueAsync{
                fetchLocation.convertToPlaceMark({ (place) in
                    
                    if isFilter{
                        self.filterDataDic["address"] = "\(place.locality ?? ""), \(place.administrativeArea ?? "")" as AnyObject
                        self.filterDataDic["city"] = place.locality as AnyObject
                    }
                    let stateName = self.getUSAFullStateName(state: place.administrativeArea ?? "")
                    
                    let title = "\(StringConstants.JobsIn) \(stateName)"
                    let attributedString =  NSMutableAttributedString(string: title)
                    attributedString.setColorForText(textForAttribute: stateName, withColor: AppColors.black46, font: AppFonts.Poppins_Bold.withSize(17))
                    print_debug(attributedString)
                    self.titleLabel.attributedText = attributedString
                })
            }
        }
    }
}


//MARK:- Web Services
//====================
extension JobsListVC {
    
    func getJobList(filterID: String, searchText: String, sortByFilter: Int, pageNo: Int,primpage: Int, loader: Bool, lat: String, long: String) {
        
        var param =  [String : Any]()
        
        param = [ApiKeys.page.rawValue      : pageNo,
                 ApiKeys.categoryId.rawValue: self.filteredCategoryId,
                 ApiKeys.search.rawValue    : searchText,
                 ApiKeys.sort.rawValue      : SortByFilter.newest.rawValue,
                 ApiKeys.latitude.rawValue  : AppUserDefaults.value(forKey : .filterLat).stringValue.isEmpty ? lat : AppUserDefaults.value(forKey : .filterLat).stringValue,
                 ApiKeys.longitude.rawValue : AppUserDefaults.value(forKey : .filterLong).stringValue.isEmpty ? long : AppUserDefaults.value(forKey : .filterLong).stringValue,
                 ApiKeys.radius.rawValue    : self.radius,
                 "primpage"                 : primpage,
                 "isMap" : self.isMap
        ]
        WebServices.getCandidateJobList(parameters: param, loader: loader, success: { (json) in
            if let errorCode = json[ApiKeys.code.rawValue].int, errorCode == error_codes.success {
                
                var data = [CandidateJobList]()
                let jobs = json[ApiKeys.data.rawValue][ApiKeys.jobs.rawValue].arrayValue
                for job in jobs {
                    data.append(CandidateJobList(dict: job))
                }
                if pageNo == 0 {
                    self.jobList.removeAll(keepingCapacity: false)
                }
                if self.shouldLoadMoreData {
                    self.jobList.append(contentsOf: data)
                } else {
                    self.jobList = data
                }
                
                self.pageNo = json[ApiKeys.page.rawValue].intValue
                self.shouldLoadMoreData = json[ApiKeys.next.rawValue].boolValue
                self.primpage = json["primpage"].intValue
                if !self.mapContainerView.isHidden{
                    
                    self.clustering()
                    // Generate and add random items to the cluster manager.
                    self.generateClusterItems(kClusterItem:  self.jobList)
                    print_debug("mapView.camera.zoom \(self.mapView.camera.zoom)")
                    // Call cluster() after items have been added to perform the clustering
                    // and rendering on map.
                    print_debug("self.currentZoom \(self.currentZoom)")
                    self.clusterManager.cluster()
                    
                    self.clusterManager.setDelegate(self, mapDelegate: self)
                    //                _ = self.jobList.map{ data in
                    
                    //                    if let cllocation = self.currentLocation{
                    let fetchLocation = CLLocation(latitude: Double(lat) ?? 42.361145, longitude: Double(long) ?? -71.057083)
                    self.setMarkerToMapView(camera: nil, zoom:  self.currentZoom, selectedLocation: fetchLocation)
                    //                    }else{
                    //                        let fetchLocation = CLLocation(latitude: 42.361145, longitude: -71.057083)
                    //                        self.setMarkerToMapView(camera: nil, zoom:  self.currentZoom, selectedLocation: fetchLocation)
                    //                    }
                }
                self.jobListTableView.reloadData()
                //                self.jobListTableView.perform(#selector(self.relod), on: .main, with: nil, waitUntilDone: true)
                
                //                CommonClass.delayWithSeconds(0.05, completion: {
                if let index = self.selectedIndex , self.isSameIndex{
                    self.isSameIndex = false
                    self.jobListTableView.scrollToRow(at: index, at: .top, animated: true)
                }
                //                })
            } else {

                let msg = json[ApiKeys.message.rawValue].stringValue
                CommonClass.showToast(msg: msg)
            }
            
        }) { (error: Error) in

            CommonClass.showToast(msg: error.localizedDescription )
        }
    }
    
    func getCandidateJobDetail(loader: Bool, job_id: String, recruiterId: String) {
        
        let value = AppUserDefaults.value(forKey: .userData)
        print_debug(value["is_profile_added"])
        print_debug(value["isExperience"])
        if value["isExperience"] == 0 {
            let sceen = ProfileErrorPopUpVC.instantiate(fromAppStoryboard: .Main)
            sceen.delegate = self
            self.view.addSubview(sceen.view)
            self.add(childViewController: sceen)
            
        }else{
            
            candidateAppliedJob(job_id: job_id, recruiterId: recruiterId)
        }
    }
    
    func candidateAppliedJob(job_id: String, recruiterId: String) {
        
        let params = [ApiKeys.userId.rawValue: AppUserDefaults.value(forKey: .userId), ApiKeys.jobId.rawValue: job_id, ApiKeys.recruiterId.rawValue: recruiterId, "categoryId": self.categoryId ] as [String : Any]
        print_debug(params)
        WebServices.applyJob(parameters: params, success: { (json) in
            if let errorCode = json[ApiKeys.code.rawValue].int, errorCode == error_codes.success {
                if let index = self.indexpath{
                    if !self.jobList.isEmpty{
                        self.jobList[index.row].isApplied = 1
                    }
                }
                
                let sceen = ResetPasswordPopupVC.instantiate(fromAppStoryboard: .Main)
                commingFrom = .jobList
                sceen.jobId = job_id
                sceen.delegate = self
                self.view.addSubview(sceen.view)
                self.add(childViewController: sceen)
                NotificationCenter.default.post(Notification.init(name: Notification.Name(rawValue: "Refresh_Saved_Job")))
            } else {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
            
        }) { (error) in
            print_debug(error)
            CommonClass.showToast(msg: error.localizedDescription)
            
        }
    }
}



//MARK:- Sortpoupvc delgate
//===========================================
extension JobsListVC: SortPopUpDelegate {
    
    func didApplyFilter(selecteIndex: Int) {
    }
}

//MARK:- Tableview delegate and datasource
//===========================================
extension JobsListVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = self.jobList.count
        return shouldLoadMoreData ? count + 1 : count
        // return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isLoadingIndexPath(indexPath){
            guard let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.defaultReuseIdentifier, for: indexPath) as? LoadMoreTableViewCell else {
                fatalError("Failed to initialize LoadMoreTableViewCell")
            }
            self.isPaginationEnable = true
            return cell
        }
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "JobListCell") as! JobListCell
        
        if indexPath.row <= self.jobList.count - 1{
            
            let jobLocation = CLLocation(latitude: self.jobList[indexPath.row].latitude, longitude: self.jobList[indexPath.row].longitude)
            
                if let data = AppUserDefaults.value(forKey: .userData).dictionary, let lat = data["lat"]?.double,let lng = data["lng"]?.double{
                    if lat != 0.0{
                        let loc = CLLocation(latitude: lat, longitude: lng)
                        var distance = (loc.distance(from: jobLocation)/1000).rounded()
                        distance = (distance*0.62137).rounded()
                        print_debug(distance)
                        self.jobList[indexPath.row].distance = Int(distance)

                    }else{
                        let cllocation = CLLocation(latitude: 42.361145, longitude: -71.057083)
                        var distance = (cllocation.distance(from: jobLocation)/1000).rounded()
                        distance = (distance*0.62137).rounded()
                        print_debug(distance)
                        self.jobList[indexPath.row].distance = Int(distance)
                    }
                }else{
                    let cllocation = CLLocation(latitude: 42.361145, longitude: -71.057083)
                    var distance = (cllocation.distance(from: jobLocation)/1000).rounded()
                    distance = (distance*0.62137).rounded()
                    print_debug(distance)
                    self.jobList[indexPath.row].distance = Int(distance)
            }
            cell.populateCandidateJobDetail(self.jobList[indexPath.row])
        }
        
        cell.shareButton.addTarget(self, action: #selector(self.shareJobButtonTapped(_:)), for: .touchUpInside)
        
        cell.applyButton.addTarget(self, action: #selector(self.applyButtonTapped(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard self.shouldLoadMoreData else { return }
        
        if indexPath.row == self.jobList.count  && self.shouldLoadMoreData   && isPaginationEnable {  //for 2nd last cell
            getJobList(filterID: self.filteredCategoryId, searchText: "", sortByFilter: self.sortByFilter.rawValue, pageNo: self.pageNo, primpage: primpage, loader: false, lat: self.companyLatitude, long: companyLongtitude)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedIndex = indexPath
        let sceen = CandidateJobDetailVC.instantiate(fromAppStoryboard: .Candidate)
        sceen.jobId = self.jobList[indexPath.row].jobId
        sceen.isSaved   =  self.jobList[indexPath.row].isSaved
        sceen.distance = self.jobList[indexPath.row].distance
        self.jobList[indexPath.row].totalJobView = self.jobList[indexPath.row].totalJobView + 1
        tableView.reloadRows(at: [indexPath], with: .none)
        sceen.categoryId = self.categoryId
        sceen.delegate = self
        sceen.selectedIndex = indexPath
        sceen.jobDetail = self.jobList[indexPath.row]
        sceen.currentLocation = self.filterDataDic["coordinates"] as? CLLocation
        sceen.commingFromScreen = .jobListCell
        if let detail = self.jobList[indexPath.row].recruiterDetail {
            sceen.recruiterDetail = detail
            sceen.recruiterId = detail.recruiterId
        }
        sharedAppDelegate.navigationController?.pushViewController(sceen, animated: true)
    }
}

//MARK: - Ontap Yes Button
//=========================
extension JobsListVC:  OnTapYesButton {
    
    func yesBtnTap() {
        let obj = YourProfileStep1VC.instantiate(fromAppStoryboard: .Recruiter)
        obj.job_id = self.job_id
        obj.recruiterId = self.recruiterId
        obj.categoryId = self.categoryId
        obj.fromTabbar = false
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    func editButtonTapped() {
        //        self.pop()
    }
}


extension JobsListVC: ShouldOpenStatusPopup {
    
    func presentShortlistedPopup(jobApplyId: String,  categoryId: String) {
        
        guard let index = self.jobList.index(where: { (model) -> Bool in
            
            return model.jobId == jobApplyId
        })else{return}
        
        let indexRow = self.jobList.startIndex.distance(to: index)
        
        self.jobList[indexRow].isApplied = 1
        self.showCustomInfoWindow(false)
        UIView.setAnimationsEnabled(false)
        self.jobListTableView.reloadRows(at: [IndexPath(row: index, section: 0)], with: .none)
        UIView.setAnimationsEnabled(true)
        if fromProfile{
            getLocation()
            self.pageNo = 0
            self.primpage = 0
            self.shouldLoadMoreData = false
            self.isPaginationEnable = false
            fromProfile = false
        }
    }
}

//MARK:- DZNEmptyDataSource and Delegate
//==========================================
extension JobsListVC: DZNEmptyDataSetDelegate, DZNEmptyDataSetSource {
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return #imageLiteral(resourceName: "icHomeNoJobs")
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: StringConstants.NoJobsFound,                                                                           attributes: [NSAttributedStringKey.foregroundColor: AppColors.black46,
                                                                                                                                                              NSAttributedStringKey.font: AppFonts.Poppins_SemiBold.withSize(14)
            ])
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        return NSAttributedString(string: StringConstants.Please_try_different_location,                                                                           attributes: [NSAttributedStringKey.foregroundColor: AppColors.gray152,NSAttributedStringKey.font: AppFonts.Poppins_Regular.withSize(12)])
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}

