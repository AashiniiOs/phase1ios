//
//  SearchJobsVC.swift
//  JobGet
//
//  Created by appinventiv on 17/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import GooglePlacePicker




class SearchJobsVC: BaseVC {
    
    
    //   MARK:- enum
    //  ===================================
    enum CommingFrom {
        case jobsList
        case candidateList
    }
    
    //   MARK:- Properties
    //  ===================================
    weak var delegate: SearchCompelete?
    var userSearchResult = [String]()
    var companyLatitude: Double = 0.0
    var companyLongtitude: Double = 0.0
    var commingFrom: CommingFrom = .jobsList
    
    //        MARK:- IBOutlets
    //        ===================================
    @IBOutlet weak var searchIcon: UIImageView!
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var searchTable: UITableView!
    @IBOutlet weak var searchContainer: UIView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var searchTxtFldContainerView: UIView!
    
    //        MARK:- View lifecycle
    //        ==============================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    //        MARK:- functions
    //        ==============================
    
    private func initialSetup(){
        
        self.searchField.delegate = self
        
        if commingFrom == .jobsList {
            self.searchField.attributedPlaceholder = NSAttributedString(string: "Search", attributes: [NSAttributedStringKey.font : AppFonts.Poppins_Light.withSize(13),                                                                                                       NSAttributedStringKey.foregroundColor: AppColors.appGrey])
        } else {
            self.searchField.attributedPlaceholder = NSAttributedString(string: "Search by Name", attributes: [NSAttributedStringKey.font : AppFonts.Poppins_Light.withSize(13),                                                                                                       NSAttributedStringKey.foregroundColor: AppColors.appGrey])
        }
        
        self.searchTxtFldContainerView.setCorner(cornerRadius: 5.0, clip: true)
        
        self.searchTable.dataSource = self
        self.searchTable.delegate = self
        
        self.searchIcon.image = #imageLiteral(resourceName: "ic_search_").withRenderingMode(.alwaysTemplate)
        self.searchIcon.tintColor = AppColors.appGrey
        self.searchContainer.setShadow()
        self.searchField.returnKeyType = .done
        self.userSearchResult = AppUserDefaults.getUserSearchResult(forKey: .candidateSearchResult)
        
    }
    
    func searchData(text: String) {
        
        var searchResult = AppUserDefaults.getUserSearchResult(forKey: .candidateSearchResult)
        
        if searchResult.isEmpty {
            searchResult = [text]
        } else if searchResult.contains(text){
            searchResult.removeObject(text)
            searchResult.insert(text, at: 0)
        } else {
            searchResult.insert(text, at: 0)
        }
        
        if searchResult.count > 7 {
            searchResult.remove(at: searchResult.count - 1)
        }
        AppUserDefaults.save(value: searchResult, forKey: .candidateSearchResult)
        self.dismiss(animated: true, completion: nil)
    }
    
    //        MARK:- IBActions
    //        ==============================
    @IBAction func closeButtonTapped(_ sender: UIButton) {
        
        self.searchField.text = ""
        
    }
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)
        
    }
}

//        MARK:- Textfield delegate and search
//        ====================================
extension SearchJobsVC: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if let text = textField.text, !text.isEmpty {
            print_debug(text)
            self.searchData(text: text)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let text = textField.text, !text.isEmpty {
            print_debug(text)
            self.searchData(text: text)
        } else {
            self.dismiss(animated: true, completion: nil)
        }
        return true
    }
}

//        MARK:- Tableview datasource
//        ==============================
extension SearchJobsVC: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }else{
            return userSearchResult.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell") as! SearchCell
        if indexPath.section == 0{
            cell.locationView.isHidden = false
            
        }else{
            cell.locationView.isHidden = true
            
            cell.textLabel?.text = userSearchResult[indexPath.row]
        }
        return cell
    }
}

//        MARK:- Tableview delegate
//        ==============================
extension SearchJobsVC: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.row == 0 && AppUserDefaults.value(forKey: .userType).stringValue == UserType.recuriter.rawValue{
            return 0
        }else{
            return 50
        }
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: 45))
        view.backgroundColor = .white
        
        let recentSearchLabel = UILabel(frame: CGRect(x: 15, y: 0, width: SCREEN_WIDTH - 15, height: 45))
        
        recentSearchLabel.font = AppFonts.Poppins_SemiBold.withSize(16)
        recentSearchLabel.textColor = AppColors.black46
        if section == 0{
            recentSearchLabel.text = ""
        }else{
            recentSearchLabel.text = StringConstants.Recent_Search
        }
        
        
        view.addSubview(recentSearchLabel)
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 0.00001
        }else{
            return 45
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? SearchCell else { return }
        if indexPath.section == 0{
            
            let placePicker = SearchByMapVC.instantiate(fromAppStoryboard: .settingsAndChat)
            
            present(placePicker, animated: true, completion: nil)
        }else{
            self.searchData(text: cell.textLabel?.text ?? "")
        }
    }
}

// MARK: - Google Place Picker
//======================================
extension SearchJobsVC: GMSPlacePickerViewControllerDelegate {
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        self.companyLatitude = place.coordinate.latitude
        self.companyLongtitude = place.coordinate.longitude
        self.dismiss(animated: true, completion: nil)
        if let address = place.formattedAddress {
            self.dismiss(animated: true, completion: nil)
            
            self.searchField.text = address
            AppUserDefaults.save(value: String(describing: address), forKey: .recruiterCompanyAddress)
        }
        self.searchTable.reloadData()
        viewController.dismiss(animated: true, completion: nil)
        
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        
        print("No place selected")
    }
    
}


extension SearchJobsVC: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
    }
}



//        MARK:- SearchCell
//        ==============================
class SearchCell: UITableViewCell {
    
    
    @IBOutlet weak var locationTitle: UILabel!
    @IBOutlet weak var locationView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.initialSetUp()
    }
    
    private func initialSetUp(){
        self.textLabel?.font = AppFonts.Poppins_Regular.withSize(14)
        self.textLabel?.textColor = AppColors.appBlack
        self.locationTitle.font = AppFonts.Poppins_Regular.withSize(14)
        self.locationTitle.textColor = AppColors.gray152
    }
}
