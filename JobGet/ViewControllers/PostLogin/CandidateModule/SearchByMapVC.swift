//
//  SearchByMapVC.swift
//  JobGet
//
//  Created by Appinventiv on 19/06/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreLocation
import Alamofire
import SwiftyJSON


protocol CandidateSeacrhByLocationDelegate: class {
    func candidateSeacrhByLocationDelegate(coordinates:CLLocation?, radius: String?, address: String?)
}

struct Place {
    let id: String
    let description: String
}

class SearchByMapVC: UIViewController {
    
    enum MapOpenFrom{
        
        case candJobList
        case none
    }
    
    
    // MARK: - Outlets.
    // MARK: -
    
    @IBOutlet weak var circleViewHight: NSLayoutConstraint!
    @IBOutlet weak var circleMarkerView: SmallCircleMarker!
    @IBOutlet weak var searchTableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var blurView: CircleView!
    @IBOutlet weak var navigationTitleLabel: UIButton!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var mapView: GMSMapView!
    
    // MARK: - Properties.
    // MARK: -
    private var locationManager: CLLocationManager!
    var currentLocation: CLLocation?
    var places = [Place]()
    let apiKey = "AIzaSyDqYcQgFpIbU8KsjkHIM8RYbcRXow0-MXg"
    var placesListArray = [String]()
    var addressListArray = [String]()
    var searchBarDistanceText = ""
    weak var delegate:CandidateSeacrhByLocationDelegate?
    var radius: String?
    var isOpenFromAddressPopup = false
    var city = ""
    var state = ""
    var address = ""
    var isOpenFirstTime = false
    var mapOpenFrom = MapOpenFrom.none
    var currentZoom: Float = 9.4
    var isOpenOnce = false
    
    // MARK: - View life cycle.
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        self.searchBar.resignFirstResponder()
    }
    
    
    // MARK: - Actions.
    // MARK: -
    
    @IBAction func cancelButtonTapp(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func doneButtonTapp(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        if self.isOpenFromAddressPopup{
            if let location = self.currentLocation{
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UPDATE_LOCATION"), object: nil, userInfo: ["city" : self.city,"state" : self.state,"address" : self.address, "latitude":"\(location.coordinate.latitude)", "longitude":"\(location.coordinate.longitude)"])
            }
        }else{
            self.delegate?.candidateSeacrhByLocationDelegate(coordinates: self.currentLocation, radius: self.radius ?? "20", address: "\(self.city), \(self.state) ")
        }
    }
}

//MARK: - Private methods.
//MARK: -
extension SearchByMapVC: CLLocationManagerDelegate{
    
    func initialSetup(){
        
        if self.isOpenFromAddressPopup{
            self.currentZoom = 10.3
        }
        self.blurView = CircleView(frame: self.blurView.frame)
        self.searchTableView.delegate = self
        self.searchTableView.dataSource = self
        self.searchTableView.isHidden = true
        self.searchBar.delegate = self
        self.mapView.delegate = self
        self.searchBar.returnKeyType = .done
        self.mapView.setMinZoom(8.0, maxZoom: 13.5)
        if self.mapOpenFrom == .candJobList || !self.isOpenFirstTime{
            if let clocation = self.currentLocation{
                let location = GMSCameraPosition.camera(withLatitude: clocation.coordinate.latitude, longitude: clocation.coordinate.longitude, zoom: self.currentZoom)
                self.mapView.camera = location
               
                self.updateLocationInSearchTextField(lat: clocation.coordinate.latitude, long: clocation.coordinate.longitude)
            }else{
                let location = GMSCameraPosition.camera(withLatitude: 42.361145, longitude: -71.057083, zoom: self.currentZoom)
                self.mapView.camera = location
                self.mapView.animate(to: location)
                self.updateLocationInSearchTextField(lat: 42.361145, long: -71.057083)
            }
            
        }
//        self.mapView.isMyLocationEnabled = true
        let textFieldInsideUISearchBar = searchBar.value(forKey: "searchField") as? UITextField
        textFieldInsideUISearchBar?.font = AppFonts.Poppins_Regular.withSize(13.0)
        textFieldInsideUISearchBar?.textColor = AppColors.appBlack
//        self.mapView.settings.myLocationButton = true
        
        if self.isOpenFromAddressPopup{
            self.circleMarkerView.isHidden = false
            self.view.bringSubview(toFront: self.mapView)
            self.view.bringSubview(toFront: self.circleMarkerView)
            self.view.bringSubview(toFront: self.searchTableView)
            
        }else{
            self.circleMarkerView.isHidden = true
        }
        
        locationManager = CLLocationManager()
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        
        // Check for Location Services
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.requestWhenInUseAuthorization()
            locationManager.startUpdatingLocation()
        }
        
        let swipe = UIPanGestureRecognizer(target: self, action: #selector(self.swipeMapView(_:)))
        
        self.mapView.addGestureRecognizer(swipe)
    }
    
    @objc func swipeMapView(_ gesture: UIGestureRecognizer){
        
        print_debug(gesture.numberOfTouches)
        
    }
    
    // MARK - CLLocationManagerDelegate
    
//    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
//        defer { currentLocation = locations.last }
//        
//        if currentLocation == nil {
//            // Zoom to user location
//            if let userLocation = locations.last {
//                self.mapView.camera = GMSCameraPosition.camera(withTarget: userLocation.coordinate, zoom: 10.8)
//            }
//        }
//    }
}

//MARK: - GMSMAP Delegate
//MARK: -
extension SearchByMapVC: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        
        if self.isOpenFromAddressPopup{
            
            UIView.animate(withDuration: 1.0) {
                
                self.circleViewHight.constant = 65
                self.view.layoutIfNeeded()
            }
        }
    }
    
   
    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {
        self.view.endEditing(true)
        self.searchBar.resignFirstResponder()
        self.mapView.camera = position
        if self.isOpenOnce{
            self.updateLocationInSearchTextField(lat: position.target.latitude, long: position.target.longitude)
        }
        self.isOpenOnce = true
    }
    
    
    func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        
        return false
    }
    
    func updateLocationInSearchTextField(lat: CLLocationDegrees, long: CLLocationDegrees){
      
        if self.isOpenFromAddressPopup{
            UIView.animate(withDuration: 0.5) {
                self.circleViewHight.constant = 40
                self.view.layoutIfNeeded()
            }
        }
        
        
        let textFieldInsideUISearchBar = searchBar.value(forKey: "searchField") as? UITextField
        
        textFieldInsideUISearchBar?.textColor = AppColors.gray163
        self.searchBar.text = StringConstants.Searching_Address
        
        self.currentLocation = CLLocation(latitude: lat, longitude: long)
        
        let newCoordinate = mapView.projection.coordinate(for:  CGPoint(x: self.mapView.center.x + 140 , y: self.mapView.frame.height/2 + 70))
        let startCoordinate = CLLocation(latitude: newCoordinate.latitude, longitude: newCoordinate.longitude)
        let endCoordinate = CLLocation(latitude: lat, longitude: long)
        
        var distance = (endCoordinate.distance(from: startCoordinate)/1000).rounded()
        distance = (distance*0.62137).rounded()

        print_debug("Distance:=== \(distance)")
        self.currentLocation?.convertToPlaceMark({ (place) in
            
            textFieldInsideUISearchBar?.textColor = AppColors.appBlack
            print_debug(place.locality ?? "")
            var cityWithDistance = ""
            var dist = ""
            var distInMiles = ""
            
            if Int(distance) <= 5{
                distInMiles = "5"
            }else if Int(distance) > 5 && Int(distance) <= 10 {
                distInMiles = "10"
            }else if Int(distance) > 10 && Int(distance) <= 15 {
                distInMiles = "15"
            }else if Int(distance) > 15 && Int(distance) <= 20 {
                distInMiles = "20"
            }else if Int(distance) > 20 && Int(distance) <= 25 {
                distInMiles = "25"
            }else if Int(distance) > 25 && Int(distance) <= 30 {
                distInMiles = "30"
            }else if Int(distance) > 30 && Int(distance) <= 35 {
                distInMiles = "35"
            }else if Int(distance) > 35 && Int(distance) <= 40 {
                distInMiles = "40"
            }else if Int(distance) > 40 && Int(distance) <= 45 {
                distInMiles = "45"
            }else{
                distInMiles = "50"
            }


            self.radius = distInMiles
            
            
            dist = "  \(distInMiles) \(StringConstants.miles)"
            self.searchBarDistanceText = dist
            
            if self.isOpenFirstTime{
                cityWithDistance = "\(place.locality ?? "")  \(distInMiles) \(StringConstants.miles)"
                if let locality = place.locality{
                    textFieldInsideUISearchBar?.text = locality
                    self.city = locality
                }else{
                    textFieldInsideUISearchBar?.text = place.subAdministrativeArea ?? ""
                    self.city = place.subAdministrativeArea ?? "Boston"
                }
            }else{
                self.isOpenFirstTime = true
                cityWithDistance = "\(self.city )  \(distInMiles) \(StringConstants.miles)"
            }
            
            self.state = place.administrativeArea ?? "Massachusetts"
            if self.isOpenFromAddressPopup{
                
                self.address = ""
                if let addressDictionary = place.addressDictionary, let formattedAddressLines = addressDictionary["FormattedAddressLines"] as? [String]{
//                    var count = 0
                    
                    self.address = formattedAddressLines.joined(separator: ",")
//                    let _ = formattedAddressLines.map{ text in
//                        if count == 0{
//                            self.address.append(text)
//                        }else{
//                            self.address.append(", \(text)")
//                        }
//                        count += 1
//                    }
                }
            }else{
                textFieldInsideUISearchBar?.attributedText = CommonClass.getAttributedText(wholeString: cityWithDistance, attributedString: dist, attributedColor: AppColors.gray163, normalColor: AppColors.black46, fontSize: 13.0)
            }
            
            self.searchBar.isLoading = false
        })
    }
    
  
}

//MARK: - UISearchBar Delegate methods.
//MARK: -
extension SearchByMapVC: UISearchBarDelegate{
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        self.view.endEditing(true)
        //        self.dismiss(animated: true, completion: nil)
        //        if self.isOpenFromAddressPopup{
        //            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UPDATE_LOCATION"), object: nil, userInfo: ["city" : self.city,"state" : self.state,"address" : self.address])
        //        }else{
        //            self.delegate?.candidateSeacrhByLocationDelegate(coordinates: self.currentLocation, radius: self.radius, address: "\(self.city), \(self.state)")
        //        }
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        self.getPlaces(searchString: searchText.replace(string: " ", withString: ""))
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        if let searchText = searchBar.text{
            
            if (searchText.contains(s: self.searchBarDistanceText)){
                let text = searchText.replace(string:  self.searchBarDistanceText, withString: "")
                searchBar.text = text
                
            }
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) { [weak self] in
            self?.searchBar.selectAllText()
        }
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
        if let textFieldInsideUISearchBar = searchBar.value(forKey: "searchField") as? UITextField {
            if let searchText = searchBar.text{
                if self.isOpenFromAddressPopup{
                    textFieldInsideUISearchBar.text = searchText
                }else{
                    textFieldInsideUISearchBar.attributedText = CommonClass.getAttributedText(wholeString: "\(searchText)\(self.searchBarDistanceText)", attributedString: self.searchBarDistanceText, attributedColor: AppColors.gray163, normalColor: AppColors.black46, fontSize: 13.0)
                }
            }
        }
        
        print_debug(searchBar.text)
    }
    
    private func getPlaces(searchString: String) {
        
        
        let url = "https://maps.googleapis.com/maps/api/place/autocomplete/json?key=\(apiKey)&types=(cities)&input=\(searchString)"
        
        Alamofire.request(url, method: .get, parameters: nil, encoding: URLEncoding.queryString, headers: nil).responseJSON { (response) in
            switch(response.result) {
                
            case .success(let value):
                print_debug(value)
                if !self.placesListArray.isEmpty{
                    self.placesListArray.removeAll(keepingCapacity: false)
                }
                if !self.addressListArray.isEmpty{
                    self.addressListArray.removeAll(keepingCapacity: false)
                }
                
                if let data = value as? [String: AnyObject], let predictions = data["predictions"] as? [[String: AnyObject]]{
                    
                    if predictions.isEmpty{
                        self.searchTableView.isHidden = true
                        self.tableViewHeight.constant = 0.0
                    }
                    let _ = predictions.map{ listData in
                        
                        
                        if let description = listData["description"] as? String{
                            
                            print_debug("description :: \(description)")
                            self.addressListArray.append(description)
                        }
                        
                        if let structured_formatting = listData["structured_formatting"], let main_text = structured_formatting["main_text"] {
                            
                            var state = ""
                            if let secondary_text = structured_formatting["secondary_text"], let stateName = secondary_text as? String {
                                state = stateName.components(separatedBy: ", ").dropLast().joined(separator: "")
                            }
                            print_debug("State \(state)")
                            
                            if let cityList = main_text as? String{
                                print_debug("description :: \(cityList)")
                                if state.isEmpty{
                                     self.placesListArray.append(cityList)
                                }else{
                                     self.placesListArray.append("\(cityList), \(state)")
                                }
                               
                                
                                //                                var updatedArray = [String]()
                                //                                for list in self.placesListArray{
                                //                                    if !updatedArray.contains(list){
                                //                                        updatedArray.append(list )
                                //                                    }
                                //                                }
                                //                                self.placesListArray = updatedArray
                                //                                print_debug(updatedArray)
                                
                                self.tableViewHeight.constant = CGFloat(self.placesListArray.count*40)
                                self.searchTableView.isHidden = false
                                self.searchTableView.reloadData()
                            }else{
                                self.searchTableView.isHidden = true
                                self.tableViewHeight.constant = 0.0
                            }
                        }
                    }
                }
                
            case .failure(let e):
                print_debug(e.localizedDescription)
                self.searchTableView.isHidden = true
                self.tableViewHeight.constant = 0.0
                if (e as NSError).code == NSURLErrorNotConnectedToInternet {
                    
                    // Handle Internet Not available UI
                    NotificationCenter.default.post(name: .NotConnectedToInternet, object: nil)
                }
            }
        }
    }
    
    func getLocationFromAddress(address : String)  {
        let geocoder = CLGeocoder()
        var coordinates = CLLocationCoordinate2D()
        geocoder.geocodeAddressString(address, completionHandler: {(placemarks, error) -> Void in
            if((error) != nil){
                print("Error", error!)
            }
            if let placemark = placemarks?.first {
                coordinates = placemark.location!.coordinate
                let location = GMSCameraPosition.camera(withLatitude: coordinates.latitude, longitude: coordinates.longitude, zoom: self.currentZoom)
                self.mapView.camera = location
                self.mapView.animate(to: location)
                print_debug(coordinates)
                self.updateLocationInSearchTextField(lat: coordinates.latitude, long: coordinates.longitude)
                
            }
        })
    }
}

//MARK: - UISearchBar Delegate methods.
//MARK: -
extension SearchByMapVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.placesListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SearchPlacesCell", for: indexPath) as? SearchPlacesCell else {
            fatalError("SearchPlacesCell not found.")
        }
        cell.placesLabel.text = self.placesListArray[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.searchBar.isLoading = true
        self.view.endEditing(true)
        self.searchBar.resignFirstResponder()
        self.searchTableView.isHidden = true
        self.tableViewHeight.constant = 0.0
        print_debug(self.addressListArray[indexPath.row])
        self.getLocationFromAddress(address: self.addressListArray[indexPath.row])
    }
}

class SearchPlacesCell: UITableViewCell{
    
    @IBOutlet weak var placesLabel: UILabel!
}
