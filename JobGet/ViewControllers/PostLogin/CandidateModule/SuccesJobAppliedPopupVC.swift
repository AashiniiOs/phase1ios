//
//  SuccesJobAppliedPopupVC.swift
//  JobGet
//
//  Created by macOS on 07/05/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class SuccesJobAppliedPopupVC: BaseVC {
    
    //MARK:- Properties
    //================
    
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var popupView            : UIView!
    @IBOutlet weak var confirmationImageView: UIImageView!
    @IBOutlet weak var upperLabel           : UILabel!
    @IBOutlet weak var lowerLabel           : UILabel!
    @IBOutlet weak var okButton             : UIButton!
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 0.33, delay: 0.0, usingSpringWithDamping: 0.9 , initialSpringVelocity: 0.4, options: UIViewAnimationOptions(), animations: {
            
            self.popupView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }){ (true) in
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    //MARK:- remove popup when touch otuside of popup
    //================================================
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        let touch = touches.first!
        let point = touch.location(in: self.view) //touch.location(in: self.view)
        
        if !self.popupView.frame.contains(point) {
            self.dismissPopupViewOnTouches()
        }
    }
    
    func initialSetup() {
        
        self.upperLabel.textColor = AppColors.black26
        self.lowerLabel.textColor = AppColors.gray152
        self.okButton.setCorner(cornerRadius: 5, clip: true)
        self.okButton.backgroundColor = AppColors.themeBlueColor
        self.okButton.setTitle(StringConstants.Ok.uppercased(), for: .normal)
        self.okButton.titleLabel?.font = AppFonts.Poppins_Medium.withSize(15)
        self.popupView.setCorner(cornerRadius: 5, clip: true)
        self.popupView.transform = CGAffineTransform(scaleX: 0.000001, y: 0.000001)
        
        
    }
    
    
    func dismissPopupViewOnTouches() {
        
        UIView.animate(withDuration: 0.33, delay: 0.0, usingSpringWithDamping: 0.9 , initialSpringVelocity: 0.4, options: UIViewAnimationOptions(), animations: {
            
            self.popupView.transform = CGAffineTransform( scaleX: 0.0000001,y: 0.0000001)
        }){ (true) in
            
            self.view.removeFromSuperview()
            self.removeFromParent
        }
    }
    @IBAction func okButtonTapped(_ sender: UIButton) {
        
        self.dismissPopupViewOnTouches()
    }
}
