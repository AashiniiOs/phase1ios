//
//  AddCategoryPopupVC.swift
//  JobGet
//
//  Created by macOS on 12/06/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

protocol IsCategoryAdded: class {
    func reloadData()
    
}
class AddCategoryPopupVC: BaseVC {
    
    
    //MARK:- Properties
    //==================
    weak var delegate: IsCategoryAdded?
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var addCategoryLabel: UILabel!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var categoryNameTctField: SkyFloatingLabelTextField!
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- remove popup when touch otuside of popup
    //================================================
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        let touch = touches.first!
        let point = touch.location(in: self.view) //touch.location(in: self.view)
        
        if !self.popupView.frame.contains(point) {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
}

//MARK:- Private Methods
//=======================
private extension AddCategoryPopupVC {
    
    func initialSetup() {
        
        self.addCategoryLabel.textColor = AppColors.black26
        CommonClass.setSkyFloatingLabelTextField(textField: self.categoryNameTctField, placeholder: StringConstants.Category_Name, title: StringConstants.Category_Name)
        CommonClass.setNextButton(button: self.submitBtn)
        self.submitBtn.setTitle(StringConstants.Ok.uppercased(), for: .normal)
        self.popupView.setCorner(cornerRadius: 5, clip: true)
        self.addCategoryLabel.textColor = AppColors.themeBlueColor
    }
}

//MARK: - IB Action and Target
//===============================
extension AddCategoryPopupVC {
    
    @IBAction func submitBtnTapped(_ sender: UIButton) {
        self.addCategory(loader: true)
        self.dismiss(animated: true, completion: nil)
    }
}

//MARK: - Web services
//======================
extension AddCategoryPopupVC {
    
    func addCategory(loader: Bool) {
        
        guard let categoryName = self.categoryNameTctField.text else { return }
        
        let params = ["categoryTitle": categoryName]
        WebServices.addJobCategory(parameters: params, loader: loader, success: { (json) in
            self.delegate?.reloadData()
        }) { (error) in
            CommonClass.showToast(msg: error.localizedDescription)
        }
    }
}
