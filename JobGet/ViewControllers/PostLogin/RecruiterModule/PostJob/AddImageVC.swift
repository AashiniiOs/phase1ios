//
//  AddImageVC.swift
//  JobGet
//
//  Created by macOS on 05/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class AddImageVC: BaseVC {
    
    //MARK:- Properties
    //==================
    var imageToBeUploaded : UIImage?
    var imageUrl = ""
    
    //MARK:- IBoutlets
    //================
    
    @IBOutlet weak var cameraImageView: UIImageView!
    @IBOutlet weak var addImageLabel: UILabel!
    @IBOutlet weak var jobImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var imageContainerView: UIView!
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
//MARK:- Private Methods
//=======================
private extension AddImageVC {
    
    func initialSetup() {
        
        self.jobImageView.setCorner(cornerRadius: 5, clip: true)
        let attributedAddImageString: NSMutableAttributedString = NSMutableAttributedString(string: StringConstants.AddImage )
        attributedAddImageString.setColorForText(textForAttribute: "(Optional)", withColor: AppColors.gray152, font: AppFonts.Poppins_Light.withSize(15))
        self.addImageLabel.attributedText = attributedAddImageString
        self.descriptionLabel.textColor = AppColors.gray152
        self.jobImageView.backgroundColor = AppColors.gray163
        
        
        self.populateData()
    }
    
    
    func populateData() {
        
        if let jobImg = SharedJobPost.rawJobImage {
            self.jobImageView.image = jobImg
            self.cameraImageView.isHidden = true
            self.jobImageView.contentMode = .scaleAspectFill
        }
    }
    
    func uploadImage(image : UIImage) {
//        let activityLoader = __Loader(frame: self.imageContainerView.frame)
//        activityLoader.start()
        AppNetworking.showUploadImageLoader()
        image.uploadImageToS3( success: { (uploaded : Bool, imageUrl : String) in
            if uploaded {
                SharedJobPost.jobImage = imageUrl
//                activityLoader.stop()
                 AppNetworking.hideLoader()
            }
        }, progress: { (uploadPercentage : CGFloat) in
            print_debug(uploadPercentage)
            
        }, failure: { (err : Error) in
            
            if (err.localizedDescription  == StringConstants.Internet_connection_appears_offline) {
                CommonClass.showToast(msg: StringConstants.Please_check_your_internet_connection)
            }
            SharedJobPost.jobImage = ""
            AppNetworking.hideLoader()
            //activityLoader.stop()
        })
    }
}

//MARK: - IB Action and Target
//===============================
extension AddImageVC {
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func nestBtnTapped(_ sender: UIButton) {
        let sceen = PreviewJobsVC.instantiate(fromAppStoryboard: .PostJob)
        self.navigationController?.pushViewController(sceen, animated: true)

    }
    
    @IBAction func pickImageButtonTapped(_ sender: UIButton) {
        self.captureImage(on: self)
        
    }
}

//MARK:- ImagePickerController & NavigationController Delegate
//============================================================
extension AddImageVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let imagePicked = info[UIImagePickerControllerOriginalImage] as? UIImage{
            let frameRect = CGRect(x: 50, y: self.view.center.y, width: self.view.frame.width - 100, height: 180)
            Cropper.shared.openCropper(withImage: imagePicked, mode: .custom(frameRect), on: self)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

//MARK: Extension AppInventivCropperDelegate
//MARK:
extension AddImageVC: CropperDelegate {
    
    
    func imageCropperDidCancelCrop() {
        
        print_debug("Crop cancelled")
        
    }
    func imageCropper(didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect) {
        
        self.jobImageView.contentMode = .scaleAspectFill
        self.jobImageView.image = croppedImage
        self.imageToBeUploaded = croppedImage
        SharedJobPost.rawJobImage = croppedImage
        self.cameraImageView.isHidden = true
        self.uploadImage(image: croppedImage)
    }
}

