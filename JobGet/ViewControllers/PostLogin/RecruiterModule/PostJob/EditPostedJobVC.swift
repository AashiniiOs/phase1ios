//
//  EditPostedJobVC.swift
//  JobGet
//
//  Created by macOS on 20/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import GooglePlaces
import GoogleMaps
import GooglePlacePicker
import CircularSlider

protocol UpdatedJobDetail: class {
    
    func getUpdatedJobDetail(jobDeatil: MyJobListModel?)
}

class EditPostedJobVC: BaseVC {
    
    //MARK:- Properties
    //==================
    var jobCategoryData = [JobCategory]()
    var jobDetail: MyJobListModel?
    weak var delegate: UpdatedJobDetail?
    var editImageUrl: String?
    var categoryName = [String]()
    var categoryId   = [String]()
    var pickedImage : UIImage?
    var isJobLocationTextFieldEditingEnable = true
    let hourData = ["",StringConstants.Per_Hour, StringConstants.Per_Week, StringConstants.Per_Month, StringConstants.Per_Year]
    var monthArray = [String]()
    var yearArray   = [String]()
    var experianceArray = [String]()
    var reason = ""
    var isFirstTimeTVIncreaseHeight = true
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var editJobTableView: UITableView!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var saveJobBtn: UIButton!
    
    @IBOutlet weak var hiredOnJobGetButton: UIButton!
    
    @IBOutlet weak var noLongerHiringButton: UIButton!
    @IBOutlet weak var hiredOtherPlatformButton: UIButton!
    @IBOutlet weak var closeJobPopUpView: UIView!
    
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func removePopupButtonTapp(_ sender: UIButton) {
        self.closeJobPopUpView.isHidden = true
    }
    
    @IBAction func doneButtonTapp(_ sender: UIButton) {
        if self.self.reason.isEmpty{
            CommonClass.showToast(msg: StringConstants.Please_select_reason_first)
            return
        }
        deletePostedJob()
    }
    
    @IBAction func hiringJobGetButtonTapp(_ sender: UIButton) {
        self.hiredOnJobGetButton.isSelected = true
        self.hiredOtherPlatformButton.isSelected = false
        self.noLongerHiringButton.isSelected = false
        self.reason = "1"
    }
    
    @IBAction func hiringOnAnotherButtonTapp(_ sender: UIButton) {
        self.hiredOnJobGetButton.isSelected = false
        self.hiredOtherPlatformButton.isSelected = true
        self.noLongerHiringButton.isSelected = false
        self.reason = "2"
    }
    
    @IBAction func noLongerHiringButtonTapp(_ sender: UIButton) {
        self.hiredOnJobGetButton.isSelected = false
        self.hiredOtherPlatformButton.isSelected = false
        self.noLongerHiringButton.isSelected = true
        self.reason = "3"
    }
}

//MARK:- Private Methods
//=======================
private extension EditPostedJobVC {
    
    func initialSetup() {
        
        self.editJobTableView.delegate = self
        self.editJobTableView.dataSource = self
        self.navigationView.setShadow(offset: CGSize(width: 0.0, height: 2.0))
        self.getCategoryList()
        
        if self.jobDetail?.deleteStatus == 3 {
            self.saveJobBtn.isHidden = true
        } else {
            self.saveJobBtn.isHidden = false
        }
        self.closeJobPopUpView.isHidden = true
        registerCell()
        
    }
    func registerCell() {
        self.editJobTableView.register(UINib(nibName: "SubmitButtonCell", bundle: nil), forCellReuseIdentifier: "SubmitButtonCell")
    }
    
    
    func validateData() -> Bool {
        guard let data = self.jobDetail else { return false }
        
        let toSalary = Double(data.salaryTo.replacingOccurrences(of: ",", with: "")) ?? 0.0
        let fromSalary = Double(data.salaryFrom.replacingOccurrences(of: ",", with: "")) ?? 0.0
        
     
        if data.jobTitle.isEmpty {
            CommonClass.showToast(msg: StringConstants.Please_Enter_Job_Title)
            return false
        } else if data.jobDescription.isEmpty{
            CommonClass.showToast(msg: StringConstants.Please_Enter_Job_Description)
            return false
        } else if data.salaryFrom.isEmpty && data.salaryTo.isEmpty {
            return true
            
        } else if !data.salaryTo.isEmpty && data.salaryFrom.isEmpty {
            CommonClass.showToast(msg: StringConstants.Please_Enter_From_Salary)
            return false
        } else if fromSalary > toSalary {
            CommonClass.showToast(msg: StringConstants.To_Salary_Greater_Then_From_Salary)
            return false
        }else if data.salaryFrom.isEmpty && data.salaryFrom.isEmpty && !data.perHour.isEmpty{
            CommonClass.showToast(msg: StringConstants.Please_Enter_From_Salary)
            
            return false
        } else if !data.salaryTo.isEmpty && !data.salaryFrom.isEmpty && data.perHour.isEmpty{
            CommonClass.showToast(msg: StringConstants.Please_select_duration)
            
            return false
        } else if data.companyName.isEmpty {
            CommonClass.showToast(msg: StringConstants.Please_Enter_Company_Name)
            return false
        }  else {
            return true
        }
        
    }
    
    private func uploadImage(image : UIImage) {
//        loader.start()
        AppNetworking.showUploadImageLoader()
        image.uploadImageToS3( success: { (uploaded : Bool, imageUrl : String) in
            
            if uploaded {
               AppNetworking.hideLoader()
                self.editImageUrl = imageUrl
            }
        }, progress: { (uploadPercentage : CGFloat) in
            print_debug(uploadPercentage)
            
        }, failure: { (err : Error) in
            
            if (err.localizedDescription  == StringConstants.Internet_connection_appears_offline) {
                CommonClass.showToast(msg: StringConstants.Please_check_your_internet_connection)
            }
           AppNetworking.hideLoader()
        })
    }
}


//MARK: - IB Action and Target
//===============================
extension EditPostedJobVC {
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func checkButtonTapped(_ sender: UIButton) {
        
        self.closeJobPopUpView.isHidden = false
    }
    
    @objc func onTappedSelectImage(_ sender: UIButton) {
        self.captureImage(on: self)
    }
    
    @objc func fullTimeJobBtnTapped(_ sender: UIButton) {
        guard let idx = sender.tableViewIndexPath(self.editJobTableView) else { return }
        guard let cell = self.editJobTableView.cellForRow(at: idx) as? JobTypeCell else { return }
        
        self.jobDetail?.jobType = 1
        sender.isSelected = true
        cell.partTimeButton.isSelected = false
        cell.bothButton.isSelected = false
    }
    
    @objc func partTimeJobBtnTapped(_ sender: UIButton) {
        guard let idx = sender.tableViewIndexPath(self.editJobTableView) else { return }
        guard let cell = self.editJobTableView.cellForRow(at: idx) as? JobTypeCell else { return }
        
        self.jobDetail?.jobType = 2
        sender.isSelected = true
        cell.fullTimeButton.isSelected = false
        cell.bothButton.isSelected = false
    }
    
    @objc func tipsYesBtnTappped(_ sender: UIButton) {
        guard let idx = sender.tableViewIndexPath(self.editJobTableView) else { return }
        guard let cell = self.editJobTableView.cellForRow(at: idx) as? JobTypeCell else { return }
        
        self.jobDetail?.isCommision = 1
        sender.isSelected = true
        cell.partTimeButton.isSelected = false
        
    }
    
    @objc func tipsNoBtnTappped(_ sender: UIButton) {
        guard let idx = sender.tableViewIndexPath(self.editJobTableView) else { return }
        guard let cell = self.editJobTableView.cellForRow(at: idx) as? JobTypeCell else { return }
        
        self.jobDetail?.isCommision = 0
        sender.isSelected = true
        cell.fullTimeButton.isSelected = false
        
    }
    
    @objc func bothJobBtnTapped(_ sender: UIButton) {
        guard let idx = sender.tableViewIndexPath(self.editJobTableView) else { return }
        guard let cell = self.editJobTableView.cellForRow(at: idx) as? JobTypeCell else { return }
        
        self.jobDetail?.jobType = 3
        sender.isSelected = true
        cell.partTimeButton.isSelected = false
        cell.fullTimeButton.isSelected = false
    }
    
    @objc func closeJobBtnTapped(_ sender: UIButton) {
        
        if self.validateData() {
            self.editPostedJob(loader: true)
        }
    }
}

extension EditPostedJobVC: AlertPopUpDelegate {
    
    func didTapAffirmativeButton() {
        self.deletePostedJob()
    }
    
    func didTapNegativeButton() {
        self.dismiss(animated: true, completion: nil)
    }
    
    
}

//MARK:- Web Services
//====================
extension EditPostedJobVC {
    
    func editPostedJob(loader: Bool) {
        guard let data = self.jobDetail else { return }
        var isExp : Int
        if data.experienceType == 0{
            isExp = 0
        }else{
            isExp = 1
        }
        
        let param = ["jobId" : data.jobId,
                     "jobTitle": data.jobTitle,
                     "jobDesc": data.jobDescription,
                     "jobImage": self.editImageUrl ?? data.jobImage,
                     "address": data.jobAddress,
                     "latitude": data.latitude,
                     "longitude": data.longitude,
                     "jobType": data.jobType,
                     "salaryFrom": data.salaryFrom,
                     "salaryTo": data.salaryTo,
                     "totalExperience": data.totalExperience,
                     "experienceType": data.experienceType,
                     "isExp": isExp,
                     "duration" : data.perHour,
                     "extraCompensation": data.extraCompensation,
                     "companyName": data.companyName,
                     "companyDesc": data.companyDesc,
                     "planId": data.planId,
                     "categoryId": data.categoryId,
                     "categoryName": data.categoryName,
                     "companyAddress": data.companyAddress,
                     "companyWebsite": data.companyWebsite,
                     "jobState": data.jobState,
                     "isCommision": data.isCommision,
                     "jobCity": data.jobCity
            
            ] as [String: Any]
        WebServices.editJobDetail(parameters: param, loader: loader, success: { (json) in
            if let code = json["code"].int, code == error_codes.success {
                self.jobDetail = MyJobListModel(dict: json["data"])
                self.pop()
                self.delegate?.getUpdatedJobDetail(jobDeatil: self.jobDetail)
            } else {
                let message = json["message"].stringValue
                CommonClass.showToast(msg: message)
            }
        }) { (error: Error) in
            CommonClass.showToast(msg: error.localizedDescription )
        }
    }
    
    func deletePostedJob() {
        guard let data = self.jobDetail else { return }
        let param = ["jobId": data.jobId ]
        WebServices.deletePostedJob(parameters: param , success: { (json) in
            if let code = json["code"].int, code == error_codes.success {
                let msg = json["message"].stringValue
                self.pop()
                CommonClass.showToast(msg: msg)
            } else {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }) { (error: Error) in
            CommonClass.showToast(msg: error.localizedDescription )
        }
    }
    
    func getCategoryList() {
        let param = ["": ""]
        WebServices.getCategoryList(parameters: param,loader: false, success: { (json) in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                let data = json["data"].arrayValue
                for categoryData in data {
                    self.jobCategoryData.append(JobCategory(dict: categoryData))
                    self.categoryName.append(categoryData["categoryTitle"].stringValue)
                    self.categoryId.append(categoryData["categoryImage"].stringValue)
                }
            }
        }) { (error) in
            print_debug(error)
        }
    }
}

//MARK: - Table view Delegate and DataSource
//============================================
extension EditPostedJobVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 13
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "AddImageCell") as? AddImageCell else { fatalError("invalid cell \(self)")
            }
            cell.jobImageView.contentMode = .scaleAspectFill
            if let pickImage = self.pickedImage {
                cell.jobImageView.image = pickImage
            } else if let image = self.jobDetail?.jobImage {
                cell.jobImageView.sd_setImage(with: URL(string: image), placeholderImage: nil)
            }
            if self.jobDetail?.deleteStatus != 3 {
                cell.choseImageButton.addTarget(self, action: #selector(self.onTappedSelectImage(_:)), for: .touchUpInside)
            }
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "JobTitleTextFieldCell") as? JobTitleTextFieldCell else { fatalError("invalid cell \(self)")
            }
            CommonClass.setSkyFloatingLabelTextField(textField: cell.jobTitleTextField, placeholder: StringConstants.JobTitle, title: StringConstants.JobTitle)
            cell.jobTitleTextField.rightView = nil
            if self.jobDetail?.deleteStatus != 3 {
                cell.jobTitleTextField.delegate = self
            } else {
                cell.jobTitleTextField.isUserInteractionEnabled = false
            }
            cell.jobTitleTextField.text = self.jobDetail?.jobTitle
            return cell
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "JobTitleTextFieldCell") as? JobTitleTextFieldCell else { fatalError("invalid cell \(self)")
            }
            CommonClass.setSkyFloatingLabelTextField(textField: cell.jobTitleTextField, placeholder: StringConstants.Job_Category, title: StringConstants.Job_Category)
            let button = UIButton()
            cell.jobTitleTextField.setButtonRightView(btn: button, selectedImage: #imageLiteral(resourceName: "icCreateProfileArrow"), normalImage: #imageLiteral(resourceName: "icCreateProfileArrow"), size: nil)
            cell.jobTitleTextField.text = self.jobDetail?.categoryName
            if self.jobDetail?.deleteStatus != 3 {
                cell.jobTitleTextField.delegate = self
            } else {
                cell.jobTitleTextField.isUserInteractionEnabled = false
            }
            button.addTarget(self, action: #selector(self.categoryScreenBtnTapp(_:)), for: .touchUpInside)
            return cell
        case 3:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DescriptionCell") as? DescriptionCell else { fatalError("invalid cell \(self)")
            }
            cell.descriptionTextView.text = jobDetail?.jobDescription ?? ""
            
            if self.jobDetail?.deleteStatus != 3 {
                cell.descriptionTextView.delegate = self
            } else {
                cell.descriptionTextView.isEditable = false
            }
            CommonClass.setTLFoatTextView(textView: cell.descriptionTextView, title: "Job Description")
            self.textViewHeightChange(cell.textViewHeight,textView: cell.descriptionTextView)
            return cell
        case 4:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "JobTypeCell") as? JobTypeCell else { fatalError("invalid cell \(self)")
            }
            let jobType = jobDetail?.jobType
            
            if jobType == 1 {
                cell.fullTimeButton.isSelected = true
            } else if jobType == 2 {
                cell.partTimeButton.isSelected = true
            } else  {
                cell.bothButton.isSelected = true
            }
            if self.jobDetail?.deleteStatus != 3 {
                cell.fullTimeButton.addTarget(self, action: #selector(self.fullTimeJobBtnTapped(_:)), for: .touchUpInside)
                cell.partTimeButton.addTarget(self, action: #selector(self.partTimeJobBtnTapped(_:)), for: .touchUpInside)
                cell.bothButton.addTarget(self, action: #selector(self.bothJobBtnTapped(_:)), for: .touchUpInside)
            }
            return cell
            
        case 5:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "JobTitleTextFieldCell") as? JobTitleTextFieldCell else { fatalError("invalid cell \(self)")
            }
            CommonClass.setSkyFloatingLabelTextField(textField: cell.jobTitleTextField, placeholder: StringConstants.Job_Location, title: StringConstants.Job_Location)
            let button = UIButton()
            if self.jobDetail?.deleteStatus != 3 {
                cell.jobTitleTextField.delegate = self
            } else {
                cell.jobTitleTextField.isUserInteractionEnabled = false
            }
            cell.jobTitleTextField.setButtonRightView(btn: button, selectedImage: #imageLiteral(resourceName: "icHomeDetailLocation"), normalImage: #imageLiteral(resourceName: "icHomeDetailLocation"), size: nil)
            cell.jobTitleTextField.text = "\(jobDetail?.jobAddress ?? "")"
            return cell
            
        case 6:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SalaryCell") as? SalaryCell else { fatalError("invalid cell \(self)")
            }
            if let jobDetail = self.jobDetail {
                cell.fromTextField.text = jobDetail.salaryFrom
                cell.toTextField.text   = jobDetail.salaryTo
                cell.perHourTextField.text = jobDetail.perHour
            }
            if self.jobDetail?.deleteStatus != 3 {
                cell.toTextField.delegate    = self
                cell.perHourTextField.delegate = self
                cell.fromTextField.delegate = self
            } else {
                cell.toTextField.isUserInteractionEnabled = false
                cell.perHourTextField.isUserInteractionEnabled = false
                cell.fromTextField.isUserInteractionEnabled = false
            }
            return cell
        case 7:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "JobTypeCell") as? JobTypeCell else { fatalError("invalid cell \(self)")
            }
            cell.jobTypeLabel.text = StringConstants.TipsCommessions
            cell.fullTimeButton.setTitle(StringConstants.yes, for: .normal)
            cell.partTimeButton.setTitle(StringConstants.no, for: .normal)
            cell.bothButton.setTitle(nil, for: .normal)
            cell.bothButton.setImage(nil, for: .normal)
            let commesion = jobDetail?.isCommision
            if commesion == 1 {
                cell.fullTimeButton.isSelected = true
            } else  {
                cell.partTimeButton.isSelected = true
            }
            if self.jobDetail?.deleteStatus != 3 {
                cell.fullTimeButton.addTarget(self, action: #selector(self.tipsYesBtnTappped(_:)), for: .touchUpInside)
                cell.partTimeButton.addTarget(self, action: #selector(self.tipsNoBtnTappped(_:)), for: .touchUpInside)
            }
            return cell
        case 8:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "JobTitleTextFieldCell") as? JobTitleTextFieldCell else { fatalError("invalid cell \(self)")
            }
            cell.jobTitleTextField.rightView = nil
            CommonClass.setSkyFloatingLabelTextField(textField: cell.jobTitleTextField, placeholder: StringConstants.Experienc, title: StringConstants.Experienc)
            if jobDetail?.experienceType == 1 {
                cell.jobTitleTextField.text = "\(StringConstants.Min) \(jobDetail?.totalExperience ?? "1") \(StringConstants.Months)"
            }else if jobDetail?.experienceType == 2 {
                cell.jobTitleTextField.text = "\(StringConstants.Min) \(jobDetail?.totalExperience ?? "1") \(StringConstants.Years)"
            } else {
                cell.jobTitleTextField.text = StringConstants.NoExpRequired
            }
            if self.jobDetail?.deleteStatus != 3 {
                cell.jobTitleTextField.delegate = self
            } else {
                cell.jobTitleTextField.isUserInteractionEnabled = false
            }
            return cell
            
        case 9:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "JobTitleTextFieldCell") as? JobTitleTextFieldCell else { fatalError("invalid cell \(self)")
            }
            cell.jobTitleTextField.rightView = nil
            CommonClass.setSkyFloatingLabelTextField(textField: cell.jobTitleTextField, placeholder: StringConstants.Company_Name, title: StringConstants.Company_Name)
            cell.jobTitleTextField.text = jobDetail?.companyName  ?? ""
            if self.jobDetail?.deleteStatus != 3 {
                cell.jobTitleTextField.delegate = self
            } else {
                cell.jobTitleTextField.isUserInteractionEnabled = false
            }
            return cell
            
        case 10:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SubmitButtonCell") as? SubmitButtonCell else { fatalError("invalid cell \(self)")
            }
            cell.setButton(status: self.jobDetail?.deleteStatus ?? 0)
            cell.submitButton.addTarget(self, action: #selector(self.closeJobBtnTapped(_:)), for: .touchUpInside)
            return cell
            
        case 11:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "JobTitleTextFieldCell") as? JobTitleTextFieldCell else { fatalError("invalid cell \(self)")
            }
            CommonClass.setSkyFloatingLabelTextField(textField: cell.jobTitleTextField, placeholder: StringConstants.Company_Address, title: StringConstants.Company_Address)
            if self.jobDetail?.deleteStatus != 3 {
                cell.jobTitleTextField.delegate = self
            }
            cell.jobTitleTextField.text = self.jobDetail?.companyAddress
            let button = UIButton()
            
            cell.jobTitleTextField.setButtonRightView(btn: button, selectedImage: #imageLiteral(resourceName: "icHomeDetailLocation"), normalImage: #imageLiteral(resourceName: "icHomeDetailLocation"), size: nil)
            
            return cell
            
        case 12:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "JobTitleTextFieldCell") as? JobTitleTextFieldCell else { fatalError("invalid cell \(self)")
            }
            CommonClass.setSkyFloatingLabelTextField(textField: cell.jobTitleTextField, placeholder: StringConstants.Company_Website , title: StringConstants.Company_Website)
            
            if self.jobDetail?.deleteStatus != 3 {
                cell.jobTitleTextField.delegate = self
                
            }
            cell.jobTitleTextField.text = self.jobDetail?.companyWebsite
            return cell
        default:
            return UITableViewCell()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
        case 11,12:
            
            return 0
        default:
            return UITableViewAutomaticDimension
        }
    }
    
    @objc func categoryScreenBtnTapp(_ sender: UIButton){
        
        guard let cell = sender.tableViewCell as? JobTitleTextFieldCell else { return }
        
        let categoryVC = JobCategoryVC.instantiate(fromAppStoryboard: .PostJob)
        categoryVC.currentCategory = cell.jobTitleTextField.text ?? ""
        categoryVC.jobCategoryData = self.jobCategoryData
        categoryVC.fromEditProfile = true
        categoryVC.editCategoryDelegate = self
        self.navigationController?.pushViewController(categoryVC, animated: true)
        
    }
}

//MARK: - TextField Delegate
//==============================
extension EditPostedJobVC: UITextFieldDelegate {
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        guard let index = textField.tableViewIndexPath(self.editJobTableView) else { return  true}
        print_debug(index)
        if index.row == 5 || index.row == 11 {
            index.row == 5 ? (self.isJobLocationTextFieldEditingEnable = true) : (self.isJobLocationTextFieldEditingEnable = false)
            guard let data = self.jobDetail else{return false}
            
            let northEast = CLLocationCoordinate2DMake(data.latitude + 0.001, data.longitude + 0.001)
            let southWest = CLLocationCoordinate2DMake(data.latitude - 0.001, data.longitude - 0.001)
            let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
            let config = GMSPlacePickerConfig(viewport: viewport)
            
            let placePicker = GMSPlacePickerViewController(config: config)
            placePicker.delegate = self
            present(placePicker, animated: true, completion: nil)
            
            return false
        } else if index.row == 8 {
            
            let popupVC = ChooseExpPopupVC.instantiate(fromAppStoryboard: .Recruiter)
            
            
            if jobDetail?.experienceType == 1 {
                popupVC.monthExp = Int(jobDetail?.totalExperience ?? "1")
                
            }else if jobDetail?.experienceType == 2 {
                popupVC.yearExp = Float(jobDetail?.totalExperience ?? "1")
                
            }
            popupVC.delegate = self
            
            popupVC.modalPresentationStyle = .overCurrentContext
            popupVC.modalTransitionStyle = .crossDissolve
            self.present(popupVC, animated: true, completion: nil)
            return false
            
        }  else if index.row == 2 {
            guard let cell = self.editJobTableView.cellForRow(at: index) as? JobTitleTextFieldCell else { return false }
            
            let categoryVC = JobCategoryVC.instantiate(fromAppStoryboard: .PostJob)
            categoryVC.currentCategory = cell.jobTitleTextField.text ?? ""
            categoryVC.jobCategoryData = self.jobCategoryData
            categoryVC.fromEditProfile = true
            categoryVC.editCategoryDelegate = self
            self.navigationController?.pushViewController(categoryVC, animated: true)
            
            return false
        }
            
        else {
            return true
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        guard let index = textField.tableViewIndexPath(self.editJobTableView) else { return }
        
        print_debug(index)
        if index.row == 6 {
            guard let cell = self.editJobTableView.cellForRow(at: index) as? SalaryCell else { return }
            
            if textField === cell.perHourTextField {
                MultiPicker.noOfComponent = 1
                MultiPicker.openMultiPickerIn(textField, firstComponentArray: self.hourData, secondComponentArray: [""], firstComponent: "", secondComponent: nil, titles: nil) { (data, response, index) in
                    cell.perHourTextField.text = data
                    self.jobDetail?.perHour  = data
                    print_debug(data)
                    print_debug(response)
                    print_debug(index)
                }
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        guard let index = textField.tableViewIndexPath(self.editJobTableView) else { return }
        print_debug(index)
        
        switch index.row  {
        case 1:
            self.jobDetail?.jobTitle = textField.text ?? ""
        case 5:
            self.jobDetail?.jobAddress = textField.text ?? ""
            
        case 6:
            guard let cell = self.editJobTableView.cellForRow(at: index) as? SalaryCell else { return }
            
            if cell.fromTextField === textField {
                self.jobDetail?.salaryFrom = textField.text ?? ""
            } else if cell.toTextField === textField {
                self.jobDetail?.salaryTo = textField.text  ?? ""
            }  else if cell.perHourTextField === textField {
                self.jobDetail?.perHour = textField.text  ?? ""
            }
            
        case 9:
            self.jobDetail?.companyName = textField.text  ?? ""
        case 11:
            self.jobDetail?.companyAddress = textField.text ?? ""
            
        case 12:
            self.jobDetail?.companyWebsite = textField.text ?? ""
        default:
            return
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let index = textField.tableViewIndexPath(self.editJobTableView) else { return false }
        guard let cell = self.editJobTableView.cellForRow(at: index) as? SalaryCell else { return  true}
        
        if textField == cell.fromTextField || textField == cell.toTextField {
            
            let formatter = NumberFormatter()
            formatter.numberStyle = .decimal
            formatter.locale = Locale.current
            formatter.maximumFractionDigits = 2
            
            // Uses the grouping separator corresponding to your Locale
            // e.g. "," in the US, a space in France, and so on
            if let groupingSeparator = formatter.groupingSeparator {
                
                if string == groupingSeparator {
                    return true
                }
                
                if let textWithoutGroupingSeparator = textField.text?.replacingOccurrences(of: groupingSeparator, with: "") {
                    var totalTextWithoutGroupingSeparators = textWithoutGroupingSeparator + string
                    if string == "" { // pressed Backspace key
                        totalTextWithoutGroupingSeparators.removeLast()
                        return true
                    }
                    if let numberWithoutGroupingSeparator = formatter.number(from: totalTextWithoutGroupingSeparators),
                        let formattedText = formatter.string(from: numberWithoutGroupingSeparator), (textField.text ?? "").count - range.length < 8 , string != "." {
                        
                        textField.text = formattedText
                        return false
                    }  else {
                        
                        if (textField.text ?? "").count - range.length < 8 {
                            return true
                        } else {
                            return false
                        }
                    }
                }
            }
            return true
        } else {
            return true
        }
    }
}

//MARK: - GetEditedCategory
//==============================
extension EditPostedJobVC: GetEditedCategory {
    
    func editedCategory(withName categoryName: String, withId categoryId: String) {
        self.jobDetail?.categoryName  = categoryName
        self.jobDetail?.categoryId   = categoryId
        self.editJobTableView.reloadData()
    }
    
}

//MARK: - TextView Delegate
//==============================
extension EditPostedJobVC : UITextViewDelegate {
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        self.isFirstTimeTVIncreaseHeight = false
        guard let cell = textView.tableViewCell as? DescriptionCell else{return false}
        cell.seperatorView.backgroundColor = AppColors.themeBlueColor
        
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        guard let index = textView.tableViewIndexPath(self.editJobTableView) else { return }
        
        switch index.row {
        case 3:
            guard let cell = textView.tableViewCell as? DescriptionCell else{return}
            cell.seperatorView.backgroundColor = AppColors.blakopacity15
            self.jobDetail?.jobDescription = textView.text
            
        case 10:
            self.jobDetail?.companyDesc = textView.text
        default:
            return
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
        UIView.setAnimationsEnabled(false)
        self.editJobTableView.beginUpdates()
        guard let cell = textView.tableViewCell as? DescriptionCell else{return}
        self.textViewHeightChange(cell.textViewHeight,textView: textView)
        self.editJobTableView.endUpdates()
        UIView.setAnimationsEnabled(true)
    }
    
    func textViewHeightChange(_ textViewHeight: NSLayoutConstraint, textView: UITextView){
        
        self.editJobTableView.beginUpdates()
        
        let topBottomPadding: CGFloat = 20.0
        let threeLineTextHeight: CGFloat = 95.0
        let fixedWidth = textView.frame.size.width
        let fixedHeight = textView.frame.size.height
        
        let maximumTextHeight: CGFloat = threeLineTextHeight + topBottomPadding
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: fixedHeight))
        let textHeight = textView.text.heightWithConstrainedWidth(width: textView.width-10.0, font: textView.font!)
        if textHeight > threeLineTextHeight {
            textView.isScrollEnabled = true
            textViewHeight.constant = maximumTextHeight
            
        } else {
            textView.isScrollEnabled = false
            
            if textView.text.isEmpty{
                textViewHeight.constant = 50
                textView.frame.size = CGSize(width: max(newSize.width, fixedWidth), height:50)
            }else{
                textView.frame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
                if self.isFirstTimeTVIncreaseHeight{
                    textViewHeight.constant = newSize.height + 20
                }else{
                    textViewHeight.constant = newSize.height
                }
                
            }
        }
        
        self.view.layoutIfNeeded()
        self.editJobTableView.endUpdates()
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        print_debug(newText.count)
        
        if text == ""{
            return true
        }
        if newText.count > 2000{
            showAlert(msg:  StringConstants.job_decription_text_limit)
        }
        return newText.count < 2000
    }
    
}

// MARK: - Google Place Picker
//======================================
extension EditPostedJobVC: GMSPlacePickerViewControllerDelegate {
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        
        if self.isJobLocationTextFieldEditingEnable {
            self.jobDetail?.jobAddress = place.formattedAddress ?? ""
            self.jobDetail?.longitude  = place.coordinate.longitude
            self.jobDetail?.latitude   = place.coordinate.latitude
            self.editJobTableView.reloadRows(at: [IndexPath(row: 5, section: 0)], with: .automatic)
        } else {
            self.jobDetail?.companyAddress = place.formattedAddress ?? ""
            self.jobDetail?.companyLongitude  = place.coordinate.longitude
            self.jobDetail?.companyLatitude   = place.coordinate.latitude
            self.editJobTableView.reloadRows(at: [IndexPath(row: 11, section: 0)], with: .automatic)
        }
        self.reverseGeoCode(lattitude: place.coordinate.latitude, longitude: place.coordinate.longitude)
        viewController.dismiss(animated: true, completion: nil)
        
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        
        print("No place selected")
    }
    
}

// MARK:- Reverce Geo Code
//======================================
extension EditPostedJobVC {
    
    
    func reverseGeoCode(lattitude: Double, longitude: Double)  {
        let geocoder = GMSGeocoder()
        let coordinate = CLLocationCoordinate2DMake(Double(lattitude), Double(longitude))
        
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            
            if let location = response?.firstResult() {
                
                var thoroughfar = ""
                var localit = ""
                var administrativeAre = ""
                if let thoroughfare = location.thoroughfare {
                    thoroughfar = thoroughfare
                }
                
                if let locality = location.locality{
                    localit = locality
                }else if let administrativeArea = location.administrativeArea{
                    localit = administrativeArea
                }
                
                if let administrativeArea = location.administrativeArea{
                    administrativeAre = administrativeArea
                }
                self.jobDetail?.jobCity = localit
                self.jobDetail?.jobState = administrativeAre
            }
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}



//MARK:- ImagePickerController & NavigationController Delegate
//============================================================
extension EditPostedJobVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let imagePicked = info[UIImagePickerControllerOriginalImage] as? UIImage {
            let frameRect = CGRect(x: 50, y: self.view.center.y, width: self.view.frame.width - 100, height: 180)
            Cropper.shared.openCropper(withImage: imagePicked, mode: .custom(frameRect), on: self)
        }
        picker.dismiss(animated: true, completion: nil)
    }
}

//MARK: Extension AppInventivCropperDelegate
//MARK:
extension EditPostedJobVC: CropperDelegate {
    
    
    func imageCropperDidCancelCrop() {
        
        print_debug("Crop cancelled")
        
    }
    func imageCropper(didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect) {
        
        self.pickedImage = croppedImage
        self.editJobTableView.reloadRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
        self.uploadImage(image: croppedImage)
    }
}

//MARK: - CircularSliderDelegate
//=================================
extension EditPostedJobVC: ChooseExpProtocol {
    
    func didTapOkBtn(monthExp: Int?, yearExp: Float?, isExp: Int) {
        
        var experiance = ""
        if isExp == 1 {
            if let startMonth = monthExp {
                experiance.append("\(startMonth) \(StringConstants.Month)")
                self.jobDetail?.totalExperience = "\(startMonth)"
                self.jobDetail?.experienceType = 1
            } else if let startYearExp = yearExp {
                experiance.append("\(startYearExp) \(StringConstants.Year)")
                let val = "\(startYearExp)".replacingOccurrences(of: ".0", with: "")
                self.jobDetail?.totalExperience = val
                self.jobDetail?.experienceType = 2
            }
        } else {
            self.jobDetail?.totalExperience = ""
            self.jobDetail?.experienceType = 0
        }
        
        self.editJobTableView.reloadData()
        
        print_debug(experiance)
    }
}

//MARK:- Prototype Cell
//=========================
class AddImageCell: UITableViewCell {
    
    //MARK:- Properties
    //==================
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var addImageLabel: UILabel!
    @IBOutlet weak var jobImageView: UIImageView!
    @IBOutlet weak var choseImageButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.jobImageView.setCorner(cornerRadius: 5.0, clip: true)
        self.addImageLabel.font = AppFonts.Poppins_Regular.withSize(14)
        self.addImageLabel.textColor = AppColors.gray152
    }
    
}

//MARK:- Prototype Cell
//=========================
class JobTitleTextFieldCell: UITableViewCell {
    
    //MARK:- Properties
    //==================
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var jobTitleTextField: SkyFloatingLabelTextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

//MARK:- Prototype Cell
//=========================
class DescriptionCell: UITableViewCell {
    
    //MARK:- Properties
    //==================
    
    //MARK:- IBoutlets
    //================
    
    @IBOutlet weak var seperatorView: UIView!
    
    @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var descriptionTextView: TLFloatLabelTextView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        //        self.descriptionTextView.textContainer.maximumNumberOfLines = 5
        self.descriptionTextView.textContainer.lineBreakMode = .byWordWrapping
        descriptionTextView.isScrollEnabled = false
    }
}

//MARK:- Prototype Cell
//=========================
class JobTypeCell: UITableViewCell {
    
    //MARK:- Properties
    //==================
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var jobTypeLabel: UILabel!
    @IBOutlet weak var fullTimeButton: UIButton!
    @IBOutlet weak var partTimeButton: UIButton!
    @IBOutlet weak var bothButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.jobTypeLabel.font = AppFonts.Poppins_Regular.withSize(15.0)
        self.jobTypeLabel.textColor = AppColors.gray152
        self.fullTimeButton.setTitleColor(AppColors.black46, for: .normal)
        self.partTimeButton.setTitleColor(AppColors.black46, for: .normal)
        self.bothButton.setTitleColor(AppColors.black46, for: .normal)
    }
}

//MARK:- Prototype Cell
//=========================
class SalaryCell: UITableViewCell {
    
    //MARK:- Properties
    //==================
    
    //MARK:- IBoutlets
    //================
    
    @IBOutlet weak var fromTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var toTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var perHourTextField: SkyFloatingLabelTextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        CommonClass.setSkyFloatingLabelTextField(textField: self.fromTextField, placeholder: StringConstants.from, title: StringConstants.from)
        CommonClass.setSkyFloatingLabelTextField(textField: self.toTextField, placeholder: StringConstants.to, title: StringConstants.to)
        CommonClass.setSkyFloatingLabelTextField(textField: self.perHourTextField, placeholder: StringConstants.Per_Hour, title: StringConstants.Per_Hour)
        self.setupTextFieldKeyBoadType()
    }
    
    func setupTextFieldKeyBoadType() {
        
        self.fromTextField.keyboardType = .decimalPad
        self.toTextField.keyboardType   = .decimalPad
    }
}


