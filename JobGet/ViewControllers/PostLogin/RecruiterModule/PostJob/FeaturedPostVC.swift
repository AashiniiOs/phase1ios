//
//  FeaturedCellVC.swift
//  JobGet
//
//  Created by macOS on 11/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class FeaturedPostVC: BaseVC {
    
    //MARK:- Properties
    //==================
    var featurePostModel = [FeaturedPostModel]()
    let planBgImage      = [#imageLiteral(resourceName: "icHomeOrangeCard")]
    var jobId: String?
    var isFromDetail = false
    var isFromJobPost = false
    
    
    //MARK:- IBoutlets
    //================
    // @IBOutlet weak var shadowView: UIView!
    
    @IBOutlet weak var skipButton: UIButton!
    @IBOutlet weak var featurePostTableView: UITableView!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var nextButton: UIButton!
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
}

//MARK:- Private Methods
//=======================
private extension FeaturedPostVC {
    
    func initialSetup() {
        
        self.getFeaturedPlan(loader: true)
        self.skipButton.isHidden = self.isFromDetail
        
        self.backButton.isHidden = self.isFromJobPost
        self.featurePostTableView.delegate = self
        self.featurePostTableView.dataSource = self
//        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: StringConstants.FeaturedNumber )
//        attributedString.setColorForText(textForAttribute: "9", withColor: AppColors.themeBlueColor, font: AppFonts.Poppins_Medium.withSize(15) )
        self.navigationView.setShadow()
        CommonClass.setNextButton(button: self.nextButton)
        self.registerNib()
         self.featurePostTableView.allowsSelection = true
        
    }
    
    func registerNib() {
        
        let featureCell = UINib(nibName: "FeatureCell", bundle: nil)
        self.featurePostTableView.register(featureCell, forCellReuseIdentifier: "FeatureCell")
        
    }
    
    //    func getData() {
    //
    //        jobPostData["planId"] =  self.planId
    //        jobPostData["recruiterId"] = AppUserDefaults.value(forKey: .userId)
    //
    //    }
}

//MARK: - IB Action and Target
//===============================
extension FeaturedPostVC {
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.pop()
    }
    
    @IBAction func nextButtonTapped(_ sender: UIButton) {
        
        if let vc = self.navigationController?.viewControllers {
            for controllers in vc{
                if controllers is RecruiterTabBarVC {
                    self.navigationController?.popToViewController(controllers, animated: true)
                }
            }
        }
        NotificationCenter.default.post(name: NSNotification.Name("Animating_Sptlight_Icon"), object: ["jobId" : self.jobId ?? ""])
        self.navigationController?.popToRootViewController()
        // self.getData()
//        let sceen = PreviewJobsVC.instantiate(fromAppStoryboard: .PostJob)
//        self.navigationController?.pushViewController(sceen, animated: true)
        //  self.present(sceen, animated: true)
       
    }
}


//MARK:- Web Services
//====================
extension FeaturedPostVC {
    
    func getFeaturedPlan(loader: Bool) {
        
        
        WebServices.getFeaturedPlan(parameters: ["type":"2"], loader: loader, success: { (json) in
            if let code = json["code"].int, code == error_codes.success {
                
                let data = json["data"].arrayValue
                
                for value in data {
                    self.featurePostModel.append(FeaturedPostModel(dict: value))
                }
            }
            
            self.featurePostTableView.reloadData()
        }) { (error: Error) in
            CommonClass.showToast(msg: error.localizedDescription )
        }
    }
}
//MARK: - Table view Delegate and DataSource
//============================================
extension FeaturedPostVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 1 {
            return self.featurePostModel.count
        } else  {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "FeatureDescriptionCell") as? FeatureDescriptionCell else { fatalError("invalid cell \(self)")
            }
            return cell
        case 1:
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "FeatureCell") as? FeatureCell else { fatalError("invalid cell \(self)")
            }
            cell.timeLabel.text         = self.featurePostModel[indexPath.row].planName
            cell.descriptionLabel.text  = self.featurePostModel[indexPath.row].description
            if let planamount = Double(self.featurePostModel[indexPath.row].planAmount){
                self.featurePostModel[indexPath.row].planAmount = "\(planamount.rounded(toPlaces: 2))"
                //                let amount = self.featurePostModel[indexPath.row].planAmount
                cell.priceLabel.text        = "$ \(planamount)"
            }
            cell.bgImageView.image      = self.planBgImage[indexPath.row % 4]
            return cell
            
        default:
            fatalError("invalid cell")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch indexPath.section {
        case 1:
           
            var planDetail = JSONDictionary()
            planDetail["planId"] = self.featurePostModel[indexPath.row].planId
            planDetail["planName"] = self.featurePostModel[indexPath.row].planName
            planDetail["amount"] = self.featurePostModel[indexPath.row].planAmount
            planDetail["jobId"] = self.jobId ?? ""
//            let savecCrdList = SaveCardListVC.instantiate(fromAppStoryboard: .Star)
//            savecCrdList.planDetail = planDetail
//            savecCrdList.isPremiumPlan = true
            let paymentMethod = PaymentMethodVC.instantiate(fromAppStoryboard: .Star)
            paymentMethod.isOpenFromJobPost = self.isFromJobPost
            paymentMethod.headerImage = self.planBgImage[0]
            paymentMethod.planDetail = planDetail
            paymentMethod.featurePostModel = self.featurePostModel[indexPath.row]
            paymentMethod.isPremiumPlan = true
            self.navigationController?.pushViewController(paymentMethod, animated: true)

//            self.navigationController?.pushViewController(savecCrdList, animated: true)
           
        default :
            return
            
        }
    }
}

//MARK:- Prototype Cell
//=========================
class FeatureDescriptionCell: UITableViewCell {
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.descriptionLabel.textColor = AppColors.black46
        
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: StringConstants.MakePostFeatured )
        attributedString.setColorForText(textForAttribute: StringConstants.Optional, withColor: AppColors.gray152, font: AppFonts.Poppins_Light.withSize(15))
        
        self.descriptionLabel.attributedText = attributedString
    }
}
