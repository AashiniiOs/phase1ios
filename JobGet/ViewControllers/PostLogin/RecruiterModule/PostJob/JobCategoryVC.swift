//
//  JobCategoryVC.swift
//  JobGet
//
//  Created by macOS on 05/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import SwiftyJSON
import SDWebImage

protocol EnableNextButtonProtocol: class {
    
    func isEnableNextButton(status: Bool)
}

protocol GetEditedCategory: class {
    func editedCategory(withName categoryName: String, withId categoryId: String)
}

class JobCategoryVC: BaseVC {
    
    //MARK:- Properties
    //==================
    var jobCategoryData = [JobCategory]()
    var selectedRow = -1
    var fromEditProfile = false
    var currentCategory = ""
    weak var delegate: EnableNextButtonProtocol?
    weak var editCategoryDelegate: GetEditedCategory?
    
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var jobCategoryTableView: UITableView!
    @IBOutlet weak var jobCategoryLabel: UILabel!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var navigationTitleLbl: UILabel!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    
    
    //MARK:- view Life cycle
    //=======================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//MARK:- Private Methods
//=======================
private extension JobCategoryVC {
    
    func initialSetup() {
        
        // self.navigationView.isHidden = true
        self.jobCategoryTableView.delegate = self
        self.jobCategoryTableView.dataSource = self
        CommonClass.setNextButton(button: self.nextButton)
        self.navigationView.setShadow()
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: StringConstants.JobCategoyNumber )
        attributedString.setColorForText(textForAttribute: "2", withColor: AppColors.themeBlueColor, font: AppFonts.Poppins_Medium.withSize(15) )
        
        
        self.isEnableNextBtn(status: false)
        
        if fromEditProfile {
            self.navigationTitleLbl.text = "SELECT CATEGORY"
            self.nextButton.setTitle("SELECT",for: .normal)
        } else {
            SharedJobPost.removeAllValue()
            self.getCategoryList(loader: true)
            self.nextButton.setTitle("NEXT",for: .normal)
        }
    }
    
    func isEnableNextBtn(status: Bool) {
        
        status ? (self.nextButton.backgroundColor = AppColors.themeBlueColor) : (self.nextButton.backgroundColor = AppColors.buttonDisableBgColor)
        self.nextButton.isEnabled = status
    }
    
    
    
}

//MARK: - IB Action and Target
//===============================
extension JobCategoryVC {
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func nestBtnTapped(_ sender: UIButton) {
        if fromEditProfile { //(withName categoryName: String, withId categoryId: String)
            self.editCategoryDelegate?.editedCategory(withName: SharedJobPost.categoryName, withId: SharedJobPost.categoryId)
            self.pop()
        } else {
            SharedJobPost.categoryId = self.jobCategoryData[0].selectedCategoryID
            
            let sceen = PostJobContainerVC.instantiate(fromAppStoryboard: .PostJob)
            self.navigationController?.pushViewController(sceen, animated: true)
        }
        
    }
    
    @objc func didTappedCategoryButton(sender: UIButton) {
        
        guard let index = sender.tableViewIndexPath(self.jobCategoryTableView) else { return }
        guard let cell = self.jobCategoryTableView.cellForRow(at: index) as? JobCategoryCell else { return }
        if selectedRow > -1 {
            cell.changeFontAndImageOnCellDeselection(data: self.jobCategoryData[selectedRow])
        }
        self.selectedRow = index.row
        cell.changeFontAndImageOnCellSelection( data: self.jobCategoryData[index.row])
        self.jobCategoryData[0].selectedCategoryID = self.jobCategoryData[index.row].categoryId
        SharedJobPost.companyName = self.jobCategoryData[index.row].categoryTitle
    }
}

//MARK:- Webservice Methods
//=========================
extension JobCategoryVC {
    
    func getCategoryList(loader: Bool){
        let param = ["": ""]
        WebServices.getCategoryList(parameters: param, loader: loader, success: { (json) in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                
                let data = json["data"].arrayValue
                self.jobCategoryData.removeAll(keepingCapacity: false)
                for categoryData in data {
                    self.jobCategoryData.append(JobCategory(dict: categoryData))
                }
                
                self.jobCategoryTableView.reloadData()
            }
        }) { (error) in
            print_debug(error)
        }
    }
}

//MARK: - Table view Delegate and DataSource
//============================================
extension JobCategoryVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.jobCategoryData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "JobCategoryCell") as? JobCategoryCell else { fatalError("invalid cell \(self)")
        }
        cell.populateData(data: self.jobCategoryData[indexPath.row])
        
        if self.currentCategory == self.jobCategoryData[indexPath.row].categoryTitle { // get called when it comes from edit profile
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
            cell.changeFontAndImageOnCellSelection( data: self.jobCategoryData[indexPath.row])
            SharedJobPost.categoryName = self.jobCategoryData[indexPath.row].categoryTitle
            SharedJobPost.categoryId = self.jobCategoryData[0].selectedCategoryID
            self.isEnableNextBtn(status: true)
        } else if self.selectedRow == indexPath.row {
            cell.changeFontAndImageOnCellSelection( data: self.jobCategoryData[indexPath.row])
        } else {
            cell.changeFontAndImageOnCellDeselection(data: self.jobCategoryData[indexPath.row])
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        guard let cell = tableView.cellForRow(at: indexPath) as? JobCategoryCell else { return }
        cell.changeFontAndImageOnCellSelection( data: self.jobCategoryData[indexPath.row])
        self.jobCategoryData[0].selectedCategoryID = self.jobCategoryData[indexPath.row].categoryId
        SharedJobPost.categoryName = self.jobCategoryData[indexPath.row].categoryTitle
        SharedJobPost.categoryId = self.jobCategoryData[0].selectedCategoryID
        self.selectedRow = indexPath.row
        self.isEnableNextBtn(status: true)
        
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? JobCategoryCell else { return }
        cell.changeFontAndImageOnCellDeselection(data: self.jobCategoryData[indexPath.row])
        
    }
}


//MARK:- Prototype Cell
//=========================
class JobCategoryCell: UITableViewCell {
    
    //MARK:- Properties
    //==================
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var jobImageView: UIImageView!
    @IBOutlet weak var jobNemeLabel: UILabel!
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var checkBtn: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.jobNemeLabel.font = AppFonts.Poppins_Light.withSize(16)
        self.jobImageView.roundCorners()
    }
    
    func populateData(data: JobCategory) {
        self.jobNemeLabel.text = data.categoryTitle
        if CommonClass.catagoryList(data.categoryTitle).isEmpty{
            self.jobImageView.sd_setImage(with: URL(string: data.categoryImage), placeholderImage:#imageLiteral(resourceName: "ic_select_beauty_"))
        }else{
            self.jobImageView.image = CommonClass.commonCategoryList(data.categoryTitle, isSelected: true)
        }
    }
    
    
    func changeFontAndImageOnCellSelection(data: JobCategory) {
        
        self.jobNemeLabel.textColor  = AppColors.themeBlueColor
        self.jobNemeLabel.font      = AppFonts.Poppins_Medium.withSize(16)
        if CommonClass.catagoryList(data.categoryTitle).isEmpty{
            self.jobImageView.sd_setImage(with: URL(string: data.selectedCategoryImage), placeholderImage:#imageLiteral(resourceName: "ic_select_beauty_"))
        }else{
            self.jobImageView.image = CommonClass.commonCategoryList(data.categoryTitle, isSelected: true)
        }
        
        self.checkBtn.isSelected = true
    }
    
    func changeFontAndImageOnCellDeselection(data: JobCategory) {
        
        if CommonClass.catagoryList(data.categoryTitle).isEmpty{
            self.jobImageView.sd_setImage(with: URL(string: data.categoryImage), placeholderImage:#imageLiteral(resourceName: "ic_deselect_beauty_ (2)"))
        }else{
            self.jobImageView.image = CommonClass.commonCategoryList(data.categoryTitle, isSelected: false)
        }
        self.jobNemeLabel.textColor = AppColors.black46
        self.jobNemeLabel.font      = AppFonts.Poppins_Light.withSize(16)
        self.checkBtn.isSelected = false
    }
}



