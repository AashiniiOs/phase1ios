//
//  JobDescriptionVC.swift
//  JobGet
//
//  Created by macOS on 05/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import GooglePlacePicker
import GoogleMaps

class JobDescriptionVC: UIViewController {
    
    //MARK:- Properties
    //==================
    weak var delegate: EnableNextButtonProtocol?
    var enableNextButton = false
    var isStateTxtFldEnableEditing = false
    var lattitude = AppUserDefaults.value(forKey: .userLat).doubleValue
    var longitude: Double = AppUserDefaults.value(forKey: .userLong).doubleValue
    var locationManager = CLLocationManager()

    //MARK:- IBoutlets
    //================
    @IBOutlet weak var bottomSeperatorView: UIView!
    @IBOutlet weak var jobDescriptionLabel      : UILabel!
    @IBOutlet weak var jobTitleTextField        : SkyFloatingLabelTextField!
    @IBOutlet weak var jobDescriptionTextView   : TLFloatLabelTextView!
    
    @IBOutlet weak var editLocationBtnWidth: NSLayoutConstraint!
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var stateTextField: SkyFloatingLocationTextField!
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var jobDescTextViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var companyNameTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var toggleMapBtn: UIButton!
    @IBOutlet weak var infoButton: UIButton!
    @IBOutlet weak var infoLabel: UIView!
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        self.view.isUserInteractionEnabled = true
        jobDescTextViewHeightConstraint.constant = 70

        infoLabel.isHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

//        CLLocationManager().requestWhenInUseAuthorization()
        
    }
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        infoLabel.isHidden = true

    }
    func checkButtonStatus() -> Bool {
        if (self.jobTitleTextField.text ?? "").count > 0,  (self.jobDescriptionTextView.text ?? "").count > 0, (self.stateTextField.text ?? "").count > 0, (self.companyNameTextField.text ?? "").count > 0 {
            self.delegate?.isEnableNextButton(status: true)
            return true
        } else {
            self.delegate?.isEnableNextButton(status: false)
            return false
        }
    }
    @IBAction func infoButtonTapped(_ sender: UIButton) {
        infoLabel.isHidden = false

    }
}

//MARK:- Private Methods
//=======================
private extension JobDescriptionVC {
    
    func initialSetup() {
        
        if SharedJobPost.address.isEmpty{
            self.editLocationBtnWidth.constant = 0
        }else{
            self.editLocationBtnWidth.constant = 30
        }
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.requestLocation()

        CommonClass.setSkyFloatingLabelTextField(textField: self.jobTitleTextField, placeholder: StringConstants.JobTitle, title: StringConstants.JobTitle)
        CommonClass.setSkyFloatingLocationTextField(textField: self.stateTextField, placeholder: StringConstants.location, title: StringConstants.location)
        CommonClass.setSkyFloatingLabelTextField(textField: self.companyNameTextField, placeholder: StringConstants.Company_Name, title: StringConstants.Company_Name)
        CommonClass.setTLFoatTextView(textView: self.jobDescriptionTextView, title: "Job Description. Try something like \"Looking \nfor an outgoing individual!")
        self.jobTitleTextField.delegate      = self
        self.jobDescriptionTextView.delegate = self
        self.companyNameTextField.delegate = self
        self.stateTextField.delegate = self
        self.jobDescriptionTextView.textContainer.lineBreakMode = .byWordWrapping
        self.jobDescriptionTextView.isScrollEnabled = false
        self.jobTitleTextField.autocapitalizationType = .sentences
        self.companyNameTextField.autocapitalizationType = .sentences
        self.setupFont()
        self.setupKeyboardType()
        self.populateTextField()
        self.bgView.bringSubview(toFront: self.bottomSeperatorView)
         NotificationCenter.default.addObserver(self, selector: #selector(self.manageDenyLocation), name: NSNotification.Name.init(rawValue: "DENY_CURRENT_LOCATION_PERMISSION"), object: nil)
    }
    
    @objc func manageDenyLocation(){
        
        print_debug("location access denied")
        let lat = AppUserDefaults.value(forKey: .userLat).stringValue != "0" && !AppUserDefaults.value(forKey: .userLat).stringValue.isEmpty ?  AppUserDefaults.value(forKey: .userLat).doubleValue : Double(StringConstants.Boston_Lat)!
        
        let long = AppUserDefaults.value(forKey: .userLong).stringValue == "0" && AppUserDefaults.value(forKey: .userLong).stringValue.isEmpty ? AppUserDefaults.value(forKey: .userLong).doubleValue : Double(StringConstants.Boston_Long)!
        
        let northEast = CLLocationCoordinate2DMake(lat + 0.001, long + 0.001)
        let southWest = CLLocationCoordinate2DMake(lat - 0.001, long - 0.001)
        let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
        let config = GMSPlacePickerConfig(viewport: viewport)
        
        //                let config = GMSPlacePickerConfig(viewport: nil)
        
        let placePicker = GMSPlacePickerViewController(config: config)
        
        placePicker.delegate = self
        self.present(placePicker, animated: true, completion: nil)
    }
    
    func setupKeyboardType() {
        self.jobTitleTextField.returnKeyType = .next
        self.stateTextField.returnKeyType = .next
        self.companyNameTextField.returnKeyType = .done
    }
    
    func setData() {
        
        guard let jobTitle = self.jobTitleTextField.text else { return }
        guard let jobDescription = self.jobDescriptionTextView.text else { return }
        SharedJobPost.jobTitle = jobTitle
        SharedJobPost.jobDesc = jobDescription
        SharedJobPost.address = stateTextField.text ?? ""
        SharedJobPost.latitude = self.lattitude
        SharedJobPost.longitude = self.longitude
        SharedJobPost.companyName = self.companyNameTextField.text ?? ""
    }
    
    func populateTextField() {
        
        if !SharedJobPost.jobTitle.isEmpty {
            self.jobTitleTextField.text = SharedJobPost.jobTitle
        }
        
        if !SharedJobPost.jobDesc.isEmpty {
            self.jobDescriptionTextView.text = SharedJobPost.jobDesc
        }
        
        if !SharedJobPost.companyName.isEmpty {
            self.companyNameTextField.text = SharedJobPost.companyName
        } else {
            SharedJobPost.companyName = AppUserDefaults.value(forKey: .recruiterCompanyName).stringValue
            self.companyNameTextField.text = SharedJobPost.companyName
        }
        
        if !SharedJobPost.address.isEmpty {
            self.stateTextField.text = SharedJobPost.address
        }
        
        let _ =  self.checkButtonStatus()
    }
    
    func setupFont() {
        self.jobDescriptionLabel.textColor = AppColors.black46
    }
}

//MARK: - IB Action and Target
//===============================
extension JobDescriptionVC {
    
    @IBAction func locationButtonTapp(_ sender: UIButton) {
        if self.toggleMapBtn.isSelected{
            return
        }
        
        if CLLocationManager.authorizationStatus() == .denied {
            CommonClass.delayWithSeconds(1.0, completion: {
                let alertViewController = UIAlertController(title: StringConstants.WantsFetchLocation, message: "", preferredStyle: UIAlertControllerStyle.alert)
                let okAction = UIAlertAction(title: StringConstants.Allow, style: UIAlertActionStyle.default, handler: { (action : UIAlertAction) in
                    //        TODO: Add settings
                    if #available(iOS 10.0, *) {
                        UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: { (success) in
                            
                        })
                    } else {
                        // Fallback on earlier versions
                    }
                })
                
                let deny = UIAlertAction(title: StringConstants.Deny, style: UIAlertActionStyle.default, handler: { (action : UIAlertAction) in
                    let lat = AppUserDefaults.value(forKey: .userLat).stringValue != "0" && !AppUserDefaults.value(forKey: .userLat).stringValue.isEmpty ?  AppUserDefaults.value(forKey: .userLat).doubleValue : Double(StringConstants.Boston_Lat)!
                    
                    let long = AppUserDefaults.value(forKey: .userLong).stringValue == "0" && AppUserDefaults.value(forKey: .userLong).stringValue.isEmpty ? AppUserDefaults.value(forKey: .userLong).doubleValue : Double(StringConstants.Boston_Long)!
                    
                    let northEast = CLLocationCoordinate2DMake(lat + 0.001, long + 0.001)
                    let southWest = CLLocationCoordinate2DMake(lat - 0.001, long - 0.001)
                    let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
                    let config = GMSPlacePickerConfig(viewport: viewport)
                    
                    //                let config = GMSPlacePickerConfig(viewport: nil)
                    
                    let placePicker = GMSPlacePickerViewController(config: config)
                    
                    placePicker.delegate = self
                    self.present(placePicker, animated: true, completion: nil)
                })
                alertViewController.addAction(okAction)
                alertViewController.addAction(deny)
                
                self.present(alertViewController, animated: true, completion: nil)
            })
        }else{
            
            let config = GMSPlacePickerConfig(viewport: nil)
            
            //                let config = GMSPlacePickerConfig(viewport: nil)
            
            let placePicker = GMSPlacePickerViewController(config: config)
            
            placePicker.delegate = self
            present(placePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func currentLocationBtnTapped(_ sender: UIButton) {
        
        if CLLocationManager.authorizationStatus() == .denied {
            
//             self.reverseGeoCode(lattitude: AppUserDefaults.value(forKey: .userLat).doubleValue, longitude: AppUserDefaults.value(forKey: .userLong).doubleValue, isCommingFromCurrentLocation:  true)
            CommonClass.delayWithSeconds(1.0, completion: {
                let popUpVc = AlertPopUpVC.instantiate(fromAppStoryboard: .Candidate)
                popUpVc.delegate = self
                popUpVc.modalPresentationStyle = .overCurrentContext
                popUpVc.modalTransitionStyle = .crossDissolve
                self.present(popUpVc, animated: true, completion: nil)
            })
            
        } else {
            SharedLocationManager.fetchCurrentLocation { (currentLocation) in
                print_debug(currentLocation)
                SharedLocationManager.locationManager.stopUpdatingLocation()
                self.reverseGeoCode(lattitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude, isCommingFromCurrentLocation: true)
            }
        }
    }
    
    @IBAction func toggleMapBtnTapp(_ sender: UIButton) {
        
        if self.stateTextField.hasText {
            sender.isSelected = !sender.isSelected
            self.isStateTxtFldEnableEditing = !self.isStateTxtFldEnableEditing
            self.view.endEditing(true)
        }
    }
}
extension JobDescriptionVC : UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        infoLabel.isHidden = true
    }
}

//MARK:- Location alert popup Delegate
//===========================================
extension JobDescriptionVC: AlertPopUpDelegate{
    
    func didTapAffirmativeButton() {
        //        TODO: Add settings
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: nil)
        } else {
            // Fallback on earlier versions
        }
    }
    
    func didTapNegativeButton() {
        print_debug("location access denied")
        
    }
}

//MARK: - TextField Delegate
//==============================
extension JobDescriptionVC: UITextFieldDelegate {
    
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        let _ = self.checkButtonStatus()
        self.setData()
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField === self.jobTitleTextField {
            self.jobTitleTextField.autocapitalizationType = .words
            return true
        }
        if textField === self.stateTextField, !isStateTxtFldEnableEditing {
            if CLLocationManager.authorizationStatus() == .denied {
                CommonClass.delayWithSeconds(1.0, completion: {
                    let alertViewController = UIAlertController(title: StringConstants.Location_Allow_Title, message: StringConstants.Fetch_Location_Reason_Text, preferredStyle: UIAlertControllerStyle.alert)
                    let okAction = UIAlertAction(title: StringConstants.Allow, style: UIAlertActionStyle.default, handler: { (action : UIAlertAction) in
                        //        TODO: Add settings
                        if #available(iOS 10.0, *) {
                            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: { (success) in
                                
                            })
                        } else {
                            // Fallback on earlier versions
                        }
                    })
                    
                    let deny = UIAlertAction(title: StringConstants.Deny, style: UIAlertActionStyle.default, handler: { (action : UIAlertAction) in
                        let lat = AppUserDefaults.value(forKey: .userLat).stringValue != "0" && !AppUserDefaults.value(forKey: .userLat).stringValue.isEmpty ?  AppUserDefaults.value(forKey: .userLat).doubleValue : Double(StringConstants.Boston_Lat)!
                        
                        let long = AppUserDefaults.value(forKey: .userLong).stringValue == "0" && AppUserDefaults.value(forKey: .userLong).stringValue.isEmpty ? AppUserDefaults.value(forKey: .userLong).doubleValue : Double(StringConstants.Boston_Long)!
                        let northEast = CLLocationCoordinate2DMake(lat + 0.001, long + 0.001)
                        let southWest = CLLocationCoordinate2DMake(lat - 0.001, long - 0.001)
                        let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
                        let config = GMSPlacePickerConfig(viewport: viewport)
                        
                        //                let config = GMSPlacePickerConfig(viewport: nil)
                        
                        let placePicker = GMSPlacePickerViewController(config: config)
                        
                        placePicker.delegate = self
                        self.present(placePicker, animated: true, completion: nil)
                    })
                    alertViewController.addAction(deny)
                    alertViewController.addAction(okAction)
                  
                    self.present(alertViewController, animated: true, completion: nil)
                })
            }else{
                
                let config = GMSPlacePickerConfig(viewport: nil)
                
                //                let config = GMSPlacePickerConfig(viewport: nil)
                
                let placePicker = GMSPlacePickerViewController(config: config)
                
                placePicker.delegate = self
                present(placePicker, animated: true, completion: nil)
            }
            
            return false
        } else {
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField === self.jobTitleTextField {
            self.jobDescriptionTextView.becomeFirstResponder()
            
        } else if textField === self.stateTextField {
            self.companyNameTextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        
        return true
    }
}


//MARK: - TextView Delegate
//==============================
extension JobDescriptionVC: UITextViewDelegate {
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        self.bottomSeperatorView.backgroundColor = AppColors.themeBlueColor
        return true
        
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        self.bottomSeperatorView.backgroundColor = AppColors.blakopacity15
        let _ = self.checkButtonStatus()
        self.setData()
        
        if textView.frame.size.height <= 114 {
            self.jobDescTextViewHeightConstraint.constant = textView.frame.size.height
        } else {
            self.jobDescTextViewHeightConstraint.constant = 114
        }
        self.setData()
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        print_debug(newText.count)
        if newText.count > 2000{
            showAlert(msg:  StringConstants.job_decription_text_limit)
        }
        return newText.count < 2000
    }
    
    func textViewDidChange(_ textView: UITextView) {
        
        let topBottomPadding: CGFloat = 10.0
        let threeLineTextHeight: CGFloat = 85.0
        let fixedWidth = textView.frame.size.width
        let fixedHeight = textView.frame.size.height
        
        let maximumTextHeight: CGFloat = threeLineTextHeight + topBottomPadding
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: fixedHeight))
        let textHeight = textView.text.heightWithConstrainedWidth(width: textView.width-10.0, font: textView.font!)
        if textHeight > threeLineTextHeight {
            textView.isScrollEnabled = true
            jobDescTextViewHeightConstraint.constant = maximumTextHeight
            
        } else {
            textView.isScrollEnabled = false
            if textView.text.isEmpty{
                jobDescTextViewHeightConstraint.constant = 70
                textView.frame.size = CGSize(width: max(newSize.width, fixedWidth), height:70)
            }else{
                textView.frame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
                jobDescTextViewHeightConstraint.constant = newSize.height

            }
        }
    }
}

// MARK: - Google Place Picker
//======================================
extension JobDescriptionVC: GMSPlacePickerViewControllerDelegate {
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        
        
        var postal_code = ""
        if let code = place.addressComponents {
        for component in code {
            if component.type == "postal_code" {
                postal_code = component.name
            }
        }
    }
        self.lattitude = place.coordinate.latitude
        self.longitude = place.coordinate.longitude
        if let address =  place.formattedAddress {
            
            if address.contains(s: "\(postal_code), USA"){
                self.stateTextField.text = address.replace(string: "\(postal_code), USA", withString: "")
                let _ = self.checkButtonStatus()
            }else{
                self.stateTextField.text = address
            }
            
        }
        self.editLocationBtnWidth.constant = 30.0
        self.setData()
        let _ =  self.checkButtonStatus()
        
        self.reverseGeoCode(lattitude: self.lattitude, longitude: self.longitude, isCommingFromCurrentLocation: false)
        viewController.dismiss(animated: true)
        
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        
        print("No place selected")
    }
    
}

// MARK:- Reverce Geo Code
//======================================
extension JobDescriptionVC {
    
    
    func reverseGeoCode(lattitude: Double, longitude: Double, isCommingFromCurrentLocation: Bool)  {
        let geocoder = GMSGeocoder()
        let coordinate = CLLocationCoordinate2DMake(Double(lattitude), Double(longitude))
        
        geocoder.reverseGeocodeCoordinate(coordinate) { response, error in
            
            if let location = response?.firstResult() {
                
                self.lattitude = location.coordinate.latitude
                self.longitude = location.coordinate.longitude
                var thoroughfar = ""
                var localit = ""
                var administrativeAre = ""
                if let thoroughfare = location.thoroughfare {
                    thoroughfar = thoroughfare
                }
                
                if let locality = location.locality{
                    localit = locality
                }else if let administrativeArea = location.administrativeArea{
                    localit = administrativeArea
                }
                
                if let administrativeArea = location.administrativeArea{
                    administrativeAre = administrativeArea
                }
                
                if isCommingFromCurrentLocation {
                    self.stateTextField.text = "\(thoroughfar) \(localit) \(administrativeAre) "
                    self.setData()
                }
                SharedJobPost.jobCity = localit
                SharedJobPost.jobState = administrativeAre
                print_debug(location.lines)
                print_debug(self.stateTextField.text)
                
                let _ =  self.checkButtonStatus()
            }
        }
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        print("Error: ", error.localizedDescription)
    }
    
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
}
extension JobDescriptionVC : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("error:: \(error.localizedDescription)")
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            locationManager.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let loc = locations.last else{return}
        self.reverseGeoCode(lattitude: loc.coordinate.latitude, longitude: loc.coordinate.longitude, isCommingFromCurrentLocation: true)

        if locations.first != nil {
            
            print("location:: (location)")
        }
        
    }
    
}
