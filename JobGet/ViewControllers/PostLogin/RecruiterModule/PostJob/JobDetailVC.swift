//
//  JobDetailVC.swift
//  JobGet
//
//  Created by macOS on 10/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import SwiftyJSON
import MXParallaxHeader
import WDScrollableSegmentedControl
import OnlyPictures



class JobDetailVC: BaseVC {
    
    //MARK:- Properties
    
    //    let parallexHeaderView    = JobDetailHeaderView()
    var jobDetail: MyJobListModel?
    var pendingJobDetail = [PendingJobDetail]()
    var isPendingCellLoading = true
    var isShortListedCellLoading = true
    var backFromRecruiterDetail = false
    var page: Int?
    var jobID: String?
    var senderId: String?
    var rejectedIndex: IndexPath?
    var states = false
    var cancelJobStatus: Int?
    var isOpenFromPush = false
    var isForApplicantsBtnTap = false
    var isTableViewScrollEnable = false
    var seconds = 0 //This variable will hold a starting value of seconds. It could be any amount above 0.
    var timer = Timer()
    var isTimerRunning = false //This will be used to make sure only one timer is created at a time.
    
    
    //MARK:- IBoutlets
    
    //Header view outlets.
    @IBOutlet weak var headerImageTopConstr: NSLayoutConstraint!
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var jobNameLabel: UILabel!
    @IBOutlet weak var jobImageView: UIImageView!
    @IBOutlet weak var jobNameContainerView: UIView!
    
    @IBOutlet weak var naviBgView: UIView!
    @IBOutlet weak var navigationGradientView: GradientView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var navTitleLabel: UILabel!
    @IBOutlet weak var edtiButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var jobDetailTableView: UITableView!
    @IBOutlet weak var navTitleBtn: UIButton!
    
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        self.jobDetailTableView.isUserInteractionEnabled = true
        self.initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.populateDataInToParallexHeader()
        self.getJobDetail(loader: true)
        self.isPendingCellLoading = true
        runTimer()
        CommonClass.delayWithSeconds(0.2) {
            if self.isForApplicantsBtnTap {
                self.jobDetailTableView.scrollToRow(at: IndexPath(row: 0, section: 1 ), at: .top, animated: false)
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        self.jobDetailTableView.reloadRows(at: [[0,0]], with: .none)
    }
    
}

//MARK:- Private Methods
//=======================
private extension JobDetailVC {
    
    func initialSetup() {
        
        self.jobDetailTableView.delegate = self
        self.jobDetailTableView.dataSource = self
        self.jobDetailTableView.tableHeaderView = self.headerView
        if SCREEN_HEIGHT < 800{
            self.headerImageTopConstr.constant = -20
        }else{
            self.headerImageTopConstr.constant = -45
        }
        self.jobDetailTableView.setShadow(offset: CGSize(width: 0.0, height: 2.0))
        self.jobDetailTableView.separatorColor = self.jobDetailTableView.backgroundColor
        
        self.registerCell()
    }
    
    func registerCell() {
        
        let nib = UINib(nibName: "JobDetailCell", bundle: nil)
        self.jobDetailTableView.register(nib, forCellReuseIdentifier: "JobDetailCell")
        // CandidateAboutMeCell
        let detailNib = UINib(nibName: "RecDescCell", bundle: nil)
        self.jobDetailTableView.register(detailNib, forCellReuseIdentifier: "RecDescCell")
    }
    
    func populateDataInToParallexHeader() {
        if let details = jobDetail {
            let image = details.jobImage.replacingOccurrences(of: "\"", with: "")
            self.jobImageView.sd_setImage(with: URL(string: image), placeholderImage: #imageLiteral(resourceName: "icHomeCategoryPlaceholder") , completed: nil)
            self.jobNameLabel.text = details.companyName
            if self.isOpenFromPush{
                let indexPath = IndexPath(row: 0, section: 1)
                self.jobDetailTableView.scrollToRow(at: indexPath, at: .bottom, animated: true)
            }
        }
    }
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        seconds -= 1
        //        if let index = self.selectedSpotlightIndex {
        guard let cell = self.jobDetailTableView.cellForRow(at: [0,0]) as? JobDetailCell else{return}
        cell.spotLightViewLabel.text = "Job Spotlighted for " + timeString(time: TimeInterval(self.seconds))
        
        //        }
        //This will decrement(count down)the seconds.
        //        timerLabel.text = “\(seconds)” //This will update the label.
    }
    
    func timeString(time:TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }
    
    
}

//MARK: - IB Action and Target
//===============================
extension JobDetailVC {
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        
        if self.jobDetailTableView.isScrollEnabled {
            self.navigationController?.popViewController(animated: true)
        } else {
            UIView.animate(withDuration: 0.3, animations: {
                self.isForApplicantsBtnTap = false
                self.isTableViewScrollEnable = false
                self.jobDetailTableView.contentOffset.y = 10
            })
        }
    }
    
    @IBAction func shareButtonTapped(_ sender: UIButton) {
        self.displayShareSheet(shareContent: self.jobDetail?.shareUrl ?? "")
    }
    
    @IBAction func editButtonTapped(_ sender: UIButton) {
        let sceen = EditPostedJobVC.instantiate(fromAppStoryboard: .PostJob)
        sceen.jobDetail = self.jobDetail
        sceen.delegate = self
        self.navigationController?.pushViewController(sceen, animated: false)
    }
    
    @objc func intrestedBtnTapped(sender: UIButton) {
        self.isForApplicantsBtnTap = true
        self.jobDetailTableView.scrollToRow(at: IndexPath( row: 0, section: 1), at: .bottom, animated: true)
        
    }
    
    @IBAction func navTitleBtnTap(_ sender: UIButton) {
        if !self.jobDetailTableView.isScrollEnabled {
            UIView.animate(withDuration: 0.3, animations: {
                self.isForApplicantsBtnTap = false
                self.isTableViewScrollEnable = false
                self.jobDetailTableView.contentOffset.y = 10
            })
        }
    }
}

//MARK:- Web Services
//====================
extension JobDetailVC {
    //recruiterJobGetail
    func getJobDetail(loader: Bool) {
        let param = ["jobId": self.jobID ?? ""]
        WebServices.recruiterJobDetail(parameters: param, loader: loader, success: { (json) in
            if let code = json["code"].int, code == error_codes.success {
                
                self.jobDetail = MyJobListModel(dict: json["data"])
                if let data = self.jobDetail{
                    self.seconds = CommonClass.calculateTimeDifference( to: data.premiumEnd)
                    
                }
                
                self.jobDetailTableView.reloadData()
                if self.isForApplicantsBtnTap{
                    self.jobDetailTableView.scrollToRow(at: IndexPath(row: 0, section: 1 ), at: .top, animated: false)
                }
                self.populateDataInToParallexHeader()
                
            } else {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
            
            print_debug(self.jobDetail?.deleteStatus)
            if self.jobDetail?.deleteStatus == 3 {
                self.shareButton.isHidden = true
                self.edtiButton.isHidden = true
                self.cancelJobStatus = self.jobDetail?.deleteStatus
            } else {
                self.shareButton.isHidden = false
                self.edtiButton.isHidden = false
            }
            
            
            
        }) { (error: Error) in
            CommonClass.showToast(msg: error.localizedDescription )
        }
    }
    
    func deletePostedJob() {
        let param = ["jobId": self.jobID ?? ""]
        WebServices.deletePostedJob(parameters: param , success: { (json) in
            if let code = json["code"].int, code == error_codes.success {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
                self.pop()
            } else {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }) { (error: Error) in
            CommonClass.showToast(msg: error.localizedDescription )
        }
    }
    
    func jobChangeStatus(sortlistOrReject jobType: Int, applyJobId: String, categoryId: String, loader: Bool) {
        
        let param = [ApiKeys.type.rawValue: jobType, ApiKeys.applyId.rawValue: applyJobId, ApiKeys.jobId.rawValue: self.jobID ?? ""] as [String: Any]
        WebServices.changePendingJobStatus(parameters: param, loader: loader, success: { (json) in
            if let code = json["code"].int, code == error_codes.success {
                self.isPendingCellLoading = true
                self.jobDetailTableView.reloadData()
            }
            let msg = json["message"].stringValue
            CommonClass.showToast(msg: msg)
            
            if jobType == 3 {
                guard let cell = self.jobDetailTableView.cellForRow(at: IndexPath(row: 0, section: 1)) as? PendingJobCell else { return }
                cell.shortListedVC.getShortlistedJobDetail(jobId: applyJobId, loader: false)
            }
        }) { (error) in
            CommonClass.showToast(msg: error.localizedDescription )
        }
    }
}

//MARK: - Table view Delegate and DataSource
//============================================
extension JobDetailVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let _ = self.jobDetail {
            return 2
        }else if self.isForApplicantsBtnTap {
            return 2
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if let _ = self.jobDetail {
                return 4
            }else{
                return 0
                
            }
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            switch indexPath.row {
            case 0:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "JobDetailCell") as? JobDetailCell else { fatalError("invalid cell \(self)")
                }
                cell.jobSeenCountButton.setTitleColor(AppColors.gray152, for: .normal)
                if let detail = self.jobDetail {
                    cell.populateData(detail: detail)
                    
                }
                cell.spotLightView.isHidden = true
                cell.locationBtn.addTarget(self, action: #selector(self.featuredJobBtnTapped(_:)), for: .touchUpInside)
                
                return cell
                
            case 1:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "RecDescCell") as? RecDescCell else { fatalError("invalid cell \(self)")
                }
                if let jobDetail = self.jobDetail {
                    cell.setJobDescData(jobDetail: jobDetail)
                }
                
                cell.descriptionLeadingConstraints.constant = 16
                
                return cell
            case 2:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "IntrestedApplicantsCell") as? IntrestedApplicantsCell else { fatalError("invalid cell \(self)")
                }
                
                if let planID = self.jobDetail  {
                    if !planID.isPremium {
                        cell.featuredLabel.text = "Standard"
                    } else {
                        cell.featuredLabel.text = "Spotlight"
                    }
                    
                }
                cell.intrestedButton.addTarget(self, action: #selector(self.intrestedBtnTapped(sender:)), for: .touchUpInside)
                
                if let detail = self.jobDetail {
                    
                    cell.setIntrestedImageView(applicants: detail.applicantsImage)
                    
                }
                return cell
            case 3:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "SalaryRangeCell") as? SalaryRangeCell else { fatalError("invalid cell \(self)")
                }
                cell.contentView.backgroundColor = #colorLiteral(red: 0.9725490196, green: 0.9725490196, blue: 0.9725490196, alpha: 1)
                cell.seperatorView.isHidden = true
                cell.durationLabel.isHidden = false
                cell.duationNameLabel.isHidden = false
                if let jobDetail = self.jobDetail {
                    cell.setSalaryRangeData(jobDetail: jobDetail)
                    
                }
                
                return cell
                
            default:
                fatalError("invalid cell")
            }
            
        } else  {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PendingJobCell") as? PendingJobCell else { fatalError("invalid cell \(self)")
            }
            
            if let jobId = self.jobID {
                cell.jobId = jobId
                cell.pendingJobVC.statusDelegate = self
                cell.pendingJobVC.isOpenFromPush = self.isOpenFromPush
                
                if  let status = self.cancelJobStatus, status == 3 {
                    print_debug(status)
                    cell.pendingJobVC.cancelJobStatus = status
                    cell.shortlistedCancelStatus = status
                }
                
                cell.pendingJobVC.openCandidateDetailDelegate = self
                cell.pendingJobVC.isOpenFromPush = self.isOpenFromPush
                cell.pendingJobVC.senderId = self.senderId
                if self.isPendingCellLoading {
                    cell.pendingJobVC.jobID = jobId
                    self.isPendingCellLoading = false
                }
            }
            
            if self.isShortListedCellLoading {
                cell.getData(index: 1)
                cell.shortListedVC.rejectedDelegate = self
                
                cell.shortListedVC.openDetailDelegate = self
                self.isShortListedCellLoading = false
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.jobDetailTableView.reloadRows(at: [[0,0]], with: .none)
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 {
            return UITableViewAutomaticDimension
        } else {
            if SCREEN_HEIGHT >= 812{
                return SCREEN_HEIGHT - 120
            }else{
                return SCREEN_HEIGHT - 65
            }
        }
    }
    
    //    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    //        if section == 0 {
    //            return 240.0
    //        } else {
    //            return 0.000001
    //        }
    //    }
    //
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return .leastNormalMagnitude
    }
    
    @objc func featuredJobBtnTapped(_ sender: UIButton) {
        
        guard let detail  = self.jobDetail else{return}
        //        if detail.isPremium{
        //            return
        //        }
        //        guard let index = sender.tableViewIndexPath(self.myJobTableView) else { return }
        guard let cell = sender.tableViewCell as? JobDetailCell else{return}
        if detail.isPremium{
            cell.spotLightView.isHidden = false
            cell.spotLightViewLabel.text = "Job Spotlighted for " + timeString(time: TimeInterval(self.seconds))
            //            self.myJobTableView.isUserInteractionEnabled = false
        }else{
            
            let sceen = FeaturedPostVC.instantiate(fromAppStoryboard: .PostJob)
            sceen.isFromDetail = true
            sceen.jobId = detail.jobId
            self.navigationController?.pushViewController(sceen, animated: false)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if self.isForApplicantsBtnTap {
            self.backBtn.setImage(#imageLiteral(resourceName: "ic_down_arrow_"), for: .normal)
            self.jobDetailTableView.isScrollEnabled = false
            self.navigationView.backgroundColor = AppColors.themeBlueColor
            self.naviBgView.backgroundColor = AppColors.themeBlueColor
            self.navigationGradientView.isHidden = true
            self.navTitleLabel.text = StringConstants.APPLICANTS
            self.isTableViewScrollEnable = true
        }
        
        var screenHight:CGFloat = 0
        if SCREEN_HEIGHT >= 812{
            screenHight = SCREEN_HEIGHT - 220
        }else{
            screenHight = SCREEN_HEIGHT - 65
        }
        if scrollView.contentOffset.y > screenHight {
            self.navigationGradientView.isHidden = true
//            self.jobDetailTableView.scrollToRow(at: IndexPath(row: 0, section: 1 ), at: .top, animated: false)
            self.jobDetailTableView.isScrollEnabled = true
            self.navigationView.backgroundColor = AppColors.themeBlueColor
            self.naviBgView.backgroundColor = AppColors.themeBlueColor
            self.navigationGradientView.isHidden = false
            self.navTitleLabel.text = StringConstants.APPLICANTS
            self.isTableViewScrollEnable = true
            self.backBtn.setImage(#imageLiteral(resourceName: "ic_down_arrow_"), for: .normal)
        } else {
            if self.isTableViewScrollEnable{
                return
            }
            self.jobDetailTableView.isScrollEnabled = true
            self.backBtn.setImage(#imageLiteral(resourceName: "icHomeDetailBack"), for: .normal)
            self.navigationView.backgroundColor = UIColor.clear
            self.naviBgView.backgroundColor = UIColor.clear
            self.navigationGradientView.isHidden = false
            self.navTitleLabel.text = ""
        }
        print_debug(scrollView.contentOffset.y)
        if scrollView.contentOffset.y > 210 {
            
            self.navigationView.backgroundColor = AppColors.themeBlueColor
            self.naviBgView.backgroundColor = AppColors.themeBlueColor
            self.navigationGradientView.isHidden = true
        }else{
            self.navigationView.backgroundColor = UIColor.clear
            self.naviBgView.backgroundColor = UIColor.clear
            self.navigationGradientView.isHidden = false
        }
    }
}
extension JobDetailVC: OpenRejectedPopup, AlertPopUpDelegate {
    
    func shouldRejectedPopup(index: IndexPath) {
        
        self.rejectedIndex = index
        let popUpVc = AlertPopUpVC.instantiate(fromAppStoryboard: .Candidate)
        popUpVc.delegate = self
        popUpVc.commingFromReject = true
        popUpVc.modalPresentationStyle = .overCurrentContext
        popUpVc.modalTransitionStyle = .crossDissolve
        self.present(popUpVc, animated: true, completion: nil)
        
    }
    
    func didTapAffirmativeButton() {
        
        if let index = self.rejectedIndex {
            guard let cell = self.jobDetailTableView.cellForRow(at: IndexPath(row: 0, section: 1)) as? PendingJobCell else { return }
            self.jobChangeStatus(sortlistOrReject: 3, applyJobId: cell.shortListedVC.pendingJobDetail[index.row].jobAppliedId, categoryId: cell.shortListedVC.pendingJobDetail[index.row].jobData.categoryId, loader: true)
        }
    }
    
    func didTapNegativeButton() {
        // To do Task
    }
}


// update data when edit is done
extension JobDetailVC: UpdatedJobDetail {
    
    func getUpdatedJobDetail(jobDeatil: MyJobListModel?) {
        self.jobDetail = jobDeatil
        self.jobDetailTableView.reloadData()
    }
}

// get called when pending cell status button tapped
extension JobDetailVC: ShouldOpenStatusPopup {
    
    func presentShortlistedPopup(jobApplyId: String, categoryId: String) {
        
        let shortListedPopup = SortListedStatusPopupVC.instantiate(fromAppStoryboard: .Recruiter)
        shortListedPopup.sortListedPopupType = .status
        self.view.addSubview(shortListedPopup.view)
        shortListedPopup.delegate = self
        shortListedPopup.jobApplyId = jobApplyId
        shortListedPopup.categoryId = categoryId
        
        self.add(childViewController: shortListedPopup)
    }
}

//MARK: - ShortListedApplicantsPopup Delegate
//========================================
extension JobDetailVC: ShortListedApplicantsPopup {
    
    
    func onTapAffermation(index: Int, applyJobId: String, categoryId: String ) {
        let value = index + 2
        self.jobChangeStatus(sortlistOrReject: value , applyJobId: applyJobId, categoryId: categoryId, loader: false)
    }
    
    func onTapCancel() {
        print_debug("perform action")
    }
}

//MARK: - Only Pictures DataSource
//=================================
extension JobDetailVC: OnlyPicturesDataSource, OnlyPicturesDelegate {
    
    
    func numberOfPictures() -> Int {
        return self.jobDetail?.applicantsImage.count ?? 0
    }
    
    func visiblePictures() -> Int {
        return 5
    }
    
    func pictureViews(_ imageView: UIImageView, index: Int) {
        
        if let url = URL(string: (self.jobDetail?.applicantsImage[index])!) {
            imageView.sd_setImage(with: url )
            
        } else {
            imageView.image = #imageLiteral(resourceName: "ic_placeholder_33") // placeholder image
        }
    }
    
    func pictureView(_ imageView: UIImageView, didSelectAt index: Int) {
        //TO DO TASK
    }
    
    func pictureViewDidSelect() {
        self.jobDetailTableView.scrollToRow(at: IndexPath(row: 0, section: 1 ), at: .top, animated: true)
    }
}
//MARK:- Extension MXParallaxHeaderDelegate Methods
//==================================================
extension JobDetailVC:  MXParallaxHeaderDelegate {
    
    func parallaxHeaderDidScroll(_ parallaxHeader: MXParallaxHeader) {
        
        let parallaxHeaderView = parallaxHeader.view as? JobDetailHeaderView
        
        parallaxHeaderView?.jobNameContainerView.alpha = parallaxHeader.progress > 0.9 ? 1.0 : parallaxHeader.progress - 0.1
        parallaxHeaderView?.jobNameLabel.alpha = parallaxHeader.progress > 0.9 ? 1.0 : parallaxHeader.progress
        
    }
}


extension JobDetailVC: DidOpenCandidateDetail, OpenDetail, ReloadTableView {
    
    func reload() {
        self.isShortListedCellLoading = true
        self.jobDetailTableView.reloadData()
    }
    
    func openRecruiterCandidateDetailVC(shortlistedUserId: String, categoryId: String, jobApplyId: String) {
        
        let candidateDetail = RecruiterCandidateDetailVC.instantiate(fromAppStoryboard: .Recruiter)
        candidateDetail.fromProfile = false
        candidateDetail.reloadDelegate = self
        candidateDetail.fromShortlistVC = true
        candidateDetail.jobId = self.jobID ?? ""
        candidateDetail.categoryId = categoryId
        candidateDetail.userId = shortlistedUserId
        candidateDetail.jobApplyId = jobApplyId
        if let nav = sharedAppDelegate.navigationController{
            nav.pushViewController(candidateDetail, animated: false)
        }
    }
    
    func openCandidateDetail(pendingUserID: String, categoryId: String, jobApplyId: String, deviceDetail: UserData) {
        
        let candidateDetail = RecruiterCandidateDetailVC.instantiate(fromAppStoryboard: .Recruiter)
        candidateDetail.fromProfile = false
        candidateDetail.fromPending = true
        candidateDetail.categoryId = categoryId
        candidateDetail.userId = pendingUserID
        candidateDetail.jobId = self.jobID ?? ""
        candidateDetail.deviceDetail = deviceDetail
        candidateDetail.jobApplyId = jobApplyId
        if let nav = sharedAppDelegate.navigationController{
            nav.pushViewController(candidateDetail, animated: false)
            sharedAppDelegate.window?.makeKeyAndVisible()
        }
    }
}

//MARK:- Prototype Cell
//=========================
class IntrestedApplicantsCell: UITableViewCell {
    
    //MARK:- Properties
    //==================
    
    //MARK:- IBoutlets
    //================
    
    @IBOutlet weak var applicantsView: UIView!
    @IBOutlet weak var intrestedApplicantsViewConstrant: NSLayoutConstraint!
    @IBOutlet weak var intrestedButton: UIButton!
    @IBOutlet weak var noApplicantsLabel: UILabel!
    @IBOutlet weak var featuredLabel: UILabel!
    @IBOutlet weak var intrestedApplicantsLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    @IBOutlet weak var imageOne: UIImageView!
    @IBOutlet weak var imageTwo: UIImageView!
    @IBOutlet weak var imageThree: UIImageView!
    @IBOutlet weak var imageFour: UIImageView!
    @IBOutlet weak var imageFive: UIImageView!
    
    @IBOutlet weak var gradientOne: GradientView!
    @IBOutlet weak var gradienTwo: GradientView!
    @IBOutlet weak var gradientThree: GradientView!
    @IBOutlet weak var gradientFour: GradientView!
    @IBOutlet weak var gradientFive: GradientView!
    
    @IBOutlet weak var labelOne: UILabel!
    @IBOutlet weak var labelTwo: UILabel!
    @IBOutlet weak var labelThree: UILabel!
    @IBOutlet weak var labelFour: UILabel!
    @IBOutlet weak var labelFive: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupIntrestedView()
        
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.intrestedApplicantsLabel.textColor = AppColors.gray152
        self.statusLabel.textColor = AppColors.gray152
        self.featuredLabel.textColor = AppColors.black46
        self.noApplicantsLabel.textColor = AppColors.black46
        
    }
    
    func setupIntrestedView() {
        self.imageOne.roundCorners()
        self.imageTwo.roundCorners()
        self.imageThree.roundCorners()
        self.imageFour.roundCorners()
        self.imageFive.roundCorners()
        self.gradientOne.roundCorners()
        self.gradienTwo.roundCorners()
        self.gradientThree.roundCorners()
        self.gradientFour.roundCorners()
        self.gradientFive.roundCorners()
        
        self.noApplicantsLabel.isHidden = true
        self.applicantsView.isHidden = true
    }
    
    func setIntrestedImageView(applicants: [String]) {
        self.applicantsView.isHidden = false
        print_debug(applicants)
        switch applicants.count {
        case 0:
            self.showImage(with: 0, applicantsImages: applicants)
        case 1:
            self.showImage(with: 1, applicantsImages: applicants)
        case 2:
            self.showImage(with: 2, applicantsImages: applicants)
        case 3:
            self.showImage(with: 3, applicantsImages: applicants)
        case 4:
            self.showImage(with: 4, applicantsImages: applicants)
        case 5:
            self.showImage(with: 5, applicantsImages: applicants)
        default:
            self.showImage(with: 6, applicantsImages: applicants)
        }
    }
    
    func showImage(with count: Int, applicantsImages: [String]) {
        
        self.applicantsView.isHidden = false
        switch count {
        case 0:
            self.imageOne.isHidden = true
            self.imageTwo.isHidden = true
            self.imageThree.isHidden = true
            self.imageFour.isHidden = true
            self.imageFive.isHidden = true
            self.gradientOne.isHidden = true
            self.gradienTwo.isHidden = true
            self.gradientThree.isHidden = true
            self.gradientFour.isHidden = true
            self.gradientFive.isHidden = true
            self.noApplicantsLabel.isHidden = false
            self.intrestedApplicantsViewConstrant.constant = 10
            
        case 1:
            
            self.imageOne.sd_setImage(with: URL(string: applicantsImages[0]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.labelOne.text = String(applicantsImages.count)
            
            self.imageTwo.isHidden = true
            self.imageThree.isHidden = true
            self.imageFour.isHidden = true
            self.imageFive.isHidden = true
            self.gradienTwo.isHidden = true
            self.gradientThree.isHidden = true
            self.gradientFour.isHidden = true
            self.gradientFive.isHidden = true
            
        case 2:
            
            self.imageOne.sd_setImage(with: URL(string: applicantsImages[0]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageTwo.sd_setImage(with: URL(string: applicantsImages[1]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.labelFour.text = String(applicantsImages.count)
            
            self.imageOne.isHidden = false
            self.imageTwo.isHidden = false
            self.gradientOne.isHidden = true
            self.gradienTwo.isHidden = true
            self.gradientThree.isHidden = true
            self.gradientFour.isHidden = true
            self.gradientFive.isHidden = true
            self.imageThree.isHidden = true
            self.imageFour.isHidden = true
            self.imageFive.isHidden = true
            
        case 3:
            
            self.gradientOne.isHidden = true
            self.gradienTwo.isHidden = true
            
            self.imageFour.isHidden = true
            self.imageFive.isHidden = true
            
            self.gradientFour.isHidden = true
            self.gradientFive.isHidden = true
            
            self.imageOne.sd_setImage(with: URL(string: applicantsImages[0]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageTwo.sd_setImage(with: URL(string: applicantsImages[1]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            
            self.imageThree.sd_setImage(with: URL(string: applicantsImages[2]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.labelThree.text = String(applicantsImages.count)
            
        case 4:
            
            self.gradientOne.isHidden = true
            self.gradienTwo.isHidden = true
            self.gradientThree.isHidden = true
            self.gradientFive.isHidden = true
            self.imageFive.isHidden = true
            
            self.imageOne.sd_setImage(with: URL(string: applicantsImages[0]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageTwo.sd_setImage(with: URL(string: applicantsImages[1]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            
            self.imageThree.sd_setImage(with: URL(string: applicantsImages[2]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageFour.sd_setImage(with: URL(string: applicantsImages[3]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            
            self.labelFour.text = String(applicantsImages.count)
            
        case 5:
            self.gradientOne.isHidden = true
            self.gradienTwo.isHidden = true
            self.gradientThree.isHidden = true
            self.gradientFour.isHidden = true
            
            self.imageOne.sd_setImage(with: URL(string: applicantsImages[0]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageTwo.sd_setImage(with: URL(string: applicantsImages[1]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageThree.sd_setImage(with: URL(string: applicantsImages[2]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageFour.sd_setImage(with: URL(string: applicantsImages[3]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageFive.sd_setImage(with: URL(string: applicantsImages[4]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.labelFive.text = String(applicantsImages.count)
            
        default:
            self.gradientOne.isHidden = true
            self.gradienTwo.isHidden = true
            self.gradientThree.isHidden = true
            self.gradientFour.isHidden = true
            
            self.imageOne.sd_setImage(with: URL(string: applicantsImages[0]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageTwo.sd_setImage(with: URL(string: applicantsImages[1]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageThree.sd_setImage(with: URL(string: applicantsImages[2]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageFour.sd_setImage(with: URL(string: applicantsImages[3]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageFive.sd_setImage(with: URL(string: applicantsImages[4]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.labelFive.text = String(applicantsImages.count)
        }
    }
    
}

class SalaryRangeCell: UITableViewCell {
    
    //MARK:- Properties
    //==================
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var salaryRangeLabel: UILabel!
    @IBOutlet weak var salaryLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var duationNameLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        self.salaryRangeLabel.textColor = AppColors.gray152
        self.salaryLabel.textColor = AppColors.black46
        self.durationLabel.textColor = AppColors.black46
        self.duationNameLabel.textColor = AppColors.gray152
        self.salaryLabel.numberOfLines = 0
    }
    
    func setJobDescData(jobDetail:MyJobListModel) {
        self.salaryRangeLabel.text  = StringConstants.JobDescription
        self.salaryLabel.text       = jobDetail.jobDescription
        
    }
    
    func setSalaryRangeData(jobDetail: MyJobListModel) {
        
        self.salaryRangeLabel.text  = StringConstants.Salary_Range
        
        if !jobDetail.perHour.isEmpty {
            
            self.salaryLabel.text       = "\(CommonClass.appendDollarInSalaryLabel(fromSalary: jobDetail.salaryFrom, toSalary: jobDetail.salaryTo)) (\(jobDetail.perHour))"
        } else {
            self.salaryLabel.text       = "\(CommonClass.appendDollarInSalaryLabel(fromSalary: jobDetail.salaryFrom, toSalary: jobDetail.salaryTo))"
        }
        self.duationNameLabel.text = StringConstants.TipsCommessions
        if jobDetail.isCommision == 1 {
            self.durationLabel.text = "Yes"
        } else {
            self.durationLabel.text = "No"
        }
        
        self.containerView.setShadow(offset: CGSize(width: 0.0, height: 2.0))
    }
}

class PendingJobCell: UITableViewCell, UIScrollViewDelegate {
    
    //MARK:- Properties
    //==================
    
    var jobId: String?
    var page = 0
    var pendingJobTitle         = StringConstants.pendingJobTitle
    var shortListedJobTitle     = StringConstants.shortListedJobTitle
    var rejectedJobTitle        = StringConstants.rejectedJobTitle
    var expiredJobTitle         = StringConstants.expiredJobTitle
    var emptyStringToManage     = StringConstants.emptyStringToManage
    var pendingJobVC            : PendingJobVC!
    var shortListedVC           : ShortListedVC!
    var rejectedVC              : RejectedVC!
    var expiredVC               : ExpiredVC!
    var segmentedControl        : WDScrollableSegmentedControl!
    var selectedIndex           = 0
    var shortlistedCancelStatus = 0
    
    //MARK:- IBoutlets
    //================
    
    @IBOutlet weak var pendingScrollView: UIScrollView!
    @IBOutlet weak var topBarScrollView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupScrollView()
        self.instantiateSegmentControl()
    }
    
    func instantiateSegmentControl() {
        
        self.segmentedControl = WDScrollableSegmentedControl(frame: CGRect(x: 0, y: 0, width: SCREEN_WIDTH, height: topBarScrollView.frame.height) )
        self.topBarScrollView.addSubview(segmentedControl)
        
        self.segmentedControl.setButtonColor(AppColors.themeBlueColor, for: .highlighted)
        self.segmentedControl.setButtonColor(AppColors.themeBlueColor, for: .selected)
        self.segmentedControl.indicatorColor = AppColors.themeBlueColor
        self.segmentedControl.indicatorHeight = 3.0
        self.segmentedControl.setButtonColor(AppColors.gray152, for: .normal)
        
        self.segmentedControl.isChangedSegmentIndicator = false
        self.segmentedControl.buttons = [self.pendingJobTitle, self.shortListedJobTitle, self.rejectedJobTitle, self.expiredJobTitle, self.emptyStringToManage]
        
        self.segmentedControl.font = AppFonts.Poppins_Medium.withSize(15)
        self.segmentedControl.delegate = self
        
    }
    
    func setupSegmentControlButton() {
        
        self.segmentedControl.isChangedSegmentIndicator = true
        self.segmentedControl.buttons = [self.pendingJobTitle, self.shortListedJobTitle, self.rejectedJobTitle, self.expiredJobTitle, self.emptyStringToManage]
        self.segmentedControl.font = AppFonts.Poppins_Medium.withSize(15)
    }
    
    private func setupScrollView() {
        
        self.pendingScrollView.delegate = self
        
        self.pendingJobVC = PendingJobVC.instantiate(fromAppStoryboard: .Recruiter)
        
        self.pendingJobVC.view.frame.origin = CGPoint(x: 0, y: 0)
        self.pendingJobVC.delegate = self
        
        self.pendingScrollView.frame = self.pendingJobVC.view.frame
        self.pendingScrollView.addSubview(self.pendingJobVC.view)
        
        self.shortListedVC = ShortListedVC.instantiate(fromAppStoryboard: .Recruiter)
        self.shortListedVC.jobID = self.jobId ?? ""
        self.shortListedVC.view.frame.origin = CGPoint(x: SCREEN_WIDTH, y: 0)
        self.shortListedVC.delegate = self
        
        self.pendingScrollView.frame = self.shortListedVC.view.frame
        self.pendingScrollView.addSubview(self.shortListedVC.view)
        
        self.rejectedVC = RejectedVC.instantiate(fromAppStoryboard: .Recruiter)
        self.rejectedVC.jobID = self.jobId ?? ""
        self.rejectedVC.view.frame.origin = CGPoint(x: 2 * SCREEN_WIDTH, y: 0)
        self.rejectedVC.delegate = self
        self.pendingScrollView.frame = self.rejectedVC.view.frame
        self.pendingScrollView.addSubview(self.rejectedVC.view)
        
        self.expiredVC = ExpiredVC.instantiate(fromAppStoryboard: .Recruiter)
        self.expiredVC.jobID = self.jobId ?? ""
        self.expiredVC.delegate = self
        self.expiredVC.view.frame.origin = CGPoint(x: 3 * SCREEN_WIDTH, y: 0)
        self.pendingScrollView.frame = self.expiredVC.view.frame
        self.pendingScrollView.addSubview(self.expiredVC.view)
        
        self.pendingScrollView.contentSize = CGSize(width: 4 * SCREEN_WIDTH, height: 1)
        self.pendingScrollView.isPagingEnabled = true
        
    }
    
    
    func ChangeSegmentTitle(headerData: HeaderJobCount) {
        
        switch headerData.type {
        case 1:
            self.pendingJobTitle = "\(headerData.title) (\(headerData.count)) "
            
        case 2:
            self.shortListedJobTitle = "\(headerData.title) (\(headerData.count)) "
            
        case 3:
            self.rejectedJobTitle = "\(headerData.title) (\(headerData.count)) "
            
        case 4:
            
            self.expiredJobTitle = "\(headerData.title) (\(headerData.count)) "
            
        default:
            return
        }
    }
    
    
    func getData(index: Int) {
        
        switch index {
        case 0:
            self.pendingJobVC.page = 0
            self.pendingJobVC.pendingJobDetail.removeAll()
            self.pendingJobVC.getJobPendingDetail(jobId: self.jobId ?? "", loader: false)
        case 1:
            self.shortListedVC.cancelJobStatus = self.shortlistedCancelStatus
            self.shortListedVC.page = 0
            self.shortListedVC.pendingJobDetail.removeAll()
            self.shortListedVC.getShortlistedJobDetail(jobId: self.jobId ?? "", loader: false)
            
        case 2:
            self.rejectedVC.page = 0
            self.rejectedVC.pendingJobDetail.removeAll()
            self.rejectedVC.getRejectedJobDetail(jobId: self.jobId ?? "", loader: false)
            
        case 3:
            self.expiredVC.page = 0
            self.expiredVC.pendingJobDetail.removeAll()
            self.expiredVC.getExpiredJobDetail(jobId: self.jobId ?? "", loader: false)
        default:
            return
        }
    }
    
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let offsetX = scrollView.contentOffset.x
        let scrolledIndex = offsetX/SCREEN_WIDTH
        
        if (scrolledIndex.truncatingRemainder(dividingBy: 1) == 0) {
            let index = scrolledIndex
            self.selectedIndex = Int(index)
            self.segmentedControl.selectedIndex = Int(index)
            self.getData(index: Int(index))
        }
    }
}

//MARK:- Extension for WeekViewDelegate, WDScrollableSegmentedControlDelegate Delegate
//====================================================================================
extension PendingJobCell: WDScrollableSegmentedControlDelegate {
    
    func didSelectButton(at index: Int) {
        
        //self.isHandledByWdScrollDelegate = true
        let point = CGPoint(x: CGFloat(index) * SCREEN_WIDTH, y: 0)
        self.selectedIndex = index
        
        self.getData(index: index)
        self.pendingScrollView.setContentOffset(point, animated: true)
        
    }
}

extension PendingJobCell: PendingJobCountHeader {
    
    
    func getPendingJobCount( headerdata: [HeaderJobCount]) {
        
        headerdata.forEach { (data) in
            self.ChangeSegmentTitle(headerData: data)
        }
        
        self.setupSegmentControlButton()
        print_debug(self.selectedIndex)
        self.segmentedControl.selectedIndex = self.selectedIndex
    }
}


extension PendingJobCell: ShortListedJobCountHeader {
    
    func getShortListedJobCount( headerdata: [HeaderJobCount]) {
        headerdata.forEach { (data) in
            self.ChangeSegmentTitle(headerData: data)
        }
        
        self.setupSegmentControlButton()
        print_debug(self.selectedIndex)
        self.segmentedControl.selectedIndex = self.selectedIndex
    }
}

extension PendingJobCell: RejectedJobCountHeader {
    
    func getRejectedJobCount( headerdata: [HeaderJobCount]) {
        
        headerdata.forEach { (data) in
            self.ChangeSegmentTitle(headerData: data)
        }
        
        self.setupSegmentControlButton()
        print_debug(self.selectedIndex)
        self.segmentedControl.selectedIndex = self.selectedIndex
    }
}

extension PendingJobCell: ExpiredJobCountHeader {
    
    func getExpiredJobCount( headerdata: [HeaderJobCount]) {
        
        headerdata.forEach { (data) in
            self.ChangeSegmentTitle(headerData: data)
        }
        
        self.setupSegmentControlButton()
        print_debug(self.selectedIndex)
        
        self.segmentedControl.selectedIndex = self.selectedIndex
    }
}



