//
//  PostJobContainerVC.swift
//  JobGet
//
//  Created by macOS on 31/05/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class PostJobContainerVC: BaseVC {
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var navigationView: UIView!
    
    //Step progress animation outlets.
    @IBOutlet weak var stepImage1: UIImageView!
    @IBOutlet weak var stepImage2: UIImageView!
    @IBOutlet weak var stepImage3: UIImageView!
    @IBOutlet weak var stepImage4: UIImageView!
    @IBOutlet weak var stepLabel1: UILabel!
    @IBOutlet weak var stepLabel2: UILabel!
    @IBOutlet weak var stepLabel3: UILabel!
    @IBOutlet weak var stepLabel4: UILabel!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var jobDescriptionLabel: UILabel!
    @IBOutlet weak var jobTypeLabel: UILabel!
    @IBOutlet weak var experienceLabel: UILabel!
    @IBOutlet weak var addImageLabel: UILabel!
    
    
    var jobDescObj          : JobDescriptionVC!
    //   var jobLocationObj      : JobLocationVC!
    var salaryObj           : SalaryVC!
    var requireExperianceObj: RequireExperianceVC!
    // var companyDescObj      : CompanyDescriptionVC!
    var featureObj          : FeaturedPostVC!
    var addImageObj         : AddImageVC!
    
    var count = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
}

//MARK: Private Function
//===========================
private extension PostJobContainerVC {
    
    func initialSetup() {
        self.roundViews()
        self.navigationView.setShadow()
        self.jobDescriptionLabel.textColor = AppColors.themeBlueColor
        self.jobTypeLabel.textColor = AppColors.lightGray
        self.experienceLabel.textColor = AppColors.lightGray
        self.addImageLabel.textColor = AppColors.lightGray
        self.stepLabel2.backgroundColor = AppColors.progressBarGray
        self.stepLabel3.backgroundColor = AppColors.progressBarGray
        self.stepLabel4.backgroundColor = AppColors.progressBarGray
        self.stepLabel2.textColor = AppColors.lightGray103
        self.stepLabel3.textColor = AppColors.lightGray103
        self.stepLabel4.textColor = AppColors.lightGray103
        
        self.progressView.progressTintColor = AppColors.themeBlueColor
        self.progressView.trackTintColor = AppColors.progressBarGray
        CommonClass.setNextButton(button: self.nextButton)
        
        self.experienceLabel.text = "Experience"
        self.isEnableNextBtn(status: false)
        self.addCategoryChildView()
    }
    
    func roundViews(){
        
        self.stepLabel1.roundCorners()
        self.stepLabel2.roundCorners()
        self.stepLabel3.roundCorners()
        self.stepLabel4.roundCorners()
        self.stepImage1.roundCorners()
        self.stepImage2.roundCorners()
        self.stepImage3.roundCorners()
        self.stepImage4.roundCorners()
        
    }
    
    func isEnableNextBtn(status: Bool) {
        
        status ? (self.nextButton.backgroundColor = AppColors.themeBlueColor) : (self.nextButton.backgroundColor = AppColors.buttonDisableBgColor)
        self.nextButton.isEnabled = status
    }
    
    func addCategoryChildView()  {
        
        let jobDescVC = JobDescriptionVC.instantiate(fromAppStoryboard: .PostJob)
        jobDescVC.view.frame =  self.containerView.bounds
        jobDescVC.delegate = self
        self.containerView.addSubview(jobDescVC.view)
        self.isEnableNextBtn(status: jobDescVC.checkButtonStatus())
        self.addChildViewController(jobDescVC)
        self.view.layoutIfNeeded()
    }
    
    func setStepTitleTextColor(counts: Int){
        
        switch counts {
        case 1:
            self.jobDescriptionLabel.textColor = AppColors.themeBlueColor
            self.jobTypeLabel.textColor = AppColors.lightGray
            self.experienceLabel.textColor = AppColors.lightGray
            self.addImageLabel.textColor = AppColors.lightGray
            
        case 2:
            self.jobDescriptionLabel.textColor = AppColors.lightGray
            self.jobTypeLabel.textColor = AppColors.themeBlueColor
            self.experienceLabel.textColor = AppColors.lightGray
            self.addImageLabel.textColor = AppColors.lightGray
            
        case 3:
            self.jobDescriptionLabel.textColor = AppColors.lightGray
            self.jobTypeLabel.textColor = AppColors.lightGray
            self.experienceLabel.textColor = AppColors.themeBlueColor
            self.addImageLabel.textColor = AppColors.lightGray
            
        case 4:
            self.jobDescriptionLabel.textColor = AppColors.lightGray
            self.jobTypeLabel.textColor = AppColors.lightGray
            self.experienceLabel.textColor = AppColors.lightGray
            self.addImageLabel.textColor = AppColors.themeBlueColor
            
        case 5:
            
            self.jobDescriptionLabel.textColor = AppColors.lightGray
            self.jobTypeLabel.textColor = AppColors.lightGray
            self.experienceLabel.textColor = AppColors.themeBlueColor
            self.addImageLabel.textColor = AppColors.lightGray
        case 6:
            self.jobDescriptionLabel.textColor = AppColors.lightGray
            self.jobTypeLabel.textColor = AppColors.lightGray
            self.experienceLabel.textColor = AppColors.lightGray
            self.addImageLabel.textColor = AppColors.lightGray
        case 7:
            
            self.jobDescriptionLabel.textColor = AppColors.lightGray
            self.jobTypeLabel.textColor = AppColors.lightGray
            self.experienceLabel.textColor = AppColors.lightGray
            self.addImageLabel.textColor = AppColors.themeBlueColor
            
        case 8:
            
            self.jobDescriptionLabel.textColor = AppColors.lightGray
            self.jobTypeLabel.textColor = AppColors.lightGray
            self.experienceLabel.textColor = AppColors.lightGray
            self.addImageLabel.textColor = AppColors.lightGray
        default:
            return
        }
    }
    
    func addChildView() {
        
        if count < 4 {
            if SharedJobPost.totalExperience == "0" { // can't allow 0 month exeperiance
                CommonClass.showToast(msg: StringConstants.SelectSuitableExp)
                return
            }else if count == 1 {
                self.view.endEditing(true)
                let toSalary = Double(SharedJobPost.salaryTo.replacingOccurrences(of: ",", with: "")) ?? 0.0
                let fromSalary = Double(SharedJobPost.salaryFrom.replacingOccurrences(of: ",", with: "")) ?? 0.0
                
                if fromSalary == 0 && toSalary > 0{
                    CommonClass.showToast(msg: StringConstants.Please_Enter_From_Salary)
                    
                    return
                } else if fromSalary > 0 && toSalary == 0{
                    CommonClass.showToast(msg: StringConstants.Please_Enter_To_Salary)
                    
                    return
                } else  if fromSalary > toSalary {
                    CommonClass.showToast(msg: StringConstants.To_Salary_Greater_Then_From_Salary)
                    
                    return
                }else if fromSalary > 0 && toSalary > 0 && SharedJobPost.duration.isEmpty{
                    CommonClass.showToast(msg: StringConstants.Please_select_duration)
                    
                    return
                }else if fromSalary > 0 && SharedJobPost.duration.isEmpty{
                    CommonClass.showToast(msg: StringConstants.Please_select_duration)
                    
                    return
                }else if fromSalary == 0 && toSalary == 0 && !SharedJobPost.duration.isEmpty{
                    CommonClass.showToast(msg: StringConstants.Please_Enter_From_Salary)
                    
                    return
                }
                else {
                    count += 1
                }
            }
            else {
                count += 1
            }
        } else {
            return
        }
        switch count {
            
        case 1:
            salaryObj = SalaryVC.instantiate(fromAppStoryboard: .PostJob)
            salaryObj.delegate = self
            salaryObj.view.frame =  self.containerView.bounds
            self.containerView.addSubview(salaryObj.view)
            self.addChildViewController(salaryObj)
            self.isEnableNextBtn(status: true)
            
        case 2:
            requireExperianceObj = RequireExperianceVC.instantiate(fromAppStoryboard: .PostJob)
            requireExperianceObj.delegate = self
            requireExperianceObj.view.frame = self.containerView.bounds
            self.containerView.addSubview(requireExperianceObj.view)
            self.addChildViewController(requireExperianceObj)
            self.isEnableNextBtn(status: true)
            
        case 3:
//            if SharedJobPost.experienceType == 1{
//                CommonClass.showToast(msg: "Minimum experience can't be 0")
//                return
//            }
            addImageObj = AddImageVC.instantiate(fromAppStoryboard: .PostJob)
            addImageObj.view.frame = self.containerView.bounds
            self.containerView.addSubview(addImageObj.view)
            self.addChildViewController(addImageObj)
            self.isEnableNextBtn(status: true)
        case 4:
            let sceen = PreviewJobsVC.instantiate(fromAppStoryboard: .PostJob)
            self.navigationController?.pushViewController(sceen, animated: true)
//            let featureObj = FeaturedPostVC.instantiate(fromAppStoryboard: .PostJob)
//            self.navigationController?.pushViewController(featureObj, animated: true)
            self.count -= 1
            
        default:
            return
        }
        self.setProgressBar()

    }
    func setProgressBar(){
        let counts = self.count+1
        self.setStepTitleTextColor(counts: counts)
        
        switch counts {
        case 1:
            self.progressView.animate(duration: 0.2, progress: 0.0)
            self.stepLabel1.textColor = AppColors.white
            self.stepLabel2.textColor = AppColors.lightGray103
            self.stepLabel3.textColor = AppColors.lightGray103
            self.stepLabel4.textColor = AppColors.lightGray103
            self.stepLabel1.backgroundColor = AppColors.themeBlueColor
            self.stepLabel2.backgroundColor = AppColors.progressBarGray
            self.stepLabel3.backgroundColor = AppColors.progressBarGray
            self.stepLabel4.backgroundColor = AppColors.progressBarGray
            self.stepLabel1.isHidden = false
            self.stepLabel2.isHidden = false
            self.stepLabel3.isHidden = false
            self.stepLabel4.isHidden = false
            
        case 2:
            self.progressView.animate(duration: 0.1, progress: 0.3)
            
            self.stepLabel1.textColor = AppColors.white
            self.stepLabel2.textColor = AppColors.white
            self.stepLabel3.textColor = AppColors.lightGray103
            self.stepLabel4.textColor = AppColors.lightGray103
            self.stepLabel1.backgroundColor = AppColors.themeBlueColor
            self.stepLabel2.backgroundColor = AppColors.themeBlueColor
            self.stepLabel3.backgroundColor = AppColors.progressBarGray
            self.stepLabel4.backgroundColor = AppColors.progressBarGray
            self.stepLabel1.isHidden = true
            self.stepLabel2.isHidden = false
            self.stepLabel3.isHidden = false
            self.stepLabel4.isHidden = false
        case 3:
            self.progressView.animate(duration: 0.1, progress: 0.7)
            self.stepLabel1.textColor = AppColors.white
            self.stepLabel2.textColor = AppColors.white
            self.stepLabel3.textColor = AppColors.white
            self.stepLabel4.textColor = AppColors.lightGray103
            self.stepLabel1.backgroundColor = AppColors.themeBlueColor
            self.stepLabel2.backgroundColor = AppColors.themeBlueColor
            self.stepLabel3.backgroundColor = AppColors.themeBlueColor
            self.stepLabel4.backgroundColor = AppColors.progressBarGray
            self.stepLabel1.isHidden = true
            self.stepLabel2.isHidden = true
            self.stepLabel3.isHidden = false
            self.stepLabel4.isHidden = false
            
        case 4:
            self.progressView.animate(duration: 0.1, progress: 1.0)
            self.stepLabel1.textColor = AppColors.white
            self.stepLabel2.textColor = AppColors.white
            self.stepLabel3.textColor = AppColors.white
            self.stepLabel4.textColor = AppColors.white
            self.stepLabel1.backgroundColor = AppColors.themeBlueColor
            self.stepLabel2.backgroundColor = AppColors.themeBlueColor
            self.stepLabel3.backgroundColor = AppColors.themeBlueColor
            self.stepLabel4.backgroundColor = AppColors.themeBlueColor
            
            self.stepLabel1.isHidden = true
            self.stepLabel2.isHidden = true
            self.stepLabel3.isHidden = true
            self.stepLabel4.isHidden = false
            
        default: break
        }
    }
    
    func removeChildView() {
        print_debug(self.count)
        switch self.count {
        case 0:
            self.pop()
            
        case 1:
            self.salaryObj.removeFromParent
            self.salaryObj.view.removeFromSuperview()
            self.isEnableNextBtn(status: true)
        case 2:
            if SharedJobPost.totalExperience == "0" {
                SharedJobPost.totalExperience = ""
                SharedJobPost.rawExpValue = nil
            }
            self.requireExperianceObj.removeFromParent
            self.requireExperianceObj.view.removeFromSuperview()
            self.isEnableNextBtn(status: true)
            
        case 3:
            
            self.addImageObj.removeFromParent
            self.addImageObj.view.removeFromSuperview()
            self.isEnableNextBtn(status: true)
            
        default :
            return
        }
        
        self.count -= 1
        self.setProgressBar()
    }
}

//MARK: IB Action and Target
//===========================
extension PostJobContainerVC {
    
    @IBAction func nextButtonTapped(_ sender: UIButton) {
        
        self.addChildView()
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.removeChildView()
    }
}

extension PostJobContainerVC: EnableNextButtonProtocol {
    
    func isEnableNextButton(status: Bool) {
        self.isEnableNextBtn(status: status)
    }
    
}

