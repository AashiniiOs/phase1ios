//
//  PreviewJobVC.swift
//  JobGet
//
//  Created by macOS on 09/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import SwiftyJSON

class PreviewJobVC: BaseVC {
    
    //MARK:- Properties
    //==================
    
    var jobType = ""
    var fromSalary = ""
    var toSalary = ""
    
    //MARK:- IBoutlets
    //================
    
    @IBOutlet weak var navigationBGView: UIView!
    
    @IBOutlet weak var jobCategoryContainerView: UIView!
    @IBOutlet weak var jobCategoryLabel: UILabel!
    @IBOutlet weak var companyNameContainerView: UIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var previewJobScrollView: UIScrollView!
    @IBOutlet weak var jobImageView: UIImageView!
    @IBOutlet weak var jobNameLabel: UILabel!
    @IBOutlet weak var jobDescriptionNameLabel: UILabel!
    @IBOutlet weak var jobLocationLabel: UILabel!
    @IBOutlet weak var jobTypeLabel: UILabel!
    @IBOutlet weak var jobDescriptionLabel: UILabel!
    @IBOutlet weak var jobTypeNameLabel: UILabel!
    @IBOutlet weak var jobLocationNameLabel: UILabel!
    @IBOutlet weak var experianceLabel: UILabel!
    @IBOutlet weak var salaryLabel: UILabel!
    @IBOutlet weak var salaryNameLabel: UILabel!
    @IBOutlet weak var experianceNameLabel: UILabel!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var companyLabel: UILabel!
    @IBOutlet weak var postButton: UIButton!
    @IBOutlet weak var imageCompanyLabel: UILabel!
    @IBOutlet weak var navigationView: UIView!
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
}

//MARK:- Private Methods
//=======================
private extension PreviewJobVC {
    
    func initialSetup() {
        
        
        self.setupLabelText()
        self.setupView()
        
    }
    
    func setupView() {
        
        self.navigationView.setShadow(offset: CGSize(width: 0.0, height: 2.0))
        self.jobImageView.setCorner(cornerRadius: 5, clip: true)
        self.postButton.backgroundColor = AppColors.themeBlueColor
        self.postButton.setCorner(cornerRadius: 5, clip: true)
        self.companyNameContainerView.roundCornersFromOneSide([.bottomLeft, .bottomRight], radius: 5.0)
        self.jobDescriptionLabel.textColor = AppColors.black46
        self.jobTypeLabel.textColor = AppColors.black46
        self.jobLocationLabel.textColor = AppColors.black46
        self.salaryLabel.textColor = AppColors.black46
        self.experianceLabel.textColor = AppColors.black46
        self.companyLabel.textColor = AppColors.black46
        self.jobCategoryLabel.font = AppFonts.Poppins_Regular.withSize(14)
        self.jobDescriptionNameLabel.textColor = AppColors.gray152
        self.jobTypeNameLabel.textColor = AppColors.gray152
        self.jobLocationNameLabel.textColor = AppColors.gray152
        self.salaryNameLabel.textColor = AppColors.gray152
        self.experianceNameLabel.textColor = AppColors.gray152
        self.companyNameLabel.textColor = AppColors.gray152
        self.jobCategoryLabel.textColor = AppColors.black46
        self.jobCategoryContainerView.backgroundColor = #colorLiteral(red: 0.5960784314, green: 0.5960784314, blue: 0.5960784314, alpha: 0.2)
        self.jobCategoryContainerView.setCorner(cornerRadius: self.jobCategoryContainerView.height/2 , clip: true)
        self.jobNameLabel.textColor = AppColors.black46
    }
    
    func setupLabelText() {
        
        self.jobDescriptionLabel.text = SharedJobPost.jobDesc
        self.jobType = CommonClass.getJobType(jobType:  SharedJobPost.jobType)
        self.jobTypeLabel.text = self.jobType
        self.jobLocationLabel.text = SharedJobPost.address
        
        var duration = ""
        if !SharedJobPost.duration.isEmpty{
            duration = "(\(SharedJobPost.duration))"
        }
        
        self.salaryLabel.text = "\(CommonClass.appendDollarInSalaryLabel(fromSalary: SharedJobPost.salaryFrom, toSalary: SharedJobPost.salaryTo)) \(duration)"
        
        if !SharedJobPost.totalExperience.isEmpty {
            if SharedJobPost.experienceType == 1 {
                self.experianceLabel.text = "\(StringConstants.upTo) \(SharedJobPost.totalExperience) \(StringConstants.Month.lowercased())"
            } else {
                self.experianceLabel.text = "\(StringConstants.upTo) \(SharedJobPost.totalExperience) \(StringConstants.Year.lowercased())"
            }
            
        } else {
            self.experianceLabel.text = StringConstants.NoExpRequired
        }
        
        
        self.companyLabel.text          = SharedJobPost.companyName
        self.imageCompanyLabel.text     = SharedJobPost.companyName
        self.jobNameLabel.text              = SharedJobPost.jobTitle
        self.jobCategoryLabel.text = SharedJobPost.categoryName
        if let image = SharedJobPost.rawJobImage {
            self.jobImageView.image = image
        }
    }
}

//MARK:- Web Services
//====================
extension PreviewJobVC {
    
    func postJob() {
        
        SharedJobPost.recruiterId = AppUserDefaults.value(forKey: .userId).stringValue
        print_debug(SharedJobPost.dictionary())
        WebServices.jobPost(parameters: SharedJobPost.dictionary(), success: { (json) in
            if let vc = self.navigationController?.viewControllers {
                for controllers in vc{
                    if controllers is RecruiterTabBarVC {
                        self.navigationController?.popToViewController(controllers, animated: true)
                    }
                }
            }
            self.navigationController?.popToRootViewController()
        }) { (error: Error) in
            CommonClass.showToast(msg: error.localizedDescription )
        }
        
    }
}

//MARK: - IB Action and Target
//===============================
extension PreviewJobVC {
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        //self.dismiss(animated: true, completion: nil)
        self.pop()
    }
    
    @IBAction func postJobButtonTapped(_ sender: UIButton) {
        self.postJob()
    }
    
}




