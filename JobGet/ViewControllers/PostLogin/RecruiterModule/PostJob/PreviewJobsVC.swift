//
//  PreviewJobsVC.swift
//  JobGet
//
//  Created by macOS on 09/07/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import MXParallaxHeader
import GoogleMaps

class PreviewJobsVC: UIViewController {
    
    //MARK:- Properties
    //==================
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var headerImageTopConstr: NSLayoutConstraint!
    @IBOutlet var headerView: UIView!
    
    @IBOutlet weak var gradientView: UIView!
    //    @IBOutlet weak var containerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var jobNameLabel: UILabel!
    @IBOutlet weak var jobImageView: UIImageView!
    @IBOutlet weak var jobNameContainerView: UIView!
    
    @IBOutlet weak var navigationBGView: UIView!
    
    @IBOutlet weak var previewTableView: UITableView!
    @IBOutlet weak var screenHeading: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var applyButton: UIButton!
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        self.registerCell()
        self.setupCornerRadiusAndFonts()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:-
    //MARK:- Private Methods
    private func initialSetup(){
        
        self.previewTableView.delegate = self
        self.previewTableView.dataSource = self
        self.previewTableView.tableHeaderView = self.headerView
        if SCREEN_HEIGHT < 800{
            self.headerImageTopConstr.constant = 0
        }else{
            self.headerImageTopConstr.constant = -25
        }
    }
    
    private func registerCell() {
        
        let nib = UINib(nibName: "JobDetailCell", bundle: nil)
        self.previewTableView.register(nib, forCellReuseIdentifier: "JobDetailCell")
        self.previewTableView.register(UINib(nibName: "JobDescCell", bundle: nil), forCellReuseIdentifier: JobDescCell.identifier)
        self.populateDataInToParallexHeader()
    }
    
    private func setupCornerRadiusAndFonts() {
        CommonClass.setNextButton(button: self.applyButton)
        self.previewTableView.setShadow(offset: CGSize(width: 0.0, height: 2.0))
    }
    
    func populateDataInToParallexHeader() {
        
        // if let details = candidateJobDetail {
        // let image = details.jobImage.replacingOccurrences(of: "\"", with: "")
        // self.jobImageView.sd_setImage(with: URL(string: SharedJobPost.jobImage), placeholderImage: #imageLiteral(resourceName: "icHomeCategoryPlaceholder") , completed: nil)
        if let image = SharedJobPost.rawJobImage {
            self.jobImageView.image = image
        }
        self.jobNameLabel.text = SharedJobPost.companyName
        
    }
    
    //MARK: - IB Action and Target
    //MARK: -
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func postJobBtnTapped(_ sender: UIButton) {
        self.postJob()
    }
    
    @objc private func locationBtnTapped(_ sender: UIButton){
        if (UIApplication.shared.canOpenURL(URL(string:"comgooglemaps://")!)) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(URL(string:"comgooglemaps://?center=\(SharedJobPost.latitude ?? 42.361145),\(SharedJobPost.longitude  ?? -71.057083)&zoom=14&views=traffic&q=\(SharedJobPost.latitude ?? 42.361145),\(SharedJobPost.longitude ?? -71.057083)")!, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
            }
        } else {
            print("Can't use comgooglemaps://")
        }
    }
}

//MARK:- Web Services
//====================
extension PreviewJobsVC {
    
    func postJob() {
        
        
        SharedJobPost.recruiterId = AppUserDefaults.value(forKey: .userId).stringValue
        WebServices.jobPost(parameters: SharedJobPost.dictionary(), success: { (json) in
            if let vc = self.navigationController?.viewControllers {
                _ = vc.map{  controllers in
                    if controllers is RecruiterTabBarVC {
                        self.navigationController?.popToViewController(controllers, animated: true)
                    }
                }
            }
            NotificationCenter.default.post(name: NSNotification.Name("Animating_Sptlight_Icon"), object: ["jobId" : json["data"]["_id"].stringValue])
//            self.navigationController?.popToRootViewController()
//            let sceen = FeaturedPostVC.instantiate(fromAppStoryboard: .PostJob)
//            sceen.jobId = json["data"]["_id"].stringValue
//            sceen.isFromJobPost = true
//            self.navigationController?.pushViewController(sceen, animated: true)
            
        }) { (error: Error) in
            CommonClass.showToast(msg: error.localizedDescription )
        }
    }
}

//MARK:-
//MARK:- Table view Delegate and DataSource
extension PreviewJobsVC: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.00001
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "JobDetailCell") as? JobDetailCell else { fatalError("invalid cell \(self)")
            }
            cell.chatContainerView.isHidden = false
            
            cell.jobDistanceLabel.isHidden = false
            cell.yearContainerView.isHidden = true
            cell.bookMarkBtn.isHidden = true
            cell.timeButton.isHidden = true
            cell.jobSeenCountButton.isHidden = true
            cell.jobDistanceLabel.isHidden = true
            cell.chatContainerView.isHidden = true
            cell.timeButtonTopConstrant.constant = 0
            cell.locationBtnTopConstraint.constant = 0
            cell.locationBtn.addTarget(self, action: #selector(self.locationBtnTapped(_:)), for: .touchUpInside)
            cell.populatePostJobDetail(data: SharedJobPost)
            
            return cell
            
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "NameCell") as? NameCell else { fatalError("invalid cell \(self)")
            }
            cell.employeeNameLabel.text = StringConstants.EmployerName
            
            cell.populateEmployerName()
            
            return cell
            
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: JobDescCell.identifier) as? JobDescCell else { fatalError("invalid cell \(self)")
            }
            
            cell.populateDescripton(data : SharedJobPost)
            return cell
        case 3:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "Salarycell") as? Salarycell else { fatalError("invalid cell \(self)")
            }
            
            cell.populateSalary(data: SharedJobPost)
            
            return cell
        case 4:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "NameCell") as? NameCell else { fatalError("invalid cell \(self)")
            }
            
            cell.employeeNameLabel.text = StringConstants.ExperienceRequired.replace(string: " ?", withString: "")
            
            if SharedJobPost.totalExperience.isEmpty || SharedJobPost.totalExperience == "0"{
                cell.nameLabel.text       = StringConstants.NoExpRequired
            } else if SharedJobPost.experienceType == 1 {
                cell.nameLabel.text       = "\(StringConstants.upTo) \(SharedJobPost.totalExperience) \(StringConstants.Months)"
            } else if SharedJobPost.experienceType == 2 {
                cell.nameLabel.text       = "\(StringConstants.upTo) \(SharedJobPost.totalExperience) \(StringConstants.Years)"
            }
            
            cell.seperatorView.isHidden = true
            return cell
            
        case 5:
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "LocationCell") as? LocationCell else { fatalError("invalid cell \(self)")
            }
            
            
            let marker = GMSMarker()
            if let lat = SharedJobPost.latitude, let long = SharedJobPost.longitude{
                marker.position = CLLocationCoordinate2DMake(lat, long)
            }
            
            marker.icon = #imageLiteral(resourceName: "icRedMapPin")
            marker.map = cell.mapView
            cell.mapView.camera = GMSCameraPosition.camera(withTarget: marker.position, zoom: 13)
            cell.mapView.delegate = self
            
            return cell
        default:
            fatalError("invalid cell")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 5 {
            return 200
        } else {
            return UITableViewAutomaticDimension
        }
    }
    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//
//        return self.parallexHeaderView
//    }
//
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 240.0
//    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.y > 170 {
            self.navigationBGView.backgroundColor = AppColors.themeBlueColor
            self.navigationView.backgroundColor = AppColors.themeBlueColor
        }else{
            self.navigationBGView.backgroundColor = UIColor.clear
            self.navigationView.backgroundColor = UIColor.clear
        }
    }
}



//MARK: - GMSMAP Delegate
//===============================
extension PreviewJobsVC: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, markerInfoWindow marker: GMSMarker) -> UIView? {
        
        let customInfoWindow = Bundle.main.loadNibNamed("MapInfoWindowView", owner: self, options: nil)![0] as! MapInfoWindowView
        
        customInfoWindow.frame.size = CGSize(width: 150, height: 80)
        customInfoWindow.addressLabel.textColor = AppColors.black46
        customInfoWindow.distanceLabel.textColor = AppColors.gray152
        
        customInfoWindow.addressLabel.text = SharedJobPost.companyName
        customInfoWindow.distanceLabel.text = ""
        marker.infoWindowAnchor = CGPoint(x: 1.0, y: -0.1)
        return customInfoWindow
    }
}

//MARK: -
//MARK: - Prototype Cell
class LocationCell: UITableViewCell {
    
    @IBOutlet weak var mapView: GMSMapView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
    }
}


class NameCell: UITableViewCell {
    
    @IBOutlet weak var employeeNameLabel: UILabel!
    @IBOutlet weak var seperatorView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.employeeNameLabel.textColor = AppColors.gray152
        self.nameLabel.textColor = AppColors.black46
    }
    
    func populateEmployerName() {
        let userData = AppUserDefaults.value(forKey: .userData)
        let firstName = userData["first_name"]
        let lastName = userData["last_name"]
        self.nameLabel.text = "\(AppUserDefaults.value(forKey: .firstName).stringValue.isEmpty ? firstName.stringValue : AppUserDefaults.value(forKey: .firstName).stringValue) \(AppUserDefaults.value(forKey: .lastName).stringValue.isEmpty ? lastName.stringValue : AppUserDefaults.value(forKey: .lastName).stringValue)"
    }
    
}

class Salarycell: UITableViewCell {
    
    
    @IBOutlet weak var tipsCpmmesionLabel: UILabel!
    @IBOutlet weak var tipLabel: UILabel!
    @IBOutlet weak var salaryRangeLabel: UILabel!
    @IBOutlet weak var salaryLabel: UILabel!
    @IBOutlet weak var seperatorView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.salaryRangeLabel.textColor = AppColors.gray152
        self.tipsCpmmesionLabel.textColor = AppColors.gray152
        self.tipLabel.textColor = AppColors.black46
        self.salaryLabel.textColor = AppColors.black46
        
    }
    
    func populateSalary(data: JobPostModel) {
        
        if !data.duration.isEmpty {
            self.salaryLabel.text = "\(CommonClass.appendDollarInSalaryLabel(fromSalary: data.salaryFrom, toSalary: data.salaryTo)) (\(data.duration))"
        } else {
            self.salaryLabel.text = "\(CommonClass.appendDollarInSalaryLabel(fromSalary: data.salaryFrom, toSalary: data.salaryTo))"
            
        }
        
        self.tipsCpmmesionLabel.text = StringConstants.TipsCommessions
        if data.isCommisionRequired == 0 {
            self.tipLabel.text = StringConstants.no
        } else {
            self.tipLabel.text = StringConstants.yes
        }
    }
}
