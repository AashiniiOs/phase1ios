//
//  RequireExperianceVC.swift
//  JobGet
//
//  Created by macOS on 11/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import CircularSlider

enum RequiredExperiance: String {
    case yes = "1"
    case no = "2"
}

class RequireExperianceVC: BaseVC {
    
    //MARK:- Properties
    //==================
    
    var requiredExperiance:RequiredExperiance = .no
    // var circularSlider: CircularSlider!
    weak var delegate: EnableNextButtonProtocol?

    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var yesButton: UIButton!
    @IBOutlet weak var noButton: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var chooseExperianceLabel: UILabel!
    @IBOutlet weak var sliderContainerView: CircularSlider!
    @IBOutlet weak var sliderValueLabel: UILabel!
    @IBOutlet weak var dragDotLabel: UILabel!
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

//MARK:- Private Methods
//=======================
private extension RequireExperianceVC {
    
    func initialSetup() {
        
        //  self.navigationView.isHidden = true
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: StringConstants.RequiredExperiencedNumber )
        attributedString.setColorForText(textForAttribute: "7", withColor: AppColors.themeBlueColor, font: AppFonts.Poppins_Medium.withSize(15))
        
        let attributedDescriptionString: NSMutableAttributedString = NSMutableAttributedString(string: StringConstants.RequiredExperianceText )
        attributedDescriptionString.setColorForText(textForAttribute: "(Optional)", withColor: AppColors.gray152, font: AppFonts.Poppins_Light.withSize(15))
        self.descriptionLabel.attributedText = attributedDescriptionString
        
        self.yesButton.setCorner(cornerRadius: 5, clip: true)
        self.noButton.setCorner(cornerRadius: 5, clip: true)
        self.sliderValueLabel.text = "Min. 0 month"
        self.setupButtonColor()
        showHideSlider(status: true)
        
        self.setupCircularSlider()
        self.populateData()
    }
    
    func setupCircularSlider() {
        sliderContainerView.delegate = self
    }
    func populateData() {
        
        if let _ = SharedJobPost.isExp {
            if let experiance = SharedJobPost.rawExpValue {
                self.sliderContainerView.setValue(experiance, animated: false)
            }
            self.yesButton.isSelected = true
            self.noButton.isSelected = false
            self.showHideSlider(status: false)
        } else {
            
            self.yesButton.isSelected = false
            self.noButton.isSelected = true
            SharedJobPost.isExp = nil
            self.showHideSlider(status: true)
        }
        
        
    }
    
    func setupButtonColor() {
        
        self.yesButton.setBackgroundColor(AppColors.white, for: .normal)
        self.yesButton.setBackgroundColor(AppColors.themeBlueColor, for: .selected)
        self.noButton.setBackgroundColor(AppColors.white, for: .normal)
        self.noButton.setBackgroundColor(AppColors.themeBlueColor, for: .selected)
        self.yesButton.setTitleColor(AppColors.white, for: .selected)
        self.yesButton.setTitleColor(AppColors.themeBlueColor, for: .normal)
        self.noButton.setTitleColor(AppColors.white, for: .selected)
        self.noButton.setTitleColor(AppColors.themeBlueColor, for: .normal)
        self.yesButton.borderColor = AppColors.themeBlueColor
        self.yesButton.borderWidth = 1.5
        self.noButton.borderColor = AppColors.themeBlueColor
        self.noButton.borderWidth = 1.5
    }
    
    func saveViewState() {
        
        if self.yesButton.isSelected {
            self.requiredExperiance = .yes
        } else {
            self.requiredExperiance = .no
        }
    }
    
    func showHideSlider(status: Bool) {
        
        UIView.animate(withDuration: 0.3) {
            self.chooseExperianceLabel.isHidden = status
            self.dragDotLabel.isHidden = status
            self.sliderContainerView.isHidden = status
        }
    }
}

// MARK: - CircularSliderDelegate
extension RequireExperianceVC: CircularSliderDelegate {
    func circularSlider(_ circularSlider: CircularSlider, valueForValue value: Float) -> Float {
        
        if value < 180 {
            if self.noButton.isSelected {
                
                SharedJobPost.isExp = nil
                SharedJobPost.rawExpValue = nil
                SharedJobPost.totalExperience = ""
            } else {
                self.delegate?.isEnableNextButton(status: true)

                SharedJobPost.experienceType = 1 // 1 for month
                SharedJobPost.totalExperience = "\(Int(CommonClass.getExperience(pos: value)))"
                self.sliderValueLabel.text =  "\(StringConstants.upTo) \(Int(CommonClass.getExperience(pos: value))) month"
            }
        } else {
            self.delegate?.isEnableNextButton(status: true)

            SharedJobPost.experienceType = 2 // 2 for year
            let val = "\(CommonClass.getExperience(pos: value))".replacingOccurrences(of: ".0", with: "")
            
            if CommonClass.getExperience(pos: value) > 5 {
                self.sliderValueLabel.text = "\(StringConstants.upTo) 5+ year"
                SharedJobPost.totalExperience = "5+"
            } else {
                self.sliderValueLabel.text =  "\(StringConstants.upTo) \(val) year"
                SharedJobPost.totalExperience = val
            }
            
        }
        SharedJobPost.rawExpValue = value
        print_debug(value)
        return floorf(value)
    }
}

//MARK: - IB Action and Target
//===============================
extension RequireExperianceVC {
    
    @IBAction func yesButtonTapped(_ sender: UIButton) {
        self.delegate?.isEnableNextButton(status: false)
        self.noButton.isSelected = false
        self.showHideSlider(status: false)
        sender.isSelected = true
        SharedJobPost.isExp = 1
    }
    
    @IBAction func noButtonTapped(_ sender: UIButton) {
        self.delegate?.isEnableNextButton(status: true)

        self.yesButton.isSelected = false
        self.showHideSlider(status: true)
        self.sliderContainerView.setValue(0, animated: false)
        SharedJobPost.isExp = nil
        SharedJobPost.rawExpValue = nil
        SharedJobPost.totalExperience = ""
        SharedJobPost.isExp = 0
        sender.isSelected = true
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        
    }
}




