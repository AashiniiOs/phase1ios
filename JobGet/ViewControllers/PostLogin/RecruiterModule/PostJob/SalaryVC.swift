//
//  SalaryVC.swift
//  JobGet
//
//  Created by macOS on 05/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class SalaryVC: BaseVC {
    
    
    //MARK: - Enum
    //=================
    enum JobType: Int {
        case full = 1
        case partTime = 2
        case both = 3
        case none = 4
    }
    
    //MARK:- Properties
    //==================
    let hourData = ["",StringConstants.Per_Hour, StringConstants.Per_Week, StringConstants.Per_Month, StringConstants.Per_Year]
    var jobType: JobType = .none
    weak var delegate: EnableNextButtonProtocol?
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var salaryLabel: UILabel!
    @IBOutlet weak var fromTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var toTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var perHourTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var fullTimeButton: UIButton!
    @IBOutlet weak var partTimeButton: UIButton!
    @IBOutlet weak var bothButton: UIButton!
    @IBOutlet weak var tipsLabel: UILabel!
    @IBOutlet weak var tipsYesButton: UIButton!
    @IBOutlet weak var tipsNoButton: UIButton!
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
}

//MARK:- Private Methods
//=======================
private extension SalaryVC {
    
    func initialSetup() {
        
        CommonClass.setSkyFloatingLabelTextField(textField: self.fromTextField, placeholder: "From", title: "From")
        CommonClass.setSkyFloatingLabelTextField(textField: self.toTextField, placeholder: "To", title: "To")
        CommonClass.setSkyFloatingLabelTextField(textField: self.perHourTextField, placeholder: "Duration", title: "Duration")
        self.perHourTextField.delegate = self
        let attributedString: NSMutableAttributedString = NSMutableAttributedString(string: StringConstants.SalaryNumber )
        attributedString.setColorForText(textForAttribute: "6", withColor: AppColors.themeBlueColor, font: AppFonts.Poppins_Medium.withSize(15) )
        let attributedSalaryString: NSMutableAttributedString = NSMutableAttributedString(string: StringConstants.SalaryOptional )
        attributedSalaryString.setColorForText(textForAttribute: "(Optional)", withColor: AppColors.gray152, font: AppFonts.Poppins_Light.withSize(15))
        self.salaryLabel.attributedText = attributedSalaryString
        self.fromTextField.delegate = self
        self.toTextField.delegate   = self
        self.setupTextFieldKeyBoadType()
        self.setupButtonColor()
        self.populateData()
    }
    
    func setupButtonColor() {
        self.fullTimeButton.setBackgroundColor(AppColors.white, for: .normal)
        self.fullTimeButton.setBackgroundColor(AppColors.themeBlueColor, for: .selected)
        
        self.partTimeButton.setBackgroundColor(AppColors.white, for: .normal)
        self.partTimeButton.setBackgroundColor(AppColors.themeBlueColor, for: .selected)
        
        self.bothButton.setBackgroundColor(AppColors.white, for: .normal)
        self.bothButton.setBackgroundColor(AppColors.themeBlueColor, for: .selected)
        
        self.fullTimeButton.setTitleColor(AppColors.white, for: .selected)
        self.fullTimeButton.setTitleColor(AppColors.themeBlueColor, for: .normal)
        self.partTimeButton.setTitleColor(AppColors.white, for: .selected)
        self.partTimeButton.setTitleColor(AppColors.themeBlueColor, for: .normal)
        self.bothButton.setTitleColor(AppColors.white, for: .selected)
        self.bothButton.setTitleColor(AppColors.themeBlueColor, for: .normal)
        
        self.tipsYesButton.setTitleColor(AppColors.white, for: .selected)
        self.tipsYesButton.setTitleColor(AppColors.themeBlueColor, for: .normal)
        self.tipsNoButton.setTitleColor(AppColors.white, for: .selected)
        self.tipsNoButton.setTitleColor(AppColors.themeBlueColor, for: .normal)
        
        self.tipsYesButton.setBackgroundColor(AppColors.white, for: .normal)
        self.tipsYesButton.setBackgroundColor(AppColors.themeBlueColor, for: .selected)
        
        self.tipsNoButton.setBackgroundColor(AppColors.white, for: .normal)
        self.tipsNoButton.setBackgroundColor(AppColors.themeBlueColor, for: .selected)
        
        self.fullTimeButton.setCorner(cornerRadius: 5, clip: true)
        self.partTimeButton.setCorner(cornerRadius: 5, clip: true)
        self.bothButton.setCorner(cornerRadius: 5, clip: true)
        self.tipsYesButton.setCorner(cornerRadius: 5, clip: true)
        self.tipsNoButton.setCorner(cornerRadius: 5, clip: true)
        
        self.bothButton.borderColor = AppColors.themeBlueColor
        self.bothButton.borderWidth = 1.5
        self.fullTimeButton.borderColor = AppColors.themeBlueColor
        self.fullTimeButton.borderWidth = 1.5
        self.partTimeButton.borderColor = AppColors.themeBlueColor
        self.partTimeButton.borderWidth = 1.5
        
        self.tipsYesButton.borderColor = AppColors.themeBlueColor
        self.tipsYesButton.borderWidth = 1.5
        self.tipsNoButton.borderColor = AppColors.themeBlueColor
        self.tipsNoButton.borderWidth = 1.5
    }
    
    func setupTextFieldKeyBoadType() {
        
        self.fromTextField.keyboardType = .decimalPad
        self.toTextField.keyboardType   = .decimalPad
        
    }
    
    func setData() {
        
        if self.fullTimeButton.isSelected {
            self.jobType = .full
        } else if self.partTimeButton.isSelected {
            self.jobType = .partTime
        } else if self.bothButton.isSelected {
            self.jobType = .both
        }
        
        SharedJobPost.salaryFrom = self.fromTextField.text ?? ""
        SharedJobPost.salaryTo = self.toTextField.text ?? ""
        SharedJobPost.duration = self.perHourTextField.text ?? ""
        
        
        if self.fullTimeButton.isSelected {
            self.jobType = .full
        } else if self.partTimeButton.isSelected {
            self.jobType = .partTime
        } else if self.bothButton.isSelected {
            self.jobType = .both
        }
        SharedJobPost.jobType = self.jobType.rawValue
    }
    
    func populateData() {
        
        if !SharedJobPost.salaryFrom.isEmpty {
            self.fromTextField.text = SharedJobPost.salaryFrom
        }
        
        if !SharedJobPost.salaryTo.isEmpty {
            self.toTextField.text = SharedJobPost.salaryTo
        }
        
        if !SharedJobPost.duration.isEmpty {
            self.perHourTextField.text = SharedJobPost.duration
        }
        
        if SharedJobPost.isCommisionRequired == 1 {
            self.tipsNoButton.isSelected = false
            self.tipsYesButton.isSelected = true
        } else {
            self.tipsNoButton.isSelected = true
            self.tipsYesButton.isSelected = false
        }
        
        let jobTypes = SharedJobPost.jobType
        
        switch jobTypes {
        case 1:
            self.fullTimeButton.isSelected = true
        case 2:
            self.partTimeButton.isSelected = true
        case 3:
            self.bothButton.isSelected = true
        default:
            return
        }
        
        
    }
    
    func isToSalaryGreater() -> Bool {
        
        guard let fromSalary = self.fromTextField.text else { return false }
        guard let toSalary = self.toTextField.text else { return false }
        
        if !fromSalary.isEmpty && !toSalary.isEmpty {
            guard let fromSalaryInInt = Double(fromSalary) else { return false}
            guard let toSalaryInInt   = Double(toSalary) else { return false}
            
            if toSalaryInInt > fromSalaryInInt {
                return true
            } else {
                return false
            }
        } else {
            return true
        }
    }
}


//MARK: - IB Action and Target
//===============================
extension SalaryVC {
    
    @IBAction func fullTimeButTapped(_ sender: UIButton) {
        
        self.partTimeButton.isSelected = false
        self.bothButton.isSelected = false
        sender.isSelected = true
        self.jobType = .full
        SharedJobPost.jobType = self.jobType.rawValue
    }
    
    @IBAction func partTimeBtnTapped(_ sender: UIButton) {
        
        self.fullTimeButton.isSelected = false
        self.bothButton.isSelected = false
        sender.isSelected = true
        self.jobType = .partTime
        SharedJobPost.jobType = self.jobType.rawValue
        
    }
    
    @IBAction func bothBtnTapped(_ sender: UIButton) {
        
        self.partTimeButton.isSelected = false
        self.fullTimeButton.isSelected = false
        sender.isSelected = true
        self.jobType = .both
        SharedJobPost.jobType = self.jobType.rawValue
    }
    
    @IBAction func fromEditingChangedTextField(_ sender: UITextField) {
        
    }
    
    @IBAction func toEditingChangedTextField(_ sender: UITextField) {
        
    }
    
    @IBAction func perHourEditingBeginTextFeild(_ sender: UITextField) {
        
        MultiPicker.noOfComponent = 1
        MultiPicker.openMultiPickerIn(sender, firstComponentArray: self.hourData, secondComponentArray: [""], firstComponent: "", secondComponent: nil, titles: nil) { (data, response, index) in
            self.perHourTextField.text = data
            SharedJobPost.duration = data
            print(data)
            print_debug(response)
            print_debug(index)
        }
    }
    
    @IBAction func tipsYesBtnTapped(_ sender: UIButton) {
        
        SharedJobPost.isCommisionRequired = 1
        self.tipsNoButton.isSelected = false
        sender.isSelected = true
    }
    
    @IBAction func tipsNoBtnTapped(_ sender: UIButton) {
        SharedJobPost.isCommisionRequired = 0
        self.tipsYesButton.isSelected = false
        sender.isSelected = true
        
    }
}

//MARK: - Text Field Delegate
//===============================
extension SalaryVC: UITextFieldDelegate {
    
    // disable next button when toTextField have value and fromTextField is Empty other wise enable next button
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField === self.perHourTextField{
            self.perHourTextField.tintColor = .clear
            
        }else{
            
        }
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        if textField == self.fromTextField || textField == self.toTextField {
            
            let formatter = NumberFormatter()
            formatter.numberStyle = .decimal
            formatter.locale = Locale.current
            formatter.maximumFractionDigits = 2
            
            // Uses the grouping separator corresponding to your Locale
            // e.g. "," in the US, a space in France, and so on
            if let groupingSeparator = formatter.groupingSeparator {
                
                if string == groupingSeparator {
                    return true
                }
                
                if let textWithoutGroupingSeparator = textField.text?.replacingOccurrences(of: groupingSeparator, with: "") {
                    var totalTextWithoutGroupingSeparators = textWithoutGroupingSeparator + string
                    if string == "" { // pressed Backspace key
                        totalTextWithoutGroupingSeparators.removeLast()
                        return true
                    }
                    if let numberWithoutGroupingSeparator = formatter.number(from: totalTextWithoutGroupingSeparators),
                        let formattedText = formatter.string(from: numberWithoutGroupingSeparator), (textField.text ?? "").count - range.length < 8 , string != "." {
                        
                        textField.text = formattedText
                        return false
                    }  else {
                        
                        if (textField.text ?? "").count - range.length < 8 {
                            return true
                        } else {
                            return false
                        }
                    }
                }
            }
            return true
        } else {
            return true
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        self.setData()
        
        if self.fullTimeButton.isSelected || self.partTimeButton.isSelected || self.bothButton.isSelected {
            
            if textField === self.toTextField {
                
                if (textField.text ?? "").count > 0 && (self.fromTextField.text ?? "").count > 0  {
                    self.delegate?.isEnableNextButton(status: true)
                    
                }    else if (self.fromTextField.text ?? "").isEmpty && !(textField.text ?? "").isEmpty  {
                    self.delegate?.isEnableNextButton(status: false)
                } else {
                    self.delegate?.isEnableNextButton(status: true)
                }
            } else if textField === self.fromTextField {
                if (textField.text ?? "").isEmpty && !(self.toTextField.text ?? "").isEmpty {
                    self.delegate?.isEnableNextButton(status: false)
                } else {
                    self.delegate?.isEnableNextButton(status: true)
                }
            }
        } else {
            self.delegate?.isEnableNextButton(status: true)
        }
    }
}

