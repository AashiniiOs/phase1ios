//
//  AddExperienceStepVC.swift
//  JobGet
//
//  Created by Abhi on 22/05/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//
import SwiftyJSON
import UIKit
import CircularSlider


protocol  AddExperienceDelegate: class {
    
    func addExperience(experienceData: CandidateProfileModel)
}

protocol ChangeExpProtocol: class {
    
    func chageExp(experiance: ExperienceModel, arayIndex: Int)
}

class AddExperienceStepVC: UIViewController {
    
    
    @IBOutlet weak var experienceTabelView: UITableView!
    
    
    var jobCategoryData = [JobCategory]()
    var categoryName = [String]()
    var categoryId   = [String]()
    var cardsCount = 1
    var candidateDetail : CandidateProfileModel = CandidateProfileModel(withJSON: JSON([:]))
    var row: Int?
    var job_id = ""
    // var fromTabbar = true
    var fromDidSelectRowAt = false
    var categoryIndexPath: IndexPath?
    var selectedIndex: IndexPath?
    weak var delegate: AddExperienceDelegate?
    weak var changeExpDelegate: ChangeExpProtocol?
    var selectedID = ""
    var selectCategoryName = ""
    
    
    var isOpenEditProfile = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // dont apped a empty exp Model when it comes from didSelectRow At previous screen
        if !fromDidSelectRowAt {
            candidateDetail.experience.append(ExperienceModel(withJSON: JSON([:])))
        }
        
        self.experienceTabelView.register(UINib(nibName: "SubmitButtonCell", bundle: nil), forCellReuseIdentifier: "SubmitButtonCell")
        self.experienceTabelView.register(UINib(nibName: "AddExperienceCell", bundle: nil), forCellReuseIdentifier: "AddExperienceCell")
        self.experienceTabelView.bounces = false
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.keybordHide))
        tap.numberOfTapsRequired = 1
        self.experienceTabelView.addGestureRecognizer(tap)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    @IBAction func backButtonTapp(_ sender: UIButton) {
        
        if self.isOpenEditProfile{
            self.dismiss(animated: true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
}

//MARK: PRIVATE FUNCTIONS
extension AddExperienceStepVC{
    
    private func validations() {
        
        if cardsCount != 0{
            
            for value in candidateDetail.experience{
                
                if value.categoryName.isEmpty{
                    CommonClass.showToast(msg: StringConstants.Please_select_category)
                    return
                }
                if value.companyName.isEmpty{
                    
                    CommonClass.showToast(msg: StringConstants.Please_add_company_name)
                    return
                }
                
                if value.duration.isEmpty {
                    
                    CommonClass.showToast(msg: StringConstants.Please_enter_job_duration)
                    return
                }else if value.duration == "0"{
                    CommonClass.showToast(msg: StringConstants.Please_enter_job_duration)
                    return
                }
            }
        }
        if fromDidSelectRowAt {
            guard let row = self.row else { return }
            print_debug(self.candidateDetail.experience[row])
            self.changeExpDelegate?.chageExp(experiance: self.candidateDetail.experience[row], arayIndex: row)
        } else {
            self.delegate?.addExperience(experienceData: self.candidateDetail)
        }
        if self.isOpenEditProfile{
            self.dismiss(animated: true, completion: nil)
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc private func keybordHide(){
        self.view.endEditing(true)
        
    }
}


//MARK: Table View Delegate and datasource
extension AddExperienceStepVC: UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return cardsCount + 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        switch indexPath.row {
            
        case cardsCount:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SubmitButtonCell", for: indexPath) as? SubmitButtonCell else{
                fatalError("SubmitButtonCell not found")
            }
            
            cell.submitButton.backgroundColor = #colorLiteral(red: 0.2389856699, green: 0.4166136601, blue: 0.6607115269, alpha: 1)
            cell.submitButton.setTitle(StringConstants.next, for: .normal)
            cell.submitButton.addTarget(self, action: #selector(submitButtonTapped(_:)), for: .touchUpInside)
            return cell
            
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "AddExperienceCell", for: indexPath) as? AddExperienceCell else{
                fatalError("AddExperienceCell not found")
            }
            
            cell.categoryTextfield.addTarget(self, action: #selector(categoryTextfieldTapped), for: .editingDidBegin)
            cell.companyTextfield.addTarget(self, action: #selector(companyTextfieldTapped), for: .editingDidEnd)
            cell.positionTextfield.addTarget(self, action: #selector(positionTextfieldTapped), for: .editingDidEnd)
            cell.minusButton.isHidden = true
            cell.minusButton.addTarget(self, action: #selector(minusButtonTapped), for: .touchUpInside)
            cell.fullTimeButton.addTarget(self, action: #selector(fullTimeButtonTapped), for: .touchUpInside)
            cell.partTimeButton.addTarget(self, action: #selector(partTimeButtonTapped), for: .touchUpInside)
            cell.sliderView.delegate = self
            
            print_debug(row)
            if let row = row {
                cell.populateData(experianceData: self.candidateDetail.experience[row])
                self.selectedID = self.candidateDetail.experience[row].categoryId
            }
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == cardsCount{
            return 110
            
        }else{
            return 480
        }
    }
    
    @objc func submitButtonTapped(_ sender: UIButton){
        
        validations()
        
    }
    
    @objc func categoryTextfieldTapped(_ textField: UITextField){
        
        if let indexPath = textField.tableViewIndexPath(self.experienceTabelView) {
            self.categoryIndexPath = indexPath
        }
        
        let filterVC = FilterVC.instantiate(fromAppStoryboard: .Candidate)
        filterVC.isfromAddExperiance = true
        filterVC.singleCategoryDelegate = self
        
        if !self.selectedID.isEmpty{
            filterVC.selectedID = self.selectedID
            filterVC.selectedCategoryID = self.selectedID
            filterVC.selectedCategoryName = self.selectCategoryName
        }
        
        filterVC.selectedIndex = self.selectedIndex
        self.present(filterVC, animated: true, completion: nil)
    }
    
    @objc func companyTextfieldTapped(_ textField: UITextField) {
        
        if fromDidSelectRowAt {
            guard let row = self.row else { return }
            self.candidateDetail.experience[row].companyName = textField.text ?? ""
        } else {
            self.candidateDetail.experience[cardsCount - 1].companyName = textField.text ?? ""
        }
    }
    
    @objc func positionTextfieldTapped(_ textField: UITextField){
        
        if fromDidSelectRowAt {
            guard let row = self.row else { return }
            
            self.candidateDetail.experience[row].position = textField.text ?? ""
        } else {
            self.candidateDetail.experience[cardsCount - 1].position = textField.text ?? ""
        }
        
    }
    
    @objc func minusButtonTapped(_ sender : UIButton){
        
        //        guard let indexPath = sender.tableViewIndexPath(self.experienceTabelView) as IndexPath? else{return}
        //
        //        self.cardsCount -= 1
        //        self.experienceTabelView.deleteRows(at: [indexPath], with: .automatic)
        
    }
    
    @objc func fullTimeButtonTapped(_ sender : UIButton){
        sender.isSelected = !sender.isSelected
        
        guard let cell = sender.tableViewCell as? AddExperienceCell else{return}
        cell.partTimeButton.isSelected = false
        self.candidateDetail.experience[cardsCount - 1].expType = 1
        
    }
    
    @objc func partTimeButtonTapped(_ sender : UIButton){
        sender.isSelected = !sender.isSelected
        guard let cell = sender.tableViewCell as? AddExperienceCell else{return}
        cell.fullTimeButton.isSelected = false
        self.candidateDetail.experience[cardsCount - 1].expType = 2
        
    }
    
    
    @objc func checkBoxButtonTapped(_ sender : UIButton){
        sender.isSelected = !sender.isSelected
        if sender.isSelected{
            self.candidateDetail.experience[cardsCount - 1].isCurrentCompany = 1
        }else{
            self.candidateDetail.experience[cardsCount - 1].isCurrentCompany = 0
        }
    }
}

// MARK: - CircularSliderDelegate
//=================================
extension AddExperienceStepVC: CircularSliderDelegate {
    func circularSlider(_ circularSlider: CircularSlider, valueForValue value: Float) -> Float {
        guard let cell = circularSlider.tableViewCell as? AddExperienceCell else{return 0.0}
        
        if self.fromDidSelectRowAt {
            guard let row = self.row else { fatalError("row not found in AddExperianceSetpVC") }
            if value < 180 {
                
                
                let lowerValue = "\(Int(CommonClass.getExperience(pos: value)))"
                candidateDetail.experience[row].durationType = 1
                if lowerValue == "1" ||  lowerValue == "0" {
                    cell.durationValue.text = "\(lowerValue) \(StringConstants.Month)"
                } else  {
                    cell.durationValue.text = "\(lowerValue) \(StringConstants.Months)"
                }
                
                candidateDetail.experience[row].duration = "\(lowerValue)"
                candidateDetail.experience[row].rawSliderExp = value
                
            } else {
                
                
                let lowerValue = CommonClass.getExperience(pos: value)
                
                candidateDetail.experience[row].durationType = 2
                
                if lowerValue > 5 {
                    cell.durationValue.text = "5+ \(StringConstants.Years)"
                    candidateDetail.experience[row].duration = "5+"
                } else {
                    let value = "\(lowerValue)".replacingOccurrences(of: ".0", with: "")
                    if lowerValue == 1 {
                        cell.durationValue.text = "\(value) \(StringConstants.Year)"
                    } else {
                        cell.durationValue.text = "\(value) \(StringConstants.Years)"
                    }
                    
                    
                    candidateDetail.experience[row].duration = value
                }
                candidateDetail.experience[row].rawSliderExp = value
                
            }
            
            print(value)
            return floorf(value)
        } else {
            
            if value < 180 {
                
                
                let lowerValue = "\(Int(CommonClass.getExperience(pos: value)))"
                candidateDetail.experience[cardsCount - 1].durationType = 1
                
                if lowerValue == "1" ||  lowerValue == "0" {
                    cell.durationValue.text = "\(lowerValue) \(StringConstants.Month)"
                } else  {
                    cell.durationValue.text = "\(lowerValue) \(StringConstants.Months)"
                }
                candidateDetail.experience[cardsCount - 1].duration = "\(lowerValue)"
                candidateDetail.experience[cardsCount - 1].rawSliderExp = value
                
            } else {
                
                
                let lowerValue = CommonClass.getExperience(pos: value)
                
                candidateDetail.experience[cardsCount - 1].durationType = 2
                
                if lowerValue > 5 {
                    cell.durationValue.text = "5+ \(StringConstants.Years)"
                    candidateDetail.experience[cardsCount - 1].duration = "5+"
                } else {
                    let value = "\(lowerValue)".replacingOccurrences(of: ".0", with: "")
                    
                    if lowerValue == 1 {
                        cell.durationValue.text = "\(value) \(StringConstants.Year)"
                    } else {
                        cell.durationValue.text = "\(value) \(StringConstants.Years)"
                    }
                    
                    candidateDetail.experience[cardsCount - 1].duration = "\(lowerValue)".replacingOccurrences(of: ".0", with: "")
                }
                candidateDetail.experience[cardsCount - 1].rawSliderExp = value
            }
            
            print(value)
            return floorf(value)
        }
    }
}
//MARK: WEBSERVICES
extension AddExperienceStepVC{
    
    
    
    func postProfile(){
        WebServices.addProfile(parameters: self.candidateDetail.dictionary(), loader: true, success: { (dict) in
            self.navigationController?.popViewController(animated: true)
        }) { (error) in
            self.showAlert(msg: error.localizedDescription)
        }
    }
}
//MARK: Get Category From Pofile Creation Tap
//================================================
extension AddExperienceStepVC: GetSingleCategory{
    
    func getCategoryForProfile(selectedCategoryID: String, selectedCategoryName: String, selectedIndex: IndexPath) {
        
        if let index = self.categoryIndexPath {
            guard let cell = self.experienceTabelView.cellForRow(at: index) as? AddExperienceCell else { return }
            self.selectedIndex = selectedIndex
            self.selectedID = selectedCategoryID
            self.selectCategoryName = selectedCategoryName
            if self.fromDidSelectRowAt {
                guard let row = self.row else { return }
                cell.categoryTextfield.text = selectedCategoryName
                self.candidateDetail.experience[row].categoryName = selectedCategoryName
                self.candidateDetail.experience[row].categoryTitle = selectedCategoryName
                self.candidateDetail.experience[row].categoryId = selectedCategoryID
                
            } else {
                
                cell.categoryTextfield.text = selectedCategoryName
                self.selectedIndex = selectedIndex
                self.candidateDetail.experience[index.row].categoryName = selectedCategoryName
                self.candidateDetail.experience[index.row].categoryTitle = selectedCategoryName
                self.candidateDetail.experience[index.row].categoryId = selectedCategoryID
                
            }
        }
    }
}

