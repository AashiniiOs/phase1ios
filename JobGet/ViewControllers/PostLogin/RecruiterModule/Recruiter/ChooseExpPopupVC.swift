//
//  ChooseExpPopupVC.swift
//  JobGet
//
//  Created by macOS on 05/06/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import CircularSlider

protocol ChooseExpProtocol: class {
    
    func didTapOkBtn(monthExp: Int?, yearExp:Float?, isExp: Int)
}

class ChooseExpPopupVC: BaseVC {
    
    //MARK:- Properties
    //==================
    var sliderCurrentValue = 0
    var sliderUpperCurrentVal = 182
    var monthExp     :Int?
    var yearExp      :Float?
    weak var delegate: ChooseExpProtocol?
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var noButton: UIButton!
    @IBOutlet weak var yesButton: UIButton!
    @IBOutlet weak var requiredExpBtn: UILabel!
    @IBOutlet weak var sliderValueLabel: UILabel!
    @IBOutlet weak var dismissBtn: UIButton!
    @IBOutlet weak var chooseExpLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var okBtn: UIButton!
    
    @IBOutlet weak var circularSlider: CircularSlider!
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        self.circularSlider.delegate = self
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    //MARK:- remove popup when touch otuside of popup
    //================================================
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        //        self.view.endEditing(true)
        //        let touch = touches.first!
        //        let point = touch.location(in: self.view) //touch.location(in: self.view)
        //
        //        if !self.containerView.frame.contains(point) {
        //            self.dismissPopupViewOnTouches()
        //        }
    }
    
    func dismissPopupViewOnTouches() {
        
        UIView.animate(withDuration: 0.33, delay: 0.0, usingSpringWithDamping: 0.9 , initialSpringVelocity: 0.4, options: UIViewAnimationOptions(), animations: {
            
            self.containerView.transform = CGAffineTransform( scaleX: 0.0000001,y: 0.0000001)
        }){ (true) in
        }
        
        self.view.removeFromSuperview()
        self.removeFromParent
    }
}

extension ChooseExpPopupVC {
    
    
    func initialSetup() {
        
        self.chooseExpLabel.text = StringConstants.ChooseExperience
        self.requiredExpBtn.text = StringConstants.ExperienceRequired
        self.setupButtonColor()
        self.setupCircularSlider()
    }
    
    func setupCircularSlider() {
        
        
        
        if let monthExp = self.monthExp {
            
            let angle = Float(CommonClass.getExperienceAngle(experience: Float(monthExp), experienceType: 1, fromTo: 0))
            circularSlider.setValue(angle, animated: false)
            self.yesButton.isSelected = true
            self.noButton.isSelected = false
            self.showHideSlider(status: false)
            self.sliderValueLabel.text = "\(monthExp) \(StringConstants.Month.lowercased())"
        } else if let yearExp = self.yearExp {
            let angle = Float(CommonClass.getExperienceAngle(experience: Float(yearExp), experienceType: 2, fromTo: 0))
            circularSlider.setValue(angle, animated: false)
            self.yesButton.isSelected = true
            self.noButton.isSelected = false
            self.showHideSlider(status: false)
            
            self.sliderValueLabel.text = "\(yearExp) \(StringConstants.Month.lowercased())"
        } else {
            circularSlider.setValue(0.0, animated: false)
            self.sliderValueLabel.text = "0 \(StringConstants.Month.lowercased())"
            self.yesButton.isSelected = false
            self.noButton.isSelected = true
            self.showHideSlider(status: true)
        }
    }
    
    func setupButtonColor() {
        
        self.yesButton.setCorner(cornerRadius: 5.0, clip: true)
        self.noButton.setCorner(cornerRadius: 5.0, clip: true)
        
        self.yesButton.setBackgroundColor(AppColors.white, for: .normal)
        self.yesButton.setBackgroundColor(AppColors.themeBlueColor, for: .selected)
        self.noButton.setBackgroundColor(AppColors.white, for: .normal)
        self.noButton.setBackgroundColor(AppColors.themeBlueColor, for: .selected)
        self.yesButton.setTitleColor(AppColors.white, for: .selected)
        self.yesButton.setTitleColor(AppColors.themeBlueColor, for: .normal)
        self.noButton.setTitleColor(AppColors.white, for: .selected)
        self.noButton.setTitleColor(AppColors.themeBlueColor, for: .normal)
        self.yesButton.borderColor = AppColors.themeBlueColor
        self.yesButton.borderWidth = 1.5
        self.noButton.borderColor = AppColors.themeBlueColor
        self.noButton.borderWidth = 1.5
        self.okBtn.backgroundColor = AppColors.themeBlueColor
        self.okBtn.setCorner(cornerRadius: 5.0, clip: true)
        self.containerView.setCorner(cornerRadius: 5.0, clip: true)
    }
    
    func showHideSlider(status: Bool) {
        
        self.circularSlider.isHidden = status
    }
    
}


//MARK: - IB Action and Target
//===============================
extension ChooseExpPopupVC {
    
    @IBAction func yesButtonTapped(_ sender: UIButton) {
        self.noButton.isSelected = false
        self.showHideSlider(status: false)
        self.yesButton.isSelected = true
    }
    
    @IBAction func noButtonTapped(_ sender: UIButton) {
        self.yesButton.isSelected = false
        self.showHideSlider(status: true)
        self.noButton.isSelected = true
    }
    
    @IBAction func dismissButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func okButtonTapped(_ sender: UIButton) {
        
        if self.yesButton.isSelected {
            if self.monthExp == 0 {
                CommonClass.showToast(msg: StringConstants.SelectSuitableExp)
            } else {
                self.delegate?.didTapOkBtn(monthExp: self.monthExp,yearExp: self.yearExp, isExp: 1)
                self.dismiss(animated: true, completion: nil)
            }
        } else {
            self.delegate?.didTapOkBtn(monthExp: self.monthExp,yearExp: self.yearExp, isExp: 0)
            self.dismiss(animated: true, completion: nil)
        }
        
    }
}

// MARK: - CircularSliderDelegate
extension ChooseExpPopupVC: CircularSliderDelegate {
    
    func circularSlider(_ circularSlider: CircularSlider, valueForValue value: Float) -> Float {
        
        if value < 180 {
            
            
            self.monthExp = Int(CommonClass.getExperience(pos: value))
            if let montExp = self.monthExp {
                
                if  montExp == 0 {
                    self.sliderValueLabel.text = "\(montExp) \(StringConstants.Month)"
                } else {
                    self.sliderValueLabel.text = "\(StringConstants.Min) \(montExp) \(StringConstants.Months)"
                }
                
            }
            
            self.yearExp = nil
        } else {
            
            let val = "\(CommonClass.getExperience(pos: value))".replacingOccurrences(of: ".0", with: "")
            
            if CommonClass.getExperience(pos: value) > 5 {
                self.sliderValueLabel.text = "\(StringConstants.Min) 5+ \(StringConstants.Years)"
                self.yearExp = CommonClass.getExperience(pos: value)
                
            } else if CommonClass.getExperience(pos: value) == 1 {
                self.sliderValueLabel.text =  "\(StringConstants.Min) \(val) \(StringConstants.Year)"
                self.yearExp = CommonClass.getExperience(pos: value)
            } else {
                self.sliderValueLabel.text =  "\(StringConstants.Min) \(val) \(StringConstants.Years)"
                self.yearExp = CommonClass.getExperience(pos: value)
            }
            self.monthExp = nil
        }
        
        return floorf(value)
    }
}

