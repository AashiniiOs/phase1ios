//
//  EducationStepVC.swift
//  JobGet
//
//  Created by Abhi on 22/05/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import SwiftyJSON
import UIKit

enum StepMode{
    case education,summary, addImage
}

protocol EducationSaveText: class {
    
    func didSaveText(text: String, image: UIImage?, imageURL: String?, mode: StepMode, aboutTextViewHeight: CGFloat, educationTextViewHeight: CGFloat)
}

class EducationStepVC: UIViewController {
    
    @IBOutlet weak var aboutTextView   : TLFloatLabelTextView!
    @IBOutlet weak var bottomSeperator: UIView!
    @IBOutlet weak var aboutTextViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var eductaionTextfield: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var mainLabel: UILabel!
    @IBOutlet weak var subLabel: UILabel!
    
    var selectedMode : StepMode = .education
    var fromProfile = false
    var candidateDetail : CandidateProfileModel = CandidateProfileModel(withJSON: JSON([:]))
    var job_id = ""
    var recruiterId = ""
    var aboutText = ""
    var education = ""
    var categoryId = ""
    weak var delegate: AddProfileChildVCDelegate?
    weak var educationDelegate: EducationSaveText?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        CommonClass.setTLFoatTextView(textView: self.aboutTextView, title: "\(StringConstants.Education) \(StringConstants.Optional)")
        CommonClass.setTLFoatTextView(textView: self.aboutTextView, title: "\(StringConstants.About_Me) \(StringConstants.Optional)")
        saveButton.layer.cornerRadius = 4
        self.aboutTextView.bottomLineColour = .clear
        self.aboutTextView.bottomLineActiveColour = .clear

        self.saveButton.setTitle(StringConstants.next, for: .normal)
        self.aboutTextView.delegate = self
        self.aboutTextView.textContainer.lineBreakMode = .byWordWrapping
        
        aboutTextView.isScrollEnabled = false
        setUpViewAccordingToMode()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.populateData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func populateData() {
        
        if self.selectedMode == .education{
            self.aboutTextView.text = self.education
            self.candidateDetail.about = self.aboutText
            self.candidateDetail.education = self.education
        } else {
            self.aboutTextView.text = self.aboutText
            self.candidateDetail.about = self.aboutText
        }
    }
    
    func setUpViewAccordingToMode(){
        
        if self.selectedMode == .education{
            self.eductaionTextfield.isHidden = true
            self.eductaionTextfield.text =  self.candidateDetail.education
            self.mainLabel.text = "Tell us about your education or training!"
            self.subLabel.text = "Share your highest form of education or any formal training you might have."

            self.aboutTextView.hint = ""
            self.aboutTextView.hint = "Here is a helpful example\n\"Linguistics Major at Umass Class of 2020\""
            self.aboutTextView.isHidden = false
            print_debug(self.candidateDetail.educationTextViewHeight)
            self.aboutTextViewHeightConstraint.constant = self.candidateDetail.educationTextViewHeight
            self.aboutTextView.text = self.candidateDetail.education
        } else {
            self.aboutTextView.hint = ""
            self.aboutTextView.hint = "Standout with a short but sweet opening, \ntry something like \"I'm an extremely \nfriendly person who loves working with \npeople and is an excellent communicator.\""
            self.eductaionTextfield.isHidden = true
            self.aboutTextView.isHidden = false
            self.mainLabel.text = "Introduce yourself!"
            self.subLabel.text = "Share a short blurb about yourself to help employers get to know you more"

            print_debug(self.candidateDetail.aboutMeTextViewheight)
            
            self.aboutTextViewHeightConstraint.constant = self.candidateDetail.aboutMeTextViewheight
            self.aboutTextView.text = self.candidateDetail.about
        }
    }
    
    func backButtonTapped() {
        if self.selectedMode == .education || fromProfile{
            self.navigationController?.popViewController(animated: true)
        }else{
            self.selectedMode = .education
            self.view.endEditing(true)
            setUpViewAccordingToMode()
        }
    }
    
    func skipButtonTapped() {
        
        if fromProfile{
            self.navigationController?.popViewController(animated: true)
        }else{
            if self.selectedMode == .education{
                self.selectedMode = .summary
                setUpViewAccordingToMode()
                self.delegate?.addProfileChildVC(viewController: self)
            }else{
                let obj = ProfilePictureStepVC.instantiate(fromAppStoryboard: .Recruiter)
                obj.candidateDetail = self.candidateDetail
                obj.job_id = self.job_id
                obj.recruiterId = self.recruiterId
                obj.categoryId = self.categoryId
                self.delegate?.addProfileChildVC(viewController: obj)
            }
        }
    }
    @IBAction func saveButtonTapped(_ sender: UIButton) {
        
        if fromProfile{
            postProfile()
            
        } else {
            if self.selectedMode == .education{
                self.view.endEditing(true)
                self.selectedMode = .summary
                setUpViewAccordingToMode()
                self.delegate?.addProfileChildVC(viewController: self)
                
            }else {
                let obj = ProfilePictureStepVC.instantiate(fromAppStoryboard: .Recruiter)
                self.view.endEditing(true)
                obj.candidateDetail = self.candidateDetail
                obj.job_id = self.job_id
                obj.recruiterId = self.recruiterId
                obj.categoryId = self.categoryId
                self.delegate?.addProfileChildVC(viewController: obj)
            }
        }
    }
}

extension EducationStepVC: UITextViewDelegate {
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        self.bottomSeperator.backgroundColor = AppColors.themeBlueColor
        return true
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        
        self.bottomSeperator.backgroundColor = AppColors.blakopacity15
        if self.selectedMode == .summary {
            self.candidateDetail.about = textView.text.trailingSpacesTrimmed
            self.candidateDetail.aboutMeTextViewheight = textView.frame.height
            self.educationDelegate?.didSaveText(text: textView.text, image: nil, imageURL: nil, mode: .summary, aboutTextViewHeight: self.candidateDetail.aboutMeTextViewheight, educationTextViewHeight:  self.candidateDetail.educationTextViewHeight)
            self.aboutText = textView.text.trailingSpacesTrimmed
        } else if self.selectedMode == .education {
            self.candidateDetail.education = textView.text.trailingSpacesTrimmed
            self.candidateDetail.educationTextViewHeight = textView.frame.height
            self.educationDelegate?.didSaveText(text: textView.text, image: nil, imageURL: nil, mode: .education, aboutTextViewHeight: self.candidateDetail.aboutMeTextViewheight, educationTextViewHeight: self.candidateDetail.educationTextViewHeight)
            self.education = textView.text.trailingSpacesTrimmed
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        print_debug(newText.count)
        
        return newText.count < 280
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let topBottomPadding: CGFloat = 30.0
        let threeLineTextHeight: CGFloat = 100.0
        let fixedWidth = textView.frame.size.width
        let fixedHeight = textView.frame.size.height
        
        let maximumTextHeight: CGFloat = threeLineTextHeight + topBottomPadding
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: fixedHeight))
        let textHeight = textView.text.heightWithConstrainedWidth(width: textView.width-10.0, font: textView.font!)
        
        if textHeight > threeLineTextHeight {
            textView.isScrollEnabled = true
            self.aboutTextViewHeightConstraint.constant = maximumTextHeight
            
        } else {
            textView.isScrollEnabled = false
            textView.frame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
            self.aboutTextViewHeightConstraint.constant = newSize.height
            
        }
        self.aboutText = textView.text.trailingSpacesTrimmed
        self.view.layoutIfNeeded()
    }
    
}

//Mark: Webservices
extension EducationStepVC{
    
    func postProfile(){
        WebServices.addProfile(parameters: self.candidateDetail.dictionary(), loader: true, success: { (dict) in
            self.navigationController?.popViewController(animated: true)
        }) { (error) in
            self.showAlert(msg: error.localizedDescription)
        }
    }
}
