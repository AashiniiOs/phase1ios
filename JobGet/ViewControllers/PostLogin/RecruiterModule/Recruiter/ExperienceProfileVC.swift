//
//  ExperienceProfileVC.swift
//  JobGet
//
//  Created by Appinventiv on 04/06/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol  AddProfileChildVCDelegate: class {
    
    func addProfileChildVC(viewController: UIViewController)
}


class ExperienceProfileVC: UIViewController {
    
    // MARK: - Outlets.
    // MARK: -
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var noExperienceButton: UIButton!
    @IBOutlet weak var addExperienceButton: UIButton!
    @IBOutlet weak var experienceTabelView: UITableView!
    @IBOutlet weak var shareExperienceLabel: UILabel!
    
    
    // MARK: - Properties.
    // MARK: -
    var job_id = ""
    var recruiterId = ""
    var categoryId = ""
    var fromTabbar = true
    var index : IndexPath?
    weak var delegate: AddProfileChildVCDelegate?
    var candidateDetail : CandidateProfileModel = CandidateProfileModel(withJSON: JSON([:]))
    var experianceData : ExperienceModel?
    var cardsCount = 0
    
    // MARK: - View life cycle.
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Actions.
    // MARK: -
    
    
    @IBAction func noExperienceButtonTapped(_ sender: UIButton) {
        
        let obj = EducationStepVC.instantiate(fromAppStoryboard: .Recruiter)
        obj.job_id = self.job_id
        obj.recruiterId = self.recruiterId
        obj.categoryId = self.categoryId
        self.candidateDetail.isExperience = 1 // 1 for experience
        obj.candidateDetail = self.candidateDetail
        self.delegate?.addProfileChildVC(viewController: obj)
        
    }
    
    @IBAction func addExperienceButtonTapped(_ sender: UIButton) {
        let obj = AddExperienceStepVC.instantiate(fromAppStoryboard: .Recruiter)
        obj.job_id = self.job_id
        obj.delegate = self
        self.candidateDetail.isExperience = 2 // // 2 for no experience
        obj.candidateDetail = self.candidateDetail
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
}
// MARK: - Private methods.
// MARK: -

extension ExperienceProfileVC{
    
    func initialSetup(){
        
        noExperienceButton.layer.cornerRadius = 4
        noExperienceButton.layer.borderWidth = 1.0
        noExperienceButton.layer.borderColor = #colorLiteral(red: 0.2389856699, green: 0.4166136601, blue: 0.6607115269, alpha: 1)
        noExperienceButton.isSelected = false
        
        addExperienceButton.layer.cornerRadius = 4
        addExperienceButton.layer.borderWidth = 1.0
        addExperienceButton.layer.borderColor = #colorLiteral(red: 0.2389856699, green: 0.4166136601, blue: 0.6607115269, alpha: 1)
        addExperienceButton.isSelected = false
        
        self.experienceTabelView.delegate = self
        self.experienceTabelView.dataSource = self
        self.experienceTabelView.estimatedRowHeight = 160
        self.experienceTabelView.rowHeight = UITableViewAutomaticDimension
        self.experienceTabelView.register(UINib(nibName: "SubmitButtonCell", bundle: nil), forCellReuseIdentifier: "SubmitButtonCell")
        self.experienceTabelView.tableHeaderView = headerView
        self.experienceTabelView.register(UINib(nibName: "ProfileExperienceCell", bundle: nil), forCellReuseIdentifier: "ProfileExperienceCell")
        
        if self.cardsCount > 0 {
            self.experienceTabelView.tableHeaderView?.frame.size = CGSize(width:  self.experienceTabelView.frame.width, height: CGFloat(0.0))
            self.addExperienceButton.isHidden = true
            self.noExperienceButton.isHidden = true
            self.shareExperienceLabel.isHidden = true
        }
    }
}

// MARK: - Table view delegate and datasource methods.
// MARK: -
extension ExperienceProfileVC: UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if self.candidateDetail.experience.isEmpty{
            return 0
        }else{
            print_debug(self.cardsCount)
            return self.cardsCount + 1
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        switch indexPath.row {
            
        case cardsCount:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ExperienceButtonCell", for: indexPath) as? ExperienceButtonCell else{
                fatalError("ExperienceButtonCell not found")
            }
            cell.doneButton.setTitle(StringConstants.next, for: .normal)
            cell.addMoreButton.addTarget(self, action: #selector(self.addmoreButtonTapp(_:)), for: .touchUpInside)
            cell.doneButton.addTarget(self, action: #selector(self.doneButtonTapp(_:)), for: .touchUpInside)
            return cell
            
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileExperienceCell", for: indexPath) as? ProfileExperienceCell else{
                fatalError("ProfileExperienceCell not found")
            }
            
            
            cell.populatedCell(data: self.candidateDetail, index: indexPath.row)
            
            
            cell.minusButton.addTarget(self, action: #selector(minusButtonTapped), for: .touchUpInside)
            
            return cell
            
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let obj = AddExperienceStepVC.instantiate(fromAppStoryboard: .Recruiter)
        obj.job_id = self.job_id
        obj.fromDidSelectRowAt = true
        self.experianceData = self.candidateDetail.experience[indexPath.row]
        obj.changeExpDelegate = self
        obj.row = indexPath.row
        obj.candidateDetail = self.candidateDetail
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == self.cardsCount{
            return UITableViewAutomaticDimension
            
        }else{
            return UITableViewAutomaticDimension
        }
    }
    
    @objc func minusButtonTapped(_ sender : UIButton){
        guard let indexPath = sender.tableViewIndexPath(self.experienceTabelView) as IndexPath? else{return}
        self.index = indexPath
        let popUpVc = RemoveExperiancePopupVC.instantiate(fromAppStoryboard: .RecruiterProfile)
        popUpVc.delegate = self
        
        popUpVc.modalPresentationStyle = .overCurrentContext
        popUpVc.modalTransitionStyle = .crossDissolve
        self.present(popUpVc, animated: true, completion: nil)
        
    }
    
    @objc func addmoreButtonTapp(_ sender: UIButton) {
        
        let obj = AddExperienceStepVC.instantiate(fromAppStoryboard: .Recruiter)
        obj.job_id = self.job_id
        obj.delegate = self
        self.navigationController?.pushViewController(obj, animated: true)
    }
    
    @objc func doneButtonTapp(_ sender: UIButton) {
        let obj = EducationStepVC.instantiate(fromAppStoryboard: .Recruiter)
        obj.job_id = self.job_id
        obj.recruiterId = self.recruiterId
        obj.categoryId = self.categoryId
        obj.candidateDetail = self.candidateDetail
        self.delegate?.addProfileChildVC(viewController: obj)
    }
}

// MARK: - Alert Popup Delegate .
// MARK: -

extension ExperienceProfileVC:  AlertPopUpDelegate {
    
    func didTapAffirmativeButton() {
        if let idx = self.index {
            self.candidateDetail.experience.remove(at: idx.row)
            self.cardsCount -= 1
            self.experienceTabelView.reloadData()
            if self.candidateDetail.experience.isEmpty{
                self.experienceTabelView.tableHeaderView?.frame.size = CGSize(width:  self.experienceTabelView.frame.width, height: CGFloat(70.0))
                self.addExperienceButton.isHidden = false
                self.noExperienceButton.isHidden = false
                self.shareExperienceLabel.isHidden = false

            }
        }
    }
    
    func didTapNegativeButton() {
        // TO DO TASK
    }
}

// MARK: - AddExperienceDelegate method.
// MARK: -
extension ExperienceProfileVC: AddExperienceDelegate {
    
    func addExperience(experienceData: CandidateProfileModel) {
        
        
        let _ = experienceData.experience.map{value in
            
            self.candidateDetail.experience.append(value)
        }
        self.cardsCount = self.candidateDetail.experience.count
        self.experienceTabelView.tableHeaderView?.frame.size = CGSize(width:  self.experienceTabelView.frame.width, height: CGFloat(0.0))
        self.addExperienceButton.isHidden = true
        self.noExperienceButton.isHidden = true
        self.shareExperienceLabel.isHidden = true

        self.experienceTabelView.reloadData()
    }
    
}

// MARK: - ChangeExpProtocol method.
// MARK: -
extension ExperienceProfileVC: ChangeExpProtocol {
    
    func chageExp(experiance: ExperienceModel, arayIndex: Int) {
        
        
        self.candidateDetail.experience.remove(at: arayIndex)
        self.candidateDetail.experience.insert(experiance, at: arayIndex)
        print_debug(self.candidateDetail.experience)
        self.experienceTabelView.reloadData()
    }
    
}

class ExperienceButtonCell: UITableViewCell{
    
    @IBOutlet weak var addMoreButton: UIButton!
    @IBOutlet weak var doneButton: UIButton!
}
