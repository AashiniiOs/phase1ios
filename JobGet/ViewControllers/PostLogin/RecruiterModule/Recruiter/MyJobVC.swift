//
//  MyJobVC.swift
//  JobGet
//
//  Created by macOS on 09/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import SwiftyJSON
import OnlyPictures
import DZNEmptyDataSet

class MyJobVC: BaseVC, UIGestureRecognizerDelegate {
    
    
    //MARK:- Properties
    //==================
    var pageNo = 0
    var myJobList = [MyJobListModel]()
    var shouldLoadMoreData = false
    var isFirstTimeLoad   = true
    var isPaginationEnable = false
    var latestAddedJobId: String?
    var pulsAnimatedJobID: String?
    var selectedSpotlightIndex : Int?
    var seconds = [Int]()//This variable will hold a starting value of seconds. It could be any amount above 0.
    var timer = Timer()
    var isTimerRunning = false //This will be used to make sure only one timer is created at a time.
    var fromPush = false
    var jobId = ""

    //MARK:- IBoutlets
    //================
    
    @IBOutlet weak var badgeLabel: UILabel!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var navTitleLabel: UILabel!
    @IBOutlet weak var notificationButton: UIButton!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var myJobTableView: UITableView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var addJobButton: UIButton!
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if fromPush{
            let sceen = JobDetailVC.instantiate(fromAppStoryboard: .PostJob)
            sceen.page = self.pageNo
            sceen.isOpenFromPush = self.fromPush
            sceen.jobID = self.jobId
            self.navigationController?.pushViewController(sceen, animated: true)
            fromPush = false
        }
        
        isPaginationEnable = false
        self.getJobList(loader: self.isFirstTimeLoad, searchText: "", pageNo: 0, latitude: "", longitude: "")

        if AppUserDefaults.value(forKey: .totalNotification).intValue > 0{
            self.badgeLabel.isHidden = false
        }else{
            self.badgeLabel.isHidden = true
        }
       runTimer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.latestAddedJobId = ""
        timer.invalidate()
        self.myJobTableView.reloadData()
       
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        if let index = self.selectedSpotlightIndex {
            self.myJobTableView.reloadRows(at: [[0,index]], with: .none)
            self.selectedSpotlightIndex = nil
            self.myJobTableView.isUserInteractionEnabled = true
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func runTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer() {
        if let index = self.selectedSpotlightIndex {
            self.seconds[index] -= 1

            guard let cell = self.myJobTableView.cellForRow(at: [0,index]) as? MyJobCell else{return}
            cell.spotlightViewLabel.text = "Job Spotlighted for " + timeString(time: TimeInterval(self.seconds[index]))

        }
//This will decrement(count down)the seconds.
//        timerLabel.text = “\(seconds)” //This will update the label.
    }
    
    func timeString(time:TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }

    @objc func shareButtonTapped(_ sender: UIButton) {
        guard let index = sender.tableViewIndexPath(self.myJobTableView) else { return }
        
        self.displayShareSheet(shareContent: self.myJobList[index.row].shareUrl)
    }
    
    @objc func featuredJobBtnTapped(_ sender: UIButton) {
    
        guard let index = sender.tableViewIndexPath(self.myJobTableView) else { return }
        guard let cell = sender.tableViewCell as? MyJobCell else{return}
        if self.myJobList[index.row].isPremium{
            cell.spotLightView.isHidden = false
            cell.spotlightViewLabel.text = "Job Spotlighted for " + timeString(time: TimeInterval(self.seconds[index.row]))
            self.myJobTableView.isUserInteractionEnabled = false
            self.selectedSpotlightIndex = index.row
        }else{
            let sceen = FeaturedPostVC.instantiate(fromAppStoryboard: .PostJob)
            sceen.isFromDetail = true
            sceen.jobId = self.myJobList[index.row].jobId
            self.navigationController?.pushViewController(sceen, animated: true)
        }
    }
    
    @objc func applicantsBtnTapped(_ sender: UIButton) {
        guard let index = sender.tableViewIndexPath(self.myJobTableView) else { return }
        let sceen = JobDetailVC.instantiate(fromAppStoryboard: .PostJob)
        sceen.page = self.pageNo
        sceen.isForApplicantsBtnTap = true
        sceen.jobID = self.myJobList[index.row].jobId
        self.navigationController?.pushViewController(sceen, animated: true)
    }
}

//MARK:- Private Methods
//=======================
private extension MyJobVC {
    
    func initialSetup() {
        
        self.navigationController?.interactivePopGestureRecognizer?.delegate = self
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        
        self.myJobTableView.delegate   = self
        self.myJobTableView.dataSource = self
        self.myJobTableView.emptyDataSetSource = self
        self.myJobTableView.emptyDataSetDelegate = self
        self.searchButton.isHidden = true
        self.navTitleLabel.text = StringConstants.Postings.uppercased()
        self.badgeLabel.roundCorners()
        
        if AppUserDefaults.value(forKey: .totalNotification).intValue > 0{
            self.badgeLabel.isHidden = false
        }else{
            self.badgeLabel.isHidden = true
        }
        
        self.setupViewLayout()
        self.registerNib()
        self.myJobTableView.enablePullToRefresh(tintColor: AppColors.themeBlueColor ,target: self, selector: #selector(refreshWhenPull(_:)))
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.jobIdForAnimateSpotlightIcon(_:)), name: NSNotification.Name("Animating_Sptlight_Icon"), object: nil)
    }
    
    func registerNib() {
        
        self.myJobTableView.register(UINib(nibName: "LoadMoreTableViewCell", bundle: nil), forCellReuseIdentifier: "LoadMoreTableViewCell")
        let featureCell = UINib(nibName: "FeatureCell", bundle: nil)
        self.myJobTableView.register(featureCell, forCellReuseIdentifier: "FeatureCell")
    }
    
    @objc func jobIdForAnimateSpotlightIcon(_ noti: Notification){
        
        print_debug(noti)
        if let data = noti.object as? [String:Any], let job_id = data["jobId"] as? String{
            
            self.latestAddedJobId =  job_id
            self.pulsAnimatedJobID = job_id
            self.myJobTableView.reloadData()
        }
    }
    
    func isLoadingIndexPath(_ indexPath: IndexPath) -> Bool {
        guard shouldLoadMoreData else { return false}
        
        return indexPath.row == self.myJobList.count
    }
    
    func setupViewLayout() {
        
        self.navigationView.setShadow(offset: CGSize(width: 0.0, height: 2.0))
        self.addJobButton.roundCorners()
    }
    
    @objc func refreshWhenPull(_ sender: UIRefreshControl) {
        sender.endRefreshing()
        self.latestAddedJobId = ""
        self.pageNo = 0
        self.shouldLoadMoreData = false
        self.isPaginationEnable = false
        self.getJobList(loader: false, searchText: "", pageNo: 0, latitude: "", longitude: "")
    }
}

//MARK: - IB Action and Target
//===============================
extension MyJobVC {
    
    
    @IBAction func addJobBtnTapped(_ sender: UIButton) {
        
        let sceen = JobCategoryVC.instantiate(fromAppStoryboard: .PostJob)
        self.navigationController?.pushViewController(sceen, animated: true)
    }
    
    
    @IBAction func notificationBtnTapped(_ sender: UIButton) {
        
        let notificationVC = NotificationsVC.instantiate(fromAppStoryboard: .settingsAndChat)
        self.navigationController?.pushViewController(notificationVC, animated: true)
        
    }
    
    @IBAction func searchBtnTapped(_ sender: UIButton) {
        let searchScene = SearchJobsVC.instantiate(fromAppStoryboard: .Candidate)
        searchScene.delegate = self
        
        self.present(searchScene, animated: true, completion: nil)
    }
}

//MARK:- Web Services
//====================
extension MyJobVC {
    
    func getJobList(loader: Bool, searchText: String, pageNo: Int, latitude: String, longitude: String) {
        
        let param = ["recruiterId": AppUserDefaults.value(forKey: .userId), "page" :pageNo,  ApiKeys.search.rawValue:  searchText,
                     ApiKeys.latitude.rawValue: latitude, ApiKeys.longitude.rawValue: longitude] as [String : Any]
        WebServices.recruiterJobList(parameters: param, loader: loader, success: { (json) in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                
                self.isFirstTimeLoad = false
                var data = [MyJobListModel]()
                
                if pageNo == 0{
                    self.myJobList.removeAll(keepingCapacity: false)
                }
                let jobs = json["data"]["jobs"].arrayValue
                
                for job in jobs {
                    data.append(MyJobListModel(dict: job))
                }
                if self.shouldLoadMoreData {
                    self.myJobList.append(contentsOf: data)
                } else {
                    self.myJobList = data
                }
                self.seconds.removeAll()
                for time in self.myJobList{
                    self.seconds.append(CommonClass.calculateTimeDifference(to: time.premiumEnd))

                }
                self.shouldLoadMoreData = json[ApiKeys.next.rawValue].boolValue
                self.pageNo = json["page"].intValue
                self.myJobTableView.reloadData()
                
            }  else {
                
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
            
        }) { (error: Error) in
            CommonClass.showToast(msg: error.localizedDescription )
        }
        
    }
}
//MARK: - Table view Delegate and DataSource
//============================================
extension MyJobVC: UITableViewDelegate, UITableViewDataSource {
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let count = self.myJobList.count
        return shouldLoadMoreData ? count + 1 : count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isLoadingIndexPath(indexPath){
            guard let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.defaultReuseIdentifier, for: indexPath) as? LoadMoreTableViewCell else {
                fatalError("Failed to initialize LoadMoreTableViewCell")
            }
            self.isPaginationEnable = true
            return cell
        }
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MyJobCell") as? MyJobCell else { fatalError("invalid cell \(self)")
        }
        
        cell.shareButton.addTarget(self, action: #selector(self.shareButtonTapped(_:)), for: .touchUpInside)
        cell.applicantsButton.addTarget(self, action: #selector(self.applicantsBtnTapped(_:)), for: .touchUpInside)
//        if !self.myJobList[indexPath.row].isPremium{
        cell.spotlightButton.addTarget(self, action: #selector(self.featuredJobBtnTapped(_:)), for: .touchUpInside)
//        }

        cell.populate(data: self.myJobList[indexPath.row])
        cell.spotLightView.isHidden = true
        if self.latestAddedJobId == self.myJobList[indexPath.row].jobId{
            UIView.animate(withDuration: 0.5, animations: {
                cell.spotlightAlertHight.constant = 40
            })
            
//            CommonClass.showWithDelay(msg: StringConstants.Spotlight_Job_Toast_Text)
            if let id = pulsAnimatedJobID, !id.isEmpty{
                cell.spotlightLabel.text = StringConstants.Feature_this_post
                
                cell.countView.layoutIfNeeded()
                let pulse1 = CASpringAnimation(keyPath: "transform.scale")
                pulse1.duration = 10
                pulse1.fromValue = 1.0
                pulse1.toValue = 1.15
                pulse1.autoreverses = true
                pulse1.repeatCount = 1
                pulse1.initialVelocity = 1
                pulse1.damping = 0.5
                
                let animationGroup = CAAnimationGroup()
                animationGroup.duration = 25
                animationGroup.repeatCount = 50
                animationGroup.animations = [pulse1]
                cell.jobProImageContainerView.layer.add(animationGroup, forKey: "pulse")
                cell.spotlightLabel.layer.add(animationGroup, forKey: "pulse")
                CommonClass.delayWithSeconds(10.0, completion: {
                    UIView.animate(withDuration: 0.5, animations: {
                        cell.spotlightLabel.text = ""
                        cell.spotlightAlertHight.constant = 0
                        cell.spotlightLabel.layer.removeAllAnimations()
                        cell.jobProImageContainerView.layer.removeAllAnimations()
                        self.latestAddedJobId = ""

//                        cell.layoutIfNeeded()
                        tableView.reloadData()
                    })
//                    cell.countView.layoutIfNeeded()
                })
//                cell.countView.layoutIfNeeded()
            }
           
            self.pulsAnimatedJobID = ""
        }
      
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard self.shouldLoadMoreData else { return }
        
        if indexPath.row == self.myJobList.count-1 && self.shouldLoadMoreData  && isPaginationEnable{  //for 2nd last cell
            self.getJobList(loader: false, searchText: "", pageNo: self.pageNo, latitude: "", longitude: "")
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let sceen = JobDetailVC.instantiate(fromAppStoryboard: .PostJob)
        sceen.page = self.pageNo
        sceen.jobID = self.myJobList[indexPath.row].jobId
        self.navigationController?.pushViewController(sceen, animated: true)
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    }
}


//MARK:- Get Search Text
//===========================
extension MyJobVC: SearchCompelete {
    func getSearchedText(text: String, lat: String, long: String, radius: String, sort: Int, categoryID: String, selectedCatFilterIndex: [IndexPath], categoryName: String, isClear: Bool) {
        self.getJobList(loader: true, searchText: text, pageNo: 0, latitude: lat, longitude: long)

    }
}


//MARK:- DZNEmptyDataSource and Delegate
//==========================================
extension MyJobVC: DZNEmptyDataSetDelegate, DZNEmptyDataSetSource {
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return #imageLiteral(resourceName: "icHomeNoJobs")
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: StringConstants.No_Jobs_Posted,                                                                           attributes: [NSAttributedStringKey.foregroundColor: AppColors.black46,
                                                                                                                                                                 NSAttributedStringKey.font: AppFonts.Poppins_Medium.withSize(14)
            ])
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        return NSAttributedString(string: StringConstants.There_are_many_candidates_looking_for_jobs,                                                                           attributes: [NSAttributedStringKey.foregroundColor: AppColors.gray152,
                                                                                                                                                                                             NSAttributedStringKey.font: AppFonts.Poppins_Regular.withSize(12)                                                                                                                                                                                                            ])
    }
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}



//MARK:- Prototype Cell
//=========================
class MyJobCell: UITableViewCell {
    
    //MARK:- Properties
    //==================
    var picturesArray = [String]()
    
    //MARK:- IBoutlets
    //================
    
    @IBOutlet weak var spotlightAlertHight: NSLayoutConstraint!
    @IBOutlet weak var spotlightAlertLabel: UILabel!
    @IBOutlet weak var spotlightAlertView: UIView!
    
    @IBOutlet weak var jobProImageContainerView: UIView!
    @IBOutlet weak var jobProImageView: UIImageView!
    
    @IBOutlet weak var spotlightButton: UIButton!
    @IBOutlet weak var spotlightLabel: UILabel!
    
    @IBOutlet weak var categoryBgView: UIView!
    @IBOutlet weak var imageOneHorizontalConstraint: NSLayoutConstraint!
    @IBOutlet weak var applicantsLblLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var applicantsButton: UIButton!
    @IBOutlet weak var noApplicantsLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var countView: UIView!
    @IBOutlet weak var imageThree: UIImageView!
    @IBOutlet weak var imageTwo: UIImageView!
    @IBOutlet weak var imageOne: UIImageView!
    @IBOutlet weak var jobImageView             : UIImageView!
    @IBOutlet weak var jobNameLabel             : UILabel!
    @IBOutlet weak var containerView            : UIView!
    @IBOutlet weak var locationButton: UIButton!
    @IBOutlet weak var applicantsLabel: UILabel!
    @IBOutlet weak var jobPostTimeLabel: UILabel!
    @IBOutlet weak var jobStatusLabel: UILabel!
    @IBOutlet weak var jobCategoryLabel: UILabel!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var jobSeenButton: UIButton!
    @IBOutlet weak var applicantsContainerView: UIView!
    @IBOutlet weak var imageFour: UIImageView!
    @IBOutlet weak var imageFive: UIImageView!
    @IBOutlet weak var imageSix: UIImageView!
    @IBOutlet weak var imageSeven: UIImageView!
    @IBOutlet weak var imageEight: UIImageView!
    @IBOutlet weak var imageNine: UIImageView!
    @IBOutlet weak var imageTen: UIImageView!
    
    @IBOutlet weak var countLabel4: UILabel!
    @IBOutlet weak var countLabel5: UILabel!
    @IBOutlet weak var countLabel6: UILabel!
    @IBOutlet weak var countLabel7: UILabel!
    @IBOutlet weak var countLabel8: UILabel!
    @IBOutlet weak var countLabel9: UILabel!
    @IBOutlet weak var countLabel10: UILabel!
    
    @IBOutlet weak var countView4: UIView!
    @IBOutlet weak var countView5: UIView!
    @IBOutlet weak var countView6: UIView!
    @IBOutlet weak var countView7: UIView!
    @IBOutlet weak var countView8: UIView!
    @IBOutlet weak var countView9: UIView!
    @IBOutlet weak var countView10: UIView!
    @IBOutlet weak var spotLightView: UIView!
    @IBOutlet weak var spotlightViewLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.spotlightLabel.text =  ""
        self.locationButton.setTitleColor(AppColors.black46, for: .normal)
        self.applicantsLabel.textColor = AppColors.black46
        self.jobPostTimeLabel.textColor = AppColors.gray152
        self.jobCategoryLabel.textColor = AppColors.gray152
        self.spotlightAlertHight.constant = 0
        self.jobStatusLabel.textColor = AppColors.black46
        self.jobSeenButton.setTitleColor(AppColors.gray152, for: .normal)
        self.spotLightView.isHidden = true
        self.jobImageView.setCorner(cornerRadius: 5.0, clip: true)
        self.containerView.setShadow()
        self.setupIntrestedView()

    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.spotlightAlertHight.constant = 0
        print_debug("prepare for reuser ")
    }
    
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.spotlightAlertView.roundCornersFromOneSide([.topLeft, .topRight], radius: 5.0)
        self.jobProImageContainerView.roundCorners()
        self.categoryBgView.roundCorners()
        self.containerView.setCorner(cornerRadius: 5.0, clip: false)

    }
    
    func setupIntrestedView() {
        self.imageOne.roundCorners()
        self.imageTwo.roundCorners()
        self.imageThree.roundCorners()
        self.imageFour.roundCorners()
        self.imageFive.roundCorners()
        self.imageSix.roundCorners()
        self.imageSeven.roundCorners()
        self.imageEight.roundCorners()
        self.imageNine.roundCorners()
        self.imageTen.roundCorners()
        self.countView.roundCorners()
        self.countLabel.roundCorners()
        
        self.countView4.roundCorners()
        self.countLabel4.roundCorners()
        self.countView5.roundCorners()
        self.countLabel5.roundCorners()
        self.countView6.roundCorners()
        self.countLabel6.roundCorners()
        self.countView7.roundCorners()
        self.countLabel7.roundCorners()
        self.countView8.roundCorners()
        self.countLabel8.roundCorners()
        self.countView9.roundCorners()
        self.countLabel9.roundCorners()
        
        self.countView10.roundCorners()
        self.countLabel10.roundCorners()
        
        self.noApplicantsLabel.textColor = AppColors.black46
        self.noApplicantsLabel.isHidden = true
    }
    
    func setupIntrestedViewToDefault() {
        self.imageOne.isHidden = false
        self.imageTwo.isHidden = false
        self.imageThree.isHidden = false
        
        self.imageFour.isHidden = false
        self.imageFive.isHidden = false
        self.imageSix.isHidden = false
        
        self.imageSeven.isHidden = false
        self.imageEight.isHidden = false
        self.imageNine.isHidden = false
        self.imageTen.isHidden = false
        
        self.countView4.isHidden = false
        self.countLabel4.isHidden = false
        self.countView5.isHidden = false
        self.countLabel5.isHidden = false
        self.countView6.isHidden = false
        self.countLabel6.isHidden = false
        self.countView7.isHidden = false
        self.countLabel7.isHidden = false
        self.countView8.isHidden = false
        self.countLabel8.isHidden = false
        self.countView9.isHidden = false
        self.countLabel9.isHidden = false
        
        self.countView10.isHidden = false
        self.countLabel10.isHidden = false
        
        
        self.countLabel.isHidden = false
        self.countView.isHidden = false
        self.noApplicantsLabel.isHidden = true
        self.applicantsLabel.isHidden = false
        
        
    }
    
    func populate(data: MyJobListModel) {
        
        self.jobNameLabel.text = data.jobTitle
        var time = ""
        if let timeDate = data.createdAt {
            time = timeDate.elapsedTime
        }
        self.jobPostTimeLabel.text = "\(time)"
        self.spotlightLabel.text =  ""
        if !data.isPremium {
//            self.jobStatusLabel.text = StringConstants.Status_Normal
            self.jobProImageView.image = #imageLiteral(resourceName: "icSpotlightDisable")
//            self.spotlightLabel.text = StringConstants.Job_spotlighted
        } else {
//            self.jobStatusLabel.text = StringConstants.Status_Featured
              self.jobProImageView.image = #imageLiteral(resourceName: "icSpotlightEnable")
//            self.spotlightLabel.text = StringConstants.Job_spotlighted
        }

        
        self.jobCategoryLabel.text = data.categoryName
        let image = data.jobImage.replacingOccurrences(of: "\"", with: "")
        self.jobImageView.sd_setImage(with: URL(string: image),  placeholderImage: #imageLiteral(resourceName: "icHomeCategoryPlaceholder1"), completed: nil)
//        self.jobSeenButton.setTitle("\(data.totalJobView)", for: .normal)
//        self.jobSeenButton.setTitle(String(data.totalJobView), for: .normal)
            self.jobSeenButton.isHidden = true
        var address = ""
        if !data.jobCity.isEmpty {
            address.append(data.jobCity)
        }
        
        if data.deleteStatus == 3 {
            self.containerView.backgroundColor = AppColors.cellCancel
            self.applicantsContainerView.backgroundColor = AppColors.cellCancel
        } else {
            self.containerView.backgroundColor = AppColors.white
            self.applicantsContainerView.backgroundColor = AppColors.white
        }
        
        switch data.applicantsImage.count {
        case 0:
            self.hideImage(with: 9, applicantsImages: data.applicantsImage)
        case 1:
            self.hideImage(with: 8, applicantsImages: data.applicantsImage)
        case 2:
            self.hideImage(with: 7, applicantsImages: data.applicantsImage)
        case 3:
            self.hideImage(with: 6, applicantsImages: data.applicantsImage)
        case 4:
            self.hideImage(with: 5, applicantsImages: data.applicantsImage)
        case 5:
            self.hideImage(with: 4, applicantsImages: data.applicantsImage)
        case 6:
            self.hideImage(with: 3, applicantsImages: data.applicantsImage)
        case 7:
            self.hideImage(with: 2, applicantsImages: data.applicantsImage)
        case 8:
            self.hideImage(with: 1, applicantsImages: data.applicantsImage)
        case 9:
            self.hideImage(with: 0, applicantsImages: data.applicantsImage)
            
        default:
            self.hideImage(with: 10, applicantsImages: data.applicantsImage)
        }
        
        getAbbreatedState { (stateList) in
            
            _ = stateList.map{ value in
                
                if data.jobState.contains(s:  value["state_name"] as? String ?? ""  ){
                    if let abState = value["state_abbreviations"] {
                        address.append(", ")
                        address.append(String(describing: abState))
                    }
                }
            }
        }
        if !address.contains(",") {
            address.append(", ")
            address.append(data.jobState)
        }
        self.locationButton.setTitle(address, for: .normal)
        
    }
    
    func hideImage(with count: Int, applicantsImages: [String]) {
        self.setupIntrestedViewToDefault()
        switch count {
        case 0:
            self.imageOne.sd_setImage(with: URL(string: applicantsImages[0]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageTwo.sd_setImage(with: URL(string: applicantsImages[1]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageThree.sd_setImage(with: URL(string: applicantsImages[2]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageFour.sd_setImage(with: URL(string: applicantsImages[3]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageFive.sd_setImage(with: URL(string: applicantsImages[4]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageSix.sd_setImage(with: URL(string: applicantsImages[5]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageSeven.sd_setImage(with: URL(string: applicantsImages[6]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageEight.sd_setImage(with: URL(string: applicantsImages[7]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageNine.sd_setImage(with: URL(string: applicantsImages[8]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.countLabel9.text = String(applicantsImages.count )
            
            self.imageTen.isHidden = true
            self.countView4.isHidden = true
            self.countView5.isHidden = true
            self.countView6.isHidden = true
            self.countView7.isHidden = true
            self.countView8.isHidden = true
            self.countView10.isHidden = true
            self.countView.isHidden = true
            
            self.countView9.isHidden = false
            self.applicantsLabel.text = StringConstants.Applicants
            self.imageOneHorizontalConstraint.constant = -90
            self.applicantsLblLeadingConstraint.constant = 90
            
        case 1:
            self.imageOne.sd_setImage(with: URL(string: applicantsImages[0]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageTwo.sd_setImage(with: URL(string: applicantsImages[1]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageThree.sd_setImage(with: URL(string: applicantsImages[2]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageFour.sd_setImage(with: URL(string: applicantsImages[3]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageFive.sd_setImage(with: URL(string: applicantsImages[4]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageSix.sd_setImage(with: URL(string: applicantsImages[5]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageSeven.sd_setImage(with: URL(string: applicantsImages[6]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageEight.sd_setImage(with: URL(string: applicantsImages[7]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.countLabel8.text = String(applicantsImages.count )
            self.countView4.isHidden = true
            self.countView5.isHidden = true
            self.countView7.isHidden = true
            self.countView6.isHidden = true
            self.countView9.isHidden = true
            self.countView10.isHidden = true
            self.imageNine.isHidden = true
            self.imageTen.isHidden = true
            self.countView.isHidden = true
            self.countView8.isHidden = false
            self.applicantsLabel.text = StringConstants.Applicants
            self.imageOneHorizontalConstraint.constant = -75
            self.applicantsLblLeadingConstraint.constant = 75
            
        case 2:
            self.imageOne.sd_setImage(with: URL(string: applicantsImages[0]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageTwo.sd_setImage(with: URL(string: applicantsImages[1]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageThree.sd_setImage(with: URL(string: applicantsImages[2]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageFour.sd_setImage(with: URL(string: applicantsImages[3]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageFive.sd_setImage(with: URL(string: applicantsImages[4]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageSix.sd_setImage(with: URL(string: applicantsImages[5]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageSeven.sd_setImage(with: URL(string: applicantsImages[6]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.countLabel7.text = String(applicantsImages.count )
            self.countView7.isHidden = false
            
            self.countView4.isHidden = true
            self.countView5.isHidden = true
            self.countView6.isHidden = true
            self.countView8.isHidden = true
            self.countView9.isHidden = true
            self.countView10.isHidden = true
            self.imageEight.isHidden = true
            self.imageNine.isHidden = true
            self.imageTen.isHidden = true
            self.countView.isHidden = true
            self.applicantsLabel.text = StringConstants.Applicants
            self.applicantsLblLeadingConstraint.constant = 60
            
        case 3:
            //done
            self.imageOne.sd_setImage(with: URL(string: applicantsImages[0]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageTwo.sd_setImage(with: URL(string: applicantsImages[1]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageThree.sd_setImage(with: URL(string: applicantsImages[2]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageFour.sd_setImage(with: URL(string: applicantsImages[3]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageFive.sd_setImage(with: URL(string: applicantsImages[4]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageSix.sd_setImage(with: URL(string: applicantsImages[5]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.countLabel6.text = String(applicantsImages.count )
            self.countView6.isHidden = false
            
            self.countView.isHidden = true
            self.imageSeven.isHidden = true
            self.imageEight.isHidden = true
            self.imageNine.isHidden = true
            self.imageTen.isHidden = true
            self.countView4.isHidden = true
            self.countView5.isHidden = true
            self.countView7.isHidden = true
            self.countView8.isHidden = true
            self.countView9.isHidden = true
            self.countView10.isHidden = true
            
            self.applicantsLabel.text = StringConstants.Applicants
            self.applicantsLblLeadingConstraint.constant = 50
        case 4:
            print_debug(4)
            //done
            self.imageOne.sd_setImage(with: URL(string: applicantsImages[0]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageTwo.sd_setImage(with: URL(string: applicantsImages[1]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageThree.sd_setImage(with: URL(string: applicantsImages[2]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageFour.sd_setImage(with: URL(string: applicantsImages[3]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageFive.sd_setImage(with: URL(string: applicantsImages[4]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.countLabel5.text = String(applicantsImages.count )
            self.countView5.isHidden = false
            
            self.imageSix.isHidden = true
            self.imageSeven.isHidden = true
            self.imageEight.isHidden = true
            self.imageNine.isHidden = true
            self.imageTen.isHidden = true
            self.countView4.isHidden = true
            self.countView6.isHidden = true
            self.countView7.isHidden = true
            self.countView8.isHidden = true
            self.countView9.isHidden = true
            self.countView10.isHidden = true
            self.countView.isHidden = true
            self.applicantsLabel.text = StringConstants.Applicants
            self.applicantsLblLeadingConstraint.constant = 33
        case 5:
            print_debug(5)
            
            self.imageOne.sd_setImage(with: URL(string: applicantsImages[0]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageTwo.sd_setImage(with: URL(string: applicantsImages[1]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageThree.sd_setImage(with: URL(string: applicantsImages[2]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageFour.sd_setImage(with: URL(string: applicantsImages[3]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.countLabel4.text = String(applicantsImages.count )
            self.countView.isHidden = true
            self.imageFive.isHidden = true
            self.imageSix.isHidden = true
            self.imageSeven.isHidden = true
            self.imageEight.isHidden = true
            self.imageNine.isHidden = true
            self.imageTen.isHidden = true
            self.countView5.isHidden = true
            self.countView6.isHidden = true
            self.countView7.isHidden = true
            self.countView8.isHidden = true
            self.countView9.isHidden = true
            self.countView10.isHidden = true
            
            self.countView4.isHidden = false
            
            self.applicantsLabel.text = StringConstants.Applicants
            self.applicantsLblLeadingConstraint.constant = 23
            
        case 6:
            print_debug(6)
            
            self.imageFour.isHidden = true
            self.imageFive.isHidden = true
            self.imageSix.isHidden = true
            
            self.imageSeven.isHidden = true
            self.imageEight.isHidden = true
            self.imageNine.isHidden = true
            self.imageTen.isHidden = true
            
            self.countView4.isHidden = true
            self.countView5.isHidden = true
            self.countView6.isHidden = true
            self.countView7.isHidden = true
            self.countView8.isHidden = true
            self.countView9.isHidden = true
            self.countView10.isHidden = true
            self.imageOne.sd_setImage(with: URL(string: applicantsImages[0]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageTwo.sd_setImage(with: URL(string: applicantsImages[1]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageThree.sd_setImage(with: URL(string: applicantsImages[2]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.countLabel.text = String(applicantsImages.count )
            self.applicantsLabel.text = StringConstants.Applicants
            self.applicantsLblLeadingConstraint.constant = 10
        case 7:
            print_debug(7)
            
            self.imageOne.isHidden = true
            self.imageFour.isHidden = true
            self.imageFive.isHidden = true
            self.imageSix.isHidden = true
            
            self.imageSeven.isHidden = true
            self.imageEight.isHidden = true
            self.imageNine.isHidden = true
            self.imageTen.isHidden = true
            self.countView4.isHidden = true
            self.countView5.isHidden = true
            self.countView6.isHidden = true
            self.countView7.isHidden = true
            self.countView8.isHidden = true
            self.countView9.isHidden = true
            self.countView10.isHidden = true
            
            self.imageTwo.sd_setImage(with: URL(string: applicantsImages[0]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageThree.sd_setImage(with: URL(string: applicantsImages[1]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.countLabel.text = String(applicantsImages.count)
            self.applicantsLabel.text = StringConstants.Applicants
        case 8:
            print_debug(8)
            self.imageOne.isHidden = true
            self.imageTwo.isHidden = true
            
            self.imageFour.isHidden = true
            self.imageFive.isHidden = true
            self.imageSix.isHidden = true
            
            self.imageSeven.isHidden = true
            self.imageEight.isHidden = true
            self.imageNine.isHidden = true
            self.imageTen.isHidden = true
            
            self.countView4.isHidden = true
            self.countView5.isHidden = true
            self.countView6.isHidden = true
            self.countView7.isHidden = true
            self.countView8.isHidden = true
            self.countView9.isHidden = true
            self.countView10.isHidden = true
            self.imageThree.sd_setImage(with: URL(string: applicantsImages[0]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.countLabel.text = String(applicantsImages.count)
            self.applicantsLabel.text = StringConstants.Applicant
        case 9:
            print_debug(9)
            
            self.imageOne.isHidden = true
            self.imageTwo.isHidden = true
            self.imageThree.isHidden = true
            self.imageFour.isHidden = true
            self.imageFive.isHidden = true
            self.imageSix.isHidden = true
            
            self.imageSeven.isHidden = true
            self.imageEight.isHidden = true
            self.imageNine.isHidden = true
            self.imageTen.isHidden = true
            self.countLabel.isHidden = true
            self.countView.isHidden = true
            self.countView4.isHidden = true
            self.countView5.isHidden = true
            self.countView6.isHidden = true
            self.countView7.isHidden = true
            self.countView8.isHidden = true
            self.countView9.isHidden = true
            self.countView10.isHidden = true
            self.noApplicantsLabel.isHidden = false
            self.applicantsLabel.isHidden = true
            self.noApplicantsLabel.text = StringConstants.NoApplicants
        default:
            self.imageOne.sd_setImage(with: URL(string: applicantsImages[0]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageTwo.sd_setImage(with: URL(string: applicantsImages[1]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageThree.sd_setImage(with: URL(string: applicantsImages[2]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageFour.sd_setImage(with: URL(string: applicantsImages[3]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageFive.sd_setImage(with: URL(string: applicantsImages[4]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageSix.sd_setImage(with: URL(string: applicantsImages[5]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageSeven.sd_setImage(with: URL(string: applicantsImages[6]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageEight.sd_setImage(with: URL(string: applicantsImages[7]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageNine.sd_setImage(with: URL(string: applicantsImages[8]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageTen.sd_setImage(with: URL(string: applicantsImages[9]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.countLabel10.text = String(applicantsImages.count )
            
            self.countView.isHidden = true
            self.countView4.isHidden = true
            self.countView5.isHidden = true
            self.countView6.isHidden = true
            self.countView7.isHidden = true
            self.countView8.isHidden = true
            self.countView9.isHidden = true
            self.countView10.isHidden = false
            
            self.applicantsLabel.text = StringConstants.Applicants
            self.imageOneHorizontalConstraint.constant = -90
            self.applicantsLblLeadingConstraint.constant = 105
        }
    }
}

