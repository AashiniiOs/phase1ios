//
//  PendingJobVC.swift
//  JobGet
//
//  Created by macOS on 10/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import SwiftyJSON
import DZNEmptyDataSet

protocol PendingJobCountHeader: class {
    
    func getPendingJobCount(headerdata: [HeaderJobCount])
    
}

protocol ShouldOpenStatusPopup: class {
    func presentShortlistedPopup(jobApplyId: String, categoryId: String)
}


protocol DidOpenCandidateDetail: class {
    func openCandidateDetail(pendingUserID: String, categoryId: String, jobApplyId: String,deviceDetail: UserData)
}

enum GetJobDetailType: Int {
    case pending        = 1
    case shortListed    = 2
    case rejected       = 3
    case expired        = 4
    case none           = 5
}

class PendingJobVC: BaseVC {
    
    //MARK:- Properties
    //==================
    var page: Int = 0
    var shouldLoadMoreData = false
    var headerJobCountData = [HeaderJobCount]()
    var pendingJobDetail = [PendingJobDetail]()
    weak var delegate: PendingJobCountHeader?
    weak var statusDelegate: ShouldOpenStatusPopup?
    weak var openCandidateDetailDelegate: DidOpenCandidateDetail?
    var isOpenFromPush = false
    
    var cancelJobStatus = 0
    var senderId: String?
    
    var jobID: String? {
        didSet {
            self.page = 0
            self.getJobPendingDetail(jobId: jobID ?? "", loader: true)
        }
    }
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var pendingJobTableView: UITableView!
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

//MARK:- Private Methods
//=======================
private extension PendingJobVC {
    
    func initialSetup() {
        
        self.pendingJobTableView.delegate = self
        self.pendingJobTableView.dataSource = self
        self.pendingJobTableView.emptyDataSetSource = self
        self.pendingJobTableView.emptyDataSetDelegate = self
        
        self.registerNib()
        self.pendingJobTableView.enablePullToRefresh(tintColor: AppColors.themeBlueColor ,target: self, selector: #selector(refreshWhenPull(_:)))
    }
    
    func registerNib() {
        
        let nib = UINib(nibName: "PendingJobRequestCell", bundle: nil)
        self.pendingJobTableView.register(nib, forCellReuseIdentifier: "PendingJobRequestCell")
        
        self.pendingJobTableView.register(UINib(nibName: "LoadMoreTableViewCell", bundle: nil), forCellReuseIdentifier: "LoadMoreTableViewCell")
    }
    
    func isLoadingIndexPath(_ indexPath: IndexPath) -> Bool {
        guard shouldLoadMoreData else { return false}
        return indexPath.row == self.pendingJobDetail.count
    }
    
    func getPendingHeaderData() {
        self.delegate?.getPendingJobCount(headerdata: self.headerJobCountData)
    }
}

//MARK: - IB Action and Target
//===============================
extension PendingJobVC {
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func locationBtnTapped(sender: UIButton) {
        print("location btn tapped")        
    }
    
    @objc func refreshWhenPull(_ sender: UIRefreshControl) {
        sender.endRefreshing()
        self.page = 0
        self.shouldLoadMoreData = true
        self.getJobPendingDetail(jobId: self.jobID ?? "", loader: false)
    }
    
    @objc func onTappedStatusButton(sender: UIButton) {
        if self.cancelJobStatus == 3 {
            return
        }
        guard let index = sender.tableViewIndexPath(self.pendingJobTableView) else { return }
        
        self.statusDelegate?.presentShortlistedPopup(jobApplyId: self.pendingJobDetail[index.row].jobAppliedId, categoryId: self.pendingJobDetail[index.row].jobData.categoryId)
        
    }
    
    //    TODO:- OPen recruiter chat message screen
    @objc private func messageButtonTapped(_ sender: UIButton){
        
        guard let row = sender.tableViewIndexPath(self.pendingJobTableView)?.row else { return  }
        
        let userId = self.pendingJobDetail[row].userData.userID
        
        ChatHelper.getUserDetails(userId: userId, completion: { [weak self](member) in
            
            guard let _self = self else { return }
            
            if let member = member{
                let chatVc = PersonalChatVC.instantiate(fromAppStoryboard: .Candidate)
                chatVc.chatMember = member
                chatVc.fromPendingJobVC = true
                let rootViewController = UIApplication.shared.keyWindow?.rootViewController
                _self.navigationController?.pushViewController(chatVc, animated: false)
            }else{
                _self.showAlert(msg: StringConstants.Candidate_not_available)
            }
        })
    }
}

//MARK:- Web Services
//====================
extension PendingJobVC {
    
    func getJobPendingDetail(jobId: String, loader: Bool) {
        
        let param = ["page": self.page , "jobId": jobId , "type": GetJobDetailType.pending.rawValue] as [String: Any]
        print(param)
        WebServices.getJobPendingDetail(parameters: param, loader: loader, success: { (json) in
            var data = [PendingJobDetail]()
            
            if let code = json["code"].int, code == error_codes.success {
                let details = json["data"]["userList"].arrayValue
                
                if self.page == 0 {
                    self.pendingJobDetail.removeAll(keepingCapacity: false)
                }
                
                for detail in details {
                    data.append(PendingJobDetail(dict: detail))
                }
                if self.shouldLoadMoreData {
                    self.pendingJobDetail.append(contentsOf: data)
                } else {
                    self.pendingJobDetail = data
                }
                let headerData = json["data"]["header"].arrayValue
                   self.headerJobCountData.removeAll(keepingCapacity: false)
                headerData.forEach({ (data) in
                    self.headerJobCountData.append(HeaderJobCount(dict: data))
                })
                self.page = json["data"]["page"].intValue
                self.shouldLoadMoreData = json["data"]["next"].boolValue

                self.getPendingHeaderData()
                self.pendingJobTableView.reloadData()
                var count = 0
                _ = self.pendingJobDetail.map{ value in
                    
                    if value.userData.userID == self.senderId ?? ""{
                        
                        let indexPath = IndexPath(row: count, section: 0)
                        
                        self.pendingJobTableView.scrollToRow(at: indexPath, at: .top, animated: true)
                    }
                    
                    count += 1
                    
                }
                
                
            } else {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
            
        }) { (error: Error) in
            CommonClass.showToast(msg: error.localizedDescription )
        }
    }
    
    func notifyVideoCallApi(sender: UIButton){
        
        let userData = User(json: (AppUserDefaults.value(forKey: .userData)))
        
        guard let index = sender.tableViewIndexPath(self.pendingJobTableView) as IndexPath? else{return}
        
        var params = JSONDictionary()
        params["senderName"] = userData.first_name
        params["receiverName"] = self.pendingJobDetail[index.row].userData.firstName
        params["senderId"] = userData.user_id ?? ""
        params["receiverId"] = self.pendingJobDetail[index.row].userData.userID
        params["deviceId"] = self.pendingJobDetail[index.row].userData.device_id
        params["deviceToken"] = self.pendingJobDetail[index.row].userData.device_token
        params["deviceType"] = self.pendingJobDetail[index.row].userData.device_type
        params["senderDeviceToken"] = DeviceDetail.deviceToken
        params["type"] = "calling"
        params["senderImage"] = userData.imageUrl
        params["roomId"] = "\(userData.user_id ?? "")_\(self.pendingJobDetail[index.row].userData.userID)"
        
        WebServices.notifyVideoCall(parameters: params, loader: true, success: { (data) in
            sharedAppDelegate.moveToVideoChatScene(with: Caller(json: data["data"])!,fromPush : false, fromCallKit: false)
        }) { (error) in
            self.showAlert(msg: error.localizedDescription)
            return
        }
    }
}

//MARK: - Table view Delegate and DataSource
//============================================
extension PendingJobVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = self.pendingJobDetail.count
        return shouldLoadMoreData ? count + 1 : count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isLoadingIndexPath(indexPath){
            guard let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.defaultReuseIdentifier, for: indexPath) as? LoadMoreTableViewCell else {
                fatalError("Failed to initialize LoadMoreTableViewCell")
            }
            
            return cell
        }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PendingJobRequestCell") as? PendingJobRequestCell else { fatalError("invalid cell \(self)")
        }
        
        
        cell.categoryId = self.pendingJobDetail[indexPath.row].jobData.categoryId
        
        cell.populateData(data: self.pendingJobDetail[indexPath.row])
        if self.isOpenFromPush, self.pendingJobDetail[indexPath.row].userData.userID == self.senderId ?? "" {
            self.isOpenFromPush = false
            cell.containerView.backgroundColor  = AppColors.gray152.withAlphaComponent(0.5)
            UIView.animate(withDuration: 1.5, animations: {
                cell.containerView.backgroundColor  = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            }) { (true) in
                cell.containerView.backgroundColor  = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            }
            
        }else{
            cell.containerView.backgroundColor  = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        
        cell.statusLabel.text = StringConstants.Action
        
        cell.statusButton.setImage(#imageLiteral(resourceName: "icChatSend"), for: .normal)
        cell.statusButton.addTarget(self, action: #selector(self.onTappedStatusButton(sender:)), for: .touchUpInside)
        cell.expiredImageView.isHidden = true
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        if self.cancelJobStatus == 3 {
            return
        }
        self.openCandidateDetailDelegate?.openCandidateDetail(pendingUserID: self.pendingJobDetail[indexPath.row].userData.userID, categoryId: self.pendingJobDetail[indexPath.row].jobData.categoryId, jobApplyId: self.pendingJobDetail[indexPath.row].jobAppliedId, deviceDetail: self.pendingJobDetail[indexPath.row].userData)
        
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        print(indexPath.row+1)
        guard self.shouldLoadMoreData else { return }
        
        if indexPath.row == self.pendingJobDetail.count - 1 && self.shouldLoadMoreData {  //for 2nd last cell
            self.getJobPendingDetail(jobId: self.jobID  ?? "", loader: false)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    
    @objc func videoCallButtonTapped(_ sender: UIButton){
        notifyVideoCallApi(sender: sender)
    }
}

//MARK:- DZNEmptyDataSource and Delegate
//==========================================
extension PendingJobVC: DZNEmptyDataSetDelegate, DZNEmptyDataSetSource {
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return #imageLiteral(resourceName: "icHomeNoJobs")
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: StringConstants.No_Pending_Request,attributes: [NSAttributedStringKey.foregroundColor: AppColors.black46,NSAttributedStringKey.font: AppFonts.Poppins_Bold.withSize(14)])
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: "", attributes: [NSAttributedStringKey.foregroundColor: AppColors.gray152, NSAttributedStringKey.font: AppFonts.Poppins_Regular.withSize(12)])
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}

