//
//  ProfilePictureStepVC.swift
//  JobGet
//
//  Created by Abhi on 22/05/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import SwiftyJSON
import UIKit
import GoogleMaps
import Firebase

class ProfilePictureStepVC: UIViewController {
    
    //MARK:- Properties
    //==================
    private  var imageToBeUploaded : UIImage?
    var isImageUploading = false
    private  var imageUrl = ""
    var delegate : ShouldOpenStatusPopup?
    var job_id = ""
    var categoryId = ""
    var recruiterId = ""
    var address = ""
    //
    //MARK:- IBoutlets
    //================
    
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var addImageLabel: UILabel!
    
    
    var candidateDetail : CandidateProfileModel = CandidateProfileModel(withJSON: JSON([:]))
    weak var educationDelegate: EducationSaveText?
    
    
    private var currentLocation: CLLocation? {
        
        didSet {
            
            print_debug(currentLocation)
            
            DispatchQueue.mainQueueAsync{
                self.currentLocation?.convertToPlaceMark({ (place) in
                    
                    print_debug(place)
                    
                    if let city = place.locality, let state = place.administrativeArea{
                        
                        self.candidateDetail.state = state
                        self.candidateDetail.city = city
                        self.address = "\(city), \(state)"
                    }
                    var location = ""
                    if let addressDictionary = place.addressDictionary, let formattedAddressLines = addressDictionary["FormattedAddressLines"] as? [String]{
                        
                        location = formattedAddressLines.joined(separator: ", ")
                        
                    }
                    self.candidateDetail.address = location
                })
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        if let image = self.candidateDetail.imageToUpload {
            
            self.profileImageView.image = image
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func skipButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
    @IBAction func saveButtonTapped(_ sender: UIButton) {
//        if CLLocationManager.authorizationStatus() == .denied {
//            CommonClass.delayWithSeconds(1.0, completion: {
//                let popUpVc = AlertPopUpVC.instantiate(fromAppStoryboard: .Candidate)
//                popUpVc.delegate = self
//                popUpVc.modalPresentationStyle = .overCurrentContext
//                popUpVc.modalTransitionStyle = .crossDissolve
//                self.present(popUpVc, animated: true, completion: nil)
//            })
//        }else{
            let updateProfileLocationPopupScene = UpdateProfileLocationPopupVC.instantiate(fromAppStoryboard: .settingsAndChat)
            updateProfileLocationPopupScene.delegate = self
            updateProfileLocationPopupScene.address = self.address
            updateProfileLocationPopupScene.currentLocation = CLLocation(latitude: AppUserDefaults.value(forKey: .userLat).doubleValue, longitude: AppUserDefaults.value(forKey: .userLong).doubleValue)
            sharedAppDelegate.window?.addSubview(updateProfileLocationPopupScene.view)
            self.addChildViewController(updateProfileLocationPopupScene)
//        }
    }
    
    @IBAction func pickImageButtonTapped(_ sender: UIButton) {
        self.captureImage(on: self)
    }
    
}

//MARK: PRIVATE FUNCTIONS
extension ProfilePictureStepVC{
    
    func initialSetup(){
        
        saveButton.layer.cornerRadius = 4
        profileImageView.layer.cornerRadius = 4
        saveButton.backgroundColor = #colorLiteral(red: 0.2389856699, green: 0.4166136601, blue: 0.6607115269, alpha: 1)
        saveButton.setTitle(StringConstants.Save, for: .normal)
        self.profileImageView.contentMode = .scaleAspectFill
        self.profileImageView.clipsToBounds = true
        let attributedAddImageString: NSMutableAttributedString = NSMutableAttributedString(string: StringConstants.AddImage )
        attributedAddImageString.setColorForText(textForAttribute: StringConstants.Optional, withColor: AppColors.gray152, font: AppFonts.Poppins_Light.withSize(15))
        self.addImageLabel.attributedText = attributedAddImageString
        if !AppUserDefaults.getData(forKey: .facebookImage).isEmpty{
            self.profileImageView.sd_setImage(with: URL(string: AppUserDefaults.getData(forKey: .facebookImage)), placeholderImage: #imageLiteral(resourceName: "icHomeCategoryPlaceholder1"),  completed: nil)
            self.imageUrl = AppUserDefaults.getData(forKey: .facebookImage)
            self.candidateDetail.userImage = AppUserDefaults.getData(forKey: .facebookImage)
            
            self.educationDelegate?.didSaveText(text: "", image: self.imageToBeUploaded, imageURL: imageUrl, mode: .addImage, aboutTextViewHeight: self.candidateDetail.aboutMeTextViewheight, educationTextViewHeight: self.candidateDetail.educationTextViewHeight)
        }
        self.getLocation()
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateLocation(noti:)), name: NSNotification.Name(rawValue: "UPDATE_LOCATION"), object: nil)
    }
    
    @objc func updateLocation(noti: Notification){
        
        if let data = noti.userInfo as? [String:Any]{
            
            self.candidateDetail.city = data["city"] as? String ?? "Boston"
            self.candidateDetail.state = data["state"] as? String ?? "MA"
            self.candidateDetail.address = data["address"] as? String ?? "Boston, MA"
            self.candidateDetail.latitude = data["latitude"] as? String ?? "42.361145"
            self.candidateDetail.longitude = data["longitude"] as? String ?? "-71.057083"
            self.postProfile()
        }
    }
    
    
    func getLocation() {
        
        DispatchQueue.main.async {
            
//            if CLLocationManager.authorizationStatus() == .denied {
//
//                CommonClass.delayWithSeconds(1.0, completion: {
//                    let popUpVc = AlertPopUpVC.instantiate(fromAppStoryboard: .Candidate)
//                    popUpVc.delegate = self
//                    popUpVc.modalPresentationStyle = .overCurrentContext
//                    popUpVc.modalTransitionStyle = .crossDissolve
//                    self.present(popUpVc, animated: true, completion: nil)
//                })
//            } else {
//                SharedLocationManager.fetchCurrentLocation { (location) in
//                    SharedLocationManager.locationManager.stopUpdatingLocation()
                    self.currentLocation = CLLocation(latitude: AppUserDefaults.value(forKey: .userLat).doubleValue, longitude: AppUserDefaults.value(forKey: .userLong).doubleValue)
                    self.candidateDetail.latitude = "\(AppUserDefaults.value(forKey: .userLat).doubleValue)"
                    self.candidateDetail.longitude = "\(AppUserDefaults.value(forKey: .userLong).doubleValue)"
//                }
//            }
        }
    }
    
    private func uploadImage(image : UIImage) {
        
//        loader.start()
        AppNetworking.showUploadImageLoader()
        image.uploadImageToS3( success: { (uploaded : Bool, imageUrl : String) in
            if uploaded {
                
                self.imageUrl = imageUrl
                self.candidateDetail.userImage = imageUrl
                let data = NSKeyedArchiver.archivedData(withRootObject: "")
                AppUserDefaults.save(value: data, forKey: .facebookImage)
//                AppUserDefaults.save(value: "", forKey: .facebookImage)
                self.educationDelegate?.didSaveText(text: "", image: self.imageToBeUploaded, imageURL: imageUrl, mode: .addImage, aboutTextViewHeight: self.candidateDetail.aboutMeTextViewheight, educationTextViewHeight: self.candidateDetail.educationTextViewHeight)
               AppNetworking.hideLoader()
                
            }
        }, progress: { (uploadPercentage : CGFloat) in
            print_debug(uploadPercentage)
            
        }, failure: { (err : Error) in
            
            if (err.localizedDescription  == StringConstants.Internet_connection_appears_offline) {
                CommonClass.showToast(msg: StringConstants.Please_check_your_internet_connection)
            }
            self.candidateDetail.userImage = ""
            
            AppNetworking.hideLoader()
        })
    }
    
    func updateFirebaseDetail(dict: JSON) {
        print_debug(dict["_id"].stringValue)
        var userDetails: [String: Any] = [:]
        if let fcmTocken = Messaging.messaging().fcmToken{
            userDetails[ChatEnum.User.deviceToken.rawValue] = fcmTocken
        }
        userDetails[ChatEnum.User.email.rawValue] =   dict["email"].stringValue
        
        userDetails[ChatEnum.User.firstName.rawValue] = dict["first_name"].stringValue
        userDetails[ChatEnum.User.lastName.rawValue] =  dict["last_name"].stringValue
        userDetails[ChatEnum.User.mobileNumber.rawValue] = dict["mobile"].stringValue
        userDetails[ChatEnum.User.userImage.rawValue] = dict["user_image"].stringValue
        userDetails[ChatEnum.User.userId.rawValue] = dict["_id"].stringValue
        userDetails[ChatEnum.User.isOnline.rawValue] = true
        userDetails[ChatEnum.User.deviceType.rawValue] = Device_Type.iOS.rawValue
        
        print_debug(dict["email"].stringValue)
        print_debug(dict["first_name"].stringValue)
        print_debug(dict["last_name"].stringValue)
        print_debug(dict["mobile"].stringValue)
        print_debug(dict["user_image"].stringValue)
        print_debug(dict["_id"].stringValue)
        print_debug(userDetails)
        ChatHelper.updateUserDetails(userId: dict["_id"].stringValue, details: userDetails)
    }
}

//MARK:- Location alert popup Delegate
//===========================================
extension ProfilePictureStepVC: AlertPopUpDelegate {
    func didTapNegativeButton() {
        
    }
    
    func didTapAffirmativeButton() {
        //        TODO: Add settings
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: { (success) in
                
            })
        } else {
            // Fallback on earlier versions
        }
    }
    
}


//MARK:- ImagePickerController & NavigationController Delegate
//============================================================
extension ProfilePictureStepVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let imagePicked = info[UIImagePickerControllerOriginalImage] as? UIImage{
            
            
            let frameRect = CGRect(x: 50, y: self.view.center.y, width: self.view.frame.width - 100, height: 180)
            
            Cropper.shared.openCropper(withImage: imagePicked, mode: .custom(frameRect), on: self)
            
            
        }
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        
    }
}

//MARK: Extension AppInventivCropperDelegate
//MARK:
extension ProfilePictureStepVC: CompleteProfileWithLocationDelegate {
    
    func completeProfileWithLocationDelegate() {
        postProfile()
    }
}

//MARK: Extension AppInventivCropperDelegate
//MARK:
extension ProfilePictureStepVC: CropperDelegate {
    
    func imageCropperDidCancelCrop() {
        print_debug("Crop cancelled")
    }
    
    func imageCropper(didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect) {
        
        self.profileImageView.contentMode = .scaleAspectFill
        self.profileImageView.clipsToBounds = true
        self.profileImageView.image = croppedImage
        self.imageToBeUploaded = croppedImage
        self.educationDelegate?.didSaveText(text: "",image: self.imageToBeUploaded, imageURL: nil, mode: .addImage, aboutTextViewHeight: self.candidateDetail.aboutMeTextViewheight, educationTextViewHeight: self.candidateDetail.educationTextViewHeight)
        self.uploadImage(image: croppedImage)
    }
}


//Mark: Webservices
//=====================
extension ProfilePictureStepVC {
    
    func postProfile(){
        
        print_debug(self.candidateDetail.dictionary())
        
        WebServices.addProfile(parameters:self.candidateDetail.dictionary(), loader: true, success: { (dict) in
            self.updateFirebaseDetail(dict: dict["data"])
            AppNetworking.hideLoader()
            if self.childViewControllers.count > 0{
                let viewControllers:[UIViewController] = self.childViewControllers
                for viewContoller in viewControllers{
                    viewContoller.willMove(toParentViewController: nil)
                    viewContoller.view.removeFromSuperview()
                    viewContoller.removeFromParentViewController()
                }
            }
            if self.job_id.isEmpty {
                
                let obj = RecruiterCandidateDetailVC.instantiate(fromAppStoryboard: .Recruiter)
                obj.fromProfile = true
                obj.userId = dict["data"].dictionaryValue["_id"]?.stringValue ?? ""
                if let vc = self.navigationController?.viewControllers {
                    for controllers in vc{
                        if controllers is CandidateTabBarVC{
                            (controllers as! CandidateTabBarVC).viewControllers![3] = obj
                            (controllers as! CandidateTabBarVC).tabBar.items?.last?.title = "Profile"
                            (controllers as! CandidateTabBarVC).tabBar.items?.last?.image = #imageLiteral(resourceName: "icHomeDeselectUser")
                            (controllers as! CandidateTabBarVC).tabBar.items?.last?.selectedImage = #imageLiteral(resourceName: "icHomeSelectUser")
                            self.navigationController?.popToViewController(controllers, animated: true)
                        }
                    }
                }                
            }else {
                
                let obj = RecruiterCandidateDetailVC.instantiate(fromAppStoryboard: .Recruiter)
                obj.fromProfile = true
                obj.userId = dict["data"].dictionaryValue["_id"]?.stringValue ?? ""
                if let vc = self.navigationController?.viewControllers {
                    
                    for controllers in vc {
                        if controllers is CandidateTabBarVC {
                            (controllers as! CandidateTabBarVC).viewControllers![3] = obj
                            (controllers as! CandidateTabBarVC).tabBar.items?.last?.title = "Profile"
                            (controllers as! CandidateTabBarVC).tabBar.items?.last?.image = #imageLiteral(resourceName: "icHomeDeselectUser")
                            (controllers as! CandidateTabBarVC).tabBar.items?.last?.selectedImage = #imageLiteral(resourceName: "icHomeSelectUser")
                        }
                    }
                }
                
                // check if it comes from CandidateJobDetailVC
                if let vc = self.navigationController?.viewControllers {
                    
                    for controllers in vc {
                        if controllers is CandidateJobDetailVC {
                            
                            if let controller = controllers as? CandidateJobDetailVC {
                                controller.jobId = self.job_id
                                controller.fromProfile = true
                                controller.recruiterId = self.recruiterId
                                controller.categoryId = self.categoryId
                                self.navigationController?.popToViewController(controllers, animated: true)
                            }
                        }
                    }
                }
                if let vc = self.navigationController?.viewControllers {
                    print_debug(vc)
                    for controllers in vc {
                        
                        if controllers is CandidateTabBarVC {
                            // check if it comes from JobsListVC
                            if (controllers as! CandidateTabBarVC).selectedIndex == 0 {
                                if let viewCOntroller = (controllers as! CandidateTabBarVC).viewControllers![0] as? JobsListVC {
                                    
                                    viewCOntroller.job_id = self.job_id
                                    print_debug(self.recruiterId)
                                    viewCOntroller.recruiterId = self.recruiterId
                                    viewCOntroller.categoryId = self.categoryId
                                    viewCOntroller.fromProfile = true
                                    
                                    self.navigationController?.popToViewController(controllers, animated: true)
                                }
                            } else {
                                if let viewCOntroller = (controllers as! CandidateTabBarVC).viewControllers![2] as? MyJobsVC{
                                    viewCOntroller.savedJobVC.job_id = self.job_id
                                    viewCOntroller.savedJobVC.recruiterId = self.recruiterId
                                    viewCOntroller.savedJobVC.categoryId = self.categoryId
                                    viewCOntroller.savedJobVC.fromProfile = true
                                    self.navigationController?.popToViewController(controllers, animated: true)
                                }
                            }
                        }
                    }
                }
            }
        }) { (error) in
            self.showAlert(msg: error.localizedDescription)
        }
    }
}

