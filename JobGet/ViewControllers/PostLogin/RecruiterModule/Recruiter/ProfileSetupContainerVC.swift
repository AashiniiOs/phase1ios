//
//  ProfileSetupContainerVC.swift
//  JobGet
//
//  Created by Appinventiv on 01/06/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import SwiftyJSON

enum ProfileVCType{
    
    case experience
    case educationType
    case about
    case profilePic
    case none
}
protocol SaveData: class {
    func saveCandidateData(data: CandidateProfileModel)
}

class ProfileSetupContainerVC: UIViewController {
    
    // MARK: - Outlets
    // MARK: -
    
    @IBOutlet weak var navigationTitle: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var addButton: UIButton!
    @IBOutlet weak var tickImage1: UIImageView!
    @IBOutlet weak var tickImage2: UIImageView!
    @IBOutlet weak var tickImage3: UIImageView!
    @IBOutlet weak var tickImage4: UIImageView!
    @IBOutlet weak var label1: UILabel!
    @IBOutlet weak var label2: UILabel!
    @IBOutlet weak var label3: UILabel!
    @IBOutlet weak var label4: UILabel!
    @IBOutlet weak var experienceLabel: UILabel!
    @IBOutlet weak var educationLabel: UILabel!
    @IBOutlet weak var aboutLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var profilePicLabel: UILabel!
    
    @IBOutlet weak var progressView: UIProgressView!
    
    // MARK: - Properties
    // MARK: -
    var viewControllerCount = 1
    weak var delegate : SaveData?
    var job_id: String?
    var recruiterId: String?
    var categoryId: String?
    var fromTabbar = true
    var experienceScene : ExperienceProfileVC!
    var educationStep : EducationStepVC!
    var profileVCType = ProfileVCType.none
    var profilePictureStepVC:ProfilePictureStepVC!
    var imageToBeUploaded: UIImage?
    var imageUrl: String?
    var candidateDetail : CandidateProfileModel = CandidateProfileModel(withJSON: JSON([:]))
    
    // MARK: - View life cycle.
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Action
    // MARK: -
    
    @IBAction func addButtonTapp(_ sender: UIButton) {
        
        switch self.profileVCType  {
        case ProfileVCType.experience:
            break
        case ProfileVCType.educationType:
            self.educationStep.selectedMode = .education
            self.educationStep.skipButtonTapped()
        case ProfileVCType.about:
            self.educationStep.selectedMode = .summary
            self.educationStep.skipButtonTapped()
        default:
            break
        }
    }
    
    @IBAction func backButtonTapp(_ sender: UIButton) {
        if  self.viewControllerCount > 1{
            self.viewControllerCount -= 1
        }
        
        if self.profileVCType == .experience {
            self.delegate?.saveCandidateData(data: self.experienceScene.candidateDetail)
            self.navigationController?.popViewController(animated: true)
        }
        
        
        self.setProgressBar()
        switch self.viewControllerCount  {
        case 1:
            self.profileVCType = .experience
            
            self.containerView.bringSubview(toFront: self.experienceScene.view)
            self.addButton.isHidden = true
            self.addButton.setImage(nil, for: .normal)
            self.addButton.setTitle("", for: .normal)
            
        case 2, 3:
            
            self.containerView.bringSubview(toFront: self.educationStep.view)
            if self.viewControllerCount == 2{
                self.educationStep.selectedMode = .education
                self.profileVCType = .educationType
            }else{
                self.educationStep.selectedMode = .summary
                self.profileVCType = .about
            }
            
            self.educationStep.setUpViewAccordingToMode()
            
            self.addButton.setImage(nil, for: .normal)
            self.addButton.setTitle(StringConstants.SKIP, for: .normal)
            
        default:
            break
        }
    }
}

// MARK: - Private methods.
// MARK: -
extension ProfileSetupContainerVC{
    
    private func initialSetup(){
        
        self.label1.roundCorners()
        self.label2.roundCorners()
        self.label3.roundCorners()
        self.label4.roundCorners()
        self.tickImage1.roundCorners()
        self.tickImage2.roundCorners()
        self.tickImage3.roundCorners()
        self.tickImage4.roundCorners()
        self.addButton.isHidden = true
        self.addButton.setImage(nil, for: .normal)
        experienceScene = ExperienceProfileVC.instantiate(fromAppStoryboard: .Recruiter)
        self.profileVCType = ProfileVCType.experience
        if let jobid =  self.job_id{
            experienceScene.job_id = jobid
        }
        if let recruiter_Id =  self.recruiterId{
            experienceScene.recruiterId = recruiter_Id
        }
        if let category_Id =  self.categoryId{
            experienceScene.categoryId = category_Id
        }
        
        experienceScene.candidateDetail = self.candidateDetail
        experienceScene.cardsCount = self.candidateDetail.experience.count
        experienceScene.fromTabbar = false
        experienceScene.delegate = self
        experienceScene.view.frame = self.containerView.bounds
        self.containerView.addSubview(experienceScene.view)
        self.addChildViewController(experienceScene)
        self.view.layoutIfNeeded()
    }
    
    func setProgressBar(){
        
        switch viewControllerCount {
        case 1:
            self.addButton.isHidden = false
            self.navigationTitle.text = StringConstants.Experienc.uppercased()
            self.progressView.animate(duration: 0.2, progress: 0.0)
            self.label1.textColor = AppColors.white
            self.label2.textColor = AppColors.darkGray
            self.label3.textColor = AppColors.darkGray
            self.label4.textColor = AppColors.darkGray
            self.label1.backgroundColor = AppColors.themeBlueColor
            self.label2.backgroundColor = AppColors.statusBarGray210
            self.label3.backgroundColor = AppColors.statusBarGray210
            self.label4.backgroundColor = AppColors.statusBarGray210
            self.label1.isHidden = false
            self.label2.isHidden = false
            self.label3.isHidden = false
            self.label4.isHidden = false
            self.experienceLabel.textColor = AppColors.themeBlueColor
            self.educationLabel.textColor = AppColors.gray163
            self.aboutLabel.textColor = AppColors.gray163
            
            self.profilePicLabel.textColor = AppColors.gray163
            
        case 2:
            self.addButton.isHidden = false
            self.progressView.animate(duration: 0.2, progress: 0.3)
            self.navigationTitle.text = StringConstants.Education.uppercased()
            self.label1.textColor = AppColors.white
            self.label2.textColor = AppColors.white
            self.label3.textColor = AppColors.darkGray
            self.label4.textColor = AppColors.darkGray
            self.label1.backgroundColor = AppColors.themeBlueColor
            self.label2.backgroundColor = AppColors.themeBlueColor
            self.label3.backgroundColor = AppColors.statusBarGray210
            self.label4.backgroundColor = AppColors.statusBarGray210
            self.label1.isHidden = true
            self.label2.isHidden = false
            self.label3.isHidden = false
            self.label4.isHidden = false
            self.experienceLabel.textColor = AppColors.gray163
            self.educationLabel.textColor = AppColors.themeBlueColor
            self.aboutLabel.textColor = AppColors.gray163
            
            self.profilePicLabel.textColor = AppColors.gray163
            
        case 3:
            self.addButton.isHidden = false
            self.navigationTitle.text = StringConstants.About_Me.uppercased()
            self.progressView.animate(duration: 0.2, progress: 0.65)
            self.label1.textColor = AppColors.white
            self.label2.textColor = AppColors.white
            self.label3.textColor = AppColors.white
            self.label4.textColor = AppColors.darkGray
            self.label1.backgroundColor = AppColors.themeBlueColor
            self.label2.backgroundColor = AppColors.themeBlueColor
            self.label3.backgroundColor = AppColors.themeBlueColor
            self.label4.backgroundColor = AppColors.statusBarGray210
            self.label1.isHidden = true
            self.label2.isHidden = true
            self.label3.isHidden = false
            self.label4.isHidden = false
            self.experienceLabel.textColor = AppColors.gray163
            self.educationLabel.textColor = AppColors.gray163
            self.aboutLabel.textColor = AppColors.themeBlueColor
            
            self.profilePicLabel.textColor = AppColors.gray163
            
        case 4:
            self.addButton.isHidden = true
            self.navigationTitle.text = StringConstants.PROFILE_PICTURE.uppercased()
            self.experienceLabel.textColor = AppColors.gray163
            self.educationLabel.textColor = AppColors.gray163
            self.aboutLabel.textColor = AppColors.gray163
            
            self.profilePicLabel.textColor = AppColors.themeBlueColor
            self.progressView.animate(duration: 0.2, progress: 1.0)
            self.label1.textColor = AppColors.white
            self.label2.textColor = AppColors.white
            self.label3.textColor = AppColors.white
            self.label4.textColor = AppColors.white
            self.label1.backgroundColor = AppColors.themeBlueColor
            self.label2.backgroundColor = AppColors.themeBlueColor
            self.label3.backgroundColor = AppColors.themeBlueColor
            self.label4.backgroundColor = AppColors.themeBlueColor
            self.label1.isHidden = true
            self.label2.isHidden = true
            self.label3.isHidden = true
            self.label4.isHidden = false
        default: break
        }
    }
    
    
}

extension ProfileSetupContainerVC: AddProfileChildVCDelegate{
    func addProfileChildVC(viewController: UIViewController) {
        
        if  self.viewControllerCount < 5{
            self.viewControllerCount += 1
        }
        self.setProgressBar()
        
        switch self.viewControllerCount {
        case 2,3:
            self.addButton.isHidden = false
            self.addButton.setImage(nil, for: .normal)
            self.addButton.setTitle(StringConstants.SKIP, for: .normal)
            if let vc = viewController as? EducationStepVC {
                self.educationStep = vc
                if self.viewControllerCount == 3 {
                    
                    vc.selectedMode = .summary
                    self.profileVCType = .about
                }else{
                    self.profileVCType = .educationType
                    vc.selectedMode = .education
                }
                vc.delegate = self
                vc.educationDelegate = self
                
                vc.aboutText = self.candidateDetail.about
                vc.education = self.candidateDetail.education
            }
            
            viewController.view.frame = self.containerView.bounds
            self.containerView.addSubview(viewController.view)
            self.addChildViewController(viewController)
            self.view.layoutIfNeeded()
            
        case 4:
            self.addButton.setImage(nil, for: .normal)
            self.addButton.setTitle(StringConstants.SKIP, for: .normal)
            if let vc = viewController as? ProfilePictureStepVC{
                self.profilePictureStepVC = vc
                // if let image = imageToBeUploaded {
                //   vc.imageToBeUploaded = image
                vc.candidateDetail = self.experienceScene.candidateDetail
                // }
                //vc.imageUrl = self.imageUrl
                self.profilePictureStepVC.getLocation()
                vc.educationDelegate = self
            }
            
            self.addButton.isHidden = true
            self.profileVCType = .profilePic
            viewController.view.frame = self.containerView.bounds
            self.containerView.addSubview(viewController.view)
            self.addChildViewController(viewController)
            self.containerView.bringSubview(toFront: viewController.view)
            self.view.layoutIfNeeded()
        default:
            break
        }
    }
}

extension ProfileSetupContainerVC : EducationSaveText {
    
    
    func didSaveText(text: String, image: UIImage?, imageURL: String?, mode: StepMode, aboutTextViewHeight: CGFloat, educationTextViewHeight: CGFloat) {
        switch mode {
            
        case .education:
            self.candidateDetail.education = text
            self.experienceScene.candidateDetail.education = text
            self.experienceScene.candidateDetail.educationTextViewHeight = educationTextViewHeight
        case .summary:
            self.candidateDetail.about = text
            self.experienceScene.candidateDetail.about = text
            self.experienceScene.candidateDetail.aboutMeTextViewheight = aboutTextViewHeight
        case .addImage:
            if let url = imageURL {
                self.imageUrl = url
                self.experienceScene.candidateDetail.userImage = url
            }
            
            self.experienceScene.candidateDetail.imageToUpload = image
            self.imageToBeUploaded = image
            
        }
    }
}
