//
//  RecruiterCandidateCells.swift
//  JobGet
//
//  Created by Appinventiv on 22/05/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import CircularSlider

class RecruiterCandidateInfoCell: UITableViewCell{
    
    @IBOutlet weak var visitorView: UIView!
    
    @IBOutlet weak var visitorButton: UIButton!
    
    @IBOutlet weak var employeeTypeContainerViewTopConstaint: NSLayoutConstraint!
    @IBOutlet weak var employeeTypeContainerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var employeeTypeContainerView: UIView!
    
    @IBOutlet weak var chatImage: UIImageView!
    
    @IBOutlet weak var messageButton: UIButton!
    @IBOutlet var bgView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var employeeTypeLabel: UILabel!
    @IBOutlet weak var videoCallBtn: UIButton!
    
    
    
    @IBOutlet weak var imageOne: UIImageView!
    @IBOutlet weak var imageTwo: UIImageView!
    @IBOutlet weak var imageThree: UIImageView!
    @IBOutlet weak var imageFour: UIImageView!
    @IBOutlet weak var imageFive: UIImageView!
    
    @IBOutlet weak var gradientOne: GradientView!
    @IBOutlet weak var gradienTwo: GradientView!
    @IBOutlet weak var gradientThree: GradientView!
    @IBOutlet weak var gradientFour: GradientView!
    @IBOutlet weak var gradientFive: GradientView!
    
    @IBOutlet weak var labelOne: UILabel!
    @IBOutlet weak var labelTwo: UILabel!
    @IBOutlet weak var labelThree: UILabel!
    @IBOutlet weak var labelFour: UILabel!
    @IBOutlet weak var labelFive: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.visitorView.isHidden = true
        self.employeeTypeContainerView.isHidden = true
        self.employeeTypeContainerView.backgroundColor = AppColors.gray15
        self.employeeTypeLabel.textColor = AppColors.black46
        self.employeeTypeContainerView.setCorner(cornerRadius: self.employeeTypeContainerView.height/2, clip: true)
        self.messageButton.isHidden = true
        self.chatImage.isHidden = true
        self.videoCallBtn.isHidden = true
        self.setupIntrestedView()

    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.messageButton.roundCorners()
        self.videoCallBtn.roundCorners()

    }
    
    func setupIntrestedView() {
        self.imageOne.roundCorners()
        self.imageTwo.roundCorners()
        self.imageThree.roundCorners()
        self.imageFour.roundCorners()
        self.imageFive.roundCorners()
        self.gradientOne.roundCorners()
        self.gradienTwo.roundCorners()
        self.gradientThree.roundCorners()
        self.gradientFour.roundCorners()
        self.gradientFive.roundCorners()
        
        
    }
    
    func setIntrestedImageView(applicants: [String]) {
        self.visitorView.isHidden = false
        print_debug(applicants)
        switch applicants.count {
        case 0:
            self.showImage(with: 0, applicantsImages: applicants)
        case 1:
            self.showImage(with: 1, applicantsImages: applicants)
        case 2:
            self.showImage(with: 2, applicantsImages: applicants)
        case 3:
            self.showImage(with: 3, applicantsImages: applicants)
        case 4:
            self.showImage(with: 4, applicantsImages: applicants)
        case 5:
            self.showImage(with: 5, applicantsImages: applicants)
        default:
            self.showImage(with: 6, applicantsImages: applicants)
        }
    }
    
    func showImage(with count: Int, applicantsImages: [String]) {
        
        print_debug(applicantsImages)
        switch count {
        case 0:
            self.imageOne.isHidden = true
            self.imageTwo.isHidden = true
            self.imageThree.isHidden = true
            self.imageFour.isHidden = true
            self.imageFive.isHidden = true
            self.gradientOne.isHidden = true
            self.gradienTwo.isHidden = true
            self.gradientThree.isHidden = true
            self.gradientFour.isHidden = true
            self.gradientFive.isHidden = true
            
        case 1:
            
            self.imageFive.sd_setImage(with: URL(string: applicantsImages[0]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.labelFive.text = String(applicantsImages.count)
            
            self.imageOne.isHidden = true
            self.imageTwo.isHidden = true
            self.imageThree.isHidden = true
            self.imageFour.isHidden = true
            
            self.gradienTwo.isHidden = true
            self.gradientOne.isHidden = true
            self.gradientThree.isHidden = true
            self.gradientFour.isHidden = true
            
        case 2:
            
            self.imageFive.sd_setImage(with: URL(string: applicantsImages[0]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageFour.sd_setImage(with: URL(string: applicantsImages[1]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.labelFive.text = String(applicantsImages.count)
            
            self.gradientOne.isHidden = true
            self.gradienTwo.isHidden = true
            self.gradientThree.isHidden = true
            self.gradientFour.isHidden = true
            self.imageThree.isHidden = true
            self.imageTwo.isHidden = true
            self.imageOne.isHidden = true
            
        case 3:
            print_debug(applicantsImages)
            
            self.gradientOne.isHidden = true
            self.gradienTwo.isHidden = true
            
            self.imageOne.isHidden = true
            self.imageTwo.isHidden = true
            self.gradientFour.isHidden = true
            self.gradientThree.isHidden = true
            self.imageFive.sd_setImage(with: URL(string: applicantsImages[0]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageFour.sd_setImage(with: URL(string: applicantsImages[1]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageThree.sd_setImage(with: URL(string: applicantsImages[2]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.labelFive.text = String(applicantsImages.count)
            
        case 4:
            
            self.gradientOne.isHidden = true
            self.gradientFour.isHidden = true
            self.gradientThree.isHidden = true
            self.gradienTwo.isHidden = true
            self.imageOne.isHidden = true
            self.imageFive.sd_setImage(with: URL(string: applicantsImages[0]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageFour.sd_setImage(with: URL(string: applicantsImages[1]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageThree.sd_setImage(with: URL(string: applicantsImages[2]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageTwo.sd_setImage(with: URL(string: applicantsImages[3]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            
            self.labelFive.text = String(applicantsImages.count)
            
        case 5:
            self.gradientOne.isHidden = true
            self.gradienTwo.isHidden = true
            self.gradientThree.isHidden = true
            self.gradientFour.isHidden = true
            
            self.imageOne.sd_setImage(with: URL(string: applicantsImages[0]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageTwo.sd_setImage(with: URL(string: applicantsImages[1]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageThree.sd_setImage(with: URL(string: applicantsImages[2]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageFour.sd_setImage(with: URL(string: applicantsImages[3]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageFive.sd_setImage(with: URL(string: applicantsImages[4]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.labelFive.text = String(applicantsImages.count)
            
        default:
            self.gradientOne.isHidden = true
            self.gradienTwo.isHidden = true
            self.gradientThree.isHidden = true
            self.gradientFour.isHidden = true
            
            self.imageOne.sd_setImage(with: URL(string: applicantsImages[0]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageTwo.sd_setImage(with: URL(string: applicantsImages[1]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageThree.sd_setImage(with: URL(string: applicantsImages[2]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageFour.sd_setImage(with: URL(string: applicantsImages[3]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.imageFive.sd_setImage(with: URL(string: applicantsImages[4]),  placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), completed: nil)
            self.labelFive.text = String(applicantsImages.count)
        }
    }
    
    
    func populatedCell(index: Int, data: CandidateAppliedJobDetail){
        
        var address = data.city
        getAbbreatedState { (stateList) in
            
            _ = stateList.map{ value in
                
                if data.state.contains(s: value["state_name"] as? String ?? ""){
                    if let abState = value["state_abbreviations"] {
                        address.append(String(describing: ", \(abState)"))
                    }
                }
            }
        }
        
        if !address.contains(",") , !data.state.isEmpty {
            address.append(", ")
            address.append(data.state)
        }
        
        self.addressLabel.text = address
        self.employeeTypeLabel.isHidden = false
        self.employeeTypeContainerView.isHidden = false
        self.employeeTypeContainerViewHeight.constant = 25
        self.employeeTypeContainerViewTopConstaint.constant = 10
        self.employeeTypeContainerView.setCorner(cornerRadius: self.employeeTypeContainerView.height/2, clip: true)
        switch data.jobType {
        case 1:
            self.employeeTypeLabel.text = StringConstants.Part_Time
            
        case 2:
            self.employeeTypeLabel.text = StringConstants.Full_Time
            
        case 3:
            self.employeeTypeLabel.text = StringConstants.Part_Full_Time
            
        default:
            print_debug("default")
            self.employeeTypeContainerViewHeight.constant = 0
            self.employeeTypeContainerViewTopConstaint.constant = 0
            self.employeeTypeLabel.isHidden = true
            self.employeeTypeContainerView.isHidden = true
        }
        
    }
}

class RecruiterCandidateExperienceCell: UITableViewCell{
    
    @IBOutlet weak var cornerRadiusView: UIView!
    @IBOutlet weak var experienceContainerView: UIView!
    @IBOutlet weak var categoryExperienceLabel: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var bottomSeperatorView: UIView!
    @IBOutlet weak var experianceStackView: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.cornerRadiusView.setShadow()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.bgView.setCorner(cornerRadius: 5.0, clip: true)
        self.experienceContainerView.roundCornersFromOneSide([.topLeft, .topRight], radius: 5.0)
    }
}

class RecruiterCandidateSettingCell: UITableViewCell{
    
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var settingLabel: UILabel!
    @IBOutlet weak var bgView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.bgView.setCorner(cornerRadius: 5.0, clip: true)
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.shadowView.dropShadow(color: AppColors.black46, opacity:0.2, offSet: CGSize(width: 1.0, height: 1.0), radius: 2.0)
    }
}



