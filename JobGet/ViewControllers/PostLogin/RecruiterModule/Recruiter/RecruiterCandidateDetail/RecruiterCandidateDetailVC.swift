//
//  RecruiterCandidateDetailVC.swift
//  JobGet
//
//  Created by Appinventiv on 22/05/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import MXParallaxHeader
import SwiftyJSON

protocol UpdateCandidatePendingListDelegate: class {
    
    func updatePendingCandidateListStatus(index: Int, applyJobId: String)
}

protocol ReloadTableView : class {
    func reload()
}

class RecruiterCandidateDetailVC: BaseVC {
    
    // MARK: - Outlets.
    // MARK: -
    
    @IBOutlet weak var buttonStackView: UIStackView!
    @IBOutlet weak var stackViewTrailingConst: NSLayoutConstraint!
    @IBOutlet weak var stackViewLeadingConst: NSLayoutConstraint!
    @IBOutlet weak var navigationBGView: UIView!
    
    @IBOutlet weak var rejectButton: UIButton!
    @IBOutlet weak var tableViewTopConstraints: NSLayoutConstraint!
    @IBOutlet weak var navigationViewHeight: NSLayoutConstraint!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var sharePopupView: UIView!
    @IBOutlet weak var blurView: UIView!
    @IBOutlet weak var shortlistButton: UIButton!
    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet var headerView: UIView!
    @IBOutlet weak var navigationLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var heightOfBottomView: NSLayoutConstraint!
    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var navigationTitleLabel: UILabel!
    
    @IBOutlet weak var blackArrowBackBtn: UIButton!
    
    // MARK: - Properties.
    // MARK: -
    
    var candidateData: CandidateAppliedJobDetail?
    weak var reloadDelegate: ReloadTableView?
    var deviceDetail: UserData?
    var userId = ""
    var categoryId = ""
    var jobApplyId = ""
    var jobId = ""
    var edicationLabelHeight:CGFloat = 0
    var aboutMeLabelHeight:CGFloat = 0
    var fromProfile = true
    var fromJobDetailVC = false
    var fromShortlistVC = false
    var fromRecruiterSearchVC = false
    var fromPending  = false
    
    var experienceInHeightOrder: [JSON]?
    
    weak var delegate:UpdateCandidatePendingListDelegate?
    
    
    // MARK: - View life cycle.
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.initialSetup()
        
        if !fromProfile {
            self.navigationViewHeight.constant = 0
            self.tableViewTopConstraints.constant = 0
            navigationBGView.backgroundColor = .clear
            self.navigationView.isHidden = false
            self.blackArrowBackBtn.isHidden = false
            self.blackArrowBackBtn.isHidden = true
            self.getCandidateDetail(loader: true)
            navigationTitleLabel.text = ""
            
        } else {
            self.blackArrowBackBtn.isHidden = true
            self.navigationViewHeight.constant = 44
            self.tableViewTopConstraints.constant = 44
            self.navigationView.isHidden = true
            navigationBGView.isHidden = true
            self.shortlistButton.isHidden = true
            self.buttonStackView.isHidden = true
            heightOfBottomView.constant = 0
            backButton.isHidden = false
            moreButton.isHidden = true
            navigationLabel.text = StringConstants.Profile.uppercased()
        }
        if self.fromRecruiterSearchVC{
            
            self.rejectButton.isHidden = true
            self.stackViewLeadingConst.constant = 45
            self.stackViewTrailingConst.constant = 45
        }
        
        if self.fromShortlistVC {
            self.shortlistButton.isHidden = true
            self.stackViewLeadingConst.constant = 45
            self.stackViewTrailingConst.constant = 45
            self.getCandidateDetail(loader: true)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.fromProfile{
            self.getCandidateDetail(loader: true)
        }
    }
    
    // MARK: - Actions and Target
    // MARK: -
    @IBAction func editButtonTapp(_ sender: UIButton) {
        
        let editProfileScene = CandidateEditProfileVC.instantiate(fromAppStoryboard: .settingsAndChat)
        
        editProfileScene.isFirstTimeTVIncreaseHeight = true
        editProfileScene.candidateData = self.candidateData
        editProfileScene.delegate = self
        
        self.navigationController?.pushViewController(editProfileScene, animated: true)
    }
    
    @IBAction func rejectButtonTapp(_ sender: UIButton) {
        let popUpVc = AlertPopUpVC.instantiate(fromAppStoryboard: .Candidate)
        popUpVc.delegate = self
        popUpVc.commingFromReject = true
        popUpVc.modalPresentationStyle = .overCurrentContext
        popUpVc.modalTransitionStyle = .crossDissolve
        self.present(popUpVc, animated: true, completion: nil)
        
    }
    
    
    @IBAction func shortlistButtonTapp(_ sender: UIButton) {
        print_debug("called")
        
        guard let data = self.candidateData else{return}
        if self.fromJobDetailVC {
            self.jobChangeStatus(sortlistOrReject: 2, applyJobId: self.jobApplyId, categoryId: self.categoryId, loader: true)
            
            self.pop()
        } else if fromPending {
            
            self.jobChangeStatus(sortlistOrReject: 2, applyJobId: self.jobApplyId, categoryId: self.categoryId, loader: true)
            self.pop()
        }  else {
//            if data.jobsInfo.count == 1 {
//
//                if data.jobsInfo.isEmpty{return}
//                self.candidateShortlisted(loader: true, jobId: "\(data.jobsInfo[0].jobId)", categoryId: self.categoryId)
//
//                self.pop()
//            }      else {
                if data.jobsInfo.isEmpty{return}
                let shortListedPopup = SortListedStatusPopupVC.instantiate(fromAppStoryboard: .Recruiter)
                shortListedPopup.jobInfo = data.jobsInfo
                
                shortListedPopup.delegate = self
                
                shortListedPopup.sortListedPopupType = .selectJob
                self.view.addSubview(shortListedPopup.view)
                self.add(childViewController: shortListedPopup)
//            }
        }
    }
    
    @IBAction func backButtonTapp(_ sender: UIButton) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func shareButtonTapp(_ sender: UIButton) {
        
        self.blurView.alpha = 0.65
        self.sharePopupView.alpha = 1.0
        
        UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            self.blurView.alpha = 0.0
            self.sharePopupView.alpha = 0.0
            self.blurView.isHidden = true
            self.sharePopupView.isHidden = true
        }, completion:{ (true) in
            if let data = self.candidateData {
                self.displayShareSheet(shareContent: data.shareUrl)
            }
        })
    }
    
    @IBAction func reportButtonTapp(_ sender: UIButton) {
        let popUpVc = AlertPopUpVC.instantiate(fromAppStoryboard: .Candidate)
        popUpVc.delegate = self
        popUpVc.alertType = .reportCandidate
        popUpVc.modalPresentationStyle = .overCurrentContext
        popUpVc.modalTransitionStyle = .crossDissolve
        self.present(popUpVc, animated: true, completion: nil)
    }
    
    @IBAction func moreButtonTapp(_ sender: UIButton) {
        self.blurView.alpha = 0.0
        self.sharePopupView.alpha = 0.0
        UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            self.blurView.alpha = 0.65
            self.sharePopupView.alpha = 1.0
            self.blurView.isHidden = false
            self.sharePopupView.isHidden = false
        }, completion: nil)
    }
    
    
    @objc func visitorButtonTapp(_ sender: UIButton){
        
        if let data = self.candidateData, data.visitorImageList.count == 0{
            return
        }
        
        let visitorVC = ProfileVisitorVC.instantiate(fromAppStoryboard: .settingsAndChat)
        visitorVC.view.alpha = 0.0
        UIView.animate(withDuration: 0.2) {
            visitorVC.view.alpha = 1.0
            self.view.addSubview(visitorVC.view)
            self.add(childViewController: visitorVC)
        }
        
    }
}

extension RecruiterCandidateDetailVC: AlertPopUpDelegate {
    
    func didTapAffirmativeButton() {
        self.reportJobService(candidateId: self.userId)
    }
    
    func didTapNegativeButton() {
        self.blurView.alpha = 0.0
        self.sharePopupView.alpha = 0.0
        self.blurView.isHidden = true
        self.sharePopupView.isHidden = true
    }
    
    func didTabRejectBtn() {
//        self.reportJobService(candidateId: self.userId)
        self.jobChangeStatus(sortlistOrReject: 3, applyJobId: self.jobApplyId, categoryId: self.categoryId, loader: true)
    }
    
}
// MARK: - Private methods.
// MARK: -
extension RecruiterCandidateDetailVC{
    
    fileprivate func initialSetup(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.tableView.parallaxHeader.view = self.headerView
        self.tableView.parallaxHeader.height = 170
        self.tableView.parallaxHeader.mode = MXParallaxHeaderMode.fill
        
        self.tableView.parallaxHeader.minimumHeight = 0.5
        
        self.rejectButton.borderColor = AppColors.themeBlueColor
        self.rejectButton.setTitleColor(AppColors.themeBlueColor, for: .normal)
        self.shortlistButton.backgroundColor = AppColors.themeBlueColor
        self.shortlistButton.setCorner(cornerRadius: 5.0, clip: true)
        self.shortlistButton.titleLabel?.font = AppFonts.Poppins_Medium.withSize(15.0)
        
        self.blurView.isHidden = true
        self.sharePopupView.isHidden = true
        navigationView.isHidden = false
        navigationBGView.isHidden = false
        self.sharePopupView.setCorner(cornerRadius: 3.0, clip: true)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.removePopup(gesture:)))
        self.blurView.addGestureRecognizer(tap)
        heightOfBottomView.constant = 70
        
        
        self.headerImage.sd_setImage(with: URL(string: candidateData?.user_image ?? ""), placeholderImage: #imageLiteral(resourceName: "icHomeCategoryPlaceholder"), options: [], completed: nil)
        
        self.registerNib()
    }
    
    func registerNib() {
        let nib = UINib(nibName: "RecruiterCandidateDescriptionCell", bundle: nil)
        self.tableView.register(nib, forCellReuseIdentifier: "RecruiterCandidateDescriptionCell")
        self.tableView.register(UINib(nibName: "ExpCell", bundle: nil), forCellReuseIdentifier: "ExpCell")
        let abtNib = UINib(nibName: "CandidateAbtMeCell", bundle: nil)
        self.tableView.register(abtNib, forCellReuseIdentifier: "CandidateAbtMeCell")
        self.tableView.register(UINib(nibName: "CandidateHeaderCell", bundle: nil), forCellReuseIdentifier: "CandidateHeaderCell")
        self.tableView.register(UINib(nibName: "SubmitButtonCell", bundle: nil), forCellReuseIdentifier: "SubmitButtonCell")
    }
    
    @objc func removePopup(gesture: UITapGestureRecognizer){
        self.blurView.alpha = 0.65
        self.sharePopupView.alpha = 1.0
        UIView.animate(withDuration: 0.3, delay: 0, options: UIViewAnimationOptions.curveEaseInOut, animations: {
            self.blurView.alpha = 0.0
            self.sharePopupView.alpha = 0.0
            self.blurView.isHidden = true
            self.sharePopupView.isHidden = true
        }, completion: nil)
    }
    
    
    @objc private func chatButtonTapped(_ sender: UIButton){
        
        
        //        TODO:- Show loader if required
        
        ChatHelper.getUserDetails(userId: self.userId, completion: { [weak self](member) in
            
            guard let _self = self else { return }
            
            if let member = member{
                
                let chatVc = PersonalChatVC.instantiate(fromAppStoryboard: .Candidate)
                chatVc.chatMember = member
                chatVc.isOpenFromProfile = true
                _self.navigationController?.pushViewController(chatVc, animated: false)
            }else{
                _self.showAlert(msg: StringConstants.Candidate_not_available)
            }
        })
    }
    
    @objc func videoCallButtonTapped(_ sender: UIButton){
        notifyVideoCallApi(sender: sender)
    }
    
    func notifyVideoCallApi(sender: UIButton){
        
        let userData = User(json: (AppUserDefaults.value(forKey: .userData)))
        
        
        guard let candidateDetail = self.candidateData else{return}
        
        if candidateDetail.user_login_session.isEmpty{
            CommonClass.showToast(msg: StringConstants.UserNotAvailable )
            return
        }
        
        var params = JSONDictionary()
        params["senderName"] = userData.first_name
        params["receiverName"] = candidateDetail.first_name
        params["senderId"] = userData.user_id ?? ""
        params["receiverId"] = candidateDetail.id
        params["deviceId"] = candidateDetail.user_login_session[0].device_id
        params["deviceToken"] = candidateDetail.user_login_session[0].device_token
        params["deviceType"] = candidateDetail.user_login_session[0].device_type
        params["senderDeviceToken"] = DeviceDetail.deviceToken
        params["type"] = "calling"
        params["senderImage"] = userData.imageUrl
        params["roomId"] = "\(userData.user_id ?? "")_\(candidateDetail.id)"
        
        WebServices.notifyVideoCall(parameters: params, loader: true, success: { (data) in
            sharedAppDelegate.moveToVideoChatScene(with: Caller(json: data["data"])!,fromPush : false, fromCallKit: false)
        }) { (error) in
            self.showAlert(msg: error.localizedDescription)
            return
        }
    }
}

//MARK:- Web Services
//====================
extension RecruiterCandidateDetailVC {
    
    func getCandidateDetail(loader: Bool) {
        
        let param = [ApiKeys.userId.rawValue :self.userId] as [String : Any]
        print_debug(param)
        WebServices.getCandidateDetail(parameters: param, loader: loader, success: { (json) in
            if json["code"].intValue == error_codes.success{
                
                print_debug(json)
                let jobs = json["data"]["users"]
                self.candidateData = CandidateAppliedJobDetail.init(dict: jobs)
                guard let data = self.candidateData else{return}
                AppUserDefaults.save(value: data.jobLatitude, forKey: .profileLat)
                AppUserDefaults.save(value: data.jobLongitude, forKey: .profileLong)
                if  data.jobsInfo.count == 0 && self.fromRecruiterSearchVC{
                    self.buttonStackView.isHidden = true
                    self.heightOfBottomView.constant = 0
                }
                self.headerImage.sd_setImage(with: URL(string: data.user_image), placeholderImage: #imageLiteral(resourceName: "icHomeCategoryPlaceholder"), options: [], completed: nil)
                self.tableView.reloadData()
                
            }else{
                
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
            
        }) { (error: Error) in
            CommonClass.showToast(msg: error.localizedDescription )
        }
    }
    
    func jobChangeStatus(sortlistOrReject jobType: Int, applyJobId: String, categoryId: String, loader: Bool) {
        
        let param = [ApiKeys.type.rawValue: jobType, ApiKeys.applyId.rawValue: applyJobId, ApiKeys.jobId.rawValue: self.jobId ] as [String: Any]
        print_debug(param)
        WebServices.changePendingJobStatus(parameters: param, loader: loader, success: { (json) in
           if let code = json["code"].int, code == error_codes.success {
                self.reloadDelegate?.reload()
                self.pop()
            }
            let msg = json["message"].stringValue
            CommonClass.showToast(msg: msg)
        }) { (error) in
            CommonClass.showToast(msg: error.localizedDescription )
        }
    }
    
    
    func candidateShortlisted(loader: Bool, jobId: String, categoryId: String ) {
        
        let param = [ ApiKeys.userId.rawValue :self.userId, ApiKeys.jobId.rawValue :jobId, "categoryId": categoryId ] as [String : Any]
        WebServices.candidateShortlisted(parameters: param, loader: loader, success: { (json) in
            if json["code"].intValue == error_codes.success{
//                ChatHelper.getCandInfoCoresRec(candUserId: self.userId, completion: { (detail) in
//                    print_debug(detail)
//                })
//                ChatHelper.updateCandInfoCoresRec(candUserId: self.userId, isShortListed: true, isReport: false, isStarDeduct: false)
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }else{
                
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
            
        }) { (error: Error) in
            CommonClass.showToast(msg: error.localizedDescription )
        }
    }
    
    func sortedInHeighestExperience(_ experienceData: [JSON])-> [JSON]{
        var convertInMonth = experienceData.map { (value) -> JSON in
            var duration = value
            if value["durationType"].intValue  == 2{
                duration["duration"] = JSON(value["duration"].intValue*12)
                return duration
            }else{
                duration["duration"] = JSON(value["duration"].intValue)
                return duration
            }
        }
        convertInMonth = convertInMonth.sorted {
            print_debug("\($0["duration"].intValue), \($1["duration"].intValue)")
            return ($0["duration"].intValue) > ( $1["duration"].intValue)
        }
        return convertInMonth
        
    }
    
    
    func reportJobService(candidateId: String){
        
        
        WebServices.reportCandidate(parameters: ["recruiterId": candidateId], loader: true, success: { (json) in
            self.blurView.isHidden = true
            self.sharePopupView.isHidden = true
            
            let msg = json["message"].stringValue
            CommonClass.showToast(msg: msg)
            
            self.pop()
        }) { (error) in
            
            print_debug(error.localizedDescription)
        }
    }
    
    
}

// MARK: - Table view delegate and datasource methods.
// MARK: -
extension RecruiterCandidateDetailVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if let _ = self.candidateData{
            return 3
        }else{
            return 1
        }
    }
    
    //
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.00001
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.00001
    }
    //
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            if self.fromRecruiterSearchVC || self.fromPending || self.fromShortlistVC{
                if let data = self.candidateData {
                    if data.about.isEmpty{
                        return 1
                    }else{
                        return 2
                    }
                    
                }else{
                    return 1
                }
                
            }else{
                return 2
            }
            
        } else if section == 1  {
            
            guard let data = self.candidateData else{return 0}
            
            return  1
        } else {
            if self.fromRecruiterSearchVC || self.fromPending || self.fromShortlistVC {
                if let data = self.candidateData , data.education.isEmpty{
                    return 0 
                }else{
                    return 2
                }
                
            }else{
                return 2
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            if indexPath.row == 0 {
                
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "RecruiterCandidateInfoCell", for: indexPath) as? RecruiterCandidateInfoCell else{ fatalError("RecruiterCandidateInfoCell not found.")}
                
                cell.visitorButton.addTarget(self, action: #selector(self.visitorButtonTapp(_:)), for: .touchUpInside)
                cell.messageButton.addTarget(self, action: #selector(self.chatButtonTapped(_:)), for: .touchUpInside)
                if let data = self.candidateData  {
                    if fromProfile {
                        if data.visitorImageList.count == 0{
                            cell.visitorView.isHidden = true
                        }else{
                            cell.visitorView.isHidden = false
                            cell.setIntrestedImageView(applicants: data.visitorImageList)
                        }
                        
                        cell.messageButton.isHidden = true
                        cell.chatImage.isHidden = true
                        cell.videoCallBtn.isHidden = true
                        cell.nameLabel.text = "\(data.first_name) \(data.last_name)"
                    } else {
                        cell.visitorView.isHidden = true
                        cell.visitorButton.isHidden = true
                        cell.messageButton.isHidden = false
                        cell.chatImage.isHidden = false
                        cell.videoCallBtn.isHidden = false
                        if let firstChar = data.last_name.first {
                            cell.nameLabel.text = "\(data.first_name) \(firstChar)."
                        }
                    }
                    
                    cell.populatedCell(index: indexPath.row, data: data)
                    cell.videoCallBtn.isHidden = true
                }
                return cell
                
            }else{
                
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "CandidateAbtMeCell", for: indexPath) as? CandidateAbtMeCell  else{ fatalError("RecruiterCandidateDescriptionCell not found.")}
                if let data = self.candidateData {
                    cell.descLabel.text = data.about.trailingSpacesTrimmed
                    self.aboutMeLabelHeight  = cell.descLabel.height
                }
                return cell
            }
            
        case 1:
            guard let data = self.candidateData else{  fatalError("candidate data not found")}
            
            
            if data.experience.count == 0 {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "CandidateHeaderCell") as? CandidateHeaderCell else{ fatalError("CandidateHeaderCell not found.")}
                cell.upperLabel.text = StringConstants.Experience
                cell.lowerLabel.text = StringConstants.No_Experience
                return cell
                
            } else {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "RecruiterCandidateExperienceCell", for: indexPath) as? RecruiterCandidateExperienceCell else{ fatalError("RecruiterCandidateExperienceCell not found.")}
                
                
                for view in cell.experianceStackView.arrangedSubviews {
                    view.removeFromSuperview()
                }
                
                if let data = self.candidateData{
                    var index = 0
                    let _ = data.experience.map{value in
                        
                        guard let rowView = UIView.loadFromNibNamed("ExperianceView") as? ExperianceView else{fatalError("AddExperienceView not found.")}
                        
                        rowView.populatedCell(data: data, index: index)
                        index += 1
                        print_debug(index)
                        print_debug(data.experience.count)
                        if index == data.experience.count  {
                            rowView.bottomSeperatorView.isHidden = true
                        }
                        rowView.layoutIfNeeded()
                        
                        cell.experianceStackView.addArrangedSubview(rowView)
                    }
                }
                return cell
            }
            
        default:
            
            if indexPath.row == 0 {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "CandidateAbtMeCell", for: indexPath) as? CandidateAbtMeCell  else{ fatalError("CandidateAbtMeCell not found.")}
                
                if let data = self.candidateData{
                    cell.abtMeLabel.text = StringConstants.Education
                    cell.descLabel.text = data.education.trailingSpacesTrimmed
                }
                self.edicationLabelHeight = cell.descLabel.height
                return cell
            } else {
                if fromJobDetailVC || fromRecruiterSearchVC || fromPending {
                    // return empty cell in case of comming from JobDetailVC or
                    return UITableViewCell()
                } else if fromShortlistVC {
                    return UITableViewCell()
                }
                else {
                    print_debug(indexPath)
                    guard let cell = tableView.dequeueReusableCell(withIdentifier: "RecruiterCandidateSettingCell", for: indexPath) as? RecruiterCandidateSettingCell  else{ fatalError("RecruiterCandidateSettingCell not found.")}
                    return cell
                }
            }
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 2 && indexPath.row == 1{
            
            let settingsVC  = SettingsVC.instantiate(fromAppStoryboard: .settingsAndChat)
            if let data = self.candidateData {
                settingsVC.userId = "\(data.user_id)"
            }
            
            self.navigationController?.pushViewController(settingsVC, animated: true)
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        if scrollView.contentOffset.y > -50 {
            self.navigationBGView.backgroundColor = AppColors.themeBlueColor
            self.navigationView.backgroundColor = AppColors.themeBlueColor
        }else{
            self.navigationView.backgroundColor = UIColor.clear
            self.navigationBGView.backgroundColor = UIColor.clear
        }
    }
}

extension RecruiterCandidateDetailVC: ShortListedApplicantsPopup, RefreshCandidateProfileDelegate {
    func refreshCandidateProfileDelegate(candidateData: CandidateAppliedJobDetail) {
        
        self.candidateData = candidateData
        self.tableView.reloadData()
        self.headerImage.sd_setImage(with: URL(string: candidateData.user_image), placeholderImage: #imageLiteral(resourceName: "icHomeCategoryPlaceholder"), options: [], completed: nil)
        
    }
    
    func onTapAffermation(index: Int, applyJobId: String,  categoryId: String) {
        self.categoryId = categoryId
        self.candidateShortlisted(loader: true, jobId: applyJobId, categoryId: self.categoryId)
        self.pop()
    }
    
    func onTapCancel() {
        print_debug("perform action")
    }
}


