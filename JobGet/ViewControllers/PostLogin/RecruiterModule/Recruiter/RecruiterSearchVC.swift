//
//  RecruiterSearchVC.swift
//  JobGet
//
//  Created by Abhi on 21/05/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import DZNEmptyDataSet
import GoogleMaps
import SDWebImage

class RecruiterSearchVC: BaseVC {
    
    @IBOutlet weak var searchBubbleView: UIView!
    @IBOutlet weak var badgeLabel: UILabel!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var navigationShadowView: UIView!
    @IBOutlet weak var navigationLAbel: UILabel!
    @IBOutlet weak var searchTableView: UITableView!
    @IBOutlet weak var filterButton: UIButton!
    
    //MARK:- Properties.
    var pageNo = 0
    var candidateList = [AppliedJobListModel]()    
    var shouldLoadMoreData = false
    var isFirstTimeLoad   = true
    var selectedRow = [IndexPath]()
    private var experienceType: Int?
    private var totalExperience: String?
    var rawExp: Float?
    var sliderLabelText: String?
    var categoryId = ""
    var searchText = ""
    var isSearchJob = false
    var fromFilters = false

    var filterDataDic = [String: AnyObject]()
    var currentAddress: String?
    
    
    private var currentLocation: CLLocation? {
        
        didSet {
            print_debug(currentLocation)
            self.filterDataDic["radius"] = "20" as AnyObject
            self.getCandidateList(loader: self.isFirstTimeLoad, pageNo: 0, latitude: "\(currentLocation?.coordinate.latitude ?? 42.361145)", longitude: "\(currentLocation?.coordinate.longitude ?? -71.057083)")
            AppUserDefaults.save(value: currentLocation?.coordinate.latitude ?? 42.361145, forKey: .userLat)
            AppUserDefaults.save(value: currentLocation?.coordinate.longitude ?? -71.057083, forKey: .userLong)
            DispatchQueue.main.async {[weak self] in
               guard let strongSelf = self else{return}
                strongSelf.currentLocation?.convertToPlaceMark({ (place) in
                    
                    strongSelf.filterDataDic["coordinates"] = strongSelf.currentLocation
                    strongSelf.currentAddress = "\(place.locality ?? ""), \(place.administrativeArea ?? "")"
                    strongSelf.filterDataDic["address"] = "\(place.locality ?? ""), \(place.administrativeArea ?? "")" as AnyObject
                    
                })
            }
            print_debug(currentLocation)
            
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initialSetup()
//        if !fromFilters {
            getLocation()
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isSearchJob{
            if let location = self.filterDataDic["coordinates"] as? CLLocation{
                self.getCandidateList(loader: false,  pageNo: 0, latitude: "\(location.coordinate.latitude )", longitude: "\(location.coordinate.longitude)")
            }
        }
        
        if AppUserDefaults.value(forKey: .totalNotification).intValue > 0{
            self.badgeLabel.isHidden = false
        }else{
            self.badgeLabel.isHidden = true
        }        
        isSearchJob = false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @IBAction func notificationButtonTapp(_ sender: UIButton) {
        let notificationVC = NotificationsVC.instantiate(fromAppStoryboard: .settingsAndChat)
        self.navigationController?.pushViewController(notificationVC, animated: true)
    }
    
    @IBAction func searchButtonTapp(_ sender: UIButton) {
        let searchScene = CandidateSearchFilterVC.instantiate(fromAppStoryboard: .settingsAndChat)
        searchScene.recDelegate = self
        self.fromFilters = true
        searchScene.selectedFilterIndex = self.selectedRow
        searchScene.rawExp = self.rawExp
        searchScene.searchText = self.searchText
        searchScene.sliderLabelText = self.sliderLabelText
        searchScene.filteredCategoryId = self.categoryId
        searchScene.totalSelectedCategory = self.filterDataDic["categoryName"] as? String ?? ""
        if let coordi = self.filterDataDic["coordinates"] as? CLLocation{
            searchScene.searchLocation = coordi
        }
        searchScene.radius = self.filterDataDic["radius"] as? String ?? ""
        searchScene.filterDic["currentLocation"] = self.currentLocation
        searchScene.address = self.filterDataDic["address"] as? String ?? ""
        searchScene.currentAddress = self.currentAddress
        self.navigationController?.pushViewController(searchScene, animated: true)
    }
    
    @IBAction func filterButtonTapp(_ sender: UIButton) {
        
    }
}

//MARK:- Private Methods
//=======================
private extension RecruiterSearchVC {
    
    func initialSetup() {
        
        self.filterButton.roundCorners()
        self.filterButton.setCircleShadow()
        self.searchTableView.emptyDataSetSource = self
        self.searchTableView.emptyDataSetDelegate = self
        self.searchTableView.prefetchDataSource = self
        self.searchTableView.rowHeight = UITableViewAutomaticDimension
        self.searchTableView.estimatedRowHeight = 300
        self.searchBubbleView.roundCorners()
        self.searchBubbleView.isHidden = true
        self.searchTableView.enablePullToRefresh(tintColor: AppColors.themeBlueColor ,target: self, selector: #selector(refreshWhenPull(_:)))
        self.navigationView.layoutIfNeeded()
        self.navigationLAbel.text = StringConstants.CANDIDATES
        self.navigationView.setShadow(offset: CGSize(width: 0.0, height: 2.0))
        self.badgeLabel.roundCorners()
         NotificationCenter.default.addObserver(self, selector: #selector(self.manageDenyLocation), name: NSNotification.Name.init(rawValue: "DENY_CURRENT_LOCATION_PERMISSION"), object: nil)
        self.registerNib()
    }
    
    func getLocation() {
        DispatchQueue.main.async { [weak self] in
             guard let strongSelf = self else{return}
            if CLLocationManager.authorizationStatus() == .denied {
               
                if AppUserDefaults.value(forKey: .isLogin).boolValue{
                    if let data = AppUserDefaults.value(forKey: .userData).dictionary, let lat = data["lat"]?.double,let lng = data["lng"]?.double{
                        if lat != 0.0{
                            strongSelf.currentLocation = CLLocation(latitude: lat, longitude: lng)
                        }else{
                            strongSelf.currentLocation = CLLocation(latitude: 42.361145, longitude: -71.057083)
                        }
                    }else {
                        strongSelf.currentLocation = CLLocation(latitude: 42.361145, longitude: -71.057083)
                    }
                }else{
                    CommonClass.delayWithSeconds(1.0, completion: {
                        let alertViewController = UIAlertController(title: StringConstants.Location_Allow_Title, message: StringConstants.Fetch_Location_Reason_Text, preferredStyle: UIAlertControllerStyle.alert)
                        let okAction = UIAlertAction(title: StringConstants.Allow, style: UIAlertActionStyle.default, handler: { (action : UIAlertAction) in
                            //        TODO: Add settings
                            if #available(iOS 10.0, *) {
                                UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: { (success) in
                                })
                            } 
                        })
                        
                        let deny = UIAlertAction(title: StringConstants.Deny, style: UIAlertActionStyle.default, handler: { (action : UIAlertAction) in
                            NotificationCenter.default.addObserver(strongSelf, selector: #selector(strongSelf.updateLocation(noti:)), name: NSNotification.Name(rawValue: "UPDATE_LOCATION"), object: nil)
//                            AppUserDefaults.save(value: false, forKey: .isLogin)
                            let updateProfileLocationPopupScene = UpdateProfileLocationPopupVC.instantiate(fromAppStoryboard: .settingsAndChat)
                            updateProfileLocationPopupScene.delegate = self
                            updateProfileLocationPopupScene.openLocationPopupFrom = .jobListCand
                            updateProfileLocationPopupScene.address = "Boston, MA"
                            sharedAppDelegate.window?.addSubview(updateProfileLocationPopupScene.view)
                            strongSelf.addChildViewController(updateProfileLocationPopupScene)
                        })
                        
                        alertViewController.addAction(deny)
                        alertViewController.addAction(okAction)
                        
                        strongSelf.present(alertViewController, animated: true, completion: nil)
                    })
                }
                
            } else {
                if let coordi = strongSelf.filterDataDic["coordinates"] as? CLLocation{
                    strongSelf.getCandidateList(loader: true,  pageNo: 0, latitude: "\(coordi.coordinate.latitude)", longitude: "\(coordi.coordinate.longitude)")
                }else{
                    SharedLocationManager.fetchCurrentLocation { (location) in
                        SharedLocationManager.locationManager.stopUpdatingLocation()
                        strongSelf.currentLocation = location
                    }
                }
            }
        }
    }
    
    func registerNib() {
        self.searchTableView.register(UINib(nibName: "RecruiterSearchCell", bundle: nil), forCellReuseIdentifier: "RecruiterSearchCell")
        self.searchTableView.register(UINib(nibName: "LoadMoreTableViewCell", bundle: nil), forCellReuseIdentifier: "LoadMoreTableViewCell")
    }
    
    func isLoadingIndexPath(_ indexPath: IndexPath) -> Bool {
        guard shouldLoadMoreData else { return false}
        
        return indexPath.row == self.candidateList.count
    }
    
    @objc func refreshWhenPull(_ sender: UIRefreshControl) {
        sender.endRefreshing()
        self.pageNo = 0
        self.shouldLoadMoreData = false
//        if !fromFilters{
//            getLocation()
//        }else{
            if let coordi = self.filterDataDic["coordinates"] as? CLLocation{
                self.getCandidateList(loader: false,  pageNo: 0, latitude: "\(coordi.coordinate.latitude)", longitude: "\(coordi.coordinate.longitude)")
            }
//        }
    }
    
    @objc func shareButtonTapped(_ sender: UIButton) {
        guard let index = sender.tableViewIndexPath(self.searchTableView) else { return }
        
        self.displayShareSheet(shareContent: self.candidateList[index.row].shareUrl)
    }
    
    
    func filterCandidateList(_ candidateList: [AppliedJobListModel]) -> [AppliedJobListModel] {
        
        let experienceData = candidateList.map{ ( experience)-> AppliedJobListModel in
            
            var jobListModel = experience
            var convertInMonth = experience.experience.map{ value -> ExperienceModel in
                
                var duration = value
                if value.durationType  == 1 {
                    if let durations = Int(value.duration){
                        duration.duration = "\(durations)"
                    }
                    return duration
                }else{
                    duration.duration = value.duration
                    print_debug(duration.duration)
                    return duration
                }
            }
            
            convertInMonth = convertInMonth.sorted {
                if let duration1 = Int($0.duration), let duration2 = Int($1.duration){
                    return duration1>duration2
                }
                return false
                
            }
            
            jobListModel.experience = convertInMonth
            
            return jobListModel
            
        }
        
        return experienceData
        
    }
}

//MARK:- Web Services
//====================
extension RecruiterSearchVC {
    
    func getCandidateList(loader: Bool,  pageNo: Int, latitude: String, longitude: String) {
        
        var param:JSONDictionary = [
                      ApiKeys.categoryId.rawValue: self.categoryId ,
                      ApiKeys.page.rawValue :pageNo,
                      ApiKeys.search.rawValue:  self.searchText,
                      ApiKeys.latitude.rawValue: latitude,
                      ApiKeys.longitude.rawValue: longitude,
                      "radius": self.filterDataDic["radius"] as? String ?? "20"]
           
        
        if let expType = self.experienceType {
            param["durationType"] = expType
        }
        
        if let experiance = self.totalExperience {
            param["duration"] = experiance
        }
        
        print_debug(param)
        WebServices.getCandidateList(parameters: param, loader: loader, success: { (json) in
            if json["code"].intValue == error_codes.success{
                self.isFirstTimeLoad = false
                self.fromFilters = false

                var data = [AppliedJobListModel]()
                let jobs = json["data"]["users"].arrayValue
                
                for job in jobs {
                    data.append(AppliedJobListModel(dict: job))
                }
                
                if pageNo == 0{
                    self.candidateList.removeAll(keepingCapacity: false)
                }

                if self.shouldLoadMoreData {
                    self.candidateList.append(contentsOf: data)
                } else {
                    self.candidateList = data
                }
                
                self.candidateList = self.filterCandidateList(self.candidateList)
                self.shouldLoadMoreData = json[ApiKeys.next.rawValue].boolValue
                self.pageNo = json["page"].intValue
                
                self.searchTableView.reloadData()
               
            }else{
                
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
            
        }) { (error: Error) in
            CommonClass.showToast(msg: error.localizedDescription )
        }
    }
}

//MARK: - Table view Delegate and DataSource
//============================================
extension RecruiterSearchVC: UITableViewDelegate, UITableViewDataSource,UITableViewDataSourcePrefetching {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let count = self.candidateList.count
        return shouldLoadMoreData ? count + 1 : count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isLoadingIndexPath(indexPath) {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.defaultReuseIdentifier, for: indexPath) as? LoadMoreTableViewCell else {
                fatalError("Failed to initialize LoadMoreTableViewCell")
            }
            return cell
        }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "RecruiterSearchCell", for: indexPath) as? RecruiterSearchCell else {
            fatalError("Failed to initialize LoadMoreTableViewCell")
        }
        
        cell.shareButton.addTarget(self, action: #selector(self.shareButtonTapped(_:)), for: .touchUpInside)
        cell.populatedCell(index: indexPath.row, data: self.candidateList[indexPath.row], selectedCategoryId: self.categoryId)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard self.shouldLoadMoreData else { return }
        
        if  (indexPath.row == self.candidateList.count-1) && self.shouldLoadMoreData {  //for 2nd last cell
            self.getCandidateList(loader: self.isFirstTimeLoad,  pageNo: self.pageNo, latitude: "\(currentLocation?.coordinate.latitude ?? 42.361145)", longitude: "\(currentLocation?.coordinate.longitude ?? -71.057083)")
        }
    }
        
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let candidateDetailScene = RecruiterCandidateDetailVC.instantiate(fromAppStoryboard: .Recruiter)
        candidateDetailScene.userId = self.candidateList[indexPath.row].userId
        candidateDetailScene.fromProfile = false
        candidateDetailScene.fromRecruiterSearchVC = true
        self.isSearchJob = false
        self.navigationController?.pushViewController(candidateDetailScene, animated: true)
    }
    
    func tableView(_ tableView: UITableView, prefetchRowsAt indexPaths: [IndexPath]) {
        guard tableView === self.searchTableView else {return}
        
        var urls : [URL] = []
        
        indexPaths.forEach { (indexPath) in
            if indexPath.item <= candidateList.count - 1 {
                let media = candidateList[indexPath.item].userImage
                if let url = URL(string: media) {
                    urls.append(url)
                }
            }
        }
        
        SDWebImagePrefetcher.shared().prefetchURLs(urls)
    }
    
    func tableView(_ tableView: UITableView, cancelPrefetchingForRowsAt indexPaths: [IndexPath]) {
//        SDWebImagePrefetcher.shared().cancelPrefetching()
    }
}


//MARK:- DZNEmptyDataSource and Delegate
//==========================================
extension RecruiterSearchVC: DZNEmptyDataSetDelegate, DZNEmptyDataSetSource {
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return #imageLiteral(resourceName: "ic_placeholder_11")
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: StringConstants.No_Candidate_Found,                                                                           attributes: [NSAttributedStringKey.foregroundColor: AppColors.black46,
                                                                                                                                                                     NSAttributedStringKey.font: AppFonts.Poppins_SemiBold.withSize(14)
            ])
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        return NSAttributedString(string: StringConstants.Search_for_candidate,                                                                           attributes: [NSAttributedStringKey.foregroundColor: AppColors.gray152,
                                                                                                                                                                       NSAttributedStringKey.font: AppFonts.Poppins_Regular.withSize(14)])
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}

//MARK:- Get Search Text
//===========================
extension RecruiterSearchVC: SearchCompelete, RecruiterFilterDelegate {
    func getSearchedText(text: String, lat: String, long: String, radius: String, sort: Int, categoryID: String, selectedCatFilterIndex: [IndexPath], categoryName: String, isClear: Bool) {
        
    }
        
    func recruiterFilter(text: String, lat: String, long: String, categoryId: String, selectedCategory: [IndexPath], experienceType: Int?, totalExperience: String?, rawExp: Float?, sliderLabelText: String?, categoryName: String, radius: String) {
        
        self.filterDataDic["radius"] = radius as AnyObject
        self.selectedRow = selectedCategory
        self.categoryId = categoryId
        self.experienceType = experienceType
        self.totalExperience = totalExperience
        self.rawExp = rawExp
        self.sliderLabelText = sliderLabelText
        self.searchText = text
        self.filterDataDic["categoryName"] = categoryName as AnyObject
        if !lat.isEmpty{
            self.filterDataDic["coordinates"] = CLLocation(latitude: Double(lat) ?? 42.361145, longitude:  Double(long) ?? -71.057083)
        }
        if !self.categoryId.isEmpty || !searchText.isEmpty || sliderLabelText != StringConstants.Any_Experienc ||  lat != "\(currentLocation?.coordinate.latitude ?? 42.361145)" || long != "\(currentLocation?.coordinate.longitude ?? -71.057083)"{
            self.searchBubbleView.isHidden = false
        }else{
            self.searchBubbleView.isHidden = true
        }
        
        if let lat = Double(lat), let long = Double(long){
            let loc = CLLocation(latitude: lat, longitude: long)
            self.currentLocation = loc
        }
        
        self.getCurrentCity(lat: lat, long: long)
        
        self.getCandidateList(loader: self.isFirstTimeLoad, pageNo: 0, latitude: lat, longitude: long)

    }
    
    func getCurrentCity(lat: String, long: String){
        
        let fetchLocation = CLLocation(latitude: Double(lat)!, longitude: Double(long)!)
        self.filterDataDic["coordinates"] = fetchLocation
        DispatchQueue.mainQueueAsync{
            fetchLocation.convertToPlaceMark({ (place) in
                
                self.filterDataDic["address"] = "\(place.locality ?? ""), \(place.administrativeArea ?? "")" as AnyObject

            })
        }
    }
}

//MARK:- Location alert popup Delegate
//===========================================
extension RecruiterSearchVC: AlertPopUpDelegate, CompleteProfileWithLocationDelegate {
    
    func didTapAffirmativeButton() {
        //        TODO: Add settings
        if #available(iOS 10.0, *) {
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:], completionHandler: { (success) in
            })
        } else {
            // Fallback on earlier versions
        }
        
    }
    
    @objc func manageDenyLocation(){
        
        print_debug("location access denied")
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateLocation(noti:)), name: NSNotification.Name(rawValue: "UPDATE_LOCATION"), object: nil)
        let updateProfileLocationPopupScene = UpdateProfileLocationPopupVC.instantiate(fromAppStoryboard: .settingsAndChat)
        updateProfileLocationPopupScene.delegate = self
        updateProfileLocationPopupScene.openLocationPopupFrom = .jobListCand
        updateProfileLocationPopupScene.address = "Boston, MA"
        sharedAppDelegate.window?.addSubview(updateProfileLocationPopupScene.view)
        self.addChildViewController(updateProfileLocationPopupScene)
    }
    
    func didTapNegativeButton() {
        print_debug("location access denied")
        self.getCandidateList(loader: true,  pageNo: 0, latitude: "\(currentLocation?.coordinate.latitude ?? 42.361145)", longitude: "\(currentLocation?.coordinate.longitude ?? -71.057083)")
    }
    
    
    func completeProfileWithLocationDelegate() {
        AppUserDefaults.save(value: 42.361145, forKey: .userLat)
        AppUserDefaults.save(value: -71.057083, forKey: .userLong)
        self.filterDataDic["coordinates"] = CLLocation(latitude: 42.361145, longitude: -71.057083)
        self.filterDataDic["currentLocation"] = CLLocation(latitude: 42.361145, longitude: -71.057083)
        self.filterDataDic["city"] = "Boston" as AnyObject
        self.currentAddress = "Boston, MA"
        self.filterDataDic["address"] = "Boston, MA"  as AnyObject
        self.getCandidateList(loader: true,  pageNo: 0, latitude: StringConstants.Boston_Lat, longitude: StringConstants.Boston_Long)
    }
    
    @objc func updateLocation(noti: Notification){
        
        if let data = noti.userInfo as? [String:Any]{
            
            if let lat = Double(data["latitude"] as? String ?? ""), let long = Double(data["longitude"] as? String ?? ""){
                self.filterDataDic["coordinates"] = CLLocation(latitude: lat, longitude: long)
                self.filterDataDic["currentLocation"] = CLLocation(latitude: lat, longitude: long)
                AppUserDefaults.save(value: lat, forKey: .userLat)
                AppUserDefaults.save(value: long, forKey: .userLong)
            }
            self.filterDataDic["city"] = data["city"] as AnyObject
            self.currentAddress = "\(data["city"] as? String ?? ""), \(data["state"] as? String ?? "")"
            self.filterDataDic["address"] = "\(data["city"] as? String ?? ""), \(data["state"] as? String ?? "")" as AnyObject
            self.getCandidateList(loader: true,  pageNo: 0, latitude: data["latitude"] as? String ?? "", longitude: data["longitude"] as? String ?? "")
        }
    }
}
