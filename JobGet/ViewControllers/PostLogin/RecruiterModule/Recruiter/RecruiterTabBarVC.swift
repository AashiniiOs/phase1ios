//
//  RecruiterTabBarVC.swift
//  JobGet
//
//  Created by macOS on 04/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class RecruiterTabBarVC: UITabBarController {
    
    //MARK:-
    //MARK:- IBOutlets
    @IBOutlet weak var customTabBar: UITabBar!
    
    //MARK:-
    //MARK:- Properties
    var selectIndex : Int = 0
    private var badgeObserverHandle: UInt?
    
    var myBadgeCount = 0
   // var fromPush = false
    var isChatPush = false
    var isJobPush = false
    var jobId = ""
    var roomId = ""
    var chatMember : ChatMember?

    //MARK:-
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !AppUserDefaults.value(forKey: .pauseMsg).stringValue.isEmpty{
            AppUserDefaults.save(value: "", forKey: .pauseMsg)
            let obj = AlertPopUpVC.instantiate(fromAppStoryboard: .Candidate)
            obj.alertType = .pauseImage
            obj.modalPresentationStyle = .overCurrentContext
            obj.modalTransitionStyle = .crossDissolve
            let rootViewController = UIApplication.shared.keyWindow?.rootViewController
            rootViewController?.present(obj, animated: true, completion: nil)
        }
        self.initialSetup()
        self.observeBadgeCount()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isChatPush{
            isChatPush = false
        }
        //self.fromPush = false
        
    }
    
    deinit {
        self.removeBadgeObserver()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}


//MARK:-
//MARK:- Private Functions
extension RecruiterTabBarVC {
    
    func initialSetup() {
        
        self.navigationController?.viewControllers = [self]
        
        let myJobVC             = MyJobVC.instantiate(fromAppStoryboard: .Recruiter)
        myJobVC.fromPush = self.isJobPush
        myJobVC.jobId = self.jobId
        let chatVC              = ChatVC.instantiate(fromAppStoryboard: .Candidate)
        chatVC.fromPush = isChatPush
        chatVC.roomId = self.roomId
        if let member = self.chatMember{
            chatVC.chatMember = member
        }
        let searchVC            = RecruiterSearchVC.instantiate(fromAppStoryboard: .Recruiter)
        //        let starVC              = LogoutVC.instantiate(fromAppStoryboard: .Main)
        let starVC              = StarPackage.instantiate(fromAppStoryboard: .Star) //LogoutVC
        
        let profileVC           = RecruiterProfileVC.instantiate(fromAppStoryboard: .RecruiterProfile)
        
        self.viewControllers = [myJobVC, chatVC, searchVC, starVC, profileVC]
        
        for item in self.customTabBar.items! {
            
            switch item {
                
            case (self.customTabBar.items?[0])!:
                
                item.title          =  StringConstants.Postings
                item.image          = #imageLiteral(resourceName: "icHomeDeselectJob")
                item.selectedImage  = #imageLiteral(resourceName: "icHomeSelectJob")
                
            case (self.customTabBar.items?[1])!:
                
                item.title          = StringConstants.Messages
                item.image          = #imageLiteral(resourceName: "icHomeDeselectChat")
                item.selectedImage  = #imageLiteral(resourceName: "icHomeSelectChat")
                
                
            case (self.customTabBar.items?[2])!:
                
                item.title          = StringConstants.Search
                item.image              = #imageLiteral(resourceName: "icHomeDeselectSearch")
                item.selectedImage      = #imageLiteral(resourceName: "icHomeSelectSearch")
                
            case (self.customTabBar.items?[3])!:
                
                item.title          = StringConstants.Stars
                item.image          = #imageLiteral(resourceName: "icHomeDeselectStar")
                item.selectedImage  = #imageLiteral(resourceName: "icHomeSelectStar")
                
            case (self.customTabBar.items?[4])!:
                
                item.title          = StringConstants.Profile
                item.image          = #imageLiteral(resourceName: "ic_home_profile_deselect")
                item.selectedImage  = #imageLiteral(resourceName: "ic_home_profile_select1")
                
            default:
                break
            }
            
            item.selectedImage = item.selectedImage?.withRenderingMode(.alwaysOriginal)
            item.image = item.image?.withRenderingMode(.alwaysOriginal)
        }
        
        self.selectedIndex = self.selectIndex
        self.customTabBar.backgroundColor = AppColors.white
    }
    
}


//    MARK:- BadgeCount observer
//    ===========================================
extension RecruiterTabBarVC{
    
    private func observeBadgeCount(){
        guard let userId = User.getUserModel().user_id else{
            return
        }
        badgeObserverHandle = databaseReference.child("badgeCount").child(userId).observe(.value) { [weak self](snapshot) in
            
            guard let _self = self else { return }
            if let badgeCount = snapshot.value as? Int{
                if badgeCount > 0{
                    _self.tabBar.items?[1].badgeValue = "\(badgeCount)"
                    UIApplication.shared.applicationIconBadgeNumber = badgeCount
                    _self.myBadgeCount = badgeCount
                }else{
                    _self.tabBar.items?[1].badgeValue = nil
                    UIApplication.shared.applicationIconBadgeNumber = badgeCount
                    _self.myBadgeCount = 0
                    
                }
                AppUserDefaults.save(value: _self.myBadgeCount, forKey: .messageBadge)
            }else{
                print_debug("Badge not found")
            }
        }
    }
    
    private func removeBadgeObserver(){
        if let handle = self.badgeObserverHandle,let userId = User.getUserModel().user_id{
            databaseReference.child("badgeCount").child(userId).removeObserver(withHandle: handle)
        }
    }
    
}

