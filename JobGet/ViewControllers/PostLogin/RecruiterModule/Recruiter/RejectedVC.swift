//
//  RejectedVC.swift
//  JobGet
//
//  Created by macOS on 10/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

protocol RejectedJobCountHeader: class {
    
    func getRejectedJobCount(headerdata: [HeaderJobCount])
    
}
class RejectedVC: BaseVC {
    
    //MARK:- Properties
    //==================
    var page: Int = 0
    var jobID: String?
    var shouldLoadMoreData = false
    var pendingJobDetail = [PendingJobDetail]()
    var headerJobCountData = [HeaderJobCount]()
    weak var delegate: RejectedJobCountHeader?
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var rejectedTableView: UITableView!
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//MARK:- Private Methods
//=======================
private extension RejectedVC {
    
    func initialSetup() {
        
        self.rejectedTableView.delegate = self
        self.rejectedTableView.dataSource = self
        self.rejectedTableView.emptyDataSetSource = self
        self.rejectedTableView.emptyDataSetDelegate = self
        
        self.registerNib()
        // self.getRejectedJobDetail()
    }
    
    func registerNib() {
        
        let nib = UINib(nibName: "PendingJobRequestCell", bundle: nil)
        self.rejectedTableView.register(nib, forCellReuseIdentifier: "PendingJobRequestCell")
        
        
        self.rejectedTableView.register(UINib(nibName: "LoadMoreTableViewCell", bundle: nil), forCellReuseIdentifier: "LoadMoreTableViewCell")
    }
    
    func isLoadingIndexPath(_ indexPath: IndexPath) -> Bool {
        guard shouldLoadMoreData else { return false}
        
        return indexPath.row == self.pendingJobDetail.count
    }
    
    func getRejectedHeaderData() {
        
        self.delegate?.getRejectedJobCount(headerdata: self.headerJobCountData)
        
        
    }
}

//MARK: - IB Action and Target
//===============================
extension RejectedVC {
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK:- Web Services
//====================
extension RejectedVC {
    
    func getRejectedJobDetail(jobId: String, loader: Bool) {
        
        let param = ["page": self.page, "jobId": jobId, "type": GetJobDetailType.rejected.rawValue] as [String: Any]
        
        WebServices.getJobPendingDetail(parameters: param, loader: loader, success: { (json) in
            if let code = json["code"].int, code == error_codes.success {
                
                var data = [PendingJobDetail]()
                
                let details = json["data"]["userList"].arrayValue
                
                for detail in details {
                    data.append(PendingJobDetail(dict: detail))
                }
                
                let headerData = json["data"]["header"].arrayValue
                self.headerJobCountData.removeAll(keepingCapacity: false)
                headerData.forEach({ (data) in
                    self.headerJobCountData.append(HeaderJobCount(dict: data))
                })
                
                if self.shouldLoadMoreData {
                    self.pendingJobDetail.append(contentsOf: data)
                } else {
                    self.pendingJobDetail = data
                }
                
                self.page = json["data"]["page"].intValue
                self.shouldLoadMoreData = json["data"]["next"].boolValue

                self.getRejectedHeaderData()
                self.rejectedTableView.reloadData()
                
            } else {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }) { (error: Error) in
            CommonClass.showToast(msg: error.localizedDescription )
        }
    }
}

//MARK: - Table view Delegate and DataSource
//============================================
extension RejectedVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = self.pendingJobDetail.count
        return shouldLoadMoreData ? count + 1 : count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isLoadingIndexPath(indexPath){
            guard let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.defaultReuseIdentifier, for: indexPath) as? LoadMoreTableViewCell else {
                fatalError("Failed to initialize LoadMoreTableViewCell")
            }
            
            return cell
        }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PendingJobRequestCell") as? PendingJobRequestCell else { fatalError("invalid cell \(self)")
        }
        cell.populateData(data: self.pendingJobDetail[indexPath.row])
        cell.statusButton.isHidden = true
        cell.statusLabel.isHidden = true
        cell.timerButton.isHidden = true
        cell.expiredImageView.image = #imageLiteral(resourceName: "rejected")
        cell.expiredImageView.contentMode = .scaleAspectFill
        cell.categoryId = self.pendingJobDetail[indexPath.row].jobData.categoryId
        
        cell.containerView.backgroundColor = AppColors.cellCancel
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard self.shouldLoadMoreData else { return }
        
        if indexPath.row == self.pendingJobDetail.count - 1 && self.shouldLoadMoreData {  //for 2nd last cell
            self.getRejectedJobDetail(jobId: self.jobID ?? "", loader: false)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
}

//MARK:- DZNEmptyDataSource and Delegate
//==========================================
extension RejectedVC: DZNEmptyDataSetDelegate, DZNEmptyDataSetSource {
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return #imageLiteral(resourceName: "icHomeNoJobs")
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: StringConstants.No_Rejected_Candidates,                                                                           attributes: [NSAttributedStringKey.foregroundColor: AppColors.black46,
                                                                                                                                                                         NSAttributedStringKey.font: AppFonts.Poppins_Bold.withSize(14)
            ])
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        return NSAttributedString(string: "",                                                                           attributes: [NSAttributedStringKey.foregroundColor: AppColors.gray152,
                                                                                                                                     NSAttributedStringKey.font: AppFonts.Poppins_Regular.withSize(12)                                                                                                                                                                                                            ])
    }
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}

