//
//  ShortListedVC.swift
//  JobGet
//
//  Created by macOS on 10/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

protocol ShortListedJobCountHeader: class {
    func getShortListedJobCount(headerdata: [HeaderJobCount])
}

protocol OpenRejectedPopup: class {
    
    func shouldRejectedPopup(index: IndexPath)
}

protocol OpenDetail: class {
    func openRecruiterCandidateDetailVC(shortlistedUserId: String, categoryId: String, jobApplyId: String)
}

class ShortListedVC: BaseVC {
    
    //MARK:- Properties
    //==================
    var page: Int = 0
    var shouldLoadMoreData = true
    var pendingJobDetail = [PendingJobDetail]()
    var headerJobCountData = [HeaderJobCount]()
    weak var delegate: ShortListedJobCountHeader?
    weak var rejectedDelegate: OpenRejectedPopup?
    weak var openDetailDelegate: OpenDetail?
    var cancelJobStatus = 0
    var jobID: String?
    var rejectedIndex : IndexPath?
    
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var shortListedTableView: UITableView!
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        // self.getShortlistedJobDetail(jobId: self.jobID, loader: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func removeRejectedJob(applyJobId: String) {
        self.pendingJobDetail.forEach { (pendingJob) in
            if pendingJob.jobAppliedId == applyJobId {
                self.pendingJobDetail.removeObject(pendingJob)
            }
        }
        self.shortListedTableView.reloadData()
    }
}

//MARK:- Private Methods
//=======================
private extension ShortListedVC {
    
    func initialSetup() {
        
        self.shortListedTableView.delegate = self
        self.shortListedTableView.dataSource = self
        self.shortListedTableView.emptyDataSetSource = self
        self.shortListedTableView.emptyDataSetDelegate = self
        
        self.registerNib()
        
        self.shortListedTableView.enablePullToRefresh(tintColor: AppColors.themeBlueColor ,target: self, selector: #selector(refreshWhenPull(_:)))
    }
    
    func registerNib() {
        
        let nib = UINib(nibName: "PendingJobRequestCell", bundle: nil)
        self.shortListedTableView.register(nib, forCellReuseIdentifier: "PendingJobRequestCell")
        
        self.shortListedTableView.register(UINib(nibName: "LoadMoreTableViewCell", bundle: nil), forCellReuseIdentifier: "LoadMoreTableViewCell")
    }
    
    func isLoadingIndexPath(_ indexPath: IndexPath) -> Bool {
        guard shouldLoadMoreData else { return false}
        
        return indexPath.row == self.pendingJobDetail.count
    }
    
    func getSortListedHeaderData() {
        
        self.delegate?.getShortListedJobCount(headerdata: self.headerJobCountData)
        
    }
}

//MARK: - IB Action and Target
//===============================
extension ShortListedVC {
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func refreshWhenPull(_ sender: UIRefreshControl) {
        sender.endRefreshing()
        self.page = 0
        self.shouldLoadMoreData = true
        self.getShortlistedJobDetail(jobId: self.jobID ?? "", loader: false)
    }
    
    @objc func rejectBtnTapped(sender: UIButton) {
        
        guard let index = sender.tableViewIndexPath(self.shortListedTableView) else { return }
        self.rejectedDelegate?.shouldRejectedPopup(index: index)
        
    }
    
    @objc private func messageButtonTapped(_ sender: UIButton){
        
        if self.cancelJobStatus == 3 {
            return
        }
        
        guard let row = sender.tableViewIndexPath(self.shortListedTableView)?.row else { return  }
        
        let userId = self.pendingJobDetail[row].userData.userID
        
        ChatHelper.getUserDetails(userId: userId, completion: { (member) in
            
            if let member = member{
                DispatchQueue.main.async {
                    let chatVc = PersonalChatVC.instantiate(fromAppStoryboard: .Candidate)
                    chatVc.chatMember = member
                    chatVc.fromPendingJobVC = true
                    chatVc.fromShortlisted = true
                    //                let navigation = UINavigationController(rootViewController: chatVc)
                    //                navigation.isNavigationBarHidden = true
                    
                    sharedAppDelegate.navigationController?.pushViewController(chatVc, animated: true)

                }
//                navigation.navigationController?.pushViewController(chatVc, animated: false)

            }else{
//                _self.showAlert(msg: StringConstants.Recruiter_not_available)
            }
        })
    }
    
}



//MARK:- Web Services
//====================
extension ShortListedVC {
    
    func getShortlistedJobDetail(jobId: String, loader: Bool) {
        
        self.jobID = jobId
        let param = ["page": self.page , "jobId": jobId, "type": GetJobDetailType.shortListed.rawValue] as [String: Any]
        
        WebServices.getJobPendingDetail(parameters: param, loader: loader, success: { (json) in
            var data = [PendingJobDetail]()
            if let code = json["code"].int, code == error_codes.success {
                
                let details = json["data"]["userList"].arrayValue
                
                if self.page == 0 {
                    self.pendingJobDetail.removeAll(keepingCapacity: false)
                }
                
                for detail in details {
                    data.append(PendingJobDetail(dict: detail))
                }
                self.headerJobCountData.removeAll(keepingCapacity: false)
                let headerData = json["data"]["header"].arrayValue
                
                headerData.forEach({ (data) in
                    self.headerJobCountData.append(HeaderJobCount(dict: data))
                })
                
                if self.shouldLoadMoreData {
                    self.pendingJobDetail.append(contentsOf: data)
                } else {
                    self.pendingJobDetail = data
                }
                
                self.page = json["data"]["page"].intValue
                self.shouldLoadMoreData = json["data"]["next"].boolValue

                self.getSortListedHeaderData()
                self.shortListedTableView.reloadData()
            } else {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }) { (error: Error) in
            CommonClass.showToast(msg: error.localizedDescription )
        }
        
    }
    
    func jobChangeStatus(rejectedType jobType: Int, applyJobId: String, loader: Bool) {
        
        let param = [ApiKeys.type.rawValue: jobType, ApiKeys.applyId.rawValue: applyJobId, ApiKeys.jobId.rawValue: self.jobID ?? ""] as [String: Any]
        WebServices.changePendingJobStatus(parameters: param, loader: loader, success: { (json) in
            if let code = json["code"].int, code == error_codes.success {
                self.getShortlistedJobDetail(jobId: self.jobID ?? "", loader: false)
            } else {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }) { (error) in
            CommonClass.showToast(msg: error.localizedDescription )
        }
    }
    
}

//MARK: - Table view Delegate and DataSource
//============================================
extension ShortListedVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = self.pendingJobDetail.count
        return shouldLoadMoreData ? count + 1 : count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isLoadingIndexPath(indexPath){
            guard let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.defaultReuseIdentifier, for: indexPath) as? LoadMoreTableViewCell else {
                fatalError("Failed to initialize LoadMoreTableViewCell")
            }
            
            return cell
        }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PendingJobRequestCell") as? PendingJobRequestCell else { fatalError("invalid cell \(self)")
        }
        cell.statusLabel.text = StringConstants.MESSAGE
        cell.statusButton.setImage(#imageLiteral(resourceName: "icHomeSelectChat"), for: .normal)
        cell.shortlistedGreenTick.isHidden = true
        cell.timerButton.isHidden = true
        cell.rejectButton.isHidden = false
        //OPEN CHAT on both btn tap
        cell.statusButton.addTarget(self, action: #selector(self.messageButtonTapped(_:)), for: .touchUpInside)
        cell.rejectButton.addTarget(self, action: #selector(self.messageButtonTapped(_:)), for: .touchUpInside)
        
        cell.expiredImageView.isHidden = true
        
        cell.categoryId = self.pendingJobDetail[indexPath.row].jobData.categoryId
        cell.populateData(data: self.pendingJobDetail[indexPath.row],true)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard self.shouldLoadMoreData else { return }
        if indexPath.row == self.pendingJobDetail.count - 1 && self.shouldLoadMoreData {  //for 2nd last cell
            self.getShortlistedJobDetail(jobId: self.jobID ?? "", loader: false)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if cancelJobStatus == 3 {
            return
        }
        self.openDetailDelegate?.openRecruiterCandidateDetailVC(shortlistedUserId: self.pendingJobDetail[indexPath.row].userData.userID, categoryId: self.pendingJobDetail[indexPath.row].jobData.categoryId, jobApplyId: self.pendingJobDetail[indexPath.row].jobAppliedId)
    }
}

//MARK:- DZNEmptyDataSource and Delegate
//==========================================
extension ShortListedVC: DZNEmptyDataSetDelegate, DZNEmptyDataSetSource {
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return #imageLiteral(resourceName: "icHomeNoJobs")
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: StringConstants.No_Shortlisted_Candidates,                                                                           attributes: [NSAttributedStringKey.foregroundColor: AppColors.black46,NSAttributedStringKey.font: AppFonts.Poppins_Bold.withSize(14)])
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: "",attributes: [NSAttributedStringKey.foregroundColor: AppColors.gray152,
       NSAttributedStringKey.font: AppFonts.Poppins_Regular.withSize(12)])
    }
    
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}

