//
//  SortListedStatusPopupVC.swift
//  JobGet
//
//  Created by macOS on 20/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit


protocol ShortListedApplicantsPopup: class {
    
    func onTapAffermation(index: Int, applyJobId: String, categoryId: String)
    
    func onTapCancel()
}

enum SortListedPopupType{
    
    case status
    case selectJob
    case fromChat
    case none
}

class SortListedStatusPopupVC: BaseVC {
    
    
    //MARK:- Properties
    //================
    weak var delegate: ShortListedApplicantsPopup?
    var selectedIndex = -1
    var jobApplyId = ""
    var categoryId = ""
    var sortListedPopupType:SortListedPopupType = .none
    var jobInfo = [CandidateAppliedJobInfo]()
    var newJobInfo = [CandidateAppliedJobInfo]()
    
    //MARK:- IBoutlets
    //================
    
    @IBOutlet weak var searchBarHight: NSLayoutConstraint!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var sortListedTableView: UITableView!
    @IBOutlet weak var descriptionLabel   : UILabel!
    @IBOutlet weak var doneButton         : UIButton!
    @IBOutlet weak var cancelButton       : UIButton!
    @IBOutlet weak var popupView          : UIView!
    
    @IBOutlet weak var shortListSearchBar: UISearchBar!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 0.33, delay: 0.0, usingSpringWithDamping: 0.9 , initialSpringVelocity: 0.4, options: UIViewAnimationOptions(), animations: {
            
            self.popupView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }){ (true) in
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- remove popup when touch otuside of popup
    //================================================
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
         self.view.endEditing(true)
        if self.sortListedPopupType != .fromChat{
            let touch = touches.first!
            let point = touch.location(in: self.view) //touch.location(in: self.view)
            
            if !self.popupView.frame.contains(point) {
                self.dismissPopupViewOnTouches()
            }
        }
    }
    
    func initialSetup() {
        
        if self.sortListedPopupType == .status{
//            self.descriptionLabelHeight.isActive = false
            self.tableViewHeight.isActive = true
            self.statusLabel.text = StringConstants.PENDING_ACTION
              self.descriptionLabel.text = StringConstants.Please_select_an_option
            self.searchBarHight.constant = 0
            self.shortListSearchBar.isHidden = true
            //StringConstants.Status.localized
        }else if self.sortListedPopupType == .selectJob{
            self.tableViewHeight.constant = 130
            self.descriptionLabel.text = ""
            self.statusLabel.text = StringConstants.Select_Job.localized.uppercased()
        }else if self.sortListedPopupType == .fromChat{
            //            self.descriptionLabelHeight.isActive = false
            self.tableViewHeight.isActive = true
            self.statusLabel.text = StringConstants.Please_Shortlist_Candidate
            self.descriptionLabel.text = StringConstants.Shortlist_candidate_Before_call
            //StringConstants.Status.localized
        }
        self.newJobInfo = self.jobInfo
        self.statusLabel.textColor = AppColors.black26
        self.descriptionLabel.textColor = AppColors.gray152
        self.doneButton.setCorner(cornerRadius: 5, clip: true)
        self.doneButton.backgroundColor = AppColors.themeBlueColor
        self.cancelButton.setCorner(cornerRadius: 5, clip: true)
        self.cancelButton.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.cancelButton.setTitleColor(AppColors.themeBlueColor, for: .normal)
        self.cancelButton.borderWidth = 1
        self.cancelButton.borderColor = AppColors.themeBlueColor
        self.popupView.setCorner(cornerRadius: 5, clip: true)
        self.popupView.transform = CGAffineTransform(scaleX: 0.000001, y: 0.000001)
        
        self.sortListedTableView.delegate  = self
        self.sortListedTableView.dataSource = self
        self.shortListSearchBar.delegate = self
        self.searchBarHight.constant = 0
        self.shortListSearchBar.isHidden = true
        self.customiseSearchBar()
    }
    
    func dismissPopupViewOnTouches() {
        
        UIView.animate(withDuration: 0.33, delay: 0.0, usingSpringWithDamping: 0.9 , initialSpringVelocity: 0.4, options: UIViewAnimationOptions(), animations: {
            
            self.popupView.transform = CGAffineTransform( scaleX: 0.0000001,y: 0.0000001)
        }){ (true) in
            self.view.removeFromSuperview()
            self.removeFromParent
        }
    }
    
    private func customiseSearchBar(){
        
        self.shortListSearchBar.placeholder = StringConstants.Search
        self.shortListSearchBar.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        
        // TextField Color Customization
        let textFieldInsideSearchBar = self.shortListSearchBar.value(forKey: "searchField") as? UITextField
        
        textFieldInsideSearchBar?.borderStyle = .roundedRect
        textFieldInsideSearchBar?.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1.0)
        
        textFieldInsideSearchBar?.textAlignment = .center
        
        // Placeholder Customization
        if let textFieldInsideSearchBarLabel = textFieldInsideSearchBar?.value(forKey: "placeholderLabel") as? UILabel{
            textFieldInsideSearchBarLabel.textColor = UIColor(red: 152/255, green: 152/255, blue: 152/255, alpha: 1.0)
            textFieldInsideSearchBarLabel.textAlignment = .center
            textFieldInsideSearchBarLabel.font = AppFonts.Poppins_Light.withSize(15)
        }
        
        // Glass Icon Customization
        let glassIconView = textFieldInsideSearchBar?.leftView as? UIImageView
        glassIconView?.image = glassIconView?.image?.withRenderingMode(.alwaysTemplate)
        glassIconView?.tintColor = UIColor(red: 152/255, green: 152/255, blue: 152/255, alpha: 1.0)
        
    }
    
    //MARK:- IBActions
    //====================
    @IBAction func doneButtonTapped(_ sender: UIButton) {
        if self.selectedIndex == -1 {
            CommonClass.showToast(msg: StringConstants.Please_select_an_option)
        } else {
            self.delegate?.onTapAffermation(index: self.selectedIndex, applyJobId: self.jobApplyId, categoryId: self.categoryId)
            self.dismissPopupViewOnTouches()
        }
    }
    
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        self.delegate?.onTapCancel()
        self.dismissPopupViewOnTouches()
    }
}

//MARK: - Table view Delegate and DataSource
//============================================
extension SortListedStatusPopupVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.sortListedPopupType == .selectJob ||  self.sortListedPopupType == .fromChat{
            return jobInfo.count
        }else{
            return 2
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SortListedCell") as? SortListedCell else { fatalError("invalid cell \(self)")
        }
        
        if self.sortListedPopupType == .selectJob  ||  self.sortListedPopupType == .fromChat{
            cell.sortListedLabel.text = self.jobInfo[indexPath.row].jobTitle
        }else{
            switch indexPath.row {
            case 0:
                cell.sortListedLabel.text = StringConstants.Shortlist
                cell.seperatorView.isHidden = false
            case 1:
                cell.sortListedLabel.text = StringConstants.Reject
                cell.seperatorView.isHidden = true
            default:
                fatalError("invalid cell \(self)")
            }
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let cell = tableView.cellForRow(at: indexPath) as? SortListedCell else { return }
        self.selectedIndex = indexPath.row
        cell.sortListedButton.isSelected = true
        if self.sortListedPopupType == .selectJob{
            self.jobApplyId = "\(self.jobInfo[indexPath.row].jobId)"
            
        }
        if self.sortListedPopupType == .fromChat{
            self.jobApplyId = "\(self.jobInfo[indexPath.row].jobId)"
            self.categoryId = self.jobInfo[indexPath.row].categoryId
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? SortListedCell else { return }
        cell.sortListedButton.isSelected = false
        if self.sortListedPopupType == .selectJob {
            self.jobApplyId = ""
        }
        
        if self.sortListedPopupType == .fromChat{
            self.jobApplyId = ""
            self.categoryId = ""
        }
    }
}

//    MARK:- Search Delegate
//    ===========================================
extension SortListedStatusPopupVC: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        guard let searchText = searchBar.text else { return }
        self.jobInfo = self.newJobInfo
       
        var matchList: [CandidateAppliedJobInfo] = []

        matchList =  self.jobInfo.filter{
            return $0.jobTitle.range(of: searchText, options: .caseInsensitive) != nil
        }
        

        self.jobInfo = matchList
        if searchText.isEmpty{
            self.jobInfo = self.newJobInfo
        }
        print_debug(self.jobInfo)
        print_debug(self.jobInfo.count)
        UIView.setAnimationsEnabled(false)
        self.sortListedTableView.isScrollEnabled = true
        self.sortListedTableView.alwaysBounceVertical = true
        self.sortListedTableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
}


//MARK:- Prototype Cell
//=========================
class SortListedCell: UITableViewCell {
    
    //MARK:- Properties
    //==================
    @IBOutlet weak var sortListedLabel: UILabel!
    @IBOutlet weak var sortListedButton: UIButton!
    @IBOutlet weak var seperatorView: UIView!
    
    //MARK:- IBoutlets
    //================
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.sortListedButton.isUserInteractionEnabled = false
        self.sortListedLabel.textColor = AppColors.black46
    }
}


