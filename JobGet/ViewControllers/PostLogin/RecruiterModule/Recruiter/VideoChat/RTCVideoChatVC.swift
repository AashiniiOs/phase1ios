//
//  CallVC.swift
//  de
//
//  Created by  on 01/09/17.
//  Copyright © 2017. All rights reserved.
//
import TwilioVideo
import SwiftyJSON
import UIKit
import AVFoundation
import Starscream
import Reachability

// MARK: Global Enum
enum SocketDataType: String {
    
    case login = "login"
    case leave = "leave"
    case decline = "decline"
    
}

class RTCVideoChatVC: BaseVC {
    
    var caller: Caller!
    
    var isZoom = false //used for double tap remote view
    
    var localVideoSize: CGSize?
    var remoteVideoSize: CGSize?
    var accessToken = ""
    
    var fromPush = false
    
    var timer = Timer()
    var timerIsOn = false
    var totalTimeInSec = 00
    var totalTime10min = 60
    var apiTotalTimeInSec = 00
    var timeDuration: Int?
    let reachability = Reachability()!
    let session = AVAudioSession.sharedInstance()
    
    // Video SDK components
    var room: TVIRoom?
    var camera: TVICameraCapturer?
    var localVideoTrack: TVILocalVideoTrack?
    var localAudioTrack: TVILocalAudioTrack?
    var remoteParticipant: TVIRemoteParticipant?
    var audio = TVIDefaultAudioDevice()
    var ringTonePlayer : AVAudioPlayer?
    var callerType = ""
    var isCallRecieved = false
    var isBusy = false
    var isRejectedByCan = false
    var isOpenFromCallKit = false
    
    //Views, Labels, and Buttons Outlets
    
    @IBOutlet weak var callDisconnectViewHight: NSLayoutConstraint!
    @IBOutlet weak var timer10minLabel: UILabel!
    @IBOutlet weak var callDisconnectView: UIView!
    @IBOutlet weak var localVideoView: TVIVideoView!
    @IBOutlet weak var connectedUserNameLabel: UILabel!
    @IBOutlet weak var timeDurationLabel: UILabel!
    
    @IBOutlet weak var remoteVideoView: TVIVideoView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var buttonContainerView: UIView!
    @IBOutlet weak var audioButton: UIButton!
    @IBOutlet weak var videoButton: UIButton!
    @IBOutlet weak var hangupButton: UIButton!
    @IBOutlet weak var speakerButton: UIButton!
    @IBOutlet weak var switchCameraButton: UIButton!
    
    @IBOutlet weak var initialCallView: UIView!
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var acceptCall: UIButton!
    
    @IBOutlet weak var ConnectingLabel: UILabel!
    @IBOutlet weak var rejectCall: UIButton!
    //Auto Layout Constraints used for animations
    @IBOutlet weak var localViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var localViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var localViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var localViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var footerViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonContainerViewLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var messageLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.speakerButton.setBackgroundImage(#imageLiteral(resourceName: "icMuteSpeaker"), for: .selected)
        self.speakerButton.setBackgroundImage(#imageLiteral(resourceName: "icSpeaker"), for: .normal)

        self.connectedUserNameLabel.isHidden = true
        self.connectedUserNameLabel.text = ""
        self.timeDurationLabel.text = ""
        self.timeDurationLabel.isHidden = true
        localVideoView.delegate = self
        self.userImage.roundCorners()
        self.userImage.borderColor = AppColors.white
        self.userImage.borderWidth = 1.0
        self.userImage.contentMode = .scaleAspectFill
        
        if !sharedAppDelegate.socket.isConnected{
            sharedAppDelegate.socket.connect()
        }
        
        self.callDisconnectViewHight.constant = 0
        self.callDisconnectView.isHidden = true
        self.showInitialVideo(false)

        if fromPush {
            if self.isOpenFromCallKit{
//                self.acceptVideoCall()
                self.initialCallView.alpha = 0

                startPreview(view: self.localVideoView)
                self.buttonContainerView.isHidden = false
                self.localVideoView.isHidden = false
                self.userName.text = caller.sender_name
                self.connectedUserNameLabel.text = caller.sender_name
                self.userImage.sd_setImage(with: URL(string: caller.senderImage), placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), options: [], completed: nil)
                connect()

                self.isOpenFromCallKit = false
            }else{
                self.initialCallView.alpha = 0.7
                startPreview(view: self.remoteVideoView)
                self.buttonContainerView.isHidden = true
                self.localVideoView.isHidden = true
                self.userName.text = caller.sender_name
                self.connectedUserNameLabel.text = caller.sender_name
                self.userImage.sd_setImage(with: URL(string: caller.senderImage), placeholderImage: #imageLiteral(resourceName: "ic_placeholder_33"), options: [], completed: nil)
                self.ringtoneType()

            }
        }else{
            self.initialCallView.alpha = 0
            self.buttonContainerView.isHidden = false
            self.localVideoView.isHidden = false
            startPreview(view: self.localVideoView)
            self.startTimer()
            self.ringtoneType()

            connect()
            
        }

        DispatchQueue.main.async {[weak self] in
            guard let strongSelf = self else{return}
            strongSelf.checkCameraAccess()
            strongSelf.checkAudioPermission()
        }
        let loginJson = ["type": SocketDataType.login.rawValue, "name": User.getUserModel().user_id ?? ""]
        CommonClass.writeToSocket(loginJson)
        
        sharedAppDelegate.socket.delegate = self
        addGestures()
        addOrientationObserver()
        
        //        audioButton.layer.cornerRadius = 20.0
        hangupButton.layer.cornerRadius = 20.0
        //        speakerButton.layer.cornerRadius = 20.0
        acceptCall.layer.cornerRadius = acceptCall.height/2
        rejectCall.layer.cornerRadius = rejectCall.height/2
        NotificationCenter.default.addObserver(self, selector: #selector(headphonesPlugged(_:)), name: NSNotification.Name.AVAudioSessionRouteChange, object: nil)
        
        if !areHeadphonesPluggedIn(){
            DispatchQueue.main.async {
                self.speakerButton.isSelected = true
            }
            try? session.overrideOutputAudioPort(AVAudioSessionPortOverride.none)
        }else{
            DispatchQueue.main.async {
                self.speakerButton.isSelected = false
            }
            try? session.overrideOutputAudioPort(AVAudioSessionPortOverride.none)
            
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        if let _ = self.remoteVideoView{
            self.remoteVideoView.contentMode = .scaleAspectFill
            self.localVideoView.contentMode = .scaleAspectFill
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
        self.isRejectedByCan = false
        navigationController?.setNavigationBarHidden(true, animated: true)
        if !fromPush{
            localViewBottomConstraint?.constant = 0.0
            //        localViewRightConstraint?.constant = 0.0
            localViewHeightConstraint?.constant = view.frame.size.height
            localViewWidthConstraint?.constant = view.frame.size.width
            footerViewBottomConstraint?.constant = 0.0

        }
        
        reachability.whenUnreachable = { _ in
            CommonClass.showToast(msg: "Please check your internet connection")
        }
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self)
        //        if fromPush{
        self.ringTonePlayer?.stop()
        AudioServicesRemoveSystemSoundCompletion(1151)
        self.room?.disconnect()
        reachability.stopNotifier()
        //        }
    }
    
    override var shouldAutorotate: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .allButUpsideDown
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @objc func headphonesPlugged(_ notification : Notification){
        
        guard
            let userInfo = notification.userInfo,
            let reasonRaw = userInfo[AVAudioSessionRouteChangeReasonKey] as? NSNumber,
            let reason = AVAudioSessionRouteChangeReason(rawValue: reasonRaw.uintValue)
            else { fatalError("Strange... could not get routeChange") }
        switch reason {
        case .oldDeviceUnavailable:
            try? session.overrideOutputAudioPort(AVAudioSessionPortOverride.none)
            DispatchQueue.main.async {
                self.speakerButton.isSelected = false
                print("oldDeviceUnavailable")
            }
            break
            
        case .newDeviceAvailable:
            try? session.overrideOutputAudioPort(AVAudioSessionPortOverride.none)
            DispatchQueue.main.async {
                self.speakerButton.isSelected = true
                print("headset/line plugged in")
                
            }
            break
            
        case .routeConfigurationChange:
            try? session.overrideOutputAudioPort(AVAudioSessionPortOverride.none)
            DispatchQueue.main.async {
                self.speakerButton.isSelected = false
                
                print("headset pulled out")
            }
            break
            
        case .categoryChange:
            break
            
        default:
            break
            
            
        }
    }
    func areHeadphonesPluggedIn()->Bool
    {
        let availableOutputs = session.currentRoute.outputs
        for portDescription in availableOutputs
        {
            print_debug("Port Type : \(portDescription.portType)")
            if portDescription.portType == AVAudioSessionPortBuiltInSpeaker
            {
                print_debug(portDescription.portType)
                return false
            }
        }
        return true
    }
    
    func ringtoneType(){
        var tonePath : String?
        if self.fromPush{
            tonePath = Bundle.main.path(forResource: "ringtone", ofType: "mp3")
        }else{
            tonePath = Bundle.main.path(forResource: "waiting_tone", ofType: "wav")
        }
        
        let toneURL = URL(fileURLWithPath: tonePath!)
        self.ringTonePlayer = try? AVAudioPlayer(contentsOf: toneURL)
        self.ringTonePlayer?.prepareToPlay()
        self.ringTonePlayer?.numberOfLoops = -1
        self.ringTonePlayer?.play()
        
        do {
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSessionPortOverride.speaker)
        } catch _ {
        }
    }
    
    func addGestures() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(toggleButtonContainer))
        tapGestureRecognizer.numberOfTapsRequired = 1
        
        let zoomGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(zoomRemote))
        zoomGestureRecognizer.numberOfTapsRequired = 2
        
        view.addGestureRecognizer(tapGestureRecognizer)
        view.addGestureRecognizer(zoomGestureRecognizer)
    }
    
    func addOrientationObserver() {
        _ = Notification.Name(rawValue: "UIDeviceOrientationDidChangeNotification")
    }
    
    func showInitialVideo(_ isHide: Bool){
        
        if !isHide{
            self.localViewWidthConstraint.constant = self.view.frame.width
            self.localViewHeightConstraint.constant = self.view.frame.height
            //            self.localViewTopConstraint.constant = 0
            
        }else{
            self.localViewWidthConstraint.constant = 120
            self.localViewHeightConstraint.constant = 150
            //            self.localViewTopConstraint.constant = 20
        }
    }
    
    func connect() {
        // Configure access token either from server or manually.
        // If the default wasn't changed, try fetching from server.
        if fromPush{
            accessToken = caller.twilioToken
        }else{
            accessToken = caller.senderTwilioToken
        }
        
        //        self.audio = TVIDefaultAudioDevice(block: {
        //            kDefaultAVAudioSessionConfigurationBlock()
        //        })
        //        audio.block()
        
        // Prepare local media which we will share with Room Participants.
        self.prepareLocalMedia()
        
        // Preparing the connect options with the access token that we fetched (or hardcoded).
        let connectOptions = TVIConnectOptions.init(token: accessToken) { (builder) in
            
            // Use the local media that we prepared earlier.
            builder.audioTracks = self.localAudioTrack != nil ? [self.localAudioTrack!] : [TVILocalAudioTrack]()
            builder.videoTracks = self.localVideoTrack != nil ? [self.localVideoTrack!] : [TVILocalVideoTrack]()
            
            // Use the preferred audio codec
            if let preferredAudioCodec = Settings.shared.audioCodec {
                builder.preferredAudioCodecs = [preferredAudioCodec]
            }
            
            // Use the preferred video codec
            if let preferredVideoCodec = Settings.shared.videoCodec {
                builder.preferredVideoCodecs = [preferredVideoCodec]
            }
            
            // Use the preferred encoding parameters
            if let encodingParameters = Settings.shared.getEncodingParameters() {
                builder.encodingParameters = encodingParameters
            }
            
            // The name of the Room where the Client will attempt to connect to. Please note that if you pass an empty
            // Room `name`, the Client will create one for you. You can get the name or sid from any connected Room.
            builder.roomName = self.caller.loginId
        }
        
        // Connect to the Room using the options we provided.
        room = TwilioVideo.connect(with: connectOptions, delegate: self)
        
        print_debug("Attempting to connect to room \(String(describing: self.caller.loginId))")
        
    }
    func logMessage(messageText: String) {
        //        self.messageLabel.text = messageText
        print_debug(messageText)
    }
    
    func prepareLocalMedia() {
        
        // We will share local audio and video when we connect to the Room.
        //        TwilioVideo.audioDevice = audio
        
        // Create an audio track.
        if (localAudioTrack == nil) {
            localAudioTrack = TVILocalAudioTrack.init(options: nil, enabled: true, name: "Microphone")
            
            if (localAudioTrack == nil) {
                print_debug("Failed to create audio track")
            }
        }
        
        // Create a video track which captures from the camera.
        if (localVideoTrack == nil) {
            //             startPreview(view: self.videoView)
            self.startPreview(view: self.localVideoView)
            
        }
    }
    
    // MARK: Private
    func startPreview(view : TVIVideoView) {
        if PlatformUtils.isSimulator {
            return
        }
        
        // Preview our local camera track in the local video preview view.
        camera = TVICameraCapturer(source: .frontCamera, delegate: self)
        localVideoTrack = TVILocalVideoTrack.init(capturer: camera!, enabled: true, constraints: nil, name: StringConstants.Camera)
        if (localVideoTrack == nil) {
            logMessage(messageText: "Failed to create video track")
        } else {
            // Add renderer to video track for local preview
            localVideoTrack!.addRenderer(view)
            
            logMessage(messageText: "Video track created")
            
            // We will flip camera on tap.
            let tap = UITapGestureRecognizer(target: self, action: #selector(RTCVideoChatVC.flipCamera))
            self.localVideoView.addGestureRecognizer(tap)
        }
    }
    @objc func flipCamera() {
        if (self.camera?.source == .frontCamera) {
            self.camera?.selectSource(.backCameraWide)
            CommonClass.showToast(msg: StringConstants.Back_camera)
        } else {
            self.camera?.selectSource(.frontCamera)
            CommonClass.showToast(msg: StringConstants.Front_camera)
        }
    }
    
    func callRecord() {
        guard let userId = User.getUserModel().user_id else { return }
        let param = ["senderId": caller.sender_id, "receiverId": caller.receiver_id, "callTime": "\(self.timeDuration ?? 0)", "status": "1"] as [String: Any]
        
        WebServices.callRecordApi(parameters: param, loader: false, success: { (json) in
            switch userId{
            case self.caller.sender_id:
                ChatHelper.sendCallMessages(userId: self.caller.receiver_id, timeString: "\(self.timeDuration ?? -1)")
            case self.caller.receiver_id:
                ChatHelper.sendCallMessages(userId: self.caller.sender_id, timeString: "\(self.timeDuration ?? 0)")
            default:
                return
            }
            
        }) { (error: Error) in
            CommonClass.showToast(msg: error.localizedDescription )
        }
    }
    
    func notifyVideoCallApi(){
        
        let userData = User(json: (AppUserDefaults.value(forKey: .userData)))
        
        var params = JSONDictionary()
        params["senderName"] = userData.first_name
        params["receiverName"] = caller.receiver_name
        
        params["deviceId"] = caller.device_id
        params["deviceToken"] = DeviceDetail.deviceToken
        params["deviceType"] = caller.device_type
        params["senderDeviceToken"] = caller.device_token
        params["type"] = SocketDataType.decline.rawValue
        
        if AppUserDefaults.value(forKey: .userType).stringValue == UserType.candidate.rawValue{
            params["callerType"] =  "2"
            params["receiverId"] = userData.user_id ?? ""
            params["senderId"] = caller.sender_id
        }else if AppUserDefaults.value(forKey: .userType).stringValue == UserType.recuriter.rawValue{
            params["callerType"] =  "1"
            params["senderId"] = userData.user_id ?? ""
            params["receiverId"] = caller.receiver_id
            
        }
        params["senderImage"] = userData.imageUrl
        params["roomId"] = "\(userData.user_id ?? "")_\(caller.receiver_id)"
        
        WebServices.notifyVideoCall(parameters: params,loader: false, success: { (data) in
            print_debug("Video call notification")
        }) { (error) in
            self.showAlert(msg: error.localizedDescription)
            return
        }
    }
    
    func disconnect() {
        self.notifyVideoCallApi()
    }
    
    func remoteDisconnected() {
    }
    
    @objc func toggleButtonContainer() {
    }
    
    @objc func zoomRemote() {
        //Toggle Aspect Fill or Fit
        isZoom = !isZoom
        if self.remoteVideoSize != nil {
            //            videoView(remoteVideoView, didChangeVideoSize: remoteVideoSize)
        }
    }
    
    func cleanupRemoteParticipant() {
        if ((self.remoteParticipant) != nil) {
            if ((self.remoteParticipant?.videoTracks.count)! > 0) {
                let remoteVideoTrack = self.remoteParticipant?.remoteVideoTracks[0].remoteTrack
                remoteVideoTrack?.removeRenderer(self.remoteVideoView!)
                self.remoteVideoView?.removeFromSuperview()
                self.remoteVideoView = nil
            }
        }
        self.remoteParticipant = nil
    }
    
    func sendDisconnectToPeer() {
        let json = ["type": SocketDataType.leave.rawValue, "name": caller.loginId]
        CommonClass.writeToSocket(json)
    }
    
    // MARK: IBActions
    @IBAction func audioButtonPressed (_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if (self.localAudioTrack != nil) {
            self.localAudioTrack?.isEnabled = !(self.localAudioTrack?.isEnabled)!
            if sender.isSelected {
                CommonClass.showToast(msg: StringConstants.Audio_disabled)
                self.audioButton.setBackgroundImage(#imageLiteral(resourceName: "icMuteVoice"), for: .normal)
            }else{
                self.audioButton.setBackgroundImage(#imageLiteral(resourceName: "icVoice"), for: .normal)
                CommonClass.showToast(msg: StringConstants.Audio_enabled)
            }
        }
    }
    
    @IBAction func videoButtonPressed(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        if (self.localVideoTrack != nil) {
            self.localVideoTrack?.isEnabled = !(self.localVideoTrack?.isEnabled)!
        }
        
        if sender.isSelected {
            CommonClass.showToast(msg: StringConstants.Video_disabled)
        }else{
            CommonClass.showToast(msg: StringConstants.Video_enabled)
        }
    }
    
    @IBAction func acceptCallButtonTapped(_ sender: UIButton) {
        self.acceptVideoCall()
        //        self.startTimer()
        
    }
    
    func acceptVideoCall(){
        
        self.ringTonePlayer?.stop()
        AudioServicesRemoveSystemSoundCompletion(1151)
        self.initialCallView.alpha = 0
        self.buttonContainerView.isHidden = false
        self.connectedUserNameLabel.isHidden = false
        
        self.timeDurationLabel.isHidden = false
        self.localVideoView.isHidden = false
        startPreview(view: self.localVideoView)
        connect()
        self.fromPush = false
    }
    
    @IBAction func rejectCallButtonTapped(_ sender: UIButton) {
        self.fromPush = false
        self.ringTonePlayer?.stop()
        
        self.callerType = "2"
        let json: JSONDictionary = ["type": SocketDataType.decline.rawValue, "name": User.getUserModel().user_id ?? "" , "otherName":caller.sender_id]
        CommonClass.writeToSocket(json)
        self.isRejectedByCan = true
        
        //        if self.fromPush{
        //            self.isRejectedByCan = true
        //        }
        sharedAppDelegate.endAllCalls()
        self.callRecord()
        AudioServicesRemoveSystemSoundCompletion(1151)
        navigationController?.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func hangupButtonPressed(_ sender: UIButton) {
        //        sendDisconnectToPeer()
        //        disconnect()
        self.ringTonePlayer?.stop()
        AudioServicesRemoveSystemSoundCompletion(1151)
        self.isRejectedByCan = true
        
        self.callerType = "1"
        self.notifyVideoCallApi()
        var json = JSONDictionary()
        if AppUserDefaults.value(forKey: .userType).stringValue == UserType.candidate.rawValue{
            json = ["type": SocketDataType.decline.rawValue, "name": User.getUserModel().user_id ?? "" , "otherName":caller.sender_id]
            
        }else{
            json = ["type": SocketDataType.decline.rawValue, "name":User.getUserModel().user_id ?? "" , "otherName":caller.receiver_id]
            
        }
        
        if !self.isBusy{
            CommonClass.writeToSocket(json)
        }
        self.callRecord()
        
        sharedAppDelegate.endAllCalls()
        self.room?.disconnect()
        self.stopTimer()
        navigationController?.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func speakerButtonPressed(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        
        if sender.isSelected {
            CommonClass.showToast(msg: StringConstants.Speaker_disabled)
        }else{
            CommonClass.showToast(msg: StringConstants.Speaker_enabled)
            
        }
        self.setAudioOutputSpeaker(enabled: !sender.isSelected)
        
        //        TwilioVideo.audioDevice = audio
        
    }
    
    @IBAction func switchCameraPressed(_ sender: UIButton) {
        self.flipCamera()
    }
    
    //MARK:- Manual Speaker Enagle and Disable
    func setAudioOutputSpeaker(enabled: Bool)
    {
        var _: Error?
        try? session.setCategory(AVAudioSessionCategoryPlayAndRecord)
        try? session.setMode(AVAudioSessionModeVoiceChat)
        if enabled {
            try? session.overrideOutputAudioPort(AVAudioSessionPortOverride.speaker)
        } else {
            try? session.overrideOutputAudioPort(AVAudioSessionPortOverride.none)
        }
        try? session.setActive(true)
    }
}

// MARK: TVIRoomDelegate
extension RTCVideoChatVC : TVIRoomDelegate {
    
    
    func didConnect(to room: TVIRoom) {
        
        // At the moment, this example only supports rendering one Participant at a time.
        // self.connectedUserNameLabel.text = room.localParticipant?.identity ?? ""
        logMessage(messageText: "Connected to room \(room.name) as \(String(describing: room.localParticipant?.identity))")
        if (room.remoteParticipants.count > 0) {
            self.remoteParticipant = room.remoteParticipants[0]
            self.remoteParticipant?.delegate = self
        }
    }
    
    func room(_ room: TVIRoom, didDisconnectWithError error: Error?) {
        logMessage(messageText: "Disconncted from room \(room.name), error = \(String(describing: error))")
        
        sharedAppDelegate.endAllCalls()
        self.cleanupRemoteParticipant()
        self.room = nil
        self.stopTimer()
        //        self.callRecord()
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func room(_ room: TVIRoom, didFailToConnectWithError error: Error) {
        logMessage(messageText: "Failed to connect to room with error")
        
        self.room = nil
        self.stopTimer()
        //        self.callRecord()
        navigationController?.dismiss(animated: true, completion: nil)
    }
    
    func room(_ room: TVIRoom, participantDidConnect participant: TVIRemoteParticipant) {
        
        
        if (self.remoteParticipant == nil) {
            self.remoteParticipant = participant
            self.remoteParticipant?.delegate = self
        }
        logMessage(messageText: "Participant \(participant.identity) connected with \(participant.remoteAudioTracks.count) audio and \(participant.remoteVideoTracks.count) video tracks")
    }
    
    func room(_ room: TVIRoom, participantDidDisconnect participant: TVIRemoteParticipant) {
        
        
        if (self.remoteParticipant == participant) {
            cleanupRemoteParticipant()
        }
        logMessage(messageText: "Room \(room.name), Participant \(participant.identity) disconnected")
        //        self.stopTimer()
        //        self.callRecord()
        navigationController?.dismiss(animated: true, completion: nil)
        
    }
}

// MARK: TVIRemoteParticipantDelegate
extension RTCVideoChatVC : TVIRemoteParticipantDelegate {
    
    func remoteParticipant(_ participant: TVIRemoteParticipant,
                           publishedVideoTrack publication: TVIRemoteVideoTrackPublication) {
        
        // Remote Participant has offered to share the video Track.
        logMessage(messageText: "Participant \(participant.identity) published \(publication.trackName) video track")
    }
    
    func remoteParticipant(_ participant: TVIRemoteParticipant,
                           unpublishedVideoTrack publication: TVIRemoteVideoTrackPublication) {
        
        // Remote Participant has stopped sharing the video Track.
        
        logMessage(messageText: "Participant \(participant.identity) unpublished \(publication.trackName) video track")
    }
    
    func remoteParticipant(_ participant: TVIRemoteParticipant,
                           publishedAudioTrack publication: TVIRemoteAudioTrackPublication) {
        
        // Remote Participant has offered to share the audio Track.
        
        logMessage(messageText: "Participant \(participant.identity) published \(publication.trackName) audio track")
    }
    
    func remoteParticipant(_ participant: TVIRemoteParticipant,
                           unpublishedAudioTrack publication: TVIRemoteAudioTrackPublication) {
        
        // Remote Participant has stopped sharing the audio Track.
        
        
        logMessage(messageText: "Participant \(participant.identity) unpublished \(publication.trackName) audio track")
    }
    
    func subscribed(to videoTrack: TVIRemoteVideoTrack,
                    publication: TVIRemoteVideoTrackPublication,
                    for participant: TVIRemoteParticipant) {
        
        
        // We are subscribed to the remote Participant's audio Track. We will start receiving the
        // remote Participant's video frames now.
        
        logMessage(messageText: "Subscribed to \(publication.trackName) video track for Participant \(participant.identity)")
        self.connectedUserNameLabel.text = participant.identity
        self.showInitialVideo(true)
        self.startTimer()
        self.isCallRecieved = true
        self.ConnectingLabel.text = StringConstants.Connected
        
        self.ringTonePlayer?.stop()
        AudioServicesRemoveSystemSoundCompletion(1151)
        
        if (self.remoteParticipant == participant) {
            //                        setupRemoteVideoView()
            if let remoteView = self.remoteVideoView{
                videoTrack.addRenderer(remoteView)
            }
            
            self.connectedUserNameLabel.isHidden = false
            self.timeDurationLabel.isHidden = false
        }
    }
    
    func unsubscribed(from videoTrack: TVIRemoteVideoTrack,
                      publication: TVIRemoteVideoTrackPublication,
                      for participant: TVIRemoteParticipant) {
        
        // We are unsubscribed from the remote Participant's video Track. We will no longer receive the
        // remote Participant's video.
        
        logMessage(messageText: "Unsubscribed from \(publication.trackName) video track for Participant \(participant.identity)")
        
        if (self.remoteParticipant == participant) {
            if let rVideoView = self.remoteVideoView{
                videoTrack.removeRenderer(rVideoView)
            }
            
            //            self.remoteVideoView?.removeFromSuperview()
            //            self.remoteVideoView = nil
        }
    }
    
    func subscribed(to audioTrack: TVIRemoteAudioTrack,
                    publication: TVIRemoteAudioTrackPublication,
                    for participant: TVIRemoteParticipant) {
        
        // We are subscribed to the remote Participant's audio Track. We will start receiving the
        // remote Participant's audio now.
        self.isCallRecieved = true
        logMessage(messageText: "Subscribed to \(publication.trackName) audio track for Participant \(participant.identity)")
        self.connectedUserNameLabel.text = participant.identity
        self.showInitialVideo(true)
        
        self.startTimer()
        self.ringTonePlayer?.stop()
        AudioServicesRemoveSystemSoundCompletion(1151)
        self.ConnectingLabel.text = StringConstants.Connected
        
    }
    
    func unsubscribed(from audioTrack: TVIRemoteAudioTrack,
                      publication: TVIRemoteAudioTrackPublication,
                      for participant: TVIRemoteParticipant) {
        
        
        // We are unsubscribed from the remote Participant's audio Track. We will no longer receive the
        // remote Participant's audio.
        self.stopTimer()
        
        //        self.callRecord()
        logMessage(messageText: "Unsubscribed from \(publication.trackName) audio track for Participant \(participant.identity)")
    }
    
    func remoteParticipant(_ participant: TVIRemoteParticipant,
                           enabledVideoTrack publication: TVIRemoteVideoTrackPublication) {
        
        self.videoButton.fadeout(duration: 0.8, delay: 0.0) { (true) in
            self.videoButton.fadein()
        }
        
        CommonClass.showToast(msg: StringConstants.Video_enabled)
        logMessage(messageText: "Participant \(participant.identity) enabled \(publication.trackName) video track")
    }
    
    func remoteParticipant(_ participant: TVIRemoteParticipant,
                           disabledVideoTrack publication: TVIRemoteVideoTrackPublication) {
        
        self.videoButton.fadeout(duration: 0.8, delay: 0.0) { (true) in
            self.videoButton.fadein()
        }
        CommonClass.showToast(msg: StringConstants.Video_disabled)
        logMessage(messageText: "Participant \(participant.identity) disabled \(publication.trackName) video track")
    }
    
    func remoteParticipant(_ participant: TVIRemoteParticipant,
                           enabledAudioTrack publication: TVIRemoteAudioTrackPublication) {
        
        self.audioButton.fadeout(duration: 0.8, delay: 0.0) { (true) in
            self.audioButton.fadein()
        }
        
        CommonClass.showToast(msg: StringConstants.Audio_enabled)
        
        logMessage(messageText: "Participant \(participant.identity) enabled \(publication.trackName) audio track")
    }
    
    func remoteParticipant(_ participant: TVIRemoteParticipant,
                           disabledAudioTrack publication: TVIRemoteAudioTrackPublication) {
        
        self.audioButton.fadeout(duration: 0.8, delay: 0.0) { (true) in
            self.audioButton.fadein()
        }
        CommonClass.showToast(msg: StringConstants.Audio_disabled)
        logMessage(messageText: "Participant \(participant.identity) disabled \(publication.trackName) audio track")
    }
    
    func failedToSubscribe(toAudioTrack publication: TVIRemoteAudioTrackPublication,
                           error: Error,
                           for participant: TVIRemoteParticipant) {
        
        
        logMessage(messageText: "FailedToSubscribe \(publication.trackName) audio track, error = \(String(describing: error))")
    }
    
    func failedToSubscribe(toVideoTrack publication: TVIRemoteVideoTrackPublication,
                           error: Error,
                           for participant: TVIRemoteParticipant) {
        
        logMessage(messageText: "FailedToSubscribe \(publication.trackName) video track, error = \(String(describing: error))")
    }
}
// MARK: TVICameraCapturerDelegate
extension RTCVideoChatVC : TVICameraCapturerDelegate {
    func cameraCapturer(_ capturer: TVICameraCapturer, didStartWith source: TVICameraCaptureSource) {
        //         self.videoView.shouldMirror = (source == .frontCamera)
        self.localVideoView.shouldMirror = (source == .frontCamera)
        self.view.setNeedsLayout()
        
        //        if (!localVideoTrack.isEnabled) {
        //            localVideoTrack.isEnabled = true;
        //        }
    }
}

// MARK: TVIVideoViewDelegate
extension RTCVideoChatVC : TVIVideoViewDelegate {
    func videoView(_ view: TVIVideoView, videoDimensionsDidChange dimensions: CMVideoDimensions) {
        self.view.setNeedsLayout()
    }
}

extension RTCVideoChatVC{
    
    func startTimer() {
        
        if !self.timer.isValid{
            
            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.updateTime), userInfo: nil, repeats: true)
        }
    }
    
    @objc func updateTime() {
        
        if self.isCallRecieved{
            
            if totalTimeInSec > 1{
                let duration  = "\(self.timeFormatted(self.totalTimeInSec-1))"
                self.timeDurationLabel.text = duration
                self.timeDuration = (1000*(self.totalTimeInSec))
                
            }else{
                self.timeDurationLabel.text = "\(timeFormatted(totalTimeInSec))"
                self.timeDuration = (1000*totalTimeInSec)
            }
            
            //Call automatically disconnected after 10 mins. 600 = 10 min
            if totalTimeInSec == 540{
                let duration  = "\(self.totalTime10min)"
                self.messageLabel.text = "\(StringConstants.Your_Call_Disconnected_In) \(duration) \(StringConstants.seconds)"
                self.callDisconnectViewHight.constant = 30
                self.callDisconnectView.isHidden = false
            }else if totalTimeInSec == 600 && !self.fromPush{
                self.callRecord()
                let json: JSONDictionary = ["type": SocketDataType.decline.rawValue, "name":User.getUserModel().user_id ?? "" , "otherName":caller.receiver_id]
                
                CommonClass.writeToSocket(json)
                self.notifyVideoCallApi()
                //                }
                sharedAppDelegate.endAllCalls()
                self.room?.disconnect()
                self.stopTimer()
                navigationController?.dismiss(animated: true, completion: nil)
            }
            
            if totalTimeInSec > 540{
                let duration  = "\(self.totalTime10min)"
                self.messageLabel.text = "\(StringConstants.Your_Call_Disconnected_In) \(duration) \(StringConstants.seconds)"
                self.totalTime10min -= 1
            }
            
            totalTimeInSec += 1
        }else{
            if apiTotalTimeInSec == 45{
                self.notifyVideoCallApi()
                //                if !self.isBusy{
                self.callRecord()
                let json: JSONDictionary = ["type": SocketDataType.decline.rawValue, "name":User.getUserModel().user_id ?? "" , "otherName":caller.receiver_id]
                
                CommonClass.writeToSocket(json)
                //                }
                sharedAppDelegate.endAllCalls()
                self.room?.disconnect()
                self.stopTimer()
                navigationController?.dismiss(animated: true, completion: nil)
            }
            apiTotalTimeInSec += 1
        }
    }
    
    func stopTimer() {
        
        self.timer.invalidate()
    }
    
    func timeFormatted(_ totalSeconds: Int) -> String {
        
        let sec: Int = totalSeconds % 60
        let min: Int = (totalSeconds / 60) % 60
        return String(format: "%02d:%02d", min, sec)
    }
    
    func checkCameraAccess() {
        switch AVCaptureDevice.authorizationStatus(for: .video) {
        case .denied:
            print_debug("Denied, request permission from settings")
            self.presentCameraSettings("You need to allow camera permissions from your settings to attend video calls on Jobget")
            
            //            self.showAlert(title: "Alert!", msg: "You need to allow camera permissions from your settings to attend video calls on Jobget", {
            //                var json = JSONDictionary()
            //
            //                if (self.isCallRecieved){
            //                    json = ["type": SocketDataType.decline.rawValue, "name": User.getUserModel().user_id ?? "" , "otherName": self.caller.receiver_id]
            //
            //                }else{
            //                    json = ["type": SocketDataType.decline.rawValue, "name": User.getUserModel().user_id ?? "" , "otherName": self.caller.sender_id]
            //
            //                }
            //
            //                CommonClass.writeToSocket(json)
            //                self.fromPush = false
            //                self.isRejectedByCan = true
            //                self.ringTonePlayer?.stop()
            //                //                self.notifyVideoCallApi()
            //                sharedAppDelegate.endAllCalls()
            //                self.room?.disconnect()
            //                self.stopTimer()
            //                AudioServicesRemoveSystemSoundCompletion(1151)
            //                self.navigationController?.dismiss(animated: true, completion: nil)
            //
            //                //                    self.navigationController?.dismiss(animated: true, completion: nil)
            //                print_debug("Permission denied")
            
            //            })
            
        case .restricted:
            print_debug("Restricted, device owner must approve")
        case .authorized:
            print_debug("Authorized, proceed")
        case .notDetermined:
            AVCaptureDevice.requestAccess(for: .video) { success in
                if success {
                    print_debug("Permission granted, proceed")
                } else {
                    var json = JSONDictionary()
                    if self.fromPush{
                        
                        json = ["type": SocketDataType.decline.rawValue, "name": User.getUserModel().user_id ?? "" , "otherName": self.caller.sender_id]
                        
                    }
                    else if self.isBusy{
                        sharedAppDelegate.endAllCalls()
                        self.room?.disconnect()
                        //                        self.stopTimer()
                        self.navigationController?.dismiss(animated: true, completion: nil)
                    }
                    else
                    {
                        
                        json = ["type": SocketDataType.decline.rawValue, "name": User.getUserModel().user_id ?? "" , "otherName": self.caller.receiver_id]
                    }
                    
                    CommonClass.writeToSocket(json)
                    self.fromPush = false
                    self.ringTonePlayer?.stop()
                    self.callerType = "2"
                    self.notifyVideoCallApi()
                    AudioServicesRemoveSystemSoundCompletion(1151)
                    //                    self.navigationController?.dismiss(animated: true, completion: nil)
                    print_debug("Permission denied")
                }
            }
        }
    }
    
    func checkAudioPermission(){
        
        switch session.recordPermission() {
        case AVAudioSessionRecordPermission.granted:
            print("Permission granted")
        case AVAudioSessionRecordPermission.denied:
            self.presentCameraSettings("You need to allow microphone permissions from your settings to attend video calls on Jobget")
            
            //            self.showAlert(title: "Alert!", msg: "You need to allow microphone permissions from your settings to attend video calls on Jobget", {
            //                var json = JSONDictionary()
            //
            //                if (self.isCallRecieved){
            //                    json = ["type": SocketDataType.decline.rawValue, "name": User.getUserModel().user_id ?? "" , "otherName": self.caller.receiver_id]
            //                    self.callerType = "1"
            //
            //                }else{
            //                    json = ["type": SocketDataType.decline.rawValue, "name": User.getUserModel().user_id ?? "" , "otherName": self.caller.sender_id]
            //                    self.callerType = "2"
            //
            //                }
            //
            //                CommonClass.writeToSocket(json)
            //                self.fromPush = false
            //                self.isRejectedByCan = true
            //                self.ringTonePlayer?.stop()
            //                self.callerType = "2"
            //                self.notifyVideoCallApi()
            //                AudioServicesRemoveSystemSoundCompletion(1151)
            //                //                    self.navigationController?.dismiss(animated: true, completion: nil)
            //                print_debug("Permission denied")
            //
            //            })
            
        case AVAudioSessionRecordPermission.undetermined:
            print("Request permission here")
            session.requestRecordPermission({ (granted) in
                // Handle granted
                if !granted{
                    self.fromPush = false
                    self.ringTonePlayer?.stop()
                    self.callerType = "2"
                    var json = JSONDictionary()
                    if self.fromPush{
                        
                        json = ["type": SocketDataType.decline.rawValue, "name": User.getUserModel().user_id ?? "" , "otherName": self.caller.sender_id]
                        
                    }else{
                        json = ["type": SocketDataType.decline.rawValue, "name": User.getUserModel().user_id ?? "" , "otherName": self.caller.receiver_id]
                    }
                    CommonClass.writeToSocket(json)
                    self.notifyVideoCallApi()
                    AudioServicesRemoveSystemSoundCompletion(1151)
                    self.navigationController?.dismiss(animated: true, completion: nil)
                }
            })
        }
    }
    
    func presentCameraSettings(_ message: String) {
        let alertController = UIAlertController(title: "",
                                                message: message,
                                                preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: StringConstants.Cancel, style: .default){ _ in
            self.fromPush = false
            self.isRejectedByCan = true
            
            self.callerType = "2"
            var json = JSONDictionary()
            if self.fromPush{
                
                json = ["type": SocketDataType.decline.rawValue, "name": User.getUserModel().user_id ?? "" , "otherName": self.caller.sender_id]
                
            }else{
                json = ["type": SocketDataType.decline.rawValue, "name": User.getUserModel().user_id ?? "" , "otherName": self.caller.receiver_id]
            }
            CommonClass.writeToSocket(json)
            self.notifyVideoCallApi()
            self.ringTonePlayer?.stop()
            AudioServicesRemoveSystemSoundCompletion(1151)
            self.navigationController?.dismiss(animated: true, completion: nil)
        })
        alertController.addAction(UIAlertAction(title: StringConstants.Settings, style: .cancel) { _ in
            if let url = URL(string: UIApplicationOpenSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: { _ in
                    self.fromPush = false
                    self.isRejectedByCan = true
                    
                    self.callerType = "2"
                    var json = JSONDictionary()
                    if !self.isCallRecieved{
                        
                        json = ["type": SocketDataType.decline.rawValue, "name": User.getUserModel().user_id ?? "" , "otherName": self.caller.sender_id]
                        
                    }else{
                        json = ["type": SocketDataType.decline.rawValue, "name": User.getUserModel().user_id ?? "" , "otherName": self.caller.receiver_id]
                    }
                    CommonClass.writeToSocket(json)
                    self.notifyVideoCallApi()
                    self.isRejectedByCan = true
                    self.ringTonePlayer?.stop()
                    AudioServicesRemoveSystemSoundCompletion(1151)
                    self.navigationController?.dismiss(animated: true, completion: nil)
                })
            }
        })
        
        self.present(alertController, animated: true, completion: nil)
    }
}

// MARK: WebSocketDelegate
extension RTCVideoChatVC: WebSocketDelegate {
    func websocketDidConnect(socket: WebSocketClient) {
        print_debug(socket.isConnected)
        //        self.socketLogin()
    }
    
    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        print_debug(error?.localizedDescription)
    }
    
    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        let json = JSON(parseJSON: text)
        print_debug(text)
        if json["type"].stringValue == SocketDataType.decline.rawValue{
            
            if AppUserDefaults.value(forKey: .userType).stringValue == UserType.candidate.rawValue{
                CommonClass.showToast(msg: "\(StringConstants.Call_disconnected_by) \(caller.sender_name)")
            }else{
                
                CommonClass.showToast(msg: "\(StringConstants.Call_disconnected_by) \(caller.receiver_name)")
            }
            self.room?.disconnect()
            self.stopTimer()
            
            disconnect()
            //            self.notifyVideoCallApi()
            navigationController?.dismiss(animated: true, completion: nil)
            sharedAppDelegate.endAllCalls()
        }else if json["type"].stringValue == SocketDataType.leave.rawValue{
            showAlert(title: "", msg: "\(caller.receiver_name) \(StringConstants.Busy_message)", {
                self.disconnect()
                //            self.notifyVideoCallApi()
                self.navigationController?.dismiss(animated: true, completion: nil)
                sharedAppDelegate.endAllCalls()
            })
            //            showAlert(msg: "\(caller.receiver_name) \(StringConstants.Busy_message)")
            self.isBusy = true
        }
    }
    
    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
        print_debug(data)
    }
}
