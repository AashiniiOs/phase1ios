//
//  YourProfileStep1VC.swift
//  JobGet
//
//  Created by Abhi on 21/05/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import SwiftyJSON

class YourProfileStep1VC: UIViewController {
    
    @IBOutlet weak var yourProfileLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var stepsTableView: UITableView!
    @IBOutlet weak var logoutBtn: UIButton!
    
    
    let stepsLabel = [StringConstants.experience,StringConstants.Education,StringConstants.About_Me,StringConstants.PROFILE_PICTURE]
    var job_id = ""
    var recruiterId = ""
    var categoryId = ""
    var candidateDetail : CandidateProfileModel = CandidateProfileModel(withJSON: JSON([:]))
    
    // var fromMapView = false
    var fromTabbar = true
    var isTapBackButton = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.stepsTableView.register(UINib(nibName: "SubmitButtonCell", bundle: nil), forCellReuseIdentifier: "SubmitButtonCell")
        if fromTabbar{
            self.backButton.isHidden = true
        }else{
            self.backButton.isHidden = false
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func backButtontapped(_ sender: UIButton) {
        
        
        if self.candidateDetail.experience.isEmpty, self.candidateDetail.education.isEmpty, self.candidateDetail.about.isEmpty, self.candidateDetail.userImage.isEmpty {
            self.navigationController?.popViewController(animated: true)
        } else {
            let popUpVc = AlertPopUpVC.instantiate(fromAppStoryboard: .Candidate)
            popUpVc.delegate = self
            
            popUpVc.modalPresentationStyle = .overCurrentContext
            popUpVc.modalTransitionStyle = .crossDissolve
            self.isTapBackButton = true
            popUpVc.alertType = .logoutIncomplete
            popUpVc.isOpenFromJobApplied = true
            self.present(popUpVc, animated: true, completion: nil)
        }
    }
    
    
    
    @IBAction func logoutBtnTapped(_ sender: UIButton) {
        
        let popUpVc = AlertPopUpVC.instantiate(fromAppStoryboard: .Candidate)
        popUpVc.delegate = self
        
        popUpVc.modalPresentationStyle = .overCurrentContext
        popUpVc.modalTransitionStyle = .crossDissolve
        
        if self.candidateDetail.experience.isEmpty, self.candidateDetail.education.isEmpty, self.candidateDetail.about.isEmpty, self.candidateDetail.userImage.isEmpty {
            popUpVc.commongFromSetting = true
        } else {
            
            popUpVc.alertType = .logoutIncomplete
        }
        self.present(popUpVc, animated: true, completion: nil)
    }
    
}

extension YourProfileStep1VC: AlertPopUpDelegate {
    
    func didTapAffirmativeButton() {
        print_debug("Logout")
        //            TODO:- Logout user here
        if self.isTapBackButton{
            self.navigationController?.popViewController(animated: true)
            self.dismiss(animated: false, completion: nil)
        }else{
            if let userId = User.getUserModel().user_id {
                print_debug("Logout user_id : \(userId)")
                ChatHelper.logOutUser(userId: userId)
                CommonClass().goToLogin()
                AppUserDefaults.removeAllValues()
                UIApplication.shared.applicationIconBadgeNumber = 0
            }else{
                self.showAlert(msg: StringConstants.Cannot_logout_user)
            }
        }
    }
    
    func didTapNegativeButton() {}
}


//MARK: Table View Delegate and datasource
extension YourProfileStep1VC: UITableViewDataSource,UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        
        if indexPath.row != 4{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "StepsCell", for: indexPath) as? StepsCell else{
                fatalError("StepsCell not found")
            }
            cell.stepsLabel.text = stepsLabel[indexPath.row]
            if indexPath.row == 0{
                cell.optionalLabel.text = StringConstants.required
                cell.optionalLabel.textColor = #colorLiteral(red: 0.2389856699, green: 0.4166136601, blue: 0.6588235294, alpha: 1)
            }else{
                cell.optionalLabel.text = StringConstants.Optionals
                cell.optionalLabel.textColor = AppColors.gray163
                
            }
            return cell
        }else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SubmitButtonCell", for: indexPath) as? SubmitButtonCell else{
                fatalError("SubmitButtonCell not found")
            }
            cell.submitButton.backgroundColor = #colorLiteral(red: 0.2389856699, green: 0.4166136601, blue: 0.6607115269, alpha: 1)
            cell.submitButton.setTitle(StringConstants.next.localized, for: .normal)
            cell.submitButton.addTarget(self, action: #selector(submitButtonTapped), for: .touchUpInside)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row != 4{
            return 60
        }else{
            return 200
        }
    }
    
    @objc func submitButtonTapped(){
        let obj = ProfileSetupContainerVC.instantiate(fromAppStoryboard: .Recruiter)
        print_debug(self.candidateDetail)
        obj.delegate = self
        obj.candidateDetail = self.candidateDetail
        obj.job_id = self.job_id
        obj.recruiterId = self.recruiterId
        obj.categoryId = self.categoryId
        obj.fromTabbar = false
        self.navigationController?.pushViewController(obj, animated: true)
    }
}

//MARK: Save PreviousData
//=========================
extension YourProfileStep1VC: SaveData {
    func saveCandidateData(data: CandidateProfileModel) {
        self.candidateDetail = data
    }
    
}

class StepsCell : UITableViewCell{
    
    @IBOutlet weak var stepsView: UIView!
    @IBOutlet weak var stepsLabel: UILabel!
    @IBOutlet weak var optionalLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        stepsView.drawShadow()
        stepsView.layer.cornerRadius = 4
    }
    
}
