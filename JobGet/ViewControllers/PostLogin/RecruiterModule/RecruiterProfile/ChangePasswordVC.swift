//
//  ChangePasswordVC.swift
//  JobGet
//
//  Created by macOS on 29/06/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class ChangePasswordVC: UIViewController {
    
    //MARK:- Properties
    //==================
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var newPasswordTxtField: FloatingTextField!
    @IBOutlet weak var oldPasswordTxtField: FloatingTextField!
    @IBOutlet weak var confirmPasswordTxtField: FloatingTextField!
    //MARK:- IBoutlets
    //================
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//MARK:- Private Methods
//=======================
private extension ChangePasswordVC {
    
    func initialSetup() {
        
        self.setFontAndRadius()
        self.setTextFieldDelegate()
    }
    
    func setFontAndRadius() {
        
        CommonClass.setActiveLabelTextField(textField: self.newPasswordTxtField, placeholder: StringConstants.New_Pass, title: StringConstants.New_PassTitle)
        CommonClass.setActiveLabelTextField(textField: self.oldPasswordTxtField, placeholder: StringConstants.Old_Pass, title: StringConstants.Old_PassTitle)
        CommonClass.setActiveLabelTextField(textField: self.confirmPasswordTxtField, placeholder: StringConstants.Confirm_New_Password, title: StringConstants.Confirm_New_PasswordTitle)
        self.submitBtn.setCorner(cornerRadius: 5.0, clip: true)
        self.navigationView.setShadow()
        self.newPasswordTxtField.setIconImage(img: #imageLiteral(resourceName: "icLoginPassword"), size: CGSize(width: 30.0 , height: 30.0))
        self.oldPasswordTxtField.setIconImage(img: #imageLiteral(resourceName: "icLoginPassword"), size: CGSize(width: 30.0 , height: 30.0))
        self.confirmPasswordTxtField.setIconImage(img: #imageLiteral(resourceName: "icLoginPassword"), size: CGSize(width: 30.0 , height: 30.0))
        self.submitBtn.backgroundColor = AppColors.themeBlueColor
    }
    
    func setTextFieldDelegate() {
        self.newPasswordTxtField.isSecureText = true
        self.confirmPasswordTxtField.isSecureText = true
        self.oldPasswordTxtField.isSecureText = true
        self.newPasswordTxtField.delegate = self
        self.confirmPasswordTxtField.delegate = self
        self.oldPasswordTxtField.delegate = self
    }
    
    func validation() {
        guard let oldPassword = self.oldPasswordTxtField.text else { return }
        guard let newPassword = self.newPasswordTxtField.text else { return }
        guard let confirmPassword = self.confirmPasswordTxtField.text else { return }
        
        if !oldPassword.isEmpty, !newPassword.isEmpty, !confirmPassword.isEmpty {
            if newPassword != confirmPassword {
                CommonClass.showToast(msg: StringConstants.Confirm_Password_must_match)
            } else {
                self.changePassword(loader: true)
            }
            
        } else if oldPassword.isEmpty {
            CommonClass.showToast(msg: StringConstants.EnterOldPassword)
        } else if newPassword.isEmpty {
            CommonClass.showToast(msg: StringConstants.EnterNewPassword)
        } else if confirmPassword.isEmpty {
            CommonClass.showToast(msg: StringConstants.EnterConfirmPassword)
        }
    }
}
//MARK: - IB Action and Target
//===============================
extension ChangePasswordVC {
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitBtnTapped(_ sender: UIButton) {
        self.resignFirstResponder()
        self.validation()
    }
}

//MARK: - Web services
//======================
extension ChangePasswordVC {
    
    func changePassword(loader: Bool) {
        
        guard let oldPassword = self.oldPasswordTxtField.text else { return }
        guard let newPassword = self.newPasswordTxtField.text else { return }
        guard let confirmPassword = self.confirmPasswordTxtField.text else { return }
        let params = [ApiKeys.oldPassword.rawValue: oldPassword, ApiKeys.newPassword.rawValue: newPassword, ApiKeys.confirmPassword.rawValue : confirmPassword]
        print_debug(params)
        WebServices.changePasswordAPI(parameters: params, success: { (json) in
            if json["code"].intValue == error_codes.success {
                
                CommonClass.showToast(msg: json["message"].stringValue)
                self.navigationController?.popViewController(animated: true)
            } else {
                CommonClass.showToast(msg: json["message"].stringValue)
            }
            
        }) { (error) in
            print_debug(error)
            CommonClass.showToast(msg: error.localizedDescription)
            
        }
    }
}

extension ChangePasswordVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string == " " {
            return false
        } else {
            return true
        }
    }
    
}

