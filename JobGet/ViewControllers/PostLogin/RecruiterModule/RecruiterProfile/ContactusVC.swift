//
//  ContactusVC.swift
//  JobGet
//
//  Created by Admin on 06/09/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class ContactusVC: UIViewController {
    
    
    
    // MARK: - Outlets.
    // MARK: -
    
    @IBOutlet weak var imageCollectinView: UICollectionView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var submitBtn: UIButton!
    
    // MARK: - Properties.
    // MARK: -
    var imageArray = [UIImage]()
    var imgUrlArray = [String]()
    var isUploadEnable = false
    var lengthLimit = 1000
    
    // MARK: - View life cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.view.endEditing(true)
    }
    
    // MARK: - Actions
    // MARK: -
    
    @IBAction func backButtonTapp(_ sender: UIButton) {
        
        self.pop()
    }
    
    
    @IBAction func submitButtonTapp(_ sender: UIButton) {
        self.view.endEditing(true)
        if textView.text == StringConstants.writeFeedback{
            
            CommonClass.showToast(msg: StringConstants.Please_enter_review)
            return
        }
        
        self.contactUs(loader: true)
    }
}

// MARK: - Private methods.
// MARK: -
private extension ContactusVC{
    
    func initialSetup(){
        self.navigationView.setShadow()
        self.imageCollectinView.delegate = self
        self.imageCollectinView.dataSource = self
        self.textView.delegate = self
        self.setupTextAndColor()
    }
    
    
    func setupTextAndColor() {
        self.countLabel.font = AppFonts.Poppins_Regular.withSize(12)
        self.countLabel.textColor = AppColors.gray152
        self.textView.text = StringConstants.writeFeedback
        self.textView.textColor = AppColors.gray152
        self.textView.font = AppFonts.Poppins_Regular.withSize(14)
        self.countLabel.text = "0/\(lengthLimit)"
    }
    //
    func uploadImage(image : UIImage) {
        
        image.uploadImageToS3( success: { (uploaded : Bool, imageUrl : String) in
            if uploaded {
                self.imgUrlArray.append(imageUrl)
                print_debug(self.imgUrlArray)
                self.isUploadEnable = false
                self.imageCollectinView.reloadData()
            }
        }, progress: { (uploadPercentage : CGFloat) in
            print_debug(uploadPercentage)
            
        }, failure: { (err : Error) in
            self.isUploadEnable = false
            self.imageCollectinView.reloadData()
            if (err.localizedDescription  == StringConstants.Internet_connection_appears_offline) {
                CommonClass.showToast(msg: StringConstants.Please_check_your_internet_connection)
            }
        })
    }
}

//MARK: - Web services
//======================
extension ContactusVC {
    
    func contactUs(loader: Bool) {
        
        var text = ""
        if textView.text != StringConstants.writeFeedback{
            text = textView.text
        }
        
        let params: JSONDictionary = ["media": self.imgUrlArray, "desc":text]
        
        
        WebServices.contactUs(parameters: params, loader: loader, success: { (json) in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                let msg = json["message"].stringValue
                let sceen = ResetPasswordPopupVC.instantiate(fromAppStoryboard: .Main)
                commingFrom = .contactus
                sceen.delegate = self 
                self.view.addSubview(sceen.view)
                self.add(childViewController: sceen)
            } else {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }) { (error) in
            print_debug(error)
            CommonClass.showToast(msg: error.localizedDescription)
        }
    }
}


//MARK:-  UITextView Delegate methods.
//MARK:-
extension ContactusVC: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == AppColors.gray152 {
            textView.text = nil
            textView.textColor = AppColors.black26
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard let textViewText = textView.text , lengthLimit != 0 else { return true }
        
        let newLength = textViewText.utf16.count + text.utf16.count - range.length
        if textView.text.isEmpty, text == " " {
            return false
        } 
        if newLength <= lengthLimit {
            countLabel.text = "\(newLength)/\(lengthLimit)"
            // drhDelegate?.didReachCharacterLimit(false)
        } else {
            //  drhDelegate?.didReachCharacterLimit(true)
            UIView.animate(withDuration: 0.1, animations: {
                self.countLabel.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
                
            }, completion: { (finish) in
                UIView.animate(withDuration: 0.1, animations: {
                    self.countLabel.transform = CGAffineTransform.identity
                })
            })
        }
        
        return newLength <= lengthLimit
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = StringConstants.writeFeedback
            textView.textColor = AppColors.gray152
        }
    }
}


// MARK: - CollectionView delegate and dataSource methods.
// MARK: -
extension ContactusVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if !self.imageArray.isEmpty && self.imageArray.count < 4{
            return self.imageArray.count + 1
        }else if self.imageArray.count == 4{
            return self.imageArray.count
        }else{
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ContactusCVCell", for: indexPath) as? ContactusCVCell else {
            fatalError(" not found.")
        }
        
        if !self.imageArray.isEmpty{
            if indexPath.item == self.imageArray.count{
                cell.iconImage.image =  #imageLiteral(resourceName: "ic_camera_off_")
                cell.removeButton.isHidden = true
                cell.iconImage.isHidden = false
            }else{
                if self.isUploadEnable{
                    if indexPath.row == self.imageArray.count - 1{
                        cell.activityIndicator.startAnimating()
                    }else{
                        cell.activityIndicator.stopAnimating()
                    }
                }else{
                    cell.activityIndicator.stopAnimating()
                }
                cell.iconImage.isHidden = true
                cell.imageView.image = self.imageArray[indexPath.item]
                cell.imageView.contentMode = .scaleAspectFill
                cell.removeButton.isHidden = false
                cell.removeButton.addTarget(self, action: #selector(self.removeImageButtonTapp(_:)), for: .touchUpInside)
            }
        }else{
            cell.iconImage.image =  #imageLiteral(resourceName: "ic_camera_off_")
            cell.removeButton.isHidden = true
            cell.iconImage.isHidden = false
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.imageCollectinView.frame.height, height: self.imageCollectinView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if self.imageArray.count < 4{
            if !self.imageArray.isEmpty, indexPath.item == self.imageArray.count{
                self.captureImage(on: self)
            }else if self.imageArray.isEmpty{
                self.captureImage(on: self)
            }
        }
    }
    
    @objc func removeImageButtonTapp(_ sender: UIButton){
        guard let indexPath = sender.collectionViewIndexPath(self.imageCollectinView) else {
            return
        }
        
        if indexPath.row == imgUrlArray.count || imgUrlArray.isEmpty{
            return
        }
        self.imageArray.remove(at: indexPath.item)
        self.imgUrlArray.remove(at: indexPath.item)
        self.imageCollectinView.reloadData()
    }
}
extension ContactusVC: ShouldOpenStatusPopup{
    
    func presentShortlistedPopup(jobApplyId: String, categoryId: String) {
        self.pop()
    }
}

//MARK:- ImagePickerController & NavigationController Delegate
//============================================================
extension ContactusVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let imagePicked = info[UIImagePickerControllerOriginalImage] as? UIImage{
            self.isUploadEnable = true
            self.imageArray.append(imagePicked)
            self.imageCollectinView.reloadData()
            self.uploadImage(image: imagePicked)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
}

// MARK: - CollectionView delegate and dataSource methods.
// MARK: -
class ContactusCVCell: UICollectionViewCell{
    
    @IBOutlet weak var bgView: UIView!
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var removeButton: UIButton!
    
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: .white)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupActivityIndicator()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.removeButton.roundCorners()
    }
    
    func setupActivityIndicator() {
        
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = AppColors.themeBlueColor
        
        activityIndicator.center = CGPoint(x: imageView.frame.size.width / 2, y: imageView.frame.height/2)
        imageView.addSubview(activityIndicator)
    }
}
