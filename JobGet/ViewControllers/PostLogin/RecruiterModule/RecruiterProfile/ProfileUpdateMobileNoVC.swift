//
//  ProfileUpdateMobileNoVC.swift
//  JobGet
//
//  Created by Appinventiv on 07/06/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class ProfileUpdateMobileNoVC: UIViewController {
    
    // MARK: - Outlets.
    // MARK: -
    @IBOutlet weak var navigationLabel: UILabel!
    
    @IBOutlet weak var phoneNumberTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var phoneNumbeLabel: UILabel!
    @IBOutlet weak var proceedButton: UIButton!
    @IBOutlet weak var otpReasonLabel: UILabel!
    @IBOutlet weak var countryCodeButton: UIButton!
    
    // MARK: - Properties.
    // MARK: -
    var mobileDetail = [String: String]()
    var phoneFormatter = PhoneNumberFormatter()
    
    // MARK: - View life cycle
    // MARK: -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Actions.
    // MARK: -
    @IBAction func formatPhoneNumberTextField(_ sender: UITextField) {
        
        mobileDetail["mobile"] = sender.text ?? ""
        sender.text = phoneFormatter.format(sender.text ?? "", hash: sender.hash)
        
    }
    
    @IBAction func proceedButtonTapp(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if let mobileNo = self.mobileDetail["mobile"], mobileNo.isEmpty  {
            CommonClass.showToast(msg: StringConstants.MobileCouldNotEmpty)
            return
        }
        WebServices.resendOtp(success: { (json) in
            let msg = json["message"].stringValue
            let verifyOtp = RecruiterVerifyOtpVC.instantiate(fromAppStoryboard: .Main)
            AppUserDefaults.save(value: "\((self.mobileDetail["countryCode"] ?? "") + "  " + (self.mobileDetail["mobile"] ?? ""))", forKey: .userPhoneNoFormatWithCode)
            commingFrom = .recruiterEditProfile
            verifyOtp.mobileDetail = self.mobileDetail
            self.navigationController?.pushViewController(verifyOtp, animated: true)
            CommonClass.showToast(msg: msg)
        }, failure: { (error : Error) in
            CommonClass.showToast(msg: error.localizedDescription )
            
        })
        
    }
    
    @IBAction func backButtonTapp(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func countryCodeButtonTapp(_ sender: UIButton) {
        let countryScene = CountryListVC.instantiate(fromAppStoryboard: .Main)
        countryScene.delegate = self
        self.present(countryScene, animated: true, completion: nil)
    }
    
}

// MARK: - Private methods.
// MARK: -
private extension ProfileUpdateMobileNoVC{
    
    func initialSetup(){
        
        CommonClass.setSkyFloatingLabelTextField(textField: self.phoneNumberTextField, placeholder: "", title: "")
        self.phoneNumberTextField.text = phoneFormatter.format(mobileDetail["mobile"] ?? "", hash: self.phoneNumberTextField.hash)
        self.countryCodeButton.setTitle("+\(mobileDetail["countryCode"] ?? "")", for: .normal)
        self.phoneNumberTextField.keyboardType = .numberPad
        self.proceedButton.roundCorner(.allCorners, radius: 4.0)
    }
}

// MARK:- Country Delegate Methods
// ===============================
extension ProfileUpdateMobileNoVC : CountryDelegate {
    
    func didGetCountryWith(code: String , shortName : String) {
        mobileDetail["countryCode"] = code
        self.countryCodeButton.setTitle(mobileDetail["countryCode"], for: .normal)
    }
}
