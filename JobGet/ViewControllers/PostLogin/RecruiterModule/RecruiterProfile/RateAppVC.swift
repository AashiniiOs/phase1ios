//
//  RateAppVC.swift
//  JobGet
//
//  Created by macOS on 29/06/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class RateAppVC: UIViewController {
    
    //MARK:- Properties
    //==================
    var lengthLimit = 280
    var totalRating: Double?
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var starView: FloatRatingView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var cornerRadiusView: UIView!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var reviewPopBlurView: UIView!
    @IBOutlet weak var submitReviewBtn: UIButton!
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.view.endEditing(true)
    }
    
    //MARK: - IB Action and Target
    //===============================
    
    @IBAction func cancelButtonTapp(_ sender: UIButton) {
        self.view.endEditing(true)
        self.reviewPopBlurView.alpha = 1.0
        
        UIView.animate(withDuration: 0.2, animations: {
            self.reviewPopBlurView.alpha = 0.0
        }, completion: { (true) in
            self.reviewPopBlurView.isHidden = true
        })
    }
    
    @IBAction func submitReviewBtnTapp(_ sender: UIButton) {
        self.view.endEditing(true)
        if  textView.text == StringConstants.writeFeedback{
            
            CommonClass.showToast(msg: StringConstants.Please_enter_review)
            return
        }
        
        self.rateApp(loader: true)
    }
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitBtnTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        
        if let ratings = self.totalRating, ratings < 4{
            
            self.reviewPopBlurView.alpha = 0.0
            
            UIView.animate(withDuration: 0.2, animations: {
                self.reviewPopBlurView.alpha = 1.0
            }, completion: { (true) in
                self.reviewPopBlurView.isHidden = false
            })
            
        }else{
            self.rateApp(loader: true)
        }
    }
}

//MARK:- Private Methods
//=======================
private extension RateAppVC {
    
    func initialSetup() {
        
        self.setupView()
    }
    func setupView() {
        self.navigationView.setShadow()
        self.reviewPopBlurView.isHidden = true
        self.starView.rating = 5.0
        self.totalRating =  5.0
        starView.delegate = self
        starView.contentMode = UIViewContentMode.scaleAspectFit
        self.textView.delegate = self
        starView.type = .halfRatings
        CommonClass.setNextButton(button: self.submitBtn)
        self.setupTextAndColor()
        
    }
    func setupTextAndColor() {
        self.countLabel.font = AppFonts.Poppins_Regular.withSize(12)
        self.countLabel.textColor = AppColors.gray152
        self.textView.text = StringConstants.writeFeedback
        self.textView.textColor = AppColors.gray152
        self.textView.font = AppFonts.Poppins_Regular.withSize(14)
        self.countLabel.text = "0/\(lengthLimit)"
    }
}

//MARK:- FloatRatingView Delegate methods.
//MARK:-
extension RateAppVC: FloatRatingViewDelegate {
    
    func floatRatingView(_ ratingView: FloatRatingView, isUpdating rating: Double) {
    }
    
    func floatRatingView(_ ratingView: FloatRatingView, didUpdate rating: Double) {
        
        self.totalRating = rating
    }
}

//MARK:-  UITextView Delegate methods.
//MARK:-
extension RateAppVC: UITextViewDelegate {
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == AppColors.gray152 {
            textView.text = nil
            textView.textColor = AppColors.black26
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard let textViewText = textView.text , lengthLimit != 0 else { return true }
        
        let newLength = textViewText.utf16.count + text.utf16.count - range.length
        
        if textView.text.isEmpty, text == " " {
            return false
        } 
        if newLength <= lengthLimit {
            countLabel.text = "\(newLength)/\(lengthLimit)"
            // drhDelegate?.didReachCharacterLimit(false)
        } else {
            //  drhDelegate?.didReachCharacterLimit(true)
            UIView.animate(withDuration: 0.1, animations: {
                self.countLabel.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
                
            }, completion: { (finish) in
                UIView.animate(withDuration: 0.1, animations: {
                    self.countLabel.transform = CGAffineTransform.identity
                })
            })
        }
        
        return newLength <= lengthLimit
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = StringConstants.writeFeedback
            textView.textColor = AppColors.gray152
        }
    }
}

//MARK: - Web services
//======================
extension RateAppVC {
    
    func rateApp(loader: Bool) {
        
        var text = ""
        if textView.text != StringConstants.writeFeedback{
            text = textView.text
        }
        
        let params: JSONDictionary = [ApiKeys.rating.rawValue: self.totalRating ?? 0.0, "reviews":text]
        
        WebServices.rateApp(parameters: params, loader: loader, success: { (json) in
            
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
                if let ratings = self.totalRating, ratings >= 4{
                    if let url = URL(string: "https://www.apple.com") {
                        
                        UIApplication.shared.open(url, options: [ : ], completionHandler: nil)
                    }
                }else{
                    self.reviewPopBlurView.alpha = 1.0
                    
                    UIView.animate(withDuration: 0.2, animations: {
                        self.reviewPopBlurView.alpha = 0.0
                    }, completion: { (true) in
                        self.reviewPopBlurView.isHidden = true
                    })
                    self.pop()
                }
                
            } else {
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
        }) { (error) in
            print_debug(error)
            CommonClass.showToast(msg: error.localizedDescription)
        }
    }
}


