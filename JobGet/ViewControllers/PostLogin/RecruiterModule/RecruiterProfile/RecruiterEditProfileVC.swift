//
//  RecruiterEditProfileVC.swift
//  JobGet
//
//  Created by macOS on 29/05/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import SwiftyJSON
import GooglePlaces
import GoogleMaps
import GooglePlacePicker
import Firebase

protocol RefreshRecruiterProfileDelgate: class{
    
    func refreshRecruiterProfile(recruiterProfileData: RecruiterInfoData)
}

class RecruiterEditProfileVC: BaseVC {
    
    //MARK:- Properties
    //==================
    var recruiterProfileData: RecruiterInfoData?
    var companyLatitude: Double?
    var companyLongtitude: Double?
    var phoneFormatter = PhoneNumberFormatter()
    
    weak var delegate:RefreshRecruiterProfileDelgate?
    var isFirstTimeTVIncreaseHeight = false
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var imageContainerView: UIView!
    
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var editProfileTableView: UITableView!
    @IBOutlet weak var roundCornerView: UIView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var infoView: UIView!
    
    
    //MARK:- view Life cycle
    //=======================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
        self.infoView.isHidden = true

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cameraButtonTapp(_ sender: UIButton) {
        self.captureImage(on: self)
    }
}

//MARK:- Private Methods
//=======================
private extension RecruiterEditProfileVC {
    
    func initialSetup() {
        
        self.editProfileTableView.delegate = self
        self.editProfileTableView.dataSource = self
        self.editProfileTableView.tableHeaderView = self.imageContainerView
        guard let profileData = self.recruiterProfileData else{return}
        
        if profileData.recruiterImage.isEmpty{
            if !AppUserDefaults.value(forKey: .facebookImage).stringValue.isEmpty{
                
                self.profileImageView.sd_setImage(with: URL(string: AppUserDefaults.getData(forKey: .facebookImage)), placeholderImage: #imageLiteral(resourceName: "icHomeCategoryPlaceholder1"),  completed: nil)}
            self.recruiterProfileData?.recruiterImage = AppUserDefaults.getData(forKey: .facebookImage)
        }else{
            self.profileImageView.sd_setImage(with: URL(string: profileData.recruiterImage), placeholderImage: #imageLiteral(resourceName: "icHomeCategoryPlaceholder"),  completed: nil)
        }
        
        
        self.roundCornerView.setCorner(cornerRadius: 5, clip: true)
        self.shadowView.setShadow()
    }
    
    func checkValidity() -> Bool {
        
        guard let userData = self.recruiterProfileData else{return false}
        if !userData.firstName.isEmpty {
            if userData.firstName.count < 2 {
                CommonClass.showToast(msg: StringConstants.First_Name_Invalid_Length.localized)
                return false
            }
        }
        
        if !userData.lastName.isEmpty {
            if userData.lastName.count < 1 {
                CommonClass.showToast(msg: StringConstants.Last_Name_Invalid_Length.localized)
                return false
            }
        }
        if !userData.email.isEmpty{
            if userData.email.checkIfInvalid(.email) {
                CommonClass.showToast(msg:  StringConstants.Enter_Email.localized)
                return false
            }
        } else {
            return false
        }
        
        return true
    }
    
    func editProfileService(){
        
        guard let profileData = self.recruiterProfileData else{return}
        
        var mobileNumber = profileData.recruiterCompanyData.mobileNo.replace(string: "-", withString: "")
        if mobileNumber.count > 10{
            mobileNumber.removeLast()
        }
        
        var param = [String: String]()
        param["firstName"] = profileData.firstName
        param["lastName"] = profileData.lastName
        param["companyLatitude"] = profileData.recruiterCompanyData.companyLattitude
        param["companyLongitude"] = profileData.recruiterCompanyData.companyLongitude
        param["userImage"] = profileData.recruiterImage
        param["companyDesc"] = profileData.recruiterCompanyData.companyDesc
        param["countryCode"] = profileData.recruiterCompanyData.countrycode.replace(string: "+", withString: "")
        
        param["mobile"] = mobileNumber
        param["email"] = profileData.email
        param["companyName"] = profileData.recruiterCompanyData.companyName
        WebServices.recruiterEditProfile(parameters: param, loader: true, success: { (json) in
            
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                let data = RecruiterInfoData.init(dict: json["data"])
                
                self.recruiterProfileData = data
                
                self.updateFirebaseDetail()
                if let profileData = self.recruiterProfileData{
                    
                    self.delegate?.refreshRecruiterProfile(recruiterProfileData: profileData)
                }
                CommonClass.showToast(msg: StringConstants.Profile_updated_successfully)
                self.navigationController?.popViewController(animated: true)
                
            } else {
                
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
            
        }) { (error) in
            
            CommonClass.showToast(msg: error.localizedDescription)
        }
    }
}

//MARK: PRIVATE FUNCTIONS
extension RecruiterEditProfileVC{
    
    private func uploadImage(image : UIImage) {
        
//        loader.start()
        AppNetworking.showUploadImageLoader()
        image.uploadImageToS3( success: { (uploaded : Bool, imageUrl : String) in
            if uploaded {
                
                self.recruiterProfileData?.recruiterImage = imageUrl
                AppNetworking.hideLoader()
                
            }
        }, progress: { (uploadPercentage : CGFloat) in
            print_debug(uploadPercentage)
            
        }, failure: { (err : Error) in
            
            if (err.localizedDescription  == StringConstants.Internet_connection_appears_offline) {
                CommonClass.showToast(msg: StringConstants.Please_check_your_internet_connection)
            }
            self.recruiterProfileData?.recruiterImage = ""
            
           AppNetworking.hideLoader()
        })
    }
    
    func updateFirebaseDetail() {
        
        var userDetails: [String: Any] = [:]
        
        if let recruiterData = self.recruiterProfileData {
            
            print_debug(recruiterData.userId)
            if let fcmTocken = Messaging.messaging().fcmToken{
                userDetails[ChatEnum.User.deviceToken.rawValue] = fcmTocken
            }
            userDetails[ChatEnum.User.email.rawValue] = recruiterData.email // dict["email"] //.stringValue.replacingOccurrences(of: ".", with: "")  //"user.email"
            userDetails[ChatEnum.User.firstName.rawValue] = recruiterData.firstName  //dict["first_name"].stringValue// "user.first_name"
            userDetails[ChatEnum.User.lastName.rawValue] =  recruiterData.lastName //dict["last_name"].stringValue  //"user.last_name"
            userDetails[ChatEnum.User.mobileNumber.rawValue] = recruiterData.mobile //dict["mobile"].stringValue // "user.phone"
            userDetails[ChatEnum.User.userImage.rawValue] = recruiterData.recruiterImage //"user.imageUrl"
            userDetails[ChatEnum.User.userId.rawValue] =  recruiterData.userId//dict["_id"].stringValue //  "userId"
            userDetails[ChatEnum.User.isOnline.rawValue] = true  //false
            userDetails[ChatEnum.User.deviceType.rawValue] = Device_Type.iOS.rawValue
            ChatHelper.updateUserDetails(userId: recruiterData.userId, details: userDetails)
        }
    }
}

//MARK:- ImagePickerController & NavigationController Delegate
//============================================================
extension RecruiterEditProfileVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let imagePicked = info[UIImagePickerControllerOriginalImage] as? UIImage{
            
            let frameRect = CGRect(x: 50, y: self.view.center.y, width: self.view.frame.width - 100, height: 180)
            Cropper.shared.openCropper(withImage: imagePicked, mode: .custom(frameRect), on: self)
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        
    }
}

//MARK: Extension AppInventivCropperDelegate
//MARK:
extension RecruiterEditProfileVC: CropperDelegate {
    
    
    func imageCropperDidCancelCrop() {
        
        print_debug("Crop cancelled")
        
    }
    func imageCropper(didCropImage croppedImage: UIImage, usingCropRect cropRect: CGRect) {
        
        self.profileImageView.contentMode = .scaleAspectFill
        self.profileImageView.clipsToBounds = true
        self.profileImageView.image = croppedImage
        self.uploadImage(image: croppedImage)
    }
}

//MARK: - IB Action and Target
//===============================
extension RecruiterEditProfileVC {
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func saveBtnTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        if self.checkValidity() {
            self.editProfileService()
        }
    }
}

//MARK: - Table view Delegate and DataSource
//============================================
extension RecruiterEditProfileVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0...3:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "FullNameCell") as? FullNameCell else { fatalError("invalid cell \(self)")
            }
            cell.nameTextField.delegate = self
            
            switch indexPath.row {
            case 0:
                CommonClass.setSkyFloatingLabelTextField(textField: cell.nameTextField, placeholder: StringConstants.FirstName, title: StringConstants.FirstName)
                if let profileData = self.recruiterProfileData{
                    cell.nameTextField.text = profileData.firstName
                }
                
            case 1:
                CommonClass.setSkyFloatingLabelTextField(textField: cell.nameTextField, placeholder: StringConstants.LastName, title: StringConstants.LastName)
                if let profileData = self.recruiterProfileData{
                    cell.nameTextField.text = profileData.lastName
                }
            case 2:
                CommonClass.setSkyFloatingLabelTextField(textField: cell.nameTextField, placeholder: StringConstants.Email_Address.localized, title: StringConstants.Email_Address.localized)
                if let profileData = self.recruiterProfileData{
                    cell.nameTextField.text = profileData.email
                }
            case 3:
                CommonClass.setSkyFloatingLabelTextField(textField: cell.nameTextField, placeholder: StringConstants.Company_Name, title: StringConstants.Company_Name)
                if let profileData = self.recruiterProfileData{
                    cell.nameTextField.text = profileData.recruiterCompanyData.companyName
                }
                
            default:
                fatalError("invalid cell")
            }
            return cell
        case 4:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PhoneNumberCell") as? PhoneNumberCell else { fatalError("invalid cell \(self)")
            }
            cell.phoneNoTextField.delegate = self
            if let profileData = self.recruiterProfileData{
                cell.phoneNoTextField.text = phoneFormatter.format(profileData.recruiterCompanyData.mobileNo , hash: cell.phoneNoTextField.hash)
                if profileData.recruiterCompanyData.countrycode.contains(s: "+"){
                    cell.countryCodeBtn.setTitle(profileData.recruiterCompanyData.countrycode, for: .normal)
                }else{
                    cell.countryCodeBtn.setTitle("+\(profileData.recruiterCompanyData.countrycode)", for: .normal)
                }
                
            }
            cell.countryCodeBtn.addTarget(self, action: #selector(self.updateMobileNumberButtonTapp(_:)), for: .touchUpInside)
            return cell
        case 5:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "RecruiterDescCell") as? RecruiterDescCell else { fatalError("invalid cell \(self)")
            }
            
            cell.companyDescTextView.delegate = self
            cell.infoButton.addTarget(self, action: #selector(infoButtonTapped), for: .touchUpInside)
            CommonClass.setTLFoatTextView(textView: cell.companyDescTextView, title: "Company Description. \nTell candidates more about your company!")
            if let profileData = self.recruiterProfileData{
                cell.companyDescTextView.text = profileData.recruiterCompanyData.companyDesc
                
            }
            cell.adjustTextViewHeight(textview: cell.companyDescTextView)
//            self.textViewHeightChange(70, textView: cell.companyDescTextView)

            self.textViewHeightChange(cell.textViewHeight, textView: cell.companyDescTextView)
            return cell
            
        default:
            fatalError("invalid cell \(self)")
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
    }
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.infoView.isHidden = true

    }
    @objc func updateMobileNumberButtonTapp(_ sender: UIButton){
        let updateMobile = ProfileUpdateMobileNoVC.instantiate(fromAppStoryboard: .RecruiterProfile)
        if let profileData = self.recruiterProfileData{
            updateMobile.mobileDetail["mobile"] = profileData.recruiterCompanyData.mobileNo
            updateMobile.mobileDetail["countryCode"] = profileData.recruiterCompanyData.countrycode
        }
        self.navigationController?.pushViewController(updateMobile, animated: true)
    }
    @objc func infoButtonTapped(){
        self.infoView.isHidden = false
    }
}

//MARK:- UITextFieldDelegate method
//=========================
extension RecruiterEditProfileVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let indexPath = textField.tableViewIndexPath(self.editProfileTableView) else{return false}
        
        if indexPath.row == 0 || indexPath.row == 1 {
            if (textField.text ?? "").count == 0, string == " " {
                return false
            } else if (textField.text ?? "").count - range.length < 200 {
                
                let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
                let filtered = string.components(separatedBy: cs).joined(separator: "")
                
                return (string == filtered)
                
            } else {
                return false
            }
        } else if indexPath.row == 2 {
            if string == " " {
                return false
            } else {
                return (textField.text ?? "").count - range.length < 50
            }
        } else {
            return true
        }
        
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        guard let indexPath = textField.tableViewIndexPath(self.editProfileTableView) else{return false}
        if indexPath.row == 4{
            
            let updateMobile = ProfileUpdateMobileNoVC.instantiate(fromAppStoryboard: .RecruiterProfile)
            if let profileData = self.recruiterProfileData{
                updateMobile.mobileDetail["mobile"] = profileData.recruiterCompanyData.mobileNo
                updateMobile.mobileDetail["countryCode"] = profileData.recruiterCompanyData.countrycode
            }
            self.navigationController?.pushViewController(updateMobile, animated: true)
            return false
        }else if indexPath.row == 7{
            let northEast = CLLocationCoordinate2DMake(Double(StringConstants.Boston_Lat)! + 0.001, Double(StringConstants.Boston_Long)! + 0.001)
            let southWest = CLLocationCoordinate2DMake(Double(StringConstants.Boston_Lat)! - 0.001, Double(StringConstants.Boston_Long)! - 0.001)
            let viewport = GMSCoordinateBounds(coordinate: northEast, coordinate: southWest)
            let config = GMSPlacePickerConfig(viewport: viewport)

            let placePicker = GMSPlacePickerViewController(config: config)
            placePicker.delegate = self
            present(placePicker, animated: true, completion: nil)
            return false
        }
        else{
            return true
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        guard let indexPath = textField.tableViewIndexPath(self.editProfileTableView) else{return}
        switch indexPath.row {
        case 0:
            self.recruiterProfileData?.firstName = textField.text ?? ""
        case 1:
            self.recruiterProfileData?.lastName = textField.text ?? ""
        case 2:
            self.recruiterProfileData?.email = textField.text ?? ""
        case 3:
            self.recruiterProfileData?.recruiterCompanyData.companyName = textField.text ?? ""
        default:
            return
        }
    }
}

//MARK:- UITextFieldDelegate method
//=========================
extension RecruiterEditProfileVC: UITextViewDelegate{
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        self.isFirstTimeTVIncreaseHeight = false
        guard let cell = textView.tableViewCell as? RecruiterDescCell else{return false}
        cell.bottomViewSeperator.backgroundColor = AppColors.themeBlueColor
        
        return true
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        guard let indexPath = textView.tableViewIndexPath(self.editProfileTableView) else{return}
        guard let cell = textView.tableViewCell as? RecruiterDescCell else{return}
        cell.bottomViewSeperator.backgroundColor = AppColors.blakopacity15
        if indexPath.row == 5{
            
            self.recruiterProfileData?.recruiterCompanyData.companyDesc = textView.text ?? "".trailingSpacesTrimmed
        }
    }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        
        let newText = (textView.text as NSString).replacingCharacters(in: range, with: text)
        print_debug(newText.count)
        if newText.count > 280{
            showAlert(msg:  StringConstants.job_decription_text_limit)
        }
        return newText.count < 280
    }
    func textViewDidChange(_ textView: UITextView) {
        UIView.setAnimationsEnabled(false)
        self.editProfileTableView.beginUpdates()
        guard let cell = textView.tableViewCell as? RecruiterDescCell else{return}
        self.textViewHeightChange(cell.textViewHeight,textView: textView)
        self.editProfileTableView.endUpdates()
        UIView.setAnimationsEnabled(true)
        
        //        self.textViewHeightChange(cell.textViewHeight, textView: cell.companyDescTextView)
        
    }
    
    func textViewHeightChange(_ textViewHeight: NSLayoutConstraint, textView: UITextView){
        
        self.editProfileTableView.beginUpdates()
        
        let topBottomPadding: CGFloat = 20.0
        let threeLineTextHeight: CGFloat = 95.0
        let fixedWidth = textView.frame.size.width
        let fixedHeight = textView.frame.size.height
        
        let maximumTextHeight: CGFloat = threeLineTextHeight + topBottomPadding
        let newSize = textView.sizeThatFits(CGSize(width: fixedWidth, height: fixedHeight))
        let textHeight = textView.text.heightWithConstrainedWidth(width: textView.width-10.0, font: textView.font!)
        if textHeight > threeLineTextHeight {
            textView.isScrollEnabled = true
            textViewHeight.constant = maximumTextHeight
            
        } else {
            textView.isScrollEnabled = false
            if textView.text.isEmpty{
                textViewHeight.constant = 70
                textView.frame.size = CGSize(width: max(newSize.width, fixedWidth), height:70)
            }else{
                textView.frame.size = CGSize(width: max(newSize.width, fixedWidth), height: newSize.height)
                if self.isFirstTimeTVIncreaseHeight{
                    textViewHeight.constant = newSize.height + 15
                }else{
                    textViewHeight.constant = newSize.height
                }
            }
        }
        
        self.view.layoutIfNeeded()
        self.editProfileTableView.endUpdates()
    }
}

// MARK: - Google Place Picker
//======================================
extension RecruiterEditProfileVC: GMSPlacePickerViewControllerDelegate {
    
    func placePicker(_ viewController: GMSPlacePickerViewController, didPick place: GMSPlace) {
        self.companyLatitude = place.coordinate.latitude
        self.companyLongtitude = place.coordinate.longitude
        
        if let address = place.formattedAddress {
            self.recruiterProfileData?.recruiterCompanyData.companyAddress = address
            AppUserDefaults.save(value: String(describing: address), forKey: .recruiterCompanyAddress)
        }
        self.editProfileTableView.reloadData()
        viewController.dismiss(animated: true, completion: nil)
        
    }
    
    func placePickerDidCancel(_ viewController: GMSPlacePickerViewController) {
        // Dismiss the place picker, as it cannot dismiss itself.
        viewController.dismiss(animated: true, completion: nil)
        
    }
    
}

//MARK:- Prototype Cell
//=========================
class FullNameCell: UITableViewCell {
    
    //MARK:- Properties
    //==================
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var nameTextField: SkyFloatingLabelTextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

class RecruiterDescCell: UITableViewCell {
    
    //MARK:- Properties
    //==================
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var companyDescTextView: TLFloatLabelTextView!
    @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    @IBOutlet weak var infoButton: UIButton!
    
    @IBOutlet weak var bottomViewSeperator: UIView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func adjustTextViewHeight(textview : UITextView) {
        self.companyDescTextView.textContainer.lineBreakMode = .byWordWrapping
        companyDescTextView.isScrollEnabled = false
    }
}

class PhoneNumberCell: UITableViewCell {
    
    //MARK:- Properties
    //==================
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var phoneNumberLabel: UILabel!
    
    
    @IBOutlet weak var phoneNoTextField: UITextField!
    @IBOutlet weak var countryCodeBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        phoneNoTextField.contentMode = .bottom
        
    }
}



