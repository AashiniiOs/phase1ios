//
//  RecruiterProfileVC.swift
//  JobGet
//
//  Created by macOS on 29/05/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import AnyFormatKit


class RecruiterProfileVC: BaseVC {
    
    
    //MARK:- Properties
    //==================
    var recruiterProfileData: RecruiterProfile?
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var recruiterProfleTableView: UITableView!
    
    //MARK:- view Life cycle
    //=======================
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getecruiterProfileDetail(loader: false)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//MARK:- Private Methods
//=======================
private extension RecruiterProfileVC {
    
    func initialSetup() {
        
        self.recruiterProfleTableView.delegate = self
        self.recruiterProfleTableView.dataSource = self
        
        self.titleLabel.textColor = AppColors.black46
        self.registerNib()
        
        self.getecruiterProfileDetail(loader: true)
    }
    
    func registerNib() {
        
        let nib = UINib(nibName: "CandidateAboutMeCell", bundle: nil)
        self.recruiterProfleTableView.register(nib, forCellReuseIdentifier: "CandidateAboutMeCell")
    }
}


//MARK: - IB Action and Target
//===============================
extension RecruiterProfileVC {
    
    @IBAction func editBtnTapped(_ sender: UIButton) {
        
        let editProfile = RecruiterEditProfileVC.instantiate(fromAppStoryboard: .RecruiterProfile)
        editProfile.recruiterProfileData = self.recruiterProfileData?.recruiterInfoData
        editProfile.delegate = self
        editProfile.isFirstTimeTVIncreaseHeight = true
        self.navigationController?.pushViewController(editProfile, animated: true)
        
    }
}

//MARK: - Web services
//======================
extension RecruiterProfileVC {
    
    func getecruiterProfileDetail(loader: Bool) {
        
        let params = [String: String]()
        
        WebServices.recruiterProfileDetail(parameters: params, loader: loader, success: { (json) in
            if let errorCode = json["code"].int, errorCode == error_codes.success {
                
                let data = RecruiterProfile.init(dict: json["data"].dictionaryValue)
                
                self.recruiterProfileData = data
                self.recruiterProfleTableView.reloadData()
            } else {
                
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
            
        }) { (error) in
            print_debug(error)
            CommonClass.showToast(msg: error.localizedDescription)
            
        }
    }
}

//MARK: - Table view Delegate and DataSource
//============================================
extension RecruiterProfileVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
            
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileImageCell") as? ProfileImageCell else { fatalError("invalid cell \(self)")
            }
            if let data = self.recruiterProfileData{
                
                cell.ProfileimageView.sd_setImage(with: URL(string: data.recruiterInfoData.recruiterImage),  placeholderImage: #imageLiteral(resourceName: "icHomeCategoryPlaceholder1"), completed: nil)
            }
            
            return cell
        case 1:
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "RecruiterDeatilCell") as? RecruiterDeatilCell else { fatalError("invalid cell \(self)")
            }
            
            if let data = self.recruiterProfileData {
                cell.nameLabel.text = "\(data.recruiterInfoData.firstName) \(data.recruiterInfoData.lastName)"
                cell.companyNameLabel.text = "\(data.recruiterInfoData.recruiterCompanyData.companyName)"
                let phoneFormatter = TextFormatter(textPattern: "###-###-####")
                let phoneNumber = phoneFormatter.formattedText(from: "\(data.recruiterInfoData.recruiterCompanyData.mobileNo)" ) // +12 (345) 678-90-12
                cell.phoneNumberLabel.text = "+\(data.recruiterInfoData.recruiterCompanyData.countrycode)  \(phoneNumber ?? "")"
            }
            return cell
            
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CandidateAboutMeCell") as? CandidateAboutMeCell else { fatalError("invalid cell \(self)")
            }
            if let data = self.recruiterProfileData{
                cell.aboutMeLabel.text = StringConstants.Company_Description
                cell.aboutDescLabel.text = data.recruiterInfoData.recruiterCompanyData.companyDesc.trailingSpacesTrimmed
            }
            return cell
        case 3:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DashboardCell") as? DashboardCell else { fatalError("invalid cell \(self)")
            }
            cell.dashboardCollectionView.delegate = self
            cell.dashboardCollectionView.dataSource = self
            if let _ = self.recruiterProfileData{
                cell.dashboardCollectionView.reloadData()
            }
            return cell
        case 4:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "RecruiterSettingCell") as? RecruiterSettingCell else { fatalError("invalid cell \(self)")
            }
            return cell
            
        default:
            fatalError("invalid cell in \(self)")
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 3 {
            return 250
        } else {
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == 4 {
            let settingObj = RecruiterSettingVC.instantiate(fromAppStoryboard: .RecruiterProfile)
            self.navigationController?.pushViewController(settingObj, animated: true)
        }
    }
}

//MARK: - Collection view Delegate and DataSource
//============================================
extension RecruiterProfileVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let data = self.recruiterProfileData else{return 0 }
        return data.dashboardData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "JobPostCountCollectionViewCell", for: indexPath) as? JobPostCountCollectionViewCell else { fatalError("invalid collection view cell \(self)") }
        if indexPath.item > 2 {
            cell.horizontalSeperator.isHidden = true
        } else if indexPath.item == 2 {
            cell.verticalSeperator.isHidden = true
        } else {
            cell.horizontalSeperator.isHidden = false
            cell.verticalSeperator.isHidden = false
        }
        //
        if let data = self.recruiterProfileData{
            cell.countLabel.text = "\(data.dashboardData[indexPath.row].count)"
            cell.jobPostLabel.text = data.dashboardData[indexPath.row].title
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (collectionView.frame.width/3), height: (collectionView.frame.height/2))
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
    
}

//MARK:- RefreshRecruiterProfileDelgate method for Refresh profile after edit.
extension RecruiterProfileVC: RefreshRecruiterProfileDelgate{
    
    func refreshRecruiterProfile(recruiterProfileData: RecruiterInfoData) {
        self.recruiterProfileData?.recruiterInfoData = recruiterProfileData
        self.recruiterProfleTableView.reloadData()
    }
}

//MARK:- Prototype Cell
//=========================
class ProfileImageCell: UITableViewCell {
    
    //MARK:- Properties
    //==================
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var ProfileimageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

class RecruiterDeatilCell: UITableViewCell {
    
    //MARK:- Properties
    //==================
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var companyNameLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.nameLabel.textColor = AppColors.black46
        self.phoneNumberLabel.textColor = AppColors.gray152
        self.companyNameLabel.textColor = AppColors.black46
        
    }
}

class DashboardCell: UITableViewCell {
    
    //MARK:- Properties
    //==================
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var dashboardLabel: UILabel!
    @IBOutlet weak var dashboardCollectionView: UICollectionView!
    @IBOutlet weak var dashBoardLblContainerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        dashBoardLblContainerView.backgroundColor = AppColors.themeBlueColor
        self.dashboardLabel.textColor = AppColors.white

    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.containerView.setCorner(cornerRadius: 5, clip: false)
        self.containerView.setShadow()
        dashBoardLblContainerView.roundCornersFromOneSide([.topLeft, .topRight], radius: 5)
    }
}



class RecruiterSettingCell: UITableViewCell {
    
    //MARK:- Properties
    //==================
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var settingImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var settingLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        
        self.containerView.setCorner(cornerRadius: 5, clip: false)
        self.containerView.dropShadow(color: AppColors.black46, opacity:0.2, offSet: CGSize(width: 1.0, height: 1.0), radius: 2.0)
    }
}

class JobPostCountCollectionViewCell: UICollectionViewCell {
    
    //MARK:- Properties
    //==================
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var jobPostLabel: UILabel!
    @IBOutlet weak var horizontalSeperator: UIView!
    @IBOutlet weak var verticalSeperator: UIView!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var containerView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.countLabel.textColor = AppColors.black46
        self.jobPostLabel.textColor = AppColors.black26
    }
}


