//
//  RecruiterSettingVC.swift
//  JobGet
//
//  Created by macOS on 29/05/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import StoreKit

class RecruiterSettingVC: UIViewController {
    
    //    MARK:- IBOutlets
    //    ========================================
    @IBOutlet weak var settingsTable: UITableView!
    @IBOutlet weak var screenTitle: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var navigationView: UIView!
    
    //    MARK:- Properties
    //    ========================================
    //  var isFromRecruiter = true
    private var settings = [StringConstants.pushNotification,
                            StringConstants.RestorePackage,
                            StringConstants.Star_Purchase_History,
                            StringConstants.changePassword,
                            StringConstants.invite,
                            StringConstants.Rate_Review_App,
                            StringConstants.ContactUS,
                            StringConstants.termsConditions,
                            StringConstants.privacyPloicy,
                            StringConstants.faq,
                            StringConstants.deactivate,
                            StringConstants.logout ]
    
    fileprivate var settingData = [String]()
    fileprivate var iapProducts = [SKProduct]()
    var iapProductIdentifiers = Set<String>()
    var purchaseCompletionBlock:((_ purchasedPID:Set<String>,_ restoredPID:Set<String>,_ failedPID:Set<String>)->Void)!
    
    var data = User.getUserModel()

    //    MARK:- life cycle
    //    ========================================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    //    MARK:- functions
    //    ========================================
    private func initialSetup() {
        
        self.purchaseCompletionBlock = {
            
            (purchasedPID,restoredPID,failedPID) in
            
            AppNetworking.hideLoader()
            
            //MARK:- Get restoredPID empty when user succesfully purchase the subscription
            DispatchQueue.main.async {
                
                if failedPID.isEmpty && !restoredPID.isEmpty {
                    self.getAppReceipt()
                } else {
                    AppNetworking.hideLoader()
                }
            }
        }
        self.settingsTable.dataSource = self
        self.settingsTable.delegate = self
        self.screenTitle.text = StringConstants.Settings.uppercased()
        self.navigationView.dropShadow(offSet: CGSize(width: 0, height: 1))
    }
    
    func displayShareSheet(shareContent:String) {
        let activityViewController = UIActivityViewController(activityItems: [shareContent as NSString], applicationActivities: nil)
        present(activityViewController, animated: true, completion: {})
    }
    
    //    MARK:- selctor methods
    //    ========================================
    
    
    //    MARK:- IBActions
    //    ========================================
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
//MARK: - Web services
//======================
extension RecruiterSettingVC {
    
    //MARK: Get latest app receipt
    func getAppReceipt(){
        
        IAPController.shared.fetchIAPReceipt(sharedSecrete: StringConstants.InAppPurchaseSecrateKey, success: { (receipt) in
            DispatchQueue.main.async {
                print_debug(receipt)
                //                receipt["latest_receipt"] as? String
                var transactionId: String?
                if let receiptData = receipt as? JSONDictionary, let receipts = receiptData["receipt"] as? JSONDictionary, let in_app = receipts["in_app"] as? [JSONDictionary]{
                    
                    let _ = in_app.map{ inApp in
                        transactionId = inApp["transaction_id"] as? String
                    }
                }
                if let transaction_id = transactionId, let latest_receipt = receipt["latest_receipt"] as? String{
                    let params: JSONDictionary = ["Amount":"",
                                                  "paymentToken":latest_receipt,
                                                  "planId":"",
                                                  "stars":"",
                                                  "transactionId":transaction_id
                    ]
                    self.buySubscription(params: params)
                }
                
                
                
                //                if let index = self.indexPath, let transaction_id = transactionId{
                //                    self.createTransaction(param:
                //                        ["transactionId":transaction_id, "planId":self.featurePostModel[index.row].planId,
                //                         "amount":self.featurePostModel[index.row].planAmount,
                //                         "planName":self.featurePostModel[index.row].planName
                //                        ]
                //                    )
                //                }
            }
        }, failure: { (error) in
            AppNetworking.hideLoader()
            CommonClass.showToast(msg: error?.localizedDescription ?? "")
        })
    }
    
    func buySubscription(params: JSONDictionary){
        
        WebServices.buySubscription(parameters: params, loader: true, success: { (json) in
            print_debug("Subscription bought")
        }) { (error) in
            
            CommonClass.showToast(msg: error.localizedDescription)
        }
    }
    
    func logout(loader: Bool) {
        
        //    let params = [String: String]()
        
        WebServices.logoutAPI(success: { (json) in
            
            print_debug(json)
            if let userId = User.getUserModel().user_id {
                UIApplication.shared.applicationIconBadgeNumber = 0
                print_debug("Logout user_id : \(userId)")
                ChatHelper.logOutUser(userId: userId)
                CommonClass().goToLogin()
                AppUserDefaults.removeAllValues()
            }else{
                self.showAlert(msg: StringConstants.Cannot_logout_user)
            }
            
        }) { (error) in
            print_debug(error)
            CommonClass.showToast(msg: error.localizedDescription)
            
        }
    }
    
    func restoreSubscription(){
        AppNetworking.showLoader()
        IAPController.shared.restoreIAPProducts(success: { (purchasedPID, restoredPID, failedPID) in
            
            self.getAppReceipt()
            
            AppNetworking.hideLoader()
        }, failure: {
            [weak self] (error) in
            
            if let err = error {
                AppNetworking.hideLoader()
                
                let alertController = UIAlertController(title: StringConstants.Error, message: err.localizedDescription, preferredStyle: .alert)
                let alertAction = UIAlertAction(title: StringConstants.Ok, style: .cancel, handler: nil)
                
                alertController.addAction(alertAction)
                self?.present(alertController, animated: true, completion: nil)
            }
        })
    }
}
//    MARK:- tableview datasource
//    ========================================
extension RecruiterSettingVC: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settings.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecruiterSettingsCell", for: indexPath) as! RecruiterSettingsCell
        
        if indexPath.row == 0 {
            let data = AppUserDefaults.value(forKey: .isNotify)
            if data != 1{
                cell.toggle.isOn = false
            }else{
                cell.toggle.isOn = true
            }
            cell.toggle.addTarget(self, action: #selector(notificationSwitchAction), for: .valueChanged)

            cell.toggle.isHidden = false
            cell.disclosureImage.isHidden = true
        } else if indexPath.row == self.settings.count - 1 {
            cell.disclosureImage.image = #imageLiteral(resourceName: "ic_power_settings_new_").rotateBy(angleDegree: 90)
        }
        
        cell.heading.text = self.settings[indexPath.row]
        
        return cell
    }
    
    @objc func notificationSwitchAction(_ sender: UISwitch){
        
        var type = 1
        if sender.isOn{
            type = 1
        }else{
            type = 0
        }
        
        WebServices.notifyAPI(params: ["isNotify": type], success: { (data) in
            AppUserDefaults.save(value: "\(type)", forKey: .isNotify)
        }) { (error) in
            CommonClass.showToast(msg: error.localizedDescription)
        }
        
    }

}
//    MARK:- tabeleview delegate
//    ========================================
extension RecruiterSettingVC: UITableViewDelegate{
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
            
        case 1:
            self.restoreSubscription()
            
        case 2:
            let starVC = StarVC.instantiate(fromAppStoryboard: .Star)
            self.navigationController?.pushViewController(starVC, animated: true)
            
        case 3:
            print_debug("Go to change Password")
            let changePassVc = ChangePasswordVC.instantiate(fromAppStoryboard: .RecruiterProfile)
            self.navigationController?.pushViewController(changePassVc, animated: true)
            
        case 4:
            print_debug(AppUserDefaults.value(forKey: .referralUrl).stringValue)
            let inviteText = "Get JobGet Application by using my refrral code: \(AppUserDefaults.value(forKey: .referralCode).stringValue) \n\(AppUserDefaults.value(forKey: .referralUrl).stringValue)"
            self.displayShareSheet(shareContent: inviteText)
            
        case 5:
            let rateVC = RateAppVC.instantiate(fromAppStoryboard: .RecruiterProfile)
            self.navigationController?.pushViewController(rateVC, animated: true)
            
        case 6:
            let contactusVC = ContactusVC.instantiate(fromAppStoryboard: .RecruiterProfile)
            self.navigationController?.pushViewController(contactusVC, animated: true)
            
        case 7,8,9:
            let staticPage = StaticPageVC.instantiate(fromAppStoryboard: .settingsAndChat)
            if indexPath.row == 7{
                staticPage.webViewType = .terms
            }else if indexPath.row == 8{
                staticPage.webViewType = .privacy
            }else if indexPath.row == 9{
                staticPage.webViewType = .faq
            }
            
            self.navigationController?.pushViewController(staticPage, animated: true)
            
        case 10:
            let deactivateAccountVC = DeactivateAccountVC.instantiate(fromAppStoryboard: .settingsAndChat)
            self.navigationController?.pushViewController(deactivateAccountVC, animated: true)
            
        case  11:
            print_debug("Logout")
            
            let popUpVc = AlertPopUpVC.instantiate(fromAppStoryboard: .Candidate)
            popUpVc.delegate = self
            popUpVc.commongFromSetting = true
            popUpVc.modalPresentationStyle = .overCurrentContext
            popUpVc.modalTransitionStyle = .crossDissolve
            self.present(popUpVc, animated: true, completion: nil)
            
        default:
            break
        }
    }
}

extension RecruiterSettingVC: AlertPopUpDelegate {
    
    func didTapAffirmativeButton() {
        print_debug("Logout")
        //            TODO:- Logout user here
        
        self.logout(loader: true)
        
    }
    
    func didTapNegativeButton() {
        // To do Task
    }
    
    func didTapDeativateButton() {
        print_debug("deactivate Button Tapped")
       
    }
}

//    MARK:- Settings cell
//    ========================================

class RecruiterSettingsCell: UITableViewCell{
    
    @IBOutlet weak var heading: UILabel!
    @IBOutlet weak var disclosureImage: UIImageView!
    @IBOutlet weak var toggle: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.intialSetUp()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.intialSetUp()
    }
    
    private func intialSetUp(){
        self.toggle.isHidden = true
        self.disclosureImage.isHidden = false
        self.disclosureImage.image = #imageLiteral(resourceName: "ic_chevron_right_")
    }
}

