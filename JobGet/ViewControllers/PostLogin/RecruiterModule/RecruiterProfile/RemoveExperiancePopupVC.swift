//
//  RemoveExperiancePopupVC.swift
//  JobGet
//
//  Created by macOS on 27/06/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class RemoveExperiancePopupVC: UIViewController {
    
    
    weak var delegate: AlertPopUpDelegate?
    
    @IBOutlet weak var noButton: UIButton!
    @IBOutlet weak var yesButton: UIButton!
    @IBOutlet weak var messageTwoLabel: UILabel!
    @IBOutlet weak var messageOneLabel: UILabel!
    @IBOutlet weak var popupView: UIView!
    
    var isOpenFromChat = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func initialSetup() {
        if self.isOpenFromChat{
            self.messageTwoLabel.text = StringConstants.Are_you_sure_you_want_delete_message
        }else{
            self.messageTwoLabel.text = StringConstants.Are_you_sure_want_delete_experience
        }
        
        self.yesButton.setCorner(cornerRadius: 5.0, clip: true)
        self.noButton.setCorner(cornerRadius: 5.0, clip: true)
        self.popupView.setCorner(cornerRadius: 5.0, clip: true)
        self.yesButton.borderWidth = 1.0
        self.yesButton.borderColor = AppColors.themeBlueColor
        self.yesButton.backgroundColor = AppColors.themeBlueColor
        self.yesButton.setTitleColor(AppColors.white, for: .normal)
        self.noButton.borderWidth = 1.0
        self.noButton.borderColor = AppColors.themeBlueColor
        self.noButton.backgroundColor = AppColors.white
        self.noButton.setTitleColor(AppColors.themeBlueColor, for: .normal)
    }
    
    @IBAction func yesBtnTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
        self.delegate?.didTapAffirmativeButton()
        
    }
    
    @IBAction func noBtnTapped(_ sender: UIButton) {
        self.delegate?.didTapNegativeButton()
        self.dismiss(animated: true, completion: nil)
    }
    
    
}
