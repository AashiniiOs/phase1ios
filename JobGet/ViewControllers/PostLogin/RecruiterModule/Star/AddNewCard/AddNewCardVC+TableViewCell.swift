//
//  AddNewCardVC+TableViewCell.swift
//  JobGet
//
//  Created by Admin on 10/07/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class AddNewCardNormalTextFieldCell: UITableViewCell{
    
    @IBOutlet weak var cardTextField: SkyFloatingLabelTextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func populatedCell(index: Int, cardDetail: JSONDictionary){
        
        switch index {
        case 0:
            self.cardTextField.text = cardDetail["cardOwnerName"] as? String ?? ""
            CommonClass.setSkyFloatingLabelTextField(textField: self.cardTextField, placeholder: StringConstants.NameonCard, title: StringConstants.NameonCard)
            
        case 1:
            CommonClass.setSkyFloatingLabelTextField(textField: self.cardTextField, placeholder: StringConstants.CardNumber, title: StringConstants.CardNumber)
            self.cardTextField.text = cardDetail["cardNumber"] as? String ?? ""
            
        default:
            CommonClass.setSkyFloatingLabelTextField(textField: self.cardTextField, placeholder: StringConstants.CVC, title: StringConstants.CVC)
            self.cardTextField.text = cardDetail["cvv"] as? String ?? ""
        }
    }
}

class AddNewCardPickerTextFieldCell: UITableViewCell{
    
    @IBOutlet weak var monthTextField: SkyFloatingLabelTextField!
    @IBOutlet weak var yearTextField: SkyFloatingLabelTextField!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        CommonClass.setSkyFloatingLabelTextField(textField: self.monthTextField, placeholder: StringConstants.Month, title: StringConstants.Month)
        CommonClass.setSkyFloatingLabelTextField(textField: self.yearTextField, placeholder: StringConstants.Year, title: StringConstants.Year)
    }
    
    func populatedCell( cardDetail: JSONDictionary){
        self.monthTextField.text = cardDetail["expiryDateMonth"] as? String ?? ""
        self.yearTextField.text = cardDetail["expiryDateYear"] as? String ?? ""
    }
}
