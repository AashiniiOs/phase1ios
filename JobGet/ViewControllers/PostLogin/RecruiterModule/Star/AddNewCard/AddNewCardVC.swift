//
//  AddNewCardVC.swift
//  JobGet
//
//  Created by Admin on 10/07/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import BraintreeDropIn
import Braintree

class AddNewCardVC: UIViewController {
    
    enum PickerType{
        
        case year
        case month
        case none
    }
    
    // MARK: - Properties
    // MARK: -
    var cardDetail = JSONDictionary()
    var pickerType = PickerType.none
    var phoneFormatter = PhoneNumberFormatter()
    weak var delegate: UpdateCardListDelegate?
    
    var isCreditCard = false
    
    // MARK: - Outlets.
    // MARK: -
    
    @IBOutlet weak var cardTableView: UITableView!
    @IBOutlet weak var navigationLabel: UILabel!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var addCardButton: UIButton!
    
    
    // MARK: - View life cycle.
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.view.endEditing(true)
    }
    
    // MARK: - Action
    // MARK: -
    
    @IBAction func backButtonTapp(_ sender: UIButton) {
        self.pop()
    }
    
    @IBAction func addCardButtonTapp(_ sender: UIButton) {
        self.view.endEditing(true)
        self.cardDetail["cardNumber"] = (self.cardDetail["cardNumber"] as? String ?? "").replace(string: "-", withString: "")
        if self.validation(){
            // For client authorization,
            // get your tokenization key from the Control Panel
            // or fetch a client token
            AppNetworking.showLoader()
//            self.cardDetail["cardNumber"] = (self.cardDetail["cardNumber"] as? String ?? "").replace(string: "-", withString: "")
            let braintreeClient = BTAPIClient(authorization: AppUserDefaults.value(forKey: .BrainTree_clientToken).stringValue)!
            let cardClient = BTCardClient(apiClient: braintreeClient)
            let card = BTCard(number: self.cardDetail["cardNumber"] as? String ?? "", expirationMonth: self.cardDetail["expiryDateMonth"] as? String ?? "", expirationYear: self.cardDetail["expiryDateYear"]  as? String ?? "", cvv: nil)
            cardClient.tokenizeCard(card) { (tokenizedCard, error) in
                // Communicate the tokenizedCard.nonce to your server, or handle error
                if error == nil{
                    if let nonce = tokenizedCard?.nonce{
                        print_debug(nonce)
                        self.cardDetail["paymentNonceMethod"] = nonce
                    }
                     self.addCardApi()
                    
                }else{
                    AppNetworking.hideLoader()
                     print_debug(error?.localizedDescription)
                }
            }
        }
    }
}

// MARK: - Private methods.
// MARK: -
extension AddNewCardVC{
    
    func initialSetup(){
        
        self.cardTableView.delegate = self
        self.cardTableView.dataSource = self
        if AppUserDefaults.value(forKey: .BrainTree_clientToken).stringValue.isEmpty{
            self.getClientTocken()
        }
    }
    
    func getClientTocken(){
        
        WebServices.getBrainTreeClientToken(parameters: ["":""], loader: false, success: { (json) in
            AppUserDefaults.save(value: json["data"]["clientToken"].stringValue, forKey: .BrainTree_clientToken)
        }) { (error) in
            print_debug(error.localizedDescription)
        }
    }
    
    func addCardApi(){
        if self.isCreditCard{
             self.cardDetail["debit"] = "No"
        }else{
             self.cardDetail["debit"] = "Yes"
        }
        self.cardDetail["cardNumber"] = (self.cardDetail["cardNumber"] as? String ?? "").replace(string: " ", withString: "")
        WebServices.addCard(parameters: self.cardDetail, loader: true, success: { (json) in
            self.delegate?.UpdateCardListDelegate()
            self.pop()
        }) { (error) in
            
            CommonClass.showToast(msg: error.localizedDescription)
        }
    }
    
    func validation()->Bool{
        if let name = self.cardDetail["cardOwnerName"] as? String{
            if name.composedCount < 2{
                CommonClass.showToast(msg: StringConstants.card_holder_name_limit)
                return false
            }
        }else{
            CommonClass.showToast(msg: StringConstants.plz_enter_card_holder_name)
            return false
        }
        
        if let cardNumber = self.cardDetail["cardNumber"] as? String{
            if cardNumber.composedCount < 16{
                CommonClass.showToast(msg: StringConstants.plz_enter_valid_card)
                return false
            }
        }else{
            CommonClass.showToast(msg: StringConstants.plz_enter_card_number)
            return false
        }
        if let _ = self.cardDetail["expiryDateMonth"]{
            
        }else{
            CommonClass.showToast(msg: StringConstants.please_select_expiry_month)
            return false
        }
        
        if let _ = self.cardDetail["expiryDateYear"]{
            
        }else{
            CommonClass.showToast(msg: StringConstants.please_select_expiry_year)
            return false
        }
        
        if let cardNumber = self.cardDetail["cvv"] as? String{
            if cardNumber.composedCount < 3{
                CommonClass.showToast(msg: StringConstants.please_enter_valid_cvv)
                return false
            }
        }else{
            CommonClass.showToast(msg: StringConstants.please_enter_cvv)
            return false
        }
        
        return true
    }
}

// MARK: - Table view delegate and datasource methods.
// MARK: -
extension AddNewCardVC: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0,1,3:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "AddNewCardNormalTextFieldCell", for: indexPath) as? AddNewCardNormalTextFieldCell else{fatalError("AddNewCardNormalTextFieldCell not found.")}
            cell.cardTextField.delegate = self
            cell.populatedCell(index: indexPath.row,cardDetail: self.cardDetail)
            return cell
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "AddNewCardPickerTextFieldCell", for: indexPath) as? AddNewCardPickerTextFieldCell else{fatalError("AddNewCardPickerTextFieldCell not found.")}
            cell.monthTextField.delegate = self
            cell.yearTextField.delegate = self
            cell.populatedCell(cardDetail: self.cardDetail)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 2{
            return 100
        }else{
            return 60
        }
    }
}

// MARK: - TextField delegate methods.
// MARK: -
extension AddNewCardVC: UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        guard let indexPath = textField.tableViewIndexPath(self.cardTableView) else{return false}
        
        if indexPath.row == 2 {
            guard let cell = textField.tableViewCell as? AddNewCardPickerTextFieldCell else{return false}
            
            MultiPicker.noOfComponent = 1
            if textField === cell.monthTextField{
                self.pickerType = .month
                
                MultiPicker.openMonthPicker(cell.monthTextField,  titles: ["","",""], doneBlock: { (month, str1, ind) in
                    
                    self.cardDetail["expiryDateMonth"] = month
                    cell.monthTextField.text = month
                    
                })
            }else{
                
                MultiPicker.openYearPicker(cell.yearTextField,  titles: ["","",""], doneBlock: { (year, str1, ind) in
                    self.cardDetail["expiryDateYear"] = year
                    cell.yearTextField.text = year
                })
                
                self.pickerType = .year
                
            }
            self.cardTableView.reloadRows(at: [IndexPath(row:2, section: 0)], with: .none)
        }else{
            textField.inputView = nil
            textField.inputAccessoryView = nil
            
            if indexPath.row == 0{
                textField.keyboardType = .asciiCapable
            }else{
                textField.keyboardType = .numberPad
            }
        }
        
        return true
        
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.cardTableView.reloadRows(at: [IndexPath(row:2, section: 0)], with: .none)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.cardTableView.reloadRows(at: [IndexPath(row: 2, section: 0)], with: .none)
        guard let indexPath = textField.tableViewIndexPath(self.cardTableView) else{return }
        if indexPath.row == 2 {
            guard let cell = textField.tableViewCell as? AddNewCardPickerTextFieldCell else{return }
            
            if textField === cell.monthTextField{
                self.cardDetail["expiryDateMonth"] = textField.text
            }else{
                self.cardDetail["expiryDateYear"] = textField.text
            }
            
        }else{
            
            switch indexPath.row {
            case 0:
                
                self.cardDetail["cardOwnerName"] = textField.text
            case 1:
                self.cardDetail["cardNumber"] = textField.text
            default:
                self.cardDetail["cvv"] = textField.text
            }
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let indexPath = textField.tableViewIndexPath(self.cardTableView) else{return false}
        
        switch indexPath.row {
        case 0:
            if (textField.text ?? "").count == 0, string == " " {
                return false
            } else if (textField.text ?? "").count - range.length < 25 {
                
                let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
                let filtered = string.components(separatedBy: cs).joined(separator: "")
                
                return (string == filtered)
                
            } else {
                return false
            }
//            return (textField.text ?? "").count - range.length < 25
        case 1:
//            let separator = " "
//            let formatter = NumberFormatter()
//            formatter.groupingSeparator = separator
//            formatter.groupingSize = 4
//            formatter.usesGroupingSeparator = true
//            if var number = textField.text, string != "" {
//                number = number.replacingOccurrences(of: separator, with: "")
//                if let doubleVal = Double(number) {
//                    let requiredString = formatter.string(from: NSNumber.init(value: doubleVal))
//                    textField.text = requiredString
//                }
//            }
            if textField.text!.count > 0 && textField.text!.count % 5 == 0 && textField.text!.last! != "-" {
                textField.text!.insert("-", at:textField.text!.index(textField.text!.startIndex, offsetBy: textField.text!.count-1) )
            }
            return (textField.text ?? "").count - range.length < 19
        default:
            return (textField.text ?? "").count - range.length < 4
        }
    }
}

