//
//  MonthlyStarPurchagePlanVC.swift
//  JobGet
//
//  Created by Admin on 27/09/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import StoreKit
import TTTAttributedLabel

protocol UpdateSubscriptionListDelegate: class {
    func updateSubscriptionListDelegate()
}
class MonthlyStarPurchagePlanVC: UIViewController {
    
    // MARK: - Outlets.
    // MARK: -
    @IBOutlet weak var planTypeLabel: UILabel!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var purchaseDateLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var stackviewHight: NSLayoutConstraint!
    @IBOutlet weak var planTitleLabel: UILabel!
    @IBOutlet weak var descriptionDeatilLabel: UILabel!
    @IBOutlet weak var expireDateLabel: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var purchaseButton: UIButton!
    @IBOutlet weak var labelDescriptionAdded: TTTAttributedLabel!
    
    
    // MARK: - Properties
    // MARK: -
    var product = SKProduct()
    var featurePostModel :FeaturedPostModel?
    var subscriptionDetail = JSONDictionary()
    var isSubscription = false
    var monthlySubscription :MonthlySubscription?
    weak var delegate: UpdateSubscriptionListDelegate?
    
    // MARK: - View life cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Actions
    // MARK: -
    
    @IBAction func backButtonTapp(_ sender: UIButton) {
        self.pop()
    }
    
    @IBAction func purchaseButtonTapp(_ sender: UIButton) {
        let alertController = UIAlertController(title: "", message: StringConstants.Are_you_sure_you_want_to_purchase_this_plan, preferredStyle: .alert)
        let yesAction = UIAlertAction(title: StringConstants.yes.uppercased(), style: .default, handler: {
            action in
            AppNetworking.showLoader()
            
            //MARK: Start purchasing product by calling follwing method
            
            
            IAPController.shared.purchaseProduct(product: self.product, completion: { (purchasedPID, restoredPID, failedPID) in
                
                if failedPID.isEmpty && !purchasedPID.isEmpty {
                    self.getAppReceipt()
                }
                AppNetworking.hideLoader()
            })
            
        })
        
        let noAction = UIAlertAction(title: StringConstants.no.uppercased(), style: .cancel,handler: {
            action in
        })
        
        alertController.addAction(yesAction)
        alertController.addAction(noAction)
        self.present(alertController, animated: true, completion: nil)
    }
}

// MARK: - Private methods.
// MARK: -
extension MonthlyStarPurchagePlanVC{
    
    func initialSetup(){
        if self.isSubscription{
            self.stackView.isHidden = false
            self.purchaseButton.isHidden = true
        }else{
            self.purchaseButton.isHidden = false
            self.stackviewHight.constant = 0
            self.stackView.isHidden = true
        }
        guard let featurePostData = self.featurePostModel else{return}
        descriptionDeatilLabel.text = featurePostData.description
        if let data = self.monthlySubscription {
            if let startDate = data.startDate, let endDate = data.endDate{
                let purchaseDate = self.convertedDate(date: startDate)
                let expireDate = self.convertedDate(date: endDate)
                self.purchaseDateLabel.attributedText = self.getAttributedText(wholeString: "\(StringConstants.Purchase_Date) \(purchaseDate)", normalStrings: purchaseDate, attributedString: StringConstants.Purchase_Date)
                self.expireDateLabel.attributedText = self.getAttributedText(wholeString: "\(StringConstants.Expire_Date) \(expireDate)", normalStrings: expireDate, attributedString: StringConstants.Expire_Date)
            }
        }
        
        self.priceLabel.attributedText = self.getAttributedText(wholeString: "\(StringConstants.Plan_Price) $\(featurePostData.planAmount)", normalStrings: "\(featurePostData.planAmount)", attributedString: "$\(StringConstants.Plan_Price)")
        
        self.planTypeLabel.attributedText = self.getAttributedText(wholeString: "\(StringConstants.Subscription_Type) \(StringConstants.Monthly)", normalStrings: StringConstants.Monthly, attributedString: StringConstants.Subscription_Type)
        self.labelDescriptionAdded.delegate = self
        
        getAddedDescriptionLabelText()
    }
    
    func convertedDate(date: Date)-> String{
        
        let frmtr = DateFormatter()
        frmtr.locale = Locale(identifier: "en_US_POSIX")
        frmtr.dateFormat = "d MMM, yy"
        let stringDate = frmtr.string(from: date)
        return stringDate
    }
    
    //MARK: Get latest app receipt
    func getAppReceipt(){
        
        IAPController.shared.fetchIAPReceipt(sharedSecrete: StringConstants.InAppPurchaseSecrateKey, success: { (receipt) in
            DispatchQueue.main.async {
                print_debug(receipt)
                var transactionId: String?
                if let receiptData = receipt as? JSONDictionary, let receipts = receiptData["receipt"] as? JSONDictionary, let in_app = receipts["in_app"] as? [JSONDictionary]{
                    
                    let _ = in_app.map{ inApp in
                        transactionId = inApp["transaction_id"] as? String
                        self.subscriptionDetail["purchase_date"] = inApp["purchase_date"] as? String ?? ""
                        self.subscriptionDetail["expires_date"] = inApp["expires_date"] as? String ?? ""
                    }
                }
                
                if let transaction_id = transactionId, let latest_receipt = receipt["latest_receipt"] as? String, let planData = self.featurePostModel{
                    let params: JSONDictionary = ["amount":planData.planAmount,
                                                  "paymentToken":latest_receipt,
                                                  "planId":planData.planId,
                                                  "stars":planData.stars,
                                                  "transactionId":transaction_id,
                                                  "planName": planData.planName
                    ]
                    self.buySubscription(params: params)
                }
            }
        }, failure: { (error) in
            AppNetworking.hideLoader()
            CommonClass.showToast(msg: error?.localizedDescription ?? "")
        })
    }
    
    func buySubscription(params: JSONDictionary){
        
        WebServices.buySubscription(parameters: params, loader: true, success: { (json) in
            self.delegate?.updateSubscriptionListDelegate()
            self.popToRootViewController()
        }) { (error) in
            
            CommonClass.showToast(msg: error.localizedDescription)
        }
    }
    
    func getAttributedText(wholeString : String, normalStrings: String, attributedString:String)-> NSAttributedString {
        
        let attributedText = NSMutableAttributedString(string: wholeString)
        
        attributedText.addAttributes([NSAttributedStringKey.foregroundColor:AppColors.gray152, NSAttributedStringKey.font: AppFonts.Poppins_SemiBold.withSize(14)], range: (wholeString as NSString).range(of: wholeString))
        
        attributedText.addAttributes([NSAttributedStringKey.foregroundColor:AppColors.black46, NSAttributedStringKey.font: AppFonts.Poppins_SemiBold.withSize(14)], range: (wholeString as NSString).range(of: attributedString))
        return attributedText
    }
    
    
    
    func getAddedDescriptionLabelText(){
        
        let str = "You’ll pay $\(featurePostModel?.planAmount ?? "")/month ,\ncancel anytime, for any reason\nBy placing this order, you agree to JobGet’s Terms & Conditions and Privacy Policy. Your iTunes Account will be charged $\(featurePostModel?.planAmount ?? "") each month once the free trial ends.Subscription automatically renews unless auto-renew is turned off at least 24-hours before the end of the current period. You can manage your subscription and turn off auto-renew from your iTunes Account settings."
        let attributedStr = NSMutableAttributedString(string: str)
        attributedStr.addAttributes([NSAttributedStringKey.foregroundColor:AppColors.gray152, NSAttributedStringKey.font: AppFonts.Poppins_Regular.withSize(12)], range: (str as NSString).range(of: str))
        attributedStr.addAttributes([NSAttributedStringKey.foregroundColor:AppColors.appBlue, NSAttributedStringKey.font: AppFonts.Poppins_SemiBold.withSize(13)], range: (str as NSString).range(of: "Terms & Conditions"))
        attributedStr.addAttributes([NSAttributedStringKey.foregroundColor:AppColors.appBlue, NSAttributedStringKey.font: AppFonts.Poppins_SemiBold.withSize(13)], range: (str as NSString).range(of: "Privacy Policy"))
        self.labelDescriptionAdded.setText(attributedStr)
        let termsnConditionsRange = (str as NSString).range(of: "Terms & Conditions")
        let termsURL = URL(string:"action://TC")
        labelDescriptionAdded.addLink(to: termsURL, with:termsnConditionsRange)
        let privacyPolicyRange = (str as NSString).range(of: "Privacy Policy")
        let privacyURL = URL(string:"action://PP")
        labelDescriptionAdded.addLink(to: privacyURL, with:privacyPolicyRange)
    }
}

extension MonthlyStarPurchagePlanVC : TTTAttributedLabelDelegate{
    
    func attributedLabel(_ label: TTTAttributedLabel!, didSelectLinkWith url: URL!) {
        if url.absoluteString == "action://TC" {
            let staticPage = StaticPageVC.instantiate(fromAppStoryboard: .settingsAndChat)
            staticPage.webViewType = .terms
            self.navigationController?.pushViewController(staticPage, animated: true)
        }
        else if url.absoluteString == "action://PP" {
            let staticPage = StaticPageVC.instantiate(fromAppStoryboard: .settingsAndChat)
            staticPage.webViewType = .privacy
            self.navigationController?.pushViewController(staticPage, animated: true)
        }
    }
}
