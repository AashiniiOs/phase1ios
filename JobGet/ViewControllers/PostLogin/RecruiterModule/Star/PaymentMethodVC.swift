//
//  PaymentMethodVC.swift
//  JobGet
//
//  Created by macOS on 10/07/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import MXParallaxHeader

enum CardType{
    
    case creditCard
    case debitCard
    case none
}

class PaymentMethodVC: BaseVC {
    
    //MARK:- Properties
    //==================
     var headerImage = UIImage()
     var planDetail = JSONDictionary()
     var featurePostModel: FeaturedPostModel?
     var isPremiumPlan = false
     var isOpenFromJobPost = false
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var packageNameLabel: UILabel!
    @IBOutlet weak var packageCostLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    @IBOutlet weak var starCountLabel: UILabel!
    @IBOutlet weak var validityLabel: UILabel!
    @IBOutlet weak var blurView: UIView!
    
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- Private Methods
    //=======================
    private func initialSetup() {
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        self.setupViewAndRadius()
    }
    
    private func setupViewAndRadius() {
        self.navigationView.setShadow()
        self.bgImageView.image = self.headerImage
        if let data = self.featurePostModel{
            
            self.packageNameLabel.text = data.planName
            self.packageCostLabel.text = "$\(data.planAmount)"
            self.descLabel.text = data.description
        }
        self.blurView.setCorner(cornerRadius: 5.0, clip: true)
    }
    //    private func setupParallexHeader() {
    //        self.bgImageView.image = self.headerImage
    //        self.tableView.parallaxHeader.view = self.headerView
    //        self.tableView.parallaxHeader.height = 300
    //        self.tableView.parallaxHeader.mode = MXParallaxHeaderMode.fill
    //        self.tableView.parallaxHeader.minimumHeight = 1
    //    }
    
    //MARK:- IBAction & Target
    //=======================
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.pop()
    }
}

//MARK:-
//MARK:- Table view Delegate and DataSource
extension PaymentMethodVC: UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: ChooseCardCell.identifier) as? ChooseCardCell else { fatalError("invalid cell \(self)")
            }
            return cell
        } else if indexPath.row == 1 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: DebitCardCell.identifier) as? DebitCardCell else { fatalError("invalid cell \(self)")
            }
            cell.cardCollectionView.delegate = self
            cell.cardCollectionView.dataSource = self
            return cell
        }
        else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: SelectBtnCell.identifier) as? SelectBtnCell else { fatalError("invalid cell \(self)")
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.0001
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 1 {
            return 150
        }else if indexPath.row == 2 {
            return 100
        } else {
            return UITableViewAutomaticDimension
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print_debug(indexPath)
    }
    
    
}
//MARK: - Collection view Delegate and DataSource
//============================================
extension PaymentMethodVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CardCollectionViewCell.identifier, for: indexPath) as? CardCollectionViewCell else { fatalError("invalid collection view cell \(self)") }
        
        if indexPath.item == 0{
            cell.cardLabel.text = StringConstants.debitCard
        }else{
            cell.cardLabel.text = StringConstants.creditCard
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: (collectionView.frame.width/2) - 12, height: 130)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print_debug(indexPath)
        
        guard let cell = collectionView.cellForItem(at: indexPath) as? CardCollectionViewCell else { return }
        cell.cardLabel.textColor = AppColors.themeBlueColor
        
        let savecCrdList = SaveCardListVC.instantiate(fromAppStoryboard: .Star)
        
        if indexPath.item == 0{
            savecCrdList.cardType = .debitCard
        }else{
            savecCrdList.cardType = .creditCard
        }
        savecCrdList.isOpenFromJobPost = self.isOpenFromJobPost
        savecCrdList.planDetail = planDetail
        savecCrdList.isPremiumPlan = isPremiumPlan
        
        self.navigationController?.pushViewController(savecCrdList, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? CardCollectionViewCell else { return }
        cell.cardLabel.textColor = AppColors.black46
        
    }
}


//MARK:- Prototype Cell
//=========================
class ChooseCardCell: UITableViewCell {
    
    //MARK:- Properties
    //==================
    static var identifier: String {
        return String(describing: self)
    }
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var upperLabel: UILabel!
    @IBOutlet weak var lowerLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.upperLabel.textColor = AppColors.black46
        self.lowerLabel.textColor = AppColors.gray152
        //        #imageLiteral(resourceName: "icProfileAdd")
    }
}

class DebitCardCell: UITableViewCell {
    
    //MARK:- Properties
    //==================
    static var identifier: String {
        return String(describing: self)
    }
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var cardCollectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

class SelectBtnCell: UITableViewCell {
    
    
    //MARK:- Properties
    //==================
    static var identifier: String {
        return String(describing: self)
    }
    
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var selectBtn: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        CommonClass.setNextButton(button: selectBtn)
    }
}

class CardCollectionViewCell: UICollectionViewCell {
    
    //MARK:- Properties
    //==================
    static var identifier: String {
        return String(describing: self)
    }
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var cardImageBtn: UIButton!
    @IBOutlet weak var cardLabel: UILabel!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var radiusView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.cardImageBtn.roundCorners()
        self.radiusView.setCorner(cornerRadius: 5.0, clip: true)
        self.cardLabel.textColor = AppColors.black46
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.shadowView.dropShadow(color: AppColors.gray152, opacity:0.2, offSet: CGSize(width: 0.0, height: 0.0), radius: 1.0)
    }
    
    override var isHighlighted: Bool{
        didSet{
            if isHighlighted {
                UIView.animate(withDuration: 0.3, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 1.0, options: .curveEaseOut, animations: {
                    self.transform = self.transform.scaledBy(x: 0.88, y: 0.88)
                }, completion: { (true) in
                    UIView.animate(withDuration: 0.2, animations: {
                        self.transform = CGAffineTransform.identity //.scaledBy(x: 1.0, y: 1.0)
                    }, completion: nil)
                    
                })
            }
            //            } else {
            //                UIView.animate(withDuration: 1.0, delay: 0.0, usingSpringWithDamping: 0.4, initialSpringVelocity: 1.0, options: .curveEaseOut, animations: {
            //
            //                }, completion: nil)
            //            }
        }
    }
}
