//
//  SaveCardListVC.swift
//  JobGet
//
//  Created by Admin on 13/09/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

protocol UpdateCardListDelegate: class {
    func UpdateCardListDelegate()
}



class SaveCardListVC: UIViewController {
    
    
    // MARK: - Properties.
    // MARK: -
    var selectedIndexPath: Int?
    var cardType = CardType.none
    var cardList = [CardListModel]()
    var planDetail = JSONDictionary()
    var cvvText : String?
    var isPremiumPlan = false
    var isOpenFromJobPost = false
    
    // MARK: - Outlets.
    // MARK: -
    
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var navigationTitleLabel: UILabel!
    @IBOutlet weak var paymentButton: UIButton!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var cardTableView: UITableView!
    
    // MARK: - View life cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - Actions.
    // MARK: -
    
    @IBAction func backButtonTapp(_ sender: UIButton) {
        self.pop()
    }
    
    @IBAction func deleteButtonTapp(_ sender: UIButton) {
        
        guard let _ = self.selectedIndexPath else{return}
        let popUpVc = AlertPopUpVC.instantiate(fromAppStoryboard: .Candidate)
        popUpVc.delegate = self
        popUpVc.alertType = .cardDelete
        popUpVc.modalPresentationStyle = .overCurrentContext
        popUpVc.modalTransitionStyle = .crossDissolve
        let rootViewController = UIApplication.shared.keyWindow?.rootViewController
        rootViewController?.present(popUpVc, animated: true, completion: nil)
        
    }
    
    @IBAction func paymentButtonTapp(_ sender: UIButton) {
        
        self.view.endEditing(true)
        if let _ = self.cvvText{
            if self.isPremiumPlan{
                self.buyPremium()
            }else{
                self.createTransaction()
            }
            
        }else{
           CommonClass.showToast(msg: StringConstants.please_enter_cvv)
        }
    }
}

// MARK: - Private methods.
// MARK: -
private extension SaveCardListVC{
    
    func initialSetup(){
        
        if self.cardType == CardType.debitCard{
            self.navigationTitleLabel.text = StringConstants.debitCard.uppercased()
        }else if self.cardType == CardType.creditCard{
            self.navigationTitleLabel.text = StringConstants.creditCard.uppercased()
        }
        self.cardTableView.delegate = self
        self.cardTableView.dataSource = self
        self.isEnableLoginBtn(status: false)
        self.getCardListApi()
    }
    
    func isEnableLoginBtn(status: Bool) {
        
        status ? (self.paymentButton.backgroundColor = AppColors.themeBlueColor) : (self.paymentButton.backgroundColor = AppColors.buttonDisableBgColor)
        self.paymentButton.isEnabled = status
    }
    
    func getAttributedText(wholeString : String, attributedString : String)-> NSAttributedString {
        
        let attributedText = NSMutableAttributedString(string: wholeString)
        
        attributedText.addAttributes([NSAttributedStringKey.foregroundColor:AppColors.black46, NSAttributedStringKey.font: AppFonts.Poppins_SemiBold.withSize(14)], range: (wholeString as NSString).range(of: wholeString))
        attributedText.addAttributes([NSAttributedStringKey.foregroundColor:AppColors.black46, NSAttributedStringKey.font: AppFonts.Poppins_Bold.withSize(22)], range: (attributedString as NSString).range(of: attributedString))
        
        return attributedText
    }
    
    func setStringAsCardNumberWithSartNumber(Number:Int,withString str:String ,withStrLenght len:Int ) -> String{
        
        var CrediteCard : String = ""
        if str.count > (Number + len) {
            for (index, element ) in str.enumerated(){
                if index >= Number && index < (Number + len) {
                    if index == 3 || index == 7 || index == 11 {
                        CrediteCard = CrediteCard + String("\u{2022} ")
                    }else{
                        CrediteCard = CrediteCard + String("\u{2022}")
                    }
                    
                }else{
                    CrediteCard = CrediteCard + String(element)
                }
            }
            return CrediteCard
        }else{
            print_debug("\(Number) plus \(len) are grether than strings chatarter \(str.count)")
        }
        print_debug("\(CrediteCard)")
        return str
    }
    
    func buyPremium(){
        
        guard let index = self.selectedIndexPath else {return }
        guard let planamount = Double(self.planDetail["amount"] as? String ?? "0.00")   else{return}
        self.planDetail["amount"] = "\(planamount.rounded(toPlaces: 2))"
        let param : JSONDictionary = ["customerId":self.cardList[index].customerId,
                                      "token":self.cardList[index].token,
                                      "planId":self.planDetail["planId"] ?? "",
                                      "amount":  self.planDetail["amount"] ?? "0.0",
                                      "cvv":self.cvvText ?? "",
                                      "planName":self.planDetail["planName"] ?? "",
                                      "paymentMethod":"test",
                                      "jobId": self.planDetail["jobId"] ?? "",
                                      "cardType": self.cardList[index].cardType
        ]
        WebServices.buyPremium(parameters: param, loader: true, success: { (json) in
            
            let sceen = ResetPasswordPopupVC.instantiate(fromAppStoryboard: .Main)
            commingFrom = .cardList
            sceen.delegate = self
            self.view.addSubview(sceen.view)
            self.add(childViewController: sceen)
        }) { (error) in
            CommonClass.showToast(msg: error.localizedDescription)
        }
    }
    
    func createTransaction(){

        
        guard let planamount = self.planDetail["amount"] as? Double else{return}
        self.planDetail["amount"] = "\(planamount.rounded(toPlaces: 2))"
        guard let index = self.selectedIndexPath else {return }
        let param : JSONDictionary = ["customerId":self.cardList[index].customerId,
                                      "token":self.cardList[index].token,
                                      "planId":self.planDetail["planId"] ?? "",
                                      "amount":self.planDetail["amount"] ?? "0.0",
                                      "cvv":self.cvvText ?? "",
                                      "planName":self.planDetail["planName"] ?? ""
                                    ]
        WebServices.createTransaction(parameters: param, loader: true, success: { (json) in
           
            guard let navArray = self.navigationController?.viewControllers else{return}
            
            for vc in navArray{
                if let viewcontroller = vc as? JobDetailVC {
                    
                    self.navigationController?.popToViewController(viewcontroller, animated: true)
                }
                
            }
        }) { (error) in
            CommonClass.showToast(msg: error.localizedDescription)
        }
    }
    
    
    func getCardListApi(){
        
        WebServices.getCardList(parameters: ["":""], loader: true, success: { (json) in
            
            self.cardList.removeAll()
            let creditCards = json[ApiKeys.data.rawValue]["creditCards"].arrayValue
            for cards in creditCards {
                self.cardList.append(CardListModel(withJson: cards))
            }
            self.cardTableView.reloadData()
        }) { (error) in
            CommonClass.showToast(msg: error.localizedDescription)
        }
    }
}

// MARK: - Table view delegate and datasource methods.
// MARK: -
extension SaveCardListVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            return self.cardList.count
        }else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0.0, y: 0.0, width: tableView.width, height: 30))
        
        let titleLabel = UILabel(frame: CGRect(x: 15.0, y: 5.0, width: tableView.width-30, height: 25))
        
        if section == 0 && !self.cardList.isEmpty{
            titleLabel.font = AppFonts.Poppins_Medium.withSize(14.0)
            titleLabel.textColor = AppColors.black46
            titleLabel.text = StringConstants.SelectCardForPayment
        }else{
            titleLabel.text = ""
        }
        headerView.backgroundColor = AppColors.tableViewBgColor
        headerView.addSubview(titleLabel)
        return headerView
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return 30
        }else{
            return .leastNormalMagnitude
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SaveCardCell", for: indexPath) as? SaveCardCell else{fatalError("SaveCardCell not found.")}
            
            let cardNumber =  self.setStringAsCardNumberWithSartNumber(Number: 0, withString: self.cardList[indexPath.row].maskedNumber, withStrLenght: 12)
            
            let bullat = cardNumber.replace(string: String(cardNumber.suffix(4)), withString: "")
            print_debug(cardNumber)
            cell.cardNoLabel.attributedText = self.getAttributedText(wholeString: cardNumber, attributedString: bullat)
            cell.cardHolderNameLabel.text = self.cardList[indexPath.row].cardholderName
            cell.cardTypeImageView.sd_setImage(with: URL(string: self.cardList[indexPath.row].imageUrl), completed: nil)
            if let index = self.selectedIndexPath, index == indexPath.row{
                cell.cvvTextField.isHidden = false
                cell.selecteImage.image = #imageLiteral(resourceName: "ic_radio_button_checked1")
                cell.cvvTextField.delegate = self
                
            }else{
                cell.selecteImage.image = #imageLiteral(resourceName: "ic_radio_button_unchecked1")
                cell.cvvTextField.isHidden = true
            }
            return cell
            
        }else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "AddCardCell", for: indexPath) as? AddCardCell else{fatalError("AddCardCell not found.")}
            cell.addCardButton.addTarget(self, action: #selector(self.addCardButtonTapp(_:)), for: .touchUpInside)
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.section == 0{
            if let index = self.selectedIndexPath, index == indexPath.row{
                self.selectedIndexPath = nil
                self.isEnableLoginBtn(status: false)
            }else{
                self.selectedIndexPath = indexPath.row
                self.isEnableLoginBtn(status: true)
            }
            tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
             if let index = self.selectedIndexPath, index == indexPath.row{
                return 130
             }else{
                return 100
            }
        }else{
            return 40
        }
    }
    
    @objc func addCardButtonTapp(_ sender: UIButton){
        let addCardVC = AddNewCardVC.instantiate(fromAppStoryboard: .Star)
        addCardVC.delegate = self
        if self.cardType == .creditCard{
            addCardVC.isCreditCard = true
        }else if self.cardType == .debitCard{
             addCardVC.isCreditCard = false
        }
        self.navigationController?.pushViewController(addCardVC, animated: true)
    }
}

extension SaveCardListVC: UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        self.cvvText = textField.text
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if string == " " {
            return false
        }
        return (textField.text ?? "").count - range.length < 4
    }
}


extension SaveCardListVC: AlertPopUpDelegate , UpdateCardListDelegate, ShouldOpenStatusPopup{
    
    func UpdateCardListDelegate(){
        self.getCardListApi()
    }
    func didTapAffirmativeButton() {
        guard let index = self.selectedIndexPath else{ return}
        let param: JSONDictionary = ["token": self.cardList[index].token]
        
        WebServices.deleteCard(parameters: param , loader: true, success: { (json) in
            self.cardList.remove(at: index)
            CommonClass.showToast(msg: json[ApiKeys.message.rawValue].stringValue)
            self.cardTableView.reloadData()
        }) { (error) in
            CommonClass.showToast(msg: error.localizedDescription)
        }
        //        if self.removeType == .single{
        //            self.deleteSingleNotificationApi()
        //        }else if self.removeType == .all{
        //            self.deleteNotificationList()
        //        }
    }
    
    func presentShortlistedPopup(jobApplyId: String, categoryId: String) {
        
        if let vc = self.navigationController?.viewControllers {
            for controllers in vc{
                if controllers is RecruiterTabBarVC {
                    if self.isOpenFromJobPost{
                        NotificationCenter.default.post(name: NSNotification.Name("Animating_Sptlight_Icon"), object: ["jobId" :self.planDetail["jobId"] ?? ""])
                    }
                    self.navigationController?.popToViewController(controllers, animated: true)
                    
                }
            }
        }
    }
    
    func didTapNegativeButton() {
        // To do Task
    }
}

// MARK: - Table view cell classes
// MARK: -

class SaveCardCell: UITableViewCell{
    
    @IBOutlet weak var cvvTextField: UITextField!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var selecteImage: UIImageView!
    @IBOutlet weak var cardNoLabel: UILabel!
    @IBOutlet weak var cardHolderNameLabel: UILabel!
    @IBOutlet weak var expiryDateLabel: UILabel!
    @IBOutlet weak var cardTypeImageView: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.bgView.dropShadow(color: AppColors.gray152, opacity:0.2, offSet: CGSize(width: 0.0, height: 0.0), radius: 1.0)
    }
}


class AddCardCell: UITableViewCell{
    
    @IBOutlet weak var addCardImageView: UIImageView!
    @IBOutlet weak var addCardLabel: UILabel!
    

    @IBOutlet weak var addCardButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.addCardImageView.image = self.addCardImageView.image?.withRenderingMode(.alwaysTemplate)
        self.addCardImageView.tintColor = AppColors.themeBlueColor
        
    }
}
