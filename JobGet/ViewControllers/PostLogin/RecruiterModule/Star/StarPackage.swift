
//
//  StarPackage.swift
//  JobGet
//
//  Created by macOS on 10/07/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import StoreKit

protocol UpdateTransactionListDelegate: class {
    func updateTransactionListDelegate()
}

class StarPackage: BaseVC {
    
    //MARK:- Properties
    //==================
    var featurePostModel = [FeaturedPostModel]()
    var monthlySubscription :MonthlySubscription?
    let planBgImage      = [#imageLiteral(resourceName: "icHomeBlueCard"), #imageLiteral(resourceName: "icHomeGreenCard"), #imageLiteral(resourceName: "icHomeVoiletBlueCard"), #imageLiteral(resourceName: "icHomeOrangeCard")]
    
    //MARK: Array of all products fetched from Appstore.
    fileprivate var iapProducts = [SKProduct]()
    
    //MARK: Set of all product identifiers available to be sold.
    var iapProductIdentifiers:Set<String>!
    
    //MARK: When purchase completes, this completion block will be called and an individual set for purchased product identifiers, restored product identifiers and failed product identifiers will be received.
    var purchaseCompletionBlock:((_ purchasedPID:Set<String>,_ restoredPID:Set<String>,_ failedPID:Set<String>)->Void)!
    
    var indexPath : IndexPath?
    weak var delegate: UpdateTransactionListDelegate?
    var isFromChat = false
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var bankLabel: UILabel!
    @IBOutlet weak var packageTypeLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var outOfStarsLabel: UILabel!
    @IBOutlet weak var totalStarsLabel: UILabel!
    
    @IBOutlet weak var starsView: UIView!
    
    @IBOutlet weak var starTableView: UITableView!
    @IBOutlet weak var navigationView: UIView!
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // DispatchQueue.global().async {
        self.purchaseCompletionBlock = {
            
            (purchasedPID,restoredPID,failedPID) in
            
            AppNetworking.hideLoader()
            
            //MARK:- Get restoredPID empty when user succesfully purchase the subscription
            DispatchQueue.main.async {
                
                if failedPID.isEmpty && !restoredPID.isEmpty {
                    self.getAppReceipt()
                    AppNetworking.hideLoader()
                } else {
                    AppNetworking.hideLoader()
                }
            }
        }
        self.getFeaturedPlan(loader: true)
        //}
        
        let freeStars = AppUserDefaults.value(forKey: .freeStars).intValue
        if AppUserDefaults.value(forKey: .stars).intValue > 1{
            self.totalStarsLabel.text = "\(AppUserDefaults.value(forKey: .stars).stringValue) \(StringConstants.Stars)"
        }else{
            self.totalStarsLabel.text = "\(AppUserDefaults.value(forKey: .stars).stringValue.isEmpty ? "0" : AppUserDefaults.value(forKey: .stars).stringValue) \(StringConstants.Star)"
        }
        self.outOfStarsLabel.text = "\(freeStars) of 3 \(StringConstants.Stars)"
    }
    
    //MARK:- Private Methods
    //=======================
    
    private func initialSetup() {
        
        if self.isFromChat{
            self.backButton.isHidden = false
        }else{
             self.backButton.isHidden = true
        }
       
        self.navigationView.setShadow(offset: CGSize(width: 0.0, height: 2.0))
        self.starsView.dropShadow(color: AppColors.black46, opacity:0.2, offSet: CGSize(width: 1.0, height: 1.0), radius: 2.0)
        self.starTableView.delegate = self
        self.starTableView.dataSource = self
        self.registerCell()
        self.setupPlans()
    }
    
    func setupPlans() {
        
        AppNetworking.showLoader()
        iapProductIdentifiers = [StringConstants.Single_Plan_Key, StringConstants.Basic_Plan_Key,  StringConstants.Preferred_Plan_Key, StringConstants.Premium_Plan_Key ]
        
        IAPController.shared.fetchAvailableProducts(productIdentifiers: iapProductIdentifiers, success: { [weak self] (products) in
            
            self?.iapProducts = products
            print_debug(self?.iapProducts)
            _ = products.map{ product in
                
                switch product.productIdentifier{
                case StringConstants.Single_Plan_Key:
                    self?.iapProducts[0] = product
                case StringConstants.Preferred_Plan_Key:
                    self?.iapProducts[1] = product
                case StringConstants.Premium_Plan_Key:
                    self?.iapProducts[2] = product
                case StringConstants.Basic_Plan_Key:
                    self?.iapProducts[3] = product
                default:
                    break
                }
            }
            self?.starTableView.reloadData()
            }, failure: {
                [weak self] (error) in
                
                if let err = error{
                    
                    let alertController = UIAlertController(title: FETCH_PRODUCT_ERROR, message: err.localizedDescription, preferredStyle: .alert)
                    let alertAction = UIAlertAction(title: OK, style: .cancel, handler: nil)
                    alertController.addAction(alertAction)
                    self?.present(alertController, animated: true, completion: nil)
                }
        })
    }
    
    //MARK: Get latest app receipt
    func getAppReceipt(){
         AppNetworking.showLoader()
        IAPController.shared.fetchIAPReceipt(sharedSecrete: StringConstants.InAppPurchaseSecrateKey, success: { (receipt) in
            DispatchQueue.main.async {
                print_debug(receipt)
                
                var transactionId: String?
                if let receiptData = receipt as? JSONDictionary, let receipts = receiptData["receipt"] as? JSONDictionary, let in_app = receipts["in_app"] as? [JSONDictionary]{
                    
                    let _ = in_app.map{ inApp in
                        transactionId = inApp["transaction_id"] as? String
                    }
                }
                if let index = self.indexPath, let transaction_id = transactionId{
                    self.createTransaction(param:
                        ["transactionId":transaction_id, "planId":self.featurePostModel[index.row].planId,
                         "amount":self.featurePostModel[index.row].planAmount,
                         "planName":self.featurePostModel[index.row].planName,
                         "stars":self.featurePostModel[index.row].stars
//                            ,
//                         "paymentToken":latest_receipt
                        ]
                    )
                }
            }
        }, failure: { (error) in
            AppNetworking.hideLoader()
            CommonClass.showToast(msg: error?.localizedDescription ?? "")
        })
    }
    
    private func registerCell() {
        self.starTableView.register(UINib(nibName: "PackageCell", bundle: nil), forCellReuseIdentifier: PackageCell.identifier)
    }
    
    //MARK:- IBAction & Target
    //=======================
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.pop()
    }
    
}
//MARK:- Web Services
//====================
extension StarPackage {
    
    func getFeaturedPlan(loader: Bool) {
        
        WebServices.getFeaturedPlan(parameters: ["":""], loader: loader, success: { (json) in
            if let code = json["code"].int, code == error_codes.success {
                if json["stars"].intValue > 1{
                    self.totalStarsLabel.text = "\(json["stars"].intValue) \(StringConstants.Stars)"
                }else{
                    self.totalStarsLabel.text = "\(json["stars"].stringValue.isEmpty ? "0" : json["stars"].stringValue) \(StringConstants.Star)"
                }
                self.outOfStarsLabel.text = "\(json["freeStars"].intValue) of \(json["freeStarLimit"].intValue) \(StringConstants.Stars)"
                let data = json["data"].arrayValue
                self.monthlySubscription = MonthlySubscription(dict: json["subscription"])
                self.featurePostModel.removeAll()
                for value in data {
                    self.featurePostModel.append(FeaturedPostModel(dict: value))
                }
            }
//            CommonClass.showToast(msg: json["message"].stringValue )
            self.starTableView.reloadData()
        }) { (error: Error) in
            CommonClass.showToast(msg: error.localizedDescription )
        }
    }
    
    
    func createTransaction(param: JSONDictionary){

//        var params:[String: Any] = param
//        if let amount =  param["amount"] as? Double{
//            params["amount"] = amount.rounded(toPlaces: 2)
//        }
        
        print_debug(param)
        WebServices.inappPurchase(parameters: param, loader: true, success: { (json) in
            AppNetworking.hideLoader()

            AppUserDefaults.save(value: json["totalStars"].intValue, forKey: .stars)
            if AppUserDefaults.value(forKey: .stars).intValue > 1{
                self.totalStarsLabel.text = "\(AppUserDefaults.value(forKey: .stars).stringValue) \(StringConstants.Stars)"
            }else{
                self.totalStarsLabel.text = "\(AppUserDefaults.value(forKey: .stars).stringValue.isEmpty ? "0" : AppUserDefaults.value(forKey: .stars).stringValue) \(StringConstants.Star)"
            }
            if self.isFromChat{
                self.delegate?.updateTransactionListDelegate()
                self.pop()
            }
             CommonClass.showToast(msg: json["message"].stringValue )
            
        }) { (error) in
            
            CommonClass.showToast(msg: error.localizedDescription)
        }
    }
}

//MARK:- Table view Delegate and DataSource
//MARK:-
extension StarPackage: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.featurePostModel.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      
        if indexPath.row == self.featurePostModel.count{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "StarDesciptionTableCell") as? StarDesciptionTableCell else { fatalError("invalid cell \(self)")
            }

            return cell
        }else{
            guard let cell = tableView.dequeueReusableCell(withIdentifier: PackageCell.identifier) as? PackageCell else { fatalError("invalid cell \(self)")
            }
            cell.bgImageView.image = self.planBgImage[indexPath.row % 4]
            if !self.featurePostModel.isEmpty{
                cell.populateStar(data: self.featurePostModel[indexPath.row], index: indexPath.row)
            }
            return cell

        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print_debug(indexPath)
        
        self.indexPath = indexPath
        if self.iapProducts.isEmpty{
            return
        }
        if featurePostModel.count > 4{
            return
        }
        if self.iapProducts[indexPath.row].productIdentifier == StringConstants.Basic_Plan_Key{
            let sceen = MonthlyStarPurchagePlanVC.instantiate(fromAppStoryboard: .Star)
            sceen.product = self.iapProducts[indexPath.row]
            if let detail = self.monthlySubscription, !detail.planId.isEmpty{
                sceen.isSubscription = true
                sceen.monthlySubscription = self.monthlySubscription
            }
            sceen.delegate = self
            sceen.featurePostModel = self.featurePostModel[indexPath.row]
            self.navigationController?.pushViewController(sceen, animated: true)
            
        }else{
            
            let post = self.featurePostModel[indexPath.row]
            
            var text = StringConstants.Would_You_Like_To_Purchase + " "
            
            if post.stars == "1" {
                text += "+\(post.stars) \(StringConstants.Star) "
            }else{
                text += "+\(post.stars) \(StringConstants.Stars) "
            }
            
            text += StringConstants.For + " $\(post.planAmount)"
            
            let alertController = UIAlertController(title: "", message: text, preferredStyle: .alert)
            let yesAction = UIAlertAction(title: StringConstants.yes.uppercased(), style: .default, handler: {
                action in
                AppNetworking.showLoader()
                
                //MARK: Start purchasing product by calling follwing method
                var product = SKProduct()
                
                product = self.iapProducts[indexPath.row]
                IAPController.shared.purchaseProduct(product: product, completion: { (purchasedPID, restoredPID, failedPID) in
                    
                    if failedPID.isEmpty && !purchasedPID.isEmpty {
                        AppNetworking.hideLoader()
                        self.getAppReceipt()
                    }else{
                        AppNetworking.hideLoader()
                    }
                })
            })
            
            let noAction = UIAlertAction(title: StringConstants.no.uppercased(), style: .cancel,handler: {
                action in
                self.indexPath = nil
            })
            
            alertController.addAction(yesAction)
            alertController.addAction(noAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
}

//MARK:- UpdateSubscriptionListDelegate method
//MARK:-
extension StarPackage: UpdateSubscriptionListDelegate{
    
    func updateSubscriptionListDelegate(){
        self.getFeaturedPlan(loader: true)
    }
}


class StarDesciptionTableCell : UITableViewCell{
    
    @IBOutlet weak var starLabelFooterView: UIView!
    @IBOutlet weak var descriptionStarLabel: UILabel!

    
}
