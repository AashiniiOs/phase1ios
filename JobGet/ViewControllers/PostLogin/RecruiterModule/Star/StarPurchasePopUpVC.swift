//
//  StarPurchasePopUpVC.swift
//  JobGet
//
//  Created by Admin on 21/09/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class StarPurchasePopUpVC: UIViewController {

    // MARK: - Properties
    // MARK: -
    
    // MARK: - Outlets
    // MARK: -
    
    @IBOutlet weak var purchaseButton: UIButton!
    
    // MARK: - View life cycle
    // MARK: -
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.initialSetup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: - Actions
    // MARK: -
    
    @IBAction func purchaseButtonTapp(_ sender: UIButton) {
        let starPackege = StarPackage.instantiate(fromAppStoryboard: .Star)
        self.navigationController?.pushViewController(starPackege, animated: true)
    }
    
}

// MARK: - Private methods.
// MARK: -
private extension StarPurchasePopUpVC{
    
    func initialSetup(){
        
        
    }
}
