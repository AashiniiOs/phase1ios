//
//  StarVC.swift
//  JobGet
//
//  Created by macOS on 10/07/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import DZNEmptyDataSet

class StarVC: BaseVC {
    
    //MARK:- Properties
    //==================
    var transactionList = [TransactionList]()
    var totalStars = ""
    var pageNum = 0
    var isPaginationEnable = false
    var shouldLoadMoreData = false
    var isMonthSubscription = false
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var starCountLbl: UILabel!
    @IBOutlet weak var starBalanceLbl: UILabel!
    @IBOutlet weak var starImageView: UIImageView!
    @IBOutlet weak var bgImageView: UIImageView!
    
    @IBOutlet weak var starTableView: UITableView!
    @IBOutlet weak var navigationView: UIView!
    @IBOutlet weak var notificationBtn: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var addStarBtn: UIButton!
    @IBOutlet weak var badgeLabel: UILabel!
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if AppUserDefaults.value(forKey: .totalNotification).intValue > 0{
            self.badgeLabel.isHidden = false
        }else{
            self.badgeLabel.isHidden = true
        }
        
//        if self.isMonthSubscription{
            self.transactionList(pageNo: 0, loader: true)
//            self.isMonthSubscription = false
//        }
    }
    
    //MARK:- Private Methods
    //=======================
    
    private func initialSetup() {
        if AppUserDefaults.value(forKey: .stars).intValue > 1{
            self.starCountLbl.text = "\(AppUserDefaults.value(forKey: .stars).stringValue) \(StringConstants.Stars)"
        }else{
            self.starCountLbl.text = "\(AppUserDefaults.value(forKey: .stars).stringValue.isEmpty ? "0" : AppUserDefaults.value(forKey: .stars).stringValue) \(StringConstants.Star)"
        }
        self.badgeLabel.roundCorners()
        self.starTableView.delegate = self
        self.starTableView.dataSource = self
        self.starTableView.emptyDataSetSource = self
        self.starTableView.emptyDataSetDelegate = self
        self.navigationView.setShadow()
        //self.registerCell()
        self.starCountLbl.textColor = AppColors.black46
        self.starBalanceLbl.textColor = AppColors.black46
        self.starImageView.roundCorners()
         self.starTableView.enablePullToRefresh(tintColor: AppColors.themeBlueColor ,target: self, selector: #selector(refreshWhenPull(_:)))
         self.starTableView.register(UINib(nibName: "LoadMoreTableViewCell", bundle: nil), forCellReuseIdentifier: "LoadMoreTableViewCell")
//        self.transactionList(pageNo: 0, loader: true)
    }
    
    @objc func refreshWhenPull(_ sender: UIRefreshControl) {
        sender.endRefreshing()
        self.pageNum = 0
        self.transactionList(pageNo:self.pageNum, loader: true)
    }
    
    private func isLoadingIndexPath(_ indexPath: IndexPath) -> Bool {
        guard shouldLoadMoreData else { return false}
        
        return indexPath.row == self.transactionList.count
    }
    
    func transactionList(pageNo: Int, loader: Bool){
        
        WebServices.transactionList(parameters: ["page":pageNo], loader: loader, success: { (json) in
            
            var data = [TransactionList]()
            let transactions = json[ApiKeys.data.rawValue]["jobs"].arrayValue
            self.totalStars = json[ApiKeys.data.rawValue]["stars"].stringValue
            for transaction in transactions {
                data.append(TransactionList(withJSON: transaction))
            }
//             AppUserDefaults.save(value: json[ApiKeys.data.rawValue]["stars"].intValue, forKey: .stars)
//            if AppUserDefaults.value(forKey: .stars).intValue > 1{
//                self.starCountLbl.text = "\(AppUserDefaults.value(forKey: .stars).stringValue) \(StringConstants.Stars)"
//            }else{
//                self.starCountLbl.text = "\(AppUserDefaults.value(forKey: .stars).stringValue.isEmpty ? "0" : AppUserDefaults.value(forKey: .stars).stringValue) \(StringConstants.Star)"
//            }
//            self.starCountLbl.text = "\(self.totalStars) \(StringConstants.Stars)"
            if pageNo == 0 {
                self.transactionList.removeAll(keepingCapacity: false)
            }
            if self.shouldLoadMoreData {
                self.transactionList.append(contentsOf: data)
            } else {
                self.transactionList = data
            }
            self.pageNum = json[ApiKeys.page.rawValue].intValue
            self.shouldLoadMoreData = json[ApiKeys.next.rawValue].boolValue
            self.starTableView.reloadData()
        }) { (error) in
            CommonClass.showToast(msg: error.localizedDescription)
        }
    }
    
    //MARK:- Action & Target
    //=======================
    @IBAction func notificationBtnTapped(_ sender: UIButton) {
        let notificationVC = NotificationsVC.instantiate(fromAppStoryboard: .settingsAndChat)
        self.navigationController?.pushViewController(notificationVC, animated: true)

    }
    
    @IBAction func backButtonTapp(_ sender: UIButton) {
        self.pop()
    }
    
    @IBAction func addStarBtnTapped(_ sender: UIButton) {
        let starPackege = StarPackage.instantiate(fromAppStoryboard: .Star)
        starPackege.delegate = self
        self.navigationController?.pushViewController(starPackege, animated: true)
    }
}

//MARK:-
//MARK:- Table view Delegate and DataSource
extension StarVC: UITableViewDelegate, UITableViewDataSource {
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.transactionList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if isLoadingIndexPath(indexPath){
            guard let cell = tableView.dequeueReusableCell(withIdentifier: LoadMoreTableViewCell.defaultReuseIdentifier, for: indexPath) as? LoadMoreTableViewCell else {
                fatalError("Failed to initialize LoadMoreTableViewCell")
            }
            self.isPaginationEnable = true
            return cell
        }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: LastPackageCell.identifier) as? LastPackageCell else { fatalError("invalid cell \(self)")
        }
        if self.transactionList[indexPath.row].paymentType == "3"{
             cell.totalStarLbl.text = "\(StringConstants.Total_No_Stars): \(self.transactionList[indexPath.row].stars)/month"
        }else{
             cell.totalStarLbl.text = "\(StringConstants.Total_No_Stars): \(self.transactionList[indexPath.row].stars)"
        }
       
        cell.paymentLbl.text = "Payment: \(self.transactionList[indexPath.row].paymentMethod)"
        if let time = self.transactionList[indexPath.row].createdAt{
            cell.timeLbl.text =  time.transactionElapsed
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard self.shouldLoadMoreData else { return }
        
        if indexPath.row == self.transactionList.count - 1 && self.shouldLoadMoreData   && isPaginationEnable {  //for 2nd last cell
            transactionList(pageNo:self.pageNum, loader: false)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print_debug(indexPath)
    }
}

//MARK:- DZNEmptyDataSource and Delegate
//==========================================
extension StarVC: DZNEmptyDataSetDelegate, DZNEmptyDataSetSource {
    
    func image(forEmptyDataSet scrollView: UIScrollView!) -> UIImage! {
        return #imageLiteral(resourceName: "icHomeNoJobs")
    }
    
    func title(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        return NSAttributedString(string: StringConstants.No_Package_Purchased,                                                                           attributes: [NSAttributedStringKey.foregroundColor: AppColors.black46,
                                                                                                                                                                  NSAttributedStringKey.font: AppFonts.Poppins_Medium.withSize(14)
            ])
    }
    
    func description(forEmptyDataSet scrollView: UIScrollView!) -> NSAttributedString! {
        
        return NSAttributedString(string: "",                                                                           attributes: [NSAttributedStringKey.foregroundColor: AppColors.gray152,
                                                                                                                                     NSAttributedStringKey.font: AppFonts.Poppins_Regular.withSize(12)                                                                                                                                                                                                            ])
    }
    func emptyDataSetShouldDisplay(_ scrollView: UIScrollView!) -> Bool {
        return true
    }
}

extension StarVC: UpdateTransactionListDelegate{
    
    func updateTransactionListDelegate(){
        self.transactionList(pageNo: 0, loader: true)
    }
}




class LastPackageCell: UITableViewCell {
    
    //MARK:- Properties
    //==================
    static var identifier: String {
        return String(describing: self)
    }
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var cornerRadiusView: UIView!
    @IBOutlet weak var validityLabel: UILabel!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var paymentLbl: UILabel!
    @IBOutlet weak var totalStarLbl: UILabel!
    @IBOutlet weak var starImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.totalStarLbl.textColor = AppColors.black46
        self.paymentLbl.textColor = AppColors.lightBlack
        self.timeLbl.textColor = AppColors.gray119
        self.validityLabel.textColor = AppColors.gray119
        self.shadowView.setShadow()
        self.cornerRadiusView.setCorner(cornerRadius: 5.0, clip: true)
        self.starImageView.roundCorners()
    }
}



