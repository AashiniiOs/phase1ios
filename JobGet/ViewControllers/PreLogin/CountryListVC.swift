//
//  CountryListVC.swift
//  ChekiOdds
//
//  Created by MAC on 07/11/17.
//  Copyright © 2017 Beta. All rights reserved.
//

import UIKit

@objc protocol CountryDelegate{
    func didGetCountryWith(code : String , shortName : String)
}

class CountryListVC: BaseVC {
    
    // MARK:- Properties
    // =================
    var countryList = [Country]()
    var tempList = [Country]()
    var selectedCountry : String?
    weak var delegate : CountryDelegate?
    
    // MARK:- IBOutlets
    // ================
    @IBOutlet weak var countryListTableView: UITableView!
    @IBOutlet weak var searchContainerView: UIView!
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var cancelButton: UIButton!
    
    // MARK:- View Life Cycle
    // ======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setupSubViews()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK:- Private Methods
    // ======================
    fileprivate func setupSubViews() {
        
        self.countryListTableView.delegate = self
        
        self.countryListTableView.dataSource = self
        self.cancelButton.setTitleColor(AppColors.black46, for: .normal)
        
        getCountryList { (countriesList) in
            self.getCountries(fromData: countriesList)
        }
        
        self.searchTextField.textAlignment = .left
        self.searchTextField.setPlaceholderColor(color: AppColors.black46)
        self.searchTextField.tintColor = AppColors.black46
        self.searchTextField.delegate = self
    }
    
    func getCountries(fromData data: [[String: AnyObject]]){
        for eachCountry in data{
            let data = Country(withDict: eachCountry)
            self.countryList.append(data)
        }
        
        self.tempList = self.countryList
        
        self.countryListTableView.reloadData()
    }
    
    // MARK:- IBActions
    // ================
    @IBAction func cancelButtonTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func didChangeText(_ sender: UITextField) {
        
        if (sender.text ?? "").trimmingCharacters(in: .whitespaces).count>0{
            
            var finalArray = [Country]()
            
            for data in self.tempList{
                
                let countryName = data.countryName
                let newArray:[String] = [countryName]
                let searchPredicate = NSPredicate(format: "SELF CONTAINS[c] %@", (sender.text ?? "").trimmingCharacters(in: .whitespaces))
                let array = (newArray as NSArray).filtered(using: searchPredicate)
                
                if array.count != 0 {
                    finalArray.append(data)
                }
            }
            self.countryList = finalArray
            self.countryListTableView.reloadData()
        }
        else{
            self.countryList = self.tempList
            self.countryListTableView.reloadData()
        }
    }
    
}

// MARK:- Table View Delegate and Datasource Methods
// =================================================
extension CountryListVC : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.countryList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let countryListCell = tableView.dequeueReusableCell(withIdentifier: "countryListCell") as! CountryListCell
        
        let data = self.countryList[indexPath.row]
        
        let countryName = data.countryName
        let countryCode = data.countryCode
        
        countryListCell.countryNameLabel.text = countryName
        countryListCell.countryCode.text  = countryCode
        
        return countryListCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.selectedCountry = self.countryList[indexPath.row].countryShortName
        self.delegate?.didGetCountryWith(code: self.countryList[indexPath.row].countryCode, shortName: self.countryList[indexPath.row].countryShortName)
        self.countryListTableView.reloadData()
        self.dismiss(animated: true, completion: nil)
    }
}

// MARK:- Text Field Delegate Methods
// ==================================
extension CountryListVC : UITextFieldDelegate{
    
}

// MARK:- Table View Cell
// ======================
class CountryListCell : UITableViewCell{
    
    @IBOutlet weak var countryCode: UILabel!
    @IBOutlet weak var countryNameLabel: UILabel!
    @IBOutlet weak var selectedImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.countryNameLabel.textColor = AppColors.black46
        countryCode.textColor = AppColors.black46
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        self.selectedImageView.setCorner(cornerRadius: self.selectedImageView.frame.size.height/2, clip: true)
    }
}
