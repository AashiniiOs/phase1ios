//
//  ForgotPasswordVC.swift
//  Onboarding
//
//  Created by Appinventiv on 15/09/17.
//  Copyright © 2017 Gurdeep Singh. All rights reserved.
//

import UIKit

class ForgotPasswordVC: BaseVC {
    
    //MARK:- Properties
    //==================
    
    //MARK:- IBOutlet
    //===============
    @IBOutlet weak var backButton               : UIButton!
    @IBOutlet weak var emailTextField           : FloatingTextField!
    @IBOutlet weak var sendButton               : UIButton!
    @IBOutlet weak var descriptionLabel         : UILabel!
    @IBOutlet weak var forgotPasswordLabel      : UILabel!
    @IBOutlet weak var forgotPasswordScrollView : UIScrollView!
    
    //MARK:- VIEW LIFE CYCLE
    //======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
}

//MARK:- Private Methods
//=======================
private extension ForgotPasswordVC {
    
    func initialSetup() {
        
        self.descriptionLabel.textColor = AppColors.gray152
        self.forgotPasswordLabel.textColor = AppColors.black46
        self.backButton.setTitle(StringConstants.Back.localized, for: .normal)
        self.sendButton.setCorner(cornerRadius: 5, clip: true)
        self.emailTextField.returnKeyType = UIReturnKeyType.done
        self.setupTextField()
        self.setupTapGestures()
        self.isEnablSendBtn(status: false)
    }
    
    func setupTextField() {
        
        // self.emailTextField.becomeFirstResponder()
        self.emailTextField.keyboardType = .emailAddress
        self.emailTextField.delegate     = self
        CommonClass.setActiveLabelTextField(textField: self.emailTextField, placeholder: StringConstants.Email_Address.localized, title: StringConstants.Email_Title)
        self.emailTextField.setIconImage(img: #imageLiteral(resourceName: "icLoginEmail"), size: CGSize(width: 30.0 , height: 30.0))
    }
    
    func setupTapGestures() {
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.dissmissKeyboardOnTouch))
        recognizer.numberOfTapsRequired = 1
        recognizer.numberOfTouchesRequired = 1
        self.forgotPasswordScrollView.addGestureRecognizer(recognizer)
    }
    
    func isEnablSendBtn(status: Bool) {
        
        status ? (self.sendButton.backgroundColor = AppColors.themeBlueColor) : (self.sendButton.backgroundColor = AppColors.buttonDisableBgColor)
        self.sendButton.isEnabled = status
    }
}

//MARK:- IBActions and Targets
//==============================
extension ForgotPasswordVC {
    
    @IBAction func sendButtonTapped(_ sender: UIButton) {
        
        self.view.endEditing(true)
        if self.isDataValid  {
            self.hitWebserviceForForgotPassword()
        }
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func dissmissKeyboardOnTouch() {
        
        self.view.endEditing(true)
    }
}
//MARK:- WebService
//=================

extension ForgotPasswordVC {
    
    func hitWebserviceForForgotPassword() {
        
        guard let text = self.emailTextField.text else {
            return
        }
        
        WebServices.forgotPasswordAPI(email: text, success: { [unowned self] (json) in
            
            let sceen = ConfirmationPopupVC.instantiate(fromAppStoryboard: .Main)
            sceen.email = text
            self.view.addSubview(sceen.view)
            self.addChildViewController(sceen)
            
        }) { (e) in
            CommonClass.showToast(msg: e.localizedDescription)
        }
    }
}

//MARK:- Validations
//==================
extension ForgotPasswordVC {
    
    var isDataValid : Bool {
        
        guard let text = self.emailTextField.text else {
            return false
        }
        
        if text.isEmpty {
            CommonClass.showToast(msg: StringConstants.Enter_Email.localized)
            
            return false
        } else if text.checkIfInvalid(.email) {
            CommonClass.showToast(msg: StringConstants.Invalid_Email.localized)
            return false
        }
        
        return true
    }
}

extension ForgotPasswordVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string == " " {
            return false
        }
        
        if textField === self.emailTextField {
            
            return (textField.text ?? "").count - range.length < 50
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if let text = textField.text, !text.isEmpty {
            self.isEnablSendBtn(status: true)
        } else {
            self.isEnablSendBtn(status: false)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        self.emailTextField.resignFirstResponder()
        return true
    }
}
