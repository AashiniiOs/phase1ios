//
//  LoginVC.swift
//  Onboarding
//
//  Created by Appinventiv on 09/09/16.
//  Copyright © 2016 Gurdeep Singh. All rights reserved.
//

import UIKit
//import SkyFloatingLabelTextField

class LoginVC: BaseVC {
    //MARK: - Enum
    //==============
    enum CommingFrom {
        case signupSceen
        case socialSceen
        case none
    }
    
    
    //MARK:- Properties
    //=================
    var commingFrom: CommingFrom = .none
    
    //MARK:- IBOutlet
    //===============
    @IBOutlet weak var logoImageView        : UIImageView!
    @IBOutlet weak var forgotPasswordButton : UIButton!
    @IBOutlet weak var emailTextField       : FloatingTextField!
    @IBOutlet weak var passwordTextField    : FloatingTextField!
    @IBOutlet weak var loginButton          : UIButton!
    @IBOutlet weak var welcomeLabel         : UILabel!
    @IBOutlet weak var signUpBtn            : UIButton!
    @IBOutlet weak var loginScrollView      : UIScrollView!
    @IBOutlet weak var loginContainerView   : UIView!
    @IBOutlet weak var backButton: UIButton!
    
    //MARK:- VIEW LIFE CYCLE
    //======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.initialSetup()
        
    }
}
//MARK:- Private Methods
//=======================
private extension LoginVC {
    
    private func initialSetup() {
        
        passwordTextField.isSecureTextEntry = true
        emailTextField.keyboardType = .emailAddress
        
        self.emailTextField.delegate    = self
        self.passwordTextField.delegate = self
        self.loginButton.setCorner(cornerRadius: 5, clip: true)
        self.setupTextField()
        self.applyFontAndColor()
        self.setupTapGestures()
        self.emailTextField.returnKeyType = UIReturnKeyType.next
        self.passwordTextField.returnKeyType = UIReturnKeyType.done
        self.isEnableLoginBtn(status: false)
    }
    
    func setupTapGestures() {
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.dissmissKeyboardOnTouch))
        recognizer.numberOfTapsRequired = 1
        recognizer.numberOfTouchesRequired = 1
        self.loginScrollView.addGestureRecognizer(recognizer)
    }
    
    func setupTextField() {
        
        self.emailTextField.setIconImage(img: #imageLiteral(resourceName: "icLoginEmail"), size: CGSize(width: 30.0 , height: 30.0))
        self.passwordTextField.setIconImage(img: #imageLiteral(resourceName: "icLoginPassword"), size: CGSize(width: 30.0 , height: 30.0))
        
        let button: UIButton = UIButton()
        button.titleLabel?.font = AppFonts.Poppins_Regular.withSize(11.0)
        button.setTitleColor(AppColors.gray152, for: .normal)
        button.addTarget(self, action: #selector(self.showPasswordBtnTapped(_:)), for: .touchUpInside)
        self.passwordTextField.setButtonToRightView(btn: button, selectedText:  StringConstants.Hide, normalText: StringConstants.Show, size: nil)
        
        CommonClass.setActiveLabelTextField(textField: self.emailTextField, placeholder: StringConstants.Email_Address.localized, title: StringConstants.Email_Title)
        CommonClass.setActiveLabelTextField(textField: self.passwordTextField, placeholder: StringConstants.Password.localized, title: StringConstants.Password_Title)
        self.emailTextField.autocorrectionType = .no
    }
    
    func applyFontAndColor() {
        
        self.welcomeLabel.textColor = AppColors.black46
        self.forgotPasswordButton.setTitleColor(AppColors.black46, for: .normal)
        self.forgotPasswordButton.titleLabel?.font = AppFonts.Poppins_Regular.withSize(12.0)
        self.loginButton.titleLabel?.font = AppFonts.Poppins_SemiBold.withSize(15)
    }
    
    func isEnableLoginBtn(status: Bool) {
        
        status ? (self.loginButton.backgroundColor = AppColors.themeBlueColor) : (self.loginButton.backgroundColor = AppColors.buttonDisableBgColor)
        self.loginButton.isEnabled = status
    }
}

//MARK:- IBActions and Targets
//==============================
extension LoginVC {
    
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        if self.checkValidity {
            var details = [String:Any]()
            
            details["email"] = self.emailTextField.text ?? ""
            
            details["password"] = self.passwordTextField.text ?? ""
            
            details["fcm_token"]   = sharedAppDelegate.fcmToken ?? "dummyjobget12345"
            details[StringConstants.device_token]   = DeviceDetail.deviceToken
            details[StringConstants.device_type]    = Device_Type.iOS.rawValue
            details[StringConstants.user_type]      = userType.rawValue
            details[StringConstants.device_id]      = DeviceDetail.device_id
            self.loginService(parameters: details)
        }
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func forgotPasswordButtonTapped(_ sender: UIButton) {
        let sceen = ForgotPasswordVC.instantiate(fromAppStoryboard: .Main)
        self.navigationController?.pushViewController(sceen, animated: true)
    }
    
    @IBAction func signupButtonTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        if self.commingFrom == .signupSceen {
            self.pop()
        } else if self.commingFrom == .socialSceen {
            let signUp = SignUpVC.instantiate(fromAppStoryboard: .Main)
            signUp.commingFrom = .loginSceen
            self.navigationController?.pushViewController(signUp, animated: true)
        } else {
            let signUp = SignUpVC.instantiate(fromAppStoryboard: .Main)
            self.navigationController?.pushViewController(signUp, animated: true)
        }
    }
    
    @objc func showPasswordBtnTapped(_ sender: UIButton) {
        self.view.endEditing(true)
        
        sender.isSelected = !sender.isSelected
        self.passwordTextField.isSecureTextEntry = !self.passwordTextField.isSecureTextEntry
    }
    
    @objc func dissmissKeyboardOnTouch() {
        
        self.view.endEditing(true)
    }
}
//MARK:- Validations
//===================
private extension LoginVC {
    
    var checkValidity: Bool {
        let email = self.emailTextField.text
        let password = self.passwordTextField.text
        
        switch (email, password) {
        case let (.some(email), .some(password)):
            if email.checkIfInvalid(.email) {
                
                CommonClass.showToast(msg: StringConstants.Enter_Email.localized)
                return false
                
            } else if password.count < 6 || password.isEmpty {
                
                CommonClass.showToast(msg: StringConstants.Invalid_Password.localized)
                return false
            }
            return true
            
        case (.some(_), .none):
            CommonClass.showToast(msg: StringConstants.Enter_Password.localized)
            return false
        default:
            CommonClass.showToast(msg: StringConstants.Enter_Email.localized)
            return false
            
        }
    }
}

//MARK:- Web Services
//====================
extension LoginVC {
    
    func loginService(parameters: [String: Any]) {
        
        WebServices.loginAPI(parameters: parameters, success: { (user) -> () in
            print_debug(user)
            
            let userType = user.user_Type
            
            switch userType{
                
            case .none:
                
                return
                
            case .candidate:
                
                let sceen = CandidateTabBarVC.instantiate(fromAppStoryboard: .Candidate)
                sharedAppDelegate.navigationController = UINavigationController(rootViewController: sceen)
                sharedAppDelegate.navigationController?.isNavigationBarHidden = true
                sharedAppDelegate.window?.rootViewController =  sharedAppDelegate.navigationController
                sharedAppDelegate.window?.makeKeyAndVisible()
                //                self.navigationController?.pushViewController(sceen, animated: true)
                
            case .recuriter:
                let isProfileAdded = AppUserDefaults.value(forKey: .isProfileAdded).stringValue
                
                if isProfileAdded == "0" {
                    let recruiter = RecruiterCompanyNameVC.instantiate(fromAppStoryboard: .Main)
                    self.navigationController?.pushViewController(recruiter, animated: true)
                } else {
                    let isOtpVerified = AppUserDefaults.value(forKey: .isOtpVerified).stringValue
                    
                    if isOtpVerified == "0" {
                        let mainVC = SelectUserTypeVC.instantiate(fromAppStoryboard: .Main)
                        let recruiterCompanyNameVC = RecruiterCompanyNameVC.instantiate(fromAppStoryboard: .Main)
                        let recruiterVerifyOtpVC = RecruiterVerifyOtpVC.instantiate(fromAppStoryboard: .Main)
                        self.navigationController?.setViewControllers([mainVC, recruiterCompanyNameVC, recruiterVerifyOtpVC], animated: true)
                        
                    } else { 
                        let sceen = RecruiterTabBarVC.instantiate(fromAppStoryboard: .Recruiter)
                        sharedAppDelegate.navigationController = UINavigationController(rootViewController: sceen)
                        sharedAppDelegate.navigationController?.isNavigationBarHidden = true
                        sharedAppDelegate.window?.rootViewController =  sharedAppDelegate.navigationController
                        sharedAppDelegate.window?.makeKeyAndVisible()
                        //                        self.navigationController?.pushViewController(sceen, animated: true)
                    }
                    
                }
            }
        }, failure: { (error : Error) -> () in
            
            CommonClass.showToast(msg: error.localizedDescription )
        })
    }
    
}

//MARK:- TextFieldDelegate
//========================
extension LoginVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string == " " {
            return false
        }
        
        if textField === self.emailTextField {
            return (textField.text ?? "").count - range.length < 50
        } else if textField === self.passwordTextField{
            
            return (textField.text ?? "").count - range.length < 16
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (self.emailTextField.text ?? "").count > 0,  (self.passwordTextField.text ?? "").count > 0{
            self.isEnableLoginBtn(status: true)
        } else {
            self.isEnableLoginBtn(status: false)
        }
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField === self.emailTextField {
            self.passwordTextField.becomeFirstResponder()
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
}


