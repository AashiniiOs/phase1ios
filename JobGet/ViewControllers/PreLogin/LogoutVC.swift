//
//  LogoutVc.swift
//  Onboarding
//
//  Created by Appinventiv on 01/01/17.
//  Copyright © 2017 Gurdeep Singh. All rights reserved.
//

import UIKit

class LogoutVC: BaseVC {
    
    @IBOutlet weak var logoutBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func logoutBtnTap(_ sender: UIButton) {
        
        AppUserDefaults.removeAllValues()
        FacebookController.shared.facebookLogout()
        UIApplication.shared.applicationIconBadgeNumber = 0
        CommonClass().goToLogin()
    }
    
    
    
}
