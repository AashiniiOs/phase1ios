//
//  NumberConfirmationPopupVC.swift
//  JobGet
//
//  Created by macOS on 17/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit
import SwiftyJSON

protocol OnTapYesButton: class {
    
    func yesBtnTap()
    func editButtonTapped()
    
}
class NumberConfirmationPopupVC: BaseVC {
    
    //MARK:- Properties
    //==================
    
    weak var delegate: OnTapYesButton?
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var yesButton: UIButton!
    @IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var numberConfirmationLabel: UILabel!
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 0.33, delay: 0.0, usingSpringWithDamping: 0.9 , initialSpringVelocity: 0.4, options: UIViewAnimationOptions(), animations: {
            
            self.containerView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }){ (true) in
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    //MARK:- remove popup when touch otuside of popup
    //================================================
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        let touch = touches.first!
        let point = touch.location(in: self.view) //touch.location(in: self.view)
        
        if !self.containerView.frame.contains(point) {
            self.dismissPopupViewOnTouches()
        }
    }
    
    func initialSetup() {
        
        
        self.numberConfirmationLabel.textColor = AppColors.black26
        self.phoneNumberLabel.textColor = AppColors.black26
        
        self.descriptionLabel.textColor = AppColors.gray152
        self.editButton.setCorner(cornerRadius: 5, clip: true)
        self.editButton.backgroundColor = AppColors.themeBlueColor
        self.editButton.setTitle(StringConstants.EDIT, for: .normal)
        self.editButton.titleLabel?.font = AppFonts.Poppins_Medium.withSize(15)
        self.editButton.setTitleColor(AppColors.white, for: .normal)
        
        self.yesButton.setCorner(cornerRadius: 5, clip: true)
        self.yesButton.backgroundColor = AppColors.white
        self.yesButton.setTitle(StringConstants.yes.uppercased(), for: .normal)
        self.yesButton.titleLabel?.font = AppFonts.Poppins_Medium.withSize(15)
        self.yesButton.borderWidth = 1.5
        self.yesButton.borderColor = AppColors.themeBlueColor
        self.yesButton.setTitleColor(AppColors.themeBlueColor, for: .normal)
        
        self.containerView.setCorner(cornerRadius: 5, clip: true)
        self.containerView.transform = CGAffineTransform(scaleX: 0.000001, y: 0.000001)
        let phoneNumber = AppUserDefaults.value(forKey: .userPhoneNoFormatWithCode)
        self.phoneNumberLabel.text = phoneNumber.stringValue
    }
    
    
    func dismissPopupViewOnTouches() {
        
        UIView.animate(withDuration: 0.33, delay: 0.0, usingSpringWithDamping: 0.9 , initialSpringVelocity: 0.4, options: UIViewAnimationOptions(), animations: {
            
            self.containerView.transform = CGAffineTransform( scaleX: 0.0000001,y: 0.0000001)
        }){ (true) in
            if self.yesButton.isSelected {
                let sceen = RecruiterVerifyOtpVC.instantiate(fromAppStoryboard: .Main)
                self.navigationController?.pushViewController(sceen, animated: true)
            }
        }
        
        self.view.removeFromSuperview()
        self.removeFromParent
    }
    
    
    @IBAction func editButtonTapped(_ sender: UIButton) {
        self.dismissPopupViewOnTouches()
        delegate?.editButtonTapped()
    }
    
    @IBAction func yesButtonTapped(_ sender: UIButton) {
        
        self.yesButton.isSelected = !self.yesButton.isSelected
        self.dismissPopupViewOnTouches()
        delegate?.yesBtnTap()
    }
    
}
