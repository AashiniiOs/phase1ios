//
//  ProfileErrorPopUpVC.swift
//  JobGet
//
//  Created by Abhi on 22/05/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class ProfileErrorPopUpVC: UIViewController {
    
    @IBOutlet weak var redAlertImage: UIImageView!
    @IBOutlet weak var profileLabel: UILabel!
    @IBOutlet weak var completeButton: UIButton!
    @IBOutlet weak var notNowButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    
    weak var delegate: OnTapYesButton?
    var fromSavedJob = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        completeButton.layer.cornerRadius = 4
        notNowButton.layer.cornerRadius = 4
        notNowButton.layer.borderWidth = 1
        notNowButton.layer.borderColor = #colorLiteral(red: 0.2389856699, green: 0.4166136601, blue: 0.6607115269, alpha: 1)
        self.redAlertImage.image = #imageLiteral(resourceName: "icHomeGraphic")
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 0.33, delay: 0.0, usingSpringWithDamping: 0.9 , initialSpringVelocity: 0.4, options: UIViewAnimationOptions(), animations: {
            
            self.containerView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }){ (true) in
            
        }
    }
    //MARK:- remove popup when touch otuside of popup
    //================================================
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        let touch = touches.first!
        let point = touch.location(in: self.view) //touch.location(in: self.view)
        
        if !self.containerView.frame.contains(point) {
            if fromSavedJob {
                self.dismiss(animated: true, completion: nil)
            } else {
                self.dismissPopupViewOnTouches()
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func dismissPopupViewOnTouches() {
        
        UIView.animate(withDuration: 0.33, delay: 0.0, usingSpringWithDamping: 0.9 , initialSpringVelocity: 0.4, options: UIViewAnimationOptions(), animations: {
            
            self.containerView.transform = CGAffineTransform( scaleX: 0.0000001,y: 0.0000001)
        }){ (true) in
        }
        
        self.view.removeFromSuperview()
        self.removeFromParent
    }
    
    
    @IBAction func completeButtonTapped(_ sender: UIButton) {
        if fromSavedJob {
            self.dismiss(animated: true, completion: nil)
        } else {
            self.dismissPopupViewOnTouches()
        }
        delegate?.yesBtnTap()
        
        
    }
    @IBAction func notNowButtonTapped(_ sender: UIButton) {
        if fromSavedJob {
            self.dismiss(animated: true, completion: nil)
        } else {
            self.dismissPopupViewOnTouches()
        }
        delegate?.editButtonTapped()
        
    }
}
