//
//  RecruiterCompanyNameVC.swift
//  Onboarding
//
//  Created by macOS on 30/03/18.
//  Copyright © 2018 Gurdeep Singh. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift


enum IsCommingAfterAppKill {
    case afterAppKill
    case noAppKill
}

class RecruiterCompanyNameVC: BaseVC {
    
    //MARK:- Properties
    //==================
    var isCommingAfterAppKill: IsCommingAfterAppKill = .noAppKill
    
    var selctedCountryShortName : String?
    var phoneFormatter = PhoneNumberFormatter()
    
    
    //MARK:- IBoutlets
    //================
    
    @IBOutlet weak var titleLabelBtmConst: NSLayoutConstraint!
    @IBOutlet weak var scrollViewBtmConst: NSLayoutConstraint!
    @IBOutlet weak var companyShadowView: UIView!
    @IBOutlet weak var recruiterCompanyNameScrollView       : UIScrollView!
    @IBOutlet weak var backBtn                              : UIButton!
    @IBOutlet weak var createProfileLabel                   : UILabel!
    @IBOutlet weak var companyNameContainerView             : UIView!
    @IBOutlet weak var phoneNumberContainerView             : UIView!
    @IBOutlet weak var companyImageView                     : UIImageView!
    @IBOutlet weak var companyNameTextField                 : SkyFloatingLabelTextField!
    @IBOutlet weak var createButton                         : UIButton!
    @IBOutlet weak var phoneNumberImageView                 : UIImageView!
    @IBOutlet weak var phoneNumberLabel                     : UILabel!
    @IBOutlet weak var countryCodeButton                    : UIButton!
    @IBOutlet weak var phoneNumberTextField                 : SkyFloatingLabelTextField!
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

}

//MARK:- Private Methods
//=======================
private extension RecruiterCompanyNameVC {
    
    func initialSetup() {
        
        self.phoneNumberTextField.delegate = self
        self.companyNameTextField.delegate = self
        self.isEnableLoginBtn(status: false)
        self.setupView()
        self.setupTapGestures()
        self.setTextInTextField()
        
    }
    
    func setupView() {
        
        self.createProfileLabel.textColor = AppColors.black46
        
        self.companyNameContainerView.setCorner(cornerRadius: 5.0, clip: false)
        self.companyNameTextField.autocapitalizationType = .words
        
        self.phoneNumberContainerView.setCorner(cornerRadius: 5.0, clip: false)
        
        phoneNumberContainerView.layer.shadowColor = AppColors.blakopacity15.cgColor
        phoneNumberContainerView.layer.shadowOpacity = 0.5
        phoneNumberContainerView.layer.shadowOffset = CGSize.zero
        phoneNumberContainerView.layer.shadowRadius = 2
        
        companyNameContainerView.layer.shadowColor = AppColors.blakopacity15.cgColor
        companyNameContainerView.layer.shadowOpacity = 0.5
        companyNameContainerView.layer.shadowOffset = CGSize.zero
        companyNameContainerView.layer.shadowRadius = 2
        
        self.companyImageView.roundCorners()
        self.phoneNumberImageView.roundCorners()
        
        self.phoneNumberLabel.textColor = AppColors.gray152
        self.phoneNumberLabel.font = AppFonts.Poppins_Regular.withSize(12)
        self.createButton.titleLabel?.font = AppFonts.Poppins_Medium.withSize(16)
        self.phoneNumberLabel.text = StringConstants.Phone_Number
        self.createButton.setCorner(cornerRadius: 5.0, clip: true)
        self.phoneNumberTextField.keyboardType = .numberPad
        self.countryCodeButton.titleLabel?.font = AppFonts.Poppins_Bold.withSize(14)
        self.countryCodeButton.setTitleColor(AppColors.black46, for: .normal)
        CommonClass.setSkyFloatingLabelTextField(textField: self.phoneNumberTextField, placeholder: "", title: "")
        CommonClass.setSkyFloatingLabelTextField(textField: self.companyNameTextField, placeholder: StringConstants.Company_Name, title: StringConstants.Company_Name)
        self.countryCodeButton.setImage(#imageLiteral(resourceName: "icCreateProfileArrow"), for: .normal)
    }
    
    func isEnableLoginBtn(status: Bool) {
        
        status ? (self.createButton.backgroundColor = AppColors.themeBlueColor) : (self.createButton.backgroundColor = AppColors.buttonDisableBgColor)
        self.createButton.isEnabled = status
    }
    
    func setupTapGestures() {
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.dissmissKeyboardOnTouch))
        recognizer.numberOfTapsRequired = 1
        recognizer.numberOfTouchesRequired = 1
        self.recruiterCompanyNameScrollView.addGestureRecognizer(recognizer)
    }
    
    func setTextInTextField() {
        var phoneFormatter = PhoneNumberFormatter()
        let countryCode = AppUserDefaults.value(forKey: .recruiterCountryCode).stringValue
        if countryCode != "" {
            self.countryCodeButton.setTitle("\(countryCode)", for: .normal)
        }
        self.companyNameTextField.text = AppUserDefaults.value(forKey: .recruiterCompanyName).stringValue
        let formattedPhoneNumber = AppUserDefaults.value(forKey: .recruiterMobile).stringValue
        let number = formattedPhoneNumber.replacingOccurrences(of: "+\(countryCode)", with: "", options: NSString.CompareOptions.literal, range:nil)
        
        self.phoneNumberTextField.text =  phoneFormatter.format(number, hash: self.phoneNumberTextField.hash)
    }
    
    func checkButtonStatus() {
        if (self.companyNameTextField.text ?? "").count > 0,  (self.phoneNumberTextField.text ?? "").count > 0
        {
            self.isEnableLoginBtn(status: true)
        } else {
            self.isEnableLoginBtn(status: false)
        }
    }
}

//MARK: - IB Action and Target
//===============================
extension RecruiterCompanyNameVC {
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        
//        if let viewcontroller = self.navigationController?.viewControllers[0] as? SelectUserTypeVC{
        
        self.popToRootViewController()
//            self.popToViewController(viewcontroller, animated: true)
//        }
    }
    
    @IBAction func createButtonTapped(_ sender: UIButton) {
        
        self.view.endEditing(true)
        self.recruiterSignup(loader: true)
        
    }
    
    @IBAction func countryCodeBtnTapped(_ sender: UIButton) {
        
        self.view.endEditing(true)
        let countryScene = CountryListVC.instantiate(fromAppStoryboard: .Main)
        countryScene.selectedCountry = self.selctedCountryShortName
        countryScene.delegate = self
        self.present(countryScene, animated: true, completion: nil)
    }
    
    @IBAction func formatPhoneNumberTextField(_ sender: UITextField) {
        
        sender.text = phoneFormatter.format(sender.text ?? "", hash: sender.hash)
        
    }
    
    @objc func dissmissKeyboardOnTouch() {
        
        self.view.endEditing(true)
    }
}

//MARK: - Web Services
//========================
extension RecruiterCompanyNameVC {
    
    func recruiterSignup(loader: Bool) {
        
        guard let number = self.phoneNumberTextField.text else { return }
        let phoneNumber = number.replacingOccurrences(of: "-", with: "", options: NSString.CompareOptions.literal, range:nil)
        print_debug(phoneNumber)
        guard let cuntryCode = self.countryCodeButton.titleLabel?.text else { return }
        let contryCode = cuntryCode.replacingOccurrences(of: "+", with: "", options: NSString.CompareOptions.literal, range:nil)
        
        let param = ["company_name" : self.companyNameTextField.text ?? "",
                     "country_code": contryCode ,
                     "mobile": phoneNumber,
                     "mobileFormat" : "\(cuntryCode) \(number)",
            "companyDesc":  "",
            "designation" : ""
            ] as [String: Any]
        
        WebServices.recruiterSignup(parameter: param, loader: loader, success: { (json) in
            
            let verifyOtp = RecruiterVerifyOtpVC.instantiate(fromAppStoryboard: .Main)
            commingFrom = .verifyotp
            AppUserDefaults.save(value: "\(self.companyNameTextField.text ?? "")", forKey: .recruiterCompanyName)
            
            AppUserDefaults.save(value: "\(self.phoneNumberTextField.text ?? "")", forKey: .recruiterMobile)
            AppUserDefaults.save(value: "\(self.countryCodeButton.titleLabel?.text ?? "")", forKey: .recruiterCountryCode)
            AppUserDefaults.save(value: "\((self.countryCodeButton.titleLabel?.text ?? "") + "  " + (self.phoneNumberTextField.text ?? ""))", forKey: .userPhoneNoFormatWithCode)
            self.navigationController?.pushViewController(verifyOtp, animated: true)
            
        }, failure: { (error: Error) in
            print_debug(error)
            CommonClass.showToast(msg: error.localizedDescription )
        })
    }
}
extension RecruiterCompanyNameVC: UITextFieldDelegate {
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField === self.phoneNumberTextField{
            UIView.animate(withDuration: 0.5, animations: {
                self.titleLabelBtmConst.constant = 50
            })
        }
        
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField === self.companyNameTextField {
            
            AppUserDefaults.save(value: textField.text ?? "", forKey: .recruiterCompanyName)
            
        }
        if textField === self.phoneNumberTextField{
            UIView.animate(withDuration: 0.5, animations: {
                self.titleLabelBtmConst.constant = 100
            })
        }
        textField.adjustFrameWidthToFitText()
        self.checkButtonStatus()
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField === self.companyNameTextField {
            
            return (textField.text ?? "").count - range.length < 50
        }
        
        return true
    }
}


// MARK:- Country Delegate Methods
// ===============================
extension RecruiterCompanyNameVC : CountryDelegate {
    
    func didGetCountryWith(code: String , shortName : String) {
        
        self.selctedCountryShortName = shortName
        self.countryCodeButton.setTitle(code, for: .normal)
    }
    
}
