//
//  RecruiterVerifyOtpVC.swift
//  Onboarding
//
//  Created by macOS on 30/03/18.
//  Copyright © 2018 Gurdeep Singh. All rights reserved.
//

import UIKit
import SwiftyJSON


class RecruiterVerifyOtpVC: BaseVC {
    
    //MARK:- Properties
    //==================
    var mobileDetail = [String: String]()
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var otpVerificationLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var otpDigit1TextField: OTPTextField!
    @IBOutlet weak var otpDigit2TextField: OTPTextField!
    @IBOutlet weak var otpDigit3TextField: OTPTextField!
    @IBOutlet weak var otpDigit4TextField: OTPTextField!
    @IBOutlet weak var resendOtpButton: UIButton!
    @IBOutlet weak var continueButton: UIButton!
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

//MARK:- Private Methods
//=======================
private extension RecruiterVerifyOtpVC {
    
    func initialSetup() {
        
        configureTextField(otpDigit1TextField)
        configureTextField(otpDigit2TextField)
        configureTextField(otpDigit3TextField)
        configureTextField(otpDigit4TextField)
        
        self.continueButton.backgroundColor = AppColors.themeBlueColor
        self.continueButton.setCorner(cornerRadius: 5, clip: true)
        self.continueButton.isHidden = false
        self.setupView()
        self.isEnableLoginBtn(status: false)
    }
    
    func isEnableLoginBtn(status: Bool) {
        
        status ? (self.continueButton.backgroundColor = AppColors.themeBlueColor) : (self.continueButton.backgroundColor = AppColors.buttonDisableBgColor)
        self.continueButton.isEnabled = status
    }
    
    func configureTextField(_ textField: UITextField){
        
        textField.keyboardType = .numberPad
        textField.delegate     = self
    }
    
    func setupView() {
        
        self.resendOtpButton.setTitle(StringConstants.RESEND_OTP, for: .normal)
        self.otpVerificationLabel.textColor = AppColors.black46
        self.descriptionLabel.textColor = AppColors.gray152
        
    }
}

//MARK: - IB Action and Target
//===============================
extension RecruiterVerifyOtpVC {
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func resendOtpButtonTapped(_ sender: UIButton) {
        
        let sceen = NumberConfirmationPopupVC.instantiate(fromAppStoryboard: .Main)
        sceen.delegate = self
        self.view.addSubview(sceen.view)
        self.add(childViewController: sceen)
    }
    
    @IBAction func continueBtnTapped(_ sender: UIButton) {
        
        self.verifyOtp()
    }
    
    func resetTextField() {
        self.otpDigit1TextField.text = ""
        self.otpDigit2TextField.text = ""
        self.otpDigit3TextField.text = ""
        self.otpDigit4TextField.text = ""
    }
}

extension RecruiterVerifyOtpVC {
    
    
    
    func verifyOtp() {
        
        guard let otp1 = otpDigit1TextField.text else { return }
        guard let otp2 = otpDigit2TextField.text else { return }
        guard let otp3 = otpDigit3TextField.text else { return }
        guard let otp4 = otpDigit4TextField.text else { return }
        self.isEnableLoginBtn(status: true)
        let otp = (otp1 + otp2 + otp3 + otp4)
        
        if otp.count == 4 {
            self.isEnableLoginBtn(status: true)
            
            let param: JSONDictionary = ["otp" : otp]
            
            WebServices.verifyOtp(parameters: param, success: { (json) in
                print_debug(json)
                
                
                if let errorCode = json["code"].int, errorCode == error_codes.success {
                    
                    let sceen = ResetPasswordPopupVC.instantiate(fromAppStoryboard: .Main)
                    
                    let isVerifedOtp = json["is_verified_otp"].stringValue
                    AppUserDefaults.save(value: isVerifedOtp, forKey: .isOtpVerified)
                    sceen.mobileDetail = self.mobileDetail
                    sceen.fromUpdateNumber = true
                    self.view.addSubview(sceen.view)
                    self.add(childViewController: sceen)
                } else {
                    
                    self.resetTextField()
                    let msg = json["message"].stringValue
//                    self.isEnableLoginBtn(status: false)
                    CommonClass.showToast(msg: msg)
                }
                
            }, failure: { (error : Error) in
//                self.isEnableLoginBtn(status: false)
                CommonClass.showToast(msg: error.localizedDescription )
            })
        } 
    }
    
    func resendOtp() {
        
        self.resetTextField()
        WebServices.resendOtp(success: { (json) in
            
            let msg = json["message"].stringValue
            CommonClass.showToast(msg: msg)
        }, failure: { (error : Error) in
            CommonClass.showToast(msg: error.localizedDescription )
            
        })
    }
}
//MARK: - Text Field Delegate
//===========================
extension RecruiterVerifyOtpVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string == " " {
            return false
        }
        
        if (textField == self.otpDigit1TextField)
        {
            if (range.length == 0)
            {
                textField.text = string
                self.verifyOtp()
                self.otpDigit2TextField.becomeFirstResponder()
            }
            else
            {
                textField.text = ""
            }
        }
        else if (textField == self.otpDigit2TextField)
        {
            if (range.length == 0)
            {
                textField.text = string
                self.verifyOtp()
                self.otpDigit3TextField.becomeFirstResponder()
            }
            else
            {
                textField.text = ""
                self.otpDigit1TextField.becomeFirstResponder()
            }
        }
            
        else if (textField == self.otpDigit3TextField)
        {
            if (range.length == 0)
            {
                textField.text = string
                self.verifyOtp()
                self.otpDigit4TextField.becomeFirstResponder()
            }
            else
            {
                textField.text = ""
                self.otpDigit2TextField.becomeFirstResponder()
            }
        }
        else if (textField == self.otpDigit4TextField)
        {
            if (range.length == 0)
            {
                textField.text = string
                self.verifyOtp()
                textField.resignFirstResponder()
            }
            else
            {
                textField.text = ""
                self.otpDigit3TextField.becomeFirstResponder()
            }
        }
        //   self.textFieldShouldHighLight()
        return false
    }
    
}

extension RecruiterVerifyOtpVC:  OnTapYesButton {
    
    func yesBtnTap() {
        self.resendOtp()
    }
    
    func editButtonTapped() {
        self.pop()
    }
}
