//
//  ResetPasswordPopupVC.swift
//  Onboarding
//
//  Created by macOS on 29/03/18.
//  Copyright © 2018 Gurdeep Singh. All rights reserved.
//

import UIKit

enum CommingFrom {
    
    case verifyotp
    case resetPassword
    case jobList
    case cardList
    case recruiterEditProfile
    case accountExists
    case report
    case shortlist
    case starDeduct
    case chatList
    case contactus
    
}
var commingFrom: CommingFrom = .verifyotp

class ResetPasswordPopupVC: BaseVC {
    
    //MARK:- Properties
    //================
    var delegate : ShouldOpenStatusPopup?
    var jobId = ""
    var isFromSavedJob = false
    var fromUpdateNumber = false
    var mobileDetail = [String: String]()
    var exitAccountText: String?
    var desText = ""
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var resetPasswordImageView   : UIImageView!
    @IBOutlet weak var congratulationLabel      : UILabel!
    @IBOutlet weak var descriptionLabel         : UILabel!
    @IBOutlet weak var okButton                 : UIButton!
    @IBOutlet weak var popupView                : UIView!
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 0.33, delay: 0.0, usingSpringWithDamping: 0.9 , initialSpringVelocity: 0.4, options: UIViewAnimationOptions(), animations: {
            
            self.popupView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }){ (true) in
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- remove popup when touch otuside of popup
    //================================================
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.view.endEditing(true)
        let touch = touches.first!
        let point = touch.location(in: self.view) //touch.location(in: self.view)
        if commingFrom != .report{
            if !self.popupView.frame.contains(point) {
                if isFromSavedJob {
                    self.dismiss(animated: true, completion: nil)
                } else {
                    self.dismissPopupViewOnTouches()
                }
            }
        }
    }
    
    func initialSetup() {
        
        self.congratulationLabel.textColor = AppColors.black26
        self.descriptionLabel.textColor = AppColors.gray152
        self.okButton.setCorner(cornerRadius: 5, clip: true)
        self.okButton.backgroundColor = AppColors.themeBlueColor
        self.okButton.setTitle(StringConstants.Ok.uppercased(), for: .normal)
        self.popupView.setCorner(cornerRadius: 5, clip: true)
        self.popupView.transform = CGAffineTransform(scaleX: 0.000001, y: 0.000001)
        
        if commingFrom == .resetPassword {
            self.descriptionLabel.text = StringConstants.You_have_successfully_changed_your_password
        } else if commingFrom == .verifyotp || self.fromUpdateNumber {
            self.descriptionLabel.text = StringConstants.You_successfully_verified_number
        }else if commingFrom == .accountExists {
            if let text = self.exitAccountText{
                self.descriptionLabel.text = text
            }
            self.resetPasswordImageView.image = #imageLiteral(resourceName: "icHomeGraphic")
            self.congratulationLabel.text = StringConstants.Oops
        }else if commingFrom == .cardList {
            self.descriptionLabel.text = StringConstants.Card_payment_sucess_alert
        }else if commingFrom == .shortlist {
             self.resetPasswordImageView.image = #imageLiteral(resourceName: "icHomeGraphic")
            self.congratulationLabel.text = StringConstants.Please_Shortlist_Candidate
            self.descriptionLabel.text = self.desText
        }else if commingFrom == .report {
             self.resetPasswordImageView.image = #imageLiteral(resourceName: "icHomeGraphic")
            self.congratulationLabel.text = StringConstants.REPORTED
            self.descriptionLabel.text = self.desText
        }else if commingFrom == .starDeduct {
             self.resetPasswordImageView.image = #imageLiteral(resourceName: "icHomeGraphic")
            self.congratulationLabel.text = StringConstants.Please_Consume_Star
            self.descriptionLabel.text = self.desText
        }else if commingFrom == .contactus {
                self.descriptionLabel.text = StringConstants.Contact_Us_AlertText
            self.resetPasswordImageView.image = #imageLiteral(resourceName: "icForgotPasswordSend")
            self.congratulationLabel.text = StringConstants.Great
        }
        else{
            self.descriptionLabel.text = StringConstants.You_just_applied_this_job_Hear_back_hours
            
        }
    }
    
    func dismissPopupViewOnTouches() {
        
        UIView.animate(withDuration: 0.33, delay: 0.0, usingSpringWithDamping: 0.9 , initialSpringVelocity: 0.4, options: UIViewAnimationOptions(), animations: {
            
            self.popupView.transform = CGAffineTransform( scaleX: 0.0000001,y: 0.0000001)
        }){ (true) in
            
            if commingFrom == .verifyotp {
                //  let sceen = LogoutVC.instantiate(fromAppStoryboard: .Main)
                let sceen = RecruiterTabBarVC.instantiate(fromAppStoryboard: .Recruiter)
                self.navigationController?.pushViewController(sceen, animated: true)
            } else if commingFrom == .resetPassword{
                
                if userType == .candidate {
                    if let controllers = self.navigationController?.viewControllers{
                        if let viewcontroller = controllers.first as? LoginVC{
                            self.popToViewController(viewcontroller, animated: true)
                        }else{
                            let loginScene = LoginVC.instantiate(fromAppStoryboard: .Main)
                            self.navigationController?.pushViewController(loginScene, animated: true)

                        }

                    }else{
                        let loginScene = LoginVC.instantiate(fromAppStoryboard: .Main)
                        self.navigationController?.pushViewController(loginScene, animated: true)

                    }
                } else if userType == .recuriter {
                    
                    if let vc = self.navigationController?.viewControllers.first as? LoginVC {
                        self.popToViewController(vc, animated: true)

                    }else {
                        let loginScene = LoginVC.instantiate(fromAppStoryboard: .Main)
                        self.navigationController?.pushViewController(loginScene, animated: true)
                    }
                }
            }else if commingFrom == CommingFrom.recruiterEditProfile{
                
                guard let vcArray = self.navigationController?.viewControllers else{return}
                for viewcontroller in vcArray{
                    if let vc = viewcontroller as? RecruiterEditProfileVC{
                        vc.recruiterProfileData?.recruiterCompanyData.mobileNo = self.mobileDetail["mobile"] ?? ""
                        vc.recruiterProfileData?.recruiterCompanyData.countrycode = self.mobileDetail["countryCode"] ?? ""
                        
                        self.popToViewController(vc, animated: true)
                        vc.editProfileTableView.reloadData()
                    }
                }
            }
            
            self.view.removeFromSuperview()
            self.removeFromParent
        }
    }
    
    @IBAction func okButtonTapped(_ sender: UIButton) {
        if isFromSavedJob || commingFrom == .accountExists {
            self.dismiss(animated: true, completion: nil)
        } else {
            self.dismissPopupViewOnTouches()
        }
        self.delegate?.presentShortlistedPopup(jobApplyId: jobId, categoryId: "")
    }
    
}


