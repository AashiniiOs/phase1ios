//
//  ResetPasswordVC.swift
//  Onboarding
//
//  Created by Appinventiv on 21/09/17.
//  Copyright © 2017 Gurdeep Singh. All rights reserved.
//

import UIKit

class ResetPasswordVC: BaseVC {
    
    //MARK:- Properties
    //==================
    var otp : String = ""
    var email : String = ""
    var userType = ""
    
    //MARK:- IBoutlets
    //=================
    @IBOutlet weak var resetPasswordButton          : UIButton!
    @IBOutlet weak var confirmPasswordTextField     : FloatingTextField!
    @IBOutlet weak var newPasswordTextField         : FloatingTextField!
    @IBOutlet weak var descriptionLabel             : UILabel!
    @IBOutlet weak var resetPasswordLabel           : UILabel!
    @IBOutlet weak var backButton                   : UIButton!
    @IBOutlet weak var resetPasswordScrollView: UIScrollView!
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

//MARK:- Private Methods
//=======================
private extension ResetPasswordVC {
    
    private func initialSetup() {
        
        self.resetPasswordLabel.textColor = AppColors.black46
        self.descriptionLabel.textColor   = AppColors.gray152
        self.resetPasswordButton.setCorner(cornerRadius: 5, clip: true)
        self.confirmPasswordTextField.delegate = self
        self.newPasswordTextField.delegate = self
        self.confirmPasswordTextField.isSecureText = true
        self.newPasswordTextField.isSecureText = true
        self.setupTextField()
        self.setupTapGestures()
        self.isEnableResetBtn(status: false)
    }
    
    func setupTextField() {
        
        CommonClass.setActiveLabelTextField(textField: self.newPasswordTextField, placeholder: StringConstants.New_Password.localized, title: StringConstants.New_Password_Title)
        
        CommonClass.setActiveLabelTextField(textField: self.confirmPasswordTextField, placeholder: StringConstants.Confirm_Password.localized, title: StringConstants.Confirm_Password_Title)
        
        self.newPasswordTextField.setIconImage(img: #imageLiteral(resourceName: "icLoginPassword"), size: CGSize(width: 30.0 , height: 30.0))
        self.confirmPasswordTextField.setIconImage(img: #imageLiteral(resourceName: "icLoginPassword"), size: CGSize(width: 30.0 , height: 30.0))
    }
    
    func setupTapGestures() {
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.dissmissKeyboardOnTouch))
        recognizer.numberOfTapsRequired = 1
        recognizer.numberOfTouchesRequired = 1
        self.resetPasswordScrollView.addGestureRecognizer(recognizer)
    }
    
    func isEnableResetBtn(status: Bool) {
        
        status ? (self.resetPasswordButton.backgroundColor = AppColors.themeBlueColor) : (self.resetPasswordButton.backgroundColor = AppColors.buttonDisableBgColor)
        self.resetPasswordButton.isEnabled = status
    }
    
    func popViewController() {
        
        self.navigationController?.popViewController(animated: true)
    }
}

//MARK:- Actions and Targets
//===========================
extension ResetPasswordVC {
    
    @IBAction func resetPasswordButtonTapped(_ sender: UIButton) {
        
        // self.view.endEditing(true)
        if self.isDataValid {
            
            self.hitWebserviceForResetPassword()
        }
    }
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.popViewController()
    }
    
    @objc func dissmissKeyboardOnTouch() {
        
        self.view.endEditing(true)
    }
}

//MARK:- Web Services
//====================
extension ResetPasswordVC {
    
    func hitWebserviceForResetPassword() {
        
        guard let password = self.newPasswordTextField.text else {
            return
        }
        
        let params = ["email" : self.email,
                      "password" : password,
                      "user_type" : self.userType
        ]
        
        WebServices.resetPasswordAPI(parameters: params, success: { [unowned self] (json) in
            
            let code = json["code"].intValue
            
            if code == error_codes.success{
                
                let resetPopup = ResetPasswordPopupVC.instantiate(fromAppStoryboard: .Main)
                commingFrom = .resetPassword
                
                self.view.addSubview(resetPopup.view)
                self.add(childViewController: resetPopup)
            }else{
                let msg = json["message"].stringValue
                CommonClass.showToast(msg: msg)
            }
            
        }) { (e) in
            CommonClass.showToast(msg: e.localizedDescription)
            
        }
    }
}

//MARK:- Validations
//==================
extension ResetPasswordVC {
    
    var isDataValid : Bool {
        
        guard let password = self.newPasswordTextField.text else {
            return false
        }
        guard let confirmPassword = self.confirmPasswordTextField.text else {
            return false
        }
        
        if password.isEmpty {
            CommonClass.showToast(msg: StringConstants.Enter_New_Password.localized)
            //self.showAlert(msg: StringConstants.Enter_New_Password.localized)
        }else if password.checkIfInvalid(.password) {
            CommonClass.showToast(msg: StringConstants.Password_Eight_Char_Long)
            
        }else if confirmPassword.isEmpty {
            CommonClass.showToast(msg: StringConstants.Password_Eight_Char_Long)
        }else if confirmPassword != password {
            CommonClass.showToast(msg: StringConstants.Confirm_Password_Wrong.localized)
            
        }else{
            return true
        }
        
        return false
    }
}

//MARK:- Validations
//==================
extension ResetPasswordVC: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if string == " " {
            return false
        }
        
        if textField === self.newPasswordTextField {
            
            return (textField.text ?? "").count - range.length < 16
            
        } else if textField === self.confirmPasswordTextField {
            
            return (textField.text ?? "").count - range.length < 16
        }
        
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if (self.newPasswordTextField.text ?? "").count > 0,  (self.confirmPasswordTextField.text ?? "").count > 0{
            self.isEnableResetBtn(status: true)
        } else {
            self.isEnableResetBtn(status: false)
        }
    }
}
