//
//  SelectUserTypeVC.swift
//  Onboarding
//
//  Created by macOS on 28/03/18.
//  Copyright © 2018 Gurdeep Singh. All rights reserved.
//

import UIKit

var userType = UserType.candidate
class SelectUserTypeVC: BaseVC {
    
    //MARK:- Properties
    //==================
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var getAJobButton    : UIButton!
    @IBOutlet weak var hireStaffButton  : UIButton!
    @IBOutlet weak var backgroundImage: UIImageView!
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // self.motionEffectAnimation()
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
}


//MARK:-  Methods
//=======================
extension SelectUserTypeVC {
    
    func initialSetup() {
        
        self.getAJobButton.setCorner(cornerRadius: 5, clip: true)
        self.hireStaffButton.setCorner(cornerRadius: 5, clip: true)
        
        if userType == UserType.candidate {
            self.setButtonColor(coloredButton: self.getAJobButton, nonColoredButton: self.hireStaffButton)
        } else {
            self.setButtonColor(coloredButton: self.hireStaffButton, nonColoredButton: self.getAJobButton)
        }
        
    }
    
    func setButtonColor(coloredButton: UIButton, nonColoredButton: UIButton) {
        coloredButton.borderWidth = 0
        coloredButton.borderColor = nil
        coloredButton.backgroundColor = AppColors.themeBlueColor
        
        nonColoredButton.borderWidth = 1.5
        nonColoredButton.borderColor = AppColors.white
        nonColoredButton.backgroundColor = UIColor.clear
    }
    //MARK:- MotionEffectAnimation
    //============================
    func motionEffectAnimation(){
        
        let min = CGFloat(-30)
        let max = CGFloat(30)
        let xMotion = UIInterpolatingMotionEffect(keyPath: "layer.transform.translation.x", type: .tiltAlongHorizontalAxis)
        xMotion.minimumRelativeValue = min
        xMotion.maximumRelativeValue = max
        
        let yMotion = UIInterpolatingMotionEffect(keyPath: "layer.transform.translation.y", type: .tiltAlongVerticalAxis)
        yMotion.minimumRelativeValue = min
        yMotion.maximumRelativeValue = max
        
        let motionEffectGroup = UIMotionEffectGroup()
        motionEffectGroup.motionEffects = [xMotion,yMotion]
        self.backgroundImage.addMotionEffect(motionEffectGroup)
    }
    
}

//MARK: - IB Action and Target
//===============================
extension SelectUserTypeVC {
    
    @IBAction func getAJobBtnTapped(_ sender: UIButton) {
        
        self.setButtonColor(coloredButton: sender, nonColoredButton: self.hireStaffButton)
        userType = .candidate
        let sceen = SocialLoginVC.instantiate(fromAppStoryboard: .Main)
        self.navigationController?.pushViewController(sceen, animated: true)
    }
    
    @IBAction func hireStaffBtnTapped(_ sender: UIButton) {
        
        self.setButtonColor(coloredButton: sender, nonColoredButton: self.getAJobButton)
        userType = .recuriter
        let sceen = SocialLoginVC.instantiate(fromAppStoryboard: .Main)
        self.navigationController?.pushViewController(sceen, animated: true)
    }
}
