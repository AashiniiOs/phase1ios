//
//  SignUpVC.swift
//  Onboarding
//
//  Created by Appinventiv on 09/09/16.
//  Copyright © 2016 Gurdeep Singh. All rights reserved.
//

import UIKit


class SignUpVC: BaseVC {
    
    //MARK:- Enum
    //=============
    enum CommingFrom {
        case loginSceen
        case socialSceen
        case none
    }
    
    //MARK:- IBOutlet
    //===============
    @IBOutlet var signupTableView: UITableView!
    
    //MARK:- Properties
    //=================
    var commingFrom: CommingFrom = .none
    var signupData  = [String: String]()
    var signupButtonState = false
    
    //MARK:- VIEW LIFE CYCLE
    //======================
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.initialSetup()
    }
    
}

//MARK:- Private Methods
//=======================
private extension SignUpVC {
    
    private func initialSetup() {
        
        self.signupTableView.delegate = self
        self.signupTableView.dataSource = self
        
        self.signupTableView.separatorColor      = self.signupTableView.backgroundColor
        self.registerNib()
        self.setupTapGestures()
    }
    
    func registerNib() {
        
        let submitBtn = UINib(nibName: "SubmitButtonCell", bundle: nil)
        self.signupTableView.register(submitBtn, forCellReuseIdentifier: "SubmitButtonCell")
        
        let nib = UINib(nibName: "EmailTextFieldCell", bundle: nil)
        self.signupTableView.register(nib, forCellReuseIdentifier: "EmailTextFieldCell")
    }
    
    func isEnableLoginBtn(sender: UIButton, status: Bool) {
        
        status ? (sender.backgroundColor = AppColors.themeBlueColor) : (sender.backgroundColor = AppColors.buttonDisableBgColor)
        sender.isEnabled = status
    }
    
    func setupTapGestures() {
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(self.dissmissKeyboardOnTouch))
        recognizer.numberOfTapsRequired = 1
        recognizer.numberOfTouchesRequired = 1
        self.signupTableView.addGestureRecognizer(recognizer)
    }
    
    func checkTextFieldHasText() {
        
        guard let buttonCell = self.signupTableView.cellForRow(at: IndexPath(row: 6, section: 0)) as? SubmitButtonCell else {
            return
        }
        
        guard let firstNameCell = self.signupTableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? EmailTextFieldCell else { return }
        
        guard let lastNameCell = self.signupTableView.cellForRow(at: IndexPath(row: 1, section: 0)) as? EmailTextFieldCell else { return }
        
        guard let emailCell = self.signupTableView.cellForRow(at: IndexPath(row: 2, section: 0)) as? EmailTextFieldCell else { return }
        
        guard let passwordCell = self.signupTableView.cellForRow(at: IndexPath(row: 3, section: 0)) as? EmailTextFieldCell else { return }
        
        
        if (firstNameCell.emailTextField.text ?? "").count > 0, (lastNameCell.emailTextField.text ?? "").count > 0,(emailCell.emailTextField.text ?? "").count > 0, (passwordCell.emailTextField.text ?? "").count > 0 {
            self.signupButtonState = true
            self.isEnableLoginBtn(sender: buttonCell.submitButton, status: true)
        } else {
            self.signupButtonState = false
            self.isEnableLoginBtn(sender: buttonCell.submitButton, status: false)
        }
        
    }
}


//MARK:- IBActions and Targets
//=============================
extension SignUpVC {
    
    @IBAction func backButtonTapped(_ sender: UIButton) {
        self.pop()
    }
    
    
    @objc func submitButtonTapped(_ sender: UIButton) {
        
        self.view.endEditing(true)
        if self.checkValidity() {
            self.hitWebserviceForSignup()
        }
        
    }
    
    @objc func loginButtonTapped(sender: UIButton) {
        if self.commingFrom == .loginSceen {
            self.pop()
        } else if self.commingFrom == .socialSceen {
            let sceen = LoginVC.instantiate(fromAppStoryboard: .Main)
            sceen.commingFrom = .signupSceen
            self.navigationController?.pushViewController(sceen, animated: true)
        } else {
            self.pop()
        }
        
    }
    
    @objc func didChangeTextFieldEditing(sender: UITextField){
        
        guard let idx = sender.tableViewIndexPath(self.signupTableView) else{
            return
        }
        
        switch idx.row {
            
        case 0:
            self.signupData[StringConstants.first_name]        = (sender.text ?? "").trimmingCharacters(in: .whitespaces)
            
        case 1:
            self.signupData[StringConstants.last_name]        = (sender.text ?? "").trimmingCharacters(in: .whitespaces)
            
        case 2:
            self.signupData[StringConstants.email]        = (sender.text ?? "").trimmingCharacters(in: .whitespaces)
        case 3:
            self.signupData[StringConstants.password]    = (sender.text ?? "").trimmingCharacters(in: .whitespaces)
        case 4:
            self.signupData["referral_code"] = (sender.text ?? "").trimmingCharacters(in: .whitespaces)
            
        default:
            fatalError("invalid text field editing changed")
        }
    }
    
    @objc func showPasswordBtnTapped(_ sender: UIButton) {
        
        self.view.endEditing(true)
        
        
        guard let index = sender.tableViewIndexPath(self.signupTableView) else { return }
        guard let cell = self.signupTableView.cellForRow(at: index) as? EmailTextFieldCell else { return }
        
        sender.isSelected = !sender.isSelected
        cell.emailTextField.isSecureTextEntry = !cell.emailTextField.isSecureTextEntry
    }
    
    @objc func dissmissKeyboardOnTouch() {        
        self.view.endEditing(true)
    }
}
//
//==================
extension SignUpVC {
    
    func checkValidity() -> Bool {
        
        let fistName    = self.signupData[StringConstants.first_name] ?? ""
        let lastName    = self.signupData[StringConstants.last_name] ?? ""
        let email       = self.signupData[StringConstants.email] ?? ""
        let pass        = self.signupData[StringConstants.password] ?? ""
        
        
        if !fistName.isEmpty {
            if fistName.count < 2 {
                CommonClass.showToast(msg: StringConstants.First_Name_Invalid_Length.localized)
                return false
            }
        }
        
        if !lastName.isEmpty {
            if lastName.count < 1 {
                CommonClass.showToast(msg: StringConstants.Last_Name_Invalid_Length.localized)
                return false
            }
        }
        if !email.isEmpty{
            if email.checkIfInvalid(.email) {
                CommonClass.showToast(msg:  StringConstants.Enter_Email.localized)
                return false
            }
        } else {
            return false
        }
        
        if !pass.isEmpty{
            if pass.count < 6{
                CommonClass.showToast(msg: StringConstants.Invalid_Password.localized)
                return false
            }else if pass.checkIfInvalid(ValidityExression.password) {
                CommonClass.showToast(msg: StringConstants.Invalid_Password.localized)
                return false
            }
        } else {
            return false
        }
        return true
    }
}


//MARK:- WebServices
//===================
extension SignUpVC {
    
    func hitWebserviceForSignup(){
        
        self.signupData[StringConstants.device_type]  = Device_Type.iOS.rawValue
        self.signupData[StringConstants.device_token] = DeviceDetail.deviceToken
        self.signupData[StringConstants.user_type]    = userType.rawValue
        self.signupData["fcm_token"]   = sharedAppDelegate.fcmToken ?? "dummyjobget12345"
        self.signupData[StringConstants.device_id]    = DeviceDetail.device_id
        
        print_debug(self.signupData)
        WebServices.signupApi(parameters: self.signupData, success: { (user) -> () in
            
            switch user.user_Type{
            case .none:
                return
            case .candidate:
                let sceen = CandidateTabBarVC.instantiate(fromAppStoryboard: .Candidate)
                sharedAppDelegate.navigationController = UINavigationController(rootViewController: sceen)
                sharedAppDelegate.navigationController?.isNavigationBarHidden = true
                sharedAppDelegate.window?.rootViewController =  sharedAppDelegate.navigationController
                sharedAppDelegate.window?.makeKeyAndVisible()
                
            case .recuriter:
                let sceen = RecruiterCompanyNameVC.instantiate(fromAppStoryboard: .Main)
                self.navigationController?.pushViewController(sceen, animated: true)
            }
            
            
        }, failure: { (error : Error) -> () in
            
            CommonClass.showToast(msg: error.localizedDescription)
        })
    }
}



//MARK:- TableView Delegate & Datasource
//======================================
extension SignUpVC : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        case 0...4:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "EmailTextFieldCell") as? EmailTextFieldCell else{
                fatalError("EmailTextFieldCell not found line: \(#line) function: \(#function) file: \(#file)")
            }
            
            cell.emailTextField.addTarget(self, action: #selector(self.didChangeTextFieldEditing(sender:)), for: .editingDidEnd)
            switch indexPath.row {
            case 0:
                cell.setupTextField(placeholder: StringConstants.First_Name.localized, title: "         \(StringConstants.First_Name.localized)")
                cell.emailTextField.autocapitalizationType = .words
                cell.emailTextField.setIconImage(img: #imageLiteral(resourceName: "icSignupUsername"), size: CGSize(width: 30.0, height: 30.0))
                cell.emailTextField.text = self.signupData[StringConstants.first_name] ?? ""
                cell.emailTextField.delegate = self
                cell.emailTextField.returnKeyType = .next
            case 1:
                cell.setupTextField(placeholder: StringConstants.Last_Name.localized, title: "        \(StringConstants.Last_Name.localized)")
                cell.emailTextField.autocapitalizationType = .words
                cell.emailTextField.setIconImage(img: #imageLiteral(resourceName: "icSignupUsername"), size: CGSize(width: 30.0, height: 30.0))
                cell.emailTextField.text = self.signupData[StringConstants.last_name] ?? ""
                cell.emailTextField.delegate = self
                cell.emailTextField.returnKeyType = .next
            case 2:
                cell.setupTextField(placeholder: StringConstants.Email_Address.localized, title: StringConstants.Email_Address.localized)
                cell.emailTextField.keyboardType = .emailAddress
                cell.emailTextField.autocorrectionType = .no
                cell.emailTextField.setIconImage(img: #imageLiteral(resourceName: "icLoginEmail"), size: CGSize(width: 30.0, height: 30.0))
                cell.emailTextField.text = self.signupData[StringConstants.email] ?? ""
                cell.emailTextField.delegate = self
                cell.emailTextField.returnKeyType = .next
            case 3:
                cell.setupTextField(placeholder: StringConstants.Password.localized, title: StringConstants.Password)
                cell.emailTextField.setIconImage(img: #imageLiteral(resourceName: "icLoginPassword"), size: CGSize(width: 30.0, height: 30.0))
                cell.emailTextField.isSecureTextEntry = true
                let button: UIButton = UIButton()
                button.titleLabel?.font = AppFonts.Poppins_Regular.withSize(11.0)
                button.setTitleColor(AppColors.gray152, for: .normal)
                button.addTarget(self, action: #selector(self.showPasswordBtnTapped(_:)), for: .touchUpInside)
                cell.emailTextField.setButtonToRightView(btn: button, selectedText: StringConstants.Hide, normalText: StringConstants.Show, size: nil)
                cell.emailTextField.text = self.signupData[StringConstants.password] ?? ""
                cell.emailTextField.delegate = self
                if userType == .recuriter{
                    cell.emailTextField.returnKeyType = .next
                }else{
                    cell.emailTextField.returnKeyType = .done
                }
            case 4:
                if userType == .recuriter{
                    cell.setupTextField(placeholder: StringConstants.Referral_Code_Optional, title: StringConstants.Referral_Code)
                    cell.emailTextField.setIconImage(img: #imageLiteral(resourceName: "icLoginPassword"), size: CGSize(width: 30.0, height: 30.0))
                    cell.emailTextField.text = self.signupData["referral_code"] ?? ""
                    cell.emailTextField.delegate = self
                    cell.emailTextField.returnKeyType = .done
                }else{
                    return UITableViewCell()
                }
            
            default:
                return UITableViewCell()
            }
            
            return cell
            
        case 6:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SubmitButtonCell") as? SubmitButtonCell else{
                fatalError("SubmitButtonCell not found line: \(#line) function: \(#function) file: \(#file)")
            }
            cell.submitButton.addTarget(self, action: #selector(self.submitButtonTapped(_:)), for: .touchUpInside)
            cell.submitButton.setTitle(StringConstants.SIGNUP, for: .normal)
            if self.signupButtonState {
                cell.submitButton.backgroundColor = AppColors.themeBlueColor
            } else {
                cell.submitButton.backgroundColor = AppColors.buttonDisableBgColor
            }
            
            cell.submitButton.isEnabled =  self.signupButtonState
            
            
            return cell
            
        case 5:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "AlreadyAccountCell") as? AlreadyAccountCell else{
                fatalError("AlreadyAccountCell not found line: \(#line) function: \(#function) file: \(#file)")
            }
            cell.loginButton.addTarget(self, action: #selector(self.loginButtonTapped(sender:)), for: .touchUpInside)
            return cell
            
            
        default:
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CreateAccountCell") as? CreateAccountCell else{
            fatalError("AlreadyAccountCell not found line: \(#line) function: \(#function) file: \(#file)")
        }
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch indexPath.row {
        case 0...4:
            if indexPath.row == 4{
                if userType == .recuriter{
                    return 70
                }else{
                    return 0
                }
            }else{
                if userType == .recuriter{
                     return 70
                }else{
                     return 80
                }
            }
            
        case 6:
            
            return 100
        case 5:
            return UITableViewAutomaticDimension
        default:
            return 0.0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 100
    }
}


//MARK:- TextFieldDelegate
//========================
extension SignUpVC: UITextFieldDelegate {
    
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        
        guard let idx = textField.tableViewIndexPath( self.signupTableView) else {
            return false
        }
        switch idx.row {
        case 0, 1:
            if (textField.text ?? "").count == 0, string == " " {
                return false
            } else if (textField.text ?? "").count - range.length < 200 {
                
                let cs = NSCharacterSet(charactersIn: ACCEPTABLE_CHARACTERS).inverted
                let filtered = string.components(separatedBy: cs).joined(separator: "")
                
                return (string == filtered)
                
            } else {
                return false
            }
        case 2:
            if string == " " {
                return false
            } else {
                return (textField.text ?? "").count - range.length < 50
            }
        case 3:
            
            if string == " " {
                return false
            }
            
        case 4:
            
            if string == " " {
                return false
            }
            
        default:
            fatalError("invalid textField in signupVC")
        }
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        guard let indexPath = textField.tableViewIndexPath(signupTableView) else { return false }
        
        if textField.returnKeyType == UIReturnKeyType.next {
            
            switch indexPath.row {
            case 0...3:
                if let nextCell = signupTableView.cellForRow(at: IndexPath(row: indexPath.row + 1, section: 0)) as? EmailTextFieldCell {
                    
                    nextCell.emailTextField.becomeFirstResponder()
                }
                
            default:
                break
            }
        } else {
            textField.resignFirstResponder()
        }
        return true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        self.checkTextFieldHasText()
    }
}

//MARK:- Prototype Cell
//========================

class AlreadyAccountCell : UITableViewCell {
    
    //MARK:- IB Outlets
    //===================
    @IBOutlet weak var alreadyHaveAccountLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
}

class CreateAccountCell : UITableViewCell {
    
    //MARK:- IB Outlets
    //===================
    @IBOutlet weak var helloLabel: UILabel!
    @IBOutlet weak var createAnAccountLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.helloLabel.textColor = AppColors.black46
        self.createAnAccountLabel.textColor = AppColors.lightGray103
    }
    
}



