//
//  SocialLoginVC.swift
//  JobGet
//
//  Created by macOS on 03/04/18.
//  Copyright © 2018 Appinventiv Technologies. All rights reserved.
//

import UIKit

class SocialLoginVC: BaseVC {
    
    //MARK:- Properties
    //==================
    
    //MARK:- IBoutlets
    //================
    @IBOutlet weak var fbContainerView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var fbLoginButton: UIButton!
    @IBOutlet weak var orLabel: UILabel!
    @IBOutlet weak var findJobLabel: UILabel!
    @IBOutlet weak var fbSeperatorView: UIView!
    @IBOutlet weak var signupButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var bgImage: UIImageView!
    
    //MARK:- view Life cycle
    //=======================
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.initialSetup()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
}

//MARK: - IB Action and Target
//===============================
extension SocialLoginVC {
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func fbLoginBtnTapped(_ sender: UIButton) {
        FacebookController.shared.facebookLogout()
        FacebookController.shared.getFacebookUserInfo(fromViewController: self, success: { (result,token) in
            print_debug(result)
            var details = [String:Any]()
            
            details["email"] = result.email
            details["facebook_id"] = result.id
            details["first_name"] = result.first_name
            details["last_name"] = result.last_name
            details["fcm_token"]   = sharedAppDelegate.fcmToken ?? "dummyjobget12345"
            details[StringConstants.device_type]  = Device_Type.iOS.rawValue
            details[StringConstants.device_token] = DeviceDetail.deviceToken
            details[StringConstants.device_id] = DeviceDetail.device_id
            
            
            details[StringConstants.user_type]    = userType.rawValue
            
            
//            ChatHelper.facebookLogin(fbAccessToken: token, completion: { (success) in
//                if success{
//                    let data = NSKeyedArchiver.archivedData(withRootObject: result.picture as Any)
//                    AppUserDefaults.save(value: data, forKey: .facebookImage)
//                    print_debug("Facebook login succeeded")
//                     ChatHelper.updateUsrDetails()
//                }else{
//                    print_debug("Facebook login failed")
//                }
//            })
            if let imgUrl = result.picture{
                let data = NSKeyedArchiver.archivedData(withRootObject: "\(imgUrl)")
               
//                AppUserDefaults.save(value: "\(imgUrl)", forKey: .facebookImage)
                details["user_image"] = imgUrl
                AppUserDefaults.save(value: data, forKey: .facebookImage)
            }
            print_debug(details)
            self.callSocialLogin(param: details)
        }){ (error) in
        }
    }
    
    @IBAction func signupButtonTapped(_ sender: UIButton) {
        let sceen = SignUpVC.instantiate(fromAppStoryboard: .Main)
        sceen.commingFrom = .socialSceen
        self.navigationController?.pushViewController(sceen, animated: true)
    }
    
    @IBAction func loginButtonTapped(_ sender: UIButton) {
        let sceen = LoginVC.instantiate(fromAppStoryboard: .Main)
        sceen.commingFrom = .socialSceen
        self.navigationController?.pushViewController(sceen, animated: true)
    }
    
    
}

//MARK:- Private Methods
//=======================
private extension SocialLoginVC {
    
    func initialSetup() {
        
        
        self.fbLoginButton.backgroundColor = AppColors.fbBlue
        self.fbContainerView.backgroundColor = AppColors.fbBlue
        
        self.fbLoginButton.setTitleColor(AppColors.gray227, for: .normal)
        self.fbSeperatorView.backgroundColor =  AppColors.gray227.withAlphaComponent(0.20)
        self.fbContainerView.setCorner(cornerRadius: 5, clip: true)
        self.fbLoginButton.setCorner(cornerRadius: 5.0, clip: true)
        self.loginButton.setCorner(cornerRadius: 5.0, clip: true)
        self.signupButton.setCorner(cornerRadius: 5.0, clip: true)
        
        self.loginButton.borderColor = AppColors.white
        self.signupButton.borderColor = AppColors.white
        self.loginButton.borderWidth = 2.0
        self.signupButton.borderWidth = 2.0
        
        if userType == .candidate {
            self.findJobLabel.text = StringConstants.FIND_A_JOB_AND_APPLY_NOW
        } else if userType == .recuriter {
            self.findJobLabel.text = StringConstants.POST_A_JOB_AND_HIRE_NOW
        }
    }
}

//MARK:- Private Methods
//=======================
extension SocialLoginVC{
    
    func callSocialLogin(param: [String: Any]) {
        
        WebServices.socialLogin(parameters: param, success: { (user) -> () in
            print_debug(user)
            
            switch user.user_Type {
            case .none:
                return
            case .candidate:
                let sceen = CandidateTabBarVC.instantiate(fromAppStoryboard: .Candidate)
                
               sharedAppDelegate.navigationController = UINavigationController(rootViewController: sceen)
                sharedAppDelegate.navigationController?.isNavigationBarHidden = true
                sharedAppDelegate.window?.rootViewController = sharedAppDelegate.navigationController
                sharedAppDelegate.window?.makeKeyAndVisible()
                
            case .recuriter:
                
                let isProfileAdded = AppUserDefaults.value(forKey: .isProfileAdded).stringValue
                print_debug(isProfileAdded)
                if isProfileAdded == "0" {
                    let recruiter = RecruiterCompanyNameVC.instantiate(fromAppStoryboard: .Main)
                    self.navigationController?.pushViewController(recruiter, animated: true)
                } else {
                    let isOtpVerified = AppUserDefaults.value(forKey: .isOtpVerified).stringValue
                    print_debug(isOtpVerified)
                    if isOtpVerified == "0" {
                        let mainVC = SelectUserTypeVC.instantiate(fromAppStoryboard: .Main)
                        
                        let recruiterCompanyNameVC = RecruiterCompanyNameVC.instantiate(fromAppStoryboard: .Main)
                        let recruiterVerifyOtpVC = RecruiterVerifyOtpVC.instantiate(fromAppStoryboard: .Main)
                        self.navigationController?.setViewControllers([mainVC, recruiterCompanyNameVC, recruiterVerifyOtpVC], animated: true)
                        
                    } else {
                        
                        let sceen = RecruiterTabBarVC.instantiate(fromAppStoryboard: .Recruiter)
                        sharedAppDelegate.navigationController = UINavigationController(rootViewController: sceen)
                        sharedAppDelegate.navigationController?.isNavigationBarHidden = true
                        sharedAppDelegate.window?.rootViewController = sharedAppDelegate.navigationController
                        sharedAppDelegate.window?.makeKeyAndVisible()
//                        CommonClass.showToast(msg: user.responseString ?? StringConstants.Something_Went_Wrong.localized)
                    }
                }
            }
            
        }, failure: { (error : Error) -> () in
            CommonClass.showToast(msg: error.localizedDescription)
        })
    }
}
